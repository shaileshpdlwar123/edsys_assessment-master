from django.conf.urls import include, url
from django.contrib import admin
from . import views
from django.contrib.auth.views import logout
from django.conf import settings
from django.conf.urls.static import static, serve

app_name = 'transfer_certificate'


urlpatterns = [
    
    url(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),

######################################### TC Details ###################################  
    url(r'^tc_detail/$',views.TCDetail, name='tc_detail'),  
    url(r'^apply_tc/$',views.ApplyTc, name='apply_tc'), 
    url(r'^filter_tc_apply/$',views.FilterApplyTc, name='filter_tc_apply'),
    url(r'^request_tc_form/$',views.RequestTcForm, name='request_tc_form'),
    url(r'^save_tc_form/$',views.SaveTCRequest, name='save_tc_form'), 
    url(r'^requested_tc_list/$',views.RequestedTcList, name='requested_tc_list'), 
    url(r'^grant_tc/$',views.GrantTC, name='grant_tc'),
    url(r'^view_alumni/$',views.ViewAlumni, name='view_alumni'),
    url(r'^view_alumni_list/$',views.ViewAlumniList, name='view_alumni_list'),
    url(r'^generate_tc/$',views.GenerateTc, name='generate_tc'),
    url(r'^reject_tc/$',views.RejectTC, name='reject_tc'),
    url(r'^reverse_tc/$',views.ReverseTC, name='reverse_tc'),
    url(r'^approve_reversal/$',views.ApproveReversal, name='approve_reversal'),
    url(r'^reject_reversal/$',views.RejectReversal, name='reject_reversal'),
    url(r'^export_tc/$',views.ExportTC, name='export_tc'),
    url(r'^export_tc_rec/$',views.ExportTCRec, name='export_tc_rec'),
    #url(r'^view_student_list/$',views.ViewStudentList, name='view_student_list'),
    url(r'^export_transfer_certificate/$',views.ExportTransferCertificate, name='export_transfer_certificate'),
    url(r'^student_details/(?P<std_id>[0-9]+)$', views.StudentDetails, name='student_details'),

        
   ]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
   


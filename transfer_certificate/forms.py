
from django import forms
from transfer_certificate.models import TcDetails
from datetime import datetime

inside_dubai = (
                    ('yes', ("yes")),
                    ('no', ("no")),
                )

inside_uae = (
                    ('yes', ("yes")),
                    ('no', ("no"))
              )

class TCDetailsForm(forms.ModelForm):
    student_id = forms.CharField(widget = forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    leaving_reason = forms.CharField(required = True,widget = forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off', 'maxlength':'255'}))
    inside_dubai = forms.TypedChoiceField(choices = inside_dubai, required = True, widget=forms.RadioSelect )
    inside_uae = forms.TypedChoiceField(choices = inside_uae, widget=forms.RadioSelect )
    # curriculam = forms.CharField(widget = forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    school_name = forms.CharField(required=True, widget = forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off', 'maxlength':'255'}))
    class Meta:
        model = TcDetails
        fields =['student_id','leaving_reason','inside_dubai','inside_uae','school_name']


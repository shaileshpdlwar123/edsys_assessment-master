from __future__ import unicode_literals
from registration.models import student_details
from django.db import models
from datetime import datetime

# Create your models here.
inside_dubai = (
                    ('yes', ("yes")),
                    ('no', ("no")),
                )

inside_uae = (
                    ('yes', ("yes")),
                    ('no', ("no")),
                )

class TcDetails(models.Model):
    leaving_reason = models.CharField(max_length = 255)
    inside_dubai = models.CharField(max_length=3, choices = inside_dubai)
    inside_uae = models.CharField(max_length=3, choices = inside_uae)
    curriculam = models.CharField(max_length = 45,null=True, blank=True)
    is_confirm = models.BooleanField(default=False)
    # is_reversal_approved = models.BooleanField(blank = True)
    is_reversal_approved = models.BooleanField(default=False)
    create_date = models.DateTimeField(default = datetime.now)
    update_date = models.DateTimeField(default = datetime.now)

    school_name = models.CharField(max_length = 255)
    student = models.ForeignKey(student_details)

    class Meta:
        permissions = (
            ('can_view_transfer_certificate', 'can view transfer certificate'),
            ('can_view_transfer_certificate_apply', 'can view transfer certificate apply'),
            ('can_view_transfer_certificate_view_confirm', 'can view transfer certificate view confirm'),
            ('can_view_transfer_certificate_view_alumni', 'can view transfer certificate alumni'),
            ('can_view_transfer_certificate_export_tc', 'can view transfer certificate export tc'),
        )


from django.shortcuts import render, redirect#, render_to_response
from .forms import *
#from transfer_certificate import forms
from registration.models import *
#from registration.forms import *
from django.http import HttpResponse
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image , TableStyle , Table
from reportlab.lib.pagesizes import A4, inch, landscape
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_CENTER
import django_excel as excel
from registration.decorators import user_permission_required, user_login_required
from datetime import datetime
from calendar import monthrange
from django.db.models import Q
from medical.models import *
from masters.utils import export_users_xls, todays_date
from django.contrib import messages
from masters.utils import export_users_xls, todays_date,current_academic_year_mapped_classes,current_class_mapped_sections,get_all_user_permissions
import time
# Create your views here.

@user_login_required
@user_permission_required('transfer_certificate.can_view_transfer_certificate', '/school/home/')
def TCDetail(request):
    perms = [] #get_all_user_permissions(request)
    return render(request, "tc_detail.html", {'perms': list(perms)})

@user_login_required
@user_permission_required('transfer_certificate.can_view_transfer_certificate_apply', '/school/home/')
def ApplyTc(request):
    student_list = []
    TCForm = TCDetailsForm()
    year_list = academic_year.objects.all()

    try:
        current_acd_year = academic_year.objects.get(current_academic_year=1)

        class_list = current_academic_year_mapped_classes(current_acd_year, request.user)
        # if not request.user.role.name == 'System Admin':
        if not request.user.is_system_admin():
            student_recs = student_details.objects.filter(is_active=1, joining_date__lte=todays_date()).filter(Q(academic_class_section__assistant_teacher = request.user.id) | Q(academic_class_section__staff_id = request.user.id) | Q(parent__user_id = request.user.id))
        else:
            student_recs = student_details.objects.filter(is_active=1, joining_date__lte=todays_date())

        for student_rec in student_recs:
            #If student not exists in tc class, then display on apply tc form
            if not (TcDetails.objects.filter(student = student_rec.id).exists()):
                student_list.append(student_rec)
        return render(request, "apply_tc.html" ,{'TCForm':TCForm,'list':student_list,'class_list':class_list,'year_list':year_list,'Selected_Year':current_acd_year})
    except:
        messages.warning(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
    return redirect('/school/tc_detail/')


def load_val_tc_apply(val_tc_apply,request):

    year_name = val_tc_apply.get('year_name')
    class_name = val_tc_apply.get('class_name')
    section_name = val_tc_apply.get('section_name')

    if not class_name == 'All':
        class_obj = class_details.objects.get(id=class_name)

    if not section_name == 'All':
        section_obj = sections.objects.get(id=section_name)

    if class_name == 'All':
        selected_class_name = 'All'
        selected_class_id = 'All'

    if section_name == 'All':
        selected_section_id = 'All'
        selected_section = 'All'

    if class_name == 'All' and section_name != 'All':

        # class_obj =  class_details.objects.get(id=class_name)
        section_obj =  sections.objects.get(id=section_name)

        error_flag = False

        TCForm = TCDetailsForm()
        student_list=[]
        section_list = []
        year_list = academic_year.objects.all()

        try:
            current_acd_year = academic_year.objects.get(current_academic_year=1)

            class_list = current_academic_year_mapped_classes(current_acd_year, request.user)
            # if class_name:
            #     section_list =  current_class_mapped_sections(current_acd_year,class_name,request.user)
            # ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name,
            #                                                      year_name_id=year_name,
            #                                                      section_name_id=section_name)

            if not request.user.is_system_admin():
                student_recs = student_details.objects.filter(academic_class_section__year_name_id = year_name,academic_class_section__section_name_id = section_name,is_active=1, joining_date__lte=todays_date()).filter(Q(academic_class_section__assistant_teacher = request.user.id) | Q(academic_class_section__staff_id = request.user.id) | Q(parent__user_id = request.user.id))
            else:
                student_recs = student_details.objects.filter(academic_class_section__year_name_id= year_name,academic_class_section__section_name_id = section_name,is_active=1, joining_date__lte=todays_date())

            for student_rec in student_recs:
                if not (TcDetails.objects.filter(student = student_rec.id).exists()):
                    student_list.append(student_rec)

            form_vals = {
                'TCForm': TCForm, 'list': student_list, 'class_list': class_list, 'year_list': year_list,
                 'Selected_Year': current_acd_year,'Selected_Section':section_obj

            }

            return form_vals,error_flag
        except:
            error_flag = True
            form_vals={}
            messages.warning(request, "Some Error Occoured. Please Try Again.")
        return form_vals,error_flag

    else:

        class_obj = class_details.objects.get(id=class_name)
        section_obj = sections.objects.get(id=section_name)
        error_flag = False

        TCForm = TCDetailsForm()
        student_list = []
        section_list = []
        year_list = academic_year.objects.all()

        try:
            current_acd_year = academic_year.objects.get(current_academic_year=1)

            class_list = current_academic_year_mapped_classes(current_acd_year, request.user)
            if class_name:
                section_list = current_class_mapped_sections(current_acd_year, class_name, request.user)
            ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name,
                                                                 year_name_id=year_name,
                                                                 section_name_id=section_name)

            if not request.user.is_system_admin():
                student_recs = student_details.objects.filter(academic_class_section__in=ac_recs, is_active=1,
                                                              joining_date__lte=todays_date()).filter(
                    Q(academic_class_section__assistant_teacher=request.user.id) | Q(
                        academic_class_section__staff_id=request.user.id) | Q(parent__user_id=request.user.id))
            else:
                student_recs = student_details.objects.filter(academic_class_section=ac_recs, is_active=1,
                                                              joining_date__lte=todays_date())

            for student_rec in student_recs:
                if not (TcDetails.objects.filter(student=student_rec.id).exists()):
                    student_list.append(student_rec)

            form_vals = {
                'TCForm': TCForm, 'list': student_list, 'class_list': class_list, 'year_list': year_list,
                'section_list': section_list, 'Selected_Year': current_acd_year, 'Selected_Section': section_obj,
                'Selected_Class': class_obj
            }

            return form_vals, error_flag
        except:
            error_flag = True
            form_vals = {}
            messages.warning(request, "Some Error Occoured. Please Try Again.")
        return form_vals, error_flag


@user_login_required
def FilterApplyTc(request):
    val_tc_apply = request.POST
    form_vals = {}
    error_flag= False

    try:
        if request.method == 'POST':
            request.session['val_tc_apply'] = val_tc_apply
            form_vals,error_flag = load_val_tc_apply(val_tc_apply, request)
        else:
            form_vals,error_flag = load_val_tc_apply(request.session.get('val_tc_apply'), request)
            # return redirect('/school/id_generator/')
        if error_flag:
            return redirect('/school/tc_detail/')
        return render(request, "apply_tc.html",form_vals)
    except:
        messages.warning(request, "Some Error Occoured. Please Try Again.")
    return redirect('/school/tc_detail/')

@user_login_required
@user_permission_required('transfer_certificate.can_view_transfer_certificate_apply', '/school/home/')
def RequestTcForm(request):
    if request.method == 'POST':
        check=request.POST.getlist('check')
        TCForm = TCDetailsForm()
    return render(request, "request_tc_form.html" ,{'TCForm':TCForm,'check':check })

@user_login_required
def SaveTCRequest(request):
    if request.method == 'POST':
        student_ids = request.POST['check']
        student_ids_cnverted = eval(student_ids)
        leaving_reason = request.POST['leaving_reason']
        inside_dubai = request.POST['inside_dubai']
        inside_uae = request.POST['inside_uae']
        # curriculam = request.POST['curriculam']
        school = request.POST.getlist('school_name')
        if len(school) == 0:
            school_name = ''
        else:
            school_name = school[0]

        for student_id in student_ids_cnverted:
            stud_id = student_details.objects.only('id').get(id = student_id)
            TcDetails.objects.create(student = stud_id, is_confirm = 0, leaving_reason = leaving_reason, inside_dubai = inside_dubai, inside_uae = inside_uae, school_name = school_name)
    return redirect('/school/apply_tc/')

@user_login_required
@user_permission_required('transfer_certificate.can_view_transfer_certificate_view_confirm', '/school/home/')
def RequestedTcList(request):
    # TC_Student_List = TcDetails.objects.all()

    if not request.user.is_system_admin():
        TC_Student_List = TcDetails.objects.filter(Q(student__parent__user_id=request.user.id))
    else:
        TC_Student_List = TcDetails.objects.all()

    return render(request, "grant_tc.html" ,{'TCDetailsList':TC_Student_List})

@user_login_required
@user_permission_required('transfer_certificate.can_view_transfer_certificate_view_confirm', '/school/home/')
def GrantTC(request):
    # TC_Student = TcDetails.objects.filter()
    
    if not request.user.is_system_admin():
        TC_Student = TcDetails.objects.filter(Q(student__parent__user_id=request.user.id))
    else:
        TC_Student = TcDetails.objects.all()

    student_id = request.POST.get('studentid')
    try:
        TcDetails.objects.filter(student=student_id).update(is_confirm=1, update_date=str(datetime.now()))
        student_details.objects.filter(id=student_id).update(is_active=0)
    except ValueError:
        pass
    return render(request, "grant_tc.html" ,{'TCDetailsList':TC_Student})

@user_login_required
@user_permission_required('transfer_certificate.can_view_transfer_certificate_view_alumni', '/school/home/')
def ViewAlumni(request):
    try:
        Classdetails=class_details.objects.all()
        Section=sections.objects.all()
        academic_year_recs=academic_year.objects.all()
        all_academicyr_recs = academic_year.objects.all()
        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id
        except:
            messages.warning(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
            return render(request, 'tc_detail.html')

        students=student_details.objects.all()

        if not request.user.is_system_admin():
            tc_student_list = TcDetails.objects.filter(is_confirm = 1).filter(Q(student__parent__user_id=request.user.id))
        else:
            tc_student_list = TcDetails.objects.filter(is_confirm = 1)

        # tc_student_list = TcDetails.objects.filter(is_confirm = 1)

        return render(request, "view_alumni.html" ,{"academic_year_recs": all_academicyr_recs, 'ACYearName': current_academic_year, 'acadmic_name': selected_year_id, 'ClassDetails':Classdetails,'Section':Section,'students':students,'academic_year_recs':academic_year_recs, 'list' : tc_student_list })
    except ValueError as e:
        pass
    return render(request, "view_alumni.html")

@user_login_required
@user_permission_required('transfer_certificate.can_view_transfer_certificate_view_alumni', '/school/home/')
def ViewAlumniList(request):
    tc_student_list = []
    all_classes =  class_details.objects.all()
    all_sections = sections.objects.all()
    all_academic_year = academic_year.objects.all()


    selected_academic_year = request.POST.get('academic_year')
    selected_academic_year_rec =  academic_year.objects.get(id = selected_academic_year)
    selected_academic_year_name = selected_academic_year_rec.year_name

    selected_class = request.POST.get('class_name')
    selected_class_rec =  class_details.objects.get(id = selected_class)
    selected_class_name = selected_class_rec.class_name
    selected_section = request.POST.get('section_name')
    selected_section_rec =  sections.objects.get(id = selected_section)
    selected_section_name = selected_section_rec.section_name

    year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_academic_year, section_name=selected_section, class_name=selected_class)
    student_list = student_details.objects.filter(academic_class_section=year_class_section_obj)

#     student_list = student_details.objects.filter(class_name=selected_class,section=selected_section)
    for student in student_list:
        if (TcDetails.objects.filter(student = student.id , is_confirm = 1).exists()):
            tc_student = TcDetails.objects.get(student = student.id)
            tc_student_list.append(tc_student)
    view_alumni_vals = {    'ACYearName' : selected_academic_year_name,
                            'acadmic_name' : selected_academic_year,
                            'academic_year_recs' : all_academic_year,

                            'SectName' : selected_section_name,
                            'section_name' : selected_section,
                            'Section' : all_sections,

                            'ClassName' : selected_class_name ,
                            'class_name' : selected_class,
                            'ClassDetails' : all_classes,
                            'list' : tc_student_list
                        }
    return render(request, "view_alumni.html", view_alumni_vals)

@user_login_required
def RejectTC(request):
    student_id = request.GET.get('studentid')
    TcDetails.objects.filter(student = student_id).delete()
    return render(request, "grant_tc.html")

@user_login_required
def ReverseTC(request):
    student_id = request.GET.get('studentid')
    TcDetails.objects.filter(student = student_id).update(is_reversal_approved = 1)
    # student_details.objects.filter(id=student_id).update(is_active=1)
    # return render(request, "view_alumni.html")
    return redirect('/school/view_alumni/')


@user_login_required
def ApproveReversal(request):
    student_id = request.POST.get('studentid')
    TcDetails.objects.filter(student = student_id).update(is_confirm = 0, is_reversal_approved = 0)
    student_details.objects.filter(id=student_id).update(is_active=1)
    # return render(request, "view_alumni.html")
    # print "asasa"
    return redirect('/school/view_alumni/')

@user_login_required
def RejectReversal(request):
    student_id = request.POST.get('studentid')
    TcDetails.objects.filter(student = student_id).update(is_reversal_approved = 0)
    # student_details.objects.filter(id=student_id).update(is_active=1)
    # return render(request, "view_alumni.html")
    print "pppppppp"
    return redirect('/school/requested_tc_list/')

@user_login_required
def GenerateTc(request):
    student_id = request.GET.get('studentid')
    current_date = datetime.now().strftime("%Y-%m-%d")
    tc_detail_rec = TcDetails.objects.get(student = student_id)
    student_detail_rec = student_details.objects.get(id=student_id)
    response = HttpResponse(content_type='application/pdf')
    doc = SimpleDocTemplate(response,topMargin=20)
    doc.pagesize = landscape(A4)
    birthDate = str(student_detail_rec.birth_date)
    joiningDate = str(student_detail_rec.joining_date)
    elements = []
    I = Image('http://demoschoolzen.educationzen.com/images/tia.png')
    I.drawHeight =  1.25*inch
    I.drawWidth = 1.25*inch
    data= [
                 ['Date : '+' '+''+' '+''+str(current_date)+'', '', 'Date Of Birth : '+' '+''+' '+''+ birthDate +'', ''],
                 ['Sex : '+' '+''+' '+''+student_detail_rec.gender+'', 'Name Of The Child : '+' '+''+' '+''+student_detail_rec.first_name+' '+''+'  '+student_detail_rec.father_name+'  '+''+'  '+student_detail_rec.last_name+'     ','',''],
                 ['Class : '+' '+''+' '+''+ student_detail_rec.academic_class_section.class_name.class_name +'', 'Division : '+' '+''+' '+''+student_detail_rec.academic_class_section.section_name.section_name+'',  'City : '+' '+''+' '+''+ str(student_detail_rec.city) +'', 'Nationality : '+' '+''+' '+''+ str(student_detail_rec.nationality) +''],
                 ['Student Registration No : '+' '+''+' '+''+ student_detail_rec.student_code +'', '', '', ''],
                 ['Date of Admission : '+' '+''+' '+''+joiningDate+'', '', '', ''],
                 ['Contact Details  : ', 'Name  :', ''+' '+''+' '+''+student_detail_rec.first_name+' '+''+'  '+student_detail_rec.father_name+'  '+''+'  '+student_detail_rec.last_name+'    '],
                 [' Contact Details  :', 'Tel No :', ''],
                 ['Contact Details  :', 'Email  :',''+' '+''+student_detail_rec.email+' '],
                 ['Is your child going to be transferred  :', '','Inside Dubai :',''+tc_detail_rec.inside_dubai+''],
                 ['Is your child going to be transferred  :', '','Inside UAE  :',''+tc_detail_rec.inside_uae+''],
                 ['New School Name:'+tc_detail_rec.school_name+''],
                 ['Reason For Leaving  : '+tc_detail_rec.leaving_reason+''],
                 ['Parent Signature :'],
                 ['-----------------------------------------------------------------OFFICE USE ONLY-------------------------------------------------'],
                 ['Date :','','ClassTeacher Signature: :',''],
                 ['Principal Signature :','','Accountant Signature :',''],
                 ['Student Name :'+' '+''+' '+''+student_detail_rec.first_name+' '+''+'  '+student_detail_rec.father_name+'  '+''+'  '+student_detail_rec.last_name+'  ','','Class:'+' '+''+' '+''+ student_detail_rec.academic_class_section.class_name.class_name +'',''],
                 ['Remark :','','',''],
          ]
    style =TableStyle([
                            ('GRID',(0,0),(-1,-1),1.5,colors.grey),
                            ('SPAN',(0,0),(1,0)),
                            ('SPAN',(2,0),(3,0)),
                            ('SPAN',(3,1),(1,1)),
                            ('SPAN',(0,3),(3,3)),
                            ('SPAN',(0,4),(3,4)),
                            ('SPAN',(0,4),(3,4)),
                            ('SPAN',(0,5),(0,7)),
                            ('SPAN',(2,5),(3,5)),
                            ('SPAN',(2,6),(3,6)),
                            ('SPAN',(2,7),(3,7)),
                            ('SPAN',(0,8),(1,9)),
                            ('SPAN',(0,10),(2,10)),
                            ('SPAN',(0,11),(3,11)),
                            ('SPAN',(0,12),(3,12)),
                            ('SPAN',(0,13),(3,13)),
                            ('SPAN',(0,14),(1,14)),
                            ('SPAN',(2,14),(3,14)),
                            ('SPAN',(0,15),(1,15)),
                            ('SPAN',(2,15),(3,15)),
                            ('SPAN',(0,16),(1,16)),
                            ('SPAN',(2,16),(3,16)),
                            ('SPAN',(0,17),(3,17)),
                            ('SPAN',(0,18),(3,18)),
                    ])
    #Configure style and word wrap
    s = getSampleStyleSheet()
    s = s["BodyText"]
    s.wordWrap = 'CJK'
    ps = ParagraphStyle('title', fontSize=15, alignment=TA_CENTER,)
    data2 = [[Paragraph(cell, s) for cell in row] for row in data]
    t=Table(data2)
    t.setStyle(style)
    elements.append(I)
    getSampleStyleSheet()
    elements.append(Paragraph("TRANSFER/DISCONTINUATION ACKNOWLEDGEMENT",ps))
    elements.append(Spacer(1,0.4*inch))
    #Send the data and build the file
    elements.append(t)
    doc.build(elements)
    return response

@user_login_required
@user_permission_required('transfer_certificate.can_view_transfer_certificate_export_tc', '/school/home/')
def ExportTC(request):
   from_Date_val = request.POST.get('from_Date')
   from_Date = request.POST.get('from_Date')
   from_Date = time.strftime("%Y-%m-%d")
   to_Date_val = request.POST.get('to_Date')
   to_Date = request.POST.get('to_Date')
   to_Date = time.strftime("%Y-%m-%d")


   if to_Date:

       date = datetime.strptime(to_Date,"%Y-%m-%d")
       last_date = monthrange(date.year, date.month)

       last_date = (last_date)[1]
       day = date.day

       mon = date.month
       if mon < 10:
           mon = '0' + str(mon)

       if int(date.day) == last_date:
           pass
       else:
           day = date.day + 1

       if day<10:
           day= '0'+str(day)


       year= str(date.year)


       final_to_date= str(year)+'-'+str(mon)+'-'+str(day)


       tc_recs = TcDetails.objects.filter(update_date__range=[from_Date, final_to_date],is_confirm=1)

   else:
       tc_recs = TcDetails.objects.filter(update_date__range=[from_Date, to_Date], is_confirm=1)

   # return render(request, "export_tc.html", {'tc_recs': tc_recs})
   return render(request, "export_tc.html", {'tc_recs': tc_recs, 'from_Date': from_Date_val, 'to_Date': to_Date_val})

       # return render(request, "export_tc.html" ,{})


@user_login_required
@user_permission_required('transfer_certificate.can_view_transfer_certificate_export_tc', '/school/home/')
def ExportTCRec(request):
   return render(request, "export_tc.html")

@user_login_required
@user_permission_required('transfer_certificate.can_view_transfer_certificate_export_tc', '/school/home/')
def ExportTransferCertificate(request):

    if request.method == 'POST':
        fromDate = request.POST.get('fromDate')
        to_Date = request.POST.get('toDate')

        date = datetime.strptime(to_Date, "%Y-%m-%d")
        last_date = monthrange(date.year, date.month)

        last_date = (last_date)[1]
        day = date.day

        mon = date.month
        if mon < 10:
            mon = '0' + str(mon)

        if int(date.day) == last_date:
            pass
        else:
            day = date.day + 1

        if day < 10:
            day = '0' + str(day)

        year = str(date.year)

        final_to_date = str(year) + '-' + str(mon) + '-' + str(day)

        tc_recs = TcDetails.objects.filter(update_date__range=[fromDate, final_to_date],is_confirm=1)

        column_names = ['Student Code','Student Name','Scodoo Id','Class','Section','Inside Dubai','Inside UAE','Leaving reason','New School Name']

        # rows = tc_recs.values_list('student__student_code', 'student__first_name',
        #                            'student__academic_class_section__class_name__class_name',
        #                            'student__academic_class_section__section_name__section_name', 'inside_dubai',
        #                            'inside_uae', 'leaving_reason', 'school_name')

        rows = []

        for student in tc_recs:
            temp_list = []

            temp_list.append(student.student.student_code)
            temp_list.append(student.student.get_all_name())
            temp_list.append(student.student.odoo_id)
            temp_list.append(student.student.academic_class_section.class_name.class_name)
            temp_list.append(student.student.academic_class_section.section_name.section_name)
            temp_list.append(student.inside_dubai)
            temp_list.append(student.inside_uae)
            temp_list.append(student.leaving_reason)
            temp_list.append(student.leaving_reason)
            temp_list.append(student.school_name)
            rows.append(temp_list)


        return export_users_xls('TCDetailList', column_names, rows)

@user_login_required
def StudentDetails(request, std_id):
    # student_detail_form = StudentDetailsForm()
    student_detail = student_details.objects.get(id=std_id)
    class_list = class_details.objects.all()
    section_list = sections.objects.all()
    parent_list = parents_details.objects.all()
    religion_list = religion.objects.all()
    nationality_list = nationality.objects.all()
    academicyr_list = academic_year.objects.all()

    student_detail.joining_date = student_detail.joining_date.strftime('%Y-%m-%d')
    student_detail.birth_date = student_detail.birth_date.strftime('%Y-%m-%d')
    medical_detail = None
    if student_medical.objects.filter(student=std_id):
        medical_detail = student_medical.objects.filter(student=std_id)[0]
    subject_recs = student_detail.subject_ids.all()
    mandatory_sub_list = []
    mandatory_sub_recs = mandatory_subject_mapping.objects.filter(
        academic_class_section_mapping_id=student_detail.academic_class_section.id)
    if mandatory_sub_recs:
        for mandatory_sub in mandatory_sub_recs:
            if mandatory_sub:
                mandatory_sub_list.append(mandatory_sub.subject)

    academic_class_section_mapping_classes = academic_class_section_mapping.objects.filter(
        year_name=student_detail.academic_class_section.year_name)
    class_obj_list = []
    section_obj_list = []
    raw_list = []
    for obj in academic_class_section_mapping_classes:
        raw_list.append(obj.class_name)
    class_obj_list = list(set(raw_list))

    raw_list = []
    for obj in academic_class_section_mapping.objects.filter(year_name=student_detail.academic_class_section.year_name,
                                                             class_name=student_detail.academic_class_section.class_name):
        raw_list.append(obj.section_name)
    section_obj_list = list(set(raw_list))

    if (student_detail.gender == 'False'):
        male = 'checked'
        female = ""
    else:
        female = "checked"
        male = ""
    return render(request, "student_details.html",
                  {'student_detail': student_detail, 'parent_list': parent_list, 'religion_list': religion_list,
                   'nationality_list': nationality_list, 'class_list': class_list, 'section_list': section_list,
                   'academicyr_list': academicyr_list, 'female': female, 'male': male,
                    'class_list': class_obj_list,
                   'section_list': section_obj_list, 'medical_rec': medical_detail, 'subject_details': subject_recs,
                   'mandatory_sub': mandatory_sub_list})

from __future__ import unicode_literals

from django.apps import AppConfig


class transfercertificateConfig(AppConfig):
    name = 'transfer_certificate'

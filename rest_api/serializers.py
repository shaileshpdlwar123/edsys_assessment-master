from rest_framework import routers, serializers, viewsets
from registration.models import academic_year, class_details, sections, parents_details, AuthUser, student_details, religion
from django.db import IntegrityError, transaction
from django.http import HttpResponseBadRequest, HttpResponseForbidden
import json

class AcademicYearSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = academic_year
        fields = ('year_code', 'year_name', 'start_date', 'end_date', 'current_academic_year')

class ClassDetailSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = class_details
        fields = ('class_code', 'class_name', 'create_date', 'update_date', 'next_class')

class SectionDetailSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = sections
        fields = ('section_code', 'section_name', 'create_date', 'update_date')


class AuthUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = AuthUser
        fields = ('first_name', 'middle_name', 'last_name', 'email')

    @transaction.atomic
    def create(self, validated_data):
        print (validated_data)

        save_point = transaction.savepoint()

        try:
            user = AuthUser.get_dict_instance(self.data, None, generate_password=False)
            user.save()
        except Exception as e:
            return False, str(e)

        try:
            parents_data = parents_details.get_dict_instance(validated_data.data, user)
            parents_data.save()
            transaction.savepoint_commit(save_point)
            return user, ''
        except Exception as e:
            transaction.savepoint_rollback(save_point)
            return False, str(e)

class ParentDetailSerializer(serializers.ModelSerializer):
    user = AuthUserSerializer(many=False)

    class Meta:
        model = parents_details
        fields = ('gender', 'street', 'birth_date', 'code', 'contact_num', 'city', 'state', 'postcode', 'user')

    @transaction.atomic
    def update(self, validated_data, parent_rec):
        print (validated_data)
        save_point = transaction.savepoint()

        try:
            user = AuthUser.get_dict_instance(validated_data['user'], parent_rec.user, generate_password=False)
            user.save()
        except Exception as e:
            return False, str(e)

        try:
            parents_data = parents_details.get_dict_instance(validated_data, user)
            parents_data.save()
            transaction.savepoint_commit(save_point)
            return user, ''
        except Exception as e:
            transaction.savepoint_rollback(save_point)
            return False, str(e)

class StudentDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = student_details
        fields = ('student_code', 'first_name', 'last_name', 'birth_date', 'gender', 'street', 'city', 'state', 'postcode', 'email', 'joining_date', 'nationality', 'religion')


class ReligionDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = religion
        fields = ('id', 'religion_name')
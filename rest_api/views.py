from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from registration.models import academic_year, class_details, sections, AuthUser, parents_details, student_details, religion
from .serializers import AcademicYearSerializer, ClassDetailSerializer, SectionDetailSerializer, ParentDetailSerializer, AuthUserSerializer, StudentDetailSerializer, ReligionDetailSerializer
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.authentication import TokenAuthentication, BasicAuthentication
from rest_framework.decorators import authentication_classes, permission_classes, api_view, renderer_classes
from registration.decorators import api_login_required, api_student_login_required
from django.http import HttpResponseBadRequest, HttpResponseForbidden
import json
import base64
from django.core.serializers.json import DjangoJSONEncoder
from django.forms.models import model_to_dict
from assessment.models import *
from masters.utils import *
from django.contrib.auth import authenticate


from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny, IsAuthenticated
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
class AcademicYearList(APIView):

    def get(self, request, format=None):
        # type: (object, object) -> object
        """
        Return a list of all Academic Year.
        """
        academic_year_rec = academic_year.objects.all()
        serializer = AcademicYearSerializer(academic_year_rec, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = AcademicYearSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
# @authentication_classes((TokenAuthentication, BasicAuthentication))
@api_login_required
def academic_year_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        academic_year_rec = academic_year.objects.all()
        serializer = AcademicYearSerializer(academic_year_rec, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = AcademicYearSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@api_login_required
def academic_year_detail(request, year_code):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        academic_year_rec = academic_year.objects.get(year_code=year_code)
    except academic_year.DoesNotExist as e:
        return Response(str(e), status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = AcademicYearSerializer(academic_year_rec)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = AcademicYearSerializer(academic_year_rec, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        academic_year_rec.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
# @authentication_classes((TokenAuthentication, BasicAuthentication))
@api_login_required
def create_or_get_class_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        class_rec = class_details.objects.all()
        serializer = ClassDetailSerializer(class_rec, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = ClassDetailSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@api_login_required
def update_or_get_class_detail(request, class_code):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        class_rec = class_details.objects.get(class_code=class_code)
    except class_details.DoesNotExist as e:
        return Response(str(e), status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ClassDetailSerializer(class_rec)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = ClassDetailSerializer(class_rec, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        class_rec.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
# @authentication_classes((TokenAuthentication, BasicAuthentication))
@api_login_required
def create_or_get_section_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        section_rec = sections.objects.all()
        serializer = SectionDetailSerializer(section_rec, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = SectionDetailSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@api_login_required
def update_or_get_section_detail(request, section_code):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        section_rec = sections.objects.get(section_code=section_code)
    except sections.DoesNotExist as e:
        return Response(str(e), status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = SectionDetailSerializer(section_rec)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = SectionDetailSerializer(section_rec, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        section_rec.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
# @authentication_classes((TokenAuthentication, BasicAuthentication))
@api_login_required
def create_or_get_parent_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        parent_rec = parents_details.objects.all()
        serializer = ParentDetailSerializer(parent_rec, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = ParentDetailSerializer(data=request.data)
        user_serializer = AuthUserSerializer(data=request.data['user'])
        if serializer.is_valid():
            if user_serializer.is_valid():
                created_user, message = user_serializer.create(serializer)

                if created_user == False:
                    return HttpResponseBadRequest(json.dumps({'error': message}), content_type="application/json")

            return Response(created_user.to_dict(), status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@api_login_required
def update_or_get_parent_detail(request, parent_code):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        parent_rec = parents_details.objects.get(code=parent_code)
    except parents_details.DoesNotExist as e:
        return Response(str(e), status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ParentDetailSerializer(parent_rec)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = ParentDetailSerializer(parent_rec, data=request.data)
        if serializer.is_valid():
            update_user, message = serializer.update(request.data, parent_rec)
            if update_user == False:
                return HttpResponseBadRequest(json.dumps({'error': message}), content_type="application/json")

            return Response(update_user.to_dict())
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        try:
            user = AuthUser.objects.get(id=parent_rec.user.id)
            user.delete()
            parent_rec.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Exception as e:
            return HttpResponseBadRequest(json.dumps({'error': str(e)}), content_type="application/json")


@api_view(['GET', 'POST'])
# @authentication_classes((TokenAuthentication, BasicAuthentication))
@api_login_required
def create_or_get_student_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        student_rec = student_details.objects.all()
        serializer = StudentDetailSerializer(student_rec, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = StudentDetailSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@api_login_required
def update_or_get_student_detail(request, student_code):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        student_rec = student_details.objects.get(student_code=student_code)
    except student_details.DoesNotExist as e:
        return Response(str(e), status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = StudentDetailSerializer(student_rec)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = StudentDetailSerializer(student_rec, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        student_rec.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
# @authentication_classes((TokenAuthentication, BasicAuthentication))
@api_login_required
def create_or_get_religon_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        religion_rec = religion.objects.all()
        serializer = ReligionDetailSerializer(religion_rec, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = ReligionDetailSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@api_login_required
def update_or_get_religon_detail(request, religion_id):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        religion_rec = religion.objects.get(pk=religion_id)
    except religion.DoesNotExist as e:
        return Response(str(e), status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ReligionDetailSerializer(religion_rec)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = ReligionDetailSerializer(religion_rec, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        religion_rec.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
@api_login_required
def get_student_card(request, student_id):
    try:
        from id_generation.views import PrintID
        response = (PrintID(request, student_id))
        test_str = response.content
        value = base64.b64encode(test_str)
        response_data = {}
        response_data['result'] = value
        return Response(response_data)
    except Exception as e:
        return Response({'error': 'error'}, status=status.HTTP_400_BAD_REQUEST)


from attendance.models import attendance_details
from registration.models import academic_year
import time


@api_view(['GET', 'POST'])
@api_login_required

def get_student_attendance(request):

    if request.method == 'GET':

        datalist = []
        val_dict_exp_att = json.loads(request.GET.dict().keys()[0])

        today_attendance = time.strftime("%Y-%m-%d")

        for rec in val_dict_exp_att:

            from_Date = rec['from_Date']
            class_name = rec['class_name']
            to_Date = rec['to_Date']
            section_name = rec['section_name']
            academic_years = rec['academic_year']
            date_val = rec['from_Date']
            to_date_val = rec['to_Date']


            if not from_Date:
                from_Date = time.strftime("%Y-%m-%d")
            if not to_Date:
                to_Date = time.strftime("%Y-%m-%d")
            section_rec = None
            class_rec = None
            user = request.user
            msg_flag = False

            acad_year = academic_year.objects.get(sync_id=academic_years)

            selected_class_id = class_name
            selected_class_name = class_name
            selected_section_id = section_name
            selected_section_name = section_name

            years = academic_year.objects.all()
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id

            mapped_class = []
            mapped_section = []
            if user.is_supervisor():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                    for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(supervisor_id=user.id):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)

            elif user.is_allowed_user():
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=current_academic_year):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
            else:
                if not user.is_system_admin():
                    for mapped_object in academic_class_section_mapping.objects.filter(
                            year_name=current_academic_year).filter(
                            Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)
                else:
                    for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)

            if not class_name == 'All' and not section_name == 'All':
                selected_class_id = class_details.objects.get(sync_id=class_name).id
                selected_class_name = class_details.objects.get(sync_id=class_name)
                selected_section_id = sections.objects.get(sync_id=section_name).id
                selected_section_name = sections.objects.get(sync_id=section_name)
                years = academic_year.objects.all()

                try:
                    current_academic_year = academic_year.objects.get(year_name=acad_year)
                    selected_year_id = current_academic_year.id

                    mapped_class = []
                    mapped_section = []
                    if user.is_supervisor():

                        for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                            for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                                    supervisor_id=user.id):
                                mapped_class.append(mapped_object.class_name)
                                mapped_section.append(mapped_object.section_name.to_dict())

                    elif user.is_allowed_user():
                        for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                            mapped_class.append(mapped_object.class_name)
                            if mapped_object:
                                for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                                    mapped_section.append(obj.section_name)

                    else:
                        if not user.is_system_admin():
                            for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year).filter(
                                    Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                                mapped_class.append(mapped_object.class_name)
                                mapped_section.append(mapped_object.section_name)
                        else:
                            for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                                mapped_class.append(mapped_object.class_name)
                                if mapped_object:
                                    for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                                        mapped_section.append(obj.section_name)
                except:
                    msg_flag = True
                    form_vals = {}
                    return form_vals, msg_flag


            if section_name == 'All' and class_name == 'All':
                AttendanceDetails = attendance_details.objects.filter(attendancedate__range=[from_Date, to_Date],
                                                                      student__is_active=True).order_by('student__first_name')


            elif class_name == 'All' and not section_name == 'All':
                selected_section_id = sections.objects.get(id=section_name).id
                selected_section_name = sections.objects.get(id=section_name)
                AttendanceDetails = attendance_details.objects.filter(attendancedate__range=[from_Date, to_Date],
                                                                      student__academic_class_section__section_name=section_name,
                                                                      student__is_active=True).order_by('student__first_name')

            elif not class_name == 'All' and section_name == 'All':
                selected_class_id = class_details.objects.get(id=class_name).id
                selected_class_name = class_details.objects.get(id=class_name)

                try:

                    current_academic_year = academic_year.objects.get(year_name=acad_year)
                    selected_year_id = current_academic_year.id

                    mapped_class = []
                    mapped_section = []
                    if user.is_supervisor():
                        for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                            for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                                    supervisor_id=user.id):
                                mapped_class.append(mapped_object.class_name.to_dict())
                                mapped_section.append(mapped_object.section_name.to_dict())

                    elif user.is_allowed_user():
                        for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                            mapped_class.append(mapped_object.class_name.to_dict())
                            if mapped_object:
                                for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                                    mapped_section.append(obj.section_name.to_dict())

                    else:
                        if not user.is_system_admin():
                            for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year).filter(
                                    Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                                mapped_class.append(mapped_object.class_name)
                                mapped_section.append(mapped_object.section_name)
                        else:
                            for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                                mapped_class.append(mapped_object.class_name)
                                if mapped_object:
                                    for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                                        mapped_section.append(obj.section_name)
                except:
                    msg_flag = True
                    form_vals = {}
                    return form_vals, msg_flag


                AttendanceDetails = attendance_details.objects.filter(attendancedate__range=[from_Date, to_Date],
                                                                      student__academic_class_section__class_name=class_name,
                                                                      student__is_active=True).order_by('student__first_name')


            else:

                year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                                    section_name=selected_section_id,
                                                                                    class_name=selected_class_id)

                filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj,
                                                                   is_active=True, joining_date__lte=todays_date())
                AttendanceDetails = attendance_details.objects.filter(student__id=filtered_students, attendancedate__range=[from_Date, to_Date]).order_by('student__first_name')



            student_list = []

            for stud in AttendanceDetails:

                student_list.append(stud.to_dict())

            datalist.extend(student_list)

            year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                                section_name=selected_section_id,
                                                                                class_name=selected_class_id)

            filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj,
                                                               is_active=True, joining_date__lte=todays_date())
            TodaysAttendanceDetails = attendance_details.objects.filter(student__id=filtered_students,
                                                                  attendancedate=today_attendance).order_by('student__first_name')
            student_list1 =[]
            for stud in TodaysAttendanceDetails:
                student_list1.append(stud.to_dict())


        form_vals = {'datalist': datalist,'student_list1':student_list1}

        return HttpResponse(json.dumps(form_vals), content_type="application/json")


@api_view(['GET', 'POST'])
@api_login_required
def get_report_card(request, student_id):
    if request.method == 'GET':
        val_dict_submission = request.GET
        year_name = val_dict_submission.get('year_name')
        reported_score = val_dict_submission.get('reported_score')
        subject_name = val_dict_submission.get('subject_name')
        section_name = val_dict_submission.get('section_name')
        class_name = val_dict_submission.get('class_name')
        class_rec = class_details.objects.get(id=class_name)
        section_rec = sections.objects.get(id=section_name)
        subject_rec = subjects.objects.get(id=subject_name)
        acd_year = academic_year.objects.get(id=year_name)
        round_master = assessment_round_master.objects.all()[0]
        model_to_dict(round_master)

        try:
            greading_scheme = \
            reported_exam_class_sub_mapping.objects.filter(year_id=year_name, class_name_id=class_name,
                                                           subject_id=subject_name,
                                                           reported_exam_mapp_id=reported_score)[0].grade_name.id
        except:
            form_vals = {}
            export_flag = 2

            return form_vals, export_flag

        greading_recs = grade_sub_details.objects.filter(grade_id=greading_scheme)
        grade_rec = grade_sub_details.objects.filter(grade_id=greading_scheme)[0].grade
        reported_score_recs = reported_score_details.objects.all()
        reported_mapped = reported_exam_class_sub_mapping.objects.filter(year=year_name, class_name=class_name,
                                                                         subject=subject_name).values_list(
            'reported_exam_mapp', flat=1).distinct()
        reported_score_recs = reported_score_details.objects.filter(id__in=reported_mapped)
        recs_is_saved = False
        export_flag = False

        academic_mapping = academic_class_section_mapping.objects.get(year_name_id=year_name, class_name_id=class_name,
                                                                      section_name_id=section_name)
        reported_score_obj = reported_score_details.objects.get(id=reported_score)
        if sz_reported_score_academic_mapping.objects.filter(academic_mapping=academic_mapping, subject=subject_rec,
                                                             reported_exam_mapp=reported_score_obj,
                                                             grade_scheme=grade_rec).exists():
            recs_is_saved = True

        col_full_arr = []
        for color in greading_recs:  # getting grades and creating its dict for the backend grade
            color_dic = {
                'mar_less': float(color.marks_greater),
                'mar_greater': float(color.marks_less),
                'grade': color.grades,
                'grade_obj': color
            }
            col_full_arr.append(color_dic)

        filtered_class_ids = reported_exam_class_sub_mapping.objects.filter(year=year_name).values_list('class_name',
                                                                                                        flat=1).distinct()
        class_list = class_details.objects.filter(id__in=filtered_class_ids)
        section_list = current_class_mapped_sections(acd_year, class_rec, request.user)
        reported_mapped_subjects = reported_exam_class_sub_mapping.objects.filter(year=year_name,
                                                                                  class_name=class_name).values_list(
            'subject', flat=1).distinct()
        subject_list = subjects.objects.filter(id__in=reported_mapped_subjects)
        reported_dict = {}
        reported_rec = {}
        grade_scheme_recs = []

        for weig_rec in reported_score_obj.reported_score_id.all():
            reported_dict[weig_rec.exam_name, weig_rec.frequency_name] = weig_rec.weightage
            reported_rec[weig_rec.exam_name, weig_rec.frequency_name] = weig_rec.id
        reported_mapping_recs = reported_exam_class_sub_mapping.objects.filter(reported_exam_mapp=reported_score_obj,
                                                                               subject_id=subject_name,
                                                                               class_name_id=class_name)

        student_recs = []
        for rec in reported_mapping_recs:
            if exam_scheme_acd_mapping_details.objects.filter(year=rec.year, class_obj=rec.class_name,
                                                              subject=rec.subject).exists():
                exam_mapping_parent_obj = exam_scheme_acd_mapping_details.objects.get(year=rec.year,
                                                                                      class_obj=rec.class_name,
                                                                                      subject=rec.subject)

                if exam_mapping_parent_obj.exam_name_mapping_ids.filter(exam_name=rec.exam_name,
                                                                        is_archived=False).exists():
                    exam_mapping_child_obj = exam_mapping_parent_obj.exam_name_mapping_ids.get(exam_name=rec.exam_name,
                                                                                               is_archived=False)

                    if exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping=academic_mapping,
                                                                             scheme_mapping=exam_mapping_child_obj,
                                                                             frequency=rec.frequency_name,
                                                                             is_submitted=True,
                                                                             is_archived=False).exists():

                        enter_mark_obj = \
                        exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping=academic_mapping,
                                                                              scheme_mapping=exam_mapping_child_obj,
                                                                              frequency=rec.frequency_name,
                                                                              is_submitted=True, is_archived=False)[0]

                        if enter_mark_obj.scheme_mapping.grade_name not in grade_scheme_recs:
                            grade_scheme_recs.append(enter_mark_obj.scheme_mapping.grade_name)

                        student_list = []
                        for student_rec in enter_mark_obj.student_mark_ids.all():
                            rep_obj = reported_rec[enter_mark_obj.scheme_mapping.exam_name, enter_mark_obj.frequency]
                            if student_rec.student.id == int(student_id):
                                rec_dict = {  # creating sub dict for static records
                                    'student': student_rec.student,
                                    'total_mark': student_rec.total_mark,
                                    'max_score': student_rec.max_mark,
                                    'freq': enter_mark_obj.frequency,
                                    'exam': enter_mark_obj.scheme_mapping.exam_name,
                                    'rep_obj': rep_obj,
                                }
                                student_list.append(rec_dict)
                            else:
                                pass

                        student_recs.append(student_list)

        final_list = []

        for data in range(len(student_recs)):
            for rec in range(len(student_recs[data])):
                final_list.append(student_recs[data][rec])

        final_dict = {}
        for i in final_list:
            if i["student"] not in final_dict:
                final_dict[i["student"]] = {'rec': [
                    {'max_score': i['max_score'], 'total_mark': i['total_mark'], 'freq': i['freq'], 'exam': i['exam'],
                     'rep_obj': i['rep_obj']}]}
            else:
                final_dict[i["student"]]['rec'].append(
                    {'max_score': i['max_score'], 'total_mark': i['total_mark'], 'freq': i['freq'], 'exam': i['exam'],
                     'rep_obj': i['rep_obj']})

        rec_list = []
        for i in final_dict:
            tot = 0
            max = 0
            weightage = 0.0
            grade = ''
            for rec in final_dict[i]["rec"]:
                if (rec['max_score'] == '' or rec['max_score'] == None):
                    rec['max_score'] = 0

                if (rec['total_mark'] == '' or rec['total_mark'] == None):
                    rec['total_mark'] = 0

                rep_percentage = reported_dict[rec['exam'], rec['freq']]
                tot = tot + float(rec['total_mark'])
                max = max + float(rec['max_score'])
                if float(rec['max_score']) != 0:  # getting weightage and applying the formula of grades
                    try:
                        if round_master.select:
                            weightage = weightage + float_round(((((float(float(rec['total_mark'])) / float(
                                rec['max_score'])) * 100) * float(rep_percentage)) / 100),
                                                                int(round_master.decimal_place),
                                                                ceil)
                        else:
                            weightage = weightage + float_round(((((float(float(rec['total_mark'])) / float(
                                rec['max_score'])) * 100) * float(rep_percentage)) / 100),
                                                                int(round_master.decimal_place),
                                                                floor)
                    except:
                        weightage = weightage + round((((float(float(rec['total_mark'])) / float(
                            rec['max_score'])) * 100) * float(rep_percentage)) / 100, 2)


                else:
                    weightage = weightage + 0.0

            for color_rec in col_full_arr:
                if color_rec['mar_less'] <= weightage <= color_rec[
                    'mar_greater']:  # getting grade of the specific weightage
                    grade = color_rec['grade_obj']

            ab = {
                'max': max,
                'total': tot,
                'stud': i,
                'data': final_dict[i]["rec"],
                'weightage': float(weightage),
                'grade': grade,

            }
            rec_list.append(ab)

        mark_detail_list = []
        all_rec = []
        for obj in rec_list:
            count = 0
            raw_dict = {}
            raw_dict['max'] = obj['max']
            raw_dict['total'] = obj['total']
            raw_dict['weightage'] = obj['weightage']
            raw_dict['first_name'] = obj['stud'].first_name
            raw_dict['class_name'] = class_rec.class_name
            raw_dict['section_name'] = section_rec.section_name
            raw_dict['subject_name'] = subject_rec.subject_name

            raw_dict['grade'] = obj['grade'].grades
            for rec in obj['data']:
                detail_dict = {}
                detail_dict['exam_name'] = rec['exam'].exam_name
                detail_dict['total_mark'] = rec['total_mark']
                detail_dict['max_score'] = rec['max_score']
                detail_dict['frequency_name'] = rec['freq'].frequency_name
                mark_detail_list.append(detail_dict)
                raw_dict['data'] = mark_detail_list

            raw_dict['reported_score_obj'] = [model_to_dict(obj) for obj in reported_score_obj.reported_score_id.all()]

            all_rec.append(raw_dict)

        subject_rec_list = []
        for obj in subject_list:
            raw_dict = {}
            raw_dict['id'] = obj.id
            raw_dict['subject_name'] = obj.subject_name
            subject_rec_list.append(raw_dict)

        subject_subject = []
        if subject_rec:
            raw_dict = {}
            raw_dict['id'] = subject_rec.id
            raw_dict['subject_name'] = subject_rec.subject_name
            subject_subject.append(raw_dict)

        selected_reported_score = []
        if reported_score_obj:
            raw_dict = {}
            raw_dict['id'] = reported_score_obj.id
            raw_dict['report_name'] = reported_score_obj.report_name
            selected_reported_score.append(raw_dict)

        form_vals = {
            'subject_rec': tuple(subject_subject),
            'reported_score_obj': tuple(selected_reported_score),
            'list': tuple(all_rec),
            'subject_list': tuple(subject_rec_list),

        }
        return Response(form_vals, status=status.HTTP_201_CREATED)
    return Response({'error': 'error'}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
@csrf_exempt
@authentication_classes((TokenAuthentication, BasicAuthentication))
@permission_classes((IsAuthenticated,))
@api_student_login_required
def get_assessment_details(request):
    rec_list = []
    try:
        student = student_details.objects.get(user__id=request.user.id)
        mapping_id = academic_class_section_mapping.objects.get(id=student.academic_class_section_id)
        perodic_assesment_mapping = perodic_question_academic_class_section_mapping.objects.filter(
            academic_class_section_mapping=mapping_id.id)
        for rec in perodic_assesment_mapping:
            rec_list.append(rec.to_dict())

        return Response((rec_list), status=status.HTTP_201_CREATED)
    except Exception as e:
        return Response({'error': 'error : ' + str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
@permission_classes((AllowAny,))
def login(request):
    try:
        val_dict = request.data
        username = val_dict['email']
        password = val_dict['password']

        user = authenticate(username=username, password=password)
        if user:
            student_obj = student_details.objects.get(user=user)
            token, _ = Token.objects.get_or_create(user=user)
            # return Response((student_obj.to_dict_brief(),{'token': token.key},), status=status.HTTP_201_CREATED)
            return Response({"status": True, 'details': student_obj.to_dict_brief()[0], "message": "success", },
                            status=status.HTTP_201_CREATED)
        else:
            return Response({"status": False, "error_code": 3, "message": "No User Found"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({'error': 'error : ' + str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def assignmentquestions(request):
    try:
        # val_dict = request.data
        val_dict = request.GET
        assignmentid = val_dict['assignmentid']
        # email = val_dict['email']
        details = []
        try:
            assesment_obj = assesment.objects.filter(id=assignmentid)
        except:
            return Response({"status": False, "error_code": 3, "message": "No Assignment Found"},
                            status=status.HTTP_400_BAD_REQUEST)
        if assesment_obj:
            for rec in assesment_obj:
                for obj in rec.assessment_question.all():
                    raw_dict = {}
                    raw_dict['id'] = rec.id
                    raw_dict['question_type'] = obj.question_type
                    raw_dict['assessment_id'] = rec.id
                    raw_dict['question_id'] = obj.id
                    raw_dict['position'] = obj.id
                    raw_dict['group_id'] = rec.id
                    raw_dict['assignment_code'] = rec.id
                    raw_dict['assignedby'] = rec.created_by
                    raw_dict['duedate'] = rec.assesment_date_time
                    raw_dict['title'] = rec.title
                    raw_dict['description'] = rec.description
                    raw_dict['durations'] = rec.duration
                    raw_dict['createdby'] = rec.created_by
                    raw_dict['originallycreatedby'] = rec.created_by
                    raw_dict['copy_status'] = rec.id
                    raw_dict['vark'] = rec.id
                    raw_dict['Archived'] = rec.id,
                    raw_dict['score'] = obj.mark
                    raw_dict['scheme_id'] = rec.id
                    raw_dict['strand_id'] = obj.perodic_question_strand.strand_name
                    raw_dict['scheme'] = rec.id
                    raw_dict['topic'] = obj.perodic_question_topic.topic
                    raw_dict['blooms_taxonomy'] = rec.id
                    raw_dict['keywords'] = rec.id
                    raw_dict['identifier'] = rec.id
                    raw_dict['image'] = rec.id

                    # if not obj.question_photo == '':
                    #     raw_dict['image'] = obj.question_photo
                    # else:
                    #     raw_dict['image'] = ''

                    raw_dict['xmlfile'] = rec.id
                    raw_dict['created_by'] = rec.id
                    raw_dict['question_createdby'] = rec.id
                    raw_dict['assessment_createdby'] = rec.created_by
                    raw_dict['assignment_assignedby'] = rec.id
                    raw_dict['assessment_question_id'] = rec.id
                    raw_dict['instruction'] = obj.instruction
                    raw_dict['question'] = obj.question_name
                    raw_dict['assessment_total_marks'] = rec.total_mark

                    if obj.question_type == "Match Order List":
                        raw_dict['question_list'] = [model_to_dict(i) for i in obj.question_list_option.all()]

                        # raw_dict['correct_response'] = [model_to_dict(i) for i in obj.ans.filter(correct_flag=True)]

                    if not obj.question_type == "TEXT ENTRY":
                        raw_dict['choices'] = [model_to_dict(i) for i in obj.ans.all()]
                    details.append(raw_dict)

            return Response({'status': True, 'details': details}, status=status.HTTP_201_CREATED)

        else:

            return Response({"status": False, "error_code": 3, "message": "No Assignment Found"},
                            status=status.HTTP_400_BAD_REQUEST)


    except Exception as e:
        return Response({'error': 'error : ' + str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def openassignments(request):
    try:
        # val_dict = request.data
        val_dict = request.GET
        email = val_dict['email']
        stud_list = []
        try:
            student = student_details.objects.get(user__email=email)

        except:
            return Response({"status": False, "error_code": 3, "message": "No User Found"},
                            status=status.HTTP_400_BAD_REQUEST)

        if student:
            mapping_id = academic_class_section_mapping.objects.get(id=student.academic_class_section_id)
            perodic_assesment_mapping = perodic_question_academic_class_section_mapping.objects.filter(
                academic_class_section_mapping=mapping_id.id)
            for rec in perodic_assesment_mapping:
                stud_list.append(rec.to_dict_openassignments(student))

            if perodic_assesment_mapping:
                return Response({"status": True, 'details': stud_list, "message": "success", },
                                status=status.HTTP_201_CREATED)
            else:
                return Response({"status": True, "details": "No open assignments", "message": "success"},
                                status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        return Response({'error': 'error : ' + str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def assignmentcode(request):
    try:
        val_dict = request.data
        email = val_dict['email']
        assignment_id = val_dict['assignment_id']
        assessment_code = val_dict['assignment_code']

        if assesment.objects.filter(assessment_code=assessment_code, id=assignment_id).exists():
            return Response({"status": True, "details": "Assignment Exist", "message": "success"},
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"status": False, "error_code": 3, "message": "Invalid Assignment Password"},
                            status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        return Response({'error': 'error : ' + str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def saveanswers(request):
    try:
        # val_dict = request.data
        # val_dict = request.GET
        val_dict = json.loads(request.GET.dict().keys()[0])

        # email = val_dict['email']
        student_id = val_dict['student_id']
        assessment_id = val_dict['assessment_id']
        question_id = val_dict['question_id']
        answers = val_dict['answer']

        student_obj = student_answer.objects.filter(student_id_id=student_id,particular_question_id=question_id,assessment_id=assessment_id)


        if student_obj:
            for rec in student_obj:
                for recs in rec.student_given_answer.all():
                    rec.student_given_answer.remove(recs.id)
                    answer_by_student.objects.get(id=recs.id).delete()

                for obj in answers:
                    ans_obj = answer.objects.get(id=obj['id'])
                    answer_by_student_obj = answer_by_student.objects.create(ans_by_student=obj['answer'],
                                                                             answer_id=ans_obj,
                                                                             postion_by_student=obj['postion'])
                    rec.student_given_answer.add(answer_by_student_obj.id)


        else:

            student_ans_obj = student_answer.objects.create(student_id_id=student_id,
                                                            particular_question_id=question_id,
                                                            assessment_id=assessment_id)


            for obj in answers:
                ans_obj = answer.objects.get(id=obj['id'])
                answer_by_student_obj = answer_by_student.objects.create(ans_by_student=obj['answer'],answer_id = ans_obj,
                                                                         postion_by_student=obj['postion'])
                student_ans_obj.student_given_answer.add(answer_by_student_obj.id)


        return Response({"status": True, "details": "Student Answer Save", "message": "success"},
                        status=status.HTTP_400_BAD_REQUEST)


    except Exception as e:
        return Response({'error': 'error : ' + str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def savegrade(request):
    try:
        val_dict = request.data
        email = val_dict['email']
        assignmentid = val_dict['assignmentid']
        assessment_id = val_dict['assessmentid']
        student_id = val_dict['studentid']
        total_grade = val_dict['total_grade']
        time_spent = val_dict['time_spent']
        student_grade.objects.create(time_spent=time_spent, student_id_id=student_id, assessment_id=assessment_id,
                                     grade=total_grade)

        return Response({"status": True, "details": "Student Grade Save", "message": "success"},
                        status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:

        return Response({'error': 'error : ' + str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def getgetstudentanswer(request):
    try:
        # val_dict = request.data
        val_dict = request.GET
        student_id = val_dict['student_id']
        assessment_id = val_dict['assessment_id']
        question_id = val_dict['question_id']

        obj = student_answer.objects.get(student_id_id=student_id,assessment_id=assessment_id,particular_question=question_id)

        # details=[]
        # if answer_obj:
        #
        #     for obj in answer_obj:
        raw_dict={}
        raw_dict['answer_data']=[model_to_dict(i) for i in obj.student_given_answer.all()]
        raw_dict['particular_question_id']=obj.particular_question_id
        raw_dict['question'] = obj.particular_question.question_name
        raw_dict['question_type'] = obj.particular_question.question_type
        raw_dict['instruction'] = obj.particular_question.instruction
        raw_dict['assessment_id'] = obj.assessment.id
                # raw_dict['question'] = obj.question_name
                # details.append(raw_dict)

        return Response({"status": True, "details": raw_dict, "message": "success"},
                        status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        return Response({'error': 'error : ' + str(e)}, status=status.HTTP_400_BAD_REQUEST)


from django.conf.urls import include, url
from django.contrib import admin
from . import views
from django.contrib.auth.views import logout
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns

app_name = 'rest_api'

urlpatterns = [
                # url(r'^academic_year_list/', views.AcademicYearList.as_view()),
                url(r'^academic_year_list/$', views.academic_year_list),
                url(r'^academic_year_detail/(?P<year_code>.+)$', views.academic_year_detail),

                url(r'^create_or_get_class_list/$', views.create_or_get_class_list),
                url(r'^update_or_get_class_detail/(?P<class_code>.+)$', views.update_or_get_class_detail),

                url(r'^create_or_get_section_list/$', views.create_or_get_section_list),
                url(r'^update_or_get_section_detail/(?P<section_code>.+)$', views.update_or_get_section_detail),

                url(r'^create_or_get_parent_list/$', views.create_or_get_parent_list),
                url(r'^update_or_get_parent_detail/(?P<parent_code>.+)$', views.update_or_get_parent_detail),

                url(r'^create_or_get_student_list/$', views.create_or_get_student_list),
                url(r'^update_or_get_student_detail/(?P<student_code>.+)$', views.update_or_get_student_detail),

                url(r'^create_or_get_religon_list/$', views.create_or_get_religon_list),
                url(r'^update_or_get_religon_detail/(?P<religion_id>.+)$', views.update_or_get_religon_detail),
                url(r'^get_report_card/(?P<student_id>.+)$', views.get_report_card),
                url(r'^get_student_card/(?P<student_id>.+)$', views.get_student_card),


                url(r'^get_assessment_details/$', views.get_assessment_details),
                url(r'^login/$', views.login),
                url(r'^assignmentquestions/$', views.assignmentquestions),
                url(r'^openassignments/$', views.openassignments),
                url(r'^assignmentcode/$', views.assignmentcode),
                url(r'^saveanswers/$', views.saveanswers),
                url(r'^savegrade/$', views.savegrade),

                # url(r'^get_student_attendance/(?P<student_id>.+)$', views.get_student_attendance),
                url(r'^get_student_attendance/$', views.get_student_attendance),


                url(r'^getgetstudentanswer/$', views.getgetstudentanswer),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = format_suffix_patterns(urlpatterns)



from django.conf.urls import include, url
from django.contrib import admin
from . import views
from django.contrib.auth.views import logout
from django.conf import settings
from django.conf.urls.static import static, serve

app_name = 'attendance'


urlpatterns = [
          url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
          url(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT}),

          url(r'^attendance_view/$', views.AttendanceView, name='attendance_view'),
          url(r'^manage_attendance/$', views.AttendanceManage, name='manage_attendance'),
          url(r'^attendanceDetails/$', views.AttendanceDetails, name='attendanceDetails'),
          url(r'^attendanceEdit/$', views.AttendanceEdit, name='attendanceEdit'),
          url(r'^viewAttendanceDetails/$', views.ViewAttendanceDetails, name='viewAttendanceDetails'),
          url(r'^viewAttendanceEdit/$', views.ViewAttendanceEdit, name='viewAttendanceEdit'),
          url(r'^ViewSectionDetailAttendance/$', views.ViewSectionDetailAttendance,
              name='ViewSectionDetailAttendance'),
          # url(r'^ViewSectionDetails/$', views.ViewSectionDetails, name='ViewSectionDetails'),
          #
          url(r'^saveAttendance/$', views.SaveAttendance, name='saveAttendance'),
          #url(r'^(?P<saveAttendance>[\w-]+)/$', views.SaveAttendance, name='saveAttendance'),
          # url(r'^absentCategory/$', views.SaveAttendance, name='saveAttendance'),
          url(r'^attendanceCategories/$', views.AttendanceCategories, name='attendanceCategories'),
          #
          url(r'^add_absent_categories/$', views.AddAbsentCategories, name='add_absent_categories'),
          url(r'^category_detail/$', views.CategoryDetail, name='category_detail'),
          # url(r'^attendanceCategories/$', views.AttendanceCategories, name='attendanceCategories'),
          url(r'^save_updated_category/$', views.SaveUpdatedCategory, name='save_updated_category'),
          url(r'^update_category/(?P<cateory_id>[0-9]+)$', views.UpdateCategory, name='update_category'),
          url(r'^delete_absent_category/(?P<category_id>[0-9]+)$', views.DeleteAbsentCategory,
              name='delete_absent_category'),
          url(r'^updateAttendance/$', views.UpdateAttendance, name='updateAttendance'),
          url(r'^attendanceDashboard/$', views.AttendanceDashboard, name='attendanceDashboard'),
          url(r'^studentDetailDashboard/$', views.StudentDetailDashboard, name='studentDetailDashboard'),
          url(r'^ViewAttendanceTrend/$', views.ViewAttendanceTrend, name='ViewAttendanceTrend'),
          url(r'^lateStudentList/$', views.LateStudentList, name='lateStudentList'),
          url(r'^lateStudentDetail/$', views.LateStudentDetails, name='lateStudentDetail'),
          url(r'^regularAbsenteesList/$', views.RegularAbsenteesList, name='regularAbsenteesList'),
          url(r'^regularAbsenteesDetail/$', views.RegularAbsenteesDetail, name='regularAbsenteesDetail'),
          url(r'^attendancePercentage/$', views.AttendancePercentage, name='attendancePercentage'),
          #
          url(r'^studentAttendancePercentage/$', views.StudentAttendancePercentage,name='studentAttendancePercentage'),
          url(r'^importStudentAttendance/$', views.ImportStudentAttendance, name='importStudentAttendance'),
          url(r'^saveImportStudentAttendance/$', views.SaveImportStudentAttendance,
              name='saveImportStudentAttendance'),
          url(r'^exportStudentAttendance/$', views.ExportStudentAttendance, name='exportStudentAttendance'),
          #
          # url(r'^ViewSectionDetailsAttendance/$', views.ViewSectionDetailsAttendance,
          #     name='ViewSectionDetailsAttendance'),
          url(r'^manageBulkAttendance/$', views.ManageBulkAttendance, name='manageBulkAttendance'),
          url(r'^viewBulkAttendance/$', views.ViewBulkAttendance, name='viewBulkAttendance'),
          url(r'^updateBulkAttendance/$', views.UpdateBulkAttendance, name='updateBulkAttendance'),
          url(r'^exportBulkAttendance/$', views.ExportBulkAttendance, name='exportBulkAttendance'),
          url(r'^getCurrentAcademicYear/$', views.GetCurrentAcademicYear, name='getCurrentAcademicYear'),

          url(r'^present_category_detail/$', views.PresentCategoryDetail, name='present_category_detail'),
          url(r'^add_present_categories/$', views.AddPresentCategories, name='add_present_categories'),
          url(r'^save_present_categories/$', views.SavePresentCategories, name='save_present_categories'),
          url(r'^update_present_category/(?P<present_cateory_id>[0-9]+)$', views.UpdatePresentCategory, name='update_present_category'),
          url(r'^save_present_updated_category/$', views.SavePresentUpdatedCategory, name='save_present_updated_category'),
          url(r'^delete_present_category/(?P<category_id>[0-9]+)$', views.DeletePresentCategory,
              name='delete_present_category'),
          url(r'^export_student_attendance_list/$', views.ExportStudentAttendanceList, name='export_student_attendance_list'),

          url(r'^getCategoryColor/$', views.GetCategoryColor, name='getCategoryColor'),
          url(r'^dailyAttendanceReport/$', views.DailyAttendanceReport, name='dailyAttendanceReport'),
          url(r'^export_excel_daily_attendance_report/$', views.ExportExcelDailyAttendanceReport, name='export_excel_daily_attendance_report'),
          url(r'^daily_attendance_report_pdf/$', views.DailyAttendanceReportPDF, name='daily_attendance_report_pdf'),
          url(r'^daily_attendance_report_log/$', views.DailyAttendanceReportLog, name='daily_attendance_report_log'),
          url(r'^update_absent_check_value/$', views.UpdateAbsentCheckValue, name='update_absent_check_value'),

      ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



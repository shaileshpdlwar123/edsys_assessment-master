from django.contrib import admin
from .models import attendance_details, absent_categories, present_categories

# Register your models here.
admin.site.register(absent_categories)
admin.site.register(attendance_details)
admin.site.register(present_categories)
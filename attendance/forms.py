from django import forms
from registration.models import *
from attendance.models import *
from medical.models import *




# class AttendanceClassSectionForm(forms.ModelForm):
#  # ClassDetail = ClassDetails.objects.all()
#  # Section = Sections.objects.all()
#  # class_name = forms.ModelChoiceField(queryset = ClassDetail ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
#  # section_name = forms.ModelChoiceField(queryset = Section ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
#
 
class AbsentReasonForm(forms.ModelForm):
    absent_category_code = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    absent_category_name = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    color_code = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','type': 'color'}))
    short_code = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    daily_attendance=forms.BooleanField(required=False,widget=forms.CheckboxInput(attrs={'class':'checkbox'}))
    class Meta:
        model =absent_categories
        fields = ['absent_category_name','absent_category_code','color_code', 'short_code','daily_attendance']
        
        
# class AttendanceExcelForm(forms.ModelForm):
#
#     attendance_excel_path = forms.FileField(required=False,label='Upload Photo' )
#     class Meta:
#         # model = Attendanceexcel
#         fields =['attendance_excel_path']


class AttendanceExcelForm(forms.ModelForm):
    attendance_excel = forms.FileField(required=True, label='Upload file')

    class Meta:
        model = attendance_details_excel
        fields = ['attendance_excel']


class ClassSection(forms.ModelForm):
    SectionDetail = sections.objects.all()
    ClassDetail = class_details.objects.all()

    class_name = forms.ModelChoiceField(required=True, queryset=ClassDetail,
                                        widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    section_name = forms.ModelChoiceField(required=True, queryset=SectionDetail,
                                          widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))

    class Meta:
        model = student_medical
        fields = ['class_name', 'section_name']

class AcademicYearClassSection(forms.ModelForm):
    SectionDetail = sections.objects.all()
    ClassDetail = class_details.objects.all()
    AcademicYearDetail = academic_year.objects.all()

    year_name = forms.ModelChoiceField(required=True, queryset=AcademicYearDetail,
                                          widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))

    class_name = forms.ModelChoiceField(required=True, queryset=ClassDetail,
                                        widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    section_name = forms.ModelChoiceField(required=True, queryset=SectionDetail,
                                          widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))

    class Meta:
        model = academic_class_section_mapping
        fields = ['year_name', 'class_name', 'section_name']

    # def __init__(self, *args, **kwargs):
    #     super(AcademicYearClassSection, self).__init__(*args, **kwargs)
    #     self.fields['section_name'].empty_label = 'Please Select'


class DashboardAcademicYearClassSection(forms.ModelForm):
    SectionDetail = sections.objects.all()
    ClassDetail = class_details.objects.all()
    AcademicYearDetail = academic_year.objects.all()

    year_name = forms.ModelChoiceField(required=True, queryset=AcademicYearDetail,
                                          widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))

    dashboard_class_name = forms.ModelChoiceField(required=True, queryset=ClassDetail,
                                        widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    dashboard_section_name = forms.ModelChoiceField(required=True, queryset=SectionDetail,
                                          widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))

    class Meta:
        model = academic_class_section_mapping
        fields = ['year_name', 'dashboard_class_name', 'dashboard_section_name']


class PresentReasonForm(forms.ModelForm):
    present_category_code = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off'}))
    present_category_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off'}))
    color_code = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off','type': 'color'}))
    short_code = forms.CharField(required=True,
                                 widget=forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    class Meta:
        model = present_categories
        fields = ['present_category_name', 'present_category_code', 'color_code', 'short_code']

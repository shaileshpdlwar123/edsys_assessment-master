from __future__ import division
from django.shortcuts import render
from django.shortcuts import render,redirect
from attendance.models import *
from attendance import forms
from .forms import *
from registration.forms import *
from attendance.forms import AbsentReasonForm, ClassSection, AttendanceExcelForm, AcademicYearClassSection
import datetime
import json
import ast
from .models import *
from datetime import date
import pandas as pd
from django.contrib import messages
from django.forms.models import model_to_dict, inlineformset_factory
from django.http import JsonResponse
import time
import django_excel as excel
from registration.decorators import user_group_permission_required, user_permission_required, user_login_required
from django.db.models import Q
from calendar import monthrange
from masters.utils import export_users_xls, todays_date, current_acd_month_rec,get_all_user_permissions,mapped_user_classes,current_class_mapped_sections, current_academic_year_mapped_classes, export_users_with_time_xls
# from datetime import datetime
from io import BytesIO
import xlsxwriter
from django.db.models import FloatField
from django.db.models.functions import Cast


# Create your views here.
@user_login_required
@user_permission_required('attendance.can_view_attendance', '/school/home/')
def AttendanceView(request):
    perms = [] #get_all_user_permissions(request)
    return render(request, "attendance_view.html",{'perms':list(perms)})

@user_login_required
@user_permission_required('attendance.can_view_manage_attendance', '/school/home/')
def AttendanceManage(request):
    return render(request, "manage_attendance.html")

@user_login_required
@user_permission_required('attendance.can_view_attendance_add', '/school/home/')
def AttendanceDetails(request):
    AbsentForm = AbsentReasonForm()
    AttendanceClassSection = AcademicYearClassSection()
    Year = academic_year.objects.all()
    AbsentReason1 = absent_categories.objects.all()

    user = request.user

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

        mapped_class = []
        mapped_section = []
        if user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(supervisor_id=user.id):
                    mapped_class.append(mapped_object.class_name)
                    # mapped_section.append(mapped_object.section_name)
        elif user.is_allowed_user():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                mapped_class.append(mapped_object.class_name)
        else:
            if not user.is_system_admin():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year).filter(Q(assistant_teacher = user.id) | Q(staff_id = user.id)):
                    mapped_class.append(mapped_object.class_name)
                    # mapped_section.append(mapped_object.section_name)
            else:
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                    mapped_class.append(mapped_object.class_name)
                    # mapped_section.append(mapped_object.section_name)

    except:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        return redirect('/school/manage_attendance/')

    return render(request, "attendance_details.html",
                  {'AbsentForm': AbsentForm, 'SectionDetails': list(set(mapped_section)), 'ClassSection': AttendanceClassSection,
                   'ClassDetails': list(set(mapped_class)), 'AbsentReason': AbsentReason1, 'flag': 'false',
                   'academic_year': Year, 'selected_year_id': selected_year_id,
                   'selected_year_name': current_academic_year})

@user_login_required
@user_permission_required('attendance.can_view_attendance_edit', '/school/home/')
def AttendanceEdit(request):
    AbsentForm = AbsentReasonForm()
    AttendanceClassSection = AcademicYearClassSection()
    AbsentReason1 = absent_categories.objects.all()
    Year = academic_year.objects.all()
    user = request.user

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

        mapped_class = []
        mapped_section = []
        if user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                        supervisor_id=user.id):
                    mapped_class.append(mapped_object.class_name)
                    # mapped_section.append(mapped_object.section_name)
        elif user.is_allowed_user():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                mapped_class.append(mapped_object.class_name)
        else:
            if not user.is_system_admin():
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=current_academic_year).filter(Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                    mapped_class.append(mapped_object.class_name)
                    # mapped_section.append(mapped_object.section_name)
            else:
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                    mapped_class.append(mapped_object.class_name)
                    # mapped_section.append(mapped_object.section_name)
    except:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        return redirect('/school/manage_attendance/')

    return render(request, "attendance_edit.html",
                  {'AbsentForm': AbsentForm, 'SectionDetails': list(set(mapped_section)), 'ClassSection': AttendanceClassSection,
                   'ClassDetails': list(set(mapped_class)), 'AbsentReason': AbsentReason1, 'selected_year_id': selected_year_id,
                       'selected_year_name': current_academic_year, 'academic_year': Year})

@user_login_required
@user_permission_required('attendance.can_view_view_absent_categories', '/school/home/')
def CategoryDetail(request):
    categorylist = absent_categories.objects.all()
    return render(request, "absent_categories_details.html", {'categorylist': categorylist})

@user_login_required
@user_permission_required('attendance.can_view_add_absent_categories', '/school/home/')
def AddAbsentCategories(request):
    AbsentreasonForm = AbsentReasonForm()
    return render(request, "add_absentreason.html" ,{'AbsentreasonForm':AbsentreasonForm})

@user_login_required
@user_permission_required('attendance.add_absent_categories', '/school/home/')
def AttendanceCategories(request):
    if request.method == 'POST':
        absent_reason = AbsentReasonForm(request.POST)
        #         print "form",AbsentReason
        if absent_reason.is_valid():
            if absent_categories.objects.filter(absent_category_name=request.POST['absent_category_name']).exists() or absent_categories.objects.filter(absent_category_code= request.POST['absent_category_code']).exists():
                messages.success(request, "Duplicate record can't be added.")
            else:
                absent_categories.objects.create(absent_category_name=absent_reason.cleaned_data['absent_category_name'].lower(),
                                                 absent_category_code=absent_reason.cleaned_data['absent_category_code'].lower(),
                                                 color_code=absent_reason.cleaned_data['color_code'],
                                                 short_code=absent_reason.cleaned_data['short_code'],daily_attendance=absent_reason.cleaned_data['daily_attendance'])

    return redirect('/school/category_detail/')

@user_login_required
@user_permission_required('attendance.can_view_edit_absent_categories', '/school/home/')
def UpdateCategory(request, cateory_id):
    categorylist = absent_categories.objects.get(pk=cateory_id)
    return render(request, "update_absent_category.html", {'categorylist': categorylist})

@user_login_required
@user_permission_required('attendance.change_absent_categories', '/school/home/')
def SaveUpdatedCategory(request):
    #     print 'update'
    cateory_id = request.POST['category_id']
    category_obj = absent_categories.objects.get(id=cateory_id)

    if (request.method == 'POST'):
        #         print "with in post"
        CategoryDetail = AbsentReasonForm(request.POST)
    # print ("errors",CategoryDetail.errors)

    if CategoryDetail.is_valid():
        #         print ("form is valid")

        if absent_categories.objects.filter(~Q(id = cateory_id),absent_category_name = request.POST['absent_category_name']).exists():
            messages.success(request, "Duplicate record can't be added.")

        elif absent_categories.objects.filter(~Q(id = cateory_id),absent_category_code = request.POST['absent_category_code']).exists():
            messages.success(request, "Duplicate record can't be added.")

        # if absent_categories.objects.filter(
        #         absent_category_name=request.POST['absent_category_name']).exists() or absent_categories.objects.filter(
        #         absent_category_code=request.POST['absent_category_code']).exists():
        #     messages.success(request, "Duplicate record can't be added.")
        else:
            reason_id = request.POST['category_id']
            obj = absent_categories.objects.get(pk=reason_id)
            obj.absent_category_name = (request.POST['absent_category_code']).lower()
            obj.absent_category_name = (request.POST['absent_category_name']).lower()
            obj.color_code = request.POST['color_code']
            obj.short_code = request.POST['short_code']
            obj.daily_attendance = CategoryDetail.cleaned_data['daily_attendance']
            obj.save()
            messages.success(request, "One record updated successfully")

    return redirect('/school/category_detail/')

@user_login_required
@user_permission_required('attendance.delete_absent_categories', '/school/home/')
def DeleteAbsentCategory(request, category_id):
    try:
        absent_categories.objects.filter(id=category_id).delete()
        messages.success(request, "Record deleted successfully")
    except:
        messages.success(request, "Cant delete absent category relation exists somewhere.")

    return redirect('/school/category_detail/')

from django.utils.dateparse import parse_date
# @user_login_required
# def ViewAttendanceDetails(request):
#     AbsentForm = AbsentReasonForm()
#     AbsentReason1 = absent_categories.objects.all()
#     PresentReason1 = present_categories.objects.all()
#
#     AttendanceClassSection = AcademicYearClassSection()
#     modelDict = []
#     modelDict1 = []
#
#     user = request.user
#
#     class_name = request.POST.get('class_name')
#     section_name = request.POST.get('section_name')
#     academic_years = request.POST.get('year_name')
#     date = request.POST.get('date')
#     todayDate = time.strftime("%Y-%m-%d")
#
#     className = class_details.objects.get(id=class_name)
#     sectionName = sections.objects.get(id=section_name)
#
#     flag = 'false'
#     years = academic_year.objects.all()
#
#     try:
#         current_academic_year = academic_year.objects.get(id=academic_years)
#         selected_year_id = current_academic_year.id
#
#         mapped_class = []
#         mapped_section = []
#         if user.is_supervisor():
#             for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
#                 for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
#                         supervisor_id=user.id):
#                     mapped_class.append(mapped_object.class_name)
#                     mapped_section.append(mapped_object.section_name)
#         else:
#             if not user.is_system_admin():
#                 for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years).filter(
#                                 Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
#                     mapped_class.append(mapped_object.class_name)
#                     mapped_section.append(mapped_object.section_name)
#             else:
#                 for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
#                     mapped_class.append(mapped_object.class_name)
#                     if mapped_object:
#                         for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
#                             mapped_section.append(obj.section_name)
#
#     except:
#         messages.warning(request, "Academic Year Class Section Mapping Not Found.")
#         return redirect('/school/manage_attendance/')
#
#     if date == todayDate:
#         students = []
#
#         year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years, section_name=section_name, class_name=class_name)
#         students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True, joining_date__lte=todays_date())
#         for obj in students:
#            modelDict.append(obj)
#
#         if attendance_details.objects.filter(attendancedate=date, student__in=students, student__academic_class_section__class_name=class_name, student__academic_class_section__section_name=section_name):
#             # messages.success(request, "Attendance allready taken today.")
#             flag = 'true'
#
#             present = attendance_details.objects.filter(student__in=students,
#                                                         attendancedate=todayDate,
#                                                         status='Present').count()
#
#             absent = attendance_details.objects.filter(student__in=students,
#                                                         attendancedate=todayDate,
#                                                         status='Absent').count()
#
#             late = attendance_details.objects.filter(student__in=students,
#                                                      attendancedate=todayDate,
#                                                      present_category__present_category_name='Late').count()
#
#             medical = attendance_details.objects.filter(student__in=students,
#                                                         attendancedate=todayDate,
#                                                         absent_category__absent_category_name='Medical').count()
#
#             exclused = attendance_details.objects.filter(student__in=students,
#                                                          attendancedate=todayDate,
#                                                          absent_category__absent_category_name='Excused').count()
#
#             total_attendance_taken = present + absent
#
#             attendance_summary = []
#             raw_data = {}
#             raw_data['status'] = ['Present', 'Absent', 'Late', 'Medical', 'Excused']
#             raw_data['status_count'] = [present, absent, late, medical, exclused]
#             raw_data['total_attendance_taken'] = total_attendance_taken
#             attendance_summary.append(raw_data)
#
#             return render(request, "attendance_details.html",
#                           {'AbsentForm': AbsentForm, 'attendance_summary': attendance_summary,
#                            'section_name': section_name, 'SectionDetails':  list(set(mapped_section)), 'ClassSection': AttendanceClassSection,
#                            'class_name': class_name, 'date': date, 'ClassDetails':  list(set(mapped_class)), 'flag': flag, 'selected_year_id': selected_year_id,
#                        'selected_year_name': current_academic_year, 'academic_year': years, 'selected_class': className, 'selected_section': sectionName})
#         if not students:
#             messages.warning(request, "No student is present in the selected Class-Section combination.")
#     return render(request, "attendance_details.html",
#                   {'AbsentForm': AbsentForm, 'AbsentReason': AbsentReason1,'PresentReason': PresentReason1,
#                    'section_name': section_name, 'SectionDetails':  list(set(mapped_section)), 'attendanceList': modelDict1,
#                    'studentList': modelDict, 'ClassSection': AttendanceClassSection,
#                    'class_name': class_name, 'date': date, 'ClassDetails':  list(set(mapped_class)), 'flag': flag,  'selected_year_id': selected_year_id,
#                        'selected_year_name': current_academic_year, 'academic_year': years, 'selected_class': className, 'selected_section': sectionName})

def load_export_view_attendance_filter(request, val_dict_view_att):

    AbsentForm = AbsentReasonForm()
    AbsentReason1 = absent_categories.objects.all()
    PresentReason1 = present_categories.objects.all()

    AttendanceClassSection = AcademicYearClassSection()
    modelDict = []
    modelDict1 = []

    user = request.user

    class_name = val_dict_view_att.get('class_name')
    section_name = val_dict_view_att.get('section_name')
    academic_years = val_dict_view_att.get('year_name')
    date_val = val_dict_view_att.get('date')
    date = val_dict_view_att.get('date')
    date = time.strftime("%Y-%m-%d")
    todayDate = time.strftime("%Y-%m-%d")

    className = class_details.objects.get(id=class_name)
    sectionName = sections.objects.get(id=section_name)

    flag = 'false'
    msg_flag = False
    years = academic_year.objects.all()

    try:
        current_academic_year = academic_year.objects.get(id=academic_years)
        selected_year_id = current_academic_year.id

        mapped_class = []
        mapped_section = []
        if user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                        supervisor_id=user.id):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
        elif user.is_allowed_user():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
                mapped_class.append(mapped_object.class_name)
                if mapped_object:
                    for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                        mapped_section.append(obj.section_name)
        else:
            if not user.is_system_admin():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years).filter(
                                Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
            else:
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
                    mapped_class.append(mapped_object.class_name)
                    if mapped_object:
                        for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                            mapped_section.append(obj.section_name)

    except:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        msg_flag = True
        form_vals = {}
        return form_vals, msg_flag

    if date == todayDate:
        students = []

        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
                                                                            section_name=section_name,
                                                                            class_name=class_name)
        students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True,
                                                  joining_date__lte=todays_date()).order_by('first_name')
        if not students:
            form_vals = {'AbsentForm': AbsentForm,
                         'section_name': section_name, 'SectionDetails': list(set(mapped_section)),
                         'ClassSection': AttendanceClassSection,
                         'class_name': class_name, 'date': date_val, 'ClassDetails': list(set(mapped_class)),
                         'flag': flag, 'selected_year_id': selected_year_id,
                         'selected_year_name': current_academic_year, 'academic_year': years,
                         'selected_class': className, 'selected_section': sectionName}
            messages.warning(request, "No student is available in the selected Class-Section combination.")
            return form_vals, msg_flag

        for obj in students:
            if not obj.student_attendance_relation.all().filter(attendancedate=todays_date()).exists():
                modelDict.append(obj)

        if len(modelDict) == 0:

            flag = 'true'

            present = attendance_details.objects.filter(student__in=students,
                                                        attendancedate=todayDate,
                                                        status='Present',present_category=None).count()

            absent = attendance_details.objects.filter(student__in=students,
                                                       attendancedate=todayDate,
                                                       status='Absent',absent_category=None).count()

            abs_categories_rec = absent_categories.objects.all()
            pre_categories_rec = present_categories.objects.all()

            categories = []
            present_categories_name = []
            absent_categories_name = []

            categories.append('Present')
            categories.append('Absent')

            for rec in abs_categories_rec:
                absent_categories_name.append(rec.absent_category_name.title())

            for rec in pre_categories_rec:
                present_categories_name.append(rec.present_category_name.title())

            categories_count = []
            present_categories_count = []
            absent_categories_count = []

            categories_count.append(present)
            categories_count.append(absent)

            for obj in present_categories_name:

                if present_categories.objects.filter(present_category_name=obj.lower()).exists():
                    temp1 = attendance_details.objects.filter(student__in=students,
                                                  attendancedate=todayDate,
                                                  present_category__present_category_name=obj.lower()).count()
                    present_categories_count.append(temp1)

            for obj in absent_categories_name:

                if absent_categories.objects.filter(absent_category_name=obj.lower()).exists():
                    print obj
                    temp1 = attendance_details.objects.filter(student__in=students,
                                                          attendancedate=todayDate,
                                                          absent_category__absent_category_name=obj.lower()).count()
                    absent_categories_count.append(temp1)

            total_attendance_taken = present + absent

            attendance_summary = []

            total_att_count = 0
            count = 0
            for obj in categories:
                temp_data = []
                raw_data = {}
                raw_data['status'] = obj
                raw_data['status_count'] = categories_count[count]
                raw_data['total_attendance_taken'] = total_attendance_taken

                total_att_count += int(categories_count[count])

                raw_data_one = {}
                if count == 0:
                    present_count = 0
                    for data in present_categories_name:
                        raw_data_one[data] = present_categories_count[present_count]
                        # raw_data_one['status_count'] = present_categories_count
                        total_att_count += int(present_categories_count[present_count])
                        present_count = present_count + 1
                    temp_data.append(raw_data_one)

                if count == 1:
                    absent_count = 0
                    for data1 in absent_categories_name:
                        raw_data_one[data1] = absent_categories_count[absent_count]
                        total_att_count += int(absent_categories_count[absent_count])
                        # raw_data_one['status_count'] = absent_categories_count
                        absent_count = absent_count + 1
                    temp_data.append(raw_data_one)

                count = count + 1

                raw_data['sub_categories'] = temp_data
                attendance_summary.append(raw_data)


            form_vals = {'AbsentForm': AbsentForm, 'attendance_summary': attendance_summary,'total_att_count':total_att_count,
                           'section_name': section_name, 'SectionDetails': list(set(mapped_section)),
                           'ClassSection': AttendanceClassSection,
                           'class_name': class_name, 'date': date_val, 'ClassDetails': list(set(mapped_class)),
                           'flag': flag, 'selected_year_id': selected_year_id,
                           'selected_year_name': current_academic_year, 'academic_year': years,
                           'selected_class': className, 'selected_section': sectionName}
            return form_vals, msg_flag

    form_vals = {'AbsentForm': AbsentForm, 'AbsentReason': AbsentReason1, 'PresentReason': PresentReason1,
                   'section_name': section_name, 'SectionDetails': list(set(mapped_section)),
                   'attendanceList': modelDict1,
                   'studentList': modelDict, 'ClassSection': AttendanceClassSection,
                   'class_name': class_name, 'date': date_val, 'ClassDetails': list(set(mapped_class)), 'flag': flag,
                   'selected_year_id': selected_year_id,
                   'selected_year_name': current_academic_year, 'academic_year': years, 'selected_class': className,
                   'selected_section': sectionName}
    return form_vals, msg_flag

@user_login_required
def ViewAttendanceDetails(request):
    val_dict_view_att = request.POST
    form_vals = {}
    msg_flag = False

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
    except:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        return redirect('/school/manage_attendance/')

    if request.method == 'POST':
        request.session['val_dict_view_att'] = val_dict_view_att
        form_vals,msg_flag = load_export_view_attendance_filter(request, val_dict_view_att)
    else:
        form_vals,msg_flag = load_export_view_attendance_filter(request, request.session.get('val_dict_view_att'))

    if msg_flag:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        return redirect('/school/manage_attendance/')

    return render(request, "attendance_details.html", form_vals)

def ViewAttendanceEdit(request):
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
    except:
        messages.warning(request, "Current Academic Year Not Found. Please Create Current AY First.")
        return redirect('/school/manage_attendance/')

    att_val_dict = request.POST
    form_vals = {}
    flag = 0

    if request.method == 'POST':
        request.session['att_val_dict'] = att_val_dict
        form_vals, flag = load_view_attendance_edit(att_val_dict,request)
    else:
        form_vals, flag = load_view_attendance_edit(request.session.get('att_val_dict'),request)

    if flag == 1:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        return redirect('/school/manage_attendance/')
    if flag == 2:
        messages.warning(request, "There is no students available for the selected class and section Combination")


    return render(request, "attendance_edit.html", form_vals)


def load_view_attendance_edit(att_val_dict,request):
    flag = 0
    AbsentForm = AbsentReasonForm()
    AbsentReason1 = absent_categories.objects.all()
    PresentReason1 = present_categories.objects.all()

    AttendanceClassSection = AcademicYearClassSection()
    modelDict = []
    modelDict1 = []
    class_name = att_val_dict.get('class_name')
    section_name = att_val_dict.get('section_name')
    academic_years = att_val_dict.get('year_name')
    date_val = att_val_dict.get('date')

    date = att_val_dict.get('date')
    date = time.strftime("%Y-%m-%d")
    todayDate = time.strftime("%Y-%m-%d")

    user = request.user
    className = class_details.objects.get(id=class_name)
    sectionName = sections.objects.get(id=section_name)
    years = academic_year.objects.all()

    try:
        current_academic_year = academic_year.objects.get(id=academic_years)
        selected_year_id = current_academic_year.id

        mapped_class = []
        mapped_section = []
        if user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                        supervisor_id=user.id):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)

        elif user.is_allowed_user():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
                mapped_class.append(mapped_object.class_name)
                if mapped_object:
                    for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                        mapped_section.append(obj.section_name)
        else:
            if not user.is_system_admin():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years).filter(
                                Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
            else:
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
                    mapped_class.append(mapped_object.class_name)
                    if mapped_object:
                        for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                            mapped_section.append(obj.section_name)
    except:
        flag = 1
        form_vals ={}
        return form_vals, flag
        # messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        # return redirect('/school/manage_attendance/')

    if date == todayDate:
        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
                                                                            section_name=section_name,
                                                                            class_name=class_name)
        students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True,
                                                  joining_date__lte=todays_date()).order_by('first_name')

        if not students:
            flag = 2
            form_vals ={
                'AbsentForm': AbsentForm, 'AbsentReason': AbsentReason1, 'PresentReason': PresentReason1,
                'section_name': section_name, 'SectionDetails': list(set(mapped_section)),
                'attendanceList': modelDict1,
                'studentList': modelDict, 'ClassSection': AttendanceClassSection,
                'class_name': class_name, 'date': date_val, 'ClassDetails': list(set(mapped_class)),
                'selected_year_id': selected_year_id,
                'selected_year_name': current_academic_year, 'academic_year': years,
                'selected_class': className, 'selected_section': sectionName
            }
            return form_vals, flag


        AttendanceDetailsObj = attendance_details.objects.filter(attendancedate=date, student__in=students).order_by('student__first_name')

        if AttendanceDetailsObj and students:
            for obj in AttendanceDetailsObj:
                str4 = obj.present_category
                str6 = str4  # .replace("+", " ")

                str = obj.absent_category
                str2 = obj.absent_reason
                str1 = str  # .replace("+", " ")
                str3 = str2.replace("+", " ")
                obj.absent_category = str1
                obj.present_category = str6

                obj.absent_reason = str3
                modelDict1.append(obj)
        else:
            messages.warning(request,"The Attendance is not taken for the selected class section and date Combination.")
    else:
        flag_error = True
        # print"in else conditon of today date"
        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
                                                                            section_name=section_name,
                                                                            class_name=class_name)
        students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True,
                                                  joining_date__lte=todays_date()).order_by('first_name')
        if not students:
            flag_error = False
            messages.warning(request, "There is no students available for the selected class and section Combination")

        AttendanceDetailsObj = attendance_details.objects.filter(attendancedate=date, student__in=students).order_by('student__first_name')
        # print"AttendanceDetailsObjAttendanceDetailsObjAttendanceDetailsObj",AttendanceDetailsObj

        if AttendanceDetailsObj:
            for obj in AttendanceDetailsObj:
                str = obj.absent_category
                str2 = obj.absent_reason
                str1 = str
                str3 = str2.replace("+", " ")
                obj.absent_category = str1
                obj.absent_reason = str3
                modelDict1.append(obj)
        else:
            if flag_error:
                messages.warning(request,"The Attendance is not taken for the selected class section and date Combination.")

    form_vals ={
        'AbsentForm': AbsentForm, 'AbsentReason': AbsentReason1, 'PresentReason': PresentReason1,
        'section_name': section_name, 'SectionDetails': list(set(mapped_section)),
        'attendanceList': modelDict1,
        'studentList': modelDict, 'ClassSection': AttendanceClassSection,
        'class_name': class_name, 'date': date_val, 'ClassDetails': list(set(mapped_class)),
        'selected_year_id': selected_year_id,
        'selected_year_name': current_academic_year, 'academic_year': years, 'selected_class': className,
        'selected_section': sectionName
    }
    return form_vals, flag
    # return render(request, "attendance_edit.html",
    #               {'AbsentForm': AbsentForm, 'AbsentReason': AbsentReason1, 'PresentReason': PresentReason1,
    #                'section_name': section_name, 'SectionDetails': list(set(mapped_section)),
    #                'attendanceList': modelDict1,
    #                'studentList': modelDict, 'ClassSection': AttendanceClassSection,
    #                'class_name': class_name, 'date': date, 'ClassDetails': list(set(mapped_class)),
    #                'selected_year_id': selected_year_id,
    #                'selected_year_name': current_academic_year, 'academic_year': years, 'selected_class': className,
    #                'selected_section': sectionName})


# @user_login_required
# def ViewAttendanceEdit(request):
#     AbsentForm = AbsentReasonForm()
#     AbsentReason1 = absent_categories.objects.all()
#     PresentReason1 = present_categories.objects.all()
#
#     AttendanceClassSection = AcademicYearClassSection()
#     modelDict = []
#     modelDict1 = []
#     class_name = request.POST.get('class_name')
#     section_name = request.POST.get('section_name')
#     academic_years = request.POST.get('year_name')
#     date = request.POST.get('date')
#     todayDate = time.strftime("%Y-%m-%d")
#
#     user = request.user
#     className = class_details.objects.get(id=class_name)
#     sectionName = sections.objects.get(id=section_name)
#     years = academic_year.objects.all()
#
#     try:
#         current_academic_year = academic_year.objects.get(id=academic_years)
#         selected_year_id = current_academic_year.id
#
#         mapped_class = []
#         mapped_section = []
#         if user.is_supervisor():
#             for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
#                 for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
#                         supervisor_id=user.id):
#                     mapped_class.append(mapped_object.class_name)
#                     mapped_section.append(mapped_object.section_name)
#         else:
#             if not user.is_system_admin():
#                 for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years).filter(
#                                 Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
#                     mapped_class.append(mapped_object.class_name)
#                     mapped_section.append(mapped_object.section_name)
#             else:
#                 for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
#                     mapped_class.append(mapped_object.class_name)
#                     if mapped_object:
#                         for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
#                             mapped_section.append(obj.section_name)
#     except:
#         messages.warning(request, "Academic Year Class Section Mapping Not Found.")
#         return redirect('/school/manage_attendance/')
#
#     if date == todayDate:
#         year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
#                                                                             section_name=section_name,
#                                                                             class_name=class_name)
#         students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True, joining_date__lte=todays_date())
#
#         if not students:
#             messages.warning(request,"There is no students available for the selected class and section Combination")
#             return render(request, "attendance_edit.html",
#                           {'AbsentForm': AbsentForm, 'AbsentReason': AbsentReason1, 'PresentReason': PresentReason1,
#                            'section_name': section_name, 'SectionDetails': list(set(mapped_section)),
#                            'attendanceList': modelDict1,
#                            'studentList': modelDict, 'ClassSection': AttendanceClassSection,
#                            'class_name': class_name, 'date': date, 'ClassDetails': list(set(mapped_class)),
#                            'selected_year_id': selected_year_id,
#                            'selected_year_name': current_academic_year, 'academic_year': years,
#                            'selected_class': className, 'selected_section': sectionName})
#
#         AttendanceDetailsObj = attendance_details.objects.filter(attendancedate=date,student__in=students)
#
#         if AttendanceDetailsObj and students:
#             for obj in AttendanceDetailsObj:
#                 str4 = obj.present_category
#                 str6 = str4  # .replace("+", " ")
#
#                 str = obj.absent_category
#                 str2 = obj.absent_reason
#                 str1 = str#.replace("+", " ")
#                 str3 = str2.replace("+", " ")
#                 obj.absent_category = str1
#                 obj.present_category = str6
#
#                 obj.absent_reason = str3
#                 modelDict1.append(obj)
#         else:
#             messages.warning(request, "The Attendance is not taken for the selected class section and date Combination.")
#     else:
#         flag_error = True
#         #print"in else conditon of today date"
#         year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
#                                                                             section_name=section_name,
#                                                                             class_name=class_name)
#         students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True, joining_date__lte=todays_date())
#         if not students:
#             flag_error = False
#             messages.warning(request,"There is no students available for the selected class and section Combination")
#
#         AttendanceDetailsObj = attendance_details.objects.filter(attendancedate=date, student__in=students)
#         #print"AttendanceDetailsObjAttendanceDetailsObjAttendanceDetailsObj",AttendanceDetailsObj
#
#         if AttendanceDetailsObj:
#             for obj in AttendanceDetailsObj:
#                 str = obj.absent_category
#                 str2 = obj.absent_reason
#                 str1 = str
#                 str3 = str2.replace("+", " ")
#                 obj.absent_category = str1
#                 obj.absent_reason = str3
#                 modelDict1.append(obj)
#         else:
#             if flag_error:
#                 messages.warning(request, "The Attendance is not taken for the selected class section and date Combination.")
#
#     return render(request, "attendance_edit.html",
#                   {'AbsentForm': AbsentForm, 'AbsentReason': AbsentReason1,'PresentReason': PresentReason1,
#                    'section_name': section_name, 'SectionDetails':  list(set(mapped_section)), 'attendanceList': modelDict1,
#                    'studentList': modelDict, 'ClassSection': AttendanceClassSection,
#                    'class_name': class_name, 'date': date, 'ClassDetails':  list(set(mapped_class)),  'selected_year_id': selected_year_id,
#                        'selected_year_name': current_academic_year, 'academic_year': years, 'selected_class': className, 'selected_section': sectionName})

# @user_group_permission_required('attendance.add_attendance_details', '/school/home/')
# @the_decorator("an_argument", "another_argument")
@user_login_required
@user_permission_required('attendance.add_attendance_details', '/school/home/')
def SaveAttendance(request):
    Attendance_time = datetime.datetime.now().time()
    now = datetime.datetime.now()

    attendance_flag  = False
    if request.is_ajax():
        if request.method == 'POST':

            date = now.strftime("%Y-%m-%d")
            class_name = request.POST.get('className')
            section_name = request.POST.get('sectionName')
            data = request.POST['table']

            dict_data = ast.literal_eval(data)
            for obj in dict_data:
                StudentName = obj['studentName']

                if 'late' in obj:
                    Late = obj['late']
                else:
                    Late = False

                absentExplaination = ""

                attendanceStatus = obj['student']

                Absentreason = obj['Absentreason'].replace('+', ' ') if attendanceStatus == 'Absent' else None

                PresentReason = obj['PresentReason'].replace('+', ' ') if attendanceStatus == 'Present' else None

                if attendanceStatus == 'Present':
                    if PresentReason == 'select':
                        PresentReason = None
                    else:
                        PresentReason = present_categories.objects.get(present_category_name=PresentReason)

                if attendanceStatus == 'Absent':

                    if Absentreason == 'select':
                        if obj['student'] == "Present":
                            Absentreason = None
                        else:
                            Absentreason = None
                    else:
                        Absentreason = absent_categories.objects.get(absent_category_name=Absentreason)

                    if 'absentExplaination' in obj:
                        absentExplaination = obj['absentExplaination'].replace('+', ' ')


                StudentId = obj['studentId']

                try:
                    if not attendance_details.objects.filter(student=student_details.objects.get(id=StudentId, is_active=True), attendancedate=(datetime.datetime.now())).exists():
                        attendance_flag = True
                        attendance_details.objects.create(student=student_details.objects.get(id=StudentId, is_active=True, joining_date__lte=todays_date()),
                                                        status=attendanceStatus,attendance_taken_by=request.user,
                                                        absent_category=Absentreason, present_category=PresentReason, attendancedate=date,
                                                        absent_reason=absentExplaination, attendance_time=(datetime.datetime.now()).time())
                except Exception as e:
                    return HttpResponseBadRequest(json.dumps({'error': str(e.message.split(':')[0])}),
                                                  content_type="application/json")

        return HttpResponse(json.dumps({'success': " Attendance saved successfully.", 'class_name': class_name, 'section_name': section_name,'flag':attendance_flag}))

@user_login_required
@user_permission_required('attendance.change_attendance_details', '/school/home/')
def UpdateAttendance(request):
    if request.is_ajax():
        if request.method == 'POST':
            date = request.POST.get('date')
            date = time.strftime("%Y-%m-%d")

            data = request.POST['table']
            dict_data = ast.literal_eval(data)
            PresentReason = None

            modelDict = []

            for obj in dict_data:

                StudentId = obj['studentId']
                if 'late' in obj:
                    Late = obj['late']
                else:
                    Late = False

                attendanceStatus = obj['student']

                if attendanceStatus == 'Present':
                    PresentReason = str(obj['PresentReason']).replace('+', ' ')

                    if PresentReason == 'select':
                        PresentReason = None
                    else:
                        PresentReason = present_categories.objects.get(present_category_name=PresentReason)

                    Absentreason = None
                    absentExplaination = ''

                else:

                    Absentreason = str(obj['Absentreason']).replace('+', ' ')

                    if Absentreason == 'select':
                        Absentreason = None
                    else:
                        Absentreason = absent_categories.objects.get(absent_category_name=Absentreason)
                        Late = False

                    PresentReason = None

                    if 'absentExplaination' in obj:
                        absentExplaination = obj['absentExplaination'].replace('+', ' ')
                    else:
                        absentExplaination = ""

                if attendance_details.objects.filter(student=student_details.objects.get(id=StudentId, is_active=True, joining_date__lte=todays_date()),
                                                                attendancedate=date,status=attendanceStatus,
                                                                                            absent_category=Absentreason,present_category=PresentReason,
                                                                                            absent_reason=absentExplaination).exists():
                    pass
                else:
                    StudentDetail = attendance_details.objects.filter(student=student_details.objects.get(id=StudentId, is_active=True, joining_date__lte=todays_date()),
                                                                attendancedate=date).update(status=attendanceStatus,
                                                                                            absent_category=Absentreason,present_category=PresentReason,
                                                                                            absent_reason=absentExplaination,
                                                                                            attendance_time=(datetime.datetime.now()).time(),attendance_update_date_time = datetime.datetime.now())
                    modelDict.append(StudentDetail)
            messages.success(request, "Attendance Updated Successfully.")
        return JsonResponse(modelDict, safe=False)

num_map = [(1000, 'M'), (900, 'CM'), (500, 'D'), (400, 'CD'), (100, 'C'), (90, 'XC'),
           (50, 'L'), (40, 'XL'), (10, 'X'), (9, 'IX'), (5, 'V'), (4, 'IV'), (1, 'I')]


def num2roman(num):

    roman = ''

    while num > 0:
        for i, r in num_map:
            while num >= i:
                roman += r
                num -= i

    return roman

@user_login_required
def ViewSectionDetailAttendance(request):
    modelDict = []
    class_name = request.GET.get('class_name', None)
    # roman = num2roman(int(class_name))

    class_obj = class_details.objects.all().get(class_name=class_name)

    sections = academic_class_section_mapping.objects.filter(class_name=class_obj)

    # sections = class_details.objects.all().get(class_name=roman).section_ids.all()

    for obj3 in sections:
        modelDict.append(obj3.to_dict())

    return JsonResponse(modelDict, safe=False)

from operator import itemgetter
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest
@user_login_required
@user_permission_required('attendance.can_view_attendance_dashboard', '/school/home/')
def AttendanceDashboard(request):
    try:
        start_date = datetime.date.today().strftime("%Y-%m-%d")
        past_date = datetime.date.today() + datetime.timedelta(-30)
        past_three_days_date = datetime.date.today() + datetime.timedelta(-3)
        past_seven_days_date = datetime.date.today() + datetime.timedelta(-7)
        AbsentForm = AbsentReasonForm()
        DashboardAttendanceClassSection = DashboardAcademicYearClassSection()
        AttendanceClassSection = AcademicYearClassSection()
        ClassDetail = class_details.objects.all()
        Section = sections.objects.all()
        AbsentReason1 = absent_categories.objects.all()
        years = academic_year.objects.all()
        user =request.user

        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id

            mapped_class_list = mapped_user_classes(current_academic_year,request)
            class_list = []

            for class_rec in mapped_class_list:
                if student_details.objects.filter(academic_class_section__year_name=current_academic_year,academic_class_section__class_name_id=class_rec['id']):
                    class_list.append(class_rec)



            mapped_class = []
            mapped_section = []
            if user.is_supervisor():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                    for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                            supervisor_id=user.id):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)
            elif user.is_allowed_user():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
            else:
                if not user.is_system_admin():
                    for mapped_object in academic_class_section_mapping.objects.filter(
                            year_name=current_academic_year).filter(Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)
                else:
                    for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)

        except:
            messages.warning(request, "Academic Year Class Section Mapping Not Found.")
            return redirect('/school/manage_attendance/')

        student = student_details.objects.filter(academic_class_section__year_name__current_academic_year=True, is_active=True, joining_date__lte=todays_date()).count()
        present = attendance_details.objects.filter(~Q(present_category__present_category_name='Late'),status='Present', attendancedate=start_date,student__is_active=True).count()
        present_percent = ((100.0 * present / student)) if student > 0 else 0

        late = attendance_details.objects.filter(attendancedate=start_date,student__is_active=True,
                                                 present_category__present_category_name='Late').count()
        late_percent = round((100.0 * late / student), 2) if student > 0 else 0

        Absent = attendance_details.objects.filter(~Q(absent_category__absent_category_name__in=['Medical', 'Excused']),status='Absent', attendancedate=start_date,student__is_active=True).count()
        Absent_percent = round((100.0 * Absent / student), 2) if student > 0 else 0
        Medical = attendance_details.objects.filter(absent_category__absent_category_name='Medical', attendancedate=start_date,student__is_active=True,).count()
        Medical_percent = round((100.0 * Medical / student), 2) if student > 0 else 0

        Not_Taken = student - attendance_details.objects.filter(attendancedate=start_date,student__is_active=True,).count() if student > 0 else 0
        Not_Taken_percent = round((100.0 * Not_Taken / student), 2) if student > 0 else 0

        Exclused = attendance_details.objects.filter(absent_category__absent_category_name='Excused',student__is_active=True, attendancedate=start_date).count()
        #     print "ExclusedExclusedExclusedExclused",Exclused
        Exclused_percent = round((100.0 * Exclused / student), 2) if student > 0 else 0
         #     print"Exclused_percentExclused_percentExclused_percent",Exclused_percent
        todays_student = attendance_details.objects.filter(attendancedate=start_date,student__is_active=True).count()
        today_late = attendance_details.objects.filter(present_category__present_category_name='Late', attendancedate=start_date,student__is_active=True).count()
        #     print"today_latetoday_latetoday_late",today_late
        today_late_percent = round((100.0 * today_late / todays_student), 1) if todays_student > 0 else 0
        #     print"today_late_percenttoday_late_percenttoday_late_percent",today_late_percent
        thirty_days_student = attendance_details.objects.filter(attendancedate__range=[past_date, start_date],student__is_active=True).count()
        #     print" thirty_days_student thirty_days_student thirty_days_student", thirty_days_student
        thirty_days_late_student = attendance_details.objects.filter(present_category__present_category_name='Late', attendancedate__range=[past_date,
                                                                                                       start_date],student__is_active=True).count()
        #     print"thirty_days_late_studentthirty_days_late_student",thirty_days_late_student
        #     print"thirty_days_late_studentthirty_days_late_studentthirty_days_late_student",thirty_days_late_student
        thirty_days_late_percent = round((100.0 * thirty_days_late_student / thirty_days_student), 1) if thirty_days_student > 0 else 0
        #     print"thirty_days_late_percentthirty_days_late_percentthirty_days_late_percent",thirty_days_late_percent

        three_days_absent_student = attendance_details.objects.values_list('student', flat=True).filter(status='Absent',
                                                                                                      attendancedate__range=[
                                                                                                          past_three_days_date,
                                                                                                          start_date],student__is_active=True)
        my_count = pd.Series(three_days_absent_student).value_counts()
        three_days_absent_student_count = 0
        for count, elem in enumerate(my_count):

            if elem == 3:
                three_days_absent_student_count += 1

        seven_days_absent_student = attendance_details.objects.values_list('student', flat=True).filter(status='Absent',
                                                                                                      attendancedate__range=[
                                                                                                          past_seven_days_date,
                                                                                                          start_date],student__is_active=True)
        my_count1 = pd.Series(seven_days_absent_student).value_counts()
        seven_days_absent_student_count = 0
        for count, elem in enumerate(my_count1):

            if elem == 7:
                seven_days_absent_student_count += 1

        thirtee_days_absent_student = attendance_details.objects.values_list('student', flat=True).filter(status='Absent',
                                                                                                        attendancedate__range=[
                                                                                                            past_seven_days_date,
                                                                                                            start_date],student__is_active=True)
        my_count2 = pd.Series(thirtee_days_absent_student).value_counts()
        thirtee_days_absent_student_count = 0
        for count, elem in enumerate(my_count2):

            if elem == 30:
                thirtee_days_absent_student_count += 1

        obj = student_details.objects.all().filter(is_active=True, joining_date__lte=todays_date())
        for studentDetails in obj:
            AttendanceDetails = attendance_details.objects.filter(student=studentDetails.id,student__is_active=True).count()
            AbsentStudentDetails = attendance_details.objects.filter(student=studentDetails.id, status='Absent',student__is_active=True).count()

            if AttendanceDetails != 0:
                AbsentStudentPercentage = round((100.0 * AbsentStudentDetails / AttendanceDetails), 2) if AttendanceDetails > 0 else 0
                attendance_percentage = 100 - AbsentStudentPercentage
                student_details.objects.filter(id=studentDetails.id, is_active=True, joining_date__lte=todays_date()).update(attendance_percentage=attendance_percentage)

            else:
                student_details.objects.filter(id=studentDetails.id, is_active=True, joining_date__lte=todays_date()).update(attendance_percentage=0.00)

        LessThanFiftyAttendance = student_details.objects.filter(attendance_percentage__range=[0, 49], is_active=True, joining_date__lte=todays_date()).count()

        LessThanSeventyFiveAttendance = student_details.objects.filter(attendance_percentage__range=[50, 74], is_active=True, joining_date__lte=todays_date()).count()
        LessThanNinetyAttendance = student_details.objects.filter(attendance_percentage__range=[75, 90], is_active=True, joining_date__lte=todays_date()).count()

        attendance_taken_academic_class = attendance_details.objects.filter(student__is_active=True,attendancedate=todays_date().strftime("%Y-%m-%d")).values_list('student__academic_class_section', flat=1).distinct()
        all_academic_class = academic_class_section_mapping.objects.filter(year_name=current_academic_year)

        academic_class_rec_list = []

        for acd_rec in all_academic_class:
            if student_details.objects.filter(academic_class_section=acd_rec,
                                              joining_date__lte=datetime.date.today()).exists():
                acd_rec_dict = {}
                if acd_rec.id in attendance_taken_academic_class:
                    attendance_time = attendance_details.objects.filter(student__is_active=True,student__academic_class_section=acd_rec,attendancedate=todays_date())[0].attendance_time
                    acd_rec_dict['acd_rec'] = acd_rec
                    acd_rec_dict['attendance_taken_by'] = attendance_details.objects.filter(student__is_active=True,student__academic_class_section=acd_rec,attendancedate=todays_date())[0].attendance_taken_by
                    acd_rec_dict['time'] = str(attendance_time.strftime("%r"))
                else:
                    acd_rec_dict['acd_rec'] = acd_rec
                    acd_rec_dict['attendance_taken_by'] = ''
                    acd_rec_dict['time'] = 'Not Taken'
                academic_class_rec_list.append(acd_rec_dict)

        # sorted(academic_class_rec_list, key=itemgetter(1))
        academic_class_rec_list = sorted(academic_class_rec_list, key=itemgetter('time', 'acd_rec'), reverse=True)

        current_academic_year = academic_year.objects.get(current_academic_year=1)
        absent_category_list = absent_categories.objects.filter(daily_attendance=True)
        absent_cat_list = []

        absent_category_dict = {}
        attendance_class_counter = 0

        main_list = []

        total_stud_cnt = 0
        total_present = 0
        total_late = 0
        total_percentage = 0
        total_absent = 0
        total_net_att = 0

        for c in absent_category_list:
            absent_cat_list.append(c.absent_category_name)
            absent_category_dict[c.absent_category_name] = 0

        acd_cls_sec = academic_class_section_mapping.objects.filter(year_name=current_academic_year)

        for year_rec in acd_cls_sec:
            if student_details.objects.filter(academic_class_section=year_rec,
                                              joining_date__lte=datetime.date.today()).exists():
                net_attendance = 0

                main_dict = {}
                main_dict['classes'] = year_rec.class_name
                main_dict['sections'] = year_rec.section_name
                student_list = student_details.objects.filter(academic_class_section=year_rec, is_active=True,
                                                              joining_date__lte=datetime.date.today()).values_list('id')
                total_student_cnt = student_list.count()
                attendance_taken = attendance_details.objects.filter(student__is_active=True,attendancedate=todays_date().strftime("%Y-%m-%d"),
                                                                     student__academic_class_section=year_rec).exists()

                main_dict['student_cnt'] = total_student_cnt

                if attendance_taken:
                    attendance_class_counter += 1
                    present_cnt = attendance_details.objects.filter(~Q(present_category__present_category_name='Late'),student__is_active=True,
                                                                    attendancedate=todays_date().strftime("%Y-%m-%d"),
                                                                    status='Present',
                                                                    student__academic_class_section=year_rec,
                                                                    student__in=student_list).count()
                    if student_list:
                        present_percentage = round((present_cnt / total_student_cnt) * 100, 2)
                    else:
                        present_percentage = 0.0

                    absent_cnt = attendance_details.objects.filter(Q(absent_category__daily_attendance=False) | Q(absent_category__absent_category_name=None),
                                                                   attendancedate=todays_date().strftime("%Y-%m-%d"),student__is_active=True,
                                                                   status='Absent',
                                                                   student__academic_class_section=year_rec,
                                                                   student__in=student_list).count()

                    late_cnt = attendance_details.objects.filter(attendancedate=todays_date().strftime("%Y-%m-%d"),
                                                                 status='Present',student__is_active=True,
                                                                 present_category__present_category_name='Late',
                                                                 student__academic_class_section=year_rec,
                                                                 student__in=student_list).count()
                    total_present += present_cnt
                    total_percentage += present_percentage
                    total_stud_cnt += total_student_cnt
                    total_late += late_cnt
                    total_absent += absent_cnt

                    net_attendance += int(present_cnt)
                    net_attendance += int(late_cnt)
                    # net_attendance += int(late_cnt)

                else:
                    present_cnt = ''
                    present_percentage = ''
                    absent_cnt = ''
                    late_cnt = ''
                    total_stud_cnt += total_student_cnt

                main_dict['present_cnt'] = present_cnt
                main_dict['present_percentage'] = present_percentage
                main_dict['absent_cnt'] = absent_cnt
                main_dict['late_cnt'] = late_cnt

                for cat in absent_category_list:

                    category = attendance_details.objects.filter(attendancedate=todays_date().strftime("%Y-%m-%d"), absent_category=cat,
                                                                 student__academic_class_section=year_rec,student__is_active=True,
                                                                 student__in=student_list,status='Absent')
                    if category:
                        for c in category:
                            pass

                        if attendance_taken:
                            absent_category_dict[c.absent_category.absent_category_name] += category.count()
                            main_dict[c.absent_category.absent_category_name] = category.count()
                            net_attendance += int(category.count())
                        else:
                            main_dict[c.absent_category.absent_category_name] = ''
                            absent_category_dict[c.absent_category.absent_category_name] += 0
                    else:
                        if attendance_taken:
                            main_dict[cat.absent_category_name] = 0
                            absent_category_dict[str(cat.absent_category_name)] += 0
                            net_attendance += 0
                        else:
                            main_dict[cat.absent_category_name] = ''

                if attendance_taken:
                    if total_student_cnt != 0:
                        main_dict['net_attendance'] = round((net_attendance / int(total_student_cnt)) * 100, 2)
                        total_net_att += round((net_attendance / int(total_student_cnt)) * 100, 2)
                    else:
                        main_dict['net_attendance'] = 0
                else:
                    main_dict['net_attendance'] = ''

                main_list.append(main_dict)

        temp_list = []
        net_attendance_counter = 0
        temp_list.append('Total')
        temp_list.append('')
        temp_list.append(total_stud_cnt)
        temp_list.append(total_present)
        net_attendance_counter += total_present
        temp_list.append(total_late)
        net_attendance_counter += total_late
        if total_stud_cnt != 0:
            temp_list.append(round((float(total_present) / total_stud_cnt) * 100, 2))
        else:
            temp_list.append('0')
        temp_list.append(total_absent)

        list_counter = 0

        while list_counter < len(absent_cat_list):
            temp_list.append(absent_category_dict[absent_cat_list[list_counter]])
            net_attendance_counter += absent_category_dict[absent_cat_list[list_counter]]
            list_counter += 1

        if total_stud_cnt != 0:
            temp_list.append(round((float(net_attendance_counter) / total_stud_cnt) * 100, 2))
        else:
            temp_list.append(0)

        return render(request, "attendance_dashboard.html",
                      {'AbsentForm': AbsentForm, 'SectionDetails': list(set(mapped_section)),
                       'ClassSection': AttendanceClassSection, 'DashClassSection': DashboardAttendanceClassSection,
                       'ClassDetails': list(set(mapped_class)), 'AbsentReason': AbsentReason1, 'date': start_date,
                       'current_date': str(datetime.datetime.now().strftime("%Y-%m-%d")),
                       'present_percent': present_percent, 'late_percent': late_percent,
                       'Absent_percent': Absent_percent,
                       'Medical_percent': Medical_percent, 'Not_Taken_percent': Not_Taken_percent,
                       'Exclused_percent': Exclused_percent, 'student': student,
                       'present': present, 'late': late, 'Absent': Absent, 'Medical': Medical, 'Not_Taken': Not_Taken,
                       'Exclused': Exclused, 'thirty_days_late_percent': thirty_days_late_percent,
                       'today_late_percent': today_late_percent,
                       'three_days_absent_student_count': three_days_absent_student_count,
                       'seven_days_absent_student_count': seven_days_absent_student_count,
                       'thirtee_days_absent_student_count': thirtee_days_absent_student_count,
                       'LessThanFiftyAttendance': LessThanFiftyAttendance,
                       'LessThanSeventyFiveAttendance': LessThanSeventyFiveAttendance,
                       'LessThanNinetyAttendance': LessThanNinetyAttendance,
                       'DashboardAttendanceClassSection': DashboardAttendanceClassSection,
                       'selected_year_id': selected_year_id,
                       'selected_year_name': current_academic_year, 'academic_year': years,
                       'class_list': class_list, 'academic_class_rec_list': academic_class_rec_list,
                       'main_list': main_list, 'absent_cat_list': absent_cat_list,'final_count':temp_list,
                       })
    except Exception as e:
        return HttpResponseBadRequest(json.dumps({'message': str(e)}), content_type="application/json")
        # return render(request, "attendance_dashboard.html", {})


@user_login_required
@user_permission_required('attendance.can_view_attendance_dashboard', '/school/home/')
def ViewAttendanceTrend(request):
    modelDict = []
    if request.method == 'GET':

        class_name = request.GET.get('class_name')
        #     print "class_nameclass_nameclass_nameclass_nameclass_name",class_name
        section_name = request.GET.get('section_name')
        #     print "section_namesection_namesection_namesection_namesection_name" ,section_name
        to_date = request.GET.get('to_date') if request.GET.get('to_date') else academic_year.objects.get(current_academic_year=True).end_date
        #     print "to_dateto_dateto_dateto_dateto_date",to_date
        from_date = request.GET.get('from_date') if request.GET.get('from_date') else academic_year.objects.get(current_academic_year=True).start_date
        #     print"from_datefrom_datefrom_datefrom_date",from_date
        academic_years = request.GET.get('academic_years')

        if class_name == 'All' and section_name == 'All':
            stud = student_details.objects.filter(academic_class_section__year_name__current_academic_year=True, is_active=True, joining_date__lte=todays_date())

            if academic_years:
                stud = student_details.objects.filter(academic_class_section__year_name=academic_years, is_active=True, joining_date__lte=todays_date())

            student = stud.count()
            #         print"studentstudentstudentstudentstudent",student
            student_percent = round((100.0 * student / student), 2) if student > 0 else 0
            #         print"studentstudentstudentstudentstudent",student_percent
            present = attendance_details.objects.filter(student__in=stud, status='Present', attendancedate__range=[to_date, from_date ]).count()
            present_percent = round((100.0 * present / student), 2) if student > 0 else 0
            #         print"present_percentpresent_percentpresent_percent",present_percent
            late = attendance_details.objects.filter(student__in=stud, present_category__present_category_name='Late', attendancedate__range=[to_date, from_date ]).count()
            late_percent = round((100.0 * late / student), 2) if student > 0 else 0

            Absent = attendance_details.objects.filter(student__in=stud, status='Absent', attendancedate__range=[to_date, from_date ]).count()
            Absent_percent = round((100.0 * Absent / student), 2) if student > 0 else 0
            Medical = attendance_details.objects.filter(student__in=stud, absent_category__absent_category_name='Medical', attendancedate__range=[to_date, from_date ]).count()
            Medical_percent = round((100.0 * Medical / student), 2) if student > 0 else 0

            Not_Taken = attendance_details.objects.filter(student__in=stud, absent_category__absent_category_name='Not+Taken', attendancedate__range=[to_date, from_date ]).count()
            Not_Taken_percent = round((100.0 * Not_Taken / student), 2) if student > 0 else 0

            Not_Taken = student - attendance_details.objects.filter(
                student__in=stud, attendancedate__range=[to_date, from_date ]).count() if student > 0 else 0
            Not_Taken_percent = round((100.0 * Not_Taken / student), 2) if student > 0 else 0


            Exclused = attendance_details.objects.filter(student__in=stud, absent_category__absent_category_name='Excused', attendancedate__range=[to_date, from_date ]).count()
            Exclused_percent = round((100.0 * Exclused / student), 2) if student > 0 else 0



        elif class_name == 'All' and not section_name == 'All':

            year_class_section_obj = academic_class_section_mapping.objects.filter(year_name=academic_years,
                                                                                section_name=section_name)
            stud = student_details.objects.filter(academic_class_section__in=year_class_section_obj, is_active=True, joining_date__lte=todays_date())

            # SectionName = sections.objects.get(id=section_name)
            # stud = student_details.objects.all().filter(class_name=class_name, section=section_name)
            #         print"in else........................................."
            student = attendance_details.objects.filter(student__in=stud,
                                                        attendancedate__range=[from_date, to_date]).count()
            student_percent = round((100.0 * student / student), 2) if student > 0 else 0
            #         print"studentstudentstudentstudentstudent",student_percent
            present = attendance_details.objects.filter(student__in=stud,
                                                        attendancedate__range=[from_date, to_date],
                                                        status='Present').count()
            present_percent = round((100.0 * present / student), 2) if student > 0 else 0
            #         print"present_percentpresent_percentpresent_percent",present_percent
            late = attendance_details.objects.filter(student__in=stud,
                                                     attendancedate__range=[from_date, to_date],
                                                     present_category__present_category_name='Late').count()
            late_percent = round((100.0 * late / student), 2) if student > 0 else 0
            #         print"late_percentlate_percentlate_percentlate_percent",late_percent
            Absent = attendance_details.objects.filter(student__in=stud,
                                                       attendancedate__range=[from_date, to_date],
                                                       status='Absent').count()
            Absent_percent = round((100.0 * Absent / student), 2) if student > 0 else 0
            #         print"Absent_percentAbsent_percentAbsent_percentAbsent_percent",Absent_percent
            Medical = attendance_details.objects.filter(student__in=stud,
                                                        attendancedate__range=[from_date, to_date],
                                                        absent_category__absent_category_name='Medical').count()
            Medical_percent = round((100.0 * Medical / student), 2) if student > 0 else 0
            #         print"Medical_percentMedical_percentMedical_percent",Medical_percent
            Not_Taken = student - attendance_details.objects.filter(student__in=stud,
                                                          attendancedate__range=[from_date, to_date]).count() if student > 0 else 0
            Not_Taken_percent = round((100.0 * Not_Taken / student), 2) if student > 0 else 0
            #         print"Not_Taken_percentNot_Taken_percentNot_Taken_percent",Not_Taken_percent
            Exclused = attendance_details.objects.filter(student__in=stud,
                                                         attendancedate__range=[from_date, to_date],
                                                         absent_category__absent_category_name='Excused').count()
            Exclused_percent = round((100.0 * Exclused / student), 2) if student > 0 else 0

        elif not class_name == 'All' and  section_name == 'All':
            year_class_section_obj = academic_class_section_mapping.objects.filter(year_name=academic_years,
                                                                                class_name=class_name)
            stud = student_details.objects.filter(academic_class_section__in=year_class_section_obj, is_active=True, joining_date__lte=todays_date())

            # SectionName = sections.objects.get(id=section_name)
            # stud = student_details.objects.all().filter(class_name=class_name, section=section_name)
            #         print"in else........................................."
            student = attendance_details.objects.filter(student__in=stud,
                                                        attendancedate__range=[from_date, to_date]).count()
            student_percent = round((100.0 * student / student), 2) if student > 0 else 0
            #         print"studentstudentstudentstudentstudent",student_percent
            present = attendance_details.objects.filter(student__in=stud,
                                                        attendancedate__range=[from_date, to_date],
                                                        status='Present').count()
            present_percent = round((100.0 * present / student), 2) if student > 0 else 0
            #         print"present_percentpresent_percentpresent_percent",present_percent
            late = attendance_details.objects.filter(student__in=stud,
                                                     attendancedate__range=[from_date, to_date],
                                                     present_category__present_category_name='Late').count()
            late_percent = round((100.0 * late / student), 2) if student > 0 else 0
            #         print"late_percentlate_percentlate_percentlate_percent",late_percent
            Absent = attendance_details.objects.filter(student__in=stud,
                                                       attendancedate__range=[from_date, to_date],
                                                       status='Absent').count()
            Absent_percent = round((100.0 * Absent / student), 2) if student > 0 else 0
            #         print"Absent_percentAbsent_percentAbsent_percentAbsent_percent",Absent_percent
            Medical = attendance_details.objects.filter(student__in=stud,
                                                        attendancedate__range=[from_date, to_date],
                                                        absent_category__absent_category_name='Medical').count()
            Medical_percent = round((100.0 * Medical / student), 2) if student > 0 else 0
            #         print"Medical_percentMedical_percentMedical_percent",Medical_percent
            Not_Taken = student - attendance_details.objects.filter(student__in=stud,
                                                          attendancedate__range=[from_date, to_date]).count() if student > 0 else 0
            Not_Taken_percent = round((100.0 * Not_Taken / student), 2) if student > 0 else 0
            #         print"Not_Taken_percentNot_Taken_percentNot_Taken_percent",Not_Taken_percent
            Exclused = attendance_details.objects.filter(student__in=stud,
                                                         attendancedate__range=[from_date, to_date],
                                                         absent_category__absent_category_name='Excused').count()
            Exclused_percent = round((100.0 * Exclused / student), 2) if student > 0 else 0

        else:

            year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
                                                                                section_name=section_name,
                                                                                class_name=class_name)
            stud = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True, joining_date__lte=todays_date())


            # SectionName = sections.objects.get(id=section_name)
            # stud = student_details.objects.all().filter(class_name=class_name, section=section_name)
            #         print"in else........................................."
            student = attendance_details.objects.filter(student__in=stud,
                                                      attendancedate__range=[from_date, to_date]).count()
            student_percent = round((100.0 * student / student), 2) if student > 0 else 0
            #         print"studentstudentstudentstudentstudent",student_percent
            present = attendance_details.objects.filter(student__in=stud,
                                                      attendancedate__range=[from_date, to_date],
                                                      status='Present').count()
            present_percent = round((100.0 * present / student), 2) if student > 0 else 0
            #         print"present_percentpresent_percentpresent_percent",present_percent
            late = attendance_details.objects.filter(student__in=stud,
                                                   attendancedate__range=[from_date, to_date], present_category__present_category_name='Late').count()
            late_percent = round((100.0 * late / student), 2) if student > 0 else 0
            #         print"late_percentlate_percentlate_percentlate_percent",late_percent
            Absent = attendance_details.objects.filter(student__in=stud,
                                                     attendancedate__range=[from_date, to_date],
                                                       status='Absent').count()
            Absent_percent = round((100.0 * Absent / student), 2) if student > 0 else 0
            #         print"Absent_percentAbsent_percentAbsent_percentAbsent_percent",Absent_percent
            Medical = attendance_details.objects.filter(student__in=stud,
                                                      attendancedate__range=[from_date, to_date],
                                                        absent_category__absent_category_name='Medical').count()
            Medical_percent = round((100.0 * Medical / student), 2) if student > 0 else 0
            #         print"Medical_percentMedical_percentMedical_percent",Medical_percent
            Not_Taken = student - attendance_details.objects.filter(student__in=stud,
                                                        attendancedate__range=[from_date, to_date]).count() if student > 0 else 0
            Not_Taken_percent = round((100.0 * Not_Taken / student), 2) if student > 0 else 0
            #         print"Not_Taken_percentNot_Taken_percentNot_Taken_percent",Not_Taken_percent
            Exclused = attendance_details.objects.filter(student__in=stud,
                                                       attendancedate__range=[from_date, to_date],
                                                         absent_category__absent_category_name='Excused').count()
            Exclused_percent = round((100.0 * Exclused / student), 2) if student > 0 else 0
        # print"Exclused_percentExclused_percentExclused_percent",Exclused_percent
        modelDict.append(student_percent)
        modelDict.append(present_percent)
        modelDict.append(late_percent)
        modelDict.append(Absent_percent)
        modelDict.append(Medical_percent)
        modelDict.append(Not_Taken_percent)

        modelDict.append(Exclused_percent)
    return JsonResponse(modelDict, safe=False)


def load_dashboard(request, val_dict_dashboard):
    msg_flag = False
    AttendanceClassSection = AcademicYearClassSection()
    DashboardAttendanceClassSection = DashboardAcademicYearClassSection()
    ClassDetail = class_details.objects.all()
    Section = sections.objects.all()
    past_date = datetime.date.today() + datetime.timedelta(-30)
    past_three_days_date = datetime.date.today() + datetime.timedelta(-3)
    past_seven_days_date = datetime.date.today() + datetime.timedelta(-7)
    start_date = datetime.date.today()

    section_rec = []
    class_rec = []
    all_sections = []

    try:
        class_name = val_dict_dashboard.get('class_name')
        section_name = val_dict_dashboard.get('section_name')
        date = val_dict_dashboard.get('date')
        academic_years = val_dict_dashboard.get('year_name')

        years = academic_year.objects.all()
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

        class_list = current_academic_year_mapped_classes(current_academic_year,request.user)

        if class_name != '' and section_name != '':
            all_sections = current_class_mapped_sections(current_academic_year, class_name, request.user)

            class_rec = class_details.objects.get(id=class_name)
            section_rec = sections.objects.get(id=section_name)
            year_class_section_obj = academic_class_section_mapping.objects.filter(year_name=current_academic_year,
                                                                    section_name=section_rec,
                                                                    class_name=class_rec)

        if class_name != '' and section_name == '':
            all_sections = current_class_mapped_sections(current_academic_year, class_name, request.user)
            class_rec = class_details.objects.get(id=class_name)
            year_class_section_obj = academic_class_section_mapping.objects.filter(year_name=current_academic_year, class_name=class_rec)

        if class_name == '' and section_name != '':
            academic_year_rec = academic_year.objects.get(id=current_academic_year)
            section_rec = sections.objects.get(id=section_name)
            year_class_section_obj = academic_class_section_mapping.objects.filter(year_name=current_academic_year, section_name=section_rec)
            # student_recs = student_details.objects.filter(academic_class_section__in=acd_rec, is_active=True)

        if class_name == '' and section_name == '':
            year_class_section_obj = academic_class_section_mapping.objects.filter(year_name=current_academic_year)

        stud = student_details.objects.filter(academic_class_section__in=year_class_section_obj, is_active=True,
                                              joining_date__lte=todays_date())
        student = stud.count()

        present = attendance_details.objects.filter(~Q(present_category__present_category_name='Late'),student__in=stud, attendancedate=date,
                                                    status='Present').count()
        present_percent = round((100.0 * present / student), 2) if student > 0 else 0

        late = attendance_details.objects.filter(student__in=stud, attendancedate=date,
                                                 present_category__present_category_name='Late').count()
        late_percent = round((100.0 * late / student), 2) if student > 0 else 0

        Absent = attendance_details.objects.filter(~Q(absent_category__absent_category_name__in=['Medical', 'Excused']),student__in=stud, attendancedate=date,
                                                   status='Absent').count()
        Absent_percent = round((100.0 * Absent / student), 2) if student > 0 else 0
        Medical = attendance_details.objects.filter(student__in=stud, attendancedate=date,
                                                    absent_category__absent_category_name='Medical').count()
        Medical_percent = round((100.0 * Medical / student), 2) if student > 0 else 0

        Not_Taken = student - attendance_details.objects.filter(student__in=stud,
                                                                attendancedate=date).count()
        Not_Taken_percent = round((100.0 * Not_Taken / student), 2) if student > 0 else 0

        Exclused = attendance_details.objects.filter(student__in=stud, attendancedate=date,
                                                     absent_category__absent_category_name='Excused').count()
        Exclused_percent = round((100.0 * Exclused / student), 2) if student > 0 else 0
        today_late = attendance_details.objects.filter(student__in=stud,
                                                       present_category__present_category_name='Late',
                                                       attendancedate=start_date).count()
        today_late_percent = round((100.0 * late / student), 1) if student > 0 else 0

        thirty_days_student = attendance_details.objects.filter(student__in=stud, attendancedate__range=[past_date,
                                                                                                         start_date]).count()
        thirty_days_late_student = attendance_details.objects.filter(student__in=stud,
                                                                     present_category__present_category_name='Late',
                                                                     attendancedate__range=[past_date,
                                                                                            start_date, ]).count()
        thirty_days_late_percent = round((100.0 * thirty_days_late_student / thirty_days_student),
                                         1) if thirty_days_student > 0 else 0

        three_days_absent_student = attendance_details.objects.values_list('student', flat=True).filter(
            status='Absent', attendancedate__range=[past_three_days_date, start_date, ])
        my_count = pd.Series(three_days_absent_student).value_counts()
        three_days_absent_student_count = 0
        for count, elem in enumerate(my_count):

            if elem == 3:
                three_days_absent_student_count += 1

        seven_days_absent_student = attendance_details.objects.values_list('student', flat=True).filter(
            status='Absent', attendancedate__range=[past_seven_days_date, start_date, ])
        my_count1 = pd.Series(seven_days_absent_student).value_counts()
        seven_days_absent_student_count = 0
        for count, elem in enumerate(my_count1):
            if elem == 7:
                seven_days_absent_student_count += 1

        thirtee_days_absent_student = attendance_details.objects.values_list('student', flat=True).filter(
            status='Absent', attendancedate__range=[past_seven_days_date, start_date])
        my_count2 = pd.Series(thirtee_days_absent_student).value_counts()
        thirtee_days_absent_student_count = 0
        for count, elem in enumerate(my_count2):
            if elem == 30:
                thirtee_days_absent_student_count += 1

        LessThanFiftyAttendance = student_details.objects.filter(attendance_percentage__range=[0, 49],
                                                                 is_active=True,
                                                                 joining_date__lte=todays_date()).count()

        LessThanSeventyFiveAttendance = student_details.objects.filter(attendance_percentage__range=[50, 74],
                                                                       is_active=True,
                                                                       joining_date__lte=todays_date()).count()

        LessThanNinetyAttendance = student_details.objects.filter(attendance_percentage__range=[75, 90],
                                                                  is_active=True,
                                                                  joining_date__lte=todays_date()).count()

        attendance_taken_academic_class = attendance_details.objects.filter(
            attendancedate=date,student__is_active=True).values_list('student__academic_class_section', flat=1).distinct()
        all_academic_class = academic_class_section_mapping.objects.all()

        academic_class_rec_list = []
        for acd_rec in all_academic_class:
            if student_details.objects.filter(academic_class_section=acd_rec,
                                              joining_date__lte=datetime.date.today()).exists():
                acd_rec_dict = {}
                if acd_rec.id in attendance_taken_academic_class:
                    attendance_time = attendance_details.objects.filter(student__is_active=True,student__academic_class_section=acd_rec,attendancedate=date)[0].attendance_time
                    acd_rec_dict['acd_rec'] = acd_rec
                    acd_rec_dict['attendance_taken_by'] = attendance_details.objects.filter(student__is_active=True,student__academic_class_section=acd_rec,attendancedate=date)[0].attendance_taken_by
                    acd_rec_dict['time'] = str(attendance_time.strftime("%r"))
                else:
                    acd_rec_dict['acd_rec'] = acd_rec
                    acd_rec_dict['attendance_taken_by'] = ''
                    acd_rec_dict['time'] = 'Not Taken'
                academic_class_rec_list.append(acd_rec_dict)


        academic_class_rec_list = sorted(academic_class_rec_list, key=itemgetter('time', 'acd_rec'), reverse=True)

        current_academic_year = academic_year.objects.get(current_academic_year=1)
        absent_category_list = absent_categories.objects.filter(daily_attendance=True)
        absent_cat_list = []

        absent_category_dict = {}
        attendance_class_counter = 0

        main_list = []

        total_stud_cnt = 0
        total_present = 0
        total_late = 0
        total_percentage = 0
        total_absent = 0
        total_net_att = 0

        for c in absent_category_list:
            absent_cat_list.append(c.absent_category_name)
            absent_category_dict[c.absent_category_name] = 0

        main_list = []
        acd_cls_sec = academic_class_section_mapping.objects.filter(year_name=current_academic_year)

        for year_rec in acd_cls_sec:
            if student_details.objects.filter(academic_class_section=year_rec,
                                              joining_date__lte=datetime.date.today()).exists():
                net_attendance = 0

                main_dict = {}
                main_dict['classes'] = year_rec.class_name
                main_dict['sections'] = year_rec.section_name
                student_list = student_details.objects.filter(academic_class_section=year_rec, is_active=True,
                                                              joining_date__lte=datetime.date.today()).values_list('id')
                total_student_cnt = student_list.count()

                attendance_taken = attendance_details.objects.filter(attendancedate=date,student__is_active=True,
                                                                     student__academic_class_section=year_rec).exists()

                main_dict['student_cnt'] = total_student_cnt

                if attendance_taken:
                    attendance_class_counter += 1
                    present_cnt = attendance_details.objects.filter(~Q(present_category__present_category_name='Late'),
                                                                    attendancedate=date,student__is_active=True,
                                                                    status='Present',
                                                                    student__academic_class_section=year_rec,
                                                                    student__in=student_list).count()
                    if student_list:
                        present_percentage = round((present_cnt / total_student_cnt) * 100, 2)
                    else:
                        present_percentage = 0.0

                    absent_cnt = attendance_details.objects.filter(Q(absent_category__daily_attendance=False) | Q(absent_category__absent_category_name=None),
                                                                   attendancedate=date,student__is_active=True,
                                                                   status='Absent',
                                                                   student__academic_class_section=year_rec,
                                                                   student__in=student_list).count()

                    late_cnt = attendance_details.objects.filter(attendancedate=date,
                                                                 status='Present',student__is_active=True,
                                                                 present_category__present_category_name='Late',
                                                                 student__academic_class_section=year_rec,
                                                                 student__in=student_list).count()

                    total_present += present_cnt
                    total_percentage += present_percentage
                    total_stud_cnt += total_student_cnt
                    total_late += late_cnt
                    total_absent += absent_cnt

                    net_attendance += int(present_cnt)
                    net_attendance += int(late_cnt)
                    # net_attendance += int(late_cnt)

                else:
                    present_cnt = ''
                    present_percentage = ''
                    absent_cnt = ''
                    late_cnt = ''
                    total_stud_cnt += total_student_cnt

                main_dict['present_cnt'] = present_cnt
                main_dict['present_percentage'] = present_percentage
                main_dict['absent_cnt'] = absent_cnt
                main_dict['late_cnt'] = late_cnt

                for cat in absent_category_list:

                    category = attendance_details.objects.filter(attendancedate=date,student__is_active=True,
                                                                 absent_category=cat,
                                                                 student__academic_class_section=year_rec,
                                                                 student__in=student_list,status='Absent')
                    if category:
                        for c in category:
                            pass

                        if attendance_taken:
                            absent_category_dict[c.absent_category.absent_category_name] += category.count()
                            main_dict[c.absent_category.absent_category_name] = category.count()
                            net_attendance += int(category.count())
                        else:
                            absent_category_dict[c.absent_category.absent_category_name] += 0
                            main_dict[c.absent_category.absent_category_name] = ''
                    else:
                        if attendance_taken:
                            main_dict[cat.absent_category_name] = 0
                            absent_category_dict[str(cat.absent_category_name)] += 0
                            net_attendance += 0
                        else:
                            main_dict[cat.absent_category_name] = ''

                if attendance_taken:
                    if total_student_cnt != 0:
                        main_dict['net_attendance'] = round((net_attendance / int(total_student_cnt)) * 100, 2)
                        total_net_att += round((net_attendance / int(total_student_cnt)) * 100, 2)
                    else:
                        main_dict['net_attendance'] = 0
                else:
                    main_dict['net_attendance'] = ''

                main_list.append(main_dict)

        temp_list = []
        net_attendance_counter = 0
        temp_list.append('Total')
        temp_list.append('')
        temp_list.append(total_stud_cnt)
        temp_list.append(total_present)
        net_attendance_counter += total_present
        temp_list.append(total_late)
        net_attendance_counter += total_late
        if total_stud_cnt != 0:
            temp_list.append(round((float(total_present) / total_stud_cnt) * 100, 2))
        else:
            temp_list.append('0')
        temp_list.append(total_absent)

        list_counter = 0

        while list_counter < len(absent_cat_list):
            temp_list.append(absent_category_dict[absent_cat_list[list_counter]])
            net_attendance_counter += absent_category_dict[absent_cat_list[list_counter]]
            list_counter += 1

        if total_stud_cnt != 0:
            temp_list.append(round((float(net_attendance_counter) / total_stud_cnt) * 100, 2))
        else:
            temp_list.append(0)

    except:
        msg_flag = True
        form_vals = {}
        return form_vals, msg_flag


    form_vals = {'student': student, 'present': present, 'late': late, 'Absent': Absent, 'Medical': Medical,
                 'Not_Taken': Not_Taken, 'Exclused': Exclused,
                 'ClassSection': AttendanceClassSection,
                 'DashboardAttendanceClassSection': DashboardAttendanceClassSection,
                 'present_percent': present_percent, 'late_percent': late_percent,
                 'Absent_percent': Absent_percent, 'ClassDetails': ClassDetail, 'Section': Section,
                 'Medical_percent': Medical_percent, 'Not_Taken_percent': Not_Taken_percent,
                 'Exclused_percent': Exclused_percent, 'class_name': None,
                 'section_name': None, 'today_late_percent': today_late_percent,
                 'selected_year_id': selected_year_id,
                 'selected_year_name': current_academic_year, 'academic_year': years,
                 'thirty_days_late_percent': thirty_days_late_percent,
                 'three_days_absent_student_count': three_days_absent_student_count,
                 'seven_days_absent_student_count': seven_days_absent_student_count,
                 'thirtee_days_absent_student_count': thirtee_days_absent_student_count,
                 'LessThanFiftyAttendance': LessThanFiftyAttendance,
                 'LessThanSeventyFiveAttendance': LessThanSeventyFiveAttendance,
                 'LessThanNinetyAttendance': LessThanNinetyAttendance,
                 'section_list': all_sections,
                 'class_list': class_list,
                 'section_rec': section_rec,
                 'class_rec': class_rec, 'date': date, 'academic_class_rec_list': academic_class_rec_list,
                 'main_list': main_list, 'absent_cat_list': absent_cat_list,
                 'current_date': date, 'final_count':temp_list,
                 }
    return form_vals, msg_flag

@user_login_required
@user_permission_required('attendance.can_view_attendance_import_export', '/school/home/')
def StudentDetailDashboard(request):
    val_dict_dashboard= request.POST
    form_vals = {}
    msg_flag = False

    year_class_section_object = academic_class_section_mapping.objects.all()
    if year_class_section_object:
        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id
        except:
            messages.warning(request, "Academic Year Class Section Mapping Not Found.")
            return redirect('/school/attendance_view/')

    if request.method == 'POST':
        request.session['val_dict_dashboard'] = val_dict_dashboard
        form_vals,msg_flag = load_dashboard(request, val_dict_dashboard)
    else:
        form_vals,msg_flag = load_dashboard(request, request.session.get('val_dict_dashboard'))

    if msg_flag:
        return redirect('/school/attendanceDashboard/')

    return render(request, "attendance_dashboard.html", form_vals)



# @user_login_required
# def StudentDetailDashboard(request):
#     AttendanceClassSection = AcademicYearClassSection()
#     DashboardAttendanceClassSection = DashboardAcademicYearClassSection()
#     ClassDetail = class_details.objects.all()
#     Section = sections.objects.all()
#     past_date = datetime.date.today() + datetime.timedelta(-30)
#     past_three_days_date = datetime.date.today() + datetime.timedelta(-3)
#     past_seven_days_date = datetime.date.today() + datetime.timedelta(-7)
#     start_date = datetime.date.today()
#
#     if request.method == 'POST':
#         try:
#             class_name = request.POST.get('class_name')
#             section_name = request.POST.get('section_name')
#             date = request.POST.get('date')
#             academic_years = request.POST.get('year_name')
#
#             years = academic_year.objects.all()
#             year_class_section_object = academic_class_section_mapping.objects.all()
#             if year_class_section_object:
#                 try:
#                     current_academic_year = academic_year.objects.get(current_academic_year=1)
#                     selected_year_id = current_academic_year.id
#                 except:
#                     messages.warning(request, "Academic Year Class Section Mapping Not Found.")
#                     return redirect('/school/attendance_view/')
#
#             year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
#                                                                                 section_name=section_name,
#                                                                                 class_name=class_name)
#             stud = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True, joining_date__lte=todays_date())
#             student = stud.count()
#
#             present = attendance_details.objects.filter(student__in=stud, attendancedate=date,
#                                                       status='Present').count()
#             present_percent = round((100.0 * present / student), 2) if student > 0 else 0
#
#             late = attendance_details.objects.filter(student__in=stud, attendancedate=date,
#                                                      present_category__present_category_name='Late').count()
#             late_percent = round((100.0 * late / student), 2) if student > 0 else 0
#
#             Absent = attendance_details.objects.filter(student__in=stud, attendancedate=date,
#                                                      status='Absent').count()
#             Absent_percent = round((100.0 * Absent / student), 2) if student > 0 else 0
#             Medical = attendance_details.objects.filter(student__in=stud, attendancedate=date,
#                                                         absent_category__absent_category_name='Medical').count()
#             Medical_percent = round((100.0 * Medical / student), 2) if student > 0 else 0
#
#             Not_Taken = student - attendance_details.objects.filter(student__in=stud,
#                                                         attendancedate=date).count()
#             Not_Taken_percent = round((100.0 * Not_Taken / student), 2) if student > 0 else 0
#
#             Exclused = attendance_details.objects.filter(student__in=stud, attendancedate=date,
#                                                          absent_category__absent_category_name='Excused').count()
#             Exclused_percent = round((100.0 * Exclused / student), 2) if student > 0 else 0
#             today_late = attendance_details.objects.filter(student__in=stud, present_category__present_category_name='Late', attendancedate=start_date).count()
#             today_late_percent = round((100.0 * late / student), 1) if student > 0 else 0
#
#             thirty_days_student = attendance_details.objects.filter(student__in=stud, attendancedate__range=[past_date, start_date]).count()
#             thirty_days_late_student = attendance_details.objects.filter(student__in=stud, present_category__present_category_name='Late', attendancedate__range=[past_date,
#                                                                                                            start_date, ]).count()
#             thirty_days_late_percent = round((100.0 * thirty_days_late_student / thirty_days_student), 1) if thirty_days_student > 0 else 0
#
#             three_days_absent_student = attendance_details.objects.values_list('student', flat=True).filter(
#                 status='Absent', attendancedate__range=[past_three_days_date, start_date, ])
#             my_count = pd.Series(three_days_absent_student).value_counts()
#             three_days_absent_student_count = 0
#             for count, elem in enumerate(my_count):
#
#                 if elem == 3:
#                     three_days_absent_student_count += 1
#
#             seven_days_absent_student = attendance_details.objects.values_list('student', flat=True).filter(
#                 status='Absent', attendancedate__range=[past_seven_days_date, start_date, ])
#             #     print"three_days_absent_studentthree_days_absent_student" , seven_days_absent_student
#             my_count1 = pd.Series(seven_days_absent_student).value_counts()
#             #     print"my_countmy_countmy_countmy_countmy_count",my_count1
#             seven_days_absent_student_count = 0
#             for count, elem in enumerate(my_count1):
#                 if elem == 7:
#                     seven_days_absent_student_count += 1
#
#             thirtee_days_absent_student = attendance_details.objects.values_list('student', flat=True).filter(
#                 status='Absent', attendancedate__range=[past_seven_days_date, start_date, ])
#             #     print"three_days_absent_studentthree_days_absent_student" , thirtee_days_absent_student
#             my_count2 = pd.Series(thirtee_days_absent_student).value_counts()
#             #     print"my_countmy_countmy_countmy_countmy_count",my_count2
#             thirtee_days_absent_student_count = 0
#             for count, elem in enumerate(my_count2):
#                 #        print "count, elemcount, elemcount, elem", count
#
#                 #        print "elemelemelemelem", elem
#
#                 if elem == 30:
#                     thirtee_days_absent_student_count += 1
#
#             LessThanFiftyAttendance = student_details.objects.filter(attendance_percentage__range=[0, 49], is_active=True, joining_date__lte=todays_date()).count()
#
#             LessThanSeventyFiveAttendance = student_details.objects.filter(attendance_percentage__range=[50, 74], is_active=True, joining_date__lte=todays_date()).count()
#
#             LessThanNinetyAttendance = student_details.objects.filter(attendance_percentage__range=[75, 90], is_active=True, joining_date__lte=todays_date()).count()
#
#             return render(request, "attendance_dashboard.html",
#                           {'student': student, 'present': present, 'late': late, 'Absent': Absent, 'Medical': Medical,
#                            'Not_Taken': Not_Taken, 'Exclused': Exclused,
#                            'ClassSection': AttendanceClassSection,'DashboardAttendanceClassSection': DashboardAttendanceClassSection,
#                            'present_percent': present_percent, 'late_percent': late_percent,
#                            'Absent_percent': Absent_percent, 'ClassDetails': ClassDetail, 'Section': Section,
#                            'Medical_percent': Medical_percent, 'Not_Taken_percent': Not_Taken_percent,
#                            'Exclused_percent': Exclused_percent, 'class_name': None,
#                            'section_name': None, 'today_late_percent': today_late_percent,
#                            'selected_year_id': selected_year_id,
#                            'selected_year_name': current_academic_year, 'academic_year': years,
#                            'thirty_days_late_percent': thirty_days_late_percent,
#                            'three_days_absent_student_count': three_days_absent_student_count,
#                            'seven_days_absent_student_count': seven_days_absent_student_count,
#                            'thirtee_days_absent_student_count': thirtee_days_absent_student_count,
#                            'LessThanFiftyAttendance': LessThanFiftyAttendance,
#                            'LessThanSeventyFiveAttendance': LessThanSeventyFiveAttendance,
#                            'LessThanNinetyAttendance': LessThanNinetyAttendance})
#
#         except:
#             return redirect('/school/attendanceDashboard/')


@user_login_required
def LateStudentList(request):
    AttendanceYearClassSectionForm = AcademicYearClassSection()
    user = request.user
    years = academic_year.objects.all()

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

        mapped_class = []
        mapped_section = []
        if user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                        supervisor_id=user.id):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)

        elif user.is_allowed_user():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                mapped_class.append(mapped_object.class_name)
                mapped_section.append(mapped_object.section_name)

        else:
            if not user.is_system_admin():
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=current_academic_year).filter(
                            Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
            else:
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=current_academic_year):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)

    except:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        return redirect('/school/attendance_view/')

    return render(request, "late_student_list.html", {'ClassDetails': list(set(mapped_class)),  'SectionDetails': list(set(mapped_section)), 'YearClassSectionForm':AttendanceYearClassSectionForm, 'selected_year_id': selected_year_id,
                       'selected_year_name': current_academic_year, 'academic_year': years})

# @user_login_required
# def LateStudentDetails(request):
#     # print"late student details"
#     if request.method == 'POST':
#         AttendanceYearClassSectionForm = AcademicYearClassSection()
#         past_date = datetime.date.today() + datetime.timedelta(-30)
#         start_date = datetime.date.today()
#         class_name = request.POST.get('class_name')
#         section_name = request.POST.get('section_name')
#         academic_years = request.POST.get('year_name')
#
#         user = request.user
#         className = class_details.objects.get(id=class_name)
#         sectionName = sections.objects.get(id=section_name)
#         years = academic_year.objects.all()
#
#         try:
#             current_academic_year = academic_year.objects.get(id=academic_years)
#             selected_year_id = current_academic_year.id
#
#             mapped_class = []
#             mapped_section = []
#             if user.is_supervisor():
#                 for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
#                     for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
#                             supervisor_id=user.id):
#                         mapped_class.append(mapped_object.class_name)
#                         mapped_section.append(mapped_object.section_name)
#             else:
#                 if not user.is_system_admin():
#                     for mapped_object in academic_class_section_mapping.objects.filter(
#                             year_name=academic_years).filter(
#                                     Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
#                         mapped_class.append(mapped_object.class_name)
#                         mapped_section.append(mapped_object.section_name)
#                 else:
#                     for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
#                         mapped_class.append(mapped_object.class_name)
#                         if mapped_object:
#                             for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
#                                 mapped_section.append(obj.section_name)
#         except:
#             messages.warning(request, "Academic Year Class Section Mapping Not Found.")
#             return redirect('/school/manage_attendance/')
#
#         year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
#                                                                             section_name=section_name,
#                                                                             class_name=class_name)
#         students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True, joining_date__lte=todays_date())
#         late = attendance_details.objects.filter(present_category__present_category_name='Late',
#                                                attendancedate__range=[past_date, start_date], student__in=students)
#
#         return render(request, "late_student_list.html",
#                       {'SectionName': section_name, 'ClassDetailName': class_name, 'late': late,
#                        'ClassDetails': list(set(mapped_class)), 'SectionDetails':  list(set(mapped_section)), 'class_name': class_name,
#                        'section_name': section_name, 'YearClassSectionForm':AttendanceYearClassSectionForm, 'selected_year_id': selected_year_id,
#                        'selected_year_name': current_academic_year, 'academic_year': years, 'selected_class': className, 'selected_section': sectionName})


def load_late_student_list(val_dict_late, request):

    error_flag = False

    AttendanceYearClassSectionForm = AcademicYearClassSection()
    past_date = datetime.date.today() + datetime.timedelta(-30)
    start_date = datetime.date.today()
    class_name = val_dict_late.get('class_name')
    section_name = val_dict_late.get('section_name')
    academic_years = val_dict_late.get('year_name')

    user = request.user
    className = class_details.objects.get(id=class_name)
    sectionName = sections.objects.get(id=section_name)
    years = academic_year.objects.all()

    try:
        current_academic_year = academic_year.objects.get(id=academic_years)
        selected_year_id = current_academic_year.id

        mapped_class = []
        mapped_section = []
        if user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                        supervisor_id=user.id):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)

        elif user.is_allowed_user():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
                mapped_class.append(mapped_object.class_name)
                if mapped_object:
                    for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                        mapped_section.append(obj.section_name)

        else:
            if not user.is_system_admin():
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=academic_years).filter(
                            Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
            else:
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
                    mapped_class.append(mapped_object.class_name)
                    if mapped_object:
                        for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                            mapped_section.append(obj.section_name)
    except:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        form_vals = {}
        error_flag = True
        return form_vals,error_flag


    year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
                                                                        section_name=section_name,
                                                                        class_name=class_name)
    students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True,
                                              joining_date__lte=todays_date())
    late = attendance_details.objects.filter(present_category__present_category_name='Late',
                                             attendancedate__range=[past_date, start_date], student__in=students).order_by('student__first_name')


    form_vals = {'SectionName': section_name, 'ClassDetailName': class_name, 'late': late,
                       'ClassDetails': list(set(mapped_class)), 'SectionDetails':  list(set(mapped_section)), 'class_name': class_name,
                       'section_name': section_name, 'YearClassSectionForm':AttendanceYearClassSectionForm, 'selected_year_id': selected_year_id,
                       'selected_year_name': current_academic_year, 'academic_year': years, 'selected_class': className, 'selected_section': sectionName}
    return form_vals, error_flag

@user_login_required
def LateStudentDetails(request):
    val_dict_late= request.POST
    error_flag = False

    if request.method == 'POST':
        request.session['val_dict_late'] = val_dict_late
        form_vals,error_flag = load_late_student_list(val_dict_late, request)
    else:
        form_vals,error_flag = load_late_student_list(request.session.get('val_dict_late'), request)

    if error_flag:
        return redirect('/school/manage_attendance/')

    return render(request, "late_student_list.html", form_vals)

@user_login_required
def RegularAbsenteesList(request):
    AttendanceYearClassSectionForm = AcademicYearClassSection()
    user = request.user
    years = academic_year.objects.all()

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

        mapped_class = []
        mapped_section = []
        if user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                        supervisor_id=user.id):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)

        elif user.is_allowed_user():
            for mapped_object in academic_class_section_mapping.objects.filter(
                    year_name=current_academic_year):
                mapped_class.append(mapped_object.class_name)
                mapped_section.append(mapped_object.section_name)

        else:
            if not user.is_system_admin():
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=current_academic_year).filter(
                            Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
            else:
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=current_academic_year):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
    except:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        return redirect('/school/attendance_view/')

    return render(request, "regular_absentees_list.html", {'ClassDetails': list(set(mapped_class)), 'SectionDetails':  list(set(mapped_section)), 'YearClassSectionForm':AttendanceYearClassSectionForm, 'selected_year_id': selected_year_id,
                       'selected_year_name': current_academic_year, 'academic_year': years})



def load_bulk_regular_absentees(val_dict_regular_absentees, request):

    error_flag = False

    AttendanceYearClassSectionForm = AcademicYearClassSection()
    past_date = datetime.date.today() + datetime.timedelta(-30)
    start_date = datetime.date.today()
    class_name = val_dict_regular_absentees.get('class_name')
    section_name = val_dict_regular_absentees.get('section_name')
    academic_years = val_dict_regular_absentees.get('year_name')

    user = request.user
    className = class_details.objects.get(id=class_name)
    sectionName = sections.objects.get(id=section_name)
    years = academic_year.objects.all()

    try:
        current_academic_year = academic_year.objects.get(id=academic_years)
        selected_year_id = current_academic_year.id

        mapped_class = []
        mapped_section = []
        if user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                        supervisor_id=user.id):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
        elif user.is_allowed_user():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
                mapped_class.append(mapped_object.class_name)
                if mapped_object:
                    for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                        mapped_section.append(obj.section_name)

        else:
            if not user.is_system_admin():
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=academic_years).filter(
                            Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
            else:
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
                    mapped_class.append(mapped_object.class_name)
                    if mapped_object:
                        for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                            mapped_section.append(obj.section_name)
    except:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        form_vals = {}
        error_flag =True
        return form_vals, error_flag

    year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
                                                                        section_name=section_name,
                                                                        class_name=class_name)
    students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True, joining_date__lte=todays_date())

    absentList = attendance_details.objects.filter(status='Absent',
                                                     attendancedate__range=[past_date, start_date], student__in=students).order_by('student__first_name')

    form_vals = {
        'absentList': absentList,
        'ClassDetails': list(set(mapped_class)),
        'SectionDetails': list(set(mapped_section)),
        'class_name': class_name,
        'section_name': section_name,
        'YearClassSectionForm': AttendanceYearClassSectionForm,
        'selected_year_id': selected_year_id,
        'selected_year_name': current_academic_year,
        'academic_year': years, 'selected_class': className,
        'selected_section': sectionName
    }
    return form_vals, error_flag

@user_login_required
def RegularAbsenteesDetail(request):
    val_dict_regular_absentees = request.POST
    error_flag = False

    if request.method == 'POST':
        request.session['val_dict_regular_absentees'] = val_dict_regular_absentees
        form_vals,error_flag = load_bulk_regular_absentees(val_dict_regular_absentees, request)
    else:
        form_vals,error_flag = load_bulk_regular_absentees(request.session.get('val_dict_regular_absentees'), request)

    if error_flag:
        return redirect('/school/manage_attendance/')

    return render(request, "regular_absentees_list.html", form_vals)



# @user_login_required
# def RegularAbsenteesDetail(request):
#     if request.method == 'POST':
#         AttendanceYearClassSectionForm = AcademicYearClassSection()
#         past_date = datetime.date.today() + datetime.timedelta(-30)
#         start_date = datetime.date.today()
#         class_name = request.POST.get('class_name')
#         section_name = request.POST.get('section_name')
#         academic_years = request.POST.get('year_name')
#
#         user = request.user
#         className = class_details.objects.get(id=class_name)
#         sectionName = sections.objects.get(id=section_name)
#         years = academic_year.objects.all()
#
#         try:
#             current_academic_year = academic_year.objects.get(id=academic_years)
#             selected_year_id = current_academic_year.id
#
#             mapped_class = []
#             mapped_section = []
#             if user.is_supervisor():
#                 for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
#                     for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
#                             supervisor_id=user.id):
#                         mapped_class.append(mapped_object.class_name)
#                         mapped_section.append(mapped_object.section_name)
#             else:
#                 if not user.is_system_admin():
#                     for mapped_object in academic_class_section_mapping.objects.filter(
#                             year_name=academic_years).filter(
#                                 Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
#                         mapped_class.append(mapped_object.class_name)
#                         mapped_section.append(mapped_object.section_name)
#                 else:
#                     for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
#                         mapped_class.append(mapped_object.class_name)
#                         if mapped_object:
#                             for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
#                                 mapped_section.append(obj.section_name)
#         except:
#             messages.warning(request, "Academic Year Class Section Mapping Not Found.")
#             return redirect('/school/manage_attendance/')
#
#         year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
#                                                                             section_name=section_name,
#                                                                             class_name=class_name)
#         students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True, joining_date__lte=todays_date())
#
#         absentList = attendance_details.objects.filter(status='Absent',
#                                                      attendancedate__range=[past_date, start_date], student__in=students)
#
#         return render(request, "regular_absentees_list.html",
#                       {'absentList': absentList,
#                        'ClassDetails': list(set(mapped_class)), 'SectionDetails':  list(set(mapped_section)), 'class_name': class_name,
#                        'section_name': section_name, 'YearClassSectionForm':AttendanceYearClassSectionForm, 'selected_year_id': selected_year_id,
#                        'selected_year_name': current_academic_year, 'academic_year': years, 'selected_class': className, 'selected_section': sectionName})

@user_login_required
def AttendancePercentage(request):
    AttendanceYearClassSectionForm = AcademicYearClassSection()
    user = request.user
    years = academic_year.objects.all()

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

        mapped_class = []
        mapped_section = []
        if user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                        supervisor_id=user.id):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
        elif user.is_allowed_user():
            for mapped_object in academic_class_section_mapping.objects.filter(
                    year_name=current_academic_year):
                mapped_class.append(mapped_object.class_name)
                mapped_section.append(mapped_object.section_name)
        else:
            if not user.is_system_admin():
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=current_academic_year).filter(
                            Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
            else:
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=current_academic_year):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
    except:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        return redirect('/school/attendance_view/')

    return render(request, "attendance_percentage_list.html", {'ClassDetails': list(set(mapped_class)), 'SectionDetails':  list(set(mapped_section)), 'YearClassSectionForm':AttendanceYearClassSectionForm, 'selected_year_id': selected_year_id,
                       'selected_year_name': current_academic_year, 'academic_year': years})



def load_student_attendance_percentage(val_dict_att_percentage, request):

    error_flag = False

    AttendanceYearClassSectionForm = AcademicYearClassSection()
    class_name = val_dict_att_percentage.get('class_name')
    section_name = val_dict_att_percentage.get('section_name')
    academic_years = val_dict_att_percentage.get('year_name')
    user = request.user
    className = class_details.objects.get(id=class_name)
    sectionName = sections.objects.get(id=section_name)
    years = academic_year.objects.all()

    try:
        current_academic_year = academic_year.objects.get(id=academic_years)
        selected_year_id = current_academic_year.id

        mapped_class = []
        mapped_section = []

        if user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                        supervisor_id=user.id):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)

        elif user.is_allowed_user():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
                mapped_class.append(mapped_object.class_name)
                if mapped_object:
                    for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                        mapped_section.append(obj.section_name)

        else:
            if not user.is_system_admin():
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=academic_years).filter(
                            Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
            else:
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
                    mapped_class.append(mapped_object.class_name)
                    if mapped_object:
                        for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                            mapped_section.append(obj.section_name)
    except:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        form_vals = {}
        error_flag = True
        return form_vals,error_flag

    year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
                                                                        section_name=section_name,
                                                                        class_name=class_name)
    students_list = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True,
                                                   joining_date__lte=todays_date()).order_by('first_name')

    for student in students_list:
        total_attendance_count = attendance_details.objects.all().filter(student=student).count()
        present = attendance_details.objects.filter(status='Present', student=student).count()
        present_percent = ((100.0 * present / total_attendance_count)) if present > 0 else 0
        student1 = student_details.objects.get(id=student.id, is_active=True, joining_date__lte=todays_date())
        student1.attendance_percentage = present_percent
        student1.save()
    AttendanceStudentList = student_details.objects.filter(academic_class_section__class_name=class_name,
                                                           academic_class_section__section_name=section_name,
                                                           is_active=True, joining_date__lte=todays_date()).order_by('first_name')

    form_vals = {'AttendanceStudentList': AttendanceStudentList,
                 'ClassDetails': list(set(mapped_class)),
                 'SectionDetails':  list(set(mapped_section)),
                 'class_name': class_name, 'section_name': section_name,
                 'YearClassSectionForm':AttendanceYearClassSectionForm,
                 'selected_year_id': selected_year_id,
                 'selected_year_name': current_academic_year,
                 'academic_year': years,
                 'selected_class': className,
                 'selected_section': sectionName}
    return form_vals, error_flag

@user_login_required
def StudentAttendancePercentage(request):
    val_dict_att_percentage = request.POST
    error_flag = False

    if request.method == 'POST':
        request.session['val_dict_att_percentage'] = val_dict_att_percentage
        form_vals,error_flag = load_student_attendance_percentage(val_dict_att_percentage, request)
    else:
        form_vals,error_flag = load_student_attendance_percentage(request.session.get('val_dict_att_percentage'), request)

    if error_flag:
        return redirect('/school/manage_attendance/')

    return render(request, "attendance_percentage_list.html", form_vals)




# @user_login_required
# def StudentAttendancePercentage(request):
#     AttendanceYearClassSectionForm = AcademicYearClassSection()
#     class_name = request.POST.get('class_name')
#     section_name = request.POST.get('section_name')
#     academic_years = request.POST.get('year_name')
#     user = request.user
#     className = class_details.objects.get(id=class_name)
#     sectionName = sections.objects.get(id=section_name)
#     years = academic_year.objects.all()
#
#     try:
#         current_academic_year = academic_year.objects.get(id=academic_years)
#         selected_year_id = current_academic_year.id
#
#         mapped_class = []
#         mapped_section = []
#
#         if user.is_supervisor():
#             for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
#                 for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
#                         supervisor_id=user.id):
#                     mapped_class.append(mapped_object.class_name)
#                     mapped_section.append(mapped_object.section_name)
#         else:
#             if not user.is_system_admin():
#                 for mapped_object in academic_class_section_mapping.objects.filter(
#                         year_name=academic_years).filter(
#                             Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
#                     mapped_class.append(mapped_object.class_name)
#                     mapped_section.append(mapped_object.section_name)
#             else:
#                 for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_years):
#                     mapped_class.append(mapped_object.class_name)
#                     if mapped_object:
#                         for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
#                             mapped_section.append(obj.section_name)
#     except:
#         messages.warning(request, "Academic Year Class Section Mapping Not Found.")
#         return redirect('/school/manage_attendance/')
#
#     year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
#                                                                         section_name=section_name,
#                                                                         class_name=class_name)
#     students_list = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True, joining_date__lte=todays_date())
#
#     for student in students_list:
#         total_attendance_count = attendance_details.objects.all().filter(student=student).count()
#         present = attendance_details.objects.filter(status='Present',student=student).count()
#         present_percent = ((100.0 * present / total_attendance_count)) if present > 0 else 0
#         student1 = student_details.objects.get(id=student.id, is_active=True, joining_date__lte=todays_date())
#         student1.attendance_percentage = present_percent
#         student1.save()
#     AttendanceStudentList = student_details.objects.filter(academic_class_section__class_name=class_name, academic_class_section__section_name=section_name, is_active=True, joining_date__lte=todays_date())
#     return render(request, "attendance_percentage_list.html",
#                   {'AttendanceStudentList': AttendanceStudentList, 'ClassDetails': list(set(mapped_class)), 'SectionDetails':  list(set(mapped_section)),
#                    'class_name': class_name, 'section_name': section_name, 'YearClassSectionForm':AttendanceYearClassSectionForm, 'selected_year_id': selected_year_id,
#                        'selected_year_name': current_academic_year, 'academic_year': years,
#                        'selected_class': className, 'selected_section': sectionName})

@user_login_required
@user_permission_required('attendance.can_view_attendance_import_export', '/school/home/')
def ImportStudentAttendance(request):
    AcademicClassSection = AcademicYearClassSection
    AttendanceExcel = AttendanceExcelForm()
    user = request.user
    years = academic_year.objects.all()

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

        mapped_class = []
        mapped_section = []
        if user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                        supervisor_id=user.id):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)

        elif user.is_allowed_user():
            for mapped_object in academic_class_section_mapping.objects.filter(
                    year_name=current_academic_year):
                mapped_class.append(mapped_object.class_name)
                mapped_section.append(mapped_object.section_name)

        else:
            if not user.is_system_admin():
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=current_academic_year).filter(
                            Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
            else:
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=current_academic_year):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
    except:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        return redirect('/school/attendance_view/')

    return render(request, "import_and_export_attendance.html",
                  {'AttendanceExcel':AttendanceExcel, 'ClassDetails': list(set(mapped_class)), 'SectionDetails':  list(set(mapped_section)), 'AcademicClassSection': AcademicClassSection, 'selected_year_id': selected_year_id,
                       'selected_year_name': current_academic_year, 'academic_year': years})



def load_export_student_attendance_filter(request, val_dict_exp_att):
    from_Date = val_dict_exp_att.get('from_Date')
    class_name = val_dict_exp_att.get('class_name')
    to_Date = val_dict_exp_att.get('to_Date')
    section_name = val_dict_exp_att.get('section_name')
    academic_years = val_dict_exp_att.get('year_name')
    date_val = val_dict_exp_att.get('from_Date')
    to_date_val = val_dict_exp_att.get('to_Date')
    from_Date = time.strftime("%Y-%m-%d")
    to_Date = time.strftime("%Y-%m-%d")
    section_rec = None
    class_rec = None
    user = request.user
    msg_flag = False

    acad_year = academic_year.objects.get(id=academic_years)

    selected_class_id = class_name
    selected_class_name = class_name
    selected_section_id = section_name
    selected_section_name = section_name

    years = academic_year.objects.all()
    current_academic_year = academic_year.objects.get(current_academic_year=1)
    selected_year_id = current_academic_year.id

    mapped_class = []
    mapped_section = []
    if user.is_supervisor():
        for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
            for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(supervisor_id=user.id):
                mapped_class.append(mapped_object.class_name)
                mapped_section.append(mapped_object.section_name)

    elif user.is_allowed_user():
        for mapped_object in academic_class_section_mapping.objects.filter(
                year_name=current_academic_year):
            mapped_class.append(mapped_object.class_name)
            mapped_section.append(mapped_object.section_name)
    else:
        if not user.is_system_admin():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year).filter(Q(assistant_teacher = user.id) | Q(staff_id = user.id)):
                mapped_class.append(mapped_object.class_name)
                mapped_section.append(mapped_object.section_name)
        else:
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                mapped_class.append(mapped_object.class_name)
                mapped_section.append(mapped_object.section_name)

    if not class_name == 'All' and not section_name == 'All':
        selected_class_id = class_details.objects.get(id=class_name).id
        selected_class_name = class_details.objects.get(id=class_name)
        selected_section_id = sections.objects.get(id=section_name).id
        selected_section_name = sections.objects.get(id=section_name)
        years = academic_year.objects.all()

        try:
            current_academic_year = academic_year.objects.get(year_name=acad_year)
            selected_year_id = current_academic_year.id

            mapped_class = []
            mapped_section = []
            if user.is_supervisor():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                    for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                            supervisor_id=user.id):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)

            elif user.is_allowed_user():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                    mapped_class.append(mapped_object.class_name)
                    if mapped_object:
                        for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                            mapped_section.append(obj.section_name)

            else:
                if not user.is_system_admin():
                    for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year).filter(
                                    Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)
                else:
                    for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                        mapped_class.append(mapped_object.class_name)
                        if mapped_object:
                            for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                                mapped_section.append(obj.section_name)
        except:
            msg_flag = True
            form_vals = {}
            return form_vals, msg_flag

    raw_dict = {}

    if section_name == 'All' and class_name == 'All':
        AttendanceDetails = attendance_details.objects.filter(attendancedate__range=[from_Date, to_Date],
                                                              student__is_active=True).order_by('student__first_name')
        # students_list = student_details.objects.filter(academic_class_section__year_name=acad_year, is_active=True)

    elif class_name == 'All' and not section_name == 'All':
        selected_section_id = sections.objects.get(id=section_name).id
        selected_section_name = sections.objects.get(id=section_name)
        AttendanceDetails = attendance_details.objects.filter(attendancedate__range=[from_Date, to_Date],
                                                              student__academic_class_section__section_name=section_name,
                                                              student__is_active=True).order_by('student__first_name')

    elif not class_name == 'All' and section_name == 'All':
        selected_class_id = class_details.objects.get(id=class_name).id
        selected_class_name = class_details.objects.get(id=class_name)

        try:
            current_academic_year = academic_year.objects.get(year_name=acad_year)
            selected_year_id = current_academic_year.id

            mapped_class = []
            mapped_section = []
            if user.is_supervisor():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                    for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                            supervisor_id=user.id):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)

            elif user.is_allowed_user():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                    mapped_class.append(mapped_object.class_name)
                    if mapped_object:
                        for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                            mapped_section.append(obj.section_name)

            else:
                if not user.is_system_admin():
                    for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year).filter(
                                    Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)
                else:
                    for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                        mapped_class.append(mapped_object.class_name)
                        if mapped_object:
                            for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                                mapped_section.append(obj.section_name)
        except:
            msg_flag = True
            form_vals={}
            return form_vals, msg_flag

        AttendanceDetails = attendance_details.objects.filter(attendancedate__range=[from_Date, to_Date],
                                                              student__academic_class_section__class_name=class_name,
                                                              student__is_active=True).order_by('student__first_name')


    else:
        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
                                                                            section_name=section_name,
                                                                            class_name=class_name)
        filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj,
                                                           is_active=True, joining_date__lte=todays_date())
        AttendanceDetails = attendance_details.objects.filter(student__in=filtered_students,
                                                              attendancedate__range=[from_Date, to_Date]).order_by('student__first_name')

    student_list = []
    attendance_list = []

    for stud in AttendanceDetails:
        student_list.append(stud)

    all_class_name_obj = type('', (object,), {"id": "All", "class_name": "All"})()
    all_section_name_obj = type('', (object,), {"id": "All", "section_name": "All"})()

    mapped_class.append(all_class_name_obj)
    mapped_section.append(all_section_name_obj)

    form_vals = {
        'section_name': section_name, 'SectionDetails': list(set(mapped_section)),
        'student_list': student_list,
        'class_name': class_name, 'ClassDetails': list(set(mapped_class)),
        'selected_year_id': selected_year_id,
        'selected_year_name': current_academic_year, 'academic_year': years,
        'selected_class_id': selected_class_id, 'selected_class_name': selected_class_name,
        'selected_section_id': selected_section_id, 'selected_section_name': selected_section_name,
        'from_Date': date_val, 'to_Date': to_date_val
    }


    return form_vals, msg_flag

@user_login_required
@user_permission_required('attendance.can_view_attendance_import_export', '/school/home/')
def ExportStudentAttendance(request):
    val_dict_exp_att = request.POST
    form_vals = {}
    msg_flag = False

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
    except:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        return redirect('/school/manage_attendance/')

    if request.method == 'POST':
        request.session['val_dict_exp_att'] = val_dict_exp_att
        form_vals,msg_flag = load_export_student_attendance_filter(request, val_dict_exp_att)
    else:
        form_vals,msg_flag = load_export_student_attendance_filter(request, request.session.get('val_dict_exp_att'))

    if msg_flag:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        return redirect('/school/manage_attendance/')

    return render(request, "import_and_export_attendance.html", form_vals)

# @user_login_required
# @user_permission_required('attendance.can_view_attendance_import_export', '/school/home/')
# def ExportStudentAttendance(request):

    # from_Date = request.POST.get('from_Date')
    # class_name = request.POST.get('class_name')
    # to_Date = request.POST.get('to_Date')
    # section_name = request.POST.get('section_name')
    # academic_years = request.POST.get('year_name')
    # section_rec = None
    # class_rec = None
    # user = request.user
    #
    # acad_year = academic_year.objects.get(id=academic_years)
    #
    # selected_class_id = class_name
    # selected_class_name = class_name
    # selected_section_id = section_name
    # selected_section_name = section_name
    #
    # years = academic_year.objects.all()
    # try:
    #     current_academic_year = academic_year.objects.get(current_academic_year=1)
    #     selected_year_id = current_academic_year.id
    #
    #     mapped_class = []
    #     mapped_section = []
    #     if user.is_supervisor():
    #         for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
    #             for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(supervisor_id=user.id):
    #                 mapped_class.append(mapped_object.class_name)
    #                 mapped_section.append(mapped_object.section_name)
    #     else:
    #         if not user.is_system_admin():
    #             for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year).filter(Q(assistant_teacher = user.id) | Q(staff_id = user.id)):
    #                 mapped_class.append(mapped_object.class_name)
    #                 mapped_section.append(mapped_object.section_name)
    #         else:
    #             for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
    #                 mapped_class.append(mapped_object.class_name)
    #                 mapped_section.append(mapped_object.section_name)
    #
    # except:
    #     messages.warning(request, "Academic Year Class Section Mapping Not Found.")
    #     return redirect('/school/manage_attendance/')
    #
    #
    # if not class_name == 'All' and not section_name == 'All':
    #     selected_class_id = class_details.objects.get(id=class_name).id
    #     selected_class_name = class_details.objects.get(id=class_name)
    #     selected_section_id = sections.objects.get(id=section_name).id
    #     selected_section_name = sections.objects.get(id=section_name)
    #     years = academic_year.objects.all()
    #
    #     try:
    #         current_academic_year = academic_year.objects.get(year_name=acad_year)
    #         selected_year_id = current_academic_year.id
    #
    #         mapped_class = []
    #         mapped_section = []
    #         if user.is_supervisor():
    #             for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
    #                 for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
    #                         supervisor_id=user.id):
    #                     mapped_class.append(mapped_object.class_name)
    #                     mapped_section.append(mapped_object.section_name)
    #         else:
    #             if not user.is_system_admin():
    #                 for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year).filter(
    #                                 Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
    #                     mapped_class.append(mapped_object.class_name)
    #                     mapped_section.append(mapped_object.section_name)
    #             else:
    #                 for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
    #                     mapped_class.append(mapped_object.class_name)
    #                     if mapped_object:
    #                         for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
    #                             mapped_section.append(obj.section_name)
    #     except:
    #         messages.warning(request, "Academic Year Class Section Mapping Not Found.")
    #         return redirect('/school/manage_attendance/')
    #
    # raw_dict = {}
    #
    # if section_name == 'All' and class_name == 'All':
    #     AttendanceDetails = attendance_details.objects.filter(attendancedate__range=[from_Date, to_Date],
    #                                                           student__is_active=True)
    #     # students_list = student_details.objects.filter(academic_class_section__year_name=acad_year, is_active=True)
    #
    # elif class_name == 'All' and not section_name == 'All':
    #     selected_section_id = sections.objects.get(id=section_name).id
    #     selected_section_name = sections.objects.get(id=section_name)
    #     AttendanceDetails = attendance_details.objects.filter(attendancedate__range=[from_Date, to_Date],
    #                                                           student__academic_class_section__section_name=section_name,
    #                                                           student__is_active=True)
    #     # students_list = student_details.objects.filter(academic_class_section__year_name=acad_year, academic_class_section__section_name=section_name, is_active=True)
    #
    # elif not class_name == 'All' and section_name == 'All':
    #     selected_class_id = class_details.objects.get(id=class_name).id
    #     selected_class_name = class_details.objects.get(id=class_name)
    #
    #     try:
    #         current_academic_year = academic_year.objects.get(year_name=acad_year)
    #         selected_year_id = current_academic_year.id
    #
    #         mapped_class = []
    #         mapped_section = []
    #         if user.is_supervisor():
    #             for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
    #                 for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
    #                         supervisor_id=user.id):
    #                     mapped_class.append(mapped_object.class_name)
    #                     mapped_section.append(mapped_object.section_name)
    #         else:
    #             if not user.is_system_admin():
    #                 for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year).filter(
    #                                 Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
    #                     mapped_class.append(mapped_object.class_name)
    #                     mapped_section.append(mapped_object.section_name)
    #             else:
    #                 for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
    #                     mapped_class.append(mapped_object.class_name)
    #                     if mapped_object:
    #                         for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
    #                             mapped_section.append(obj.section_name)
    #     except:
    #         messages.warning(request, "Academic Year Class Section Mapping Not Found.")
    #         return redirect('/school/manage_attendance/')
    #
    #
    #     # students_list = student_details.objects.filter(academic_class_section__year_name=acad_year,
    #     #                                                academic_class_section__class_name=class_name,
    #     #                                                is_active=True)
    #     AttendanceDetails = attendance_details.objects.filter(attendancedate__range=[from_Date, to_Date],
    #                                                           student__academic_class_section__class_name=class_name,
    #                                                           student__is_active=True)
    #
    #
    # else:
    #
    #     # year_class_section_obj = academic_class_section_mapping.objects.get(year_name=acad_year,
    #     #                                                                     section_name=section_name,
    #     #                                                                     class_name=class_name)
    #     # students_list = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True)
    #
    #     year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
    #                                                                         section_name=section_name,
    #                                                                         class_name=class_name)
    #     filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj,
    #                                                        is_active=True, joining_date__lte=todays_date())
    #     AttendanceDetails = attendance_details.objects.filter(student__in=filtered_students,
    #                                                           attendancedate__range=[from_Date, to_Date])
    #
    # student_list = []
    # attendance_list = []
    #
    # for stud in AttendanceDetails:
    #     student_list.append(stud)
    #
    # all_class_name_obj = type('', (object,), {"id": "All", "class_name": "All"})()
    # all_section_name_obj = type('', (object,), {"id": "All", "section_name": "All"})()
    #
    # mapped_class.append(all_class_name_obj)
    # mapped_section.append(all_section_name_obj)
    #
    # return render(request, "import_and_export_attendance.html",
    #               {'section_name': section_name, 'SectionDetails': list(set(mapped_section)),
    #                'student_list': student_list,
    #                'class_name': class_name, 'ClassDetails': list(set(mapped_class)),
    #                'selected_year_id': selected_year_id,
    #                'selected_year_name': current_academic_year, 'academic_year': years,
    #                'selected_class_id': selected_class_id, 'selected_class_name': selected_class_name,
    #                'selected_section_id': selected_section_id, 'selected_section_name': selected_section_name,
    #                'from_Date':from_Date, 'to_Date':to_Date})


@user_login_required
@user_permission_required('attendance.add_attendance_details_excel', '/school/home/')
@user_permission_required('attendance.change_attendance_details_excel', '/school/home/')
def SaveImportStudentAttendance(request):
    success_flag = False
    error_flag = False
    late_flag = False
    invalid_create_flag = False
    absent_categories_flag = False
    present_categories_list = []
    present_categorie_flag = False

    if request.method == 'POST':
        attendance_details_excel = AttendanceExcelForm(request.POST, request.FILES)
        if attendance_details_excel.is_valid():
            try:
                file_recs = request.FILES['attendance_excel'].get_records()
                for file_rec in file_recs :
                    student_rec = student_details.objects.filter(student_code = file_rec['Student Id'], academic_class_section__class_name__class_name=file_rec['Class'], is_active=True, joining_date__lte=todays_date())
                    if attendance_details.objects.filter(student=student_rec, attendancedate=file_rec['Attendance Date']).exists():
                        if file_rec['Status']:
                            if file_rec['Status'] == 'Present':
                                # try:
                                #     attendance_details.objects.filter(student=student_rec, attendancedate=file_rec['Attendance Date']).update(
                                #         absent_category=None,absent_reason='', status=file_rec['Status'],present_category = present_categories.objects.get(present_category_name='Present Category')
                                #         if file_rec['Present Category'] and file_rec['Present Category'] != 'None' else None)
                                #     success_flag =True
                                # except:
                                #     late_flag = True
                                #     continue

                                if file_rec['Present Category'] != 'None' and present_categories.objects.filter(
                                        present_category_name=str(file_rec['Present Category'])).exists():
                                    pre_cat_rec = present_categories.objects.get(present_category_name=str(file_rec['Present Category']))
                                    if attendance_details.objects.filter(student=student_rec,
                                                                         attendancedate=file_rec['Attendance Date'],
                                                                         present_category=pre_cat_rec.id,
                                                                         status=file_rec['Status']):
                                        pass
                                    else:
                                        attendance_details.objects.filter(student=student_rec, attendancedate=file_rec[
                                            'Attendance Date']).update(
                                            present_category=present_categories.objects.get(present_category_name=str(file_rec['Present Category']))
                                            if file_rec['Present Category'] and file_rec[ 'Present Category'] != 'None'  else None,
                                            status=file_rec['Status'],
                                            attendance_time=(datetime.datetime.now()).time(),
                                            attendance_update_date_time=datetime.datetime.now())

                                else :
                                        if file_rec['Present Category'] == '':
                                            pass
                                        if file_rec['Present Category'] != '':
                                            present_categories_list.append(file_rec['Present Category'])
                                            present_categorie_flag = True

                            elif file_rec['Status'] == 'Absent':
                                if file_rec['Absent Category'] != 'None' and absent_categories.objects.filter(absent_category_name=str(file_rec['Absent Category'])).exists():
                                    abs_cat_rec = absent_categories.objects.get(absent_category_name=str(file_rec['Absent Category']))
                                    if attendance_details.objects.filter(student=student_rec, attendancedate=file_rec['Attendance Date'],absent_category=abs_cat_rec.id,absent_reason=file_rec['Absent Reason'],status=file_rec['Status']):
                                        pass
                                    else:
                                        attendance_details.objects.filter(student=student_rec, attendancedate=file_rec['Attendance Date']).update(
                                            absent_category=absent_categories.objects.get(absent_category_name=str(file_rec['Absent Category'])) if file_rec['Absent Category'] and
                                            file_rec['Absent Category'] != 'None'  else None, absent_reason=file_rec['Absent Reason'], status=file_rec['Status'],attendance_time=(datetime.datetime.now()).time(),attendance_update_date_time = datetime.datetime.now())
                                elif file_rec['Absent Category'] == 'None':
                                    attendance_details.objects.filter(student=student_rec, attendancedate=file_rec['Attendance Date']).update(absent_category= None,
                                    absent_reason=file_rec['Absent Reason'], status=file_rec['Status'])
                            else:
                                error_flag = True
                                continue
                        else:
                            error_flag = True
                            continue
                    else:
                        try:
                            if file_rec['Status']:
                                if file_rec['Status'] == 'Present':
                                    attendance_details.objects.create(student=student_rec[0],
                                                                      attendancedate=file_rec['Attendance Date'],status=file_rec['Status'])
                                    success_flag =True
                                elif file_rec['Status'] == 'Absent':
                                    if file_rec['Absent Category']:
                                        if absent_categories.objects.filter(absent_category_name=str(file_rec['Absent Category'])).exists():
                                            attendance_details.objects.create(student=student_rec[0], attendancedate=file_rec['Attendance Date'],
                                                absent_category__absent_category_name=absent_categories.objects.get(absent_category_name=str(file_rec['Absent Category'])),
                                                absent_reason=file_rec['Absent Reason'],status=file_rec['Status'])
                                        else:
                                            absent_categories_flag = True
                                            continue
                                    else:
                                        attendance_details.objects.create(student=student_rec[0], attendancedate=file_rec['Attendance Date'],
                                            absent_category__absent_category_name= None,
                                            absent_reason=file_rec['Absent Reason'],status=file_rec['Status'])
                                else:
                                    error_flag = True
                                    continue
                            else:
                                error_flag = True
                                continue
                        except:
                            invalid_create_flag = True
                            continue

                if error_flag:
                    messages.warning(request,'Invalid Status Entered For Some Records.')

                if success_flag:
                    messages.success(request,'Records Updated Successfully.')

                if absent_categories_flag:
                    messages.warning(request,'Invalid Absent Categories Entered.')

                if invalid_create_flag:
                    messages.warning(request,'Invalid Data To Create Attendance Records.')

                if present_categorie_flag:
                    for categories in present_categories_list:
                        messages.warning(request, str(categories) + " " + "present category dose not exist," + " " + "Please create the" + " " + str(categories) + " " + "to use it.")

                if late_flag:
                    messages.warning(request,'Please Create Late in Present Categories Before Using it.')

            except Exception as e:
                messages.warning(request,  'You are trying import invalid file format or invalid records (Details:-)'+ str(e))
                    # return HttpResponseBadRequest(json.dumps({'message': str(e)}), content_type="application/json")
    return redirect('/school/importStudentAttendance/')

# @user_login_required
# @user_permission_required('attendance.add_attendance_details_excel', '/school/home/')
# @user_permission_required('attendance.change_attendance_details_excel', '/school/home/')
# def SaveImportStudentAttendance(request):
#     if request.method == 'POST':
#         attendance_details_excel = AttendanceExcelForm(request.POST, request.FILES)
#         if attendance_details_excel.is_valid():
#             try:
#                 file_recs = request.FILES['attendance_excel'].get_records()
#                 for file_rec in file_recs :
#                     student_rec = student_details.objects.filter(id = file_rec['Id'], academic_class_section__class_name__class_name=file_rec['Class'], is_active=True, joining_date__lte=todays_date())
#                     if attendance_details.objects.filter(student=student_rec, attendancedate=file_rec['Attendance Date']).exists():
#                         attendance_details.objects.filter(student=student_rec, attendancedate=file_rec['Attendance Date']).update(
#                             absent_category=absent_categories.objects.get(absent_category_name=str(file_rec['Absent Category'])) if file_rec['Absent Category'] and file_rec['Absent Category'] != 'None'  else None,
#                             absent_reason=file_rec['Absent Reason'], status=file_rec['Status'])
#                     else:
#                         attendance_details.objects.create(student=student_rec[0],
#                                                           attendancedate=file_rec['Attendance Date'],
#                                                           absent_category__absent_category_name=absent_categories.objects.get(absent_category_name=str(file_rec['Absent Category'])) if file_rec['Absent Category'] else None,
#                                                           absent_reason=file_rec['Absent Reason'],status=file_rec['Status'])
#             except Exception as e:
#                 messages.success(request,  'You are trying import invalid file format or invalid records (Details:-)'+ str(e))
#                     # return HttpResponseBadRequest(json.dumps({'message': str(e)}), content_type="application/json")
#
#
#     return redirect('/school/importStudentAttendance/')

@user_login_required
@user_permission_required('attendance.can_view_bulk_attendance', '/school/home/')
def ManageBulkAttendance(request):
    AbsentForm = AbsentReasonForm()
    AttendanceYearClassSectionForm = AcademicYearClassSection()
    AttendanceClassSection = ClassSectionDetailsForm()
    AbsentReason1 = absent_categories.objects.all()

    user = request.user
    current_month = datetime.datetime.now().month
    current_month_name = datetime.date(1900, int(current_month), 1).strftime('%B')
    years = academic_year.objects.all()

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

        mapped_class = []
        mapped_section = []
        if user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                        supervisor_id=user.id):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)

        elif user.is_allowed_user():
            for mapped_object in academic_class_section_mapping.objects.filter(
                    year_name=current_academic_year):
                mapped_class.append(mapped_object.class_name)
                mapped_section.append(mapped_object.section_name)

        else:
            if not user.is_system_admin():
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=current_academic_year).filter(
                            Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
            else:
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=current_academic_year):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
    except:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        return redirect('/school/attendance_view/')




    absent_categories_rec = absent_categories.objects.all()
    present_categories_rec = present_categories.objects.all()

    categories = []
    for rec in absent_categories_rec:
        categories.append(rec)

    for rec in present_categories_rec:
        categories.append(rec)

    absent_category = type('', (object,), {"absent_category_name": "absent", "absent_category_code": "absent",
                                           "color_code": "#ff0000"})()
    present_category = type('', (object,), {"present_category_name": "present", "present_category_code": "present",
                                            "color_code": "#00ff00"})()
    na = type('', (object,),
              {"present_category_name": "delete", "present_category_code": "N/A", "color_code": "#ffffff"})()

    # categories.append(na)
    categories.append(absent_category)
    categories.append(present_category)

    return render(request, "bulk_attendance_dashboard.html",
                  {'AbsentForm': AbsentForm, 'SectionDetails': list(set(mapped_section)), 'ClassSection': AttendanceYearClassSectionForm,
                    'ClassDetails': list(set(mapped_class)), 'AbsentReason': AbsentReason1,  'selected_year_id': selected_year_id,
                       'selected_year_name': current_academic_year, 'academic_year': years, 'selected_month':current_month, 'selected_month_name': current_month_name, 'categories': categories})

# import time
# from datetime import datetime, date , tzinfo, timedelta, time
# @user_login_required
# def ViewBulkAttendance(request):
#     AbsentForm = AbsentReasonForm()
#     AttendanceYearClassSectionForm = AcademicYearClassSection()
#     AbsentReason1 = absent_categories.objects.all()
#     AttendanceClassSection = ClassSectionDetailsForm()
#     modelDict = []
#     given_month = int(request.POST.get('month'))
#     given_year = int(request.POST.get('year_name'))
#     class_name = request.POST.get('class_name')
#     section_name = request.POST.get('section_name')
#     user = request.user
#
#     now = datetime.datetime.now()
#     current_month = int(now.strftime("%m"))#
#     today = int(now.strftime("%d"))#now.strftime("%Y-%m-%d")
#
#     selected_month_name = datetime.date(1900, given_month, 1).strftime('%B')
#     acad_year = academic_year.objects.get(id=given_year)
#     numbers_of_days = int(monthrange(int(acad_year.start_date.year), given_month)[1])
#     # days = [date(int(acad_year.start_date.year), given_month, day) for day in range(1, numbers_of_days + 1)]
#
#     selected_class_id = class_name
#     selected_class_name = class_name
#     selected_section_id = section_name
#     selected_section_name = section_name
#
#     student_ids_list =[]
#
#     years = academic_year.objects.all()
#     try:
#         current_academic_year = academic_year.objects.get(current_academic_year=1)
#         selected_year_id = current_academic_year.id
#
#         ay_start_month = current_academic_year.start_date.month
#         ay_end_month = current_academic_year.end_date.month
#
#         flag = True
#         counter = 0
#         count = ay_start_month
#         month_list_id = []
#         month_recs = []
#         month_dict ={}
#         last_day = monthrange(current_academic_year.start_date.year, count)[1]
#         # month_dict[ay_start_month] = str(current_academic_year.start_date)
#         month_dict[ay_start_month] = datetime.date(current_academic_year.start_date.year, count, last_day).strftime( "%Y-%m-%d")
#         month_list_id.append(month_dict)
#
#         sp_flag = False
#         while flag:
#             if count == ay_end_month:
#                 flag = False
#                 # print 'gg'
#             else:
#                 if count != 12:
#                     if sp_flag == False:
#                         # month_dict = {}
#                         count = count + 1
#                         if count == now.month:
#                             cur_month = datetime.date(current_academic_year.start_date.year, count, now.day).strftime( "%Y-%m-%d")
#                         else:
#                             last_day = monthrange(current_academic_year.start_date.year, count)[1]
#                             cur_month = datetime.date(current_academic_year.start_date.year, count, last_day).strftime( "%Y-%m-%d")
#                         month_dict[count] = cur_month
#                         # cur_month = datetime.date(current_academic_year.start_date.year, count, 1).strftime("%Y-%m-%d")
#                         # month_dict[count] = cur_month
#                         # month_list_id.append(month_dict)
#                     else:
#
#                         # month_dict = {}
#                         count = count + 1
#                         if count == now.month:
#                             cur_month = datetime.date(current_academic_year.end_date.year, count, now.day).strftime("%Y-%m-%d")
#                         else:
#                             last_day = monthrange(current_academic_year.end_date.year, count)[1]
#                             cur_month = datetime.date(current_academic_year.end_date.year, count, last_day).strftime("%Y-%m-%d")
#
#
#                         month_dict[count] = cur_month
#                         # month_list_id.append(month_dict)
#                 else:
#                     sp_flag = True
#                     # month_dict = {}
#                     count = 1
#                     if count == now.month:
#                         cur_month = datetime.date(current_academic_year.end_date.year, count, now.day).strftime("%Y-%m-%d")
#                     else:
#                         last_day = monthrange(current_academic_year.end_date.year, count)[1]
#                         cur_month = datetime.date(current_academic_year.end_date.year, count, last_day).strftime("%Y-%m-%d")
#                     month_dict[count] = cur_month
#                     # month_list_id.append(month_dict)
#
#                     # month_list_id.append(count)
#         selected_month_rec = month_dict[given_month]
#         current_month_rec = month_dict[current_month]
#         date_formate = datetime.date(int(selected_month_rec.split('-')[0]),int(selected_month_rec.split('-')[1]),int(selected_month_rec.split('-')[2]))
#         current_month_date= datetime.date(int(current_month_rec.split('-')[0]),int(current_month_rec.split('-')[1]),int(current_month_rec.split('-')[2]))
#         # date_formate = datetime.datetime.strptime(selected_month_rec, '%Y-%m-%d')
#
#         mapped_class = []
#         mapped_section = []
#         if user.is_supervisor():
#             for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
#                 for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(supervisor_id=user.id):
#                     mapped_class.append(mapped_object.class_name)
#                     mapped_section.append(mapped_object.section_name)
#         else:
#             if not user.is_system_admin():
#                 for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year).filter(Q(assistant_teacher = user.id) | Q(staff_id = user.id)):
#                     mapped_class.append(mapped_object.class_name)
#                     mapped_section.append(mapped_object.section_name)
#             else:
#                 for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
#                     mapped_class.append(mapped_object.class_name)
#                     mapped_section.append(mapped_object.section_name)
#
#     except:
#         messages.warning(request, "Academic Year Class Section Mapping Not Found.")
#         return redirect('/school/manageBulkAttendance/')
#
#
#     if not class_name == 'All' and not section_name == 'All':
#         selected_class_id = class_details.objects.get(id=class_name).id
#         selected_class_name = class_details.objects.get(id=class_name)
#         selected_section_id = sections.objects.get(id=section_name).id
#         selected_section_name = sections.objects.get(id=section_name)
#         years = academic_year.objects.all()
#
#         try:
#             current_academic_year = academic_year.objects.get(year_name=acad_year)
#             selected_year_id = current_academic_year.id
#
#             mapped_class = []
#             mapped_section = []
#             if user.is_supervisor():
#                 for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
#                     for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
#                             supervisor_id=user.id):
#                         mapped_class.append(mapped_object.class_name)
#                         mapped_section.append(mapped_object.section_name)
#             else:
#                 if not user.is_system_admin():
#                     for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year).filter(
#                                     Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
#                         mapped_class.append(mapped_object.class_name)
#                         mapped_section.append(mapped_object.section_name)
#                 else:
#                     for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
#                         mapped_class.append(mapped_object.class_name)
#                         if mapped_object:
#                             for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
#                                 mapped_section.append(obj.section_name)
#         except:
#             messages.warning(request, "Academic Year Class Section Mapping Not Found.")
#             return redirect('/school/manageBulkAttendance/')
#
#     raw_dict = {}
#
#     if section_name == 'All' and class_name == 'All':
#         students_list = student_details.objects.filter(academic_class_section__year_name=acad_year, is_active=True, joining_date__lte=selected_month_rec)
#         students_ids = students_list.values_list('id')
#     elif class_name == 'All' and not section_name == 'All':
#         selected_section_id = sections.objects.get(id=section_name).id
#         selected_section_name = sections.objects.get(id=section_name)
#         students_list = student_details.objects.filter(academic_class_section__year_name=acad_year, academic_class_section__section_name=section_name, is_active=True, joining_date__lte=selected_month_rec)
#         students_ids = students_list.values_list('id')
#
#     elif not class_name == 'All' and section_name == 'All':
#         selected_class_id = class_details.objects.get(id=class_name).id
#         selected_class_name = class_details.objects.get(id=class_name)
#         students_list = student_details.objects.filter(academic_class_section__year_name=acad_year,
#                                                        academic_class_section__class_name=class_name,
#                                                        is_active=True, joining_date__lte=selected_month_rec)
#         students_ids = students_list.values_list('id')
#
#
#     else:
#
#         year_class_section_obj = academic_class_section_mapping.objects.get(year_name=acad_year,
#                                                                             section_name=section_name,
#                                                                             class_name=class_name)
#         students_list = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True, joining_date__lte=selected_month_rec)
#         students_ids = students_list.values_list('id')
#
#     days = [date(int(date_formate.year), given_month, day) for day in range(1, numbers_of_days + 1)]
#     for stud in students_list:
#         if stud:
#             raw_dict1 = {}
#             raw_dict1['student'] = stud.to_dict()
#             # modelDict.append(raw_dict1)
#             raw_data = []
#             for day in days:
#                 attendance = attendance_details.objects.filter(student=stud, attendancedate=day)
#                 raw_dict = {}
#                 if attendance:
#                     raw_dict['attendance'] = RawDict(attendance[0].to_dict())
#                 else:
#                     raw_dict['attendance'] = None
#                 # raw_dict1['student'].append(raw_dict)
#                 raw_data.append(raw_dict)
#             raw_dict1['student']['bulk_attendance'] = [data for data in raw_data]
#             modelDict.append(raw_dict1)
#
#     all_class_name_obj = type('', (object,), {"id": "All", "class_name": "All"})()
#     all_section_name_obj = type('', (object,), {"id": "All", "section_name": "All"})()
#
#     mapped_class.append(all_class_name_obj)
#     mapped_section.append(all_section_name_obj)
#
#     if students_ids:
#         for rec_id in students_ids:
#             student_ids_list.append(int(rec_id[0]))
#
#     return render(request, "bulk_attendance_dashboard.html",
#                   {'AbsentForm': AbsentForm, 'AbsentReason': AbsentReason1, "today": today,'selected_month_rec':date_formate, 'given_month':given_month, 'current_month': current_month,
#                    'section_name': section_name, 'SectionDetails': list(set(mapped_section)), 'attendanceList': modelDict,
#                    'studentList': students_list,'students_ids':student_ids_list, 'ClassSection': AttendanceYearClassSectionForm,'current_month_date':current_month_date,
#                    'class_name': class_name, 'ClassDetails': list(set(mapped_class)), 'date_list': days,  'selected_year_id': selected_year_id,
#                        'selected_year_name': current_academic_year, 'academic_year': years, 'selected_class_id': selected_class_id, 'selected_class_name': selected_class_name, 'selected_section_id': selected_section_id, 'selected_section_name': selected_section_name, 'selected_month':given_month, 'selected_month_name': selected_month_name})


def load_bulk_bulk_att(val_dict_bulk_attendance, request):

    error_flag = False

    AbsentForm = AbsentReasonForm()
    AttendanceYearClassSectionForm = AcademicYearClassSection()
    AbsentReason1 = absent_categories.objects.all()
    AttendanceClassSection = ClassSectionDetailsForm()
    modelDict = []
    given_month = int(val_dict_bulk_attendance.get('month'))
    given_year = int(val_dict_bulk_attendance.get('year_name'))
    class_name = val_dict_bulk_attendance.get('class_name')
    section_name = val_dict_bulk_attendance.get('section_name')
    user = request.user

    now = datetime.datetime.now()
    current_month = int(now.strftime("%m"))  #
    today = int(now.strftime("%d"))  # now.strftime("%Y-%m-%d")

    selected_month_name = datetime.date(1900, given_month, 1).strftime('%B')
    acad_year = academic_year.objects.get(id=given_year)
    numbers_of_days = int(monthrange(int(acad_year.start_date.year), given_month)[1])
    # days = [date(int(acad_year.start_date.year), given_month, day) for day in range(1, numbers_of_days + 1)]

    selected_class_id = class_name
    selected_class_name = class_name
    selected_section_id = section_name
    selected_section_name = section_name

    student_ids_list = []

    years = academic_year.objects.all()
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

        ay_start_month = current_academic_year.start_date.month
        ay_end_month = current_academic_year.end_date.month

        flag = True
        counter = 0
        count = ay_start_month
        month_list_id = []
        month_recs = []
        month_dict = {}
        last_day = monthrange(current_academic_year.start_date.year, count)[1]
        # month_dict[ay_start_month] = str(current_academic_year.start_date)
        month_dict[ay_start_month] = datetime.date(current_academic_year.start_date.year, count, last_day).strftime(
            "%Y-%m-%d")
        month_list_id.append(month_dict)

        sp_flag = False
        while flag:
            if count == ay_end_month:
                flag = False
                # print 'gg'
            else:
                if count != 12:
                    if sp_flag == False:
                        # month_dict = {}
                        count = count + 1
                        if count == now.month:
                            cur_month = datetime.date(current_academic_year.start_date.year, count, now.day).strftime(
                                "%Y-%m-%d")
                        else:
                            last_day = monthrange(current_academic_year.start_date.year, count)[1]
                            cur_month = datetime.date(current_academic_year.start_date.year, count, last_day).strftime(
                                "%Y-%m-%d")
                        month_dict[count] = cur_month
                        # cur_month = datetime.date(current_academic_year.start_date.year, count, 1).strftime("%Y-%m-%d")
                        # month_dict[count] = cur_month
                        # month_list_id.append(month_dict)
                    else:

                        # month_dict = {}
                        count = count + 1
                        if count == now.month:
                            cur_month = datetime.date(current_academic_year.end_date.year, count, now.day).strftime(
                                "%Y-%m-%d")
                        else:
                            last_day = monthrange(current_academic_year.end_date.year, count)[1]
                            cur_month = datetime.date(current_academic_year.end_date.year, count, last_day).strftime(
                                "%Y-%m-%d")

                        month_dict[count] = cur_month
                        # month_list_id.append(month_dict)
                else:
                    sp_flag = True
                    # month_dict = {}
                    count = 1
                    if count == now.month:
                        cur_month = datetime.date(current_academic_year.end_date.year, count, now.day).strftime(
                            "%Y-%m-%d")
                    else:
                        last_day = monthrange(current_academic_year.end_date.year, count)[1]
                        cur_month = datetime.date(current_academic_year.end_date.year, count, last_day).strftime(
                            "%Y-%m-%d")
                    month_dict[count] = cur_month
                    # month_list_id.append(month_dict)

                    # month_list_id.append(count)
        selected_month_rec = month_dict[given_month]
        current_month_rec = month_dict[current_month]
        date_formate = datetime.date(int(selected_month_rec.split('-')[0]), int(selected_month_rec.split('-')[1]),
                                     int(selected_month_rec.split('-')[2]))
        current_month_date = datetime.date(int(current_month_rec.split('-')[0]), int(current_month_rec.split('-')[1]),
                                           int(current_month_rec.split('-')[2]))
        # date_formate = datetime.datetime.strptime(selected_month_rec, '%Y-%m-%d')

        mapped_class = []
        mapped_section = []
        if user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(supervisor_id=user.id):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)

        elif user.is_allowed_user():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                mapped_class.append(mapped_object.class_name)
                mapped_section.append(mapped_object.section_name)

        else:
            if not user.is_system_admin():
                for mapped_object in academic_class_section_mapping.objects.filter(
                        year_name=current_academic_year).filter(Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
            else:
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)

    except:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        error_flag = True
        form_vals ={}
        return form_vals,error_flag

    if not class_name == 'All' and not section_name == 'All':
        selected_class_id = class_details.objects.get(id=class_name).id
        selected_class_name = class_details.objects.get(id=class_name)
        selected_section_id = sections.objects.get(id=section_name).id
        selected_section_name = sections.objects.get(id=section_name)
        years = academic_year.objects.all()

        try:
            current_academic_year = academic_year.objects.get(year_name=acad_year)
            selected_year_id = current_academic_year.id

            mapped_class = []
            mapped_section = []
            if user.is_supervisor():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                    for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                            supervisor_id=user.id):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)
            elif user.is_allowed_user():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                    mapped_class.append(mapped_object.class_name)
                    if mapped_object:
                        for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                            mapped_section.append(obj.section_name)

            else:
                if not user.is_system_admin():
                    for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year).filter(
                                    Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)
                else:
                    for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                        mapped_class.append(mapped_object.class_name)
                        if mapped_object:
                            for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                                mapped_section.append(obj.section_name)
        except:
            messages.warning(request, "Academic Year Class Section Mapping Not Found.")
            error_flag = True
            form_vals = {}
            return form_vals, error_flag


    raw_dict = {}

    if section_name == 'All' and class_name == 'All':
        students_list = student_details.objects.filter(academic_class_section__year_name=acad_year, is_active=True,
                                                       joining_date__lte=selected_month_rec).order_by('first_name')
        students_ids = students_list.values_list('id')
    elif class_name == 'All' and not section_name == 'All':
        selected_section_id = sections.objects.get(id=section_name).id
        selected_section_name = sections.objects.get(id=section_name)
        students_list = student_details.objects.filter(academic_class_section__year_name=acad_year,
                                                       academic_class_section__section_name=section_name,
                                                       is_active=True, joining_date__lte=selected_month_rec).order_by('first_name')
        students_ids = students_list.values_list('id')

    elif not class_name == 'All' and section_name == 'All':
        selected_class_id = class_details.objects.get(id=class_name).id
        selected_class_name = class_details.objects.get(id=class_name)
        students_list = student_details.objects.filter(academic_class_section__year_name=acad_year,
                                                       academic_class_section__class_name=class_name,
                                                       is_active=True, joining_date__lte=selected_month_rec).order_by('first_name')
        students_ids = students_list.values_list('id')


    else:

        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=acad_year,
                                                                            section_name=section_name,
                                                                            class_name=class_name)
        students_list = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True,
                                                       joining_date__lte=selected_month_rec).order_by('first_name')
        students_ids = students_list.values_list('id')

    days = [date(int(date_formate.year), given_month, day) for day in range(1, numbers_of_days + 1)]
    for stud in students_list:
        if stud:
            raw_dict1 = {}
            raw_dict1['student'] = stud.to_dict()
            # modelDict.append(raw_dict1)
            raw_data = []
            for day in days:
                attendance = attendance_details.objects.filter(student=stud, attendancedate=day)
                raw_dict = {}
                if attendance:
                    raw_dict['attendance'] = RawDict(attendance[0].to_dict())
                else:
                    raw_dict['attendance'] = None
                # raw_dict1['student'].append(raw_dict)
                raw_data.append(raw_dict)
            raw_dict1['student']['bulk_attendance'] = [data for data in raw_data]
            modelDict.append(raw_dict1)

    all_class_name_obj = type('', (object,), {"id": "All", "class_name": "All"})()
    all_section_name_obj = type('', (object,), {"id": "All", "section_name": "All"})()

    mapped_class.append(all_class_name_obj)
    mapped_section.append(all_section_name_obj)

    if students_ids:
        for rec_id in students_ids:
            student_ids_list.append(int(rec_id[0]))

    absent_categories_rec = absent_categories.objects.all()
    present_categories_rec = present_categories.objects.all()

    categories = []
    for rec in absent_categories_rec:
        categories.append(rec)

    for rec in present_categories_rec:
        categories.append(rec)

    absent_category = type('', (object,), {"absent_category_name": "absent", "absent_category_code": "absent", "color_code": "#ff0000"})()
    present_category = type('', (object,), {"present_category_name": "present", "present_category_code": "present", "color_code": "#00ff00"})()
    na = type('', (object,), {"present_category_name": "delete", "present_category_code": "N/A", "color_code": "#ffffff"})()

    # categories.append(na)
    categories.append(absent_category)
    categories.append(present_category)

    form_vals = {'AbsentForm': AbsentForm, 'AbsentReason': AbsentReason1, "today": today,
                   'selected_month_rec': date_formate, 'given_month': given_month, 'current_month': current_month,
                   'section_name': section_name, 'SectionDetails': list(set(mapped_section)),
                   'attendanceList': modelDict,
                   'studentList': students_list, 'students_ids': student_ids_list,
                   'ClassSection': AttendanceYearClassSectionForm, 'current_month_date': current_month_date,
                   'class_name': class_name, 'ClassDetails': list(set(mapped_class)), 'date_list': days,
                   'selected_year_id': selected_year_id,
                   'selected_year_name': current_academic_year, 'academic_year': years,
                   'selected_class_id': selected_class_id, 'selected_class_name': selected_class_name,
                   'selected_section_id': selected_section_id, 'selected_section_name': selected_section_name,
                   'selected_month': given_month, 'selected_month_name': selected_month_name, 'categories': categories}
    return form_vals, error_flag

@user_login_required
def ViewBulkAttendance(request):
    val_dict_bulk_attendance = request.POST
    error_flag = False

    if request.method == 'POST':
        request.session['val_dict_bulk_attendance'] = val_dict_bulk_attendance
        form_vals,error_flag = load_bulk_bulk_att(val_dict_bulk_attendance, request)
    else:
        form_vals,error_flag = load_bulk_bulk_att(request.session.get('val_dict_bulk_attendance'), request)

    if error_flag:
        return redirect('/school/manageBulkAttendance/')

    return render(request, "bulk_attendance_dashboard.html", form_vals)

def GetCategoryColor(request):
    print request
    absent_categories_rec = absent_categories.objects.filter(absent_category_name=(request.GET['category']))
    present_categories_rec = present_categories.objects.filter(present_category_name=(request.GET['category']))

    categories = []
    for rec in absent_categories_rec:
        categories.append(rec.to_dict())

    for rec in present_categories_rec:
        categories.append(rec.to_dict())

    # absent_category = type('', (object,), {"absent_category_name": "absent", "absent_category_code": "absent",
    #                                        "color_code": "#ff0000"})()
    if request.GET['category'] == 'absent':
        absent_category = {"absent_category_name": "absent", "absent_category_code": "absent",
                                           "color_code": "#ff0000", 'status': 'Absent'}
        categories.append(absent_category)

    # present_category = type('', (object,), {"present_category_name": "present", "present_category_code": "present",
    #                                         "color_code": "#00ff00"})()
    if request.GET['category'] == 'present':
        present_category = {"present_category_name": "present", "present_category_code": "present",
                                            "color_code": "green", 'status':'Present'}
        categories.append(present_category)


    return HttpResponse(json.dumps({'categories': categories}), content_type="application/json")

# @user_login_required
# @user_permission_required('attendance.change_attendance_details', '/school/home/')
# def UpdateBulkAttendance(request):
#
#     if request.is_ajax():
#         if request.method == 'POST':
#             val_dict = json.loads(request.POST['table'])
#             model_dict = []
#             for obj in val_dict:
#                 for i in range(31):
#                     try:
#                         attendance_date = str(obj['attendance_date_'+str(i+1)])
#                         student_status = str(obj['student_'+str(i+1)])
#                         # print "Date: ",attendance_date, " Status: ",student_status
#                         if student_status == 'Absent':
#
#                             if attendance_date:
#                                 updated_objects = attendance_details.objects.filter(student__id=int(obj['studentId']),
#                                                             attendancedate=attendance_date).update(attendancedate=attendance_date,
#                                 status=student_status,late=False)
#                             model_dict.append(updated_objects)
#                         else:
#                             if attendance_date:
#                                 updated_objects = attendance_details.objects.filter(student__id=int(obj['studentId']),
#                                                             attendancedate=attendance_date).update(attendancedate=attendance_date,
#                                 status=student_status, absent_reason='', absent_category=None)
#                         # print attendance_date
#                             model_dict.append(updated_objects)
#                         continue
#                     except:
#                         continue
#                 # if attendance_date:
#                 #     AttendanceDetail.objects.filter(student__id=int(obj['studentId']), attendancedate=str(obj['attendance_date'])).update(
#                 #         status=str(obj['student'])
#                 #     )
#             messages.success(request, "Attendance updated successfully.")
#             # return redirect('/school/manageBulkAttendance/')
#             return JsonResponse(model_dict, safe=False)

@user_login_required
def ExportBulkAttendance(request):
    if request.method == 'POST':
        # val_dict = json.loads(request.POST)

        month_dates = request.POST.getlist('attendanceDate')
        student_ids = request.POST.getlist('list_exp')
        class_name = str(request.POST.get('class_name'))
        section_name = str(request.POST.get('section_name'))
        acad_year = request.POST.get('academic_year')
        all_students_ids = json.loads(request.POST.getlist('students_ids')[0])
        month_date = None

        if class_name != "All":
            class_name_export = class_details.objects.get(id=class_name)
        else:
            class_name_export = "All"
        if section_name != "All":
            section_name_export = sections.objects.get(id=section_name)
        else:
            section_name_export = "All"

        file_name = "Data_" + str(class_name_export) + "_" + str(section_name_export)

        # academic_years = academic_year.objects.get(current_academic_year=True)

        studentIDS = []
        if student_ids:
            for std in student_ids[0].split(','):
                studentIDS.append(std)

        if not studentIDS[0]:
            studentIDS = []
            for rec in all_students_ids:
                studentIDS.append(int(rec))

        for date in month_dates:
            if date:
                month_date = date

        if not month_date:
                messages.success(request, "No attendance record found!")
                return redirect('/school/manageBulkAttendance/')

        given_month = int(month_date.split('-')[1])
        given_year = int(month_date.split('-')[0])
        numbers_of_days = int(monthrange(given_year, given_month)[1])

        from_Date = str(given_year)+'-'+str(given_month)+'-1'
        to_Date = str(given_year) + '-' + str(given_month) + '-'+str(numbers_of_days)

        if not studentIDS[0]:
            AttendanceDetails = attendance_details.objects.filter(attendancedate__range=[from_Date, to_Date])

        filtered_students = []
        for stud_id in studentIDS:
            if stud_id:
                filtered_students.append(student_details.objects.get(id=stud_id, is_active=True, joining_date__lte=todays_date()))
                AttendanceDetails = attendance_details.objects.filter(student__in=filtered_students, attendancedate__range=[from_Date, to_Date])

        rows = []
        # rows = AttendanceDetails.values_list('student__id', 'student__get_full_name','student__student_code', 'student__odoo_id', 'student__academic_class_section__class_name__class_name',
        #                     'student__academic_class_section__section_name__section_name', 'status',
        #                     'attendancedate', 'present_category__present_category_name', 'absent_category__absent_category_name')


        for attendance_rec in AttendanceDetails:
            attendance_list = []
            attendance_list.append(attendance_rec.student.id)
            attendance_list.append(attendance_rec.student.get_all_name())
            attendance_list.append(attendance_rec.student.student_code)
            attendance_list.append(attendance_rec.student.odoo_id)
            attendance_list.append(attendance_rec.student.academic_class_section.class_name.class_name)
            attendance_list.append(attendance_rec.student.academic_class_section.section_name.section_name)
            attendance_list.append(attendance_rec.status)
            attendance_list.append(attendance_rec.attendancedate)
            if attendance_rec.present_category:
                attendance_list.append(attendance_rec.present_category.present_category_name)
            else:
                attendance_list.append('')

            if attendance_rec.absent_category:
                attendance_list.append(attendance_rec.absent_category.absent_category_name)
            else:
                attendance_list.append('')

            rows.append(attendance_list)
            # attendance_list.append()

        column_names = ['Id', 'Student Name','Student Id','Scodoo Id', 'Class','Section', 'Status','Attendance Date', 'Present Category', 'Absent Category']
        try:

            return export_users_xls(file_name, column_names, rows)

        except Exception, e:
            return HttpResponseBadRequest(json.dumps({'message': str(e)}), content_type="application/json")
        # else:
        #     return HttpResponseBadRequest("Please select student record!")
@user_login_required
def GetCurrentAcademicYear(request):
    if request.method == 'GET':
        model_dict = []
        all_years = academic_year.objects.all()

        for year in all_years:
            model_dict.append(year.to_dict())

        return JsonResponse(model_dict, safe=False)


@user_login_required
@user_permission_required('attendance.change_attendance_details', '/school/home/')
def UpdateBulkAttendance(request):

    if request.is_ajax():
        if request.method == 'POST':
            val_dict = json.loads(request.POST['table'])
            now = datetime.datetime.now()
            model_dict = []
            if val_dict:
                for obj in val_dict:
                    student_id = obj['student_id']
                    attendance_date = obj['attendance_date']
                    update_date= int(attendance_date.split('-')[2])
                    updated_year_rec = current_acd_month_rec(int(attendance_date.split('-')[1])).split('-')
                    attendance_date = str(updated_year_rec[0])+'-'+str(updated_year_rec[1])+'-'+str(update_date)

                    student_status = obj['status']

                    if student_status == 'Absent':

                        if attendance_date:
                            try:
                                absent_category_rec = absent_categories.objects.get(absent_category_name=str(obj['category'])) if 'category' in obj and obj['category'] else None
                            except:
                                absent_category_rec = None

                            updated_objects = attendance_details.objects.filter(student__id=int(obj['student_id']),
                                                        attendancedate=attendance_date).update(attendancedate=attendance_date,
                            status=student_status,late=False,absent_category=absent_category_rec, present_category=None, absent_reason='')

                            if not updated_objects:
                                attendance_details.objects.create(
                                    student=student_details.objects.get(id=int(obj['student_id'])),
                                    status=student_status, absent_category=absent_category_rec, present_category=None,
                                    attendancedate=attendance_date,
                                    absent_reason='',
                                    attendance_time=(datetime.datetime.now() + datetime.timedelta(minutes=330)).time())


                        model_dict.append(updated_objects)


                    # if student_status == 'Medical':
                    #
                    #     if attendance_date:
                    #         updated_objects = attendance_details.objects.filter(student__id=int(obj['student_id']),
                    #                                     attendancedate=attendance_date).update(attendancedate=attendance_date,present_category=None,
                    #         status='Absent', absent_category=absent_categories.objects.get(absent_category_name=student_status),absent_reason='',attendance_update_date_time =datetime.datetime.now())
                    #
                    #         if not updated_objects:
                    #             attendance_details.objects.create(
                    #                 student=student_details.objects.get(id=int(obj['student_id'])),
                    #                 status='Absent', absent_category=absent_categories.objects.get(absent_category_name=student_status), present_category=None,
                    #                 attendancedate=attendance_date,
                    #                 absent_reason='',
                    #                 attendance_time=(datetime.datetime.now() + datetime.timedelta(minutes=330)).time())
                    #
                    #     model_dict.append(updated_objects)
                    #
                    # if student_status == 'Excused':
                    #
                    #     if attendance_date:
                    #         updated_objects = attendance_details.objects.filter(student__id=int(obj['student_id']),
                    #                                     attendancedate=attendance_date).update(attendancedate=attendance_date,present_category=None,
                    #         status='Absent', absent_category=absent_categories.objects.get(absent_category_name=student_status),absent_reason='',attendance_update_date_time =datetime.datetime.now())
                    #
                    #         if not updated_objects:
                    #             attendance_details.objects.create(
                    #                 student=student_details.objects.get(id=int(obj['student_id'])),
                    #                 status='Absent', absent_category=absent_categories.objects.get(absent_category_name=student_status), present_category=None,
                    #                 attendancedate=attendance_date,
                    #                 absent_reason='',
                    #                 attendance_time=(datetime.datetime.now() + datetime.timedelta(minutes=330)).time())
                    #
                    #     model_dict.append(updated_objects)

                    if student_status == 'Present':
                        try:
                            present_category_rec = present_categories.objects.get(
                            present_category_name=str(obj['category'])) if 'category' in obj and obj['category'] else None
                        except:
                            present_category_rec = None
                        if attendance_date:
                            updated_objects = attendance_details.objects.filter(student__id=int(obj['student_id']),
                                                        attendancedate=attendance_date).update(attendancedate=attendance_date,
                            status=student_status, absent_reason='', absent_category=None,present_category=present_category_rec,attendance_update_date_time =datetime.datetime.now())

                            if not updated_objects:
                                attendance_details.objects.create(student=student_details.objects.get(id=int(obj['student_id'])),
                                    status=student_status,absent_category=None, present_category=present_category_rec, attendancedate=attendance_date,
                                    absent_reason='',attendance_time=(datetime.datetime.now() + datetime.timedelta(minutes=330)).time())

                        model_dict.append(updated_objects)
                    #
                    # if student_status == 'Late':
                    #     if attendance_date:
                    #         updated_objects = attendance_details.objects.filter(student__id=int(obj['student_id']),
                    #                                     attendancedate=attendance_date).update(attendancedate=attendance_date,status='Present',
                    #                                     absent_reason='', absent_category=None, present_category=present_categories.objects.get(present_category_name=student_status),attendance_update_date_time =datetime.datetime.now())
                    #
                    #         if not updated_objects:
                    #             attendance_details.objects.create(
                    #                 student=student_details.objects.get(id=int(obj['student_id'])),
                    #                 status='Present', absent_category=None, present_category=present_categories.objects.get(present_category_name=student_status),
                    #                 attendancedate=attendance_date,
                    #                 absent_reason='', attendance_time=(
                    #                 datetime.datetime.now() + datetime.timedelta(minutes=330)).time())
                    #
                    #     model_dict.append(updated_objects)


                    if student_status == 'N/A':
                        empty=[]
                        if attendance_date:
                            updated_objects = attendance_details.objects.filter(student__id=int(obj['student_id']),
                                                        attendancedate=attendance_date)
                            updated_objects.delete()

                        model_dict.append(empty)

                messages.success(request, "Attendance updated successfully.")
            else:
                messages.success(request, "Attendance record not found.")
            # return redirect('/school/manageBulkAttendance/')
            return JsonResponse(model_dict, safe=False)


@user_login_required
@user_permission_required('attendance.can_view_view_present_categories', '/school/home/')
def PresentCategoryDetail(request):
    categorylist = present_categories.objects.all()
    return render(request, "present_categories_details.html", {'categorylist': categorylist})


@user_login_required
@user_permission_required('attendance.can_view_add_present_categories', '/school/home/')
def AddPresentCategories(request):
    PresentreasonForm = PresentReasonForm()
    return render(request, "add_presentreason.html" ,{'PresentreasonForm':PresentreasonForm})


@user_login_required
@user_permission_required('attendance.add_present_categories', '/school/home/')
def SavePresentCategories(request):
    if request.method == 'POST':
        present_reason = PresentReasonForm(request.POST)
        if present_reason.is_valid():
            if present_categories.objects.filter(present_category_name=request.POST['present_category_name']).exists() or present_categories.objects.filter(present_category_code= request.POST['present_category_code']).exists():
                messages.success(request, "Duplicate record can't be added.")
            else:
                # present_reason.save()
                present_categories.objects.create(
                    present_category_name=present_reason.cleaned_data['present_category_name'].lower(),
                    present_category_code=present_reason.cleaned_data['present_category_code'].lower(),
                    color_code=present_reason.cleaned_data['color_code'],short_code=present_reason.cleaned_data['short_code'])

    return redirect('/school/present_category_detail/')


@user_login_required
@user_permission_required('attendance.can_view_edit_present_categories', '/school/home/')
def UpdatePresentCategory(request, present_cateory_id):
    categorylist = present_categories.objects.get(pk=present_cateory_id)
    return render(request, "update_present_category.html", {'categorylist': categorylist})

@user_login_required
@user_permission_required('attendance.change_present_categories', '/school/home/')
def SavePresentUpdatedCategory(request):
    cateory_id = request.POST['category_id']
    category_obj = present_categories.objects.get(id=cateory_id)
    if (request.method == 'POST'):
        if present_categories.objects.filter(~Q(id = cateory_id),present_category_name = request.POST['present_category_name']).exists():
            messages.success(request, "Duplicate record can't be added.")
        elif present_categories.objects.filter(~Q(id = cateory_id),present_category_code = request.POST['present_category_code']).exists():
            messages.success(request, "Duplicate record can't be added.")
        else:
            reason_id = request.POST['category_id']
            obj = present_categories.objects.get(pk=reason_id)
            obj.present_category_code = (request.POST['present_category_code']).lower()
            obj.present_category_name = (request.POST['present_category_name']).lower()
            obj.color_code = request.POST['color_code']
            obj.short_code = request.POST['short_code']
            obj.save()
            messages.success(request, "One record updated successfully")
    return redirect('/school/present_category_detail/')


@user_login_required
@user_permission_required('attendance.delete_present_categories', '/school/home/')
def DeletePresentCategory(request, category_id):
    try:

        present_categories.objects.filter(id=category_id).delete()
        messages.success(request, "Record deleted successfully")
    except:
        messages.success(request, "Cant delete present category relation exists somewhere.")

    return redirect('/school/present_category_detail/')


@user_login_required
@user_permission_required('attendance.can_view_attendance_import_export', '/school/home/')
def ExportStudentAttendanceList(request):

    from_Date = request.POST.get('e_from_Date')
    class_name = request.POST.get('e_class_name')
    to_Date = request.POST.get('e_to_Date')
    section_name = request.POST.get('e_section_name')
    academic_years = request.POST.get('e_year_name')
    date_val = request.POST.get('e_from_Date')
    to_date_val = request.POST.get('e_to_Date')
    from_Date = time.strftime("%Y-%m-%d")
    to_Date = time.strftime("%Y-%m-%d")


    acad_year = academic_year.objects.get(id=academic_years)
    user =request.user
    selected_class_id = class_name
    selected_class_name = class_name
    selected_section_id = section_name
    selected_section_name = section_name
    if class_name != "All":
        class_name_export = class_details.objects.get(id=class_name)
    else:
        class_name_export = "All"
    if section_name != "All":
        section_name_export = sections.objects.get(id=section_name)
    else:
        section_name_export = "All"

    file_name = "Data_" + str(class_name_export) + "_" + str(section_name_export)

    years = academic_year.objects.all()
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

        mapped_class = []
        mapped_section = []
        if user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(supervisor_id=user.id):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)

        elif user.is_allowed_user():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                mapped_class.append(mapped_object.class_name)
                mapped_section.append(mapped_object.section_name)
        else:
            if not user.is_system_admin():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year).filter(Q(assistant_teacher = user.id) | Q(staff_id = user.id)):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)
            else:
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                    mapped_class.append(mapped_object.class_name)
                    mapped_section.append(mapped_object.section_name)

    except:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")
        return redirect('/school/manage_attendance/')


    if not class_name == 'All' and not section_name == 'All':
        selected_class_id = class_details.objects.get(id=class_name).id
        selected_class_name = class_details.objects.get(id=class_name)
        selected_section_id = sections.objects.get(id=section_name).id
        selected_section_name = sections.objects.get(id=section_name)
        years = academic_year.objects.all()

        try:
            current_academic_year = academic_year.objects.get(year_name=acad_year)
            selected_year_id = current_academic_year.id

            mapped_class = []
            mapped_section = []
            if user.is_supervisor():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                    for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                            supervisor_id=user.id):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)
            elif user.is_allowed_user():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                    mapped_class.append(mapped_object.class_name)
                    if mapped_object:
                        for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                            mapped_section.append(obj.section_name)
            else:
                if not user.is_system_admin():
                    for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year).filter(
                                    Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)
                else:
                    for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                        mapped_class.append(mapped_object.class_name)
                        if mapped_object:
                            for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                                mapped_section.append(obj.section_name)
        except:
            messages.warning(request, "Academic Year Class Section Mapping Not Found.")
            return redirect('/school/manage_attendance/')

    raw_dict = {}

    if section_name == 'All' and class_name == 'All':
        AttendanceDetails = attendance_details.objects.filter(attendancedate__range=[from_Date, to_Date],
                                                              student__is_active=True).order_by('student__first_name')#.values('student__id', 'student__first_name', 'student__academic_class_section__class_name__class_name', 'student__academic_class_section__section_name__section_name', 'status', 'absent_reason', 'attendancedate', 'absent_category__absent_category_name')

    elif class_name == 'All' and not section_name == 'All':
        selected_section_id = sections.objects.get(id=section_name).id
        selected_section_name = sections.objects.get(id=section_name)
        AttendanceDetails = attendance_details.objects.filter(attendancedate__range=[from_Date, to_Date],
                                                              student__academic_class_section__section_name=section_name,
                                                              student__is_active=True).order_by('student__first_name')#.values('student__id', 'student__first_name', 'student__academic_class_section__class_name__class_name', 'student__academic_class_section__section_name__section_name', 'status', 'absent_reason', 'attendancedate', 'absent_category__absent_category_name')


    elif not class_name == 'All' and section_name == 'All':
        selected_class_id = class_details.objects.get(id=class_name).id
        selected_class_name = class_details.objects.get(id=class_name)

        try:
            current_academic_year = academic_year.objects.get(year_name=acad_year)
            selected_year_id = current_academic_year.id

            mapped_class = []
            mapped_section = []
            if user.is_supervisor():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                    for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                            supervisor_id=user.id):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)
            elif user.is_allowed_user():
                for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                    mapped_class.append(mapped_object.class_name)
                    if mapped_object:
                        for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                            mapped_section.append(obj.section_name)
            else:
                if not user.is_system_admin():
                    for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year).filter(
                                    Q(assistant_teacher=user.id) | Q(staff_id=user.id)):
                        mapped_class.append(mapped_object.class_name)
                        mapped_section.append(mapped_object.section_name)
                else:
                    for mapped_object in academic_class_section_mapping.objects.filter(year_name=acad_year):
                        mapped_class.append(mapped_object.class_name)
                        if mapped_object:
                            for obj in academic_class_section_mapping.objects.filter(class_name=class_name):
                                mapped_section.append(obj.section_name)
        except:
            messages.warning(request, "Academic Year Class Section Mapping Not Found.")
            return redirect('/school/manage_attendance/')



        AttendanceDetails = attendance_details.objects.filter(attendancedate__range=[from_Date, to_Date],
                                                              student__academic_class_section__class_name=class_name,
                                                              student__is_active=True).order_by('student__first_name')#.values('student__id', 'student__first_name', 'student__academic_class_section__class_name__class_name', 'student__academic_class_section__section_name__section_name', 'status', 'absent_reason', 'attendancedate', 'absent_category__absent_category_name')


    else:
        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_years,
                                                                            section_name=section_name,
                                                                            class_name=class_name)
        filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj,
                                                           is_active=True, joining_date__lte=todays_date())
        AttendanceDetails = attendance_details.objects.filter(student__in=filtered_students,
                                                              attendancedate__range=[from_Date, to_Date]).order_by('student__first_name')#.values('student__id', 'student__first_name', 'student__academic_class_section__class_name__class_name', 'student__academic_class_section__section_name__section_name', 'status', 'absent_reason', 'attendancedate', 'absent_category__absent_category_name')
    try:
        attendance_list = []

        for attendance in AttendanceDetails:
            student_list = []
            # student_list.append(attendance.student.id)
            student_list.append(attendance.student.get_all_name())
            student_list.append(attendance.student.student_code)
            student_list.append(attendance.student.odoo_id)
            student_list.append(attendance.student.academic_class_section.class_name.class_name)
            student_list.append(attendance.student.academic_class_section.section_name.section_name)
            student_list.append(attendance.status)
            if attendance.absent_reason != None and attendance.absent_reason != '':
                student_list.append(attendance.absent_reason)
            else:
                student_list.append('')
            student_list.append(attendance.attendancedate)
            try:
                if attendance.absent_category.absent_category_name:
                    student_list.append(attendance.absent_category.absent_category_name)
                else:
                    student_list.append('')
            except:
                student_list.append('')

            try:
                if attendance.present_category.present_category_name:
                    student_list.append(attendance.present_category.present_category_name)
                else:
                    student_list.append('')
            except:
                student_list.append('')
            attendance_list.append(student_list)

        column_names = ['Student Name', 'Student Id','Scodoo Id', 'Class', 'Section','Status','Absent Reason', 'Attendance Date',
                        'Absent Category', 'Present Category']

        output = BytesIO()
        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet('AttTemplate')

        # Create some cell formats with protection properties.
        unlocked = workbook.add_format({'locked': False})
        locked = workbook.add_format({'locked': True})

        # Format the worksheet to unlock all cells.
        worksheet.set_column('A:XDF', None, unlocked)

        # Turn worksheet protection on.
        worksheet.protect()
        bold = workbook.add_format({'bold': True})

        row_num = 0
        for col_num in range(len(column_names)):
            worksheet.set_column(col_num, col_num, 20)
            worksheet.write(row_num, col_num, column_names[col_num],locked)

        for row in attendance_list:
            row_num += 1
            for col_num in range(len(row)):
                if col_num==0 or col_num==1 or col_num==2 or col_num==3 or col_num==4 or col_num==5:
                    worksheet.write(row_num, col_num, unicode(row[col_num]),locked)
                else:
                    worksheet.write(row_num, col_num, unicode(row[col_num]),unlocked)


        workbook.close()
        output.seek(0)
        response = HttpResponse(output.read(), content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename=' + file_name + ".xls"

        return response

    except Exception as e:
        return HttpResponseBadRequest(json.dumps({'message': str(e)}), content_type="application/json")


@user_login_required
@user_permission_required('attendance.can_view_daily_attendance', '/school/home/')
def DailyAttendanceReport(request):
    current_academic_year = academic_year.objects.get(current_academic_year=1)
    absent_category_list = absent_categories.objects.filter(daily_attendance=True)
    absent_cat_list = []

    total_stud_cnt = 0
    total_present = 0
    total_late = 0
    total_percentage = 0
    total_absent= 0
    total_net_att= 0

    date = datetime.datetime.now().strftime("%Y-%m-%d")
    absent_category_dict ={}
    attendance_class_counter = 0

    for c in absent_category_list:
        absent_cat_list.append(c.absent_category_name)
        absent_category_dict[c.absent_category_name] = 0

    if request.POST:
        year = request.POST.get('year_name', None)
        date = request.POST.get('date', None)

        main_list = []
        acd_cls_sec = academic_class_section_mapping.objects.filter(year_name=year)
        for year_rec in acd_cls_sec:

            if student_details.objects.filter(academic_class_section=year_rec,joining_date__lte=datetime.date.today()).exists():

                net_attendance = 0

                main_dict = {}
                main_dict['classes'] = year_rec.class_name
                main_dict['sections'] = year_rec.section_name
                student_list = student_details.objects.filter(academic_class_section=year_rec, is_active=True,
                                                              joining_date__lte=datetime.date.today()).values_list('id')
                total_student_cnt = student_list.count()


                attendance_taken = attendance_details.objects.filter(attendancedate=date,
                                                                     student__academic_class_section=year_rec).exists()

                main_dict['student_cnt'] = total_student_cnt

                if attendance_taken:
                    attendance_class_counter += 1
                    present_cnt = attendance_details.objects.filter(~Q(present_category__present_category_name='Late'),
                                                                    attendancedate=date, status='Present',
                                                                    student__academic_class_section=year_rec,
                                                                    student__in=student_list).count()

                    if student_list:
                        present_percentage = round((present_cnt / total_student_cnt) * 100, 2)

                    else:
                        present_percentage = 0.0


                    absent_cnt = attendance_details.objects.filter(Q(absent_category__daily_attendance=False) | Q(absent_category__absent_category_name=None),attendancedate=date, status='Absent',
                                                                   student__academic_class_section=year_rec,
                                                                   student__in=student_list).count()

                    late_cnt = attendance_details.objects.filter(attendancedate=date, status='Present',
                                                                 present_category__present_category_name='Late',
                                                                 student__academic_class_section=year_rec,
                                                                 student__in=student_list).count()

                    total_present += present_cnt
                    total_percentage += present_percentage
                    total_stud_cnt += total_student_cnt
                    total_late += late_cnt
                    total_absent += absent_cnt

                    net_attendance += int(present_cnt)
                    net_attendance += int(late_cnt)
                    # net_attendance += int(late_cnt)

                else:
                    present_cnt = ''
                    present_percentage = ''
                    absent_cnt = ''
                    late_cnt = ''
                    total_stud_cnt += total_student_cnt

                main_dict['present_cnt'] = present_cnt
                main_dict['present_percentage'] = present_percentage
                main_dict['absent_cnt'] = absent_cnt
                main_dict['late_cnt'] = late_cnt

                for cat in absent_category_list:

                    category = attendance_details.objects.filter(attendancedate=date, absent_category=cat,
                                                                 student__academic_class_section=year_rec,
                                                                 student__in=student_list,status='Absent')
                    if category:
                        for c in category:
                            pass

                        if attendance_taken:
                            main_dict[c.absent_category.absent_category_name] = category.count()
                            net_attendance += int(category.count())
                            absent_category_dict[c.absent_category.absent_category_name] += category.count()
                        else:
                            main_dict[c.absent_category.absent_category_name] = ''
                            absent_category_dict[c.absent_category.absent_category_name] += 0
                    else:
                        if attendance_taken:
                            main_dict[cat.absent_category_name] = 0
                            absent_category_dict[str(cat.absent_category_name)] += 0
                            net_attendance += 0
                        else:
                            main_dict[cat.absent_category_name] = ''

                if attendance_taken:
                    if total_student_cnt != 0:
                        main_dict['net_attendance'] = round((net_attendance / int(total_student_cnt)) * 100, 2)
                        total_net_att += round((net_attendance / int(total_student_cnt)) * 100, 2)
                    else:
                        main_dict['net_attendance'] = 0
                else:
                    main_dict['net_attendance'] = ''

                main_list.append(main_dict)

        temp_list = []
        net_attendance_counter = 0
        temp_list.append('Total')
        temp_list.append('')
        temp_list.append(total_stud_cnt)
        temp_list.append(total_present)
        net_attendance_counter += total_present
        temp_list.append(total_late)
        net_attendance_counter += total_late
        if total_stud_cnt != 0:
            temp_list.append(round((float(total_present) / total_stud_cnt) * 100, 2))
        else:
            temp_list.append('0')
        temp_list.append(total_absent)

        list_counter = 0

        while list_counter < len(absent_cat_list):
            temp_list.append(absent_category_dict[absent_cat_list[list_counter]])
            net_attendance_counter += absent_category_dict[absent_cat_list[list_counter]]
            list_counter += 1

        if total_stud_cnt != 0:
            temp_list.append(round((float(net_attendance_counter)/total_stud_cnt)*100,2))
        else:
            temp_list.append(0)

        # main_list.append(temp_list)
    else:
        return render(request, "daily_attendance_report.html",
                      {'year': current_academic_year, 'absent_cat_list': absent_cat_list,'date': date})

    return render(request, "daily_attendance_report.html",
                  {'year': current_academic_year, 'main_list': main_list, 'absent_cat_list': absent_cat_list,
                   'date': date,'final_count':temp_list})


def ExportExcelDailyAttendanceReport(request):
    current_academic_year = academic_year.objects.get(current_academic_year=1)
    absent_category_list = absent_categories.objects.filter(daily_attendance=True)
    absent_cat_list = []

    total_stud_cnt = 0
    total_present = 0
    total_late = 0
    total_percentage = 0
    total_absent = 0
    total_net_att = 0

    absent_category_dict = {}
    attendance_class_counter = 0

    try:
        for c in absent_category_list:
            absent_cat_list.append(c.absent_category_name)
            absent_category_dict[c.absent_category_name] = 0

        # if request.POST:
        # year=request.POST.get('year_name',None)
        year = current_academic_year
        date = request.POST.get('excel_date', None)

        if date == None or date == '':
            date = datetime.datetime.now().strftime("%Y-%m-%d")

        final_list = []
        acd_cls_sec = academic_class_section_mapping.objects.filter(year_name=year)
        for year_rec in acd_cls_sec:
            if student_details.objects.filter(academic_class_section=year_rec,
                                              joining_date__lte=datetime.date.today()).exists():
                net_attendance = 0
                main_list = []

                student_list = student_details.objects.filter(academic_class_section=year_rec, is_active=True,
                                                              joining_date__lte=datetime.date.today()).values_list('id')
                total_student_cnt = student_list.count()

                attendance_taken = attendance_details.objects.filter(attendancedate=date,
                                                                     student__academic_class_section=year_rec).exists()

                if attendance_taken:
                    attendance_class_counter += 1
                    present_cnt = attendance_details.objects.filter(~Q(present_category__present_category_name='Late'),
                                                                    attendancedate=date, status='Present',
                                                                    student__academic_class_section=year_rec,
                                                                    student__in=student_list).count()
                    if student_list:
                        present_percentage = round((present_cnt / total_student_cnt) * 100, 2)
                    else:
                        present_percentage = 0.0

                    absent_cnt = attendance_details.objects.filter(Q(absent_category__daily_attendance=False) | Q(absent_category__absent_category_name=None),
                                                                   attendancedate=date, status='Absent',
                                                                   student__academic_class_section=year_rec,
                                                                   student__in=student_list).count()

                    late_cnt = attendance_details.objects.filter(attendancedate=date, status='Present',
                                                                 present_category__present_category_name='Late',
                                                                 student__academic_class_section=year_rec,
                                                                 student__in=student_list).count()

                    total_present += present_cnt
                    total_percentage += present_percentage
                    total_stud_cnt += total_student_cnt
                    total_late += late_cnt
                    total_absent += absent_cnt

                    net_attendance += int(present_cnt)
                    net_attendance += int(late_cnt)
                    # net_attendance += int(late_cnt)
                else:
                    present_cnt = ''
                    present_percentage = ''
                    absent_cnt = ''
                    late_cnt = ''
                    total_stud_cnt += total_student_cnt

                main_list.append(year_rec.class_name.class_name)
                main_list.append(year_rec.section_name.section_name)
                main_list.append(total_student_cnt)
                main_list.append(present_cnt)
                main_list.append(late_cnt)
                main_list.append(present_percentage)
                main_list.append(absent_cnt)

                column_names = ['Class', 'Section', 'Total', 'Present', 'Late', 'Present%', 'Absent']

                for cat in absent_category_list:
                    category = attendance_details.objects.filter(attendancedate=date, absent_category=cat,
                                                                 student__academic_class_section=year_rec,
                                                                 student__in=student_list,status='Absent')

                    if category:
                        for c in category:
                            column_names.append(c.absent_category.absent_category_name)

                        if attendance_taken:
                            absent_category_dict[c.absent_category.absent_category_name] += category.count()
                            main_list.append(category.count())
                            net_attendance += int(category.count())
                        else:
                            main_list.append(str(''))
                            absent_category_dict[c.absent_category.absent_category_name] += 0
                    else:
                        column_names.append(cat.absent_category_name)

                        if attendance_taken:
                            main_list.append('0')
                            absent_category_dict[str(cat.absent_category_name)] += 0
                        else:
                            main_list.append(str(''))

                column_names.append('Net Attendance')

                if attendance_taken:
                    if total_student_cnt != 0:
                        main_list.append(str(round((net_attendance / int(total_student_cnt)) * 100, 2)))
                        total_net_att += round((net_attendance / int(total_student_cnt)) * 100, 2)
                    else:
                        main_list.append('')
                else:
                    main_list.append(str(''))

                final_list.append(main_list)

        temp_list = []
        net_attendance_counter = 0
        temp_list.append('Total')
        temp_list.append('')
        temp_list.append(total_stud_cnt)
        temp_list.append(total_present)
        net_attendance_counter += total_present
        temp_list.append(total_late)
        net_attendance_counter += total_late
        if total_stud_cnt != 0:
            temp_list.append(round((float(total_present) / total_stud_cnt) * 100, 2))
        else:
            temp_list.append('0')
        temp_list.append(total_absent)

        list_counter = 0

        while list_counter < len(absent_cat_list):
            temp_list.append(absent_category_dict[absent_cat_list[list_counter]])
            net_attendance_counter += absent_category_dict[absent_cat_list[list_counter]]
            list_counter += 1

        if total_stud_cnt != 0:
            temp_list.append(round((float(net_attendance_counter) / total_stud_cnt) * 100, 2))
        else:
            temp_list.append(0)

        final_list.append(temp_list)

        sz_daily_attendance_report_log.objects.create(downloaded_by = request.user)
        return export_users_with_time_xls('DailyAttendanceReport', column_names, final_list,date)
    except:
        messages.warning(request, 'Some Error Occurred. Please Try Again')
    return redirect('/school/dailyAttendanceReport/')


from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, TableStyle, Table
from reportlab.lib.pagesizes import A4, inch, landscape
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_CENTER,TA_RIGHT


def DailyAttendanceReportPDF(request):
    current_academic_year = academic_year.objects.get(current_academic_year=1)
    absent_category_list = absent_categories.objects.filter(daily_attendance=True)
    absent_cat_list = []

    total_stud_cnt = 0
    total_present = 0
    total_late = 0
    total_percentage = 0
    total_absent = 0
    total_net_att = 0

    absent_category_dict = {}
    attendance_class_counter = 0

    try:
        for c in absent_category_list:
            absent_cat_list.append(c.absent_category_name)
            absent_category_dict[c.absent_category_name] = 0

        # if request.POST:
        # year=request.POST.get('year_name',None)
        year = current_academic_year
        date = request.POST.get('pdf_date', None)

        if date == None or date == '':
            date = datetime.datetime.now().strftime("%Y-%m-%d")

        final_list = []
        acd_cls_sec = academic_class_section_mapping.objects.filter(year_name=year)

        column_names = ['Class', 'Section', 'Total', 'Present', 'Late', 'Present%', 'Absent']

        counter = 0
        for year_rec in acd_cls_sec:
            if student_details.objects.filter(academic_class_section=year_rec,
                                              joining_date__lte=datetime.date.today()).exists():
                net_attendance = 0
                main_list = []

                student_list = student_details.objects.filter(academic_class_section=year_rec, is_active=True,
                                                              joining_date__lte=datetime.date.today()).values_list('id')
                total_student_cnt = student_list.count()

                attendance_taken = attendance_details.objects.filter(attendancedate=date,
                                                                     student__academic_class_section=year_rec).exists()

                if attendance_taken:
                    attendance_class_counter += 1
                    present_cnt = attendance_details.objects.filter(~Q(present_category__present_category_name='Late'),
                                                                    attendancedate=date, status='Present',
                                                                    student__academic_class_section=year_rec,
                                                                    student__in=student_list).count()
                    if student_list:
                        present_percentage = round((present_cnt / total_student_cnt) * 100, 2)
                    else:
                        present_percentage = 0.0

                    absent_cnt = attendance_details.objects.filter(Q(absent_category__daily_attendance=False) | Q(absent_category__absent_category_name=None),
                                                                   attendancedate=date, status='Absent',
                                                                   student__academic_class_section=year_rec,
                                                                   student__in=student_list).count()

                    late_cnt = attendance_details.objects.filter(attendancedate=date, status='Present',
                                                                 present_category__present_category_name='Late',
                                                                 student__academic_class_section=year_rec,
                                                                 student__in=student_list).count()

                    total_present += present_cnt
                    total_percentage += present_percentage
                    total_stud_cnt += total_student_cnt
                    total_late += late_cnt
                    total_absent += absent_cnt

                    net_attendance += int(present_cnt)
                    net_attendance += int(late_cnt)
                    # net_attendance += int(late_cnt)

                else:
                    present_cnt = ''
                    present_percentage = ''
                    absent_cnt = ''
                    late_cnt = ''
                    total_stud_cnt += total_student_cnt

                main_list.append(str(year_rec.class_name.class_name))
                main_list.append(str(year_rec.section_name.section_name))
                main_list.append(str(total_student_cnt))
                main_list.append(str(present_cnt))
                main_list.append(str(late_cnt))
                main_list.append(str(present_percentage))
                main_list.append(str(absent_cnt))

                for cat in absent_category_list:
                    category = attendance_details.objects.filter(attendancedate=date, absent_category=cat,
                                                                 student__academic_class_section=year_rec,
                                                                 student__in=student_list,status='Absent')
                    if category:
                        for c in category:
                            if c.absent_category.absent_category_name not in column_names:
                                column_names.append(str(c.absent_category.absent_category_name))

                        if attendance_taken:
                            absent_category_dict[c.absent_category.absent_category_name] += category.count()
                            main_list.append(str(category.count()))
                            net_attendance += int(category.count())
                        else:
                            main_list.append(str(''))
                            absent_category_dict[c.absent_category.absent_category_name] += 0

                    else:
                        if cat.absent_category_name not in column_names:
                            column_names.append(str(cat.absent_category_name))
                            absent_category_dict[str(cat.absent_category_name)] += 0

                        if attendance_taken:
                            main_list.append('0')
                        else:
                            main_list.append(str(''))

                if counter == 0:
                    column_names.append('Net Attendance')
                    final_list.append(column_names)
                    counter += 1

                if attendance_taken:
                    if total_student_cnt != 0:
                        total_net_att += round((net_attendance / int(total_student_cnt)) * 100, 2)
                        main_list.append(str(round((net_attendance / int(total_student_cnt)) * 100, 2)))
                    else:
                        main_list.append('')
                else:
                    main_list.append(str(''))

                final_list.append(main_list)

        temp_list = []
        net_attendance_counter = 0
        temp_list.append('Total')
        temp_list.append('')
        temp_list.append(str(total_stud_cnt))
        temp_list.append(str(total_present))
        net_attendance_counter += total_present
        temp_list.append(str(total_late))
        net_attendance_counter += total_late
        if total_stud_cnt != 0:
            temp_list.append(str(round((float(total_present) / total_stud_cnt) * 100, 2)))
        else:
            temp_list.append('0')
        temp_list.append(str(total_absent))

        list_counter = 0

        while list_counter < len(absent_cat_list):
            temp_list.append(str(absent_category_dict[absent_cat_list[list_counter]]))
            net_attendance_counter += absent_category_dict[absent_cat_list[list_counter]]
            list_counter += 1

        if total_stud_cnt != 0:
            temp_list.append(str(round((float(net_attendance_counter) / total_stud_cnt) * 100, 2)))
        else:
            temp_list.append('0')

        final_list.append(temp_list)

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=' + 'DailyAttendanceReportPDF.pdf'

        elements = []

        school_rec = school_details.objects.filter()[0]
        if school_rec.logo:
            img_path = 'http://' + request.META['HTTP_HOST'] + '/school/media/' + str(school_rec.logo)
            I = Image(img_path)
            I.drawHeight = 1 * inch
            I.drawWidth = 1 * inch
            elements.append(I)

        doc = SimpleDocTemplate(response, topMargin=10)
        doc.pagesize = landscape(A4)


        # #Get this line right instead of just copying it from the docs
        style = TableStyle([('ALIGN', (1, 1), (-2, -2), 'RIGHT'),
                            ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
                            ('VALIGN', (0, 0), (0, -1), 'TOP'),
                            ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
                            ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
                            ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
                            ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
                            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                            ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                            ])

        # Configure style and word wrap


        ps = ParagraphStyle('title', fontSize=10, alignment=TA_CENTER, spaceBefore=10, spaceAfter=10)
        elements.append(Paragraph(str(school_rec.school_name),ps))
        ps = ParagraphStyle('title', fontSize=10, alignment=TA_RIGHT, spaceBefore=10, spaceAfter=10)
        elements.append(Paragraph('Downloaded On- ' + '-'.join(str(date).split('-')[::-1])+'  '+str(datetime.datetime.now().strftime("%H:%M")),ps))

        s = getSampleStyleSheet()
        s = s["BodyText"]
        s.wordWrap = 'CJK'
        data = [[Paragraph(cell, s) for cell in row] for row in final_list]
        table_obj = Table(data,colWidths=[None,None,None,None,None,None,None,None,1.2*inch])
        table_obj.setStyle(style)

        elements.append(table_obj)
        doc.build(elements)
        sz_daily_attendance_report_log.objects.create(downloaded_by=request.user)
        return response
    except Exception as e:
        messages.warning(request, 'Some Error Occurred. Please Try Again')
    return redirect('/school/dailyAttendanceReport/')


@user_login_required
@user_permission_required('attendance.can_view_daily_attendance_report_log', '/school/home/')
def DailyAttendanceReportLog(request):
    try:
        export_log = sz_daily_attendance_report_log.objects.all()
        return render(request, "daily_attendance_report_log.html", {'logs_recs': export_log})
    except:
        messages.warning(request, "Some Error Occurred. Try Again.")
    return redirect('/school/attendance_view/')


@user_login_required
def UpdateAbsentCheckValue(request):
    try:
        daily_id=request.GET.get('daily_id')
        val=request.GET.get('val')
        if val:
            absent_categories.objects.filter(id=daily_id).update(daily_attendance=True)
        else:
            absent_categories.objects.filter(id=daily_id).update(daily_attendance=False)

        return redirect('/school/category_detail/')
    except:
            messages.warning(request, "Some Error Occurred. ")
            return redirect('/school/category_detail/')

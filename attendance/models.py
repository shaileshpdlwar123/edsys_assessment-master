from __future__ import unicode_literals

from django.db import models

# Create your models here.
class absent_categories(models.Model):
    absent_category_name = models.CharField(max_length=45, blank=True, null=True)
    absent_category_code = models.CharField(max_length=45, blank=True, null=True)
    color_code = models.CharField(max_length=25, default='#ffffff')
    short_code = models.CharField(max_length=25, default='', null=True, blank=True)
    daily_attendance=models.BooleanField(default=False)

    class Meta:
        managed = True
        db_table = 'absent_categories'

        permissions = (
            ('can_view_view_absent_categories', 'can view view absent categories'),
            ('can_view_add_absent_categories', 'can view add absent categories'),
            ('can_view_edit_absent_categories', 'can view edit absent categories'),
        )

    def __unicode__(self):
        return self.absent_category_name

    def to_dict(self):
        res = {
            'absent_category_name': self.absent_category_name,
            'absent_category_code': self.absent_category_code,
            'color_code': self.color_code,
            'short_code': self.short_code,
            'status': 'Absent',
        }
        return res



class present_categories(models.Model):
    present_category_name = models.CharField(max_length=45, blank=True, null=True)
    present_category_code = models.CharField(max_length=45, blank=True, null=True)
    color_code = models.CharField(max_length=25, default='#ffffff')
    short_code = models.CharField(max_length=25, default='', null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'present_categories'

        permissions = (
            ('can_view_view_present_categories', 'can view view present categories'),
            ('can_view_add_present_categories', 'can view add present categories'),
            ('can_view_edit_present_categories', 'can view edit present categories'),
        )

    def __unicode__(self):
        return self.present_category_name

    def to_dict(self):
        res = {
            'present_category_name': self.present_category_name,
            'present_category_code': self.present_category_code,
            'color_code': self.color_code,
            'short_code': self.short_code,
            'status':'Present',
        }
        return res


class attendance_details(models.Model):

    student = models.ForeignKey('registration.student_details',db_column='student', related_name='student_attendance_relation')
    absent_category = models.ForeignKey(absent_categories, blank=True, null=True, on_delete=models.PROTECT)
    absent_reason = models.CharField(max_length=255, blank=True, null=True)
    late = models.BooleanField(default=False)
    attendancedate = models.DateField()
    attendance_time = models.TimeField()
    attendance_update_date_time = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=200)
    present_category = models.ForeignKey(present_categories, blank=True, null=True, on_delete=models.PROTECT)
    attendance_taken_by = models.ForeignKey('registration.AuthUser', on_delete=models.PROTECT, null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'attendance_details'

        permissions = (
            ('can_view_attendance', 'can view attendance'),
            ('can_view_manage_attendance', 'can view manage attendance'),
            ('can_view_attendance_add', 'can view attendance add'),
            ('can_view_attendance_edit', 'can view attendance edit'),
            ('can_view_attendance_dashboard', 'can view attendance dashboard'),
            ('can_view_attendance_import_export', 'can view attendance import export'),
            ('can_view_bulk_attendance', 'can view bulk attendance'),
            ('can_view_daily_attendance', 'can view daily attendance'),
        )

    def __unicode__(self):
        return str(self.attendancedate)

    def to_dict(self):
        res = {
            'student_id': self.student.id,
            'first_name': self.student.first_name,
            'class_name': self.student.academic_class_section.class_name.to_dict(),
            'section_name': self.student.academic_class_section.section_name.to_dict(),
            'attendance_date': str(self.attendancedate.isoformat()),
            'status': self.status,
            'absent_category': str(self.absent_category if self.absent_category else None),
            'absent_reason ': str(self.absent_reason if self.absent_reason else None),
            'present_category': str(self.present_category if self.present_category else None)
        }
        return res


class attendance_details_excel(models.Model):
    attendance_excel = models.FileField(upload_to = 'uploads/')

class RawDict(dict):
    pass


from masters.utils import todays_date


class sz_daily_attendance_report_log(models.Model):
    downloaded_by = models.ForeignKey('registration.AuthUser', on_delete=models.PROTECT, null=True)
    uploaded_on = models.DateTimeField(auto_now=True)

    class Meta:
        permissions = (
            ('can_view_daily_attendance_report_log', 'can view daily attendance report log'),
        )

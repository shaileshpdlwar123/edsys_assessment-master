﻿from django.conf.urls import include, url
from django.contrib import admin
from . import views
from django.contrib.auth.views import logout
from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve
app_name = 'registration'


urlpatterns = [

    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT}),
 #===============================Group URLS==================================================#
    url(r'^view_user_permission_mapping/$', views.ViewUserGroupPermissionMapping, name='view_user_permission_mapping'),
    # url(r'^get_user_excluded_permission/$', views.getUserExcludedPermission, name='get_user_excluded_permission'),
    url(r'^get_user_excluded_permission/$', views.getGroupExcludedPermission, name='get_user_excluded_permission'),
    url(r'^get_user_role_wise/$', views.GetUserRoleWise, name='get_user_role_wise'),
    # url(r'^user_permission_mapping/$', views.UserPermissionMapping, name='user_permission_mapping'),
    url(r'^user_permission_mapping/$', views.UserGroupPermissionMapping, name='user_permission_mapping'),

    url(r'^view_user_permission_unmapping/$', views.UnmappUserGroupPermission, name='view_user_permission_unmapping'),
    url(r'^get_user_include_permission/$', views.getGroupIncludePermission, name='get_user_include_permission'),
    url(r'^get_user_include_exclude_permission/$', views.getGroupIncludeExcludePermission, name='get_user_include_exclude_permission'),
    url(r'^user_permission_unmapping/$', views.UserGroupPermissionUnMapping, name='user_permission_unmapping'),

    url(r'^delete_role_user_mapping_from_edit/$', views.DeleteGroupUserMappingFromEditRole, name='delete_role_user_mapping_from_edit'),
    url(r'^get_assign_role/(?P<role_id>[0-9]+)$', views.GetAssignRole, name='get_assign_role'),
    url(r'^save_assign_role/$', views.SaveRoleUserAssigning, name='save_assign_role'),
 #===============================Group URLS==================================================#
    
 #============================== Login URLS =================================================#   
    # url(r'^login/$', views.Login, name='login'),
    url(r'^login_succes/$', views.Login_Succes, name='login_succes'),
    url(r'^login_failed/$', views.Login_Failed, name='login_failed'),
    url(r'^logout/$', views.Logout, name='logout'),
    url(r'^staff_login/$', views.StaffHome, name='staff_login'),
    url(r'^parent_login/$', views.ParentHome, name='parent_login'),

#============================== Tiles URLS =================================================#
    url(r'^home/$',views.Home, name='home'),
    url(r'^my_profile/$',views.ManageUserProfile, name='my_profile'),
    url(r'^updated_user_profile/$',views.UpdatedUserProfile, name='updated_user_profile'),
    url(r'^change_password/$',views.ChangePassword, name='change_password'),
    url(r'^manage_users/$',views.ManageUser, name='manage_users'),
    url(r'^academic_school/$',views.AcademicSchool, name='academic_school'),
    url(r'^parents_students/$',views.ParentsStudents, name='parents_students'),
    url(r'^classes_sections/$',views.ClassesSections, name='classes_sections'),
    
#============================== User Details ==============================================#
    url(r'^view_user/$',views.ViewUser, name='view_user'),
    url(r'^add_user/$',views.AddUser, name='add_user'),
    url(r'^save_user/$',views.SaveUser, name='save_user'),
    url(r'^update_user/(?P<staff_id>[0-9]+)$', views.UpdateUser, name='update_user'),
    url(r'^save_updated_user/$',views.SaveUpdatedUser, name='save_updated_user'),
    url(r'^delete_user/(?P<staff_id>[0-9]+)$', views.DeleteUser, name='delete_user'),
    url(r'^inactive_user/$',views.InActiveUser, name='inactive_user'),
    url(r'^active_user/$',views.ActiveUser, name='active_user'),

    url(r'^export_user_template/$',views.ExportUserTemplate, name='export_user_template'),
    url(r'^import_user_records/$',views.ImportUserRecords, name='import_user_records'),
    url(r'^user_import_log/$', views.UserImportLog, name='user_import_log'),
    url(r'^import_from_user_preview/$', views.ImportFromUserPreview, name='import_from_user_preview'),

    
#============================== Role Details ==============================================# 
    url(r'^view_role/$',views.ViewRole, name='view_role'),
    url(r'^add_role/$',views.AddRole, name='add_role'),
    url(r'^save_role/$',views.SaveRole, name='save_role'),
    url(r'^update_role/(?P<role_id>[0-9]+)$', views.UpdateRole, name='update_role'),
    url(r'^save_updated_role/$',views.SaveUpdatedRole, name='save_updated_role'),
    url(r'^delete_role/(?P<role_id>[0-9]+)$', views.DeleteRole, name='delete_role'),
    
#============================== Academic Details ==============================================#  
    url(r'^view_academicyr/$',views.ViewAcademicYr, name='view_academicyr'),   
    url(r'^year_name_exist/$',views.YearNameExist, name='year_name_exist'),
    url(r'^year_code_exist/$',views.YearCodeExist, name='year_code_exist'),
    url(r'^add_academicyr/$',views.AddAcademicYr, name='add_academicyr'),
    url(r'^save_academicyr/$',views.SaveAcdemicYr, name='save_academicyr'),
    url(r'^update_academicyr/(?P<year_id>[0-9]+)$', views.UpdateAcademicYr, name='update_academicyr'),
    url(r'^save_updated_academicyr/$',views.SaveUpdatedAcademicYr, name='save_updated_academicyr'),
    url(r'^delete_academicyr/(?P<year_id>[0-9]+)$', views.DeleteAcademicYr, name='delete_academicyr'),
    
#============================== School Details ==============================================#    
    url(r'^view_school/$',views.ViewSchool, name='view_school'),
    url(r'^update_school/(?P<school_id>[0-9]+)$', views.UpdateSchool, name='update_school'),
    url(r'^save_updated_school/$',views.SaveUpdatedSchool, name='save_updated_school'),
    url(r'^delete_school/(?P<school_id>[0-9]+)$', views.DeleteSchool, name='delete_school'),
    
#============================== Class Details ==============================================#   
    url(r'^view_class/$',views.ViewClass, name='view_class'), 
    url(r'^add_class/$',views.AddClass, name='add_class'),
    url(r'^save_class/$',views.SaveClass, name='save_class'),
    url(r'^update_class/(?P<class_id>[0-9]+)$', views.UpdateClass, name='update_class'),
    url(r'^save_updated_class/$',views.SaveUpdatedClass, name='save_updated_class'),
    url(r'^delete_class/(?P<class_id>[0-9]+)$', views.DeleteClass, name='delete_class'),
    
#============================== Section Details ==============================================#  
    url(r'^view_section/$',views.ViewSection, name='view_section'),
    url(r'^add_section/$',views.AddSection, name='add_section'),
    url(r'^save_section/$',views.SaveSection, name='save_section'),
    url(r'^update_section/(?P<section_id>[0-9]+)$', views.UpdateSection, name='update_section'),
    url(r'^save_updated_section/$',views.SaveUpdatedSection, name='save_updated_section'),
    url(r'^delete_section/(?P<section_id>[0-9]+)$', views.DeleteSection, name='delete_section'),


#================================= Class Structure ===============================
    url(r'^class_structure/$',views.ClassStructure, name='class_structure'),
    url(r'^class_structure_details/$',views.ClassStructureDetails, name='class_structure_details'),
    url(r'^export_class_structure_details/$',views.ExportClassStructureDetails, name='export_class_structure_details'),
#============================== Subject Details ==============================================#
    url(r'^view_subject/$',views.ViewSubject, name='view_subject'),
    url(r'^add_subject/$',views.AddSubject, name='add_subject'),
    url(r'^save_subject/$',views.SaveSubject, name='save_subject'),
    url(r'^update_subject/(?P<subject_id>[0-9]+)$', views.UpdateSubject, name='update_subject'),
    url(r'^save_updated_subject/$',views.SaveUpdatedSubject, name='save_updated_subject'),
    url(r'^delete_subject/(?P<subject_id>[0-9]+)$', views.DeleteSubject, name='delete_subject'),
    url(r'^save_substrands/$',views.SaveSubStrands, name='save_substrands'),
    url(r'^update_substrands/$',views.UpdateSubStrands, name='update_substrands'),
    url(r'^delete_substrands/$',views.DeleteSubstrands, name='delete_substrands'),
    url(r'^subject_strand_delete/$', views.DeleteSubjectStrand, name='subject_strand_delete'),
    url(r'^subject_substrand_delete/$', views.DeleteSubjectSubStrand, name='subject_substrand_delete'),

                  #============================== Student Details ==============================================#
    url(r'^view_student/$',views.ViewStudent, name='view_student'),
    url(r'^change_section/$',views.ChangeSection, name='change_section'),
    url(r'^view_students_photo/$',views.ViewStudenstPhoto, name='view_students_photo'),
    url(r'^change_section_student_list/$',views.ShuffleStudentList, name='change_section_student_list'),
    url(r'^shuffle_students/$',views.ShuffleStudent, name='shuffle_students'),
    url(r'^bulk_export_student/$',views.BulkExportStudent, name='bulk_export_student'),
    url(r'^bulk_export_student_list/$',views.BulkExportStudentList, name='bulk_export_student_list'),
    url(r'^export_student_list/$',views.ExportStudentList, name='export_student_list'),
    url(r'^add_student/$',views.AddStudent, name='add_student'),
    url(r'^save_student/$',views.SaveStudent, name='save_student'),
    url(r'^update_student/(?P<std_id>[0-9]+)$', views.UpdateStudent, name='update_student'),
    url(r'^update_student_from_tc/(?P<std_id>[0-9]+)$', views.UpdateStudentFromTc, name='update_student_from_tc'),
    url(r'^update_student_from_photo/(?P<std_id>[0-9]+)$', views.UpdateStudentFromPhoto, name='update_student_from_photo'),
    url(r'^update_student_from_missing_profile/(?P<std_id>[0-9]+)$', views.UpdateStudentFromMissingProfile, name='update_student_from_missing_profile'),
    url(r'^save_updated_student/$',views.SaveUpdatedStudent, name='save_updated_student'),
    url(r'^delete_student/$', views.DeleteStudent, name='delete_student'),
    url(r'^export_bulk_student/$', views.ExportBulkStudent, name='export_bulk_student'),
    url(r'^export_student/$',views.ExportStudent, name='export_student'),
    url(r'^view_filter_student_list/$',views.ViewFilterStudentList, name='view_filter_student_list'),
    url(r'^view_photo_filter_student_list/$',views.ViewPhotoFilterStudentList, name='view_photo_filter_student_list'),
    # url(r'^export_student_photos/$',views.ExportStudentPhotos, name='export_student_photos'),


    url(r'^view_filter_student_record/$',views.ViewFilterStudentRecord, name='view_filter_student_record'),

    url(r'^viewParentEmail/$',views.viewParentEmail, name='viewParentEmail'),
    url(r'^viewstudent_subjectdetails/$',views.ViewStudentSubjectDetails, name='viewstudent_subjectdetails'),
    url(r'^student_subject_list/$',views.StudentSubjectList, name='student_subject_list'),

    url(r'^import_students_bulk_details/$',views.StudentsBulkImport, name='import_students_bulk_details'),
    url(r'^missing_profile_student/$',views.MissingProfileStudent, name='missing_profile_student'),
    url(r'^view_missing_student_list/$',views.ViewMissingStudentList, name='view_missing_student_list'),
    url(r'^export_student_missing_profile/$',views.ExportStudentMissingProfile, name='export_student_missing_profile'),

    url(r'^view_student_profile_filter/$',views.ViewStudentProfileFilter, name='view_student_profile_filter'),
    url(r'^export_student_template/$',views.ExportStudentTemplate, name='export_student_template'),
    url(r'^import_students_template_records/$',views.ImportStudentsTemplateRecords, name='import_students_template_records'),
    url(r'^student_import_log/$',views.StudentImportLog, name='student_import_log'),
    url(r'^view_filter_student_profile_list/$',views.ViewFilterStudentProfileList, name='view_filter_student_profile_list'),

    url(r'^academic_house/$',views.AcademicHouse, name='academic_house'),
    url(r'^filter_academic_house_info/$',views.FilterAcademicHouseInfo, name='filter_academic_house_info'),
    url(r'^export_academic_house_info/$',views.ExportAcademicHouseInfo, name='export_academic_house_info'),
    url(r'^import_academic_house_info/$',views.ImportAcademicHouseInfo, name='import_academic_house_info'),

    url(r'^academic_transport/$',views.AcademicTrasport, name='academic_transport'),
    url(r'^update_transport_info/$',views.UpdateTransportInfo, name='update_transport_info'),
    url(r'^filter_academic_transport_info/$',views.FilterAcademicTransportInfo, name='filter_academic_transport_info'),
    url(r'^export_academic_transport_info/$',views.ExportAcademicTransportInfo, name='export_academic_transport_info'),
    url(r'^import_academic_transport_info/$',views.ImportAcademicTransportInfo, name='import_academic_transport_info'),

#============================== Parent Details ==============================================#
    url(r'^view_parent/$',views.ViewParent, name='view_parent'),
    url(r'^update_parent_contact/$',views.UpdateParentContact, name='update_parent_contact'),
    url(r'^update_parent_contact_view/(?P<parent_id>[0-9]+)$',views.UpdateParentContactView, name='update_parent_contact_view'),
    url(r'^parent_filter/$',views.ParentFilter, name='parent_filter'),
    url(r'^parent_filter_export/$',views.ParentFilterExport, name='parent_filter_export'),
    url(r'^import_parents_contact/$',views.ParentContactsImport, name='import_parents_contact'),
    url(r'^add_parent/$',views.AddParent, name='add_parent'),
    url(r'^save_parent/$',views.SaveParent, name='save_parent'),
    url(r'^update_parent/(?P<parent_id>[0-9]+)$', views.UpdateParent, name='update_parent'),
    url(r'^save_updated_parent/$',views.SaveUpdatedParent, name='save_updated_parent'),
    url(r'^delete_parent/$',views.DeleteParent, name='delete_parent'),
    url(r'^approve_parent/$',views.ApproveParent, name='approve_parent'),

#----------------------- Parent & Student Update Request ---------------------------------


    url(r'^approve_request_tile/$',views.ApproveRequestTile, name='approve_request_tile'),
    url(r'^approve_student_list/$',views.ApproveStudentList, name='approve_student_list'),
    url(r'^reject_student_details/$',views.RejectStudentDetails, name='reject_student_details'),
    url(r'^new_reject_student_details/$',views.NewRejectStudentDetails, name='new_reject_student_details'),

    url(r'^approve_student_details/(?P<std_id>[0-9]+)$',views.ApproveStudentDetails, name='approve_student_details'),
    url(r'^new_approve_student_details/$',views.NewApproveStudentDetails, name='new_approve_student_details'),

    url(r'^approve_parent_list/$',views.ApproveParentList, name='approve_parent_list'),
    url(r'^approve_parent_details/(?P<std_id>[0-9]+)$',views.ApproveParentDetails, name='approve_parent_details'),
    url(r'^reject_parent_details/$',views.RejectParentDetails, name='reject_parent_details'),
    url(r'^new_reject_parent_details/$',views.NewRejectParentDetails, name='new_reject_parent_details'),
    url(r'^new_approve_parent_details/$',views.NewApproveParentDetails, name='new_approve_parent_details'),
    url(r'^export_parent_template/$',views.ExportParentTemplate, name='export_parent_template'),
    url(r'^import_parent_records/$',views.ImportParentRecords, name='import_parent_records'),
    url(r'^parent_import_log/$',views.ParentImportLog, name='parent_import_log'),

    # url(r'^reject_student_details/(?P<std_id>[0-9]+)$',views.RejectStudentDetails, name='reject_student_details'),

######################################### Academic-Class-Section Mapping ##############################    
    url(r'^academicyr_class_section_mapping/$', views.AcademicyrClassSectionMapping, name='academicyr_class_section_mapping'),
    url(r'^save_academicr_class_section_mapping/$', views.SaveAcademicrClassSectionMapping, name='save_academicr_class_section_mapping'),
    url(r'^delete_academicr_class_section_mapping/$', views.DeleteAcademicrClassSectionMapping, name='delete_academicr_class_section_mapping'),
    url(r'^export_academicr_class_section_mapping/$', views.ExportAcademicrClassSectionMapping, name='export_academicr_class_section_mapping'),

######################################### Mandatory-Subject Mapping ###################################
    url(r'^mandatory_subject_mapping/$', views.MandatorySubjectMapping, name='mandatory_subject_mapping'),
    url(r'^mandatory_subject_mapping_filter/$', views.MandatorySubjectMappingFilter, name='mandatory_subject_mapping_filter'),
    url(r'^mandatory_subject_mapping_filtered_list/$', views.MandatorySubjectMappingFilterList, name='mandatory_subject_mapping_filtered_list'),
    url(r'^save_mandatory_subject_mapping/$', views.SaveMandatorySubjectMapping, name='save_mandatory_subject_mapping'),
    url(r'^delete_mandatory_subject_mapping/$', views.DeleteMandatorySubjectMapping, name='delete_mandatory_subject_mapping'),
    url(r'^export_mandatory_subject_mapping/$', views.ExportMandatorySubjectMapping, name='export_mandatory_subject_mapping'),

####################################### Supervisor-Class Mapping #######################################################    
    url(r'^supervisor_class_mapping/$', views.SupervisorClassMapping, name='supervisor_class_mapping'),
    url(r'^supervisor_class_mapping_filter_list/$', views.SupervisorClassMappingFilterList, name='supervisor_class_mapping_filter_list'),
    url(r'^save_supervisor_class_mapping/$', views.SaveSupervisorClassMapping, name='save_supervisor_class_mapping'),
    url(r'^delete_academicr_supervisor_class_section_mapping/$', views.DeleteAcademicSupervisorClassSection, name='delete_academicr_supervisor_class_section_mapping'),
    url(r'^export_supervisor_class_mapping/$', views.ExportSupervisorClassMapping, name='export_supervisor_class_mapping'),
    url(r'^view_maped_section/$', views.ViewMapedSection, name='view_maped_section'),
    url(r'^view_mapped_subjects/$', views.ViewMappedSubjects, name='view_mapped_subjects'),
    url(r'^view_supervisor_export_filtered_list/$', views.ExportFilteredList, name='view_supervisor_export_filtered_list'),
    url(r'^export_supervisor_class/$', views.ExportSupervisorClass, name='export_supervisor_class'),
    url(r'^updating_lead_supervisor/$', views.updating_lead_supervisor, name='updating_lead_supervisor'),


####################################### Optional-Subject Mapping #######################################################       
    url(r'^optional_subject_mapping/$', views.OptionalSubjectMapping, name='optional_subject_mapping'),
    url(r'^optional_subject_mapping_filter/$', views.OptionalSubjectMappingFilter, name='optional_subject_mapping_filter'),
    url(r'^mapped_nationalities/$', views.MappedNationalities, name='mapped_nationalities'),
    url(r'^optional_subject_mapping_filter_list/$', views.OptionalSubjectMappingFilterList, name='optional_subject_mapping_filter_list'),
    url(r'^optional_subject_student_mapping/$', views.OptionalSubjectStudentMapping, name='optional_subject_student_mapping'),
    url(r'^save_optional_subject_mapping/$', views.SaveOptionalSubjectMapping, name='save_optional_subject_mapping'),
    url(r'^delete_optional_subject_mapping/$', views.DeleteOptionalSubjectMapping, name='delete_optional_subject_mapping'),
    url(r'^export_optional_subject_mapping/$', views.ExportOptionalSubjectMapping, name='export_optional_subject_mapping'),

####################################### Teacher-Subject Mapping #######################################################   
    url(r'^teacher_subject_mapping/$', views.TeacherSubjectMapping, name='teacher_subject_mapping'),
    url(r'^save_teacher_subject_mapping/$', views.SaveTeacherSubjectMapping, name='save_teacher_subject_mapping'),
    url(r'^delete_teacher_subject_mapping/$', views.DeleteTeacherSubjectMapping, name='delete_teacher_subject_mapping'),
    url(r'^export_teacher_subject_mapping_list/$', views.ExportTeacherSubjectMappingList, name='export_teacher_subject_mapping_list'),
    url(r'^export_acd_class_sec_mapping_list/$', views.ExportAcdClassSecMappingList, name='export_acd_class_sec_mapping_list'),
    url(r'^view_filter_acd_class_sec_list/$', views.FilteredAcdClassSecMappingList, name='view_filter_acd_class_sec_list'),
    url(r'^export_teacher_subject_mapping/$', views.ExportTeacherSubjectMapping, name='export_teacher_subject_mapping'),
    url(r'^export_teacher_subject_filtered_mapping/$', views.ExportTeacherSubjectFilteredMapping, name='export_teacher_subject_filtered_mapping'),

    ######################  Role User MAPPING URLS ########################################
    url(r'^view_role_user_mapping/$', views.ViewUserRoleMapping, name='view_role_user_mapping'),
    url(r'^role_user_mapping/$', views.RoleUserMapping, name='role_user_mapping'),
    url(r'^delete_role_user_mapping/$', views.DeleteUserRoleMapping, name='delete_role_user_mapping'),
    url(r'^export_role_user_mapping/$', views.ExportRoleUserMapping, name='export_role_user_mapping'),
    
    ####################################### Teacher-Class Mapping #######################################################    
    url(r'^teacher_class_mapping/$', views.TeacherClassMapping, name='teacher_class_mapping'),
    url(r'^teacher_class_mapping_filtered_list/$', views.TeacherClassMappingFilteredList, name='teacher_class_mapping_filtered_list'),
    url(r'^teacher_class_mapping_filteration_list/$', views.TeacherClassMappingFiltereationList, name='teacher_class_mapping_filteration_list'),
    url(r'^classTeacherMapping/$', views.ClassTeacherMaping, name='classTeacherMapping'),
    url(r'^delete_class_teacher_mapping/$', views.DeleteClassTeacherMapping, name='delete_class_teacher_mapping'),
    url(r'^viewAcademicDetails/$',views.ViewAcademicDetail, name='viewAcademicDetails'),
    url(r'^viewSectionDetails/$',views.ViewSectionDetail, name='viewSectionDetails'),
    url(r'^export_teacher_class_mapping/$', views.ExportTeacherClassMapping, name='export_teacher_class_mapping'),
    url(r'^viewTeacherDetails/$',views.ViewTeacherDetails, name='viewTeacherDetails'),
    url(r'^export_optional_subject/$', views.ExportOptionalSubject, name='export_optional_subject'),
    url(r'^view_export_optional_subject/$', views.ViewExportOptionalSubject, name='view_export_optional_subject'),
    url(r'^export_optional_subject_student_list/$',views.ExportOptionalSubjectList, name='export_optional_subject_student_list'),


    url(r'^validate_email_id/$', views.ValidateEmailId, name='validate_email_id'),
    url(r'^validate_scoodo_id/$', views.ValidateScoodoId, name='validate_scoodo_id'),
    url(r'^validate_update_scoodo_id/$', views.ValidateUpdateScoodoId, name='validate_update_scoodo_id'),
    url(r'^validate_contact_number/$', views.ValidateContactNumber, name='validate_contact_number'),


    #-------------------------- view_students_subjects-------------------------------------

    url(r'^view_students_subjects/$', views.ViewStudentSubjects, name='view_students_subjects'),
    url(r'^filter_view_student_subject/$', views.FilterViewStudentSubjects, name='filter_view_student_subject'),
    url(r'^export_view_student_subject/$', views.ExportViewStudentSubjects, name='export_view_student_subject'),


    #============================== Custom Groups ==============================================#
    url(r'^view_custom_groups/$',views.ViewCustomGroups, name='view_custom_groups'),
    url(r'^add_custom_group_sub/$',views.AddCustomGroupSub, name='add_custom_group_sub'),
    url(r'^save_custom_group_sub/$',views.SaveCustomGroupSub, name='save_custom_group_sub'),
    url(r'^update_custom_group_sub_group/(?P<custom_group_id>[0-9]+)$', views.UpdateCustomGroupSubGroup, name='update_custom_group_sub_group'),
    url(r'^save_updated_custom_group_sub_group/$',views.SaveUpdatedCustomGroupSubGroup, name='save_updated_custom_group_sub_group'),
    url(r'^custom_sub_group_delete/$', views.CustomSubGroupDelete, name='custom_sub_group_delete'),

    url(r'^delete_custom_group/(?P<custom_group_id>[0-9]+)$', views.DeleteCustomGroup, name='delete_custom_group'),
    url(r'^assign_group/$',views.AssignGroup, name='assign_group'),
    url(r'^filter_assign_student/$',views.FilterAssignStudent, name='filter_assign_student'),
    url(r'^request_assign_student/$',views.RequestAssignStudent, name='request_assign_student'),
    url(r'^get_custom_sub_group_name/$',views.GetCustomSubGroupName, name='get_custom_sub_group_name'),
    url(r'^save_assign_student/$',views.SaveAssignStudent, name='save_assign_student'),
    url(r'^assign_student_list/$',views.AssignStudentList, name='assign_student_list'),
    url(r'^custom_group_sub_details/$',views.CustomGroupSubDetails, name='custom_group_sub_details'),
    url(r'^remove_group_sub_group/$',views.RemoveGroupSubGroup, name='remove_group_sub_group'),

    url(r'^override_lead_supervisor/$', views.OverrideLeadSupervisor, name='override_lead_supervisor'),
    url(r'^override_lead_supervisor_cancel/$', views.OverrideLeadSupervisorCancel, name='override_lead_supervisor_cancel'),
    url(r'^save_update_photo/$', views.save_update_photo, name='save_update_photo'),
    url(r'^updating_lead_supervisor/$', views.updating_lead_supervisor, name='updating_lead_supervisor'),
    url(r'^update_confirm_tc/(?P<std_id>[0-9]+)$', views.UpdateConfirmTc, name='update_confirm_tc'),
    url(r'^bulk_delete_custom_group/$', views.BulkDeleteCustomGroup, name='bulk_delete_custom_group'),
    url(r'^get_custom_group_list/$', views.GetCustomGroupList, name='get_custom_group_list'),
    url(r'^update_primary_group/$', views.UpdatePrimaryGroup, name='update_primary_group'),
    url(r'^update_active_group/$', views.UpdateActiveGroup, name='update_active_group'),
    url(r'^view_attendance_section_detail/$',views.ViewAttendanceSectionDetail, name='view_attendance_section_detail'),
    url(r'^updating_parent_contact_number/$',views.updating_parent_contact_number, name='updating_parent_contact_number'),
    url(r'^preview_view_edit_parent/$',views.preview_view_edit_parent, name='preview_view_edit_parent'),
    url(r'^view_status_list/$',views.ViewStatusList, name='view_status_list'),
    url(r'^updated_user_permission/$',views.updated_user_permission, name='updated_user_permission'),
    url(r'^Updated_user_permission_mapping/$',views.Updated_user_permission_mapping, name='Updated_user_permission_mapping'),

   ]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
   


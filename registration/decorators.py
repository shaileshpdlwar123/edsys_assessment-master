from django.http import HttpResponse, HttpResponseRedirect, Http404, HttpResponseForbidden
from django.shortcuts import render,redirect
import json
from django.shortcuts import render_to_response
from django.template import RequestContext
from .models import *
from functools import wraps

from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.core.exceptions import PermissionDenied
from django.shortcuts import resolve_url
from django.utils import six
from django.utils.decorators import available_attrs
from django.utils.six.moves.urllib.parse import urlparse
from django.contrib import messages
from masters.utils import get_all_user_permissions

def user_passes_test(test_func, login_url=None, redirect_field_name=REDIRECT_FIELD_NAME):
    """
    Decorator for views that checks that the user passes the given test,
    redirecting to the log-in page if necessary. The test should be a callable
    that takes the user object and returns True if the user passes.
    """

    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request.user):
                return view_func(request, *args, **kwargs)
            path = request.build_absolute_uri()
            resolved_login_url = resolve_url(login_url or settings.LOGIN_URL)
            # If the login url is the same scheme and net location then just
            # use the path as the "next" url.
            login_scheme, login_netloc = urlparse(resolved_login_url)[:2]
            current_scheme, current_netloc = urlparse(path)[:2]
            if ((not login_scheme or login_scheme == current_scheme) and
                    (not login_netloc or login_netloc == current_netloc)):
                path = request.get_full_path()
            from django.contrib.auth.views import redirect_to_login
            return redirect_to_login(
                path, resolved_login_url, redirect_field_name)
        return _wrapped_view
    return decorator

def user_login_required(view_func):
    def wrap(request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseRedirect("/")
        # elif request.user.is_blocked:
        #     return HttpResponseRedirect("/school/user_blocked")
        # elif not request.user.is_active:
        #     return HttpResponseRedirect("/school/change_password")
        else:
            return view_func(request, *args, **kwargs)

    wrap.__doc__ = view_func.__doc__
    wrap.__name__ = view_func.__name__
    return wrap

def admin_login_required(view_func):
    def wrap(request, *args, **kwargs):
        if not request.user.is_authenticated() or not AuthUser.objects.get(id=request.user.id).is_redbytesAdmin():
            return HttpResponseRedirect("/school/login")
        else:
            return view_func(request, *args, **kwargs)

    wrap.__doc__ = view_func.__doc__
    wrap.__name__ = view_func.__name__
    return wrap

def class_login_required(view_func):
    def wrap(request, *args, **kwargs):
        if not request.user.is_authenticated() or not AuthUser.objects.get(id=request.user.id).is_():
            return HttpResponseRedirect("/school/login")
        else:
            return view_func(request, *args, **kwargs)

    wrap.__doc__ = view_func.__doc__
    wrap.__name__ = view_func.__name__
    return wrap

def user_has_permission(view_func):
    def wrap(request, *args, **kwargs):
        if not request.user.is_authenticated() or not AuthUser.objects.get(id=request.user.id).is_():
            return HttpResponseRedirect("/school/login")
        else:
            return view_func(request, *args, **kwargs)

    # wrap.__doc__ = view_func.__doc__
    # wrap.__name__ = view_func.__name__
    return wrap

def the_decorator(arg1, arg2):

    def _method_wrapper(view_method):

        def _arguments_wrapper(request, *args, **kwargs) :
            """
            Wrapper with arguments to invoke the method
            """

            #do something with arg1 and arg2

            return view_method(request, *args, **kwargs)

        return _arguments_wrapper

    return _method_wrapper

def decorator(arg1, arg2):

    def real_decorator(function):

        def wrapper(*args, **kwargs):
            print ("Congratulations.  You decorated a function that does something with %s and %s" % (arg1, arg2))
            function(*args, **kwargs)
        return wrapper

    return real_decorator

def user_permission_required(perm, login_url=None, raise_exception=False):
    """
    Decorator for views that checks whether a user has a particular permission
    enabled, redirecting to the log-in page if necessary.
    If the raise_exception parameter is given the PermissionDenied exception
    is raised.
    """
    def check_perms(user):
        if isinstance(perm, six.string_types):
            perms = (perm, )
        else:
            perms = perm
        # First check if the user has the permission (even anon users)
        if user.has_perms(perms):
            return True
        # In case the 403 handler should be called raise the exception
        if raise_exception:
            raise PermissionDenied
        # As the last resort, show the login form
        return False
    return user_passes_test(check_perms, login_url=login_url)


def user_group_permission_required(perm, login_url=None, raise_exception=False):
    """
    Decorator for views that checks whether a user has a particular permission
    enabled, redirecting to the log-in page if necessary.
    If the raise_exception parameter is given the PermissionDenied exception
    is raised.
    """
    def check_perms(user):
        if isinstance(perm, six.string_types):
            perms = (perm, )
        else:
            perms = perm
        # First check if the user has the permission (even anon users)
        if user.has_perms(perms) or user.groups.all()[0].permissions.all().filter(name=perm).exists():
            return True
        # In case the 403 handler should be called raise the exception
        if raise_exception:
            raise PermissionDenied
        # As the last resort, show the login form
        return False

    return user_passes_test(check_perms, login_url=login_url)

def pass_test(test_func, login_url=None, redirect_field_name=REDIRECT_FIELD_NAME):
    def class_login_required(view_func):
        def wrap(request, *args, **kwargs):
            if test_func(request.user):
                return view_func(request, *args, **kwargs)

            if not request.user.is_authenticated() or not AuthUser.objects.get(id=request.user.id).is_():
                return HttpResponseRedirect("/school/login")
            else:
                return view_func(request, *args, **kwargs)

        wrap.__doc__ = view_func.__doc__
        wrap.__name__ = view_func.__name__
        return wrap


def the_decorator1(perm, login_url=None, raise_exception=False):

    def _method_wrapper(view_method):

        def _arguments_wrapper(request, *args, **kwargs) :

            if request.user.is_authenticated():

                if isinstance(perm, six.string_types):
                    perms = (perm,)
                else:
                    perms = perm

                if not request.user.has_perms(perms) or not request.user.groups.all()[0].permissions.all().filter(name=perm).exists():
                    return HttpResponseRedirect("/school/login")

                # print request.user.username
            else:
                print 'User not found'
            """
            Wrapper with arguments to invoke the method
            """

            #do something with arg1 and arg2

            return view_method(request, *args, **kwargs)

        return _arguments_wrapper

    return _method_wrapper


def user_permission_required(perm, redirect_url):
    def location_required(f):
        def wrap(request, *args, **kwargs):
            # locations = model_class.objects.filter(user=request.user)

            if isinstance(perm, six.string_types):
                perms = (perm,)[0]
            else:
                perms = perm

            # if not request.user.has_perms(perms):
            if not request.user.is_system_admin() and perms not in get_all_user_permissions(request):
                messages.success(request, "You can't perform this operation because of limited access.")
                return HttpResponseRedirect(redirect_url)
            return f(request, *args, **kwargs)
        wrap.__doc__=f.__doc__
        wrap.__name__=f.__name__
        return wrap
    return location_required

def superuser_login_required(view_func):
    def wrap(request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseRedirect("/school/login")
        elif not request.user.is_superuser:
            return HttpResponseRedirect("/school/home")
        else:
            return view_func(request, *args, **kwargs)

    wrap.__doc__ = view_func.__doc__
    wrap.__name__ = view_func.__name__
    return wrap

def api_login_required(view_func):
    def wrap(request, *args, **kwargs):
        if request.user.is_authenticated() and request.user.is_active:
            # if request.user.is_active:
            #     return HttpResponseRedirect("/school/")
            return view_func(request, *args, **kwargs)
        js = json.dumps({'authenticated': False})
        return HttpResponse(js, content_type='application/json', status=403)
    wrap.__doc__ = view_func.__doc__
    wrap.__dict__ = view_func.__dict__
    return wrap

def api_student_login_required(view_func):
    def wrap(request, *args, **kwargs):
        if request.user.is_authenticated() and request.user.is_active:
            if request.user.is_student():
                return view_func(request, *args, **kwargs)
        js = json.dumps({'authenticated': False})
        return HttpResponse(js, content_type='application/json', status=403)
    wrap.__doc__ = view_func.__doc__
    wrap.__dict__ = view_func.__dict__
    return wrap
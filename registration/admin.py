from django.contrib import admin
from .models import class_details, sections, subjects, subject_substrands, student_details, AuthUser, role, staff_details, parents_details, academic_year, academic_class_section_mapping,subject_strands
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext, ugettext_lazy as _

# Register your models here.
# class CustomUserAdmin(UserAdmin):
#     fieldsets = (
#         (None, {'fields': ('username', 'password')}),
#         (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'is_active', 'role')}),
#     )
#     list_display = ('username', 'email', 'first_name', 'last_name', 'role')
#     list_filter = ('is_staff', 'is_superuser', 'is_active', 'role')
#     search_fields = ('username', 'first_name', 'last_name', 'email')
# admin.site.register(AuthUser, CustomUserAdmin)
#
# admin.site.register(role)
# admin.site.register(class_details)
# admin.site.register(sections)
# admin.site.register(subjects)
# admin.site.register(subject_substrands)
# admin.site.register(student_details)
# admin.site.register(staff_details)
# admin.site.register(parents_details)

# Register your models here.
class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'is_active', 'role')}),
    )
    list_display = ('username', 'email', 'first_name', 'last_name')
    list_filter = ('is_staff', 'is_superuser', 'is_active')
    search_fields = ('username', 'first_name', 'last_name', 'email')
admin.site.register(AuthUser, CustomUserAdmin)

admin.site.register(role)
admin.site.register(class_details)
admin.site.register(sections)
admin.site.register(subjects)
admin.site.register(subject_substrands)
admin.site.register(student_details)
admin.site.register(staff_details)
admin.site.register(parents_details)
admin.site.register(academic_year)
admin.site.register(academic_class_section_mapping)
admin.site.register(subject_strands)

﻿
from django import forms
from registration.models import *
from medical.models import *
from django.contrib.admin.widgets import FilteredSelectMultiple
import itertools
from medical.models import *
from django.contrib.auth import authenticate
from django.contrib.auth.models import Group
from django.contrib.auth.models import Permission
from masters.models import *

class loginForm(forms.Form):
    username = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control form-control-solid placeholder-no-fix' , 'placeholder':'Username','autocomplete': 'off'}))
    password = forms.CharField(required=True,widget=forms.PasswordInput(attrs={'class':'form-control form-control-solid placeholder-no-fix' , 'placeholder':'Password','autocomplete': 'off'}))

    def login(self, request):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        return user

class SchoolDetailsForm(forms.ModelForm):
    
    school_name = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off', 'maxlength':'255'}))
    school_address = forms.CharField(required=False,widget=forms.Textarea(attrs={'class':'form-control' , 'autocomplete': 'off','rows':'2', 'style':'resize:none','maxlength':'255'}))
    contact_num = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control con_num' , 'autocomplete': 'off','onkeypress':'return isNumber(event)', 'maxlength':'16', 'placeholder':"+XXX XXXXXXXXX"}))

    email = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off', 'maxlength':'250'}))
    logo = forms.FileField(required=False, label='Upload Photo' )
    website = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off', 'maxlength':'255'}))
    #street=forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    street = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','rows':'2', 'style':'resize:none','maxlength':'255'}))
    city = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','maxlength':'80'}))
    pin = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','maxlength':'8'}))
    state = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','maxlength':'80'}))
    principal_Photo = forms.FileField(required=False, label='Upload_Photo_principal')
    principal_name = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off', 'maxlength': '255'}))
    principal_signature = forms.FileField(required=False, label='Upload_Photo_signature')
    principal_qr_code = forms.FileField(required=False, label='Upload_qr_code')


    class Meta:
        model = school_details
        fields =['school_name','school_address','contact_num','email','website','logo','city','pin','state','street','principal_Photo','principal_name','principal_signature','principal_qr_code']

        
class StaffDetailsForm(forms.ModelForm):

    first_name = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only ', 'maxlength':'255'}))
    birth_date = forms.DateField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off' ,'type':'date', 'max':"9999-12-31"}))
    gender = forms.TypedChoiceField(
                   
                   choices=(('Male', 'Male'), ('Female', 'Female')),required=True,
                   widget=forms.RadioSelect
                )
    SchoolNames=school_details.objects.all()
    photo =  forms.FileField(required=False,label='Upload Photo' )
    # DepartmentNames= department_details.objects.all()
    #is_active = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    role_type = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    contact_num = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control con_num' , 'autocomplete': 'off','onkeypress':'return isNumber(event)', 'maxlength':'16', 'placeholder':"+XXX XXXXXXXXX"}))
    email = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','maxlength':'250'}))
    # department = forms.ModelChoiceField(required=False,queryset = DepartmentNames, widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    user = forms.ModelChoiceField(required=False,queryset = AuthUser.objects.all(), widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    middle_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only ', 'maxlength':'255'}))
    last_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only ', 'maxlength':'255'}))
    # password=forms.CharField(required=False,widget=forms.PasswordInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    city = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only ','maxlength':'80'}))
    state = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only ','maxlength':'80'}))
    postcode = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','maxlength':'8', 'onkeypress':'return isNumber(event)', 'title':'Enter Numbers Only '}))
    street = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','maxlength':'255','style':'resize:none','title':'No Format'}))
    
    #academic_year_mapping = forms.ModelMultipleChoiceField(required=False,queryset=academic_year.objects.all(), widget=forms.SelectMultiple(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    academic_year_mapping = forms.ModelChoiceField(required=False,queryset = academic_year.objects.all(), widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    #supervisor_name = forms.ModelMultipleChoiceField(required=False,queryset = staff_details.objects.all() ,widget=forms.SelectMultiple(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    supervisor_name = forms.ModelChoiceField(required=False,queryset = AuthUser.objects.all().filter(role__name='Supervisor'), widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))#'''staff_details.objects.filter(role=4)'''
    class_name_mapping = forms.ModelMultipleChoiceField(required=False,queryset=class_details.objects.all(), widget=FilteredSelectMultiple("ClassDetail", is_stacked=False))
    section_name_mapping = forms.ModelMultipleChoiceField(required=False,queryset=sections.objects.all(), widget=FilteredSelectMultiple("Sections", is_stacked=False))
    #section_name_mapping = forms.ModelMultipleChoiceField(required=False,queryset=sections.objects.all(), widget=forms.SelectMultiple(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    
    #teacher_name_mapping = forms.ModelMultipleChoiceField(required=False,queryset=staff_details.objects.all(), widget=forms.SelectMultiple(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    teacher_name_mapping = forms.ModelChoiceField(required=False,queryset = AuthUser.objects.all().filter(role__name='Class Teacher'), widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    subject_name_mapping = forms.ModelMultipleChoiceField(required=False,queryset=subjects.objects.all(), widget=FilteredSelectMultiple("Subject", is_stacked=False))
    
    class Meta:
            model = staff_details
            fields =['photo', 'department', 'user', 'middle_name', 'contact_num', 'birth_date', 'role_type', 'gender'
                     ,'city','state','postcode','street','supervisor_name','class_name_mapping',
                     'section_name_mapping','teacher_name_mapping','subject_name_mapping','academic_year_mapping']

    # def __init__(self, *args, **kwargs):
    #         super(StaffDetailsForm, self).__init__(*args, **kwargs)
    #         self.fields['department'].empty_label = 'Please Select'
                    
class RoleDetailsForm(forms.ModelForm):
    name = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only ', 'maxlength':'80'}))
    
    class Meta:
        model = role
        fields =['name']


class GroupDetailsForm(forms.ModelForm):
    name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only '}))

    class Meta:
        model = Group
        fields = ['name']
        
class AcademicDetailsForm(forms.ModelForm):
    year_code = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[0-9a-zA-Z- ]+$', 'title':'Only alphanumeric characters are allowed', 'maxlength':'32'}))
    year_name = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[0-9a-zA-Z- ]+$', 'title':'Only alphanumeric characters are allowed','maxlength':'32'}))
    start_date = forms.DateField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off' ,'type':'date','max':"9999-12-31"}))
    end_date = forms.DateField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off' ,'type':'date','max':"9999-12-31"}))
    #current_academic_year = forms.BooleanField(required=False,widget=forms.CheckboxInput(attrs={'class':'your_class'}))
    yearname_mapping =  forms.ModelChoiceField(required=False,queryset = academic_year.objects.all() ,initial=0,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    class_name_mapping = forms.ModelMultipleChoiceField(required=False,queryset=class_details.objects.all(), widget=FilteredSelectMultiple("ClassDetails", is_stacked=False))
    section_name_mapping = forms.ModelMultipleChoiceField(required=False,queryset=sections.objects.all(), widget=FilteredSelectMultiple("Sections", is_stacked=False))
    
    class Meta:
        model = academic_year
        fields =['year_code','year_name','start_date','end_date','yearname_mapping','class_name_mapping','section_name_mapping']


####--------- MAPPING-----------------#######################
class AcademicClassSectionMappingForm(forms.ModelForm):
    # crnt_acd = forms.DateField(required=False, queryset=academic_year.objects.get(current_academic_year=1))
    year_name = forms.ModelChoiceField(required=True, queryset=academic_year.objects.all(), initial=0,widget=forms.Select( attrs={'class': 'form-control', 'autocomplete': 'off'}))
    class_name = forms.ModelMultipleChoiceField(required=True, queryset=class_details.objects.all(), widget=FilteredSelectMultiple("ClassDetails", is_stacked=False))
    section_name = forms.ModelMultipleChoiceField(required=True, queryset=sections.objects.all(), widget=FilteredSelectMultiple("Sections", is_stacked=False))

    class Meta:
        model = academic_class_section_mapping
        fields = ['year_name', 'class_name','section_name']

class MandatorySubjectMapping(forms.ModelForm):
    academic_class_section_mapping =  forms.ModelMultipleChoiceField(required=True, queryset=academic_class_section_mapping.objects.all(), widget=FilteredSelectMultiple("AcademicClassSectionMappingForm", is_stacked=False))
    subject = forms.ModelMultipleChoiceField(required=True,queryset=subjects.objects.all(), widget=FilteredSelectMultiple("Subject", is_stacked=False))
    class_name_mapping = forms.ModelMultipleChoiceField(required=True, queryset=class_details.objects.all(), widget=forms.SelectMultiple( attrs={'class': 'form-control', 'autocomplete': 'off'}))
    subject_name_mapping = forms.ModelMultipleChoiceField(required=True, queryset=subjects.objects.all(), widget=FilteredSelectMultiple("Subject", is_stacked=False))
    section_name_mapping = forms.ModelMultipleChoiceField(required=True, queryset=sections.objects.all(), widget=forms.SelectMultiple( attrs={'class': 'form-control', 'autocomplete': 'off'}))

    class Meta:
        model = mandatory_subject_mapping
        fields = ['academic_class_section_mapping', 'subject']

class ClassDetailsForm(forms.ModelForm):
     class_code = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','maxlength':'10','title':'Only Alpha-Numeric is Allowed.','pattern':'^[a-zA-Z0-9 ]{1,10}$' }))
     class_name=forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','maxlength':'32', 'title':'Only Alpha-Numeric is Allowed.','pattern':'^[a-zA-Z0-9 ]{1,32}$'}))
     class_name_mapping = forms.ModelMultipleChoiceField(required=False,queryset = class_details.objects.all(), widget=forms.SelectMultiple(attrs={'class':'form-control' , 'autocomplete': 'off'}))
     # previous_class = forms.ModelMultipleChoiceField(required=False, queryset = class_details.objects.all(), widget=FilteredSelectMultiple('ClassDetailsForm', is_stacked=False))
     subject_name_mapping = forms.ModelMultipleChoiceField(required=False,queryset=subjects.objects.all(), widget=FilteredSelectMultiple("Subject", is_stacked=False))
     section_name_mapping = forms.ModelMultipleChoiceField(required=False,queryset=sections.objects.all(), widget=forms.SelectMultiple(attrs={'class':'form-control' , 'autocomplete': 'off'}))
     
     class Meta:
        model = class_details
        fields =['class_code','class_name','class_name_mapping','subject_name_mapping','section_name_mapping']
     
     # def __init__(self, *args, **kwargs):
     #
     #     super(ClassDetailsForm, self).__init__(*args, **kwargs)
     #
     #     finalDict1=[]
     #     queryset1 = subjects.objects.filter(type = "Mandatory")
     #     finalDict1.append(list(itertools.chain(queryset1)))
     #     cnt=0
     #     k=[]
     #     for r in finalDict1:
     #        if len(r)!=0:
     #         for s in r:
     #             print (s)
     #             k.append(s)
     #     self.fields['subject_name_mapping'].queryset = subjects.objects.filter(id__in=[r.id for r in k])
#          self.fields['section_name_mapping'].queryset = sections.objects.none()
#          if self.is_bound:
#             self.fields['section_name_mapping'].queryset = sections.objects.all()  
                
class SectionDetailsForm(forms.ModelForm):
    sections_type_obj = section_type_details.objects.all()
    section_name = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','maxlength':'32', 'title':'Only Alphabets is Allowed.','pattern':'^[a-zA-Z ]{1,32}$'}))
    section_code = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','maxlength':'10', 'title':'Only Alpha-Numeric is Allowed.','pattern':'^[a-zA-Z0-9 ]{1,10}$' }))
    section_type = forms.ModelChoiceField(required=True, queryset=sections_type_obj,empty_label='Please Select',widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    class Meta:
        model = sections
        fields =['section_name','section_code','section_type']
        
class SubjectDetailsForm(forms.ModelForm):
    subject_code = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','maxlength':'32', 'title':'Only Alpha-Numeric is Allowed With  "-" "_"','pattern':'^[\w](?!\d*[-_]+\d*$)[ \w-]+$' }))
    subject_name = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','maxlength':'32','title':'Only Alpha-Nnumeric is Allowed With  "-" "_"','pattern':'^[A-Za-z](?!\d*[-_]\d*$)[ \w-]*$'}))
    
    class Meta:
        model = subjects
        fields =['subject_code', 'subject_name', 'type']
        
class SubjectSubstrandsForm(forms.ModelForm):
    substrands_name = forms.CharField(widget=forms.TextInput(attrs={'class':'substrand_value' , 'autocomplete': 'off', 'maxlength':'32', 'pattern':'[A-Za-z ]+', 'title':'Only Alphabets is Allowed '}))
    substrands_code = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control col-6 col-sm-3' , 'autocomplete': 'off'}))
    class Meta:
        model = subject_substrands
        fields =['substrands_name' , 'substrands_code']

class StudentDetailsForm(forms.ModelForm):
    ClassDetail = class_details.objects.all()
    Sections = sections.objects.all()
    ParentsDetails = parents_details.objects.filter(user__is_active=True)
    AcademicYear = academic_year.objects.all()
    Nationality = nationality.objects.all()
    Religion = religion.objects.all()
    NationalityGroup = nationality_group.objects.all()
    house_rec = house_master.objects.all()

    first_name = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off', 'maxlength':'255'}))
    birth_date = forms.DateField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off' ,'type':'date', 'max':"9999-12-31"}))
    
    gender = forms.TypedChoiceField(
                   
                   choices=(('Male', 'Male'), ('Female', 'Female')),
                   widget=forms.RadioSelect
                )


    emirati = forms.TypedChoiceField(

        choices=(('yes', 'Yes'), ('no', 'No')),
        widget=forms.RadioSelect
    )

    select = forms.TypedChoiceField(

        choices=(('yes', 'Yes'), ('no', 'No')),
        widget=forms.RadioSelect
    )

    sen = forms.TypedChoiceField(

        choices=(('yes', 'Yes'), ('no', 'No')),
        widget=forms.RadioSelect
    )
    # transport = forms.TypedChoiceField(
    #
    #     choices=(('own', 'Own'), ('school', 'School')),
    #     widget=forms.RadioSelect
    # )

    transport = forms.TypedChoiceField(
        choices=(('own', 'Own Transport'), ('school', 'School Transport')),
        widget=forms.RadioSelect
    )




    house_name = forms.ModelChoiceField(required=False, queryset=house_rec,
                                      widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))

    student_code = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z0-9 ]+', 'title':'Enter Characters Only '}))
    class_name = forms.ModelChoiceField(required=True,queryset = ClassDetail ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    mother_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only ', 'maxlength':'255'}))
    father_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only ', 'maxlength':'255'}))
    street = forms.CharField(required=False,widget=forms.Textarea(attrs={'class':'form-control' , 'autocomplete': 'off','rows':'2', 'style':'resize:none','maxlength':'255','title':'No Format'}))
    city = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only','maxlength':'80'}))
    state = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only','maxlength':'80'}))
    postcode = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','maxlength':'8', 'onkeypress':'return isNumber(event)', 'title':'Enter Numbers Only '}))
    email = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off', 'maxlength':'250'}))
    last_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only ', 'maxlength':'255'}))
    photo =  forms.FileField(required=False,label='Upload Photo' )
    section_name = forms.ModelChoiceField(required=True ,queryset = Sections ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    joining_date = forms.DateField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off' ,'type':'date', 'max':"9999-12-31"}))
    year_name = forms.ModelChoiceField(required=True,queryset = AcademicYear ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    parent = forms.ModelChoiceField(required=False,queryset = ParentsDetails ,widget=forms.Select(attrs={'class':'form-control selectpicker show-tick' , 'autocomplete': 'off','data-live-search':'true'}))
    parent_mail = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    blood_group = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control viewOnlyAccess' , 'autocomplete': 'off'}))
    height = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control viewOnlyAccess' , 'autocomplete': 'off'}))
    weight = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control viewOnlyAccess' , 'autocomplete': 'off'}))
    under_medication = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control viewOnlyAccess' , 'autocomplete': 'off'}))
    allergic = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control selectpicker show-tick viewOnlyAccess' , 'autocomplete': 'off','data-live-search':'true'}))
    nationality = forms.ModelChoiceField(required=True,queryset = Nationality ,widget=forms.Select(attrs={'class':'form-control selectpicker show-tick' , 'autocomplete': 'off','data-live-search':'true'}))
    religion = forms.ModelChoiceField(required=True,queryset = Religion ,widget=forms.Select(attrs={'class':'form-control selectpicker show-tick' , 'autocomplete': 'off','data-live-search':'true'}))
    
    
    religion_mapping = forms.ModelMultipleChoiceField(required=False ,queryset=Religion ,widget=forms.SelectMultiple(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    nationality_mapping = forms.ModelMultipleChoiceField(required=False ,queryset =Nationality ,widget=forms.SelectMultiple(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    class_name_mapping = forms.ModelMultipleChoiceField(required=False , queryset = class_details.objects.all() ,widget=forms.SelectMultiple(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    section_name_mapping = forms.ModelMultipleChoiceField(required=False ,queryset = sections.objects.all() ,widget=forms.SelectMultiple(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    subject_name_mapping = forms.ModelMultipleChoiceField(required=False,queryset=subjects.objects.all(), widget=FilteredSelectMultiple("Subject", is_stacked=False))
    student_name_mapping = forms.ModelMultipleChoiceField(required=False,queryset=student_details.objects.all(), widget=FilteredSelectMultiple("StudentDetails", is_stacked=False))
    nationality_group_mapping = forms.ModelMultipleChoiceField(required=False, queryset=NationalityGroup, widget=forms.SelectMultiple(
        attrs={'class': 'form-control', 'autocomplete': 'off'}))

    bus_no = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control transport_class', 'autocomplete': 'off', 'pattern': '[A-Za-z0-9 ]+',
               'title': 'Enter Characters Only ', 'maxlength': '32'}))


    pick_up_time = forms.TimeField(required=False,widget=forms.TimeInput(
        attrs={'class': 'form-control transport_class col-sm-3 tiadd_studentme ', 'type': 'time','format':'%I:%M %p', 'data-fv-time': 'true'}))

    pick_up_point = forms.CharField(required=False, widget=forms.Textarea(
        attrs={'class': 'form-control transport_class', 'autocomplete': 'off', 'rows': '2', 'style': 'resize:none',
               'maxlength': '32'}))

    drop_bus_no = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control transport_class', 'autocomplete': 'off', 'pattern': '[A-Za-z0-9 ]+',
               'title': 'Enter Characters Only ', 'maxlength': '32'}))

    drop_off_point = forms.CharField(required=False, widget=forms.Textarea(
        attrs={'class': 'form-control transport_class', 'autocomplete': 'off', 'rows': '2', 'style': 'resize:none',
               'maxlength': '32'}))
    # drop_off_time = forms.TimeField(required=True, widget=forms.TextInput(
    #     attrs={'class': 'form-control', 'autocomplete': 'off', 'type': 'time'}))

    # drop_off_time = forms.CharField(required=True, widget=forms.TextInput(
    #     attrs={'class': 'form-control input-small transport_class', 'autocomplete': 'off',
    #            }))

    # drop_off_time = forms.CharField(required=True, widget=forms.TextInput(
    #     attrs={'class': 'form-control transport_class', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
    #            'title': 'Enter Characters Only ', 'maxlength': '255'}))
    # drop_off_time = forms.TimeField(widget=forms.TextInput(attrs={'class': 'timepicker'}))
    # drop_off_time = forms.DateField(widget=forms.DateInput(attrs={'class': 'timepicker'}))

    # gender_mapping = forms.ModelMultipleChoiceField(required=False, queryset=Nationality,
    #                                                      widget=forms.SelectMultiple(
    #                                                          attrs={'class': 'form-control', 'autocomplete': 'off'}))
    # from django.contrib.admin.widgets import AdminTimeWidget
    drop_off_time = forms.TimeField(required=False,widget=forms.TimeInput(
        attrs={'class': 'form-control transport_class col-sm-3 time ', 'format':'%I:%M %p','type':'time','data-fv-time': 'true'}))
    emergency_contact_num = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'onkeypress': 'return isNumber(event)',
               'maxlength': '16', 'placeholder': "+XXX XXXXXXXXX"}))

    middle_name = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only ', 'maxlength': '255'}))

    odoo_id = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control ', 'autocomplete': 'off',
                'maxlength': '32'}))

    roll_no = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control ', 'autocomplete': 'off',
               'maxlength': '32'}))

    class Meta:
        model = student_details
        fields =['first_name','birth_date','gender','emirati','select','sen','transport','house_name','student_code','class_name','mother_name','father_name','street','city','state','postcode',
                 'email','last_name','photo','section_name','joining_date','year_name','parent',
                 'parent_mail' , 'blood_group','height','weight','under_medication','allergic','nationality','religion',
                 'religion_mapping','nationality_mapping','class_name_mapping','section_name_mapping','subject_name_mapping','student_name_mapping','nationality_group_mapping','bus_no','pick_up_time','pick_up_point','drop_bus_no','drop_off_time','drop_off_point','emergency_contact_num','middle_name','odoo_id','roll_no']

from django.forms.models import BaseModelFormSet

class OneRequiredFormSet(BaseModelFormSet):
    def _construct_form(self, i, **kwargs):
        f = super(OneRequiredFormSet, self)._construct_form(i, **kwargs)
        if i == 0:
            f.empty_permitted = False
            f.required = True
        return f

class SecondaryParentDetailsForm(forms.ModelForm): 
    parenttype = (('Select','Select' ), ('Parent','Parent') ,('Guardian','Guardian'))
    sec_parent_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control col-6 col-sm-3' , 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only ', 'maxlength':'255'}))
    sec_parent_mail = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control col-6 col-sm-3' , 'autocomplete': 'off', 'maxlength':'250'}))
    sec_parent_contact = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control col-6 col-sm-3' , 'autocomplete': 'off','onkeypress':'return isNumber(event)', 'maxlength':'16', 'placeholder':"+XXX XXXXXXXXX"}))
    type = forms.ChoiceField(required=False,choices = parenttype , widget=forms.Select(attrs={'class':'form-control col-6 col-sm-3'}))
    sec_photo = forms.FileField(required=False, label='Upload Photo', widget=forms.FileInput(attrs={'class':"abc"}))
    class Meta:
        model = secondary_parent_details
        fields =['sec_parent_name','sec_parent_mail','sec_parent_contact','type','sec_photo']
        

   
        
class ParentsDetailsForm(forms.ModelForm):
    first_name = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off',  'maxlength':'255'}))
    contact_num = forms.CharField(required=True, widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','onkeypress':'return isNumber(event)', 'maxlength':'16', 'placeholder':"+XXX XXXXXXXXX"}))
    birth_date = forms.DateField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off' ,'type':'date', 'max':"9999-12-31"}))
    gender = forms.TypedChoiceField(
        required=False,
                   choices=((False, 'Male'), (True, 'Female')),
                   widget=forms.RadioSelect
                )
    street = forms.CharField(required=False,widget=forms.Textarea(attrs={'class':'form-control' , 'autocomplete': 'off','rows':'2', 'style':'resize:none','maxlength':'255','title':'No Format'}))
    email = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off', 'maxlength':'250'}))
    code = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off', 'pattern':'[A-Za-z0-9 ]+', 'title':'Only Characters and Numbers are allowed '}))
    middle_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off', 'title':'Enter Characters Only ', 'maxlength':'255'}))
    last_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off', 'title':'Enter Characters Only ', 'maxlength':'255'}))
    password = forms.CharField(required=False,widget=forms.PasswordInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    city = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only ','maxlength':'80'}))
    state = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' ,'maxlength':'80', 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only '}))
    postcode = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','title':'Enter Numbers Only ','maxlength':'8', 'onkeypress':'return isNumber(event)'}))
    photo = forms.FileField(required=False, label='Upload Photo')

    mother_name = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only ', 'maxlength': '255'}))
    mother_email = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'maxlength': '250'}))

    mother_contact_num = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'onkeypress': 'return isNumber(event)',
               'maxlength': '16', 'placeholder': "+XXX XXXXXXXXX"}))

    mother_middle_name = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only ', 'maxlength': '255'}))

    mother_last_name = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only ', 'maxlength': '255'}))

    class Meta:
        model = parents_details
        fields =['first_name','birth_date','gender','street','email','code','middle_name','last_name','contact_num','city','state','postcode','photo','mother_name','mother_email','mother_contact_num','mother_middle_name','mother_last_name']

class ParentsExcelForm(forms.ModelForm):
    excel = forms.FileField(required=False, label='Upload File')
    class Meta:
        model = parents_details_excel
        fields = ['excel']

class ClassSectionDetailsForm(forms.ModelForm):

    SectionDetail = sections.objects.all()
    ClassDetail = class_details.objects.all()
    
    class_name = forms.ModelChoiceField(required=False , queryset = ClassDetail ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    section_name = forms.ModelChoiceField(required=False ,queryset = SectionDetail ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
  
  
    class Meta:
        model = student_medical
        fields =['class_name','section_name']
        
class RoleUserMappingForm(forms.ModelForm):
    role_name_recs = role.objects.exclude(name__in=['Parent'])
    username = forms.ModelMultipleChoiceField(required=True,queryset=AuthUser.objects.filter(is_active=True).exclude(role__isnull=False), widget=FilteredSelectMultiple("StaffDetails", is_stacked=False))
    name = forms.ModelChoiceField(required=True,queryset = role_name_recs ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}),empty_label = 'Please Select')

    class Meta:
        model = role
        fields = ['username','name']

class GroupUserMappingForm(forms.ModelForm):
    role_name_recs = Group.objects.exclude(name__in=['Parent', 'admin'])
    username = forms.ModelMultipleChoiceField(required=True,queryset=AuthUser.objects.exclude(role__isnull=False), widget=FilteredSelectMultiple("StaffDetails", is_stacked=False))
    name = forms.ModelChoiceField(required=True,queryset = role_name_recs ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}),empty_label = 'Please Select')

    class Meta:
        model = Group
        fields = ['username','name']
        
class UserPermissionMappingForm(forms.ModelForm):
    user_recs = AuthUser.objects.all()
    name = forms.ModelMultipleChoiceField(required=True,queryset=Permission.objects.all(), widget=FilteredSelectMultiple("StaffDetails", is_stacked=False))
    username = forms.ModelChoiceField(required=True,queryset = user_recs ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}),empty_label = 'Please Select')

    class Meta:
        model = Permission
        fields = ['name','username']

class ClassTeacherMappingForm(forms.ModelForm):
    
    academic_year_recs = academic_year.objects.all()
    class_recs = class_details.objects.all()
    section_recs = sections.objects.all()
    class_teacher_recs = AuthUser.objects.filter(role__name='Class Teacher', is_active=True)
    assistant_teacher_recs = AuthUser.objects.filter(role__name='Assistant Teacher', is_active=True)
    year_name = forms.ModelChoiceField(required=True,queryset = academic_year_recs ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    class_name = forms.ModelChoiceField(required=True,queryset = class_recs ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    section_name = forms.ModelChoiceField(required=True,queryset = section_recs ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    teacher_name = forms.ModelChoiceField(required=True,queryset = AuthUser.objects.filter(role__name='Class Teacher', is_active=True) ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    assistant_teacher_name = forms.ModelChoiceField(required=False,queryset = AuthUser.objects.filter(role__name='Assistant Teacher', is_active=True) ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    class Meta:
        model = academic_class_section_mapping
        fields = ['year_name','class_name','section_name','teacher_name','assistant_teacher_name']
   
    def __init__(self, *args, **kwargs):
            super(ClassTeacherMappingForm, self).__init__(*args, **kwargs)
            self.fields['class_name'].empty_label = 'Please Select'
            self.fields['section_name'].empty_label = 'Please Select'
            self.fields['teacher_name'].empty_label = 'Please Select'
            self.fields['assistant_teacher_name'].empty_label = 'Please Select'
    
# class AcademicyrClassSectionMappingForm(forms.ModelForm):
#     yearname =  forms.ModelChoiceField(queryset = academic_year.objects.all() ,initial=0,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
#     class_name = forms.ModelMultipleChoiceField(queryset=class_details.objects.all(), widget=FilteredSelectMultiple("ClassDetails", is_stacked=False))
#     section_name = forms.ModelMultipleChoiceField(queryset=sections.objects.all(), widget=FilteredSelectMultiple("Sections", is_stacked=False))
#     
#     class Meta:
#         model = academic_year
#         fields =['yearname','class_name','section_name']

class SupervisorClassMappingForm(forms.ModelForm):


    academic_year_mapping = forms.ModelChoiceField(required=True, queryset=academic_year.objects.all(),
                                                   widget=forms.Select(
                                                       attrs={'class': 'form-control', 'autocomplete': 'off'}))
    # supervisor_name = forms.ModelMultipleChoiceField(required=False,queryset = staff_details.objects.all() ,widget=forms.SelectMultiple(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    supervisor_name = forms.ModelChoiceField(required=True, queryset=AuthUser.objects.filter(role__name='Supervisor', is_active=True),
                                             widget=forms.Select(
                                                 attrs={'class': 'form-control', 'autocomplete': 'off'}))
    class_name_mapping = forms.ModelMultipleChoiceField(required=True, queryset=class_details.objects.all(),
                                                        widget=FilteredSelectMultiple("ClassDetail", is_stacked=False))
    section_name_mapping = forms.ModelMultipleChoiceField(required=True, queryset=sections.objects.none(),
                                                          widget=FilteredSelectMultiple("Sections", is_stacked=False))
    # section_name_mapping = forms.ModelMultipleChoiceField(required=False,queryset=sections.objects.all(), widget=forms.SelectMultiple(attrs={'class':'form-control' , 'autocomplete': 'off'}))

    # teacher_name_mapping = forms.ModelMultipleChoiceField(required=False,queryset=staff_details.objects.all(), widget=forms.SelectMultiple(attrs={'class':'form-control' , 'autocomplete': 'off'}))

    subject_name_mapping = forms.ModelMultipleChoiceField(required=True, queryset=subjects.objects.all(),
                                                          widget=FilteredSelectMultiple("Subject", is_stacked=False))


    class Meta:
        model = academic_supervisor_class_section_mapping
        fields = ['academic_class_section_mapping','subject','supervisor_id']
        
    def __init__(self, *args, **kwargs):
            super(SupervisorClassMappingForm, self).__init__(*args, **kwargs)
            self.fields['supervisor_name'].empty_label = 'Please Select'



class TeacherSubjectMappingForm(forms.ModelForm):
    academic_year_mapping = forms.ModelChoiceField(required=True, queryset=academic_year.objects.all(),
                                                   widget=forms.Select(
                                                       attrs={'class': 'form-control', 'autocomplete': 'off'}))
    # supervisor_name = forms.ModelMultipleChoiceField(required=False,queryset = staff_details.objects.all() ,widget=forms.SelectMultiple(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    class_name_mapping = forms.ModelMultipleChoiceField(required=True, queryset=class_details.objects.all(),
                                                        widget=FilteredSelectMultiple("ClassDetail", is_stacked=False))
    section_name_mapping = forms.ModelMultipleChoiceField(required=True, queryset=sections.objects.none(),
                                                          widget=FilteredSelectMultiple("Sections", is_stacked=False))
    # section_name_mapping = forms.ModelMultipleChoiceField(required=False,queryset=sections.objects.all(), widget=forms.SelectMultiple(attrs={'class':'form-control' , 'autocomplete': 'off'}))

    teacher_name_mapping = forms.ModelChoiceField(required=True, queryset=AuthUser.objects.exclude(role__isnull=True).filter(role__name__in=['Class Teacher', 'Assistant Teacher', 'Subject Teacher', ]).filter(is_active=True),
                                                  widget=forms.Select(
                                                      attrs={'class': 'form-control', 'autocomplete': 'off'}))

    subject_name_mapping = forms.ModelMultipleChoiceField(required=True, queryset=subjects.objects.all(),
                                                          widget=FilteredSelectMultiple("Subject", is_stacked=False))

    class Meta:
        model = teacher_subject_mapping
        fields = ['academic_class_section_mapping','subject','staff_id']
        
    def __init__(self, *args, **kwargs):
            super(TeacherSubjectMappingForm, self).__init__(*args, **kwargs)
            self.fields['teacher_name_mapping'].empty_label = 'Please Select'


class ChangePasswordForm(forms.Form):

    current_password = forms.CharField(label=(u'Current Password'), widget=forms.PasswordInput(render_value=False))
    current_password.widget.attrs['placeholder'] = 'Current Password'
    current_password.widget.attrs['class'] = 'form-control'

    password = forms.CharField(label=(u'Password'), widget=forms.PasswordInput(render_value=False))
    password.widget.attrs['placeholder'] = 'Password'
    password.widget.attrs['class'] = 'form-control'

    password1 = forms.CharField(label=(u'Verify Password'), widget=forms.PasswordInput(render_value=False))
    password1.widget.attrs['placeholder'] = 'Confirm Password'
    password1.widget.attrs['class'] = 'form-control'

    def clean(self):
        if self.cleaned_data.get('current_password') is None:
            raise forms.ValidationError("Current password is necessary")
        if self.cleaned_data.get('password') is None:
            raise forms.ValidationError("New password is necessary")
        if self.cleaned_data.get('password1') is None:
            raise forms.ValidationError("Confirm password is necessary")

        if self.cleaned_data['password'] != self.cleaned_data['password1']:
            raise forms.ValidationError("The passwords did not match.")
        return self.cleaned_data


class StudentsExcelForm(forms.ModelForm):
    excel = forms.FileField(required=True, label='Upload File')
    class Meta:
        model = students_details_excel
        fields = ['excel']

class ImportParentRecordsExcelForm(forms.ModelForm):
    excel = forms.FileField(required=True, label='Upload File')
    class Meta:
        model = import_parent_recs_details_excel
        fields = ['excel']

class ImportUserRecordsExcelForm(forms.ModelForm):
    excel = forms.FileField(required=True, label='Upload File')
    class Meta:
        model = import_user_recs_details_excel
        fields = ['excel']

class ImportHouseExcelForm(forms.ModelForm):
    excel = forms.FileField(required=True, label='Upload File')
    class Meta:
        model = sz_import_house_info_excel
        fields = ['excel']

class ImportTransportExcelForm(forms.ModelForm):
    excel = forms.FileField(required=True, label='Upload File')
    class Meta:
        model = sz_import_transport_info_excel
        fields = ['excel']


class AssignStudentForm(forms.ModelForm):
    custom_groups_recs = custom_groups.objects.filter(is_group_active=True)
    custom_sub_groups_recs = custom_sub_groups.objects.all()
    custom_group_id = forms.ModelChoiceField(required=True, queryset=custom_groups_recs, widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    custom_sub_group_id = forms.ModelChoiceField(required=False, queryset=custom_sub_groups_recs, widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    class Meta:
        model = assign_group_student
        fields =['custom_group_id','custom_sub_group_id']
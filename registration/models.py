from __future__ import unicode_literals
import django
from django.conf import settings
from django.db import models
from datetime import datetime
import time
import os
from django.contrib.auth.models import Group, Permission
from django.contrib.auth.models import AbstractUser
from django.db.models import Q
import random
import string
from masters.models import nationality_group,house_master,section_type_details
from validate_email import validate_email

time =(    ('',''),    ('am','AM'),    ('pm','PM'),)


#=================== Start: OneToMAny =============================
class OneToManyField(models.ManyToManyField):
    """A forgein key field that behaves just like djangos ManyToMany field,
    the only difference is that an instance of the other side can only be 
    related to one instance of your side. Also see the test cases.
    """
    def contribute_to_class(self, cls, name):
        # Check if the intermediate model will be auto created.
        # The intermediate m2m model is not auto created if:
        #  1) There is a manually specified intermediate, or
        #  2) The class owning the m2m field is abstract.
        #  3) The class owning the m2m field has been swapped out.
        auto_intermediate = False
        if not self.rel.through and not cls._meta.abstract and not cls._meta.swapped:
            auto_intermediate = True
        
        #One call super contribute_to_class and have django create the intermediate model.
        super(OneToManyField, self).contribute_to_class(cls, name)

        if auto_intermediate == True:
            #Set unique_together to the 'to' relationship, this ensures a OneToMany relationship.
            self.rel.through._meta.unique_together = ((self.rel.through._meta.unique_together[0][1],),)

#=================== End: OneToMAny =============================

# Create your models here.

class role(models.Model):
    name = models.CharField(max_length=250)
    create_date = models.DateTimeField(default=datetime.now)
    update_date = models.DateTimeField(auto_now=True)
    role_permission = models.ManyToManyField(Permission) #
    # role_permission = models.ForeignKey(Permission, null=True, blank=True, related_name='role_permission_mapping', on_delete=models.PROTECT)
    # role_permission = models.ForeignKey(Group, null=True, blank=True, related_name='role_permission_mapping', on_delete=models.PROTECT)

    def __unicode__(self):
        return self.name


# class role_permission_mapping(models.Model):
#     role_id = models.ForeignKey(role, null=True, blank=True, related_name='role_relation', on_delete=models.PROTECT)
#     role_permission = models.ForeignKey(Permission, null=True, blank=True, related_name='role_permission_mapping',
#                                         on_delete=models.PROTECT)

class AuthUser(AbstractUser):

    ID = 'user_id'
    USERNAME = 'username'
    PASSWORD = 'password'
    FIRST_NAME = 'first_name'
    MIDDLE_NAME = 'middle_name'
    LAST_NAME = 'last_name'
    EMAIL = 'email'
    IS_ACTIVE = 'is_active'
    LAST_LOGIN = 'last_login'
    ROLE = 'role'

    SYSTEAM_ADMIN = 'System Admin'#Group.objects.filter(Q(name__istartswith='System Admin') | Q(name__istartswith='system admin') | Q(name='System Admin'))[0]
    CLASS_TEACHER = 'Class Teacher'#Group.objects.filter(Q(name__istartswith='Class Teacher') | Q(name__istartswith='class teacher') | Q(name='Class Teacher'))[0]
    ASSISTANT_TEACHER = 'Assistant Teacher'#Group.objects.filter(Q(name__istartswith='Assistant Teacher') | Q(name__istartswith='assistant teacher') | Q(name='Assistant Teacher'))[0]
    SUBJECT_TEACHER = 'Subject Teacher'#Group.objects.filter(Q(name__istartswith='Subject Teacher') | Q(name__istartswith='subject teacher') | Q(name='Subject Teacher'))[0]
    OFFICER = 'Officer'#Group.objects.filter(Q(name__istartswith='Officer') | Q(name__istartswith='officer') | Q(name='Officer'))[0]
    PARENT = 'Parent'#Group.objects.filter(Q(name__istartswith='Parent') | Q(name__istartswith='parent') | Q(name='Parent'))[0]
    SUPERVISOR = 'Supervisor'#Group.objects.filter(Q(name__istartswith='Supervisor') | Q(name__istartswith='supervisor') | Q(name='Supervisor'))[0]
    ADMIN = 'Admin'
    Registar = 'Registar'
    Medical_Staff = 'Medical Staff'
    Medical_Inspector = 'Medical Inspector'
    Transport_Staff = 'Transport Staff'
    STUDENT = 'student'

    first_name = models.CharField(max_length=256)
    middle_name = models.CharField(max_length=256, blank=True, null=True)
    last_name = models.CharField(max_length=256, blank=True, null=True)
    encoded_pwd = models.CharField(max_length=100, blank=True, null=True)
    last_login = models.DateTimeField(blank=True, null=True)
    role = models.ManyToManyField(role, related_name='user_role')
    connector_is_active = models.BooleanField(default=False)
    # role = models.ForeignKey(role, null=True, blank=True, related_name='user_role', on_delete=models.PROTECT)

    class Meta:
        managed = True
        db_table = 'auth_user'

        permissions = (
            ('can_view_manage_user', 'can view manage user'),
            ('can_view_add_user', 'can view add user'),
            ('can_view_view_edit_user', 'can view view edit user'),
            ('can_view_manage_role', 'can view manage role'),
            ('can_view_role_user_mapping', 'can view role user mapping'),
            ('can_view_user_permission_mapping', 'can view user permission mapping'),
            ('can_view_academic_school', 'can view academic school'),
            ('can_view_class_section', 'can view class section'),
            ('can_view_parent_student', 'can view parent student'),
            ('can_view_promotion', 'can view promotion'),
            ('can_view_id_cards', 'can view id cards'),
            ('can_view_settings', 'can view settings'),
            ('can_view_assessment', 'can view assessment'),
            ('can_view_report_card', 'can view report card'),
            ('can_view_parent_approve_reject_request', 'can view parent approve reject request'),
        )

    @staticmethod
    def get_excel_instance(user_detail_form, user=None, generate_password=False):

        if user:
            user = AuthUser.objects.get(id=user.id)
        else:
            user = AuthUser()
            user.username = user_detail_form['Email']
            # user.email = user_detail_form.cleaned_data[AuthUser.EMAIL]

        if user_detail_form['Email']:
            user.email = user_detail_form['Email']
            # user.email = user_detail_form[AuthUser.EMAIL]

        if user_detail_form['First Name']:
            user.first_name = user_detail_form['First Name']

        if user_detail_form['Middle Name']:
            user.middle_name = user_detail_form['Middle Name']
        else:
            user.middle_name = ''

        if user_detail_form['Last Name']:
            user.last_name = user_detail_form['Last Name']
        else:
            user.last_name = ''

        if generate_password:
            user.password = 'redbytes'

        if user_detail_form['Contact Number']:
            user.contact_num = user_detail_form['Contact Number']
        else:
            user.contact_num = ''

        if user_detail_form['Birth Date']:
            user.birth_date = user_detail_form['Birth Date']

        if user_detail_form['Gender']:
            user.gender = user_detail_form['Gender']

        # if profile_pic:
        #     staff_detail_obj.photo = profile_pic

        if user_detail_form['Street']:
            user.street = user_detail_form['Street']
        else:
            user.street = ''

        if user_detail_form['City']:
            user.city =user_detail_form['City']
        else:
            user.city = ''

        if user_detail_form['State']:
            user.state =user_detail_form['State']
        else:
            user.state = ''

        if user_detail_form['Post Code']:
            user.postcode =user_detail_form['Post Code']
        else:
            user.postcode = ''
        if user_detail_form.has_key('Department Name'):
            if user_detail_form['Department Name']:
                user.department = department_details.objects.get(depatment_name=user_detail_form['Department Name'])

        return user

    @staticmethod
    def get_parent_excel_instance(user_detail_form, user=None, generate_password=False):

        if user:
            user = AuthUser.objects.get(id=user.id)
        else:
            user = AuthUser()
            user.username = user_detail_form['Father Email Id']
            # user.email = user_detail_form.cleaned_data[AuthUser.EMAIL]

        if user_detail_form['Father Email Id']:
            user.email = user_detail_form['Father Email Id']
            # user.email = user_detail_form[AuthUser.EMAIL]

        if user_detail_form['Father First Name']:
            user.first_name = user_detail_form['Father First Name']

        if user_detail_form['Father Middle Name']:
            user.middle_name = user_detail_form['Father Middle Name']
        else:
            user.middle_name = ''

        if user_detail_form['Father Last Name']:
            user.last_name = user_detail_form['Father Last Name']
        else:
            user.last_name = ''

        if generate_password:
            user.password = 'redbytes'

        # if user_detail_form['Father Contact No.']:
        #     user.contact_num = user_detail_form['Father Contact No.']
        # else:
        #     user.contact_num = ''
        #
        # if user_detail_form['Birth Date']:
        #     user.birth_date = user_detail_form['Birth Date']
        #
        # if user_detail_form['Gender']:
        #     user.gender = user_detail_form['Gender']

        # if profile_pic:
        #     staff_detail_obj.photo = profile_pic

        # if user_detail_form['Street']:
        #     user.street = user_detail_form['Street']
        # else:
        #     user.street = ''
        #
        # if user_detail_form['City']:
        #     user.city =user_detail_form['City']
        # else:
        #     user.city = ''
        #
        # if user_detail_form['State']:
        #     user.state =user_detail_form['State']
        # else:
        #     user.state = ''
        #
        # if user_detail_form['Post Code']:
        #     user.postcode =user_detail_form['Post Code']
        # else:
        #     user.postcode = ''
        # if user_detail_form.has_key('Department Name'):
        #     if user_detail_form['Department Name']:
        #         user.department = department_details.objects.get(depatment_name=user_detail_form['Department Name'])

        return user

    @staticmethod
    def get_instance(user_detail_form, user=None, generate_password=False):

        if user:
            user = AuthUser.objects.get(id=user.id)
        else:
            user = AuthUser()
            user.username = user_detail_form.cleaned_data[AuthUser.EMAIL]
            # user.email = user_detail_form.cleaned_data[AuthUser.EMAIL]

        if user_detail_form.cleaned_data[AuthUser.EMAIL]:
            user.email = user_detail_form.cleaned_data[AuthUser.EMAIL]

        if user_detail_form.cleaned_data[AuthUser.FIRST_NAME]:
            user.first_name = user_detail_form.cleaned_data[AuthUser.FIRST_NAME]

        if user_detail_form.cleaned_data[AuthUser.MIDDLE_NAME]:
            user.middle_name = user_detail_form.cleaned_data[AuthUser.MIDDLE_NAME]
        else:
            user.middle_name = ''

        if user_detail_form.cleaned_data[AuthUser.LAST_NAME]:
            user.last_name = user_detail_form.cleaned_data[AuthUser.LAST_NAME]
        else:
            user.last_name = ''

        if generate_password:
            user.password =user_detail_form.cleaned_data[AuthUser.PASSWORD]

        return user

    @staticmethod
    def get_dict_instance(user_detail_form, user=None, generate_password=False):

        if user:
            user = AuthUser.objects.get(id=user.id)
        else:
            user = AuthUser()
            user.username = user_detail_form[AuthUser.EMAIL]

        if AuthUser.EMAIL in user_detail_form and user_detail_form[AuthUser.EMAIL]:
            user.email = user_detail_form[AuthUser.EMAIL]

        if  AuthUser.FIRST_NAME in user_detail_form and user_detail_form[AuthUser.FIRST_NAME]:
            user.first_name = user_detail_form[AuthUser.FIRST_NAME]

        if  AuthUser.LAST_NAME in user_detail_form and user_detail_form[AuthUser.LAST_NAME]:
            user.last_name = user_detail_form[AuthUser.LAST_NAME]
        else:
            user.last_name = ''

        if generate_password:
            user.password =user_detail_form[AuthUser.PASSWORD]

        return user

    def get_pura_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """

        if self.last_name == None or self.last_name == 'None':
            self.last_name=''

        if self.middle_name == None or self.middle_name == 'None':
            self.middle_name=''
        import re
        full_name = '%s %s %s' % (self.first_name, self.middle_name, self.last_name)
        name = re.sub(' +', ' ', full_name)
        # full_name = '%s %s %s' % (self.first_name, self.middle_name, self.last_name)
        return name




    def get_user_Id(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        userID = '%d' % (self.id)
        return userID.strip()

    def is_system_admin(self):
        # return True if self.role.name == self.SYSTEAM_ADMIN else False
        return True if self.role.all().filter(name__in=[self.SYSTEAM_ADMIN, self.ADMIN]).exists() else False


    def is_class_teacher(self):
        return True if self.role.all().filter(name=self.CLASS_TEACHER).exists() else False
        # return True if self.role.name == self.CLASS_TEACHER else False

    def is_assistant_teacher(self):
        return True if self.role.all().filter(name=self.ASSISTANT_TEACHER).exists() else False
        # return True if self.role.name == self.ASSISTANT_TEACHER else False

    def is_subject_teacher(self):
        return True if self.role.all().filter(name=self.SUBJECT_TEACHER).exists() else False
        # return True if self.role.name == self.SUBJECT_TEACHER else False

    def is_officer(self):
        return True if self.role.all().filter(name=self.OFFICER).exists() else False
        # return True if self.role.name == self.OFFICER else False

    def is_supervisor(self):
        return True if self.role.all().filter(name=self.SUPERVISOR).exists() else False

    def is_student(self):
        return True if self.role.all().filter(name=self.STUDENT).exists() else False

    # def is_systeamAdmin(self):
    #     return True if self.role.all().filter(name=self.ADMIN).exists() else False
        # return True if self.role.name == self.SUPERVISOR else False

    def is_parent(self):
        return True if self.role.all().filter(name=self.PARENT).exists() else False
        # return True if self.role.name == self.PARENT else False


    def is_allowed_user(self):
        # return True if self.role.name == self.SYSTEAM_ADMIN else False
        return True if self.role.all().filter(name__in=[self.Registar, self.Medical_Staff, self.Transport_Staff, self.Medical_Inspector]).exists() else False

    @property
    def get_user_permissions(self):

        role_list = self.role.all()
        perms = []
        for role_name in role_list:
            role_rec = role.objects.get(name=role_name)
            user_group_perms = Permission.objects.filter(group__name=role_rec.name)
            user_role_perms = role.objects.get(name=role_rec.name)

            group_include_perms = Permission.objects.all().filter(Q(id__in=user_group_perms))
            role_include_perms = user_role_perms.role_permission.all()

            for obj in role_include_perms:
                perms.append(obj.content_type.app_label + '.' + obj.codename)

            for obj in group_include_perms:
                perms.append(obj.content_type.app_label + '.' + obj.codename)
        return perms

    def to_dict(self):
        json_dict = {}
        json_dict['id'] = self.id
        json_dict['email'] = self.email
        json_dict['first_name'] = self.first_name
        json_dict['last_name'] = self.last_name

        return json_dict


def content_file_name_school(instance, filename):
    dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
    ext = filename.split('.')[-1]
    filename = "%s_%s.%s" % (instance.school_name, dirname, ext)
    return os.path.join('doc', filename)

def content_file_name_principal_photo(instance, filename):
    dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
    ext = filename.split('.')[-1]
    filename = "%s_%s.%s" % (instance.school_name, dirname, ext)

    return os.path.join('doc', filename)

def content_file_name_principal_signature(instance, filename):
    dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
    ext = filename.split('.')[-1]
    filename = "%s_%s.%s" % (instance.school_name, dirname, ext)

    return os.path.join('doc', filename)

def content_file_name_principal_qr_code(instance, filename):
    dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
    ext = filename.split('.')[-1]
    filename = "%s_%s.%s" % (instance.school_name, dirname, ext)

    return os.path.join('doc', filename)

def content_file_name(instance, filename):
    dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
    ext = filename.split('.')[-1]
    filename = "%s_%s.%s" % (instance.user.first_name, dirname, ext)

def content_file_name_student(instance, filename):
    dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
    ext = filename.split('.')[-1]
    filename = "%s_%s.%s" % (instance.first_name, dirname, ext)

    return os.path.join('images', filename)



def content_file_name_parent(instance, filename):
    dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
    ext = filename.split('.')[-1]
    filename = "%s_%s.%s" % (instance.first_name, dirname, ext)

    return os.path.join('images', filename)


class sections(models.Model):
    section_code = models.CharField(max_length=45)
    section_name = models.CharField(max_length=35)
    section_type = models.ForeignKey(section_type_details, on_delete=models.PROTECT, null=True, blank=True)
    create_date = models.DateTimeField(default=datetime.now)
    update_date = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    sync_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        permissions = (
            ('can_view_section', 'can view section'),
            ('can_view_shuffle_student_section', 'can view shuffle student section'),

        )

    def __unicode__(self):
        return self.section_name

    def to_dict(self):
        res = {
            'id': self.id,
            'section_name': self.section_name,
        }
        return res




class department_details(models.Model):
    depatment_name = models.CharField(max_length=81)
    create_date = models.DateTimeField(default=datetime.now)
    update_date = models.DateTimeField(auto_now=True)

    class Meta:
        permissions = (
            ('can_view_department_master', 'can view department master'),
        )

    def __unicode__(self):
        return self.depatment_name


class staff_details(models.Model):

    CONTACT_NUM = 'contact_num'
    DOB = 'birth_date'
    GENDER = 'gender'
    PROFILE_PIC = 'photo'
    STREET = 'street'
    CITY = 'city'
    STATE = 'state'
    PINCODE = 'postcode'
    DEPARTMENT = 'department'

    contact_num = models.CharField(max_length=16, null=True, blank=True)
    birth_date = models.DateField(null=True)
    gender = models.CharField(max_length=25)
    photo = models.FileField(upload_to=content_file_name)
    street = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=80, null=True, blank=True)
    state = models.CharField(max_length=80, null=True, blank=True)
    postcode = models.CharField(max_length=8, null=True, blank=True)
    create_date = models.DateTimeField(default=datetime.now)
    update_date = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='staff_relation')
    department = models.ForeignKey(department_details,on_delete=models.PROTECT,blank=True,null=True)

    def __unicode__(self):
        return self.user.email

    @staticmethod
    def get_instance(staff_detail_form, user=None):

        if user.staff_relation and user.staff_relation.all():
            staff_detail_obj = staff_details.objects.get(user=user)
        else:
            staff_detail_obj = staff_details()
            staff_detail_obj.user = user

        if staff_detail_form.cleaned_data[staff_details.CONTACT_NUM]:
            staff_detail_obj.contact_num = staff_detail_form.cleaned_data[staff_details.CONTACT_NUM]
        else:
            staff_detail_obj.contact_num = ''

        if staff_detail_form.cleaned_data[staff_details.DOB]:
            staff_detail_obj.birth_date = staff_detail_form.cleaned_data[staff_details.DOB]

        if staff_detail_form.cleaned_data[staff_details.GENDER]:
            staff_detail_obj.gender = staff_detail_form.cleaned_data[staff_details.GENDER]

        # if profile_pic:
        #     staff_detail_obj.photo = profile_pic

        if staff_detail_form.cleaned_data[staff_details.STREET]:
            staff_detail_obj.street = staff_detail_form.cleaned_data[staff_details.STREET]
        else:
            staff_detail_obj.street = ''

        if staff_detail_form.cleaned_data[staff_details.CITY]:
            staff_detail_obj.city =staff_detail_form.cleaned_data[staff_details.CITY]
        else:
            staff_detail_obj.city = ''

        if staff_detail_form.cleaned_data[staff_details.STATE]:
            staff_detail_obj.state =staff_detail_form.cleaned_data[staff_details.STATE]
        else:
            staff_detail_obj.state = ''

        if staff_detail_form.cleaned_data[staff_details.PINCODE]:
            staff_detail_obj.postcode =staff_detail_form.cleaned_data[staff_details.PINCODE]
        else:
            staff_detail_obj.postcode = ''

        if staff_detail_form.cleaned_data[staff_details.DEPARTMENT]:
            staff_detail_obj.department = department_details.objects.get(pk=staff_detail_form.cleaned_data[staff_details.DEPARTMENT].id)

        return staff_detail_obj

    @staticmethod
    def get_excel_instance(staff_detail_form, user=None):

        if user.staff_relation and user.staff_relation.all():
            staff_detail_obj = staff_details.objects.get(user=user)
        else:
            staff_detail_obj = staff_details()
            staff_detail_obj.user = user

        if staff_detail_form['Contact Number']:
            staff_detail_obj.contact_num = staff_detail_form['Contact Number']
        else:
            staff_detail_obj.contact_num = ''

        if staff_detail_form['Birth Date']:
            staff_detail_obj.birth_date = staff_detail_form['Birth Date']

        if staff_detail_form['Gender']:
            staff_detail_obj.gender = staff_detail_form['Gender']

        if staff_detail_form['Street']:
            staff_detail_obj.street = staff_detail_form['Street']
        else:
            staff_detail_obj.street = ''

        if staff_detail_form['State']:
            staff_detail_obj.state = staff_detail_form['State']
        else:
            staff_detail_obj.state = ''

        if staff_detail_form['Post Code']:
            staff_detail_obj.postcode = staff_detail_form['Post Code']
        else:
            staff_detail_obj.postcode = ''

        if staff_detail_form['City']:
            staff_detail_obj.city = staff_detail_form['City']
        else:
            staff_detail_obj.city = ''

        # if staff_detail_form['Department Name']:
        #     staff_detail_obj.department = department_details.objects.get(
        #         depatment_name=staff_detail_form['Department Name'])

        return staff_detail_obj


class academic_year(models.Model):
    year_code = models.CharField(max_length=35)
    year_name = models.CharField(max_length=35)
    start_date = models.DateField()
    end_date = models.DateField()
    current_academic_year = models.BooleanField()
    create_date = models.DateTimeField(default=datetime.now)
    update_date = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    sync_id = models.CharField(max_length=255, blank=True, null=True)

    # class_ids = models.ManyToManyField(class_details)
    class Meta:
        permissions = (
            ('can_view_academic_year_info', 'can view academic year info'),
        )

    def __unicode__(self):
        return self.year_name

    def to_dict(self):
        res = {
            'id': self.id,
            'year_code': self.year_code,
            'year_name': self.year_name,
            'current_academic_year': self.current_academic_year,
            'start_date': self.start_date,
            'is_active': self.is_active,
            'end_date': self.end_date,

        }
        return res

class class_details(models.Model):
    class_code = models.CharField(max_length=10)
    class_name = models.CharField(max_length=32)
    next_class = models.ForeignKey('self', on_delete=models.PROTECT,  null=True, related_name='next_class_relation')
    create_date = models.DateTimeField(default=datetime.now, null=True, blank=True)
    update_date = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    sync_id = models.CharField(max_length=255, blank=True, null=True)
    #section_ids = models.ManyToManyField(sections)
    #subject_ids = models.ManyToManyField(subjects)

    class Meta:
        permissions = (
            ('can_view_class', 'can view class'),
        )
    
    def __unicode__(self):
        return self.class_name

    def to_dict(self):
        res = {
            'id': self.id,
            'class_code': self.class_code,
            'create_date': str(self.create_date),
            'update_date': str(self.update_date),
            'is_active': self.is_active,
            'class_name': self.class_name
            # 'next_class': self.next_class,

        }
        return res

#########------- Mapping ------- ################
class academic_class_section_mapping_manage(models.Manager):
    def get_queryset(self, class_name):
        return super(academic_class_section_mapping_manage, self).get_queryset().filter(class_name=class_name)

class academic_class_section_mapping(models.Model):
    year_name = models.ForeignKey(academic_year, on_delete=models.PROTECT, related_name='mapped_year')
    class_name = models.ForeignKey(class_details, on_delete=models.PROTECT, null=True, related_name='mapped_class')
    section_name = models.ForeignKey(sections, on_delete=models.PROTECT, null=True, related_name='mapped_section')
    staff = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True)
    assistant_teacher = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True,related_name="assistant_teacher_name")


    class Meta:
        permissions = (
            ('can_view_acadenic_class_section_mapping', 'can view acadenic class section mapping'),
            ('can_view_class_teacher_mapping', 'can view class teacher mapping'),
            ('can_view_staff_based', 'can view staff based'),
            ('can_view_assistant_based', 'can view assistant based'),
            ('can_view_year_based', 'can view year based'),
            ('can_view_class_based', 'can view class based'),
            ('can_view_section_based', 'can view section based'),
            ('can_view_student_subject', 'can view student subject'),
        )

    def __unicode__(self):
        return self.year_name.year_name

    def to_dict(self):
        res = {
            'id': self.id,
            'class_name': self.class_name.class_name,
            'section_name': self.section_name.section_name,
        }
        return res

class subject_substrands(models.Model):
    substrands_name = models.CharField(max_length=250)
    substrands_code = models.CharField(max_length=45, null=True)


class subject_strands(models.Model):
    strand_name = models.CharField(max_length=250)
    strand_code = models.CharField(max_length=45)
    substrands_ids = models.ManyToManyField(subject_substrands)

class subjects(models.Model):
    subject_code = models.CharField(max_length=32)
    subject_name = models.CharField(max_length=32)
    type = models.CharField(max_length=50)
    create_date = models.DateTimeField(default=datetime.now)
    update_date = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    strands_ids = models.ManyToManyField(subject_strands)

    class Meta:
        permissions = (
            ('can_view_subject', 'can view subject'),
        )

    def __unicode__(self):
        return self.subject_name

    def to_dict(self):
        res = {
            'id': self.id,
            'subject_code': self.subject_code,
            'subject_name': self.subject_name,
        }
        return res

class academic_supervisor_class_section_mapping(models.Model):
    academic_class_section_mapping = models.ForeignKey(academic_class_section_mapping,  on_delete=models.PROTECT, related_name='supervisor_academic_class_section_relation')
    subject = models.ForeignKey(subjects, on_delete=models.PROTECT)
    supervisor_id = models.ForeignKey(AuthUser, on_delete=models.PROTECT)
    is_lead_supervisor = models.BooleanField(default=False)


    class Meta:
        permissions = (
            ('can_view_supervisor_class_mapping', 'can view supervisor class mapping'),
        )



class temp_academic_supervisor_class_section_mapping(models.Model):
    acd_sup_cls_sec_map_id = models.ForeignKey(academic_supervisor_class_section_mapping,null=True,related_name='temp_acd_sup_cls_sec_map_id')
    # academic_class_section_mapping = models.ForeignKey(academic_class_section_mapping,  on_delete=models.PROTECT, related_name='temp_supervisor_academic_class_section_relation')
    # subject = models.ForeignKey(subjects, on_delete=models.PROTECT)
    # supervisor_id = models.ForeignKey(AuthUser, on_delete=models.PROTECT)


class mandatory_subject_mapping(models.Model):
    academic_class_section_mapping = models.ForeignKey(academic_class_section_mapping,  on_delete=models.PROTECT)
    subject = models.ForeignKey(subjects, on_delete=models.PROTECT)

    class Meta:
        permissions = (
            ('can_view_mandatory_subject_mapping', 'can view mandatory subject mapping'),
            ('can_view_optional_subject_mapping', 'can view optional subject mapping'),
            ('add_optional_subject_mapping', 'can add optional subject mapping'),
        )
    
class teacher_subject_mapping(models.Model):
    academic_class_section_mapping = models.ForeignKey(academic_class_section_mapping,  on_delete=models.PROTECT, related_name='subject_teacher_academic_class_section_relation')
    subject = models.ForeignKey(subjects, on_delete=models.PROTECT)
    staff_id = models.ForeignKey(AuthUser, on_delete=models.PROTECT)

    class Meta:
        permissions = (
            ('can_view_teacher_subject_mapping', 'can view teacher subject mapping'),
        )

    # def get_teacher_name(self):
    #     """
    #     Returns the first_name plus the last_name, with a space in between.
    #     """
    #     full_name = '%s %s %s' % (self.staff_id.first_name, self.staff_id.middle_name, self.staff_id.last_name)
    #     return full_name.strip()
    
class school_details(models.Model):
    school_name = models.CharField(max_length=250)
    school_address = models.CharField(max_length=250)
    contact_num = models.CharField(max_length=16)
    email = models.CharField(max_length=250)
    logo = models.FileField(upload_to=content_file_name_school)
    website = models.CharField(max_length=255)
    city = models.CharField(max_length=80)
    pin = models.CharField(max_length=8)
    state = models.CharField(max_length=80)
    street=models.CharField(max_length=255)
    create_date = models.DateTimeField(default=datetime.now)
    update_date = models.DateTimeField(auto_now=True)
    principal_Photo = models.FileField(upload_to=content_file_name_principal_photo, null=True,blank=True)
    principal_name = models.CharField(max_length=80, null=True)
    principal_signature = models.FileField(upload_to=content_file_name_principal_signature, null=True)
    principal_qr_code = models.FileField(upload_to=content_file_name_principal_qr_code, null=True)

    class Meta:
        permissions = (
            ('can_view_school_info', 'can view school info'),
        )

    def __unicode__(self):
        return self.school_name


class secondary_parent_details(models.Model):
    sec_parent_name = models.CharField(max_length=255, null=True,blank=True)
    sec_parent_mail = models.CharField(max_length=100, null=True,blank=True)
    sec_parent_contact = models.CharField(max_length=16, null=True,blank=True)
    sec_parent_address = models.CharField(max_length=255, null=True,blank=True)
    type = models.CharField(max_length=45, blank=True, null=True)
    sec_photo = models.FileField(upload_to=content_file_name, null=True,blank=True)

class parents_details(models.Model):

    CONTACT_NUM = 'contact_num'
    DOB = 'birth_date'
    GENDER = 'gender'
    CODE = 'code'
    STREET = 'street'
    CITY = 'city'
    STATE = 'state'
    PINCODE = 'postcode'
    DEPARTMENT = 'department'
    MOTHERNAME = 'mother_name'
    MOTHERCONTACT = 'mother_contact_num'
    MOTHEREMAIL = 'mother_email'
    SECONDARY_PARENT_IDS = 'secondary_parent_ids'
    MOTHERMIDDLENAME = 'mother_middle_name'
    MOTHERLASTNAME = 'mother_last_name'
    msg=''

    birth_date = models.DateField(null=True,blank=True)
    gender = models.CharField(max_length=25,null=True,blank=True)
    street = models.CharField(max_length=255,null=True,blank=True)
    code = models.CharField(max_length=30,null=True,blank=True)
    contact_num = models.CharField(max_length=16,blank=True, null=True)
    # photo = models.FileField(upload_to=content_file_name, null=True)
    city = models.CharField(max_length=80,null=True,blank=True)
    state = models.CharField(max_length=80,null=True,blank=True)
    postcode = models.CharField(max_length=8,null=True,blank=True)
    is_active = models.BooleanField(default=True)
    create_date = models.DateTimeField(default=datetime.now)
    update_date = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='parent_relation')
    secondary_parent_ids = OneToManyField(secondary_parent_details)
    reject_reason = models.CharField(max_length=500, null=True)
    photo = models.FileField(upload_to=content_file_name_student,blank=True, null=True)
    mother_name = models.CharField(max_length=255, blank=True, null=True)
    mother_contact_num = models.CharField(max_length=16,blank=True, null=True)
    mother_email = models.CharField(max_length=250,blank=True, null=True)
    mother_middle_name = models.CharField(max_length=255, blank=True, null=True)
    mother_last_name = models.CharField(max_length=255, blank=True, null=True)


    class Meta:
        permissions = (
            ('can_view_add_parent', 'can view add parent'),
            ('can_view_view_edit_parent', 'can view view edit parent'),

            ('can_view_parent_contact_update', 'can view parent contact update'),
        )

    def get_mother_pura_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """

        if self.mother_name == None or self.mother_name == 'None' or self.mother_name == '':
            self.mother_name = ''

        if self.mother_middle_name == None or self.mother_middle_name == 'None' or self.mother_middle_name == '':
            self.mother_middle_name = ''

        if self.mother_last_name == None or self.mother_last_name == 'None' or self.mother_last_name == '':
            self.mother_last_name = ''

        full_name = '%s %s %s' % (self.mother_name, self.mother_middle_name, self.mother_last_name)
        return full_name.strip()
    
    def __unicode__(self):
        return self.code

    def hasNumbers(inputString):
        return any(char.isdigit() for char in inputString)

    @staticmethod
    def validate(val_dict):

        def phone_number_validate(str_val):
            ab = list(str_val)
            for x in ab:
                if x == '+' or x == ' ':
                    pass
                else:
                    try:
                        int(x)
                    except ValueError:
                        return False
            return True

        def hasNumbers(inputString):
            return any(char.isdigit() for char in inputString)

        def name_validate(str):
            if (len(str) > 0) and (len(str) <= 255):
                flag = hasNumbers(str)
                if flag:
                    # parents_details.msg ='Invalid Name Value'
                    # return True, parents_details.msg
                    return True
            else:
                # parents_details.msg = 'Invalid Name Value'
                return True
            return False

        first_name = name_validate(str(val_dict['First Name']))
        if first_name:
            return True

        if val_dict['Last Name']:
            last_name = name_validate(str(val_dict['Last Name']))
            if last_name:
                return True

        if val_dict['Middle Name']:
            middle_name = name_validate(str(val_dict['Middle Name']))
            if middle_name:
                return True

        email = validate_email(str(val_dict['Email']))
        if email is False:
            return True

        cont_num = str(val_dict['Mobile No.'])
        contct_falg = phone_number_validate(str(cont_num))
        if (len(cont_num)<=16) and (contct_falg is True):
            pass
        else:
            return  True

        if val_dict['Street']:
            if (len(str(val_dict['Street'])) == 0) or (len(str(val_dict['Street'])) > 255):
                return True

        if val_dict['City']:
            if (len(str(val_dict['City'])) > 0) and (len(str(val_dict['City'])) <= 80):
                flag = hasNumbers(str(val_dict['City']))
                if flag:
                    return True
            else:
                return True

        if val_dict['State']:
            if (len(str(val_dict['State'])) > 0) and (len(str(val_dict['State'])) <= 80):
                flag = hasNumbers(str(val_dict['State']))
                if flag:
                    return True
            else:
                return True

        if val_dict['Post Code']:
            post_code = str(val_dict['Post Code'])
            ps_flag = post_code.isdigit()
            if (len(post_code) <= 8) and (ps_flag is True):
                pass
            else:
                return True

        return  False

    @staticmethod
    def get_instance(parent_detail_form, user=None):

        if user.parent_relation and user.parent_relation.all():
            parent_detail_obj = parents_details.objects.get(user=user)
        else:
            parent_detail_obj = parents_details()
            parent_detail_obj.user = user
            parent_detail_obj.code = parent_detail_form.cleaned_data[parents_details.CODE]

        if parent_detail_form.cleaned_data[parents_details.CONTACT_NUM]:
            parent_detail_obj.contact_num = parent_detail_form.cleaned_data[parents_details.CONTACT_NUM]

        if parent_detail_form.cleaned_data[parents_details.DOB]:
            parent_detail_obj.birth_date = parent_detail_form.cleaned_data[parents_details.DOB]

        if parent_detail_form.cleaned_data[parents_details.GENDER]:
            parent_detail_obj.gender = parent_detail_form.cleaned_data[parents_details.GENDER]

        if parent_detail_form.cleaned_data[parents_details.STREET]:
            parent_detail_obj.street = parent_detail_form.cleaned_data[parents_details.STREET]

        if parent_detail_form.cleaned_data[parents_details.STATE]:
            parent_detail_obj.state = parent_detail_form.cleaned_data[parents_details.STATE]

        if parent_detail_form.cleaned_data[parents_details.PINCODE]:
            parent_detail_obj.postcode = parent_detail_form.cleaned_data[parents_details.PINCODE]

        if parent_detail_form.cleaned_data[parents_details.CITY]:
            parent_detail_obj.city = parent_detail_form.cleaned_data[parents_details.CITY]

        if parent_detail_form.cleaned_data[parents_details.MOTHERNAME]:
            parent_detail_obj.mother_name = parent_detail_form.cleaned_data[parents_details.MOTHERNAME]

        if parent_detail_form.cleaned_data[parents_details.MOTHERCONTACT]:
            parent_detail_obj.mother_contact_num = parent_detail_form.cleaned_data[parents_details.MOTHERCONTACT]

        if parent_detail_form.cleaned_data[parents_details.MOTHEREMAIL]:
            parent_detail_obj.mother_email = parent_detail_form.cleaned_data[parents_details.MOTHEREMAIL]

        if parent_detail_form.cleaned_data[parents_details.MOTHERMIDDLENAME]:
            parent_detail_obj.mother_middle_name = parent_detail_form.cleaned_data[parents_details.MOTHERMIDDLENAME]

        if parent_detail_form.cleaned_data[parents_details.MOTHERLASTNAME]:
            parent_detail_obj.mother_last_name = parent_detail_form.cleaned_data[parents_details.MOTHERLASTNAME]

        return parent_detail_obj

    @staticmethod
    def get_my_profile_instance(parent_detail_form, user=None):

        if user.parent_relation and user.parent_relation.all():
            parent_detail_obj = parents_details.objects.get(user=user)
        else:
            parent_detail_obj = parents_details()
            parent_detail_obj.user = user
            parent_detail_obj.code = parent_detail_form.cleaned_data[parents_details.CODE]

        if parent_detail_form.cleaned_data[parents_details.CONTACT_NUM]:
            parent_detail_obj.contact_num = parent_detail_form.cleaned_data[parents_details.CONTACT_NUM]

        if parent_detail_form.cleaned_data[parents_details.DOB]:
            parent_detail_obj.birth_date = parent_detail_form.cleaned_data[parents_details.DOB]

        if parent_detail_form.cleaned_data[parents_details.GENDER]:
            parent_detail_obj.gender = parent_detail_form.cleaned_data[parents_details.GENDER]

        if parent_detail_form.cleaned_data[parents_details.STREET]:
            parent_detail_obj.street = parent_detail_form.cleaned_data[parents_details.STREET]

        if parent_detail_form.cleaned_data[parents_details.STATE]:
            parent_detail_obj.state = parent_detail_form.cleaned_data[parents_details.STATE]

        if parent_detail_form.cleaned_data[parents_details.PINCODE]:
            parent_detail_obj.postcode = parent_detail_form.cleaned_data[parents_details.PINCODE]

        if parent_detail_form.cleaned_data[parents_details.CITY]:
            parent_detail_obj.city = parent_detail_form.cleaned_data[parents_details.CITY]


        return parent_detail_obj

    @staticmethod
    def get_parent_instance(parent_detail_form, user=None):

        if user.parent_relation and user.parent_relation.all():
            parent_detail_obj = parents_details.objects.get(user=user)
        else:
            parent_detail_obj = parents_details()
            parent_detail_obj.user = user
            parent_detail_obj.code = ''

        if parent_detail_form['Father Contact No.']:
            parent_detail_obj.contact_num = parent_detail_form['Father Contact No.']

        if parent_detail_form['Mother First Name']:
            parent_detail_obj.mother_name = parent_detail_form['Mother First Name'].strip()

        if parent_detail_form['Mother Email Id']:
            parent_detail_obj.mother_email = parent_detail_form['Mother Email Id'].strip()

        if parent_detail_form['Mother Contact No.']:
            parent_detail_obj.mother_contact_num = parent_detail_form['Mother Contact No.']

        if parent_detail_form['Mother Middle Name']:
            parent_detail_obj.mother_middle_name = parent_detail_form['Mother Middle Name'].strip()

        if parent_detail_form['Mother Last Name']:
            parent_detail_obj.mother_last_name = parent_detail_form['Mother Last Name'].strip()

        # if parent_detail_form['Birth Date']:
        # parent_detail_obj.birth_date = None

        # if parent_detail_form['Gender']:
        #     parent_detail_obj.gender = parent_detail_form['Gender']

        # if parent_detail_form['Gender']:
        #     if parent_detail_form['Gender'] == 'Male' or parent_detail_form['Gender'] == 'True':
        #         parent_detail_obj.gender = 'True'
        #     elif parent_detail_form['Gender'] == 'Female' or parent_detail_form['Gender'] == 'False':
        #         parent_detail_obj.gender = 'False'

        if parent_detail_form['Street']:
            parent_detail_obj.street = parent_detail_form['Street'].strip()

        if parent_detail_form['City']:
            parent_detail_obj.city = parent_detail_form['City'].strip()

        if parent_detail_form['State']:
            parent_detail_obj.state = parent_detail_form['State'].strip()

        if parent_detail_form['Postal Code']:
            parent_detail_obj.postcode = parent_detail_form['Postal Code']



        return parent_detail_obj


    @staticmethod
    def get_dict_instance(parent_detail_form, user=None):

        if user.parent_relation and user.parent_relation.all():
            parent_detail_obj = parents_details.objects.get(user=user)
        else:
            parent_detail_obj = parents_details()
            parent_detail_obj.user = user
            parent_detail_obj.code = parent_detail_form[parents_details.CODE]

        if parents_details.CONTACT_NUM in parent_detail_form and parent_detail_form[parents_details.CONTACT_NUM]:
            parent_detail_obj.contact_num = parent_detail_form[parents_details.CONTACT_NUM]

        if parents_details.DOB in parent_detail_form and parent_detail_form[parents_details.DOB]:
            parent_detail_obj.birth_date = parent_detail_form[parents_details.DOB]

        if parents_details.GENDER in parent_detail_form and parent_detail_form[parents_details.GENDER]:
            parent_detail_obj.gender = parent_detail_form[parents_details.GENDER]

        if parents_details.STREET in parent_detail_form and parent_detail_form[parents_details.STREET]:
            parent_detail_obj.street = parent_detail_form[parents_details.STREET]

        if parents_details.STATE in parent_detail_form and parent_detail_form[parents_details.STATE]:
            parent_detail_obj.state = parent_detail_form[parents_details.STATE]

        if parents_details.PINCODE in parent_detail_form and parent_detail_form[parents_details.PINCODE]:
            parent_detail_obj.postcode = parent_detail_form[parents_details.PINCODE]

        if parents_details.CITY in parent_detail_form and parent_detail_form[parents_details.CITY]:
            parent_detail_obj.city = parent_detail_form[parents_details.CITY]

        if parents_details.MOTHERNAME in parent_detail_form and parent_detail_form[parents_details.MOTHERNAME]:
            parent_detail_obj.mother_name = parent_detail_form[parents_details.MOTHERNAME]

        if parents_details.MOTHERCONTACT in parent_detail_form and parent_detail_form[parents_details.MOTHERCONTACT]:
            parent_detail_obj.mother_contact_num = parent_detail_form[parents_details.MOTHERCONTACT]

        if parents_details.MOTHEREMAIL in parent_detail_form and parent_detail_form[parents_details.MOTHEREMAIL]:
            parent_detail_obj.mother_email = parent_detail_form[parents_details.MOTHEREMAIL]

        return parent_detail_obj


class parents_details_excel(models.Model):
    excel = models.FileField(upload_to='uploads/')

class students_details_excel(models.Model):
    excel = models.FileField(upload_to='uploads/')

class import_student_details_excel(models.Model):
    uploaded_by = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True)
    total_rec = models.CharField(max_length=10, null=True)
    success_rec = models.CharField(max_length=10, null=True)
    unsuccess_rec = models.CharField(max_length=10, null=True)
    uploaded_on = models.DateTimeField(default=datetime.now)
    excel = models.FileField(upload_to='uploads/')

    class Meta:
        permissions = (
            ('can_view_student_import_log', 'can view student import log'),
        )

class import_parent_recs_details_excel(models.Model):
    uploaded_by = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True)
    total_rec =   models.CharField(max_length=10,null=True)
    uploaded_on = models.DateTimeField(default=datetime.now)
    excel = models.FileField(upload_to='uploads/',null=True)

    class Meta:
        permissions = (
            ('can_view_parent_import_log', 'can view parent import log'),
        )


class import_user_recs_details_excel(models.Model):
    uploaded_by = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True)
    total_rec = models.CharField(max_length=10,null=True)
    uploaded_on = models.DateTimeField(default=datetime.now)
    excel = models.FileField(upload_to='uploads/',null=True)

class month(models.Model):
    month_name = models.CharField(max_length=20)


class nationality(models.Model):
    nationality_name = models.CharField(max_length=80)
    # nationatity_group_name = models.ForeignKey(nationality_group, on_delete=models.PROTECT, null=True)


    class Meta:
        permissions = (
            ('can_view_nationality', 'can view nationality'),
        )
    
    def __unicode__(self):
        return self.nationality_name
 
class religion(models.Model):
    religion_name = models.CharField(max_length=80)

    class Meta:
        permissions = (
            ('can_view_religion', 'can view religion'),
        )
    
    def __unicode__(self):
        return self.religion_name


from masters.utils import to_dict_list
from datetime import date

class student_details(models.Model):
    first_name = models.CharField(max_length=255, null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    gender = models.CharField(max_length=25, null=True, blank=True)
    emirati = models.CharField(max_length=25,default="no")
    select = models.CharField(max_length=25,default="no")
    sen = models.CharField(max_length=25,default="no")

    transport = models.CharField(max_length=25,default="own")
    bus_no = models.CharField(max_length=80,blank=True,null=True)
    pick_up_time = models.TimeField(null=True,blank=True)
    pick_up_point = models.CharField(max_length=100, blank=True, null=True)
    drop_off_point = models.CharField(max_length=100, blank=True, null=True)
    drop_bus_no = models.CharField(max_length=100, blank=True, null=True)
    drop_off_time = models.TimeField(null=True,blank=True)

    house_name = models.ForeignKey(house_master, null=True, related_name='parent_relation')

    mother_name = models.CharField(max_length=255,blank=True,null=True)
    father_name = models.CharField(max_length=255,blank=True,null=True)
    city = models.CharField(max_length=80,blank=True,null=True)
    street = models.CharField(max_length=255,blank=True,null=True)
    state = models.CharField(max_length=80,blank=True,null=True)
    postcode = models.CharField(max_length=8,blank=True,null=True)
    email = models.CharField(max_length=250,blank=True,null=True)
    photo = models.FileField(upload_to=content_file_name_student)
    last_name = models.CharField(max_length=255,blank=True,null=True)
    joining_date = models.DateField()
    parent_mail = models.CharField(max_length=250,null=True)
    attendance_percentage = models.FloatField(max_length=45, null=True)
    reject_reason = models.CharField(max_length=500, null=True)
    
    is_active = models.BooleanField(default=True)
    create_date = models.DateTimeField(default=datetime.now)
    update_date = models.DateTimeField(auto_now=True)

    academic_class_section = models.ForeignKey(academic_class_section_mapping, null=True, on_delete=models.PROTECT, related_name='student_year_class_section_relation')

    parent = models.ForeignKey(parents_details,null=True, on_delete=models.PROTECT,related_name='parent_relation')
    nationality = models.ForeignKey(nationality, on_delete=models.PROTECT, null=True, blank=True)
    religion = models.ForeignKey(religion, on_delete=models.PROTECT, null=True, blank=True)
    subject_ids = models.ManyToManyField(subjects, related_name='optional_subject_relation', null=True, blank=True)
    emergency_contact_num = models.CharField(max_length=16, blank=True, null=True)
    middle_name = models.CharField(max_length=255, blank=True, null=True)

    odoo_id = models.CharField(max_length=255, blank=True, null=True)
    roll_no = models.CharField(max_length=255, blank=True, null=True)
    student_code = models.CharField(max_length=25, blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='student_relation',blank=True, null=True)

    class Meta:
        permissions = (
            ('can_view_add_student', 'can view add student'),
            ('can_view_view_edit_student', 'can view view edit student'),

            ('can_view_student_photos', 'can view student photos'),
            ('can_view_missing_student_profile', 'can view missing student profile'),
        )
        ordering = ('first_name',)
    
    def __unicode__(self):
        return self.first_name

    def get_all_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        if self.middle_name == None:
            self.middle_name = ''
        full_name = '%s %s %s' % (self.first_name, self.middle_name, self.last_name)
        return full_name.strip()

    def get_student_all_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        if self.middle_name == None or self.middle_name =='' :
            full_name = '%s %s' % (self.first_name, self.last_name)
        else:
            full_name = '%s %s %s' % (self.first_name, self.middle_name, self.last_name)
        return full_name.strip()

    # @property
    # def get_full_name(self):
    #     """
    #     Returns the first_name plus the last_name, with a space in between.
    #     """
    #     if self.middle_name == None:
    #         self.middle_name = ''
    #     full_name = '%s %s %s' % (self.first_name, self.middle_name, self.last_name)
    #     return full_name.strip()



    @property
    def is_past_date(self):
        return date.today() >= self.joining_date

    def to_dict(self):
        res = {
            'id': self.id,
            'student_code': self.student_code,
            'first_name': self.first_name,
            'get_all_name': self.get_all_name(),
            'class_name': self.academic_class_section.class_name,
            'section_name': self.academic_class_section.section_name,
            'academic_year': self.academic_class_section.year_name,
            'joining_data': self.joining_date,
            'optional_subject_count': to_dict_list(self.subject_ids.all())
        }
        return res

    def to_dict_brief(self):
        res = {
            "status": self.is_active,
            # "details": {
                "id": self.user.id,
                "username": self.user.username,
                "email": self.user.email,
                "password": self.user.password,
                "emailVerified": "0",
                "registration_no": self.student_code,
                "grade": self.academic_class_section.class_name.class_name,
                "section": self.academic_class_section.section_name.section_name,
                "gender": self.gender,
                "nationality": self.nationality.nationality_name,
                "arabs": self.select,
                "emirates": self.emirati,
                "sen": self.sen,
                "dateof_birth": self.birth_date,
                "name": self.user.get_full_name(),
                "school": "2",
                "type": self.user.role.get().name,
                "createdAt": self.create_date,
                "modifiedAt": self.update_date,
                "role_id": self.user.role.get().id
            },
            # "message": "success"
        # }
        return res




def random_string_generator(size, include_lowercase=True, include_uppercase=True, include_number=True):
    s = ""
    if include_lowercase:
        s = s + string.ascii_lowercase
    if include_uppercase:
        s = s + string.ascii_uppercase
    if include_number:
        s = s + string.digits

    if len(s) > 0:
        s = ''.join(random.sample(s, len(s)))
        return ''.join(random.choice(s) for _ in range(size))

class sz_preview_imported_user(models.Model):
    first_name = models.CharField(max_length=254, null=True)
    middle_name = models.CharField(max_length=254, null=True)
    last_name = models.CharField(max_length=254, null=True)

    email = models.CharField(max_length=254, null=True)
    contact = models.CharField(max_length=16, null=True)
    gender = models.CharField(max_length=25, null=True)
    street = models.CharField(max_length=80, null=True)
    city = models.CharField(max_length=80, null=True)
    state = models.CharField(max_length=80, null=True)
    post_code = models.CharField(max_length=8, null=True)
    birth_date = models.DateField(null=True)
    department = models.CharField(max_length=80, null=True)

class sz_import_house_info_excel(models.Model):
    uploaded_by = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True)
    total_rec = models.CharField(max_length=10, null=True)
    uploaded_on = models.DateTimeField(default=datetime.now)
    excel = models.FileField(upload_to='uploads/', null=True)

    class Meta:
        permissions = (
            ('can_view_house_export', 'can view house export'),
        )

class sz_import_transport_info_excel(models.Model):
    uploaded_by = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True)
    total_rec = models.CharField(max_length=10, null=True)
    uploaded_on = models.DateTimeField(default=datetime.now)
    excel = models.FileField(upload_to='uploads/', null=True)

    class Meta:
        permissions = (
            ('can_view_transport_export', 'can view transport export'),
        )


class custom_sub_groups(models.Model):
    sub_group_code = models.CharField(max_length=250,blank=True, null=True)
    sub_group_name = models.CharField(max_length=250,blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.sub_group_name


class custom_groups(models.Model):
    group_code = models.CharField(max_length=250,blank=True, null=True)
    group_name = models.CharField(max_length=250,blank=True, null=True)
    is_primary = models.BooleanField(default=False)
    is_show_attendance_dashboard = models.BooleanField(default=False)
    is_group_active = models.BooleanField(default=True)
    custom_sub_group_ids = OneToManyField(custom_sub_groups)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        permissions = (
            ('can_view_custom_groups', 'can view custom groups'),
            ('can_view_custom_sub_groups', 'can view custom sub groups'),
            ('can_view_edit_custom_groups', 'can view edit custom groups'),
            ('can_view_assign_groups_students', 'can view assign groups students'),
        )
    def __unicode__(self):
        return self.group_name

class assign_group_student(models.Model):
    custom_group_id = models.ForeignKey(custom_groups, on_delete=models.PROTECT, null=True)
    custom_sub_group_id = models.ForeignKey(custom_sub_groups, on_delete=models.PROTECT, null=True)
    student_id = models.ForeignKey(student_details, on_delete=models.PROTECT, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

#===============================================================================
#===============================================================================
from django.shortcuts import render, redirect
from forms  import *
from models import *
from django.contrib import messages
from django.http import HttpResponse
import ast
from django.db.models import Q
import json
from registration.decorators import user_login_required, user_has_permission, the_decorator, decorator, user_permission_required, user_group_permission_required
from masters.utils import todays_date, current_academic_year_mapped_classes, current_class_mapped_sections

@user_login_required
def PromotionMenu(request):
    return render(request, "promotions_menu.html")

@user_login_required
def PromotionStudent(request):
    all_classes = class_details.objects.all()
    # all_sections = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        all_classes = current_academic_year_mapped_classes(selected_year_id,request.user)

    except:
        messages.success(request, "Academic Year Class Section Mapping Not Fund.")
        # return render(request, "promotions_menu.html")
        return redirect('/school/promotions_menu/')

    try:
        current_acd_year = academic_year.objects.get(current_academic_year = True)
        flag = 0
        for academicyr_rec in all_academicyr_recs:
            if academicyr_rec:
               if academicyr_rec.start_date > current_acd_year.end_date:
                   flag = 1

        if flag == 1:
            return render(request, "promotions_student_list.html", {'ClassDetails' : all_classes,"year_list" : all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})
        else:
            messages.success(request, "Please Create Next Academic Year First And Try Again.")
            return render(request, "promotions_menu.html")
    except:
        messages.success(request, "Some Error Occoured..")
        return render(request, "promotions_menu.html")

def load_prom_data(val_dict,user_type):
    all_sections = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    flag = False
    try:
        selected_year_id = val_dict.get('year_name')  # It will be used in HTML page as a value of selected Class
        selected_year_rec = academic_year.objects.get(id=selected_year_id)
        selected_year_name = selected_year_rec.year_name

        selected_class_id = val_dict.get('class_name')  # It will be used in HTML page as a value of selected Class
        selected_class = class_details.objects.get(id=selected_class_id)
        selected_class_name = selected_class.class_name

        selected_section_id = val_dict.get(
            'section_name')  # It will be used in HTML page as a value of selected Section
        selected_section = sections.objects.get(id=selected_section_id)
        selected_section_name = sections.section_name

        current_acd_year = academic_year.objects.get(year_name=selected_year_name)
        all_classes = current_academic_year_mapped_classes(selected_year_id, user_type)
        all_sections = current_class_mapped_sections(selected_year_id, selected_class_id, user_type)

        next_acd_year = (current_acd_year.end_date - all_academicyr_recs[0].start_date).days
        for academicyr_rec in all_academicyr_recs:
            if academicyr_rec:
                acd_year = academicyr_rec.start_date - current_acd_year.end_date
                if acd_year.days > 0:
                    if next_acd_year > acd_year.days:
                        next_acd_year = acd_year.days
                        next_acd_year_obj = academicyr_rec

        detain_section_list = []
        detain_flag = False
        promote_flag = False
        promotion_filtered_recs = []

        try:
            promotion_filtered_recs = academic_class_section_mapping.objects.filter(year_name=next_acd_year_obj.id,
                                                                                    class_name_id=selected_class.next_class.id)
        except:
            flag = True
            # messages.warning(val_dict, "Next Class Details Not Found For Promotion. Promotion Not Possible")
        # promotion_filtered_recs = academic_class_section_mapping.objects.filter(year_name=next_acd_year_obj.id)
        next_class_rec = None
        if promotion_filtered_recs:
            next_class_rec = promotion_filtered_recs[0]
            promote_flag = True

        detain_filtered_sections = academic_class_section_mapping.objects.filter(year_name=next_acd_year_obj.id,
                                                                                 class_name=selected_class_id)
        if detain_filtered_sections:
            detain_flag = True

        for section in detain_filtered_sections:
            detain_section_list.append(section.section_name)

        # filtering student records to pass HTML page
        student_list = []

        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                            section_name=selected_section_id,
                                                                            class_name=selected_class_id)
        filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj,
                                                           is_active=True, joining_date__lte=todays_date())

        for student_rec in filtered_students:
            if student_rec:
                # If student not exists in promotion confirmation class, then display on apply promotions form
                if not (promotion_details.objects.filter(
                            Q(student=student_rec.id, is_promotion_applied=True) | Q(student=student_rec.id,
                                                                                     is_promoted=True)).exists()):
                    student_list.append(student_rec)

        # Passing values to the HTML page through form_vals
        form_vals = {
            'flag':flag,
            'PromoteFlag': promote_flag,
            'next_class_rec': next_class_rec,
            'promotion_filtered_recs': promotion_filtered_recs,
            'DetainFlag': detain_flag,
            'StudentList': student_list,
            'Selected_Section_Id': selected_section_id,
            'SectName': selected_section_name,
            'section_name': selected_section,
            'Section': all_sections,
            "SectionList": detain_section_list,

            'Selected_Class_Id': selected_class_id,
            'ClassName': selected_class_name,
            'class_name': selected_class,
            'ClassDetails': all_classes,

            'NextAcdYear': next_acd_year_obj,
            'Selected_Year_Id': selected_year_id,
            'selected_year_name': selected_year_name,
            'selected_year': selected_year_rec,
            'year_list': all_academicyr_recs
        }
    except:
        messages.success(val_dict, "Some Error Occoured... Please Try Again")
        return render(val_dict, "promotions_student_list.html", {'Selected_Year_Id':selected_year_rec.id,'selected_year_name':selected_year_rec.year_name,'ClassDetails': all_classes, 'Section': all_sections, "year_list": all_academicyr_recs})
    return form_vals

@user_login_required
def PromotionList(request):
    val_dict = request.POST
    form_vals = {}
    flag = False

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)

    except:
        messages.success(request, "Academic Year Class Section Mapping Not Fund.")
        return redirect('/school/promotions_menu/')


    if request.method == 'POST':
        request.session['val_dict'] = val_dict
        form_vals = load_prom_data(val_dict,request.user)
    else:
        form_vals = load_prom_data(request.session.get('val_dict'),request.user)

    if form_vals['flag'] == True:
        messages.warning(request, "Next Class Details Not Found For Promotion. Promotion Not Possible")
    return render(request, "promotions_student_list.html", form_vals)

@user_login_required
def SavePromotionList(request):
    print "in view promotion List"


    all_sections = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        all_classes = current_academic_year_mapped_classes(selected_year_id, request.user)

        studen_ids = request.POST.get('strikeId')
        year_id = request.POST.get('year_name1')
        class_id = request.POST.get('class_name1')
        section_id = request.POST.get('section_name1')
        check = ast.literal_eval(studen_ids)

        if type(check) is int:
            student_recs = student_details.objects.filter(id=check)
        else:
            student_recs = student_details.objects.filter(id__in=check)
            # check_list = list(check)

        acd_class_sec_map_id = academic_class_section_mapping.objects.get(year_name=year_id, section_name=section_id, class_name=class_id)
        for rec in student_recs:
            if rec:
                form = promotion_details(student_id = rec.id, academic_class_section_id = acd_class_sec_map_id.id,  is_promotion_applied = True)
                form.save()
        messages.success(request, "Selected Students Registered for Promotions.. Please Wait for confirmation.")

        return redirect('/school/promotions_student_list/')
        # return render(request, "promotions_student_list.html", {'ClassDetails' : all_classes, 'Section' : all_sections , "year_list" : all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})
    except:
        current_academic_year = None
        selected_year_id = None
        messages.success(request, "Some Error Occoured... Please Try Again")
        return redirect('/school/promotions_student_list/')
        # return render(request, "promotions_student_list.html", {'ClassDetails' : all_classes, 'Section' : all_sections , "year_list" : all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})

@user_login_required
def PromotionConfirmationList(request):

    all_classes = class_details.objects.all()
    # all_sections = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        all_classes = current_academic_year_mapped_classes(selected_year_id, request.user)
    except:
        messages.success(request, "Academic Year Class Section Mapping Not Fund.")
        return redirect('/school/promotions_menu/')

    return render(request, "promotion_confirmation_list.html", {'ClassDetails': all_classes,"year_list": all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})


def load_pro_cnf_data(val_dict, user_type):
    # all_classes = class_details.objects.all()
    promotion_obj = ''
    all_sections = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    selected_year_id = val_dict.get('year_name')  # It will be used in HTML page as a value of selected Class
    selected_year_rec = academic_year.objects.get(id=selected_year_id)
    selected_year_name = selected_year_rec.year_name

    selected_class_id = val_dict.get('class_name')  # It will be used in HTML page as a value of selected Class
    selected_class = class_details.objects.get(id=selected_class_id)
    selected_class_name = selected_class.class_name

    selected_section_id = val_dict.get(
        'section_name')  # It will be used in HTML page as a value of selected Section
    selected_section = sections.objects.get(id=selected_section_id)
    selected_section_name = sections.section_name

    all_classes = current_academic_year_mapped_classes(selected_year_id, user_type)
    all_sections = current_class_mapped_sections(selected_year_id, selected_class_id, user_type)


    #=========================================================
    current_acd_year = academic_year.objects.get(year_name=selected_year_name)

    next_acd_year = (current_acd_year.end_date - all_academicyr_recs[0].start_date).days
    for academicyr_rec in all_academicyr_recs:
        if academicyr_rec:
            acd_year = academicyr_rec.start_date - current_acd_year.end_date
            if acd_year.days > 0:
                if next_acd_year > acd_year.days:
                    next_acd_year = acd_year.days
                    next_acd_year_obj = academicyr_rec

    next_year_section_list = []

    next_year_section = academic_class_section_mapping.objects.filter(year_name=next_acd_year_obj.id,
                                                                             class_name=selected_class_id)

    for section in next_year_section:
        next_year_section_list.append(section.section_name)

    #=================================================================
    acd_year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                            section_name=selected_section_id,
                                                                            class_name=selected_class_id)
    filtered_students = student_details.objects.filter(academic_class_section=acd_year_class_section_obj,
                                                       is_active=True, joining_date__lte=todays_date())
    mapped_list = []

    for student_rec in filtered_students:
        if student_rec:
            if (promotion_details.objects.filter(student=student_rec.id, is_promotion_applied=True).exists()):
                promotion_obj = promotion_details.objects.get(student=student_rec.id, is_promotion_applied=True)
                promotion_obj.academic_class_section.year_name.start_date = promotion_obj.academic_class_section.year_name.start_date.strftime(
                    '%d-%m-%Y')
                mapped_data = {
                    'student_first_name': student_rec.first_name,
                    'student_last_name': student_rec.last_name,
                    # 'student_Parent': student_rec.parent,
                    'student_code': student_rec.student_code,
                    'student_id': student_rec.id,
                    'student_rec': student_rec,

                    'class_name': student_rec.academic_class_section.class_name.class_name,
                    'section_name': student_rec.academic_class_section.section_name.section_name,
                    'academic_year': student_rec.academic_class_section.year_name.year_name,

                    'promoted_class': promotion_obj.academic_class_section.class_name,
                    'promoted_section': promotion_obj.academic_class_section.section_name,
                    'promoted_acd_year': promotion_obj.academic_class_section.year_name,
                }
                mapped_list.append(mapped_data)

    # Passing values to the HTML page through form_vals
    form_vals = {
        'StudentList': mapped_list,
        'Selected_Section_Id': selected_section_id,
        'SectName': selected_section_name,
        'section_name': selected_section,
        'Section': all_sections,
        'Selected_Class_Id': selected_class_id,
        'ClassName': selected_class_name,
        'class_name': selected_class,
        'ClassDetails': all_classes,

        'Selected_Year_Id': selected_year_id,
        'selected_year_name': selected_year_name,
        'selected_year': selected_year_rec,
        'year_list': all_academicyr_recs,
        'next_year_section_list':next_year_section_list
    }
    return form_vals

@user_login_required
def PromotionConfirmationFilteredList(request):
    val_dict = request.POST
    form_vals = {}

    if request.method == 'POST':
        request.session['val_dict'] = val_dict
        form_vals = load_pro_cnf_data(val_dict,request.user)
    else:
        form_vals = load_pro_cnf_data(request.session.get('val_dict'),request.user)

    return render(request, "promotion_confirmation_list.html", form_vals)

@user_login_required
def PromoteStudents(request):

    all_classes = class_details.objects.all()
    all_sections = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        all_classes = current_academic_year_mapped_classes(selected_year_id, request.user)

        if request.method == 'POST':
            check = request.POST.getlist('check')

            for student_id in check:
                promoted_rec = promotion_details.objects.get(student = student_id, is_promotion_applied = True)
                # student_details.objects.filter(id = student_id).update(academic_class_section_id = promoted_rec.academic_class_section.id)
                promotion_details.objects.filter(id = promoted_rec.id).update(is_promotion_applied = False, is_promoted = True )
            messages.success(request, "Selected Students Has Been Promoted To Selected Classes.")
    except:
        messages.success(request, "Academic Year Class Section Mapping Not Fund.")
    return redirect('/school/promotion_confirmation_filtered_list/')
        # return render(request, "promotions_menu.html")
        # current_academic_year = None
        # selected_year_id = None

    # return render(request, "promotion_confirmation_list.html", {'ClassDetails': all_classes, 'Section': all_sections, "year_list": all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})
@user_login_required
def promotion_update_section_info(request):
    if request.method == 'POST':
            check = request.POST.get('section_data')
            selected_id = json.loads(check)

    try:
        for i in selected_id:
            student_id = i['student_id']
            student_recs = student_details.objects.filter(id=student_id)
            acd_class_sec_map_id = academic_class_section_mapping.objects.get(year_name=i['year_name'], section_name=i['selected_section'],class_name=i['class_name'])
            promotion_details.objects.filter(student_id=student_recs[0].id).update(academic_class_section_id = acd_class_sec_map_id.id, is_promotion_applied = True)
        messages.success(request, "One record updated successfully")
    except:
        messages.success(request, "Some Error Occoured... Please Try Again")
    return redirect('/school/promotion_confirmation_filtered_list/')

@user_login_required
def DetainStudents(request):
    all_classes = class_details.objects.all()
    all_sections = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()


    studen_ids = request.POST.get('strikeId1')
    year_id = request.POST.get('detain_year')
    class_id = request.POST.get('detain_class')
    section_id = request.POST.get('detain_section_name')
    check = ast.literal_eval(studen_ids)
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        all_classes = current_academic_year_mapped_classes(selected_year_id, request.user)
        acd_class_sec_map_id = academic_class_section_mapping.objects.get(year_name=year_id, section_name=section_id, class_name=class_id)

        if type(check) is int:
            promotion_details.objects.create(student_id=check, academic_class_section_id=acd_class_sec_map_id.id, is_detained=True)
            # student_recs = student_details.objects.filter(id = check)
        else:
            for id in check:
                promotion_details.objects.create(student_id=id, academic_class_section_id=acd_class_sec_map_id.id,is_detained=True)

        messages.success(request, "Selected Students Has Been Sent For Detention.")
    except:
        current_academic_year = None
        selected_year_id = None
        messages.success(request, "Some Error Occoured... Please Try Again...")
    return redirect('/school/promotions_student_list/')
    # return render(request, "promotions_student_list.html", {'ClassDetails' : all_classes, 'Section' : all_sections , "year_list" : all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})



@user_login_required
def Promote(request):
    all_academicyr_recs = academic_year.objects.all()

    current_academic_year = academic_year.objects.filter(current_academic_year=1)
    selected_year_id = current_academic_year[0].id
    ay = request.POST.get('year_name')

    if  ay:
        academic_year.objects.filter(id = selected_year_id).update(current_academic_year=0)
        academic_year.objects.filter(id = ay).update(current_academic_year = True)
    #
    student_list = promotion_details.objects.filter(Q(is_detained=True) | Q(is_promoted=True))

    for student in student_list:
        if student:
            if student.academic_class_section.year_name.id == int(ay):

                if student.is_detained ==True:
                    promoted_rec = promotion_details.objects.get(id=student.id)
                    promotion_details.objects.filter(id=promoted_rec.id).update(is_detained = False)
                    student_details.objects.filter(id=student.student_id).update( academic_class_section_id=student.academic_class_section_id)
                else:
                    promoted_rec = promotion_details.objects.get(id=student.id)
                    promotion_details.objects.filter(id=promoted_rec.id).update(final_promoted=True, is_promoted=False)
                    student_details.objects.filter(id=student.student_id).update(academic_class_section_id=student.academic_class_section_id)
    # print "ff"

    return render(request, "promote.html", {'Selected_Year_Id':selected_year_id, 'year_list':all_academicyr_recs})
from django.conf.urls import include, url
from django.contrib import admin
from . import views
from django.contrib.auth.views import logout
from django.conf import settings
from django.conf.urls.static import static, serve

app_name = 'promotions'

urlpatterns = [
      url(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
      url(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),
######################################### ID Details ###################################
      url(r'^promotions_menu/$', views.PromotionMenu, name='promotions_menu'),
      url(r'^promotions_student_page/$', views.PromotionStudent, name='promotions_student_page'),
      url(r'^promotions_student_list/$', views.PromotionList, name='promotions_student_list'),
      url(r'^save_promotion_list/$', views.SavePromotionList, name='save_promotion_list'),
      url(r'^promotion_confirmation_list/$', views.PromotionConfirmationList, name='promotion_confirmation_list'),
      url(r'^promotion_confirmation_filtered_list/$', views.PromotionConfirmationFilteredList, name='promotion_confirmation_filtered_list'),
      url(r'^promote_students/$', views.PromoteStudents, name='promote_students'),
      url(r'^detain_students/$', views.DetainStudents, name='detain_students'),
      url(r'^promote/$', views.Promote, name='promote'),
      url(r'^promotion_update_section_info/$',views.promotion_update_section_info, name='promotion_update_section_info'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
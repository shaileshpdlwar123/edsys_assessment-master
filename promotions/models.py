from __future__ import unicode_literals

from django.db import models
from registration.models import *


# Create your models here.
class promotion_details(models.Model):
    is_promotion_applied = models.BooleanField(default=False)
    is_promoted = models.BooleanField(default=False)
    final_promoted = models.BooleanField(default=False)
    is_detained = models.BooleanField(default=False)
    academic_class_section = models.ForeignKey(academic_class_section_mapping, null=True)
    student = models.ForeignKey(student_details)
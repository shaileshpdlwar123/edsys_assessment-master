from django.shortcuts import render, redirect, render_to_response
from registration.models import *
from forms import *
from medical.models import *
import ast
from django.contrib import messages
import xlwt
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext
from wsgiref.util import FileWrapper
import xlrd
import random, string
import django_excel as excel
from pickle import NONE
from registration.decorators import user_permission_required
from masters.utils import *
from registration.decorators import user_login_required, user_permission_required
from masters.utils import export_users_xls, todays_date
from datetime import datetime
import json

@user_login_required
@user_permission_required('medical.can_view_medical_info', '/school/home/')
def MedicalMenu(request):
    return render(request, "medical.html")


@user_login_required
@user_permission_required('medical.can_view_add_medical_details', '/school/home/')
def ViewClassSection(request):
    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    year_class_section_object = academic_class_section_mapping.objects.all()
    if year_class_section_object:
        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id
            return render(request, "view_class_section.html",
                          {'class_list': all_class_recs, 'section_list': all_section_recs,
                           "year_list": all_academicyr_recs, 'selected_year_name': current_academic_year,
                           'Selected_Year_Id': selected_year_id})
        except academic_year.DoesNotExist:
            messages.success(request, "Current Academic Year Not Fund.")
            return render(request, 'medical.html')
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Fund.")
        return render(request, 'medical.html')

def load_medical_filter(request, val_dict_medical):

    all_academicyr_recs = academic_year.objects.all()

    selected_year_id = val_dict_medical.get('year_name')  # It will be used in HTML page as a value of selected Class
    selected_year_rec = academic_year.objects.get(id=selected_year_id)
    selected_year_name = selected_year_rec.year_name

    selected_class_id = val_dict_medical.get('class_name')  # It will be used in HTML page as a value of selected Class
    selected_class_rec = class_details.objects.get(id=selected_class_id)
    selected_class_name = selected_class_rec.class_name

    selected_section_id = val_dict_medical.get('section_name')  # It will be used in HTML page as a value of selected Section
    selected_section_rec = sections.objects.get(id=selected_section_id)
    selected_section_name = selected_section_rec.section_name
    all_section_recs = current_class_mapped_sections(selected_year_rec, selected_class_rec, request.user)

    year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                        section_name=selected_section_id,
                                                                        class_name=selected_class_id)
    filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True,
                                                       joining_date__lte=todays_date())
    all_class_recs = current_academic_year_mapped_classes(selected_year_id, request.user)

    student_list = []
    medical_list = []

    for obj in filtered_students:

        student_id = obj.id
        student_name = student_medical.objects.filter(student=student_id)
        if not student_name:
            obj = student_details.objects.get(id=student_id)
            student_list.append(obj)

        else:
            for obj in student_name:
                medical_list.append(obj)


                #     Passing values to the HTML page through form_vals
    form_vals = {
        'StudentList': student_list,
        'Selected_Section_Id': selected_section_id,
        'selected_section_name': selected_section_name,
        'selected_section': selected_section_rec,
        'section_list': all_section_recs,
        'Selected_Class_Id': selected_class_id,
        'selected_class_name': selected_class_name,
        'selected_class': selected_class_rec,
        'class_list': all_class_recs,
        'MedicalList': medical_list,

        'Selected_Year_Id': selected_year_id,
        'selected_year_name': selected_year_name,
        'selected_year': selected_year_rec,
        'year_list': all_academicyr_recs,
    }
    return form_vals

@user_login_required
@user_permission_required('medical.can_view_add_medical_details', '/school/home/')
def ViewMedicalList(request):
    val_dict_medical = request.POST
    form_vals = {}

    if request.method == 'POST':
        request.session['val_dict_medical'] = val_dict_medical
        form_vals = load_medical_filter(request, val_dict_medical)
    else:
        form_vals = load_medical_filter(request, request.session.get('val_dict_medical'))

    return render(request, "medical_list.html", form_vals)


@user_login_required
@user_permission_required('medical.add_student_medical', '/school/home/')
def SaveMedicalInfo(request):
    try:
        data = json.loads(request.POST.get('json_data'))

        student_ids = student_medical.objects.all().values_list('student__id',flat=1)

        for rec in data:
            stud_rec = student_details.objects.get(id=rec['stud_id'])

            if stud_rec.id not in student_ids:
                student_medical.objects.create(student=stud_rec, bmi=rec['bmi'], allergic=rec['allergic'],
                                               under_medication=rec['medication'], weight=rec['weight'],
                                               height=rec['height'], blood_group=rec['bloodgrp'])

        messages.success(request, "Record saved successfully")
    except:
        messages.warning(request, "Record Not Saved... Some Error Occurred.")
    return redirect('/school/view_medical_list/')

# @user_login_required
# @user_permission_required('medical.add_student_medical', '/school/home/')
# def SaveMedicalInfo(request):
#     all_class_recs = class_details.objects.all()
#     all_section_recs = sections.objects.all()
#     all_academicyr_recs = academic_year.objects.all()
#
#     try:
#         current_academic_year = academic_year.objects.get(current_academic_year=1)
#         selected_year_id = current_academic_year.id
#         selected_year_rec = academic_year.objects.get(id=selected_year_id)
#         selected_year_name = selected_year_rec.year_name
#
#         selected_class_id = request.POST.get('class_name')  # It will be used in HTML page as a value of selected Class
#         selected_class_rec = class_details.objects.get(id=selected_class_id)
#         selected_class_name = selected_class_rec.class_name
#
#         selected_section_id = request.POST.get(
#             'section_name')  # It will be used in HTML page as a value of selected Section
#         selected_section_rec = sections.objects.get(id=selected_section_id)
#         selected_section_name = selected_section_rec.section_name
#
#         year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
#                                                                             section_name=selected_section_id,
#                                                                             class_name=selected_class_id)
#         filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj,
#                                                            is_active=True, joining_date__lte=todays_date())
#
#         if request.method == 'POST':
#             medical_details_form = MedicalDetailsForm(request.POST)
#             if medical_details_form.is_valid():
#                 studentid = request.POST.get('studentid')
#                 blood_group = request.POST.get('bloodgrp')
#                 height = request.POST.get('height')
#                 weight = request.POST.get('weight')
#                 bmi = request.POST.get('calculate_bmi')
#                 under_medication = request.POST.get('medication')
#                 allergic = request.POST.get('allergic')
#                 student = medical_details_form.save(commit=False)
#                 student.blood_group = blood_group
#                 student.under_medication = under_medication
#                 student.student_id = request.POST['studentid']
#                 student.save()
#
#                 student.bmi = bmi
#                 student.save()
#                 messages.success(request, "Record Successfully Saved.")
#
#             student_updated_list = []
#             medical_updated_list = []
#
#             for obj in filtered_students:
#
#                 student_id = obj.id
#                 student_name = student_medical.objects.filter(student=student_id)
#                 if not student_name:
#                     obj = student_details.objects.get(id=student_id)
#                     student_updated_list.append(obj)
#
#                 else:
#                     for obj in student_name:
#                         medical_updated_list.append(obj)
#
#                         #     Passing values to the HTML page through form_vals
#             form_updated_vals = {
#                 'StudentList': student_updated_list,
#                 'Selected_Section_Id': selected_section_id,
#                 'selected_section_name': selected_section_name,
#                 'selected_section': selected_section_rec,
#                 'section_list': all_section_recs,
#
#                 'Selected_Class_Id': selected_class_id,
#                 'selected_class_name': selected_class_name,
#                 'selected_class': selected_class_rec,
#                 'class_list': all_class_recs,
#                 'MedicalList': medical_updated_list,
#
#                 'Selected_Year_Id': selected_year_id,
#                 'selected_year_name': selected_year_name,
#                 'selected_year': selected_year_rec,
#                 'year_list': all_academicyr_recs,
#
#             }
#             return render(request, "medical_list.html", form_updated_vals)
#     except:
#         messages.warning(request, "Record Not Saved... Some Error Occoured.")
#     return render(request, "medical.html")


@user_login_required
@user_permission_required('medical.can_view_view_edit_medical_details', '/school/home/')
def ViewUpdateClassSection(request):
    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    year_class_section_object = academic_class_section_mapping.objects.all()
    if year_class_section_object:
        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id
            return render(request, "view_update_class_section.html",
                          {'class_list': all_class_recs, 'section_list': all_section_recs,
                           "year_list": all_academicyr_recs, 'selected_year_name': current_academic_year,
                           'Selected_Year_Id': selected_year_id})
        except academic_year.DoesNotExist:
            messages.success(request, "Current Academic Year Not Fund.")
            return render(request, 'medical.html')

    else:
        messages.success(request, "Academic Year Class Section Mapping Not Fund.")
        return render(request, 'medical.html')


def load_view_medical_filter(request, val_dict_view_medical):

    medical_detail_excel = MedicalDetailsExcelForm()
    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    selected_year_id = val_dict_view_medical.get('year_name')  # It will be used in HTML page as a value of selected Class
    selected_year_rec = academic_year.objects.get(id=selected_year_id)
    selected_year_name = selected_year_rec.year_name

    selected_class_id = val_dict_view_medical.get('class_name')  # It will be used in HTML page as a value of selected Class
    selected_class_rec = class_details.objects.get(id=selected_class_id)
    selected_class_name = selected_class_rec.class_name
    all_class_recs = current_academic_year_mapped_classes(selected_year_id, request.user)

    selected_section_id = val_dict_view_medical.get('section_name')  # It will be used in HTML page as a value of selected Section
    selected_section_rec = sections.objects.get(id=selected_section_id)
    selected_section_name = selected_section_rec.section_name
    all_section_recs = current_class_mapped_sections(selected_year_rec, selected_class_rec, request.user)

    year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                        section_name=selected_section_id,
                                                                        class_name=selected_class_id)
    filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True,
                                                       joining_date__lte=todays_date())

    student_list = []
    medical_list = []

    for obj in filtered_students:
        student_id = obj.id
        student_name = student_medical.objects.filter(student=student_id)

        if not student_name:
            obj = student_details.objects.get(id=student_id)
            student_list.append(obj)

        else:
            for obj in student_name:
                medical_list.append(obj)

                #     Passing values to the HTML page through form_vals
    form_vals = {
        'StudentList': student_list,
        'Selected_Section_Id': selected_section_id,
        'selected_section_name': selected_section_name,
        'selected_section': selected_section_rec,
        'section_list': all_section_recs,
        'Selected_Class_Id': selected_class_id,
        'selected_class_name': selected_class_name,
        'selected_class': selected_class_rec,
        'class_list': all_class_recs,
        'MedicalList': medical_list,
        'MedicalDetailsExcel': medical_detail_excel,

        'Selected_Year_Id': selected_year_id,
        'selected_year_name': selected_year_name,
        'selected_year': selected_year_rec,
        'year_list': all_academicyr_recs,
    }
    return form_vals

@user_login_required
@user_permission_required('medical.can_view_add_medical_details', '/school/home/')
def ViewUpdateMedicalList(request):
    val_dict_view_medical = request.POST
    form_vals = {}

    if request.method == 'POST':
        request.session['val_dict_view_medical'] = val_dict_view_medical
        form_vals = load_view_medical_filter(request, val_dict_view_medical)
    else:
        form_vals = load_view_medical_filter(request, request.session.get('val_dict_view_medical'))

    return render(request, "update_medical_list.html", form_vals)

@user_login_required
@user_permission_required('medical.change_student_medical', '/school/home/')
def UpdateMedicalInfo(request):
    student = request.GET.get('stdid')
    bloodgroup = request.GET.get('bldgrp')
    height = request.GET.get('hgt')
    weight = request.GET.get('wgt')
    bmi=request.GET.get('BMI')
    medication = request.GET.get('mdt')
    allergic = request.GET.get('alg')

    if (bloodgroup == ''):
        messages.success(request, "Blood Group is Mandatory... Record Not Updated.")
        return redirect('/school/medical_menu/')

    else:
        Studentid = student_details.objects.filter(id=student)
        student_medical.objects.filter(student_id=Studentid).update(blood_group=bloodgroup, height=height,weight=weight, under_medication=medication,bmi=bmi, allergic=allergic,update_date=datetime.now())
        messages.success(request, "Record Successfully Updated.")
    return redirect('/school/medical_menu/')


@user_login_required
@user_permission_required('medical.can_view_medical_info', '/school/home/')
def ExportMedicalInfo(request):
    medical_detail_excel = MedicalDetailsExcelForm()

    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    selected_class_id = request.POST.get('class_name')
    selected_class_rec = class_details.objects.get(id=selected_class_id)
    selected_class_name = selected_class_rec.class_name

    selected_section_id = request.POST.get('section_name')
    selected_section_rec = sections.objects.get(id=selected_section_id)
    selected_section_name = selected_section_rec.section_name
    selected_year_id = request.POST.get('year_name')  # It will be used in HTML page as a value of selected Class
    selected_year_rec = academic_year.objects.get(id=selected_year_id)
    selected_year_name = selected_year_rec.year_name


    file_name = "Data_" + str(selected_class_rec) + "_" + str(selected_section_rec)

    year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                        section_name=selected_section_id,
                                                                        class_name=selected_class_id)
    filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True,
                                                       joining_date__lte=todays_date()).values_list('id',flat=1)

    # filtered_students = student_details.objects.filter(class_name_id = selected_class_rec, section_id = selected_section_rec, is_active = 1) #is_active = 1

    student_list = []
    medical_list = []

    # for obj in filtered_students:
    #     student_id = obj.id
    medical_recs = student_medical.objects.filter(student__id__in=filtered_students) #.values_list('student__student_code',
                                                                                      # 'student__first_name',
                                                                                      # 'blood_group', 'height', 'weight','bmi',
                                                                                      # 'under_medication', 'allergic',
                                                                                      # 'student_id')

    for student_medical_obj in medical_recs:
        temp_rec_list = []
        temp_rec_list.append(student_medical_obj.student.student_code)
        temp_rec_list.append(student_medical_obj.student.get_all_name())
        temp_rec_list.append(student_medical_obj.student.odoo_id)
        temp_rec_list.append(student_medical_obj.blood_group)
        temp_rec_list.append(student_medical_obj.height)
        temp_rec_list.append(student_medical_obj.weight)
        if student_medical_obj.bmi ==None:
            temp_rec_list.append("")
        else:
            temp_rec_list.append(student_medical_obj.bmi)
        temp_rec_list.append(student_medical_obj.under_medication)
        temp_rec_list.append(student_medical_obj.allergic)
        medical_list.append(temp_rec_list)

    try:
        column_names = ['Student Code', 'Student Name', 'Scodoo Id', 'Blood Group', 'Height', 'Weight','BMI', 'Under Medication', 'Allergic']
        return export_locked_column_xls(file_name, column_names, medical_list,[0,1,2,6])
    except:
        messages.success(request, "Some Error Occurred...")
        form_vals = {
            'StudentList': student_list,
            'Selected_Section_Id': selected_section_id,
            'selected_section_name': selected_section_name,
            'selected_section': selected_section_rec,
            'section_list': all_section_recs,
            'Selected_Class_Id': selected_class_id,
            'selected_class_name': selected_class_name,
            'selected_class': selected_class_rec,
            'class_list': all_class_recs,
            'MedicalList': medical_list,
            'MedicalDetailsExcel': medical_detail_excel,

            'Selected_Year_Id': selected_year_id,
            'selected_year_name': selected_year_name,
            'selected_year': selected_year_rec,
            'year_list': all_academicyr_recs,
        }
    return render(request, 'update_medical_list.html', form_vals, {'context1': 'not done'})


@user_login_required
@user_permission_required('medical.can_view_medical_info', '/school/home/')
def ImportMedicalInfo(request):

    if request.method == 'POST':
        success_flag = False
        blood_group_flag = False
        bmi = ''
        weight = ''
        height = ''
        height_in_meter = ''
        try:
            medical_details_excel = MedicalDetailsExcelForm(request.POST, request.FILES)
            if medical_details_excel.is_valid():
                file_recs = request.FILES['excel'].get_records()
                for file_rec in file_recs:
                    student_rec = student_details.objects.filter(student_code=file_rec['Student Code'])
                    student_medical_rec = student_medical.objects.filter(student=student_rec)

                    if(student_medical.objects.filter(student=student_rec,blood_group=file_rec['Blood Group'], height=file_rec['Height'],weight=file_rec['Weight'], under_medication=file_rec['Under Medication'],allergic=file_rec['Allergic']).exists()) :
                        pass

                    elif (file_rec['Blood Group'] == ""):
                        blood_group_flag = True

                    elif (file_rec['Blood Group']=="" and file_rec['Height']==""and file_rec['Weight'] ==""and file_rec['Under Medication']==''and file_rec['Allergic']==""):
                        pass

                    else:
                        bmi = file_rec['BMI']
                        if (file_rec['Height'] != "" and file_rec['Weight'] != "" ):
                            height =  float(file_rec['Height'])
                            weight =  float(file_rec['Weight'])
                            height_in_meter = height / 100
                            bmi =weight /((height_in_meter)*(height_in_meter));
                            bmi = round(bmi,2)

                        student_medical_rec.update(blood_group=file_rec['Blood Group'], height=file_rec['Height'],
                                               weight=file_rec['Weight'],bmi=bmi, under_medication=file_rec['Under Medication'],
                                               allergic=file_rec['Allergic'],update_date =datetime.now())
        except:
            messages.warning(request,"Some Error Occoured.. It may be happen because of invalid file, or invalid file data. ")

        if success_flag:
            messages.warning(request,"Records Updated")

        if blood_group_flag:
            messages.warning(request, "Blood Group is mandatory")

    return redirect('/school/view_update_medical_list/')

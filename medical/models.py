from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from registration.models import *
from datetime import datetime
# Create your models here.
class student_medical(models.Model):
    blood_group = models.CharField(max_length=250)
    height = models.CharField(max_length=250)
    weight = models.CharField(max_length=250)
    under_medication = models.CharField(max_length=250)#selection field and get object all on forms.py
    allergic = models.CharField(max_length=250)#selection field and get object all on forms.py
    bmi = models.CharField(max_length=250,null=True)
    
    student = models.ForeignKey(student_details,related_name='student_medical_rel')
    create_date = models.DateTimeField(default=datetime.now)
    update_date = models.DateTimeField(default=datetime.now)


    class Meta:
        permissions = (
            ('can_view_medical_info', 'can view medical info'),
            ('can_view_add_medical_details', 'can view add medical details'),
            ('can_view_view_edit_medical_details', 'can view view edit medical details'),
        )

class medical_details_excel(models.Model):
    excel = models.FileField(upload_to = 'uploads/')
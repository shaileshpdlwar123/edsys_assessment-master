from django import forms
from registration.models import *
from medical.models import *
from registration.forms import *

class MedicalDetailsForm(forms.ModelForm):
    blood_group = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    height = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','onkeypress':'return isNumber(event)',}))
    weight = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    bmi = forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    under_medication = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    allergic = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    # student_name = forms.ModelChoiceField(required=False,queryset=student_details.objects.all(), widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    class Meta:

        model = student_medical
        fields =['blood_group','height','bmi','weight','under_medication','allergic']


class MedicalDetailsExcelForm(forms.ModelForm):
    excel = forms.FileField(required=True, label='Upload File')

    class Meta:
        model = medical_details_excel
        fields = ['excel']
from django.conf.urls import include, url
from django.contrib import admin
from . import views
from django.contrib.auth.views import logout
from django.conf import settings
from django.conf.urls.static import static, serve

app_name = 'medical'

urlpatterns = [
                  url(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
                  url(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),

                  url(r'^medical_menu/$', views.MedicalMenu, name='medical_menu'),
                  url(r'^view_class_section_details/$', views.ViewClassSection, name='view_class_section_details'),
                  url(r'^view_medical_list/$', views.ViewMedicalList, name='view_medical_list'),
                  url(r'^save_medical_info/$', views.SaveMedicalInfo, name='save_medical_info'),

                  url(r'^view_update_class_section_details/$', views.ViewUpdateClassSection,
                      name='view_update_class_section_details'),
                  url(r'^view_update_medical_list/', views.ViewUpdateMedicalList, name='view_update_medical_list'),
                  url(r'^upated_medical_info/$', views.UpdateMedicalInfo, name='upated_medical_info'),
                  url(r'^export_medical_info/$', views.ExportMedicalInfo, name='export_medical_info'),
                  url(r'^import_medical_info/$', views.ImportMedicalInfo, name='import_medical_info'),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

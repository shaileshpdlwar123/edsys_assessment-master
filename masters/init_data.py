# -*- coding: utf-8 -*-
__author__ = 'vaibhav'

from django.conf import settings
import os
from django.contrib.auth.models import Group
from registration.models import nationality, religion, school_details, role, department_details, AuthUser, month,custom_groups,custom_sub_groups
from masters.models import exam_type ,tc_type,assessment_configration_master
import datetime
import json
from attendance.models import absent_categories, present_categories

"""
>>> from masters.init_data import load_initial_data
>>> load_initial_data()
"""

def import_Group(file_path):

    ROLE_ID_INDEX = 0
    ROLE_name_INDEX = 1

    IMPORTING_MESSAGE = "imported %d role"
    with open(file_path) as fp:
        fp.readline() #ignore titles (chapter_id	chapter	block_id	block	icd_id	icd_label)
        count = 0
        for line in fp:
            line = line.strip()
            tokens = line.split('\t')
            Group.objects.create(name=tokens[ROLE_name_INDEX])
            count += 1
            if count%1000 == 0:
                print IMPORTING_MESSAGE % count
        print IMPORTING_MESSAGE % count

def import_role(file_path):

    ROLE_ID_INDEX = 0
    ROLE_name_INDEX = 1

    IMPORTING_MESSAGE = "imported %d role"
    with open(file_path) as fp:
        fp.readline() #ignore titles (chapter_id	chapter	block_id	block	icd_id	icd_label)
        count = 0
        for line in fp:
            line = line.strip()
            tokens = line.split('\t')
            role.objects.create(name=tokens[ROLE_name_INDEX])
            count += 1
            if count%1000 == 0:
                print IMPORTING_MESSAGE % count
        print IMPORTING_MESSAGE % count

def import_default_role():

    raw_dir = settings.RAW_ROOT
    role_path = os.path.join(raw_dir, 'text', 'role.txt')
    import_role(role_path)
    import_Group(role_path)


def import_nationality(file_path):

    NATIONALITY_ID_INDEX = 0
    NATIONALITY_NAME_INDEX = 1

    IMPORTING_MESSAGE = "imported %d nationality"
    with open(file_path) as fp:
        fp.readline() #ignore titles (chapter_id	chapter	block_id	block	icd_id	icd_label)
        count = 0
        for line in fp:
            line = line.strip()
            tokens = line.split('\t')
            nationality.objects.create(nationality_name=tokens[NATIONALITY_NAME_INDEX])
            count += 1
            if count%1000 == 0:
                print IMPORTING_MESSAGE % count
        print IMPORTING_MESSAGE % count

def import_default_nationality():

    raw_dir = settings.RAW_ROOT
    nationality_path = os.path.join(raw_dir, 'text', 'nationality.txt')
    import_nationality(nationality_path)

def import_religion(file_path):

    RELIGION_ID_INDEX = 0
    RELIGION_NAME_INDEX = 1

    IMPORTING_MESSAGE = "imported %d religion"
    with open(file_path) as fp:
        fp.readline() #ignore titles (chapter_id	chapter	block_id	block	icd_id	icd_label)
        count = 0
        for line in fp:
            line = line.strip()
            tokens = line.split('\t')
            religion.objects.create(religion_name=tokens[RELIGION_NAME_INDEX])
            count += 1
            if count%1000 == 0:
                print IMPORTING_MESSAGE % count
        print IMPORTING_MESSAGE % count

def import_default_religion():

    raw_dir = settings.RAW_ROOT
    religion_path = os.path.join(raw_dir, 'text', 'religion.txt')
    import_religion(religion_path)



def LoadInitialSchoolData():

    # print "LoadInitialSchoolData ", str(datetime.datetime.now())
    # global HANDLE
    # raw_dir = settings.RAW_ROOT
    # schoolfile = os.path.join(raw_dir, 'text', 'default_school.txt')
    # with open(schoolfile, 'r') as data_file:
    #
    #     print 'Data', data_file
    #     school_array = json.load(data_file)
    #     data_file.close()
    #
    # for user_obj in school_array:

    user_obj = {
        "school_name": "Bishop",
        "contact_num": "1234567890",
        "email": "bishop@school.com",
        "school_address": "MG Road",
        "street": "MG Road",
        "city": "Pune",
        "state": "Maharashtra",
        "pin": "123456",
        "website": "https://www.bishop.co.edu",
    }

    school_details.objects.create(school_name=user_obj['school_name'], school_address=user_obj['school_address'], contact_num=user_obj['contact_num'], email=user_obj['email'], website=user_obj['website'], city=user_obj['city'], pin=user_obj['pin'], state=user_obj['state'], street=user_obj['street'])


def load_absent_category():
    absent_categories.objects.create(absent_category_code='Category-01', absent_category_name='Medical')
    absent_categories.objects.create(absent_category_code='Category-02', absent_category_name='Excused')

def load_present_category():
    present_categories.objects.create(present_category_code='Category-01', present_category_name='Late')
    present_categories.objects.create(present_category_code='Category-02', present_category_name='Warned')

def load_department():
    department_details.objects.create(depatment_name='Front End')
    department_details.objects.create(depatment_name='Back End')

def load_exam_type():
    exam_type.objects.create(exam_type_name='Baseline')
    exam_type.objects.create(exam_type_name='Formative')
    exam_type.objects.create(exam_type_name='Summative')

def load_assessment_configration():
    assessment_configration_master.objects.create(configration_name='‘Absent’')
    assessment_configration_master.objects.create(configration_name='‘NA’')
    assessment_configration_master.objects.create(configration_name='Joined Late')
    assessment_configration_master.objects.create(configration_name='Medical')


def default_role_as_admin():
    role_rec = role.objects.get(name='System Admin')
    user = AuthUser.objects.get(username='admin')
    group_rec = Group.objects.get(name=role_rec.name)
    user.role = role_rec
    user.save()
    user.groups.set([group_rec])

def import_month_master():
    month_lst = ['January', 'February', 'March', 'April', 'May', 'June', 'July',
                 'August', 'September', 'October', 'November', 'December']

    for month_obj in month_lst:
        month.objects.create(month_name=month_obj)


def load_tc_type():
    tc_type.objects.create(tc_type_name='Inside Dubai')
    tc_type.objects.create(tc_type_name='Inside UAE')


def load_custom_group():
    custom_group_name = custom_groups.objects.create(group_name='Gender', is_primary=True)
    custom_group_id = custom_group_name.id
    if (custom_group_id >= 1) and (custom_group_id <= 9):
        grade_code = "G-000" + str(custom_group_id)
        custom_groups.objects.filter(id=custom_group_id).update(group_code=grade_code)
    elif (custom_group_id >= 10) and (custom_group_id <= 99):
        grade_code = "G-00" + str(custom_group_id)
        custom_groups.objects.filter(id=custom_group_id).update(group_code=grade_code)
    elif (custom_group_id >= 100) and (custom_group_id <= 999):
        grade_code = "G-0" + str(custom_group_id)
        custom_groups.objects.filter(id=custom_group_id).update(group_code=grade_code)
    else:
        grade_code = "G-" + str(custom_group_id)
        custom_groups.objects.filter(id=custom_group_id).update(group_code=grade_code)

    gender_list = ['Male', 'Female']
    for gender_rec in gender_list:
        custom_sub_group_obj = custom_sub_groups.objects.create(sub_group_name=gender_rec)
        custom_sub_group_id = custom_sub_group_obj.id
        if (custom_sub_group_id >= 1) and (custom_sub_group_id <= 9):
            sub_group_code = "S-000" + str(custom_sub_group_id)
            custom_sub_groups.objects.filter(id=custom_sub_group_id).update(sub_group_code=sub_group_code)

        elif (custom_sub_group_id >= 10) and (custom_sub_group_id <= 99):
            sub_group_code = "S-00" + str(custom_sub_group_id)
            custom_sub_groups.objects.filter(id=custom_sub_group_id).update(sub_group_code=sub_group_code)

        elif (custom_sub_group_id >= 100) and (custom_sub_group_id <= 999):
            sub_group_code = "S-0" + str(custom_group_id)
            custom_sub_groups.objects.filter(id=custom_sub_group_id).update(sub_group_code=sub_group_code)
        else:
            sub_group_code = "S-" + str(custom_sub_group_id)
            custom_sub_groups.objects.filter(id=custom_sub_group_id).update(sub_group_code=sub_group_code)

        custom_group_name.custom_sub_group_ids.add(custom_sub_group_obj.id)

    custom_group_name = custom_groups.objects.create(group_name='Nationality', is_primary=True)
    custom_group_id = custom_group_name.id

    if (custom_group_id >= 1) and (custom_group_id <= 9):
        grade_code = "G-000" + str(custom_group_id)
        custom_groups.objects.filter(id=custom_group_id).update(group_code=grade_code)
    elif (custom_group_id >= 10) and (custom_group_id <= 99):
        grade_code = "G-00" + str(custom_group_id)
        custom_groups.objects.filter(id=custom_group_id).update(group_code=grade_code)
    elif (custom_group_id >= 100) and (custom_group_id <= 999):
        grade_code = "G-0" + str(custom_group_id)
        custom_groups.objects.filter(id=custom_group_id).update(group_code=grade_code)
    else:
        grade_code = "G-" + str(custom_group_id)
        custom_groups.objects.filter(id=custom_group_id).update(group_code=grade_code)


    nationality_list = ['Emirati', 'Non-Emirati', 'Arab', 'Non-Arab']
    for nationality_rec in nationality_list:
        custom_sub_group_obj = custom_sub_groups.objects.create(sub_group_name=nationality_rec)
        custom_sub_group_id = custom_sub_group_obj.id
        if (custom_sub_group_id >= 1) and (custom_sub_group_id <= 9):
            sub_group_code = "S-000" + str(custom_sub_group_id)
            custom_sub_groups.objects.filter(id=custom_sub_group_id).update(sub_group_code=sub_group_code)

        elif (custom_sub_group_id >= 10) and (custom_sub_group_id <= 99):
            sub_group_code = "S-00" + str(custom_sub_group_id)
            custom_sub_groups.objects.filter(id=custom_sub_group_id).update(sub_group_code=sub_group_code)

        elif (custom_sub_group_id >= 100) and (custom_sub_group_id <= 999):
            sub_group_code = "S-0" + str(custom_group_id)
            custom_sub_groups.objects.filter(id=custom_sub_group_id).update(sub_group_code=sub_group_code)
        else:
            sub_group_code = "S-" + str(custom_sub_group_id)
            custom_sub_groups.objects.filter(id=custom_sub_group_id).update(sub_group_code=sub_group_code)

        custom_group_name.custom_sub_group_ids.add(custom_sub_group_obj.id)

def load_assessment_configration_name():
    assessment_configration_master.objects.create(configration_name='Not Applicable',short_name='NA',select='exclude')

def load_initial_data():
    import_default_role()
    import_default_nationality()
    import_default_religion()
    LoadInitialSchoolData()
    load_absent_category()
    load_present_category()
    load_department()
    load_exam_type()
    load_assessment_configration_name()
    load_tc_type()
    import_month_master()
    load_custom_group()
    load_assessment_configration()
    # default_role_as_admin()



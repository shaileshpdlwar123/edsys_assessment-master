import datetime
import time
from apscheduler.scheduler import Scheduler
from registration.models import academic_year
# Start the scheduler
sched = Scheduler()
sched.daemonic = False
sched.start()

def job_function():
    current_acd_year = academic_year.objects.get(is_active=True, current_academic_year=True)
    all_academicyr_recs = academic_year.objects.all()
    end_date = current_acd_year.end_date
    todayDate = datetime.date.today()
    if end_date == todayDate:
        for academicyr_rec in all_academicyr_recs:
            if academicyr_rec:
                acd_year = academicyr_rec.start_date - current_acd_year.end_date
                if acd_year.days > 0:
                    next_acd_year_obj = academicyr_rec

        academic_year.objects.filter(id=current_acd_year.id).update(current_academic_year=False)
        academic_year.objects.filter(id=next_acd_year_obj.id).update(current_academic_year=True)

# Schedules job_function to be run once each minute
sched.add_cron_job(job_function,  minute='0-59')
from django.conf.urls import include, url
from django.contrib import admin
from . import views
from django.contrib.auth.views import logout
from django.conf import settings
from django.conf.urls.static import static

app_name = 'masters'

urlpatterns = [

                  ########=================================odoo configuration======================================================
                  url(r'^template_odoo_config_list/$', views.TemplateOdooConfigList, name='template_odoo_config_list'),
                  url(r'^add_odoo_config/$', views.AddOdooConfig, name='add_odoo_config'),
                  url(r'^template_update_config/(?P<config_id>[0-9]+)$', views.TemplateUpdateOdooConfig,
                      name='template_update_config'),
                  url(r'^update_odoo_config/$', views.UpdateOdooConfig, name='update_odoo_config'),
                  url(r'^test_connection/$', views.TestOdooConnection, name='test_connection'),
                  url(r'^start_syncing/$', views.StartOdooSync, name='start_syncing'),
                  url(r'^sync_switch/$', views.OdooSyncOnOff, name='sync_switch'),
                  #=================================================================================================================

                  url(r'^month_masters/$', views.MonthMasters, name='month_masters'),

                  url(r'^masters_home/$', views.ManageMasters, name='masters_home'),
                  url(r'^data_access/$', views.DataAccessMasters, name='data_access'),
                  url(r'^religion_masters/$', views.ManageReligionMasters, name='religion_masters'),
                  url(r'^add_religion_masters/$', views.AddReligionMasters, name='add_religion_masters'),
                  url(r'^update_religion/(?P<religion_id>[0-9]+)$', views.UpdateReligion, name='update_religion'),
                  url(r'^save_updated_religion/$', views.SaveUpdatedReligion, name='save_updated_religion'),
                  url(r'^delete_religion/(?P<religion_id>[0-9]+)$', views.DeleteReligion, name='delete_religion'),

                  # =-==-=-=-=-==-=-=-Nationality Urls=-=-=-====-=-=-=-=

                  url(r'^nationality_masters/$', views.ManageNationalityMasters, name='nationality_masters'),
                  url(r'^add_nationality_masters/$', views.AddNationalityMasters, name='add_nationality_masters'),
                  url(r'^update_nationality/(?P<nationality_id>[0-9]+)$', views.UpdateNationality,
                      name='update_nationality'),
                  url(r'^save_updated_nationality/$', views.SaveUpdatedNationality, name='save_updated_nationality'),
                  url(r'^delete_nationality/(?P<nationality_id>[0-9]+)$', views.DeleteNationality,
                      name='delete_nationality'),

                  # =-=-=-=-=-=-=-=-=-=--Department Master Urls=-=-=-=-=-=-=-=-=-=-
                  url(r'^department_master/$', views.ManageDepartment, name='department_master'),
                  url(r'^add_department_masters/$', views.AddDepartmentMasters, name='add_department_masters'),
                  url(r'^update_department/(?P<department_id>[0-9]+)$', views.UpdateDepartment,
                      name='update_department'),
                  url(r'^save_updated_department/$', views.SaveUpdatedDepartment, name='save_updated_department'),
                  url(r'^delete_department/(?P<department_id>[0-9]+)$', views.DeleteDepartment,
                      name='delete_department'),

                  # =-==-=-=-=-==-=-=-Exam Urls=-=-=-====-=-=-=-=

                  url(r'^exam_masters/$', views.ManageExamMasters, name='exam_masters'),
                  url(r'^add_exam_masters/$', views.AddExamMasters, name='add_exam_masters'),
                  url(r'^update_exam/(?P<exam_type_id>[0-9]+)$', views.UpdateExam,
                      name='update_exam'),
                  url(r'^save_updated_exam/$', views.SaveUpdatedExam, name='save_updated_exam'),
                  url(r'^delete_exam/(?P<exam_type_id>[0-9]+)$', views.DeleteExam,
                      name='delete_exam'),

                  # =-==-=-=-=-==-=-=-Rubrics Urls=-=-=-====-=-=-=-=

                  url(r'^rubrics_masters/$', views.RubricsMasters, name='rubrics_masters'),
                  url(r'^add_rubrics_masters/$', views.AddRubricsMasters, name='add_rubrics_masters'),
                  url(r'^update_rubrics/(?P<rubrics_id>[0-9]+)$', views.UpdateRubrics,
                      name='update_rubrics'),
                  url(r'^save_updated_rubrics/$', views.SaveUpdatedRubrics, name='save_updated_rubrics'),
                  url(r'^delete_rubrics/(?P<rubrics_id>[0-9]+)$', views.DeleteRubrics,
                      name='delete_rubrics'),

                  url(r'^rubrics_category/$', views.RubricsCategory, name='rubrics_category'),
                  url(r'^add_rubrics_category/$', views.AddRubricsCategory, name='add_rubrics_category'),
                  url(r'^update_rubrics_catgory/(?P<rubrics_category_id>[0-9]+)$', views.UpdateRubricsCategory,
                      name='update_rubrics_catgory'),
                  url(r'^save_updated_rubrics_category/$', views.SaveUpdatedRubricsCategory,
                      name='save_updated_rubrics_category'),
                  url(r'^delete_rubrics_category/(?P<rubrics_category_id>[0-9]+)$', views.DeleteRubricsCategory,
                      name='delete_rubrics_category'),

                  url(r'^nationality_group/$', views.NationalityGroup, name='nationality_group'),
                  url(r'^add_nationality_group/$', views.AddNationalityGroup, name='add_nationality_group'),

                  url(r'^update_nationality_group/(?P<nationality_group_id>[0-9]+)$', views.UpdateNationalityGroup,
                      name='update_nationality_group'),
                  url(r'^save_updated_nationality_group/$', views.SaveUpdatedNationalityGroup,
                      name='save_updated_nationality_group'),

                  url(r'^delete_nationality_group/(?P<nationality_group_id>[0-9]+)$', views.DeleteNationalityGroup,
                      name='delete_nationality_group'),

                  # =-==-=-=-=-==-=-=-Home Urls=-=-=-====-=-=-=-=
                  url(r'^house_master_list/$', views.HouseMasterList, name='house_master_list'),
                  url(r'^add_house/$', views.AddHouse, name='add_house'),
                  url(r'^update_hose/(?P<house_id>[0-9]+)$', views.UpdateHose,
                      name='update_hose'),
                  url(r'^save_updated_house/$', views.SaveUpdatedHouse, name='save_updated_house'),
                  url(r'^delete_house/(?P<house_id>[0-9]+)$', views.DeleteHouse,
                      name='delete_house'),

                  url(r'^nationality_group_mapping/$', views.NationalityGroupMapping, name='nationality_group_mapping'),

                  url(r'^group_nationality_mapping/$', views.GroupNationalityMapping, name='group_nationality_mapping'),
                  url(r'^save_group_nationality_mapping/$', views.SaveGroupNationalityMapping,
                      name='save_group_nationality_mapping'),
                  url(r'^delete_group_nationality_mapping/$', views.DeleteGroupNationalityMapping,
                      name='delete_group_nationality_mapping'),

                  url(r'^tc_type_master/$', views.TCTYPEMASTER, name='tc_type_master'),

                  # =-==-=-=-=-==-=-=-Assessment Configration Urls=-=-=-====-=-=-=-=

                  url(r'^assessment_configration/$', views.AssessmentConfigration, name='assessment_configration'),
                  url(r'^add_assessment_configration/$', views.AddAssessmentConfigration,
                      name='add_assessment_configration'),
                  url(r'^update_assessment_configration/(?P<ass_config_id>[0-9]+)$', views.UpdateAssessmentConfigration,
                      name='update_assessment_configration'),

                  url(r'^save_updated_assessment_configraton/$', views.SaveUpdatedAssessmentConfigraton,
                      name='save_updated_assessment_configraton'),
                  url(r'^delete_assessment_configration/(?P<ass_config_id>[0-9]+)$', views.DeleteAssessmentConfigration,
                      name='delete_assessment_configration'),

                  url(r'^assessment_configrtion_roundoff/$', views.AssessmentConfigrtionRoundOff,
                      name='assessment_configrtion_roundoff'),

                  url(r'^assessment_round_off/$', views.AssessmentRoundOff, name='assessment_round_off'),
                  url(r'^add_round_off/$', views.AddRoundOff, name='add_round_off'),
                  # url(r'^round_off_list/$', views.RoundOffList, name='round_off_list'),
                  url(r'^update_round_off/', views.UpdateRoundOff,
                      name='update_round_off'),
                  url(r'^save_updated_round_off/$', views.SaveUpdatedRoundOff, name='save_updated_round_off'),
                  url(r'^delete_round_off/(?P<round_off_id>[0-9]+)$', views.DeleteRoundOff,
                      name='delete_round_off'),
                  url(r'^active_round/$', views.ActiveRound, name='active_round'),
                  url(r'^inactive_round/$', views.InActiveRound, name='inactive_round'),

                  # ============== Section Type ============================

                  url(r'^section_type_master/$', views.SectionTypeMaster, name='section_type_master'),
                  url(r'^add_section_type/$', views.AddSectionType, name='add_section_type'),
                  url(r'^save_section_type/$', views.SaveSectionType, name='save_section_type'),
                  url(r'^update_section_type/(?P<type_id>[0-9]+)$', views.UpdateSectionType,name='update_section_type'),
                  url(r'^save_updated_section_type/$', views.SaveUpdateSectionType, name='save_updated_section_type'),
                  url(r'^delete_section_type/(?P<type_id>[0-9]+)$', views.DeleteSectionType, name='delete_section_type'),

                  # ============== report_card_term_master ============================
                  url(r'^report_card_term_master/$', views.ReportCardTermMaster, name='report_card_term_master'),
                  # url(r'^report_card_conf/$', views.ReportCardConf, name='report_card_conf'),
                  url(r'^add_report_card_term_name/$', views.AddReportCardTermName, name='add_report_card_term_name'),
                  url(r'^save_report_name/$', views.SaveReportName, name='save_report_name'),
                  url(r'^update_report_term_name/(?P<type_id>[0-9]+)$', views.UpdateReportTermName,name='update_report_term_name'),
                  url(r'^save_updated_section_type/$', views.SaveUpdateSectionType, name='save_updated_section_type'),
                  url(r'^delete_report_name/(?P<type_id>[0-9]+)$', views.DeleteReportName,
                      name='delete_report_name'),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

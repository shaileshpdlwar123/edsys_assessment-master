from django import forms
from registration.models import *
from masters.models import *




# class AttendanceClassSectionForm(forms.ModelForm):
#     pass
 # ClassDetail = ClassDetails.objects.all()
 # Section = Sections.objects.all()
 # class_name = forms.ModelChoiceField(queryset = ClassDetail ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
 # section_name = forms.ModelChoiceField(queryset = Section ,widget=forms.Select(attrs={'class':'form-control' , 'autocomplete': 'off'}))
 
 
class ReligionForm(forms.ModelForm):
    religion_name = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','maxlength':'80'}))
    class Meta:
        model =religion
        fields = ['religion_name']

class NationalityForm(forms.ModelForm):
    # nationality_group_name_recs = nationality_group.objects.all()

    # nationality_group_name = forms.ModelChoiceField(required=True, queryset=nationality_group_name_recs,
    #                                                widget=forms.Select(
    #                                                    attrs={'class': 'form-control', 'autocomplete': 'off'}))

    nationality_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off','maxlength':'80'}))

    class Meta:
        model = nationality
        fields = ['nationality_name']

class DepartmentForm(forms.ModelForm):
    depatment_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off','maxlength':'80'}))

    class Meta:
        model = department_details
        fields = ['depatment_name']

class ExamForm(forms.ModelForm):
    exam_type_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off'}))

    class Meta:
        model = exam_type
        fields = ['exam_type_name']

class RubricsForm(forms.ModelForm):

    rubrics_category_recs = rubrics_category.objects.all()

    rubrics_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control emp', 'autocomplete': 'off','pattern':'^[\w](?!\d*[-_]+\d*$)[ \w-]+$', 'title':'Only Alphanumeric is allowed with "-" and "_"'}))
    rubrics_mark = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control emp', 'autocomplete': 'off','pattern':'[0-9 ]+', 'title':'Enter Numbers Only '}))

    rubrics_category_name = forms.ModelChoiceField(required=True, queryset=rubrics_category_recs,
                                       widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))

    class Meta:
        model = rubrics_master
        fields = ['rubrics_name','rubrics_mark','rubrics_category_name']


class RubricsCategoryForm(forms.ModelForm):
    rubric_catgory = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off','pattern':'^[\w](?!\d*[-_]+\d*$)[ \w-]+$', 'title':'Only Alphanumeric is allowed with "-" and "_"'}))

    class Meta:
        model = rubrics_category
        fields = ['rubric_catgory']


class NationalityGroupForm1(forms.ModelForm):
    nationatity_group_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off','pattern':'[A-Za-z -]+', 'title':'Enter Characters Only','maxlength': '80'}))

    class Meta:
        model = nationality_group
        fields = ['nationatity_group_name']

class HouseForm(forms.ModelForm):
    house_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off','pattern':'[A-Za-z -]+','title':'Enter Characters Only','maxlength': '32'}))

    class Meta:
        model = house_master
        fields = ['house_name']

class NationalityGroupForm(forms.ModelForm):
    Nationality = nationality.objects.all()
    NationalityGroup = nationality_group.objects.all()
    nationality_group_name = forms.ModelChoiceField(required=True, queryset=NationalityGroup,
                                       widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))

    nationality_mapping_name = forms.ModelMultipleChoiceField(required=False, queryset=Nationality,
                                                         widget=forms.SelectMultiple(
                                                             attrs={'class': 'form-control', 'autocomplete': 'off'}))
    class Meta:
        model = nationality_group_mapping
        fields = ['nationality_group_name','nationality_mapping_name']

from django.contrib.admin.widgets import FilteredSelectMultiple

class GroupNationalityForm(forms.ModelForm):
    Nationality = nationality.objects.all()
    NationalityGroup = nationality_group.objects.all()
    # crnt_acd = forms.DateField(required=False, queryset=academic_year.objects.get(current_academic_year=1))
    nationality_group_name = forms.ModelChoiceField(required=True, queryset=NationalityGroup,widget=forms.Select( attrs={'class': 'form-control', 'autocomplete': 'off'}))
    nationality_mapping_name = forms.ModelMultipleChoiceField(required=True, queryset=Nationality, widget=FilteredSelectMultiple("NationalityDetails", is_stacked=False))

    class Meta:
        model = nationality_group_mapping
        fields = ['nationality_group_name', 'nationality_mapping_name']


class AssessmentConfigrationForm(forms.ModelForm):
    configration_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off'}))
    short_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off'}))

    select = forms.TypedChoiceField(

        choices=(('zero', 'Zero'), ('exclude', 'Exclude')),
        widget=forms.RadioSelect
    )

    class Meta:
        model = assessment_configration_master
        fields = ['configration_name','short_name','select']

# class RoundOffForm(forms.ModelForm):
#
#     select = forms.TypedChoiceField(
#
#         choices=(('up', 'Round Up'), ('down', 'Round Down')),
#         widget=forms.RadioSelect
#     )
#
#     class Meta:
#         model = assessment_round_master
#         fields = ['select']
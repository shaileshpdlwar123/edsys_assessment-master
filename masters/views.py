from django.shortcuts import render, redirect
from .forms import ReligionForm, NationalityForm, DepartmentForm,ExamForm,RubricsForm,RubricsCategoryForm,NationalityGroupForm,HouseForm,GroupNationalityForm,NationalityGroupForm1,AssessmentConfigrationForm
from registration.models import religion, nationality, department_details, month
from django.contrib import messages
from registration.decorators import admin_login_required
from registration.decorators import user_login_required, user_has_permission, the_decorator, decorator, user_permission_required, superuser_login_required
from django.contrib.auth.models import Permission
from django.contrib.auth.models import Group
from models import *
from assessment.models import *
from .utils import compute_current_year
import os
import base64

# Create your views here.
@user_login_required
def ManageMasters(request):
    return render(request, "masters_home.html")

def MonthMasters(request):
    month_recs = month.objects.all()
    return render(request, "month_master.html",{'month_recs':month_recs})

@user_login_required
def DataAccessMasters(request):
    role_list = Group.objects.all()
    permissions = Permission.objects.all()

    return render(request, "data_access_rights.html", {'role_list': role_list, 'permissions': permissions})

@user_login_required
def ManageReligionMasters(request):
    relig = religion.objects.all()
    return render(request, "religion_master.html", {'religion_list': relig})

@user_login_required
def AddReligionMasters(request):

    if request.method == 'GET':
        forms = ReligionForm()
        return render(request, "add_religion_master.html", {'form': forms})

    if request.method == 'POST':
        Religion = ReligionForm(request.POST)
        acd_year_id = request.POST.getlist('religion_name')
        len(acd_year_id)
        print "dd"
        if Religion.is_valid():
            if religion.objects.filter(religion_name=request.POST['religion_name']).exists():
                messages.success(request, "Duplicate record can't be added.")
            else:
                Religion.save()

        return redirect('/school/religion_masters/')

@user_login_required
def UpdateReligion(request, religion_id):
    relig = religion.objects.get(pk=religion_id)
    return render(request, "update_religion.html", {'religion': relig})

@user_login_required
def SaveUpdatedReligion(request):
    #     print 'update'

    if (request.method == 'POST'):
        #         print "with in post"
        Religion = ReligionForm(request.POST)
        print ("errors",Religion.errors)
        if Religion.is_valid():
            if religion.objects.filter(religion_name=request.POST['religion_name']).exists():
                messages.success(request, "Duplicate record can't be added.")
            #         print ("form is valid")
            else:
                religion_id = request.POST['religion_id']
                obj = religion.objects.get(pk=religion_id)
                obj.religion_name = request.POST['religion_name']
                obj.save()
                messages.success(request, "One record updated successfully")

    return redirect('/school/religion_masters/')


@user_login_required
def DeleteReligion(request, religion_id):
    try:
        religion.objects.filter(id=religion_id).delete()
        messages.success(request, "record deleted successfully")
    except:
        messages.success(request, "Cant delete religion relation exists somewhere.")
    return redirect('/school/religion_masters/')

#=-==--=-=-=-=-=--=Nationality Master=-=-=-=-=-=-==-=-=-=-=-=-==-=--=-=-=

@user_login_required
def ManageNationalityMasters(request):
    national = nationality.objects.all()
    return render(request, "nationality_master.html", {'nationality_list': national})

@user_login_required
def AddNationalityMasters(request):

    if request.method == 'GET':
        forms = NationalityForm()
        return render(request, "add_nationality_master.html", {'form': forms})

    if request.method == 'POST':
        Nationality = NationalityForm(request.POST)
        if Nationality.is_valid():
            if nationality.objects.filter(nationality_name=request.POST['nationality_name']).exists():
                messages.success(request, "Duplicate record can't be added.")
            else:
                nationality_obj = nationality()
                # nationality_obj.nationatity_group_name = nationality_group.objects.get(id=request.POST.get('nationality_group_name'))
                nationality_obj.nationality_name = request.POST.get('nationality_name')
                nationality_obj.save()
        return redirect('/school/nationality_masters/')


@user_login_required
def UpdateNationality(request, nationality_id):
    national = nationality.objects.get(pk=nationality_id)
    nationality_group_recs = nationality_group.objects.all()
    return render(request, "update_nationality.html", {'nationality': national,'nationality_group_recs':nationality_group_recs})

@user_login_required
def SaveUpdatedNationality(request):
    #     print 'update'

    if (request.method == 'POST'):
        #         print "with in post"
        Nationality = NationalityForm(request.POST)
        nationality_id = request.POST['nationality_id']
        # print ("errors",Religion.errors)
        if Nationality.is_valid():
            if nationality.objects.filter(~Q(id=nationality_id),nationality_name=request.POST['nationality_name']).exists():
                messages.success(request, "Duplicate record can't be added.")
            else:
                #         print ("form is valid")
                nationality_id = request.POST['nationality_id']
                obj = nationality.objects.get(pk=nationality_id)
                obj.nationality_name = request.POST['nationality_name']
                # nationality_group_id = request.POST['nationality_group_name']
                # nationality_group_instance = nationality_group.objects.get(id=nationality_group_id)
                # obj.nationatity_group_name = nationality_group_instance
                obj.save()
                messages.success(request, "One record updated successfully")

    return redirect('/school/nationality_masters/')


@user_login_required
def DeleteNationality(request, nationality_id):
    try:
        nationality.objects.filter(id = nationality_id).delete()
        messages.success(request, "record deleted successfully")
    except:
        messages.success(request, "Cant delete nationality relation exists somewhere.")
    return redirect('/school/nationality_masters/')

#=-=-=-=-=-=-=-=-=-Department Master=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

@user_login_required
def ManageDepartment(request):
    department = department_details.objects.all()
    return render(request, "department_masters.html", {'department_list': department})

@user_login_required
def AddDepartmentMasters(request):

    if request.method == 'GET':
        forms = DepartmentForm()
        return render(request, "add_department_master.html", {'form': forms})

    if request.method == 'POST':
        Department = DepartmentForm(request.POST)
        if Department.is_valid():
            if department_details.objects.filter(depatment_name=request.POST['depatment_name']).exists():
                messages.success(request, "Duplicate record can't be added.")
            else:
                Department.save()

        return redirect('/school/department_master/')

@user_login_required
def UpdateDepartment(request, department_id):
    dept = department_details.objects.get(pk=department_id)
    return render(request, "update_department.html", {'department': dept})

@user_login_required
def SaveUpdatedDepartment(request):
    #     print 'update'

    if (request.method == 'POST'):
        #         print "with in post"
        Department = DepartmentForm(request.POST)
        # print ("errors",Religion.errors)
        if Department.is_valid():
            if department_details.objects.filter(depatment_name=request.POST['depatment_name']).exists():
                messages.success(request, "Duplicate record can't be added.")
            else:
                #         print ("form is valid")
                department_id = request.POST['department_id']
                obj = department_details.objects.get(pk=department_id)
                obj.depatment_name = request.POST['depatment_name']
                obj.save()
                messages.success(request, "One record updated successfully")

    return redirect('/school/department_master/')

@user_login_required
def DeleteDepartment(request, department_id):
    try:
        department_details.objects.filter(id=department_id).delete()
        messages.success(request, "record deleted successfully")
    except:
        messages.success(request, "Cant delete department relation exists somewhere.")
    return redirect('/school/department_master/')

#=-==--=-=-=-=-=--=Exam Master=-=-=-=-=-=-==-=-=-=-=-=-==-=--=-=-=
@user_login_required
def ManageExamMasters(request):
    exam_type_recs = exam_type.objects.all()
    return render(request, "exam_master.html", {'exam_type_recs': exam_type_recs})

@user_login_required
def AddExamMasters(request):

    if request.method == 'GET':
        forms = ExamForm()
        return render(request, "add_exam_master.html", {'form': forms})

    if request.method == 'POST':
        ExamTypeForm = ExamForm(request.POST)
        if ExamTypeForm.is_valid():
            if exam_type.objects.filter(exam_type_name=request.POST['exam_type_name']).exists():
                messages.success(request, "Duplicate record can't be added.")
            else:
                ExamTypeForm.save()
        return redirect('/school/exam_masters/')


@user_login_required
def UpdateExam(request, exam_type_id):
    exam_type_rec = exam_type.objects.get(pk=exam_type_id)
    return render(request, "update_exam_type.html", {'exam_type_rec': exam_type_rec})

@user_login_required
def SaveUpdatedExam(request):
    if (request.method == 'POST'):
        ExamTypeForm = ExamForm(request.POST)
        if ExamTypeForm.is_valid():
            exam_type_id = request.POST['exam_type_id']
            exam_type_rec = exam_type.objects.get(id=exam_type_id)
            if exam_scheme_exam_name_mapping.objects.filter(exam_name__exam_type__exam_type_name=exam_type_rec).exists():
                messages.success(request, "Exam Type already used")
                return redirect('/school/exam_masters/')
            if exam_type.objects.filter(exam_type_name=request.POST['exam_type_name']).exists():
                messages.success(request, "Duplicate record can't be added.")
            else:
                exam_type_id = request.POST['exam_type_id']
                obj = exam_type.objects.get(pk=exam_type_id)
                obj.exam_type_name = request.POST['exam_type_name']
                obj.save()
                messages.success(request, "One record updated successfully")

    return redirect('/school/exam_masters/')


@user_login_required
def DeleteExam(request, exam_type_id):
    try:
        exam_type.objects.filter(id=exam_type_id).delete()
        messages.success(request, "One record deleted successfully")
    except:
        messages.success(request, "Cant delete exam type relation exists somewhere.")
    return redirect('/school/exam_masters/')


#=-==--=-=-=-=-=--=Rubrics Master=-=-=-=-=-=-==-=-=-=-=-=-==-=--=-=-=
@user_login_required
@user_permission_required('assessment.can_view_view_rubric_score_list', '/school/home/')
def RubricsMasters(request):
    rubrics_recs = rubrics_master.objects.all()
    return render(request, "rubrics_list.html", {'rubrics_recs': rubrics_recs})


@user_login_required
def AddRubricsMasters(request):
    if request.method == 'GET':
        forms = RubricsForm()
        return render(request, "add_rubrics_master.html", {'form': forms})

    if request.method == 'POST':
        rubrics_category = request.POST.get('rubrics_category_name')
        rubrics_name = request.POST.getlist('rubrics_name')
        rubrics_mark = request.POST.getlist('rubrics_mark')
        exist_flag = False
        exist_flag_name=False
        exist_flag_score=False
        list_size = len(rubrics_mark)

        for count in range(list_size):
            if rubrics_master.objects.filter(rubrics_category_name_id=rubrics_category,
                                             rubrics_name=rubrics_name[count],rubrics_mark=rubrics_mark[count]).exists():
                exist_flag = True
                continue
            elif rubrics_master.objects.filter(rubrics_category_name_id=rubrics_category,
                                             rubrics_name=rubrics_name[count]).exists():
                exist_flag_name = True
                continue
            elif rubrics_master.objects.filter(rubrics_category_name_id=rubrics_category,
                                               rubrics_mark=rubrics_mark[count]).exists():
                exist_flag_score = True
                continue
            else:
                rubrics_master.objects.create(rubrics_category_name_id=rubrics_category,
                                                                   rubrics_name=rubrics_name[count],
                                                                   rubrics_mark=rubrics_mark[count])

        if exist_flag:
            messages.warning(request, "Rubric Score and Rubric Score Name Already Exists!")
        elif exist_flag_name:
            messages.warning(request, "Rubric Score Name Already Exists!")
        elif exist_flag_score:
            messages.warning(request, "Rubric Score Already Exists!")
        else:
            messages.success(request, "Record Added Successfully!")
        return redirect('/school/rubrics_masters/')

@user_login_required
def UpdateRubrics(request, rubrics_id):
    rubrics_rec = rubrics_master.objects.get(pk=rubrics_id)
    rubrics_category_list = rubrics_category.objects.all()

    return render(request, "update_rubrics.html", {'rubrics_rec': rubrics_rec,'rubrics_category_list':rubrics_category_list})

from django.db.models import Q
@user_login_required
def SaveUpdatedRubrics(request):
    if (request.method == 'POST'):
        rubrics_id = request.POST['rubrics_id']
        rubrics_category_id = request.POST['rubrics_category']
        rubrics_category_instance = rubrics_category.objects.get(id=rubrics_category_id)

        if rubrics_master.objects.filter(~Q(id=rubrics_id),rubrics_name=request.POST['rubrics_name'],rubrics_category_name=rubrics_category_id).exists():
            messages.warning(request, "Rubric Score Name Already Exist")

        elif rubrics_master.objects.filter(~Q(id=rubrics_id), rubrics_mark=request.POST['rubrics_mark'],rubrics_category_name=rubrics_category_id).exists():
            messages.warning(request, "Rubric Score  Already Exist")
        else:
            obj = rubrics_master.objects.get(pk=rubrics_id)
            obj.rubrics_name = request.POST['rubrics_name']
            obj.rubrics_mark = request.POST['rubrics_mark']
            obj.rubrics_category_name = rubrics_category_instance
            obj.save()
            messages.success(request, "One record updated successfully")

    return redirect('/school/rubrics_masters/')


@user_login_required
def DeleteRubrics(request, rubrics_id):
    # rubrics_master.objects.filter(id=rubrics_id).delete()
    # return redirect('/school/rubrics_masters/')

    try:
        rubrics_master.objects.filter(id=rubrics_id).delete()
        messages.success(request, "record deleted successfully")
    except:
        messages.success(request, "Cant delete rubrics category relation exists somewhere.")
    return redirect('/school/rubrics_masters/')




#=-==--=-=-=-=-=--=Rubrics Category=-=-=-=-=-=-==-=-=-=-=-=-==-=--=-=-=
@user_login_required
@user_permission_required('assessment.can_view_view_rubric_category_list', '/school/home/')
def RubricsCategory(request):
    rubrics_category_recs = rubrics_category.objects.all()
    return render(request, "rubric_catgory_list.html", {'rubrics_category_recs': rubrics_category_recs})


@user_login_required
def AddRubricsCategory(request):

    if request.method == 'GET':
        forms = RubricsCategoryForm()
        return render(request, "rubrics_add_category.html", {'form': forms})

    if request.method == 'POST':
        FormRubrics = RubricsCategoryForm(request.POST)
        if FormRubrics.is_valid():
            if rubrics_category.objects.filter(rubric_catgory=request.POST['rubric_catgory']).exists():
                messages.success(request, "Rubric Category Name Already Exist")

            else:
                FormRubrics.save()
                rubrics_category_name=rubrics_category.objects.get(rubric_catgory=request.POST['rubric_catgory'])
                rubrics_master.objects.create(rubrics_name='NA',rubrics_mark=0,rubrics_category_name=rubrics_category_name)
        return redirect('/school/rubrics_category/')

@user_login_required
def UpdateRubricsCategory(request, rubrics_category_id):
    rubrics_rec = rubrics_category.objects.get(pk=rubrics_category_id)
    return render(request, "rubrics_update_category.html", {'rubrics_rec': rubrics_rec})


from django.db.models import Q
@user_login_required
def SaveUpdatedRubricsCategory(request):
    if (request.method == 'POST'):
        rubrics_category_id = request.POST['rubrics_category_id']
        if rubrics_category.objects.filter(~Q(id=rubrics_category_id),rubric_catgory=request.POST['rubrics_category_name']).exists():
            messages.success(request, "Rubric Category Name Already Exist")
        else:
            obj = rubrics_category.objects.get(pk=rubrics_category_id)
            obj.rubric_catgory = request.POST['rubrics_category_name']
            obj.save()
            messages.success(request, "One record updated successfully")

    return redirect('/school/rubrics_category/')

@user_login_required
def DeleteRubricsCategory(request, rubrics_category_id):
    try:
        rubric_count = rubrics_master.objects.filter(rubrics_category_name_id=rubrics_category_id).count()
        if rubric_count==1:
            rubrics_master.objects.filter(rubrics_category_name_id=rubrics_category_id,rubrics_name='NA').delete()
            rubrics_category.objects.filter(id=rubrics_category_id).delete()
            messages.success(request, "record deleted successfully")
            return redirect('/school/rubrics_category/')
        else:
            messages.success(request, "Cant delete rubrics category relation exists somewhere.")
            return redirect('/school/rubrics_category/')
    except:
        messages.success(request, "Cant delete rubrics category relation exists somewhere.")
    return redirect('/school/rubrics_category/')


@user_login_required
def NationalityGroup(request):
    nationality_group_recs = nationality_group.objects.all()
    return render(request, "nationality_group_list.html", {'nationality_group_recs': nationality_group_recs})



@user_login_required
def AddNationalityGroup(request):

    if request.method == 'GET':
        forms = NationalityGroupForm1()
        return render(request, "add_nationality_group.html", {'form': forms})

    if request.method == 'POST':
        FormNationality = NationalityGroupForm1(request.POST)
        if FormNationality.is_valid():
            if nationality_group.objects.filter(nationatity_group_name=request.POST['nationatity_group_name']).exists():
                messages.success(request, "Nationality Group Name Already Exist")

            else:
                FormNationality.save()
        return redirect('/school/nationality_group/')

@user_login_required
def UpdateNationalityGroup(request, nationality_group_id):
    nationality_group_rec = nationality_group.objects.get(pk=nationality_group_id)
    return render(request, "update_nationality_group.html", {'nationality_group_rec': nationality_group_rec})

from django.db.models import Q
@user_login_required
def SaveUpdatedNationalityGroup(request):
    if (request.method == 'POST'):
        nationality_group_id = request.POST['nationality_group_id']
        if nationality_group.objects.filter(~Q(id=nationality_group_id),nationatity_group_name=request.POST['nationality_group_name']).exists():
            messages.success(request, "Nationality Group Name Already Exist")
        else:
            obj = nationality_group.objects.get(pk=nationality_group_id)
            obj.nationatity_group_name = request.POST['nationality_group_name']
            obj.save()
            messages.success(request, "One record updated successfully")

    return redirect('/school/nationality_group/')

@user_login_required
def DeleteNationalityGroup(request, nationality_group_id):
    # rubrics_category.objects.filter(id=rubrics_category_id).delete()
    # return redirect('/school/rubrics_category/')
    try:
        nationality_group.objects.filter(id=nationality_group_id).delete()
        messages.success(request, "record deleted successfully")
    except:
        messages.success(request, "Cant delete nationality group relation exists somewhere.")
    return redirect('/school/nationality_group/')

#=-==--=-=-=-=-=--=Home Master=-=-=-=-=-=-==-=-=-=-=-=-==-=--=-=-=
@user_login_required
def HouseMasterList(request):
    house_recs = house_master.objects.all()
    return render(request, "house_master_list.html", {'house_recs': house_recs})


@user_login_required
def AddHouse(request):

    if request.method == 'GET':
        forms = HouseForm()
        return render(request, "add_house.html", {'form': forms})

    if request.method == 'POST':
        HouseFormRec = HouseForm(request.POST)
        if HouseFormRec.is_valid():
            if house_master.objects.filter(house_name=request.POST['house_name']).exists():
                messages.success(request, "Duplicate record can't be added.")
            else:
                HouseFormRec.save()
        return redirect('/school/house_master_list/')

@user_login_required
def UpdateHose(request, house_id):
    house_rec = house_master.objects.get(pk=house_id)
    return render(request, "update_house.html", {'house_rec': house_rec})

@user_login_required
def SaveUpdatedHouse(request):
    if (request.method == 'POST'):
        HouseTypeForm = HouseForm(request.POST)
        if HouseTypeForm.is_valid():
            if house_master.objects.filter(house_name=request.POST['house_name']).exists():
                messages.success(request, "Duplicate record can't be added.")
            else:
                house_instance = request.POST['house_id']
                obj = house_master.objects.get(pk=house_instance)
                obj.house_name = request.POST['house_name']
                obj.save()

    return redirect('/school/house_master_list/')

@user_login_required
def DeleteHouse(request, house_id):
    # rubrics_category.objects.filter(id=rubrics_category_id).delete()
    # return redirect('/school/rubrics_category/')
    try:
        house_master.objects.filter(id=house_id).delete()
        messages.success(request, "record deleted successfully")
    except:
        messages.success(request, "Cant delete house relation exists somewhere.")
    return redirect('/school/house_master_list/')


@user_login_required
def NationalityGroupMapping(request):

    nationality_group_form = NationalityGroupForm()
    return render(request, 'nationality_group_mapping.html',
                  {'nationality_group_form': nationality_group_form})




    # all_academicyr_recs = academic_year.objects.all()
    #
    # religion_list = religion.objects.all()
    # nationality_list = nationality.objects.all()
    #
    # section_list = sections.objects.all()
    # class_list = class_details.objects.all()
    #
    # student_recs = student_details.objects.filter(is_active=True, joining_date__lte=todays_date())
    #
    # year_class_section_object = academic_class_section_mapping.objects.all()
    # if year_class_section_object:
    #     try:
    #         current_academic_year = academic_year.objects.get(current_academic_year=1)
    #         selected_year_id = current_academic_year.id
    #         mapped_list = []
    #         for student_rec in student_recs:
    #             for subject_rec in student_rec.subject_ids.all():
    #                 if subject_rec:
    #
    #                     mapped_data = {
    #                         'subject_name': subject_rec.subject_name,
    #                         'subject_id': subject_rec.id,
    #
    #                         'student_first_name': student_rec.first_name,
    #                         'student_last_name': student_rec.last_name,
    #                         'student_id': student_rec.id,
    #
    #                         'class_name': student_rec.academic_class_section.class_name.class_name,
    #                         'section_name': student_rec.academic_class_section.section_name.section_name ,
    #                         'academic_year': student_rec.academic_class_section.year_name.year_name,
    #
    #                         'religion': student_rec.religion,
    #                         'nationality': student_rec.nationality,
    #                     }
    #                     mapped_list.append(mapped_data)
    #         return render(request,'optional_subject_mapping.html', {'nationality_list':nationality_list, 'religion_list':religion_list,'StudentOptionalSubjectMappingForm': student_details_form, "StudentDetails": mapped_list, "year_list": all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})
    #     except academic_year.DoesNotExist:
    #         mapped_list = []
    #         for student_rec in student_recs:
    #             for subject_rec in student_rec.subject_ids.all():
    #                 if subject_rec:
    #                     mapped_data = {
    #                         'subject_name': subject_rec.subject_name,
    #                         'subject_id': subject_rec.id,
    #
    #                         'student_first_name': student_rec.first_name,
    #                         'student_last_name': student_rec.last_name,
    #                         'student_id': student_rec.id,
    #
    #                         'class_name': student_rec.academic_class_section.class_name.class_name,
    #                         'section_name': student_rec.academic_class_section.section_name.section_name,
    #                         'academic_year': student_rec.academic_class_section.year_name.year_name,
    #
    #                         'religion': student_rec.religion,
    #                         'nationality': student_rec.nationality,
    #                     }
    #                     mapped_list.append(mapped_data)
    #
    #         return render(request, 'optional_subject_mapping.html',
    #                       {'class_list':class_list,'section_list':section_list,'nationality_list':nationality_list, 'religion_list':religion_list, 'StudentOptionalSubjectMappingForm': student_details_form, "StudentDetails": mapped_list,
    #                        "year_list": all_academicyr_recs})
    # else:
    #     messages.warning(request, "Academic Year Class Section Mapping Not Found.")
    #     return render(request, 'academic_school.html')

@user_login_required
def GroupNationalityMapping(request):
    try:
        group_nationality_form = GroupNationalityForm()
        nationality_group_mapping_recs = nationality_group_mapping.objects.all()
        return render(request, 'grup_nationality_mapping.html',
                      {'group_nationality_form': group_nationality_form,'nationality_group_mapping_recs':nationality_group_mapping_recs})
    except:
        messages.success(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
        return render(request,'academic_school.html')


@user_login_required
def SaveGroupNationalityMapping(request):
    try:
        nationality_group_name_ids = request.POST.get('nationality_group_name')
        nationality_mapping_name_ids = request.POST.getlist('nationality_mapping_name')



        for nationality_obj in nationality_mapping_name_ids:
            rec = nationality_group_mapping.objects.filter(nationality_group_name_id = nationality_group_name_ids, nationality_mapping_name_id = nationality_obj)
            if rec:
                pass
            else:
                form = nationality_group_mapping(nationality_group_name_id = nationality_group_name_ids, nationality_mapping_name_id = nationality_obj)
                form.save()
    except:
        messages.success(request, "Record not Saved. Please Try Again ")
    return redirect('/school/group_nationality_mapping/')


@user_login_required
def DeleteGroupNationalityMapping(request):
    if request.method == 'POST':
        try:
            check=request.POST.getlist('check')
            for rec in check:
                object = rec.split(',')
                nationality_group_mapping_id = object[0]
                nationality_name_id= object[1]
                nationality_group_mapping.objects.filter(nationality_group_name_id = nationality_group_mapping_id, nationality_mapping_name_id = nationality_name_id).delete()
        except:
            messages.success(request, "Record Not Deleted.. This Nationality Group and Nationality Is Used In Other Tables, Delete Them First And Try Again. ")
    return redirect('/school/group_nationality_mapping/')


#=-==--=-=-=-=-=--=TC Type Master=-=-=-=-=-=-==-=-=-=-=-=-==-=--=-=-=
@user_login_required
def TCTYPEMASTER(request):
    exam_type_recs = tc_type.objects.all()
    return render(request, "tc_type_master.html", {'exam_type_recs': exam_type_recs})

#=-==--=-=-=-=-=--=Assessment Configration=-=-=-=-=-=-==-=-=-=-=-=-==-=--=-=-=
@user_login_required
def AssessmentConfigration(request):
    assessment_configration_master_recs = assessment_configration_master.objects.all()
    return render(request, "assessment_configration.html", {'assessment_configration_master_recs': assessment_configration_master_recs})


@user_login_required
def AddAssessmentConfigration(request):

    if request.method == 'GET':
        forms = AssessmentConfigrationForm()
        return render(request, "add_assessment_configration.html", {'form': forms})

    if request.method == 'POST':
        AssessmentForm = AssessmentConfigrationForm(request.POST)
        if AssessmentForm.is_valid():
            if assessment_configration_master.objects.filter(configration_name=request.POST['configration_name']).exists():
                messages.success(request, "Duplicate record can't be added.")
            else:
                AssessmentForm.save()
        return redirect('/school/assessment_configration/')


@user_login_required
def UpdateAssessmentConfigration(request, ass_config_id):
    assessment_configration_rec = assessment_configration_master.objects.get(pk=ass_config_id)

    if (assessment_configration_rec.select == 'zero'):
        zero = 'checked'
        exclude = ""
    else:
        exclude = "checked"
        zero = ""
    return render(request, "update_assessment_configration.html", {'assessment_configration_rec': assessment_configration_rec,'zero':zero,'exclude':exclude})

@user_login_required
def SaveUpdatedAssessmentConfigraton(request):
    if (request.method == 'POST'):
        AssessmentForm = AssessmentConfigrationForm(request.POST)
        if AssessmentForm.is_valid():
            ass_config_id = request.POST['ass_config_id']
            if assessment_configration_master.objects.filter(~Q(id=ass_config_id),configration_name=request.POST['configration_name']).exists():
                messages.success(request, "Duplicate record can't be added.")
            else:

                obj = assessment_configration_master.objects.get(pk=ass_config_id)
                obj.configration_name = request.POST['configration_name']
                obj.short_name = request.POST['short_name']
                obj.select = request.POST['select']
                obj.save()
                messages.success(request, "One record updated successfully")

    return redirect('/school/assessment_configration/')

@user_login_required
def DeleteAssessmentConfigration(request, ass_config_id):
    try:
        ass_config = assessment_configration_master.objects.get(id=ass_config_id)
        if ass_config.short_name=='NA':
            messages.success(request, "Cant delete default assessment configration.")
        else:
            assessment_configration_master.objects.filter(id=ass_config_id).delete()
            messages.success(request, "One record deleted successfully")
    except:
        messages.success(request, "Cant delete exam type relation exists somewhere.")
    return redirect('/school/assessment_configration/')

@user_login_required
@user_permission_required('assessment.can_view_view_configration_categories', '/school/home/')
def AssessmentConfigrtionRoundOff(request):
    return render(request, "assessment_configration_roundoff.html", {})

@user_login_required
def AssessmentRoundOff(request):
    assessment_configration_master_recs = assessment_configration_master.objects.all()
    return render(request, "assessment_configration.html", {'assessment_configration_master_recs': assessment_configration_master_recs})


@user_login_required
def AddRoundOff(request):

    if request.method == 'GET':
        return render(request, "add_round_off.html", {})

    if request.method == 'POST':
        select = request.POST['select']
        decimal_place = request.POST['decimal_place']

        if assessment_round_master.objects.filter(select=request.POST['select']).exists():
            messages.success(request, "Round-Up and Round-Down Already Exists")
            return render(request, "add_round_off.html", {})
        else:
            round_obj = assessment_round_master.objects.create(select=select, decimal_place=decimal_place)
        return redirect('/school/round_off_list/')


# @user_login_required
# def RoundOffList(request):
#     round_off_recs = assessment_round_master.objects.all()
#     return render(request, "round_off_list.html", {'round_off_recs': round_off_recs})

@user_login_required
def UpdateRoundOff(request):
    is_saved = False   # Flag to check record is already exist or not
    try:
        assessment_round_rec = assessment_round_master.objects.get()
        is_saved = True

        if assessment_round_rec.select:
            round_up = 'checked'
            round_down = ""
        else:
            round_down = "checked"
            round_up = ""

        rec = int(assessment_round_rec.decimal_place)
        zero = one = two = three = four = five = ''
        if (rec == 0):
            zero = 'checked'
        elif (rec == 1):
            one = 'checked'
        elif (rec == 2):
            two = 'checked'
        elif (rec == 3):
            three = 'checked'
        elif (rec == 4):
            four = 'checked'
        else:
            five = 'checked'

        return render(request, "update_round_off.html",
                      {'assessment_round_rec': assessment_round_rec, 'round_up': round_up, 'round_down': round_down,
                       'zero': zero, 'one': one, 'two': two, 'three': three, 'four': four, 'five': five,
                       'is_saved': is_saved})
    except:
        round_up = 'checked'
        round_down = ""

        zero = one = three = four = five = ''
        two = 'checked'

        return render(request, "update_round_off.html",
                      {'round_up': round_up, 'round_down': round_down,
                       'zero': zero, 'one': one, 'two': two, 'three': three, 'four': four, 'five': five,
                       'is_saved': is_saved})

@user_login_required
def SaveUpdatedRoundOff(request):
    if request.method == 'POST':

        if request.POST['select'] == 'round_up':
            bool_rec = True
        else:
            bool_rec = False

        try:
            obj = assessment_round_master.objects.get()
            obj.select = bool_rec
            obj.decimal_place = request.POST['round_select']
            obj.save()
            messages.success(request, "Record updated successfully")
        except:
            assessment_round_master.objects.create(select = bool_rec, decimal_place = request.POST['round_select'])
            messages.success(request, "Record saved successfully")
    return redirect('/school/update_round_off/')


@user_login_required
def DeleteRoundOff(request, round_off_id):
    try:
        assessment_round_master.objects.filter(id=round_off_id).delete()
        messages.success(request, "One record deleted successfully")
    except:
        messages.success(request, "Cant delete relation exists somewhere.")
    return redirect('/school/round_off_list/')

from django.http import JsonResponse
@user_login_required
def ActiveRound(request):
    round_id = request.POST.get('round_id', None)
    status_check = assessment_round_master.objects.filter(is_active=1)
    if status_check:
        flag=False
    else:
        assessment_round_master.objects.filter(id=round_id).update(is_active=1)
        flag = True;
    return JsonResponse(flag, safe=False)

@user_login_required
def InActiveRound(request):
    round_id = request.POST.get('round_id', None)
    assessment_round_master.objects.filter(id=round_id).update(is_active=0)
    flag = True;
    return JsonResponse(flag,safe=False)

@user_login_required
def SectionTypeMaster(request):
    section_type_recs = section_type_details.objects.all()
    return render(request, "section_type_list.html", {'section_type_recs': section_type_recs})

@user_login_required
def AddSectionType(request):
    return render(request, "add_section_type.html",)

@user_login_required
def SaveSectionType(request):
    section_type_recs = section_type_details.objects.all()
    if request.method == 'POST':
        scetion_type_name = request.POST.get('section_type_name')
        try:
            section_type_details.objects.create(section_type_name = scetion_type_name)
            messages.success(request, "Record Added Successfully.")
        except:
            messages.warning(request, "Some Error Occoured. Recored Not Saved")

    return render(request, "section_type_list.html", {'section_type_recs': section_type_recs})


def UpdateSectionType(request,type_id):
    section_type_obj = section_type_details.objects.get(id=type_id)
    return render(request, "update_section_type.html", {'section_type_rec': section_type_obj})

def SaveUpdateSectionType(request):
    scetion_type_name = request.POST.get('section_type_name')
    try:
        rec_id = request.POST.get('rec_id')
        section_type_obj = section_type_details.objects.filter(id=rec_id).update(section_type_name = scetion_type_name)
        messages.success(request, "Record Updated Successfully.")
    except:
        messages.warning(request, "Record Not Updated. Some Error Occoured")
        return redirect('/school/section_type_master/')
    return redirect('/school/update_section_type/'+str(rec_id))

def DeleteSectionType(request,type_id):
    try:
        section_type_details.objects.filter(id=type_id).delete()
        messages.success(request, "Record Deleted Successfully.")
    except:
        messages.warning(request, "Record Not Deleted. Some Error Occoured")
    return redirect('/school/section_type_master/')


import os
import shutil
from django.conf import settings
from registration.models import student_details
from datetime import datetime

def student_file_name(instance, filename):
    dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
    ext = filename.split('.')[-1]
    filename = "%s_%s.%s" % (instance.first_name.split(' ')[0], dirname, ext)

    return os.path.join('images', filename)

def import_student_bulk_photo(source):
    # dest = os.environ["/home/redbytes/Desktop/data/"]
    dest = settings.MEDIA_ROOT
    count = 0
    # Avoid using the reserved word 'file' for a variable - renamed it to 'filename' instead
    for filename in os.listdir(source):
        # os.path.splitext does exactly what its name suggests - split the name and extension of the file including the '.'
        name, extension = os.path.splitext(filename)

        try:
            student = student_details.objects.get(odoo_id=str(name))
            new_file = student_file_name(student, filename)
            student.photo = new_file
            student.save()
            count = count + 1
            if extension == ".jpg":
                dest_filename = dest + new_file
                if not os.path.isfile(dest_filename):
                    # We copy the file as is
                    shutil.copy(os.path.join(source, filename) , dest_filename)

            if extension == ".png":
                dest_filename = dest + new_file
                if not os.path.isfile(dest_filename):
                    # We copy the file as is
                    shutil.copy(os.path.join(source, filename) , dest_filename)



        except Exception as e:

            with open(settings.MEDIA_ROOT+'logs/bulk_student_photo_error_log.txt', 'a') as the_file:
                error_msg = 'odoo Id :=> '+str(name)+' Error :=> '+ str(e)
                the_file.write(error_msg)
                the_file.write("\n")
            pass

    print(str(count) + ' records updated.')

# from masters.views import import_student_bulk_photo
# import_student_bulk_photo('/home/ubuntu/image_zip/1/A')


#=-==--=-=-=-=-=--=Report Name Master=-=-=-=-=-=-==-=-=-=-=-=-==-=--=-=-=
@user_login_required
def ReportCardTermMaster(request):
    report_card_term_list = report_card_term_details.objects.all()
    return render(request, "report_card_term_list.html", {'report_card_term_list': report_card_term_list})

# def ReportCardConf(request):
#
#     is_saved = False
#     assessment_round_rec = report_card_conf.objects.get()
#     is_saved = True
#
#     report_card_term_list = report_card_term_details.objects.all()
#     return render(request, "report_card_term_list.html", {'report_card_term_list': report_card_term_list})

@user_login_required
def AddReportCardTermName(request):
    return render(request, "add_report_card_term.html",)


@user_login_required
def SaveReportName(request):
    report_card_term_list = report_card_term_details.objects.all()
    if request.method == 'POST':
        report_name = request.POST.get('report_name')
        try:
            if report_card_term_details.objects.filter(report_card_term_name=report_name).exists():
                messages.success(request, "Duplicate record can't be added.")
            else:
                report_card_term_details.objects.create(report_card_term_name = report_name)
                messages.success(request, "Record Added Successfully.")
        except:
            messages.warning(request, "Some Error Occoured. Recored Not Saved")

    return render(request, "report_card_term_list.html", {'report_card_term_list': report_card_term_list})


def UpdateReportTermName(request,type_id):
    report_name_obj = report_card_term_details.objects.get(id=type_id)
    return render(request, "update_report_card_term.html", {'report_name_obj': report_name_obj})


def SaveUpdateSectionType(request):
    report_name = request.POST.get('report_name')
    try:
            rec_id = request.POST.get('rec_id')
            if report_card_term_details.objects.filter(~Q(id=rec_id), report_card_term_name=report_name).exists():
               messages.success(request, "Report Name Already Exists")
            else:
                report_name_obj = report_card_term_details.objects.filter(id=rec_id).update(report_card_term_name = report_name)
                messages.success(request, "Record Updated Successfully.")
    except:
        messages.warning(request, "Record Not Updated. Some Error Occoured")
        return redirect('/school/report_card_term_master/')
    return redirect('/school/update_report_term_name/'+str(rec_id))

def DeleteReportName(request,type_id):
    try:
        report_card_term_details.objects.filter(id=type_id).delete()
        messages.success(request, "Record Deleted Successfully.")
    except:
        messages.warning(request, "Record Not Deleted. Some Error Occoured")
    return redirect('/school/report_card_term_master/')



import xmlrpclib
import logging
from registration.models import *
from datetime import datetime, timedelta
import datetime as d
logger = logging.getLogger(__name__)
import json
from django.views import View
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest

def OdooSyncOnOff(request):
    if request.method == 'POST':
        if request.user.is_superuser:
            AuthUser.objects.filter().update(connector_is_active=(json.loads(request.POST['switch'])))
    return HttpResponse(json.dumps({'flag': json.loads(request.POST['switch'])}))

@superuser_login_required
def OdooSyncCongiguration(request):
    return render(request,'template_odoo_configuration.html')

@superuser_login_required
def TemplateOdooConfigList(request):
    configuration_list = OdooConfiguration.objects.all()
    return render(request,'template_odoo_config_list.html',{'configuration_list': configuration_list})

@superuser_login_required
def TemplateAddOdooConfig(request):
    return render(request,'template_add_odoo_configuration.html')

@superuser_login_required
def TemplateUpdateOdooConfig(request, config_id=None):
    odoo_config_rec = OdooConfiguration.objects.get(id=config_id)
    return render(request,'template_odoo_configuration.html',{'odoo_config_rec': odoo_config_rec})

@superuser_login_required
def UpdateOdooConfig(request):
    if request.method == 'POST':
        val_dict = request.POST
        odoo_config_instance = OdooConfiguration.objects.get(id=val_dict['config_id'])
        odoo_config_instance.odoo_ip_address = val_dict['odoo_ip_address']
        odoo_config_instance.odoo_server_port = val_dict['odoo_server_port']
        odoo_config_instance.odoo_db_name = val_dict['odoo_db_name']
        odoo_config_instance.odoo_db_username = val_dict['odoo_db_username']
        odoo_config_instance.odoo_db_password = val_dict['odoo_db_password']
        odoo_config_instance.is_config_active = True if val_dict['is_config_active'].title() == 'True' else False
        odoo_config_instance.save()
    return redirect('/school/template_update_config/'+val_dict['config_id'])

@superuser_login_required
def AddOdooConfig(request):
    if request.method == 'POST':
        val_dict = request.POST
        odoo_config_instance = OdooConfiguration()
        odoo_config_instance.odoo_ip_address = val_dict['odoo_ip_address']
        odoo_config_instance.odoo_server_port = val_dict['odoo_server_port']
        odoo_config_instance.odoo_db_name = val_dict['odoo_db_name']
        odoo_config_instance.odoo_db_username = val_dict['odoo_db_username']
        odoo_config_instance.odoo_db_password = val_dict['odoo_db_password']
        odoo_config_instance.is_config_active = True if val_dict['is_config_active'] == 'true' or 'True' else False
        odoo_config_instance.save()
    return render(request,'template_odoo_config_list.html')

def make_database_connection():

    try:
        OdooSync = OdooConfiguration.objects.get(is_config_active=True)

        ODOO_SERVER_PORT = OdooSync.odoo_server_port
        ODOO_SERVER_URL = 'http://'+OdooSync.odoo_ip_address
        ODOO_SERVER_DBNAME = OdooSync.odoo_db_name
        ODOO_USERNAME = OdooSync.odoo_db_username
        ODOO_PASSWORD = OdooSync.odoo_db_password

        if not ODOO_SERVER_PORT:
            # If ODOO_SERVER_PORT == 0 or False, you use port standard 80
            url_login = ''.join([ODOO_SERVER_URL, '/xmlrpc/common'])
        else:
            url_login = ''.join([ODOO_SERVER_URL, ':', ODOO_SERVER_PORT, '/xmlrpc/common'])
            instance = ''.join([ODOO_SERVER_URL, ':', ODOO_SERVER_PORT, '/xmlrpc/object'])

        ODOO_SOCK_COMMON = xmlrpclib.ServerProxy(url_login)
        ODOO_SOCK_OBJECT = xmlrpclib.ServerProxy(instance)

        ODOO_ID = ODOO_SOCK_COMMON.login(ODOO_SERVER_DBNAME, ODOO_USERNAME, ODOO_PASSWORD)

        return ODOO_ID, ODOO_SOCK_OBJECT, OdooSync.id

    except Exception as e:
        # print(str(e))
        return None , str(e.message.split(':')[0]), None


def TestOdooConnection(request):
    odoo_id, odoo_sock_object, odoo_record = make_database_connection()

    if odoo_id:
        return HttpResponse(json.dumps({'success': "Connection Successfully Established"}))
    else:
        return HttpResponseBadRequest(json.dumps({'error': str("Connection failed! \n")+str(odoo_id)}),
                                      content_type="application/json")


def syncy_academic_year():

    odoo_id, odoo_sock_object, id = make_database_connection()
    odoo_record = OdooConfiguration.objects.get(id=id)
    try:
        # args = [('student_id', '=', 'TIAS-2166')]  # query clause
        args1 = ([])
        ids = odoo_sock_object.execute(odoo_record.odoo_db_name, odoo_id, odoo_record.odoo_db_password, 'batch', 'search', args1)

        print('ids ', ids)

        fields = ['id', 'name', 'code', 'start_date', 'end_date']
        data = odoo_sock_object.execute(odoo_record.odoo_db_name, odoo_id, odoo_record.odoo_db_password, 'batch', 'read', ids, fields)  # id
        print('data => ', data)

    except Exception as e:
        return HttpResponse({'error':str(e)})


    for obj in data:
        try:
            academic_year_instance = academic_year.objects.get(sync_id=obj['id'])
            academic_year_instance.year_name=obj['name']
            academic_year_instance.sync_id = obj['id']
            academic_year_instance.start_date=obj['start_date']
            academic_year_instance.end_date=obj['end_date']
            academic_year_instance.current_academic_year = compute_current_year(obj['start_date'],obj['end_date'])
            academic_year_instance.save()
        except:
            academic_year_instance = academic_year()
            academic_year_instance.sync_id = obj['id']
            academic_year_instance.year_code = obj['code']
            academic_year_instance.year_name = obj['name']
            academic_year_instance.start_date = obj['start_date']
            academic_year_instance.end_date = obj['end_date']
            academic_year_instance.current_academic_year = compute_current_year(obj['start_date'],obj['end_date'])
            academic_year_instance.save()

    return odoo_record.id

def syncy_class():

    odoo_id, odoo_sock_object, id = make_database_connection()
    odoo_record = OdooConfiguration.objects.get(id=id)
    try:
        # args = [('student_id', '=', 'TIAS-2166')]  # query clause
        args1 = ([])
        ids = odoo_sock_object.execute(odoo_record.odoo_db_name, odoo_id, odoo_record.odoo_db_password, 'course', 'search', args1)

        print('ids ', ids)

        fields = ['id', 'name', 'code', 'next_course']
        data = odoo_sock_object.execute(odoo_record.odoo_db_name, odoo_id, odoo_record.odoo_db_password, 'course', 'read', ids, fields)  # id
        print('data => ', data)

    except Exception as e:
        return HttpResponse({'error':str(e)})


    for obj in data:
        try:
            class_instance = class_details.objects.get(class_name=obj['name'])
            class_instance.sync_id = obj['id']
            class_instance.class_name = obj['name']
            class_instance.class_name = obj['name']
            class_instance.next_class = class_details.objects.get(class_name=obj['next_course'][1]) if obj['next_course'] else None
            class_instance.save()
        except:
            try:
                if obj['code'] and obj['name']:
                    class_instance = class_details()
                    class_instance.sync_id = obj['id']
                    class_instance.class_code = obj['code']
                    class_instance.class_name = obj['name']
                    try:
                        class_instance.next_class = class_details.objects.get(class_name=obj['next_course'][1]) if obj['next_course'] else None
                    except:
                        class_instance.next_class = None

                    class_instance.save()
            except Exception as e:
                pass

    return odoo_record.id

def syncy_section():

    odoo_id, odoo_sock_object, id = make_database_connection()
    odoo_record = OdooConfiguration.objects.get(id=id)
    try:
        # args = [('student_id', '=', 'TIAS-2166')]  # query clause
        args1 = ([])
        ids = odoo_sock_object.execute(odoo_record.odoo_db_name, odoo_id, odoo_record.odoo_db_password, 'section', 'search', args1)

        print('ids ', ids)

        fields = ['id', 'name', 'code']
        data = odoo_sock_object.execute(odoo_record.odoo_db_name, odoo_id, odoo_record.odoo_db_password, 'section', 'read', ids, fields)  # id
        print('data => ', data)

    except Exception as e:
        return HttpResponse({'error':str(e)})


    for obj in data:
        try:
            section_instance = sections.objects.get(section_name=obj['name'])
            section_instance.sync_id = obj['id']
            section_instance.section_code = obj['code']
            section_instance.section_name = obj['name']
            section_instance.save()
        except:
            try:
                if obj['code'] and obj['name']:
                    section_instance = sections()
                    section_instance.sync_id = obj['id']
                    section_instance.section_code = obj['code']
                    section_instance.section_name = obj['name']
                    section_instance.save()
            except Exception as e:
                pass

    return odoo_record.id


def syncy_get_or_create_section():

    odoo_id, odoo_sock_object, id = make_database_connection()
    odoo_record = OdooConfiguration.objects.get(id=id)
    try:

        for obj in sections.objects.all():
            args = [('code', '=', str(obj.section_code))]  # query clause
            ids = odoo_sock_object.execute(odoo_record.odoo_db_name, odoo_id, odoo_record.odoo_db_password, 'section',
                                           'search', args)
            if not ids:

                id = odoo_sock_object.execute(odoo_record.odoo_db_name, odoo_id, odoo_record.odoo_db_password, 'section', 'create', {
                    'code': str(obj.section_code), 'name': str(obj.section_name)
                })

    except Exception as e:
        return HttpResponse({'error':str(e)})

    return odoo_record.id


def syncy_get_or_create_student():

    odoo_id, odoo_sock_object, id = make_database_connection()
    odoo_record = OdooConfiguration.objects.get(id=id)
    try:
        student_ids_list = []
        for obj in student_details.objects.filter(academic_class_section__year_name__current_academic_year=True):
            args = [('odoo_id', '=', str(obj.odoo_id))]  # query clause
            ids = odoo_sock_object.execute(odoo_record.odoo_db_name, odoo_id, odoo_record.odoo_db_password, 'res.partner',
                                           'search', args)
            if not ids:
                student_ids_list.append(obj.id)

                id = odoo_sock_object.execute(odoo_record.odoo_db_name, odoo_id, odoo_record.odoo_db_password, 'section', 'create', {
                    'code': str(obj.section_code), 'name': str(obj.section_name)
                })

    except Exception as e:
        return HttpResponse({'error':str(e)})

    return odoo_record.id

def syncy_class_section_mapping():

    odoo_id, odoo_sock_object, id = make_database_connection()
    odoo_record = OdooConfiguration.objects.get(id=id)
    try:
        # args = [('student_id', '=', 'TIAS-2166')]  # query clause
        args1 = ([])
        ids = odoo_sock_object.execute(odoo_record.odoo_db_name, odoo_id, odoo_record.odoo_db_password, 'course', 'search', args1)

        print('ids ', ids)

        fields = ['id', 'name', 'code', 'section']
        data = odoo_sock_object.execute(odoo_record.odoo_db_name, odoo_id, odoo_record.odoo_db_password, 'course', 'read', ids, fields)  # id
        print('data => ', data)

    except Exception as e:
        return HttpResponse({'error':str(e)})


    for obj in data:
        try:
            for sec in obj.get('section'):
                if not academic_class_section_mapping.objects.filter(class_name__sync_id=obj.get('id'),section_name__sync_id=sec).exists():
                    cls_obj = class_details.objects.get(sync_id=obj.get('id'))
                    sec_obj = sections.objects.get(sync_id=sec)
                    cls_sec_obj = academic_class_section_mapping.objects.create(year_name=academic_year.objects.get(current_academic_year=True),class_name=cls_obj,section_name=sec_obj)

        except Exception as e:
            pass

    return odoo_record.id

def create_or_update_student_form_erp():
    odoo_id, odoo_sock_object, id = make_database_connection()
    odoo_record = OdooConfiguration.objects.get(id=id)

    # odoo_record = OdooConfiguration.objects.get(id=id)
    try:
        # args = [('is_student', '=', True),('active', '=', True)]  # query clause
        args = [('is_student', '=', True),('student_section_id', '!=', False)]  # query clause
        # args1 = ([])
        ids = odoo_sock_object.execute(odoo_record.odoo_db_name, odoo_id, odoo_record.odoo_db_password, 'res.partner', 'search', args)

        print('ids ', ids)

        fields = ['id', 'name', 'last_name', 'email', 'year_id', 'class_id', 'student_section_id', 'image', 'birth_date', 'date_of_joining', 'mobile', 'street', 'arab', 'emirati']
        data = odoo_sock_object.execute(odoo_record.odoo_db_name, odoo_id, odoo_record.odoo_db_password, 'res.partner', 'read', ids, fields)  # id
        print('data => ', data)

    except Exception as e:
        return HttpResponse({'error':str(e)})

    for obj in data:
        try:
            user_instance = student_details.objects.get(odoo_id=obj['id'])
            user_instance.email = obj.get('email')
            user_instance.first_name = obj.get('name')
            user_instance.last_name = obj.get('last_name')
            user_instance.birth_date = obj.get('birth_date')
            user_instance.joining_date = obj.get('date_of_joining')
            # user_instance.nationality = obj.get('nationality')
            user_instance.postcode = obj.get('postal_address')
            user_instance.street = obj.get('street')
            if obj.get('arab') == 'arab':
                user_instance.select = 'yes'
            if obj.get('arab') == 'non_arab':
                user_instance.select = 'no'

            if obj.get('emirati') == 'y':
                user_instance.emirati = 'yes'
            if obj.get('emirati') == 'n':
                user_instance.emirati = 'no'
            user_instance.emergency_contact_num = obj.get('mobile')
            if obj.get('image'):
                if user_instance.photo:
                    if os.path.exists(settings.MEDIA_ROOT + str(user_instance.photo)):
                        os.remove(settings.MEDIA_ROOT + str(user_instance.photo))
                try:
                    dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
                    filename = "%s_%s.%s" % (obj['name'], dirname, 'png')
                    raw_file_path_and_name = (os.path.join('/images', filename))
                    raw_data = base64.b64decode(obj['image'])
                    f = open(settings.MEDIA_ROOT+'images/' +filename, 'wb')
                    f.write(raw_data)
                    f.close()
                    user_instance.photo = raw_file_path_and_name
                except Exception as e:
                    pass

            user_instance.save()
        except Exception as e:
            try:
                if obj.get('email') and not student_details.objects.filter(odoo_id=obj['id']).exists():
                    user_instance = student_details()
                    user_instance.email = obj.get('email')
                    user_instance.first_name = obj.get('name')
                    user_instance.last_name = obj.get('last_name')
                    user_instance.birth_date = obj.get('birth_date')
                    user_instance.joining_date = obj.get('date_of_joining')
                    # user_instance.nationality = obj.get('nationality')
                    user_instance.emergency_contact_num = obj.get('mobile')
                    user_instance.street = obj.get('street')
                    user_instance.postcode = obj.get('postal_address')
                    user_instance.odoo_id = obj.get('id')
                    if obj.get('arab') == 'arab':
                        user_instance.select = 'yes'
                    if obj.get('arab') == 'non_arab':
                        user_instance.select = 'no'

                    if obj.get('emirati') == 'y':
                        user_instance.emirati = 'yes'
                    if obj.get('emirati') == 'n':
                        user_instance.emirati = 'no'
                    year_class_sec_obj = academic_class_section_mapping.objects.filter(year_name__sync_id=obj.get('year_id')[0],
                                                               class_name__sync_id=obj.get('class_id')[0])

                    if obj.get('student_section_id'):
                        year_class_sec_obj = year_class_sec_obj.filter(section_name__sync_id=obj.get('student_section_id')[0])
                        user_instance.academic_class_section = year_class_sec_obj[0]


                    # if obj.get('image'):
                    #     if os.path.exists(settings.MEDIA_ROOT + str(user_instance.photo)):
                    #         os.remove(settings.MEDIA_ROOT + str(user_instance.photo))
                    #     try:
                    #         dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
                    #         filename = "%s_%s.%s" % (obj['name'], dirname, 'png')
                    #         raw_file_path_and_name = (os.path.join('/images', filename))
                    #         raw_data = base64.b64decode(obj['image'])
                    #         f = open(settings.MEDIA_ROOT + 'images/' + filename, 'wb')
                    #         f.write(raw_data)
                    #         f.close()
                    #         user_instance.photo = raw_file_path_and_name
                    #     except Exception as e:
                    #         pass
                    user_instance.save()
            except Exception as e:
                pass

    return odoo_record.id

def StartOdooSync(request):

    if request.method == 'POST':

        val_dict = json.loads(json.dumps(request.POST))
        if request.user.connector_is_active == True:

            try:

                if str(val_dict['year_check'].title()) == 'True':
                    id = syncy_academic_year()
                    # pass

                if str(val_dict['class_check'].title()) == 'True':
                    id = syncy_class()

                if str(val_dict['section_check'].title()) == 'True':
                    id = syncy_section()
                    # syncy_get_or_create_section()
                    syncy_class_section_mapping()


                if str(val_dict['student_check'].title()) == 'True':
                    create_or_update_student_form_erp()

            except Exception as e:
                messages.warning(request, "Some Error Occoured durring Sync :=> " + str(e))
                pass

            return redirect('/school/template_update_config/' + str(val_dict['id']))
        else:
            messages.success(request, "Odoo sync is off. if you want to perform this operation Odoo sync must be on. Please contact Administrator")
            return redirect('/school/template_update_config/' + str(val_dict['id']))




import os
import shutil
from django.conf import settings
from registration.models import student_details
from datetime import datetime

def get_all_photos(request):
    # dest = os.environ["/home/redbytes/Desktop/data/"]
    dest = settings.MEDIA_ROOT
    count = 0
    # Avoid using the reserved word 'file' for a variable - renamed it to 'filename' instead
    # for filename in os.listdir(destination):
    # # os.path.splitext does exactly what its name suggests - split the name and extension of the file including the '.'
    # name, extension = os.path.splitext(filename)

    try:
        destination = "/home/ubuntu/TIAS_PHOTO"
        student_list = student_details.objects.all()

        for student in student_list:

            if student.photo and student.odoo_id:
                name, extension = os.path.splitext(dest + str(student.photo))
                if extension == ".jpg":
                    dest_filename = dest +str(student.photo)
                    if os.path.isfile(dest_filename):
                        print()
                        # We copy the file as is
                        shutil.copy(dest_filename, os.path.join(destination, str(student.odoo_id)))

                if extension == ".png":
                    dest_filename = dest +str(student.photo)
                    if os.path.isfile(dest_filename):
                        # We copy the file as is
                        shutil.copy(dest_filename, os.path.join(destination, str(student.odoo_id)))



    except Exception as e:
        pass

# from masters.views import import_student_bulk_photo
# import_student_bulk_photo('/home/ubuntu/image_zip/1/A')


import os


def change_file_ext(cur_dir, old_ext, new_ext, sub_dirs=False):
    if sub_dirs:
        for root, dirs, files in os.walk(cur_dir):
            for filename in files:
                file_ext = os.path.splitext(filename)[1]
                if old_ext == file_ext:
                    oldname = os.path.join(root, filename)
                    newname = oldname.replace(old_ext, new_ext)
                    os.rename(oldname, newname)
    else:
        files = os.listdir(cur_dir)
        for filename in files:
            file_ext = os.path.splitext(filename)[1]
            if old_ext == file_ext:
                newfile = filename.replace(filename,filename+'.png')
                os.rename(cur_dir + '/' + filename, cur_dir + '/' + newfile)


def change_file_extensions(request):
    change_file_ext('/home/redbytes/Documents/TIAD_PHOTO/student_photos', '.jpg', '.png', sub_dirs=False)
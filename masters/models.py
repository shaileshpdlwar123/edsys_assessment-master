from __future__ import unicode_literals

from django.db import models

# from registration.models import nationality
# from registration.models import *


# Create your models here.

class exam_type(models.Model):
    exam_type_name = models.CharField(max_length=250)

    def __unicode__(self):
        return self.exam_type_name

class section_type_details(models.Model):
    section_type_name = models.CharField(max_length=80)

    def __unicode__(self):
        return self.section_type_name



class rubrics_category(models.Model):
    rubric_catgory = models.CharField(max_length=100)

    def __unicode__(self):
        return self.rubric_catgory



class rubrics_master(models.Model):
    rubrics_name = models.CharField(max_length=100)
    rubrics_mark = models.CharField(max_length=5)
    rubrics_category_name = models.ForeignKey(rubrics_category, on_delete=models.PROTECT, null=True)

    def __unicode__(self):
        return self.rubrics_name


class nationality_group(models.Model):
    nationatity_group_name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.nationatity_group_name

class house_master(models.Model):
    house_name = models.CharField(max_length=250)

    def __unicode__(self):
        return self.house_name

class nationality_group_mapping(models.Model):
    nationality_group_name = models.ForeignKey(nationality_group, on_delete=models.PROTECT, null=True)
    nationality_mapping_name = models.ForeignKey('registration.nationality', on_delete=models.PROTECT, null=True)


    # def __unicode__(self):
    #     return self.nationality_group_name


class tc_type(models.Model):
    tc_type_name = models.CharField(max_length=260)

    def __unicode__(self):
        return self.tc_type_name



class assessment_configration_master(models.Model):
    configration_name = models.CharField(max_length=250)
    short_name = models.CharField(max_length=100,blank=True, null=True)
    select = models.CharField(max_length=25,blank=True, null=True)

    def __unicode__(self):
        return self.configration_name


class assessment_round_master(models.Model):
    select = models.BooleanField(default=True)
    decimal_place = models.CharField(max_length=10)

class report_card_conf(models.Model):
    url = models.CharField(max_length=80, blank=True, null=True)
    user_name = models.CharField(max_length=80, blank=True, null=True)
    port = models.CharField(max_length=80, blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

class report_card_term_details(models.Model):
    report_card_term_name = models.CharField(max_length=80,blank=True, null=True)

    def __unicode__(self):
        return self.report_card_term_name


class OdooConfiguration(models.Model):
    odoo_ip_address = models.CharField(max_length=100,null=True,blank=True)
    odoo_server_port = models.CharField(max_length=10,null=True,blank=True)
    odoo_db_name = models.CharField(max_length=100,null=True,blank=True)
    odoo_db_username = models.CharField(max_length=100,null=True,blank=True)
    odoo_db_password = models.CharField(max_length=100,null=True,blank=True)
    is_config_active = models.BooleanField(default=False)

    def __unicode__(self):
        return self.odoo_ip_address
from django import template
from assessment.models import grade_sub_details,subject_strands
from registration.models import student_details
from assessment.models import *

register = template.Library()

@register.filter(name='subtract')
def subtract(value, arg):
    return value - arg

@register.filter(name='mul')
def mul(value, arg):
    """Multiply the arg to the value."""
    return value * arg

@register.filter(name='range')
def filter_range(start, end):
  return range(start, end)

@register.filter
def index(List, i):
    try:
        return List[int(i)]
    except:
        return ''

@register.filter
def exam_index(List, i):
    try:
        return List[int(i)].exam_name.exam_name
    except:
        return ''

@register.filter
def weightage_index(List, i):
    try:
        return List[int(i)].weightage
    except:
        return ''

@register.filter
def frequency_index(List, i):
    try:
        return List[int(i)].frequency_name.frequency_name
    except:
        return ''

@register.filter
def strindex(List, i):
    try:
        return List[i]
    except:
        return ''

@register.filter
def dict_val(dict, i):
    # print dict
    return dict.get(i)

@register.filter
def list_len(list):
    # print dict
    return list_len.count()

@register.filter
def check_id_in_list(id, list):
    try:
        if id in list:
            return True
        else:
            return False
    except:
        return False

@register.filter
def index_returner(main_dict, index_obj):
    try:
        return main_dict[index_obj]['rec']
    except:
        return main_dict[index_obj][0]['rec']


@register.filter
def get_grade_details(grade_obj):
    grade_recs = grade_sub_details.objects.filter(grade=grade_obj.grade)

    return grade_recs



@register.filter
def get_sub_strand_details(strand_obj):
    sub_strand_recs = subject_strands.objects.get(id=strand_obj).substrands_ids.all()
    return sub_strand_recs



@register.filter
def get_pick_pick_up_time(pick_up_rec):
    if not pick_up_rec == None:
        pick_up_time = pick_up_rec.strftime('%H:%M')
    else:
        pick_up_time = None
    return pick_up_time


@register.filter
def get_perodic_sub_strand_details(strand_obj):
    # strand_obj_id = strand_obj.id
    sub_strand_recs =subject_strands.objects.get(id=strand_obj).substrands_ids.all()
    # sub_strand_recs = perodic_strand.objects.get(id=strand_obj_id).sub_strand_ids.all()
    return sub_strand_recs





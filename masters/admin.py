from django.contrib import admin
from .models import assessment_round_master, OdooConfiguration


# Register your models here.

admin.site.register(assessment_round_master)
admin.site.register(OdooConfiguration)
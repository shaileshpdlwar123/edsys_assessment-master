# -*- coding: utf-8 -*-
import xlwt
# import Workbook, easyxf
from django.http import HttpResponse
from datetime import datetime
import datetime as d
from django.conf import settings
from django.core.mail import send_mail
from django.contrib import messages
from registration.models import academic_class_section_mapping
from django.db.models import Q
from django.forms.models import model_to_dict
from calendar import monthrange
from registration.models import AuthUser,role,academic_year,school_details
from django.contrib.auth.models import Permission
from Crypto.Cipher import AES
import base64
from io import BytesIO
import xlsxwriter
import collections
from math import ceil, floor
import unicodedata

# PDF import libs
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, TableStyle, Table
from reportlab.lib.pagesizes import A4, inch, landscape
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_CENTER,TA_RIGHT

# from datetime import date
# import datetime

def export_users_xls(output_file_name, columns, rows):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename='+output_file_name+".xls"

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet(output_file_name,cell_overwrite_ok=True)

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    # columns = output_column_heading

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    # rows = AuthUser.objects.all().values_list('username', 'first_name', 'last_name', 'email', 'role__name')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, unicode(row[col_num]), font_style)

    wb.save(response)
    return response


def export_users_with_time_xls(output_file_name, columns, rows,date):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename='+output_file_name+".xls"

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet(output_file_name,cell_overwrite_ok=True)

    # Sheet header, first row
    row_num = 2

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    ws.write(0, 6,'Downloaded on', font_style)
    ws.write(0, 7,'-'.join(str(date).split('-')[::-1])+'  '+str(datetime.now().strftime("%H:%M")), font_style)

    # columns = output_column_heading

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    # rows = AuthUser.objects.all().values_list('username', 'first_name', 'last_name', 'email', 'role__name')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, unicode(row[col_num]), font_style)

    wb.save(response)
    return response



def export_locked_column_xls(output_file_name, column_names, rows, locked_columns):

    output = BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet(output_file_name)

    # Create some cell formats with protection properties.
    unlocked = workbook.add_format({'locked': False,'text_wrap': True})
    locked = workbook.add_format({'locked': True,'text_wrap': True})

    # Format the worksheet to unlock all cells.
    worksheet.set_column('A:XDF', None, unlocked)

    # Turn worksheet protection on.
    worksheet.protect()
    # bold = workbook.add_format({'bold': True})

    row_num = 0
    for col_num in range(len(column_names)):
        worksheet.set_column(col_num, col_num, 18)
        worksheet.write(row_num, col_num, column_names[col_num], locked)

    for row in rows:
        row_num += 1

        for col_num in range(len(row)):
            if col_num in locked_columns:
                worksheet.write(row_num, col_num, unicode(row[col_num]), locked)
            else:
                worksheet.write(row_num, col_num, unicode(row[col_num]), unlocked)

    workbook.close()
    output.seek(0)
    response = HttpResponse(output.read(), content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=' + output_file_name + ".xls"

    return response


# def drange(start, stop, step):
#     r = start
#     while r < stop:
#         yield r
#         r += step


# def export_locked_validation_column_xls(output_file_name, column_names, rows, locked_columns):
#     # mark_list = [round(x * 0.1, 1) for x in range(20 * 10)]
#     # absent_list = ['NA', 'Ab']
#     # mark_list.extend(absent_list)
#     # print len(mark_list)
#
#     output = BytesIO()
#     workbook = xlsxwriter.Workbook(output)
#     worksheet = workbook.add_worksheet(output_file_name)
#
#     # Create some cell formats with protection properties.
#     unlocked = workbook.add_format({'locked': False,'text_wrap': True})
#     locked = workbook.add_format({'locked': True,'text_wrap': True})
#
#     # unlocked.set_text_wrap()
#     # locked.set_text_wrap()
#
#     # Format the worksheet to unlock all cells.
#     worksheet.set_column('A:XDF', None, locked)
#
#     # Turn worksheet protection on.
#     worksheet.protect()
#      #bold = workbook.add_format({'bold': True})
#
#     row_num = 0
#     for col_num in range(len(column_names)):
#         worksheet.write(row_num, col_num, column_names[col_num], locked)
#
#     for row in rows:
#         row_num += 1
#
#         for col_num in range(len(row)):
#             worksheet.set_column(col_num, col_num, 18)
#
#             if col_num in locked_columns:
#
#                 worksheet.write(row_num, col_num, unicode(row[col_num]), locked)
#
#             else:
#                 try:
#                     if row[col_num] != '':
#                         float(row[col_num])
#                     worksheet.data_validation(row_num, col_num, row_num, col_num, {'validate': 'decimal',
#                                                                                    'criteria': 'between',
#                                                                                    'minimum': 0,
#                                                                                    'maximum': row[col_num + 1],
#                                                                                    # 'input_title': 'Enter an integer:',
#                                                                                    # 'input_message': 'between 1 and 100',
#                                                                                    'error_title': 'Input value not valid!',
#                                                                                    'error_message': 'Enter Value Between 0 to ' + str(
#                                                                                        row[col_num + 1])})
#                     worksheet.write(row_num, col_num, unicode(row[col_num]), unlocked)
#                 except:
#                     worksheet.write(row_num, col_num, unicode(row[col_num]), locked)
#
#                 # print mark_list
#                 # worksheet.data_validation(row_num,col_num,row_num, col_num, {'validate': 'list',
#                 #                  'source': mark_list,
#                 #                  })
#
#
#                 # print row_num, row_num, col_num, col_num
#
#                 # worksheet.data_validation(row_num,col_num,row_num, col_num, {'validate': 'decimal',
#                 #                   'criteria': 'between',
#                 #                   'minimum': 0,
#                 #                   'maximum': row[col_num+1],
#                 #                   # 'input_title': 'Enter an integer:',
#                 #                   # 'input_message': 'between 1 and 100',
#                 #                   'error_title': 'Input value not valid!',
#                 #                   'error_message': 'Enter Value Between 0 to '+str(row[col_num+1])})
#                 # worksheet.write(row_num, col_num, unicode(row[col_num]), unlocked)
#     worksheet.set_column(0, 0, 8)
#     worksheet.set_column(4, 4, 20)
#     # worksheet.set_column(5, 5, 10)
#     # worksheet.set_column(6, 6, 10)
#     workbook.close()
#     output.seek(0)
#     response = HttpResponse(output.read(), content_type='application/ms-excel')
#     response['Content-Disposition'] = 'attachment; filename=' + output_file_name + ".xls"
#
#     return response

def export_locked_validation_column_xls(output_file_name, column_names, rows, locked_columns,max_mark_list):
    # mark_list = [round(x * 0.1, 1) for x in range(20 * 10)]
    # absent_list = ['NA', 'Ab']
    # mark_list.extend(absent_list)
    # print len(mark_list)

    output = BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet(output_file_name)

    # Create some cell formats with protection properties.
    unlocked = workbook.add_format({'locked': False,'text_wrap': True})
    locked = workbook.add_format({'locked': True,'text_wrap': True})

    # unlocked.set_text_wrap()
    # locked.set_text_wrap()

    # Format the worksheet to unlock all cells.
    worksheet.set_column('A:XDF', None, locked)

    # Turn worksheet protection on.
    worksheet.protect()
     #bold = workbook.add_format({'bold': True})

    row_num = 0
    for col_num in range(len(column_names)):
        worksheet.write(row_num, col_num, column_names[col_num], locked)

    for row in rows:
        row_num += 1

        for col_num in range(len(row)):
            worksheet.set_column(col_num, col_num, 18)

            if col_num in locked_columns:

                worksheet.write(row_num, col_num, unicode(row[col_num]), locked)

            else:
                try:
                    # if row[col_num] != '':
                    #     float(row[col_num])
                    # worksheet.data_validation(row_num, col_num, row_num, col_num, {'validate': 'decimal',
                    #                                                                'criteria': 'between',
                    #                                                                'minimum': 0,
                    #                                                                'maximum': row[col_num + 1],
                    #                                                                # 'input_title': 'Enter an integer:',
                    #                                                                # 'input_message': 'between 1 and 100',
                    #                                                                'error_title': 'Input value not valid!',
                    #                                                                'error_message': 'Enter Value Between 0 to ' + str(
                    #                                                                    row[col_num + 1])})
                    # worksheet.write(row_num, col_num, unicode(row[col_num]), unlocked)
                    if row[col_num] != '':
                            str(row[col_num])


                            # worksheet.data_validation(row_num, col_num, row_num, col_num, {'validate': 'decimal',
                            #                                                                     'criteria': 'between',
                            #                                                                     'minimum': 0,
                            #                                                                     'maximum':max_mark_list[count],
                            #                                                                     'error_title': 'Input value not valid!',
                            #                                                                     'error_message': 'Enter Value Between 0 to ' + str(max_mark_list[count])})

                            data = {1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F', 7: 'G', 8: 'H', 9: 'I', 10: 'J', 11: 'K',
                             12: 'L', 13: 'M', 14: 'N', 15: 'O', 16: 'P', 17: 'Q', 18: 'R', 19: 'S', 20: 'T', 21: 'U',
                             22: 'V', 23: 'W', 24: 'X', 25: 'Y', 26: 'Z'}


                            from assessment.models import assessment_configration_master
                            formula = ''
                            i = 1
                            for obj in assessment_configration_master.objects.all():
                                if i == 1:
                                    formula += '=OR(AND('+data[col_num+1]+str(row_num+1)+'>=0,'+data[col_num+1]+str(row_num+1)+'<='+str(max_mark_list[count])+'),'+data[col_num+1]+str(row_num+1)+'="'+str(obj.short_name)+'")'
                                else:
                                    formula += '+OR(AND('+data[col_num+1]+str(row_num+1)+'>=0,'+data[col_num+1]+str(row_num+1)+'<='+str(max_mark_list[count])+'),'+data[col_num+1]+str(row_num+1)+'="'+str(obj.short_name)+'")'
                                i = i + 1

                            worksheet.data_validation(row_num, col_num, row_num, col_num, {'validate': 'custom',
                                                                                            'value': formula,
                                                                                            'error_title': 'Input value not valid!',
                                                                                            'error_message': 'Enter Value Between 0 to ' + str(
                                                                                               max_mark_list[count])
                                                                                           })
                            worksheet.write(row_num, col_num, unicode(row[col_num]), unlocked)
                            count =count+1
                except:
                    worksheet.write(row_num, col_num, unicode(row[col_num]), locked)

                # print mark_list
                # worksheet.data_validation(row_num,col_num,row_num, col_num, {'validate': 'list',
                #                  'source': mark_list,
                #                  })


                # print row_num, row_num, col_num, col_num

                # worksheet.data_validation(row_num,col_num,row_num, col_num, {'validate': 'decimal',
                #                   'criteria': 'between',
                #                   'minimum': 0,
                #                   'maximum': row[col_num+1],
                #                   # 'input_title': 'Enter an integer:',
                #                   # 'input_message': 'between 1 and 100',
                #                   'error_title': 'Input value not valid!',
                #                   'error_message': 'Enter Value Between 0 to '+str(row[col_num+1])})
                # worksheet.write(row_num, col_num, unicode(row[col_num]), unlocked)
    worksheet.set_column(0, 0, 8)
    worksheet.set_column(4, 4, 20)
    # worksheet.set_column(5, 5, 10)
    # worksheet.set_column(6, 6, 10)
    workbook.close()
    output.seek(0)
    response = HttpResponse(output.read(), content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=' + output_file_name + ".xls"

    return response

def export_freezd_column_xls(output_file_name, column_names, rows):

    output = BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet(output_file_name)

    # Create some cell formats with protection properties.
    unlocked = workbook.add_format({'locked': False,'text_wrap': True})
    locked = workbook.add_format({'locked': True,'text_wrap': True})

    # Format the worksheet to unlock all cells.
    worksheet.set_column('A:XDF', None, locked)

    # Turn worksheet protection on.
    worksheet.protect()
    # bold = workbook.add_format({'bold': True})

    row_num = 0
    for col_num in range(len(column_names)):
        worksheet.set_column(col_num, col_num, 18)
        worksheet.write(row_num, col_num, column_names[col_num])

    for row in rows:
        row_num += 1

        for col_num in range(len(row)):
            # if col_num in locked_columns:
            worksheet.write(row_num, col_num, unicode(row[col_num]))

            # else:
            #     worksheet.data_validation(row_num,row_num, col_num, col_num, {'validate': 'decimal',
            #                       'criteria': 'between',
            #                       'minimum': 0,
            #                       'maximum': row[col_num+1],
            #                       'error_title': 'Input value not valid!',
            #                       'error_message': 'Enter Value Between 0 to '+str(row[col_num+1])})
            #     worksheet.write(row_num, col_num, unicode(row[col_num]), unlocked)

    workbook.close()
    output.seek(0)
    response = HttpResponse(output.read(), content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=' + output_file_name + ".xls"

    return response

def export_pdf(output_file_name, records):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=' + str(output_file_name)+'.pdf'

    elements = []

    # school_rec = school_details.objects.filter()[0]
    # if school_rec.logo:
    #     img_path = 'http://' + request.META['HTTP_HOST'] + '/school/media/' + str(school_rec.logo)
    #     I = Image(img_path)
    #     I.drawHeight = 1 * inch
    #     I.drawWidth = 1 * inch
    #     elements.append(I)

    doc = SimpleDocTemplate(response, topMargin=10)
    doc.pagesize = landscape(A4)

    # #Get this line right instead of just copying it from the docs
    style = TableStyle([('ALIGN', (1, 1), (-2, -2), 'RIGHT'),
                        ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
                        ('VALIGN', (0, 0), (0, -1), 'TOP'),
                        ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
                        ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
                        ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
                        ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
                        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                        ])

    # Configure style and word wrap

    ps = ParagraphStyle('title', fontSize=10, alignment=TA_CENTER, spaceBefore=10, spaceAfter=10)
    # elements.append(Paragraph(str(school_rec.school_name), ps))
    ps = ParagraphStyle('title', fontSize=10, alignment=TA_RIGHT, spaceBefore=10, spaceAfter=10)
    # elements.append(Paragraph('Downloaded On- ' + '-'.join(str(date).split('-')[::-1]) + '  ' + str(
    #     datetime.datetime.now().strftime("%H:%M")), ps))

    s = getSampleStyleSheet()
    s = s["BodyText"]
    s.wordWrap = 'CJK'
    data = [[Paragraph(cell, s) for cell in row] for row in records]
    table_obj = Table(data)
    table_obj.setStyle(style)

    elements.append(table_obj)
    doc.build(elements)
    return response

def current_academic_year_mapped_classes(academic_year_id, request_type):
    academic_class_section_mapping_classes = []
    modelDict = []
    if request_type.is_system_admin() or request_type.is_allowed_user():
        academic_class_section_mapping_classes = academic_class_section_mapping.objects.filter( year_name=academic_year_id)
    else:
        # academic_class_section_mapping_classes = academic_class_section_mapping.objects.filter(year_name=academic_year_id, staff=request.user)
        if request_type.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_year_id):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                        supervisor_id=request_type.id):
                    academic_class_section_mapping_classes.append(mapped_object)
        else:
            academic_class_section_mapping_classes = academic_class_section_mapping.objects.filter(
                year_name=academic_year_id).filter(Q(assistant_teacher=request_type.id) | Q(staff_id=request_type.id))

    raw_list = []
    for obj in academic_class_section_mapping_classes:
        raw_list.append(obj.class_name)
    class_obj_list = list(set(raw_list))
    for obj in class_obj_list:
        modelDict.append(obj.to_dict())
    return modelDict

def current_class_mapped_sections(academic_year,class_name,request_type):
    modelDict=[]
    # class_name = request.GET.get('class_name',None)
    # academic_year = request.GET.get('academic_year',None)
    ClassSectionMap = []

    # if request.user.role.name == 'System Admin':
    if request_type.is_system_admin()  or request_type.is_allowed_user():
        ClassSectionMap = academic_class_section_mapping.objects.filter(class_name=class_name, year_name=academic_year)
    else:

        # if request.user.role.name == 'Supervisor':
        if request_type.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(class_name=class_name, year_name=academic_year):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(supervisor_id=request_type.id):
                    ClassSectionMap.append(mapped_object)
        else:
            ClassSectionMap = academic_class_section_mapping.objects.filter(class_name=class_name, year_name=academic_year).filter(Q(assistant_teacher = request_type.id) | Q(staff_id = request_type.id))

    raw_list = []
    for obj in ClassSectionMap:
        raw_list.append(obj.section_name)
    section_obj_list = list(set(raw_list))

    # for obj in ClassSectionMap:
    #     modelDict.append(model_to_dict(obj.section_name))
    # return modelDict

    for obj in section_obj_list:
        modelDict.append(obj.to_dict())
    return modelDict


def mapped_user_classes(acd_year,request):
    modelDict = []
    academic_year_id = acd_year
    academic_class_section_mapping_classes = []

    if request.user.is_system_admin()  or request.user.is_allowed_user():
        academic_class_section_mapping_classes = academic_class_section_mapping.objects.filter(
            year_name=academic_year_id)
    else:
        # academic_class_section_mapping_classes = academic_class_section_mapping.objects.filter(year_name=academic_year_id, staff=request.user)
        if request.user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_year_id):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                        supervisor_id=request.user.id):
                    academic_class_section_mapping_classes.append(mapped_object)


        elif request.user.is_subject_teacher():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_year_id):
                for obj in mapped_object.subject_teacher_academic_class_section_relation.all().filter(
                        staff_id=request.user.id):
                    academic_class_section_mapping_classes.append(mapped_object)



        else:
            academic_class_section_mapping_classes = academic_class_section_mapping.objects.filter(
                year_name=academic_year_id).filter(Q(assistant_teacher=request.user.id) | Q(staff_id=request.user.id))

    raw_list = []
    for obj in academic_class_section_mapping_classes:
        raw_list.append(obj.class_name)
    class_obj_list = list(set(raw_list))

    for obj in class_obj_list:
        modelDict.append(obj.to_dict())
    return modelDict


def todays_date():
    return datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)

def into_datetime(datime):
    return datetime.strptime(datime, '%Y-%m-%d %H:%M:%S').replace(second=0, microsecond=0)

def isnumber(x):
    import numpy
    try:
        return type(numpy.float(x)) == float
    except ValueError:
        return False

def parsePhone(phone):
    """
    Phone numbers can be in many formats
    +011 91 12345678990
    +1234567890
    01234567890
    9849 123456
    Purpose of this function is to scrub it and bring to uniform 10-digit number
    """
    if phone is None: return None
    # scrub the phone number
    phone = phone.replace(' ', '')  # remove spaces
    phone = filter(lambda x: x.isdigit(), phone)  # remove any non-digits

    if len(phone) < 10:
        return None

    if len(phone) > 10:  # not a ten-digit phone number, remove the extra digits
        rev_phone = phone[::-1][0:10]  # 09876543212 => 2123456789 0 is removed
        phone = rev_phone[::-1]  # reverse it back to the original number
    return phone

from django.template.loader import render_to_string, get_template
def send_email_to_parent(to_mail, subject, message , first_name):
    # to = [to_mail]
    try:
        get_admin_email = AuthUser.objects.filter(role=1)[0]
        from_email = get_admin_email.email
    except:
        get_admin_email = settings.EMAIL_HOST_USER
        from_email = get_admin_email

    # from_email = settings.EMAIL_HOST_USER
    to = [to_mail, from_email]

    template = get_template('approve_parent.html')
    html_content = render_to_string('approve_parent.html',{'first_name':first_name})

    try:
        send_mail(subject, message, from_email, to, fail_silently=True , html_message=html_content)
    except:
        messages.warning('Network Error Occur Please Try Later')
    return to_mail

def send_email_to_parent_reject(to_mail, subject, message, first_name , reason):
    # to = [to_mail]

    # get_admin_email = AuthUser.objects.get(role=1)
    # from_email = get_admin_email.email
    try:
        get_admin_email = AuthUser.objects.filter(role=1)[0]
        from_email = get_admin_email.email
    except:
        get_admin_email = settings.EMAIL_HOST_USER
        from_email = get_admin_email
    #from_email = settings.EMAIL_HOST_USER

    to = [to_mail, from_email]

    template = get_template('reject_parent.html')
    html_content = render_to_string('reject_parent.html', {'first_name': first_name,'reason':reason})

    try:
        send_mail(subject, message, from_email, to, fail_silently=True, html_message=html_content)
    except:
        messages.warning('Network Error Occur Please Try Later')
    return to_mail

def get_all_user_permissions(request):

    role_list = request.user.role.all()
    perms = []
    for role_name in role_list:
        role_rec = role.objects.get(name=role_name)
        user_group_perms = Permission.objects.filter(group__name=role_rec.name)
        user_role_perms = role.objects.get(name=role_rec.name)

        group_include_perms = Permission.objects.all().filter(Q(id__in=user_group_perms))
        role_include_perms = user_role_perms.role_permission.all()


        for obj in role_include_perms:
            perms.append(obj.content_type.app_label + '.' + obj.codename)

        for obj in group_include_perms:
            perms.append(obj.content_type.app_label + '.' + obj.codename)

    return  perms

# import datetime
from datetime import date

def current_acd_month_rec(given_month):
    now = datetime.now()
    current_academic_year = academic_year.objects.get(current_academic_year=1)
    ay_start_month = current_academic_year.start_date.month
    ay_end_month = current_academic_year.end_date.month

    flag = True
    counter = 0
    count = ay_start_month
    month_list_id = []
    month_dict = {}
    last_day = monthrange(current_academic_year.start_date.year, count)[1]
    month_dict[ay_start_month] = date(current_academic_year.start_date.year, count, last_day).strftime(
        "%Y-%m-%d")
    month_list_id.append(month_dict)

    sp_flag = False
    while flag:
        if count == ay_end_month:
            flag = False
        else:
            if count != 12:
                if sp_flag == False:
                    count = count + 1
                    if count == now.month:
                        cur_month = date(current_academic_year.start_date.year, count, now.day).strftime(
                            "%Y-%m-%d")
                    else:
                        last_day = monthrange(current_academic_year.start_date.year, count)[1]
                        cur_month = date(current_academic_year.start_date.year, count, last_day).strftime(
                            "%Y-%m-%d")
                    month_dict[count] = cur_month
                else:
                    count = count + 1
                    if count == now.month:
                        cur_month = date(current_academic_year.end_date.year, count, now.day).strftime(
                            "%Y-%m-%d")
                    else:
                        last_day = monthrange(current_academic_year.end_date.year, count)[1]
                        cur_month = date(current_academic_year.end_date.year, count, last_day).strftime(
                            "%Y-%m-%d")

                    month_dict[count] = cur_month
            else:
                sp_flag = True
                count = 1
                if count == now.month:
                    cur_month = date(current_academic_year.end_date.year, count, now.day).strftime(
                        "%Y-%m-%d")
                else:
                    last_day = monthrange(current_academic_year.end_date.year, count)[1]
                    cur_month = date(current_academic_year.end_date.year, count, last_day).strftime(
                        "%Y-%m-%d")
                month_dict[count] = cur_month
    selected_month_rec = month_dict[given_month]
    return selected_month_rec

# from edsys_assesment import settings
def encode_password(pwd):
    cipher = AES.new(settings.PWD_SECRET_KEY, AES.MODE_ECB)  #secret key is from settings file and Aes is for encreption
    encoded_pwd = base64.b64encode(cipher.encrypt(pwd))
    return encoded_pwd

def decode_password(pwd):
    cipher = AES.new(settings.PWD_SECRET_KEY, AES.MODE_ECB)
    decoded_pwd = (cipher.decrypt(base64.b64decode(pwd))).strip()
    return decoded_pwd


def to_dict_list(objects):
    """

    :param objects: object should define to_dict method
    :return: list of to_dict on each object
    """
    res = []
    if isinstance(objects, collections.Iterable):
        for object in objects:
            res.append(object.to_dict())
    elif objects:
        res.append(objects.to_dict())

    return res

class make_incrementor(object):
    count = 0

    def __init__(self, start):
        self.count = start

    def inc(self, jump=1):
        self.count += jump
        return self.count

    def res(self):
        self.count = 0
        return self.count

    def disp(self):
        # self.count = 0
        return self.count


def float_round(num, places = 0, direction = floor):
    return direction(num * (10**places)) / float(10**places)


def compute_current_year(start_date, end_date):

        s_date = datetime.strptime(start_date, "%Y-%m-%d").date()
        e_date = datetime.strptime(end_date, "%Y-%m-%d").date()
        c_date = d.date.today()
        if s_date <= c_date <= e_date:
            return True
        else:
            return False



from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
import StringIO
import os

# def fetch_resources(uri, rel):
#     if os.sep == '\\': # deal with windows and wrong slashes
#         uri2 = os.sep.join(uri.split('/'))
#     else:# else, just add the untouched path.
#        uri2 = uri
#
#     path = '%s%s' % (settings.SITE_ROOT, uri2)
#     return path
#
def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html  = template.render(context_dict)
    result = BytesIO()
    # result = StringIO.StringIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    # pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result, encoding='UTF-8')
    # pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), dest=result, link_callback=fetch_resources)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None


# def render_to_pdf(template_src, request, context_dict={}):
#     template = get_template(template_src)
#     html = template.render(context_dict)
#     result = StringIO.StringIO()
#     # path = get_full_path_x(request)
#     # pdf = pisa.pisaDocument(StringIO.StringIO( html.encode ("ISO-8859-1")), result, path='http://localhost:8000/')
#     # return HttpResponse(result.getvalue(), mimetype='application/pdf')
#     result = StringIO.StringIO()
#     # pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("ISO-8859-1")), dest=result, link_callback=fetch_resources)
#     pdf = pisa.pisaDocument(StringIO.StringIO(html.encode('UTF-8')),
#                             result, encoding='UTF-8', link_callback=fetch_resources)
#     return HttpResponse(result.getvalue(), mimetype='application/pdf')
#
# def get_full_path_x(request):
#     full_path = ('http', ('', 's')[request.is_secure()], '://', request.META['HTTP_HOST'], request.path)
#     return ''.join(full_path)


# def fetch_resources(uri, rel):
#   path = os.path.join(settings.MEDIA_ROOT, uri.replace (settings.MEDIA_URL, ""))
#   return path


# def render_to_pdf( template_src, context_dict={}):
#     now = datetime.now()
#     filename = now.strftime('%Y-%m-%d') + '.pdf'
#     template = get_template(template_src)
#     html = template.render(context_dict)
#     result = StringIO.StringIO()
#
#     pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")),result, path=path)
#
#     if not pdf.err:
#         response = HttpResponse(result.getvalue(), mimetype='application/pdf')
#         response['Content-Disposition'] = 'attachment; filename="'+filename+'"'
#         return response
#     return HttpResponse('We had some errors<pre>%s</pre>' % escape(html))
#
# def get_full_path_x(request):
#     full_path = ('http', ('', 's')[request.is_secure()], '://',
#     request.META['HTTP_HOST'], request.path)
#     return ''.join(full_path)


from django.http import HttpResponse
from django.template.loader import render_to_string
from django.template import RequestContext
from django.conf import settings
# import ho.pisa as pisa
from xhtml2pdf import pisa
import cStringIO as StringIO
import cgi
import os
from django.template.loader import render_to_string
from django.template import RequestContext
from django.template import Context

# def render_to_pdf(template_src, request, context_dict={}):
#     html  = render_to_string(template_src, { 'pagesize' : 'A4', }, context_instance=RequestContext(request))
#     result = StringIO.StringIO()
#     pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), dest=result, link_callback=fetch_resources )
#     if not pdf.err:
#         return HttpResponse(result.getvalue(), mimetype='application/pdf')
#     return HttpResponse('Gremlins ate your pdf! %s' % cgi.escape(html))
#
# def fetch_resources(uri, rel):
#     path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ""))
#
#     return path

# def link_callback(uri, rel):
#     """
#     Convert HTML URIs to absolute system paths so xhtml2pdf can access those
#     resources
#     """
#     # use short variable names
#     sUrl = settings.STATIC_URL      # Typically /static/
#     sRoot = settings.STATIC_ROOT    # Typically /home/userX/project_static/
#     mUrl = settings.MEDIA_URL       # Typically /static/media/
#     mRoot = settings.MEDIA_ROOT     # Typically /home/userX/project_static/media/
#
#     # convert URIs to absolute system paths
#     if uri.startswith(mUrl):
#         path = os.path.join(mRoot, uri.replace(mUrl, ""))
#     elif uri.startswith(sUrl):
#         path = os.path.join(sRoot, uri.replace(sUrl, ""))
#     else:
#         return uri  # handle absolute uri (ie: http://some.tld/foo.png)
#
#     # make sure that file exists
#     if not os.path.isfile(path):
#             raise Exception(
#                 'media URI must start with %s or %s' % (sUrl, mUrl)
#             )
#     return path
#
# def render_to_pdf(template_src, request, context_dict={}):
#     context = {'myvar': 'this is your template context'}
#     # Create a Django response object, and specify content_type as pdf
#     response = HttpResponse(content_type='application/pdf')
#     response['Content-Disposition'] = 'attachment; filename="report.pdf"'
#     # find the template and render it.
#     template = get_template(template_src)
#     html = template.render(context_dict)
#
#     # create a pdf
#     pisaStatus = pisa.CreatePDF(
#        html, dest=response, link_callback=link_callback)
#     # if error then show some funy view
#     if pisaStatus.err:
#        return HttpResponse('We had some errors <pre>' + html + '</pre>')
#     return response

#===============================================================================================================
# def export_Skill_Marks_locked_validation_column_xls(output_file_name, column_names, rows, locked_columns,topic_dict_list,sub_strand_dict_list,max_mark_list):
#     # mark_list = [round(x * 0.1, 1) for x in range(20 * 10)]
#     # absent_list = ['NA', 'Ab']
#     # mark_list.extend(absent_list)
#     # print len(mark_list)
#
#
#     output = BytesIO()
#     workbook = xlsxwriter.Workbook(output)
#     worksheet = workbook.add_worksheet(output_file_name)
#
#     # Create some cell formats with protection properties.
#     unlocked = workbook.add_format({'locked': False,'text_wrap': True})
#     locked = workbook.add_format({'locked': True,'text_wrap': True})
#
#     unlocked.set_text_wrap()
#     # locked.set_text_wrap()
#
#     # Format the worksheet to unlock all cells.
#     worksheet.set_column('A:XDF', None, locked)
#
#     # Turn worksheet protection on.
#     worksheet.protect()
#      #bold = workbook.add_format({'bold': True})
#
#     row_num = 2
#
#     font_style = xlwt.XFStyle()
#
#     font_style.font.bold = True
#
#     for col_num in range(len(column_names)):
#
#         worksheet.write(row_num, col_num, column_names[col_num], locked)
#
#     for row in rows:
#         row_num += 1
#         count = 0
#         for col_num in range(len(row)):
#             worksheet.set_column(col_num, col_num, 18)
#             if col_num in locked_columns:
#                worksheet.write(row_num, col_num, unicode(row[col_num]), locked)
#
#             else:
#                 try:
#                     if row[col_num] != '':
#                             str(row[col_num])
#
#
#                             worksheet.data_validation(row_num, col_num, row_num, col_num, {'validate': 'decimal',
#                                                                                                 'criteria': 'between',
#                                                                                                 'minimum': 0,
#                                                                                                 'maximum':max_mark_list[count],
#                                                                                                 'error_title': 'Input value not valid!',
#                                                                                                 'error_message': 'Enter Value Between 0 to ' + str(max_mark_list[count])})
#                             worksheet.write(row_num, col_num, unicode(row[col_num]), unlocked)
#                             count =count+1
#
#                 except:
#                  worksheet.write(row_num, col_num, unicode(row[col_num]), locked)
#
#     merge_format = workbook.add_format({
#         'align': 'center',})
#
#     count = 0
#     total = 9
#
#     start_column = 9
#     end_colum = 8
#
#     for i in topic_dict_list:
#         count = count + 1
#         div = float(i['subtopic_count'])
#         ans = round(1 / div)
#         if ans == 0:
#             start_column = start_column
#         if count != 1:
#             if div == int(1):
#                 worksheet.write(1, start_column, i['topic_name'])
#             else:
#                 end_colum += int(div)
#                 worksheet.merge_range(1, start_column, 1, end_colum, i['topic_name'], merge_format)
#             start_column = start_column + int(div)
#         else:
#             end_colum+=int(div)
#             if div==int(1):
#                 worksheet.write(1, start_column,i['topic_name'])
#             else:
#                 worksheet.merge_range(1, 9, 1, end_colum, i['topic_name'], merge_format)
#
#
#
#             start_column = start_column + int(div)
#     start_column = 9
#     end_colum = 8
#     for i in sub_strand_dict_list:
#         div = float(i['subtopic_count'])
#         ans = round(1 / div)
#         if ans == 0:
#             start_column = start_column
#         if div == int(1):
#             worksheet.write(0, start_column, i['sub_strand_name'])
#             start_column = start_column + int(div)
#         else:
#             end_colum += int(div)
#             worksheet.merge_range(0, start_column, 0, end_colum, i['sub_strand_name'], merge_format)
#             #worksheet.write(0, start_column, i['sub_strand_name'])
#
#             start_column = start_column + int(div)
#
#
#
#
#     worksheet.set_column(0, 0, 8)
#     worksheet.set_column(4, 4, 20)
#     # worksheet.set_column(5, 5, 10)
#     # worksheet.set_column(6, 6, 10)
#     workbook.close()
#     output.seek(0)
#     response = HttpResponse(output.read(), content_type='application/ms-excel')
#     response['Content-Disposition'] = 'attachment; filename=' + output_file_name + ".xls"
#     return response

def export_Skill_Marks_locked_validation_column_xls(output_file_name, column_names, rows, locked_columns,topic_dict_list,sub_strand_dict_list,max_mark_list):
    # mark_list = [round(x  0.1, 1) for x in range(20  10)]
    # absent_list = ['NA', 'Ab']
    # mark_list.extend(absent_list)
    # print len(mark_list)


    output = BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet(output_file_name)

    # Create some cell formats with protection properties.
    unlocked = workbook.add_format({'locked': False,'text_wrap': True})
    locked = workbook.add_format({'locked': True,'text_wrap': True})

    unlocked.set_text_wrap()
    # locked.set_text_wrap()

    # Format the worksheet to unlock all cells.
    worksheet.set_column('A:XDF', None, locked)

    # Turn worksheet protection on.
    worksheet.protect()
     #bold = workbook.add_format({'bold': True})

    row_num = 2

    font_style = xlwt.XFStyle()

    font_style.font.bold = True

    for col_num in range(len(column_names)):

        worksheet.write(row_num, col_num, column_names[col_num], locked)

    for row in rows:
        row_num += 1
        count = 0
        for col_num in range(len(row)):
            worksheet.set_column(col_num, col_num, 18)
            if col_num in locked_columns:
               worksheet.write(row_num, col_num, unicode(row[col_num]), locked)

            else:
                try:
                    if row[col_num] != '':
                            str(row[col_num])


                            # worksheet.data_validation(row_num, col_num, row_num, col_num, {'validate': 'decimal',
                            #                                                                     'criteria': 'between',
                            #                                                                     'minimum': 0,
                            #                                                                     'maximum':max_mark_list[count],
                            #                                                                     'error_title': 'Input value not valid!',
                            #                                                                     'error_message': 'Enter Value Between 0 to ' + str(max_mark_list[count])})

                            data = {1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F', 7: 'G', 8: 'H', 9: 'I', 10: 'J', 11: 'K',
                             12: 'L', 13: 'M', 14: 'N', 15: 'O', 16: 'P', 17: 'Q', 18: 'R', 19: 'S', 20: 'T', 21: 'U',
                             22: 'V', 23: 'W', 24: 'X', 25: 'Y', 26: 'Z'}


                            from assessment.models import assessment_configration_master
                            formula = ''
                            i = 1
                            for obj in assessment_configration_master.objects.all():
                                if i == 1:
                                    formula += '=OR(AND('+data[col_num+1]+str(row_num+1)+'>=0,'+data[col_num+1]+str(row_num+1)+'<='+str(max_mark_list[count])+'),'+data[col_num+1]+str(row_num+1)+'="'+str(obj.short_name)+'")'
                                else:
                                    formula += '+OR(AND('+data[col_num+1]+str(row_num+1)+'>=0,'+data[col_num+1]+str(row_num+1)+'<='+str(max_mark_list[count])+'),'+data[col_num+1]+str(row_num+1)+'="'+str(obj.short_name)+'")'
                                i = i + 1

                            worksheet.data_validation(row_num, col_num, row_num, col_num, {'validate': 'custom',
                                                                                            'value': formula,
                                                                                            'error_title': 'Input value not valid!',
                                                                                            'error_message': 'Enter Value Between 0 to ' + str(
                                                                                               max_mark_list[count])
                                                                                           })
                            worksheet.write(row_num, col_num, unicode(row[col_num]), unlocked)
                            count =count+1

                except Exception as e:
                 worksheet.write(row_num, col_num, unicode(row[col_num]), locked)

    merge_format = workbook.add_format({
        'align': 'center',})

    count = 0
    total = 9

    start_column = 9
    end_colum = 8

    for i in topic_dict_list:
        count = count + 1
        div = float(i['subtopic_count'])
        ans = round(1 / div)
        if ans == 0:
            start_column = start_column
        if count != 1:
            if div == int(1):
                worksheet.write(1, start_column, i['topic_name'])
            else:
                end_colum += int(div)
                worksheet.merge_range(1, start_column, 1, end_colum, i['topic_name'], merge_format)
            start_column = start_column + int(div)
        else:
            end_colum+=int(div)
            if div==int(1):
                worksheet.write(1, start_column,i['topic_name'])
            else:
                worksheet.merge_range(1, 9, 1, end_colum, i['topic_name'], merge_format)



            start_column = start_column + int(div)
    start_column = 9
    end_colum = 8
    for i in sub_strand_dict_list:
        div = float(i['subtopic_count'])
        ans = round(1 / div)
        if ans == 0:
            start_column = start_column
        if div == int(1):
            worksheet.write(0, start_column, i['sub_strand_name'])
            start_column = start_column + int(div)
        else:
            end_colum += int(div)
            worksheet.merge_range(0, start_column, 0, end_colum, i['sub_strand_name'], merge_format)
            #worksheet.write(0, start_column, i['sub_strand_name'])

            start_column = start_column + int(div)




    worksheet.set_column(0, 0, 8)
    worksheet.set_column(4, 4, 20)
    # worksheet.set_column(5, 5, 10)
    # worksheet.set_column(6, 6, 10)
    workbook.close()
    output.seek(0)
    response = HttpResponse(output.read(), content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=' + output_file_name + ".xls"
    return response


def export_wraped_column_xls(output_file_name, column_names, rows):
    output = BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet(output_file_name)
    unlocked = workbook.add_format({'locked': False, 'text_wrap': True})
    locked = workbook.add_format({'locked': True, 'text_wrap': True})
    worksheet.set_column('A:XDF', None, unlocked)
    row_num = 0
    for col_num in range(len(column_names)):
        worksheet.set_column(col_num, col_num, 18)
        worksheet.write(row_num, col_num, column_names[col_num], unlocked)
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
                worksheet.write(row_num, col_num, unicode(row[col_num]), unlocked)

    workbook.close()
    output.seek(0)
    response = HttpResponse(output.read(), content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=' + output_file_name + ".xls"

    return response




def export_test(output_file_name, column_names, rows, locked_columns,max_mark_list):
    # mark_list = [round(x  0.1, 1) for x in range(20  10)]
    # absent_list = ['NA', 'Ab']
    # mark_list.extend(absent_list)
    # print len(mark_list)


    output = BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet(output_file_name)

    # Create some cell formats with protection properties.
    unlocked = workbook.add_format({'locked': False,'text_wrap': True})
    locked = workbook.add_format({'locked': True,'text_wrap': True})

    unlocked.set_text_wrap()
    # locked.set_text_wrap()

    # Format the worksheet to unlock all cells.
    worksheet.set_column('A:XDF', None, locked)

    # Turn worksheet protection on.
    worksheet.protect()
     #bold = workbook.add_format({'bold': True})

    row_num = 2

    font_style = xlwt.XFStyle()

    font_style.font.bold = True

    for col_num in range(len(column_names)):

        worksheet.write(row_num, col_num, column_names[col_num], locked)

    for row in rows:
        row_num += 1
        count = 0
        for col_num in range(len(row)):
            worksheet.set_column(col_num, col_num, 18)
            if col_num in locked_columns:
               worksheet.write(row_num, col_num, unicode(row[col_num]), locked)

            else:
                try:
                    if row[col_num] != '':
                            str(row[col_num])


                            # worksheet.data_validation(row_num, col_num, row_num, col_num, {'validate': 'decimal',
                            #                                                                     'criteria': 'between',
                            #                                                                     'minimum': 0,
                            #                                                                     'maximum':max_mark_list[count],
                            #                                                                     'error_title': 'Input value not valid!',
                            #                                                                     'error_message': 'Enter Value Between 0 to ' + str(max_mark_list[count])})

                            data = {1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F', 7: 'G', 8: 'H', 9: 'I', 10: 'J', 11: 'K',
                             12: 'L', 13: 'M', 14: 'N', 15: 'O', 16: 'P', 17: 'Q', 18: 'R', 19: 'S', 20: 'T', 21: 'U',
                             22: 'V', 23: 'W', 24: 'X', 25: 'Y', 26: 'Z'}


                            from assessment.models import assessment_configration_master
                            formula = ''
                            i = 1
                            for obj in assessment_configration_master.objects.all():
                                if i == 1:
                                    formula += '=OR(AND('+data[col_num+1]+str(row_num+1)+'>=0,'+data[col_num+1]+str(row_num+1)+'<='+str(max_mark_list[count])+'),'+data[col_num+1]+str(row_num+1)+'="'+str(obj.short_name)+'")'
                                else:
                                    formula += '+OR(AND('+data[col_num+1]+str(row_num+1)+'>=0,'+data[col_num+1]+str(row_num+1)+'<='+str(max_mark_list[count])+'),'+data[col_num+1]+str(row_num+1)+'="'+str(obj.short_name)+'")'
                                i = i + 1

                            worksheet.data_validation(row_num, col_num, row_num, col_num, {'validate': 'custom',
                                                                                            'value': formula,
                                                                                            'error_title': 'Input value not valid!',
                                                                                            'error_message': 'Enter Value Between 0 to ' + str(
                                                                                               max_mark_list[count])
                                                                                           })
                            worksheet.write(row_num, col_num, unicode(row[col_num]), unlocked)
                            count =count+1

                except Exception as e:
                 worksheet.write(row_num, col_num, unicode(row[col_num]), locked)


    worksheet.set_column(0, 0, 8)
    worksheet.set_column(4, 4, 20)
    # worksheet.set_column(5, 5, 10)
    # worksheet.set_column(6, 6, 10)
    workbook.close()
    output.seek(0)
    response = HttpResponse(output.read(), content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=' + output_file_name + ".xls"
    return response

compare_by_element = lambda a, b: len(a) == len(b) and len(a) == sum([1 for i, j in zip(a, b) if i == j])
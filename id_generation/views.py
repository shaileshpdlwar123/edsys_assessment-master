from django.shortcuts import render, redirect
from registration.models import *
from id_generation.models import *
from django.http import HttpResponse
from reportlab.lib.utils import ImageReader
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import inch
from reportlab.lib.colors import white, red, black, blue, brown, royalblue, rosybrown, \
    purple, Color, lightblue, fidblue, ReportLabFidBlue, ReportLabLightBlue, ReportLabBluePCMYK
from django.contrib import messages
from registration.decorators import user_login_required, user_has_permission, the_decorator, decorator, \
    user_permission_required
from masters.utils import todays_date, get_all_user_permissions, current_class_mapped_sections, \
    current_academic_year_mapped_classes
from medical.models import student_medical
import textwrap


# This View is for loading class and sections to the first page of ID module
@user_login_required
def IdMenu(request):
    perms = [] #get_all_user_permissions(request)
    return render(request, "id_menu.html", {'perms': list(perms)})


@user_login_required
def ViewStaffList(request):
    user_list = AuthUser.objects.exclude(role__name='Parent').exclude(is_superuser=True).exclude(is_active=False)
    return render(request, "view_staff_list.html", {'staff_list': user_list})


def StaffIdGenerate(request, usr_id):
    user_detail = AuthUser.objects.get(id=usr_id)
    school_rec = school_details.objects.get(id=1)
    department_list = department_details.objects.all()  # change department_list to department_recs

    if user_detail.staff_relation.all():
        if user_detail.staff_relation.all()[0].birth_date:
            user_detail.staff_relation.all()[0].birth_date = user_detail.staff_relation.all()[0].birth_date.strftime(
                '%Y-%m-%d')

    return render(request, "staff_id_generate.html",
                  {'staff_detail': user_detail, 'school_detail': school_rec, 'department_list': department_list})


@user_login_required
def IdGenerator(request):
    # all_class_recs = class_details.objects.all()
    # all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    all_transport_rec = student_details.objects.all()
    current_academic_year = None
    selected_year_id = None



    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        all_class_recs = current_academic_year_mapped_classes(selected_year_id, request.user)
    except:
        messages.error(request, "Please add academic year first.")
        return render(request, "id_generator.html",
                      {'class_list': all_class_recs,  "year_list": all_academicyr_recs,
                       'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                       'all_transport_rec': all_transport_rec})

    return render(request, "id_generator.html",
                  {'class_list': all_class_recs,  "year_list": all_academicyr_recs,
                   'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                   'all_transport_rec': all_transport_rec})

@user_login_required
def ViewStudentTemplate(request):
    temp_one = temp_two = ''
    template = id_student_selected_template.objects.get().template

    if template == 'temp_one':
        temp_one = 'checked'
    else:
        temp_two = 'checked'

    return render(request, "selected_student_template.html",{'temp_two':temp_two,'temp_one':temp_one})

@user_login_required
def UpdatedStudentTemplate(request):
    template = request.POST.get('template')
    id_student_selected_template.objects.filter().update(template=template)

    return redirect('/school/view_student_template/')


# This view is for displaying all Students list whom ID we want to generate
def load_data(val_dict, request):
    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    all_transport_rec = student_details.objects.all()

    selected_class_rec = ''
    selected_class_name = ''
    selected_section_rec = ''
    selected_section_name = ''

    selected_transport_rec = ''
    selected_transport_name = ''
    filtered_students = ''

    selected_year_id = val_dict.get('year_name')  # It will be used in HTML page as a value of selected Class
    selected_year_rec = academic_year.objects.get(id=selected_year_id)
    selected_year_name = selected_year_rec.year_name

    selected_transport_id = val_dict.get('transport_type')

    # if (selected_transport_id == "own"):
    selected_transport_name = selected_transport_id.title()
    # else:
    #     selected_transport_name = "School"


    selected_class_id = val_dict.get('class_name')  # It will be used in HTML page as a value of selected Class

    if selected_class_id:
        selected_class_rec = class_details.objects.get(id=selected_class_id)
        selected_class_name = selected_class_rec.class_name
        all_section_recs = current_class_mapped_sections(selected_year_rec, selected_class_rec, request.user)

    selected_section_id = val_dict.get('section_name')  # It will be used in HTML page as a value of selected Section
    if selected_section_id:
        selected_section_rec = sections.objects.get(id=selected_section_id)
        selected_section_name = selected_section_rec.section_name

    all_class_recs = current_academic_year_mapped_classes(selected_year_rec, request.user)


    if selected_class_id == '' and selected_section_id == '' and selected_transport_id:

        filtered_students = student_details.objects.filter(transport=selected_transport_id, is_active=True,
                                                           joining_date__lte=todays_date())




    elif selected_class_id and selected_section_id and selected_transport_id :
        year_class_section_obj = academic_class_section_mapping.objects.filter(year_name=selected_year_id,
                                                                               section_name=selected_section_id,
                                                                               class_name=selected_class_id)
        filtered_students = student_details.objects.filter(academic_class_section__in=year_class_section_obj,
                                                           is_active=True, joining_date__lte=todays_date(),transport=selected_transport_id)



    elif selected_class_id and selected_section_id and selected_transport_id == '':

        year_class_section_obj = academic_class_section_mapping.objects.filter(year_name=selected_year_id,
                                                                               section_name=selected_section_id,
                                                                               class_name=selected_class_id)
        filtered_students = student_details.objects.filter(academic_class_section__in=year_class_section_obj,
                                                           is_active=True, joining_date__lte=todays_date())

    elif selected_class_id == '' and selected_section_id and selected_transport_id == '':
        year_class_section_obj = academic_class_section_mapping.objects.filter(year_name=selected_year_id,
                                                                               section_name=selected_section_id)
        filtered_students = student_details.objects.filter(academic_class_section__in=year_class_section_obj,
                                                           is_active=True, joining_date__lte=todays_date())

    elif selected_class_id and selected_section_id == '' and selected_transport_id == '':
        year_class_section_obj = academic_class_section_mapping.objects.filter(year_name=selected_year_id,
                                                                               class_name=selected_class_id)
        filtered_students = student_details.objects.filter(academic_class_section__in=year_class_section_obj,
                                                           is_active=True, joining_date__lte=todays_date())


    elif selected_class_id == "" and selected_section_id and selected_transport_id:

        year_class_section_obj = academic_class_section_mapping.objects.filter(year_name=selected_year_id,
                                                                               section_name=selected_section_id,)

        filtered_students = student_details.objects.filter(academic_class_section__in=year_class_section_obj,

                                                           is_active=True, joining_date__lte=todays_date(),
                                                           transport=selected_transport_id)

    elif selected_class_id and selected_section_id=="" and selected_transport_id:

        year_class_section_obj = academic_class_section_mapping.objects.filter(year_name=selected_year_id,
                                                                               class_name=selected_class_id, )

        filtered_students = student_details.objects.filter(academic_class_section__in=year_class_section_obj,

                                                           is_active=True, joining_date__lte=todays_date(),
                                                           transport=selected_transport_id)

    else:

        year_class_section_obj = academic_class_section_mapping.objects.filter(year_name=selected_year_id)

        filtered_students = student_details.objects.filter(academic_class_section__in=year_class_section_obj,
                                                           is_active=True,
                                                           joining_date__lte=todays_date())



    #     Passing values to the HTML page through form_vals
    form_vals = {
        'student_list': filtered_students,
        'Selected_Section_Id': selected_section_id,
        'selected_section_name': selected_section_name,
        'selected_section': selected_section_rec,
        'section_list': all_section_recs,
        'selected_transport_name': selected_transport_name,

        'Selected_Class_Id': selected_class_id,
        'selected_class_name': selected_class_name,
        'selected_class': selected_class_rec,
        'class_list': all_class_recs,

        'Selected_Year_Id': selected_year_id,
        'selected_year_name': selected_year_name,
        'selected_year': selected_year_rec,
        'year_list': all_academicyr_recs,

        'selected_transport_id': selected_transport_id,
        'selected_transport_rec': selected_transport_rec,
        'selected_transport_name': selected_transport_name,
        'all_transport_rec': all_transport_rec,

    }

    return form_vals


@user_login_required
def IdGeneratorList(request):
    val_dict = request.POST
    form_vals = {}

    if request.method == 'POST':
        request.session['val_dict'] = val_dict
        form_vals = load_data(val_dict, request)
    else:
        form_vals = load_data(request.session.get('val_dict'), request)
        # return redirect('/school/id_generator/')

    return render(request, "id_generator.html", form_vals)


@user_login_required
def IdGenerate(request, std_id):
    student_rec = student_details.objects.get(id=std_id)
    blood_group = ''
    parent_name = ''

    id_flag = 1

    if str(student_rec.transport).lower() == 'school':
        template_type = 'school'

        # front_file_name =settings.STATICFILES_DIRS[0] + str('/id_images/Template_school.png')
    else:
        template_type = 'own'
        front_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/Template_own.png')

    if student_medical.objects.filter(student__id=student_rec.id).exists():
        blood_group = student_medical.objects.get(student__id=student_rec.id).blood_group

    if student_rec.parent:
        parent_name = student_rec.parent.user.get_pura_name()

    school_rec = school_details.objects.get(id=1)
    school_name = str(school_rec.school_name).split(' ')
    student_rec.birth_date = student_rec.birth_date.strftime('%Y-%m-%d')

    student_template = id_student_selected_template.objects.get().template
    # student_template = id_student_selected_template.objects.filter()



    if student_template == 'temp_two':
        id_flag = 2



    return render(request, "id_generate.html",
                  {'student_detail': student_rec, 'school_detail': school_rec, 'template_type': template_type,
                   'blood_group': blood_group, 'parent_name': parent_name,'school_name':school_name,'id_flag':id_flag})


# This view is for Generating PDF of Multiple selected ID cards
# def MultipleStudentIDlist(request):
#     if request.method == 'POST':
#
#         selected_student_ids = request.POST.getlist('check')  # Getting Ids  of selected students
#
#         response = HttpResponse(content_type='application/pdf')
#         response['Content-Disposition'] = 'attachment; filename = "IDCard.pdf"'
#         p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
#
#         for selected_student_id in selected_student_ids:
#             student_rec = student_details.objects.get(id=selected_student_id)
#             if student_rec.gender == True:
#                 gender = 'Female'
#             else:
#                 gender = 'Male'
#             student_rec.birth_date = student_rec.birth_date.strftime(
#                 '%Y-%m-%d')  # It will format the birth date of the student over the PDF else birth date will be printed in Default format
#
#             p.setPageSize((400, 300))
#
#             school_rec = school_details.objects.get(
#                 id=1)  # Here 1 is the Id of first school and Only one school will be present
#             academicYear = str(student_rec.academic_class_section.year_name.year_name)
#             student_enrollment_num = str(student_rec.student_code)
#             if school_rec.logo:
#                 logo = ImageReader(school_rec.logo)  # Getting edsys_assesment logo
#                 p.drawImage(logo, 3, 225, width=1 * inch, height=1 * inch, mask='auto')
#
#             p.setFillColor(red)
#             p.setFont('Times-Bold', 18)
#             p.drawString(90, 250, school_rec.school_name)
#             #  If student photo will be present then student photo will be printed else blank rectangular box will be there
#
#             p.setFillColor(black)
#             if student_rec.photo:
#                 photo = ImageReader(student_rec.photo)
#                 p.drawImage(photo, 30, 95, width=1.2 * inch, height=1.3 * inch, mask='auto')
#             else:
#                 p.rect(30, 95, 1 * inch, 1.2 * inch)
#
#             p.setFont('Helvetica-Bold', 16)
#             p.drawString(135, 195, student_rec.first_name + ' ' + student_rec.last_name)
#             p.setFont('Helvetica-Bold', 14)
#             p.drawString(135, 175, 'Student Code:  ' + student_enrollment_num)
#
#             p.drawString(135, 155, 'Class:  ' + student_rec.academic_class_section.class_name.class_name)
#             p.drawString(265, 155, 'Section:  ' + student_rec.academic_class_section.section_name.section_name)
#             p.drawString(135, 135, 'DOB:  ' + student_rec.birth_date)
#             p.drawString(135, 115, 'Gender:  ' + gender)
#             p.drawString(135, 95, 'Parent Name:  ' + student_rec.father_name + ' ' + student_rec.last_name)
#             p.drawString(135, 75, 'Contact No:  ' + student_rec.parent.contact_num if student_rec.parent else '')
#             p.drawString(30, 197, academicYear)
#
#             p.setFont('Times-Bold', 10)
#             p.drawString(30, 55, 'Principal Signature')
#             p.setFillColor(red)
#             p.rect(0 * inch, 0 * inch, 10 * inch, 0.6 * inch, fill=1, stroke=False)
#             p.showPage()  # Breaking the page and further data will be printed on next page
#
#             # p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
#             p.setFillColor(red)
#             p.setFont('Times-Bold', 18)
#
#             if school_rec.logo:
#                 p.drawImage(logo, 3, 225, width=1 * inch, height=1 * inch, mask='auto')
#             p.drawString(90, 250, school_rec.school_name)
#
#             # p.drawImage(logo, 15, 220   ,  width = 6*inch, height = 1.2*inch, mask = 'auto')
#             p.setFillColor(black)
#             p.setFont('Helvetica-Bold', 18)
#             p.drawString(90, 175, 'If Lost/Found Please Contact :  ')
#             p.drawString(90, 150, 'Tel No:  ' + school_rec.contact_num)
#             p.drawString(90, 125, 'Email:  ' + school_rec.email)
#             p.setFillColor(red)
#             p.rect(0, 0, 9 * inch, 0.6 * inch, fill=1, stroke=False)
#
#             p.setFillColor(white)  # for web site color setting to white
#             p.setStrokeColor(white)
#             p.setFont("Helvetica-Bold", 16)
#             p.drawString(65, 8, 'Website :  ' + school_rec.website)
#             p.showPage()  # Breaking the page and further data will be printed on next page
#         p.save()  # saving the PDF once all done
#     return response


from reportlab.pdfbase.pdfmetrics import stringWidth
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont



@user_login_required
def PrintID(request, std_id):

    if id_student_selected_template.objects.get().template == 'temp_one':
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename = "IDCard.pdf"'
        p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
        p.setPageSize((300, 450))
        student_rec = student_details.objects.get(id=std_id)
        blood_group = ''

        if str(student_rec.transport).lower() == 'school':
            front_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/Template_school.png')
        else:
            front_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/Template_own.png')
        back_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/Template_back.png')
        contact_png = settings.STATICFILES_DIRS[0] + str('/id_images/contact.png')
        # file_name =settings.STATICFILES_DIRS[0] + str('/Student_Id_Card_template.png')

        student_rec.birth_date = student_rec.birth_date.strftime(
            '%Y-%m-%d')  # It will format the birth date of the student over the PDF else birth date will be printed in Default format

        school_rec = school_details.objects.get(
            id=1)  # Here 1 is the Id of first school and Only one school will be present
        academicYear = str(student_rec.academic_class_section.year_name.year_name)

        front_backgroung = ImageReader(front_file_name)  # Getting schoolzen logo
        # p.drawImage(logo, -18, -2, width=8.5 * inch, height=6.3 * inch, mask='auto')

        if student_rec.photo:
            photo = ImageReader(student_rec.photo)
            p.drawImage(photo, 70, 250, width=2 * inch, height=2 * inch, mask='auto')
        p.drawImage(front_backgroung, -18, -2, width=4.5 * inch, height=6.3 * inch, mask='auto')

        if school_rec.logo:
            logo = ImageReader(school_rec.logo)  # Getting schoolzen logo
            p.drawImage(logo, 5, 8, width=0.6 * inch, height=0.6 * inch, mask='auto')

        p.setFont('Helvetica-Bold', 18)
        p.setFillColor('#306598')
        p.drawString((300 / 2.0) - (len(str(student_rec.odoo_id)) * 6), 215, str(student_rec.odoo_id))
        # p.drawString(110, 215, str(student_rec.odoo_id))

        p.setFont('Helvetica-Bold', 16)
        p.setFillColor('#306598')
        student_name = student_rec.get_all_name()

        count = 0
        y_cod = 215
        if len(student_name) > 25:
            wrap_text = textwrap.wrap(student_name, width=25)
            total_index = len(wrap_text)
            while (total_index > 0):
                p.drawString(30, y_cod - 20, wrap_text[count])
                total_index = total_index - 1
                count = count + 1
                y_cod = y_cod - 20
                # p.drawString(30, 195, wrap_text[1])
        else:
            p.drawString(30, 195, str(student_name))

        p.setFont('Helvetica-Bold', 12)
        p.drawString(30, 155, 'Academic Year:')

        p.setFillColor('#636565')
        p.drawString(130, 155, academicYear)

        p.setFillColor('#306598')
        p.drawString(30, 130, 'Class:')

        p.setFillColor('#636565')
        p.drawString(75, 130, str(student_rec.academic_class_section.class_name.class_name) + ',' + str(
            student_rec.academic_class_section.section_name.section_name))

        if student_medical.objects.filter(student__id=student_rec.id).exists():
            blood_group = student_medical.objects.get(student__id=student_rec.id).blood_group

        text_len = len(str(student_rec.academic_class_section.class_name.class_name) + ',' + str(
            student_rec.academic_class_section.section_name.section_name))
        if text_len >= 10:
            bg_x = (75 + (text_len) * 8)
        else:
            bg_x = (75 + (text_len) * 10)

        p.setFillColor('#306598')
        p.drawString(bg_x, 130, 'Blood Group:')
        p.setFillColor('#636565')
        bg_x = bg_x + (len('Blood Group:') * 7)
        p.drawString(bg_x, 130, str(blood_group))

        p.setFillColor('#306598')
        p.drawString(30, 105, 'Parent Name:')
        p.setFillColor('#636565')

        if student_rec.parent:
            p.drawString(130, 105, str(student_rec.parent.user.get_pura_name()))

        p.setFillColor('#306598')
        p.drawString(30, 80, 'Emergency      :')
        p.drawImage(contact_png, 97, 75, width=0.2 * inch, height=0.2 * inch, mask='auto')

        if student_rec.emergency_contact_num != None and student_rec.emergency_contact_num != '':
            p.setFillColor('#636565')
            p.drawString(130, 80, str(student_rec.emergency_contact_num))

        p.setFillColor(white)
        p.drawString(130, 18, 'Transport Type:   ' + str(student_rec.transport).title())

        if str(student_rec.transport).lower() == 'school':
            p.setFillColor('#306598')
            p.setFont('Helvetica-Bold', 9)
            p.drawString(85, 5, 'Pickup Bus:   ' + str(student_rec.bus_no))
            p.drawString(185, 5, 'Drop Bus:   ' + str(student_rec.drop_bus_no))
        p.showPage()  # Breaking the page and further data will be printed on next page

        back_backgroung = ImageReader(back_file_name)  # Getting schoolzen logo

        p.setPageSize((300, 450))
        p.drawImage(back_backgroung, -18, -2, width=4.5 * inch, height=6.3 * inch, mask='auto')

        if school_rec.logo:
            logo = ImageReader(school_rec.logo)  # Getting schoolzen logo
            p.drawImage(logo, 110, 300, width=1.2 * inch, height=1.2 * inch, mask='auto')

        p.setFont('Times-Bold', 18)

        p.setFillColor('#010101')
        p.drawString(35, 260, str(school_rec.school_name))

        p.setFillColor('#262626')
        p.setFont('Helvetica-Bold', 10)
        p.drawString(20, 245, '------------------------------------------------------------------------------')

        if len(str(school_rec.school_address)) <= 45:
            p.drawString(30, 230, str(school_rec.school_address))
            p.drawString(30, 210,
                         'City- ' + str(school_rec.city) + ', State- ' + str(school_rec.state) + ' -' + str(
                             school_rec.pin))
            p.drawString(30, 190, str(school_rec.email))
            p.drawString(30, 170, str(school_rec.website))
            p.drawString(30, 150, 'PH: ' + str(school_rec.contact_num))
        elif len(str(school_rec.school_address)) <= 90:
            wrap_text = textwrap.wrap(str(school_rec.school_address), width=45)
            p.drawString(30, 230, wrap_text[0])
            p.drawString(30, 210, wrap_text[1])
            p.drawString(30, 190,
                         'City- ' + str(school_rec.city) + ', State- ' + str(school_rec.state) + ' -' + str(
                             school_rec.pin))
            p.drawString(30, 170, str(school_rec.email))
            p.drawString(30, 150, str(school_rec.website))
            p.drawString(30, 130, 'PH: ' + str(school_rec.contact_num))
        else:
            wrap_text = textwrap.wrap(str(school_rec.school_address), width=45)
            p.drawString(30, 230, wrap_text[0])
            p.drawString(30, 210, wrap_text[1])
            p.drawString(30, 190, wrap_text[2])
            p.drawString(30, 170,
                         'City- ' + str(school_rec.city) + ', State- ' + str(school_rec.state) + ' -' + str(
                             school_rec.pin))
            p.drawString(30, 150, str(school_rec.email))
            p.drawString(30, 130, str(school_rec.website))
            p.drawString(30, 110, 'PH: ' + str(school_rec.contact_num))
        p.showPage()  # Breaking the page and further data will be printed on next page
        p.save()
        # return response
    else:
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename = "IDCard.pdf"'
        p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
        p.setPageSize((310, 450))
        student_rec = student_details.objects.get(id=std_id)
        TrajanPro =settings.BASE_DIR + '/TrajanPro-Bold.ttf'
        pdfmetrics.registerFont(TTFont('TrajanPro-Bold', TrajanPro))
        front_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/front_blank_page_3.png')
        back_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/back_id.jpg')
        contact_png = settings.STATICFILES_DIRS[0] + str('/id_images/phone-contact.png')

        student_rec.birth_date = student_rec.birth_date.strftime(
            '%Y-%m-%d')  # It will format the birth date of the student over the PDF else birth date will be printed in Default format

        school_rec = school_details.objects.get(
            id=1)  # Here 1 is the Id of first school and Only one school will be present
        academicYear = str(student_rec.academic_class_section.year_name.year_name)

        front_backgroung = ImageReader(front_file_name)  # Getting schoolzen logo

        if student_rec.photo:
            photo = ImageReader(student_rec.photo)
            p.drawImage(photo, 90, 200, width=1.8 * inch, height=1.8 * inch, mask='auto')
        p.drawImage(front_backgroung, -0, -2, width=4.5 * inch, height=6.3 * inch, mask='auto')

        if school_rec.logo:
            logo = ImageReader(school_rec.logo)  # Getting schoolzen logo
            p.drawImage(logo, 29, 345, width=0.9 * inch, height=1.1 * inch, mask='auto')

        # p.setFont('Times-Bold', 22)
        p.setFont('TrajanPro-Bold', 22)
        p.setFillColor(white)

        y_name = 410
        for name in str(school_rec.school_name).split(' '):
            p.drawString(100, y_name, name.upper())
            y_name -= 20

        p.setFillColor(black)

        p.setFont('Helvetica-Bold', 14)
        student_name = student_rec.get_all_name()

        count = 0
        y_cod = 190
        x_cod = 20
        x_diff = 18
        if len(student_name) > 35:
            wrap_text = textwrap.wrap(student_name, width=35)
            total_index = len(wrap_text)
            while (total_index > 0):
                p.drawString(x_cod, y_cod - x_diff, wrap_text[count])
                total_index = total_index - 1
                count = count + 1
                y_cod = y_cod - x_diff
        else:
            y_cod -= x_diff

            if student_name.split('  '):
                student_name = student_name.replace('  ', ' ')

            p.drawString(x_cod, y_cod, str(student_name))

        p.setFont('Helvetica-Bold', 12)
        y_cod -= x_diff
        p.drawString(x_cod, y_cod, 'Student ID          :   '+str(student_rec.odoo_id))

        y_cod -= x_diff
        p.drawString(x_cod, y_cod, 'Class                  :   '+str(student_rec.academic_class_section.class_name.class_name) + ',  Section : ' + str(
            student_rec.academic_class_section.section_name.section_name))

        y_cod -= x_diff
        p.drawString(x_cod, y_cod, 'Academic Year  :   '+academicYear)

        y_cod -= x_diff


        if student_rec.parent:
            p.drawString(x_cod, y_cod, 'Parent Name      :   '+str(student_rec.parent.user.get_pura_name()))
        else:
            p.drawString(x_cod, y_cod, 'Parent Name      :   ')
        y_cod -= x_diff

        p.drawImage(contact_png, 87, y_cod-3, width=0.2 * inch, height=0.2 * inch, mask='auto')

        if student_rec.emergency_contact_num != None and student_rec.emergency_contact_num != '':
            p.drawString(x_cod, y_cod, 'Emergency         :   '+str(student_rec.emergency_contact_num))
        else:
            p.drawString(x_cod, y_cod, 'Emergency         :  ')

        y_cod -= x_diff

        p.drawString(x_cod, y_cod, 'Transport Type  :   ' + str(student_rec.transport).title())

        y_cod -= x_diff

        if str(student_rec.transport).lower() == 'school':
            p.drawString(x_cod, y_cod,
                         'Pickup                :   ' + str(
                             student_rec.pick_up_point).strip() if student_rec.pick_up_point and str(
                             student_rec.pick_up_point) != 'None' else 'Pickup                :   ')

            y_cod -= x_diff
            p.drawString(x_cod, y_cod,
                         'Drop off              :   ' + str(
                             student_rec.drop_off_point).strip() if student_rec.drop_off_point and str(
                             student_rec.pick_up_point) != 'None'  else 'Drop off              :   ')
        p.showPage()  # Breaking the page and further data will be printed on next page

        back_backgroung = ImageReader(back_file_name)  # Getting schoolzen logo

        p.setPageSize((310, 450))
        p.drawImage(back_backgroung, -0, -2, width=4.5 * inch, height=6.3 * inch, mask='auto')

        if school_rec.logo:
            logo = ImageReader(school_rec.logo)  # Getting schoolzen logo
            p.drawImage(logo, 110, 350, width=1.2 * inch, height=1.2 * inch, mask='auto')


        if school_rec.principal_qr_code:
            qr_code = ImageReader(school_rec.principal_qr_code)  # Getting schoolzen logo
            p.drawImage(qr_code, 30, 290, width=0.8* inch, height=0.8 * inch, mask='auto')

        y_axis = 270

        p.drawString(x_cod, y_axis, 'This card is the property of school.')
        p.drawString(x_cod, y_axis-15, 'If found, please return to')

        p.setFont('TrajanPro-Bold', 16)

        y_axis -= 40
        p.drawString(x_cod, y_axis, str(school_rec.school_name))

        p.setFont('Helvetica-Bold', 10)
        y_axis -= 1

        if len(str(school_rec.school_address)) <= 45:
            y_axis -= 20
            p.drawString(x_cod, y_axis, str(school_rec.school_address))
            y_axis -= 20
            p.drawString(x_cod, y_axis,'City- ' + str(school_rec.city) + ', State- ' + str(school_rec.state) + ' -' + str(school_rec.pin))
            y_axis -= 20
            p.drawString(x_cod, y_axis, 'PH: ' + str(school_rec.contact_num))
            y_axis -= 20
            p.drawString(x_cod, y_axis, str(school_rec.email))
            y_axis -= 20
            p.drawString(x_cod, y_axis, str(school_rec.website))

        elif len(str(school_rec.school_address)) <= 90:
            wrap_text = textwrap.wrap(str(school_rec.school_address), width=55)
            y_axis -= 20
            p.drawString(x_cod, y_axis, wrap_text[0])
            try:
                if wrap_text[1]:
                    y_axis -= 20
                    p.drawString(x_cod, y_axis, wrap_text[1])
            except:
                pass
            y_axis -= 20
            p.drawString(x_cod, y_axis,'City- ' + str(school_rec.city) + ', State- ' + str(school_rec.state) + ' -' + str(school_rec.pin))
            y_axis -= 20
            p.drawString(x_cod, y_axis, 'PH: ' + str(school_rec.contact_num))
            y_axis -= 20
            p.drawString(x_cod, y_axis, str(school_rec.email))
            y_axis -= 20
            p.drawString(x_cod, y_axis, str(school_rec.website))
            # y_axis -= 20
            # p.drawString(x_cod, y_axis, 'PH: ' + str(school_rec.contact_num))
        else:
            wrap_text = textwrap.wrap(str(school_rec.school_address), width=45)
            y_axis -= 20
            p.drawString(x_cod, y_axis, wrap_text[0])
            y_axis -= 20
            p.drawString(x_cod, y_axis, wrap_text[1])
            y_axis -= 20
            p.drawString(x_cod, y_axis, wrap_text[2])
            y_axis -= 20
            p.drawString(x_cod, y_axis,'City- ' + str(school_rec.city) + ', State- ' + str(school_rec.state) + ' -' + str(school_rec.pin))
            y_axis -= 20
            p.drawString(x_cod, y_axis, 'PH: ' + str(school_rec.contact_num))
            y_axis -= 20
            p.drawString(x_cod, y_axis, str(school_rec.email))
            y_axis -= 20
            p.drawString(x_cod, y_axis, str(school_rec.website))
            # y_axis -= 20
            # p.drawString(x_cod, y_axis, 'TL: ' + str(school_rec.contact_num))
        p.showPage()  # Breaking the page and further data will be printed on next page
        p.save()
    return response


###--- ID backup 04-06-18 ------------------##########
# @user_login_required
# def PrintID(request, std_id):
#     response = HttpResponse(content_type='application/pdf')
#     response['Content-Disposition'] = 'attachment; filename = "IDCard.pdf"'
#     p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
#     p.setPageSize((310, 450))
#     student_rec = student_details.objects.get(id=std_id)
#     blood_group = ''
#
#     # if str(student_rec.transport).lower() == 'school':
#     front_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/front_id.jpg')
#     # else:
#     #     front_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/Template_own.png')
#     back_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/back_id.jpg')
#     contact_png = settings.STATICFILES_DIRS[0] + str('/id_images/contact.png')
#     # file_name =settings.STATICFILES_DIRS[0] + str('/Student_Id_Card_template.png')
#
#     student_rec.birth_date = student_rec.birth_date.strftime(
#         '%Y-%m-%d')  # It will format the birth date of the student over the PDF else birth date will be printed in Default format
#
#     school_rec = school_details.objects.get(
#         id=1)  # Here 1 is the Id of first school and Only one school will be present
#     academicYear = str(student_rec.academic_class_section.year_name.year_name)
#
#     front_backgroung = ImageReader(front_file_name)  # Getting edsys_assesment logo
#     # p.drawImage(logo, -18, -2, width=8.5 * inch, height=6.3 * inch, mask='auto')
#
#     if student_rec.photo:
#         photo = ImageReader(student_rec.photo)
#         p.drawImage(photo, 70, 250, width=2 * inch, height=2 * inch, mask='auto')
#     p.drawImage(front_backgroung, -0, -2, width=4.5 * inch, height=6.3 * inch, mask='auto')
#
#     if school_rec.logo:
#         logo = ImageReader(school_rec.logo)  # Getting edsys_assesment logo
#         p.drawImage(logo, 10, 320, width=0.9 * inch, height=1.2 * inch, mask='auto')
#
#     p.setFont('Helvetica-Bold', 18)
#     p.setFillColor('#306598')
#     p.drawString((300 / 2.0) - (len(str(student_rec.odoo_id)) * 6), 215, str(student_rec.odoo_id))
#     # p.drawString(110, 215, str(student_rec.odoo_id))
#
#     p.setFont('Helvetica-Bold', 16)
#     p.setFillColor('#306598')
#     student_name = student_rec.get_all_name()
#
#     count = 0
#     y_cod = 215
#     if len(student_name) > 25:
#         wrap_text = textwrap.wrap(student_name, width=25)
#         total_index = len(wrap_text)
#         while (total_index > 0):
#             p.drawString(30, y_cod - 20, wrap_text[count])
#             total_index = total_index - 1
#             count = count + 1
#             y_cod = y_cod - 20
#             # p.drawString(30, 195, wrap_text[1])
#     else:
#         p.drawString(30, 195, str(student_name))
#
#     p.setFont('Helvetica-Bold', 12)
#     p.drawString(30, 155, 'Academic Year:')
#
#     p.setFillColor('#636565')
#     p.drawString(130, 155, academicYear)
#
#     p.setFillColor('#306598')
#     p.drawString(30, 130, 'Class:')
#
#     p.setFillColor('#636565')
#     p.drawString(75, 130, str(student_rec.academic_class_section.class_name.class_name) + ',' + str(
#         student_rec.academic_class_section.section_name.section_name))
#
#     if student_medical.objects.filter(student__id=student_rec.id).exists():
#         blood_group = student_medical.objects.get(student__id=student_rec.id).blood_group
#
#     text_len = len(str(student_rec.academic_class_section.class_name.class_name) + ',' + str(
#         student_rec.academic_class_section.section_name.section_name))
#     if text_len >= 10:
#         bg_x = (75 + (text_len) * 8)
#     else:
#         bg_x = (75 + (text_len) * 10)
#
#     p.setFillColor('#306598')
#     p.drawString(bg_x, 130, 'Blood Group:')
#     p.setFillColor('#636565')
#     bg_x = bg_x + (len('Blood Group:') * 7)
#     p.drawString(bg_x, 130, str(blood_group))
#
#     p.setFillColor('#306598')
#     p.drawString(30, 105, 'Parent Name:')
#     p.setFillColor('#636565')
#
#     if student_rec.parent:
#         p.drawString(130, 105, str(student_rec.parent.user.get_pura_name()))
#
#     p.setFillColor('#306598')
#     p.drawString(30, 80, 'Emergency      :')
#     p.drawImage(contact_png, 97, 75, width=0.2 * inch, height=0.2 * inch, mask='auto')
#
#     if student_rec.emergency_contact_num != None and student_rec.emergency_contact_num != '':
#         p.setFillColor('#636565')
#         p.drawString(130, 80, str(student_rec.emergency_contact_num))
#
#     p.setFillColor(white)
#     p.drawString(130, 18, 'Transport Type:   ' + str(student_rec.transport).title())
#
#     if str(student_rec.transport).lower() == 'school':
#         p.setFillColor('#306598')
#         p.setFont('Helvetica-Bold', 9)
#         p.drawString(85, 5, 'Pickup Bus:   ' + str(student_rec.bus_no))
#         p.drawString(185, 5, 'Drop Bus:   ' + str(student_rec.drop_bus_no))
#     p.showPage()  # Breaking the page and further data will be printed on next page
#
#     back_backgroung = ImageReader(back_file_name)  # Getting edsys_assesment logo
#
#     p.setPageSize((310, 450))
#     p.drawImage(back_backgroung, -0, -2, width=4.5 * inch, height=6.3 * inch, mask='auto')
#
#     if school_rec.logo:
#         logo = ImageReader(school_rec.logo)  # Getting edsys_assesment logo
#         p.drawImage(logo, 110, 300, width=1.2 * inch, height=1.2 * inch, mask='auto')
#
#     p.setFont('Times-Bold', 18)
#
#     p.setFillColor('#010101')
#     p.drawString(35, 260, str(school_rec.school_name))
#
#     p.setFillColor('#262626')
#     p.setFont('Helvetica-Bold', 10)
#     p.drawString(20, 245, '------------------------------------------------------------------------------')
#
#     if len(str(school_rec.school_address)) <= 45:
#         p.drawString(30, 230, str(school_rec.school_address))
#         p.drawString(30, 210,
#                      'City- ' + str(school_rec.city) + ', State- ' + str(school_rec.state) + ' -' + str(school_rec.pin))
#         p.drawString(30, 190, str(school_rec.email))
#         p.drawString(30, 170, str(school_rec.website))
#         p.drawString(30, 150, 'PH: ' + str(school_rec.contact_num))
#     elif len(str(school_rec.school_address)) <= 90:
#         wrap_text = textwrap.wrap(str(school_rec.school_address), width=45)
#         p.drawString(30, 230, wrap_text[0])
#         p.drawString(30, 210, wrap_text[1])
#         p.drawString(30, 190,
#                      'City- ' + str(school_rec.city) + ', State- ' + str(school_rec.state) + ' -' + str(school_rec.pin))
#         p.drawString(30, 170, str(school_rec.email))
#         p.drawString(30, 150, str(school_rec.website))
#         p.drawString(30, 130, 'PH: ' + str(school_rec.contact_num))
#     else:
#         wrap_text = textwrap.wrap(str(school_rec.school_address), width=45)
#         p.drawString(30, 230, wrap_text[0])
#         p.drawString(30, 210, wrap_text[1])
#         p.drawString(30, 190, wrap_text[2])
#         p.drawString(30, 170,
#                      'City- ' + str(school_rec.city) + ', State- ' + str(school_rec.state) + ' -' + str(school_rec.pin))
#         p.drawString(30, 150, str(school_rec.email))
#         p.drawString(30, 130, str(school_rec.website))
#         p.drawString(30, 110, 'PH: ' + str(school_rec.contact_num))
#     p.showPage()  # Breaking the page and further data will be printed on next page
#     p.save()
#     return response


@user_login_required
def PrintBack(request):
    if id_student_selected_template.objects.get().template == 'temp_one':
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename = "IDCard.pdf"'
        p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
        p.setPageSize((300, 450))
        school_rec = school_details.objects.get(
            id=1)  # Here 1 is the Id of first school and Only one school will be present

        back_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/Template_back.png')

        back_backgroung = ImageReader(back_file_name)  # Getting schoolzen logo

        p.drawImage(back_backgroung, -18, -2, width=4.5 * inch, height=6.3 * inch, mask='auto')

        if school_rec.logo:
            logo = ImageReader(school_rec.logo)  # Getting schoolzen logo
            p.drawImage(logo, 110, 300, width=1.2 * inch, height=1.2 * inch, mask='auto')

        p.setFont('Times-Bold', 18)

        p.setFillColor('#010101')
        p.drawString(35, 260, str(school_rec.school_name))

        p.setFillColor('#262626')
        p.setFont('Helvetica-Bold', 10)
        p.drawString(10, 245, '---------------------------------------------------------------------------')

        if len(str(school_rec.school_address)) <= 45:
            p.drawString(30, 230, str(school_rec.school_address))
            p.drawString(30, 210,
                         'City- ' + str(school_rec.city) + ', State- ' + str(school_rec.state) + ' -' + str(
                             school_rec.pin))
            p.drawString(30, 190, str(school_rec.email))
            p.drawString(30, 170, str(school_rec.website))
            p.drawString(30, 150, 'PH: ' + str(school_rec.contact_num))
        elif len(str(school_rec.school_address)) <= 90:
            wrap_text = textwrap.wrap(str(school_rec.school_address), width=45)
            p.drawString(30, 230, wrap_text[0])
            p.drawString(30, 210, wrap_text[1])
            p.drawString(30, 190,
                         'City- ' + str(school_rec.city) + ', State- ' + str(school_rec.state) + ' -' + str(
                             school_rec.pin))
            p.drawString(30, 170, str(school_rec.email))
            p.drawString(30, 150, str(school_rec.website))
            p.drawString(30, 130, 'PH: ' + str(school_rec.contact_num))
        else:
            wrap_text = textwrap.wrap(str(school_rec.school_address), width=45)
            p.drawString(30, 230, wrap_text[0])
            p.drawString(30, 210, wrap_text[1])
            p.drawString(30, 190, wrap_text[2])
            p.drawString(30, 170,
                         'City- ' + str(school_rec.city) + ', State- ' + str(school_rec.state) + ' -' + str(
                             school_rec.pin))
            p.drawString(30, 150, str(school_rec.email))
            p.drawString(30, 130, str(school_rec.website))
            p.drawString(30, 110, 'PH: ' + str(school_rec.contact_num))
        p.showPage()  # Breaking the page and further data will be printed on next page
        p.save()
    else:
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename = "IDCard.pdf"'
        p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
        p.setPageSize((310, 450))
        school_rec = school_details.objects.get(
            id=1)  # Here 1 is the Id of first school and Only one school will be present

        back_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/back_id.jpg')
        TrajanPro = settings.BASE_DIR + '/TrajanPro-Bold.ttf'
        pdfmetrics.registerFont(TTFont('TrajanPro-Bold', TrajanPro))

        school_rec = school_details.objects.get(
            id=1)  # Here 1 is the Id of first school and Only one school will be present

        if school_rec.logo:
            logo = ImageReader(school_rec.logo)  # Getting schoolzen logo
            p.drawImage(logo, 10, 350, width=0.9 * inch, height=1.2 * inch, mask='auto')


        # school_name = '\n'.join(str(school_rec.school_name).split(' '))

        p.setFont('Times-Bold', 22)
        p.setFillColor(white)

        y_name = 420
        for name in str(school_rec.school_name).split(' '):
            p.drawString(100, y_name, name.upper())
            y_name -= 20

        p.setFillColor(black)
        # p.drawString((300 / 2.0) - (len(str(student_rec.odoo_id)) * 6), 215, str(student_rec.odoo_id))
        # p.drawString(110, 215, str(student_rec.odoo_id))

        p.setFont('Helvetica-Bold', 14)
        # p.setFillColor(black)

        count = 0
        y_cod = 190
        x_cod = 20
        x_diff = 18

        back_backgroung = ImageReader(back_file_name)  # Getting schoolzen logo

        p.setPageSize((310, 450))
        p.drawImage(back_backgroung, -0, -2, width=4.5 * inch, height=6.3 * inch, mask='auto')

        if school_rec.logo:
            logo = ImageReader(school_rec.logo)  # Getting schoolzen logo
            p.drawImage(logo, 110, 350, width=1.2 * inch, height=1.2 * inch, mask='auto')


        if school_rec.principal_qr_code:
            qr_code = ImageReader(school_rec.principal_qr_code)  # Getting schoolzen logo
            p.drawImage(qr_code, 30, 290, width=0.8* inch, height=0.8 * inch, mask='auto')


        y_axis = 270

        p.drawString(x_cod, y_axis, 'This card is the property of school.')
        p.drawString(x_cod, y_axis - 15, 'If found, please return to')

        p.setFont('TrajanPro-Bold', 16)

        y_axis -= 40
        p.drawString(x_cod, y_axis, str(school_rec.school_name))

        p.setFont('Helvetica-Bold', 10)
        y_axis -= 1

        if len(str(school_rec.school_address)) <= 45:
            y_axis -= 20
            p.drawString(x_cod, y_axis, str(school_rec.school_address))
            y_axis -= 20
            p.drawString(x_cod, y_axis,
                         'City- ' + str(school_rec.city) + ', State- ' + str(school_rec.state) + ' -' + str(school_rec.pin))
            y_axis -= 20
            p.drawString(x_cod, y_axis, 'PH: ' + str(school_rec.contact_num))
            y_axis -= 20
            p.drawString(x_cod, y_axis, str(school_rec.email))
            y_axis -= 20
            p.drawString(x_cod, y_axis, str(school_rec.website))

        elif len(str(school_rec.school_address)) <= 90:
            wrap_text = textwrap.wrap(str(school_rec.school_address), width=55)
            y_axis -= 20
            p.drawString(x_cod, y_axis, wrap_text[0])
            try:
                if wrap_text[1]:
                    y_axis -= 20
                    p.drawString(x_cod, y_axis, wrap_text[1])
            except:
                pass
            y_axis -= 20
            p.drawString(x_cod, y_axis,
                         'City- ' + str(school_rec.city) + ', State- ' + str(school_rec.state) + ' -' + str(school_rec.pin))
            y_axis -= 20
            p.drawString(x_cod, y_axis, 'PH: ' + str(school_rec.contact_num))
            y_axis -= 20
            p.drawString(x_cod, y_axis, str(school_rec.email))
            y_axis -= 20
            p.drawString(x_cod, y_axis, str(school_rec.website))
            # y_axis -= 20
            # p.drawString(x_cod, y_axis, 'PH: ' + str(school_rec.contact_num))
        else:
            wrap_text = textwrap.wrap(str(school_rec.school_address), width=45)
            y_axis -= 20
            p.drawString(x_cod, y_axis, wrap_text[0])
            y_axis -= 20
            p.drawString(x_cod, y_axis, wrap_text[1])
            y_axis -= 20
            p.drawString(x_cod, y_axis, wrap_text[2])
            y_axis -= 20
            p.drawString(x_cod, y_axis,
                         'City- ' + str(school_rec.city) + ', State- ' + str(school_rec.state) + ' -' + str(school_rec.pin))
            y_axis -= 20
            p.drawString(x_cod, y_axis, 'PH: ' + str(school_rec.contact_num))
            y_axis -= 20
            p.drawString(x_cod, y_axis, str(school_rec.email))
            y_axis -= 20
            p.drawString(x_cod, y_axis, str(school_rec.website))
            # y_axis -= 20
            # p.drawString(x_cod, y_axis, 'TL: ' + str(school_rec.contact_num))
        p.showPage()  # Breaking the page and further data will be printed on next page
        p.save()
    return response


def MultipleStudentIDlist(request):
    if request.method == 'POST':

        if id_student_selected_template.objects.get().template == 'temp_one':
            selected_student_ids = request.POST.getlist('check')  # Getting Ids  of selected students

            response = HttpResponse(content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename = "IDCard.pdf"'
            p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
            p.setPageSize((300, 450))

            school_rec = school_details.objects.get(
                id=1)  # Here 1 is the Id of first school and Only one school will be present

            for selected_student_id in selected_student_ids:
                try:
                    student_rec = student_details.objects.get(id=selected_student_id)
                    blood_group = ''

                    if str(student_rec.transport).lower() == 'school':
                        front_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/Template_school.png')
                    else:
                        front_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/Template_own.png')
                    contact_png = settings.STATICFILES_DIRS[0] + str('/id_images/contact.png')

                    academicYear = str(student_rec.academic_class_section.year_name.year_name)

                    front_backgroung = ImageReader(front_file_name)  # Getting schoolzen logo
                    # p.drawImage(logo, -18, -2, width=8.5 * inch, height=6.3 * inch, mask='auto')

                    if student_rec.photo:
                        photo = ImageReader(student_rec.photo)
                        p.drawImage(photo, 70, 250, width=2 * inch, height=2 * inch, mask='auto')
                    p.drawImage(front_backgroung, -18, -2, width=4.5 * inch, height=6.3 * inch, mask='auto')

                    if school_rec.logo:
                        logo = ImageReader(school_rec.logo)  # Getting schoolzen logo
                        p.drawImage(logo, 5, 8, width=0.6 * inch, height=0.6 * inch, mask='auto')

                    p.setFont('Helvetica-Bold', 18)
                    p.setFillColor('#306598')
                    p.drawString((300 / 2.0) - (len(str(student_rec.odoo_id)) * 6), 215, str(student_rec.odoo_id))
                    # p.drawString(110, 215, str(student_rec.odoo_id))

                    p.setFont('Helvetica-Bold', 16)
                    p.setFillColor('#306598')
                    student_name = student_rec.get_all_name()

                    count = 0
                    y_cod = 215
                    if len(student_name) > 25:
                        wrap_text = textwrap.wrap(student_name, width=25)
                        total_index = len(wrap_text)
                        while (total_index > 0):
                            p.drawString(30, y_cod - 20, wrap_text[count])
                            total_index = total_index - 1
                            count = count + 1
                            y_cod = y_cod - 20

                    else:
                        p.drawString(30, 195, str(student_name))

                    p.setFont('Helvetica-Bold', 12)
                    p.drawString(30, 155, 'Academic Year:')

                    p.setFillColor('#636565')
                    p.drawString(130, 155, academicYear)

                    p.setFillColor('#306598')
                    p.drawString(30, 130, 'Class:')

                    p.setFillColor('#636565')
                    p.drawString(75, 130, str(student_rec.academic_class_section.class_name.class_name) + ',' + str(
                        student_rec.academic_class_section.section_name.section_name))

                    if student_medical.objects.filter(student__id=student_rec.id).exists():
                        blood_group = student_medical.objects.get(student__id=student_rec.id).blood_group

                    p.setFillColor('#306598')
                    text_len = len(str(student_rec.academic_class_section.class_name.class_name) + ',' + str(
                        student_rec.academic_class_section.section_name.section_name))
                    if text_len >= 10:
                        bg_x = (75 + (text_len) * 8)
                    else:
                        bg_x = (75 + (text_len) * 10)

                    p.drawString(bg_x, 130, 'Blood Group:')
                    p.setFillColor('#636565')
                    bg_x = bg_x + (len('Blood Group:') * 7)
                    p.drawString(bg_x, 130, str(blood_group))

                    p.setFillColor('#306598')
                    p.drawString(30, 105, 'Parent Name:')
                    p.setFillColor('#636565')
                    if student_rec.parent:
                        p.drawString(130, 105, str(student_rec.parent.user.get_pura_name()))

                    p.setFillColor('#306598')
                    p.drawString(30, 80, 'Emergency      :')
                    p.drawImage(contact_png, 97, 75, width=0.2 * inch, height=0.2 * inch, mask='auto')
                    p.setFillColor('#636565')

                    if student_rec.emergency_contact_num != None and student_rec.emergency_contact_num != '':
                        p.drawString(130, 80, str(student_rec.emergency_contact_num))

                    p.setFillColor(white)
                    p.drawString(130, 18, 'Transport Type:   ' + str(student_rec.transport).title())

                    if str(student_rec.transport).lower() == 'school':
                        p.setFillColor('#306598')
                        p.setFont('Helvetica-Bold', 9)
                        p.drawString(80, 5, 'Pickup Bus:   ' + str(student_rec.bus_no))
                        p.drawString(180, 5, 'Drop Bus:   ' + str(student_rec.drop_bus_no))
                    p.showPage()  # Breaking the page and further data will be printed on next page
                except:
                    continue
            p.save()  # saving the PDF once all done

        else:

            selected_student_ids = request.POST.getlist('check')  # Getting Ids  of selected students

            response = HttpResponse(content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename = "IDCard.pdf"'
            p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
            p.setPageSize((300, 450))

            school_rec = school_details.objects.get(
                id=1)  # Here 1 is the Id of first school and Only one school will be present

            for selected_student_id in selected_student_ids:
                try:
                    student_rec = student_details.objects.get(id=selected_student_id)
                    TrajanPro = settings.BASE_DIR + '/TrajanPro-Bold.ttf'
                    pdfmetrics.registerFont(TTFont('TrajanPro-Bold', TrajanPro))

                    front_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/front_blank_page_3.png')
                    contact_png = settings.STATICFILES_DIRS[0] + str('/id_images/phone-contact.png')

                    academicYear = str(student_rec.academic_class_section.year_name.year_name)

                    front_backgroung = ImageReader(front_file_name)  # Getting schoolzen logo

                    if student_rec.photo:
                        photo = ImageReader(student_rec.photo)
                        p.drawImage(photo, 90, 200, width=1.8 * inch, height=1.8 * inch, mask='auto')
                    p.drawImage(front_backgroung, -0, -2, width=4.5 * inch, height=6.3 * inch, mask='auto')

                    # if school_rec.logo:
                    #     logo = ImageReader(school_rec.logo)  # Getting schoolzen logo
                    #     p.drawImage(logo, 10, 350, width=0.9 * inch, height=1.2 * inch, mask='auto')

                    if school_rec.logo:
                        logo = ImageReader(school_rec.logo)  # Getting schoolzen logo
                        p.drawImage(logo, 29, 345, width=0.9 * inch, height=1.1 * inch, mask='auto')


                    p.setFont('TrajanPro-Bold', 22)
                    p.setFillColor(white)

                    y_name = 410
                    for name in str(school_rec.school_name).split(' '):
                        p.drawString(100, y_name, name.upper())
                        y_name -= 20

                    p.setFillColor(black)

                    p.setFont('Helvetica-Bold', 14)
                    student_name = student_rec.get_all_name()

                    count = 0
                    y_cod = 190
                    x_cod = 20
                    x_diff = 18
                    if len(student_name) > 35:
                        wrap_text = textwrap.wrap(student_name, width=35)
                        total_index = len(wrap_text)
                        while (total_index > 0):
                            p.drawString(x_cod, y_cod - x_diff, wrap_text[count])
                            total_index = total_index - 1
                            count = count + 1
                            y_cod = y_cod - x_diff
                    else:
                        y_cod -= x_diff
                        if student_name.split('  '):
                            student_name = student_name.replace('  ', ' ')
                        p.drawString(x_cod, y_cod, str(student_name))

                    p.setFont('Helvetica-Bold', 12)
                    y_cod -= x_diff
                    p.drawString(x_cod, y_cod, 'Student ID          :   ' + str(student_rec.odoo_id))

                    y_cod -= x_diff
                    p.drawString(x_cod, y_cod, 'Class                  :   ' + str(
                        student_rec.academic_class_section.class_name.class_name) + ',  Section : ' + str(
                        student_rec.academic_class_section.section_name.section_name))

                    y_cod -= x_diff
                    p.drawString(x_cod, y_cod, 'Academic Year  :   ' + academicYear)

                    y_cod -= x_diff

                    if student_rec.parent:
                        p.drawString(x_cod, y_cod, 'Parent Name      :   ' + str(student_rec.parent.user.get_pura_name()))
                    else:
                        p.drawString(x_cod, y_cod, 'Parent Name      :   ')
                    y_cod -= x_diff

                    p.drawImage(contact_png, 87, y_cod - 3, width=0.2 * inch, height=0.2 * inch, mask='auto')

                    if student_rec.emergency_contact_num != None and student_rec.emergency_contact_num != '':
                        p.drawString(x_cod, y_cod, 'Emergency         :   ' + str(student_rec.emergency_contact_num))
                    else:
                        p.drawString(x_cod, y_cod, 'Emergency         :  ')

                    y_cod -= x_diff

                    p.drawString(x_cod, y_cod, 'Transport Type  :   ' + str(student_rec.transport).title())

                    y_cod -= x_diff

                    # if str(student_rec.transport).lower() == 'school':
                    #     p.drawString(x_cod, y_cod, 'Pickup :  ' + str(student_rec.bus_no) + '    Drop of :  ' + str(
                    #         student_rec.drop_bus_no))
                    # p.showPage()  # Breaking the page and further data will be printed on next page
                    if str(student_rec.transport).lower() == 'school':
                        p.drawString(x_cod, y_cod,
                                     'Pickup                :   ' + str(student_rec.pick_up_point).strip() if student_rec.pick_up_point and str(student_rec.pick_up_point) != 'None' else 'Pickup                :   ')

                        y_cod -= x_diff
                        p.drawString(x_cod, y_cod,
                                     'Drop off              :  ' + str(student_rec.drop_off_point).strip() if student_rec.drop_off_point  and str(student_rec.pick_up_point) != 'None'  else 'Drop off              :  ')
                    p.showPage()  # Breaking the page and further data will be printed on next page
                except:
                    continue
            p.save()  # saving the PDF once all done
    return response



###------- Multiple student Id Print backup -- 04-06-18 ------#############################
# def MultipleStudentIDlist(request):
#     if request.method == 'POST':
#
#         selected_student_ids = request.POST.getlist('check')  # Getting Ids  of selected students
#
#         response = HttpResponse(content_type='application/pdf')
#         response['Content-Disposition'] = 'attachment; filename = "IDCard.pdf"'
#         p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
#         p.setPageSize((300, 450))
#
#         school_rec = school_details.objects.get(
#             id=1)  # Here 1 is the Id of first school and Only one school will be present
#
#         for selected_student_id in selected_student_ids:
#             try:
#                 student_rec = student_details.objects.get(id=selected_student_id)
#                 blood_group = ''
#
#                 if str(student_rec.transport).lower() == 'school':
#                     front_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/Template_school.png')
#                 else:
#                     front_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/Template_own.png')
#                 contact_png = settings.STATICFILES_DIRS[0] + str('/id_images/contact.png')
#
#                 academicYear = str(student_rec.academic_class_section.year_name.year_name)
#
#                 front_backgroung = ImageReader(front_file_name)  # Getting edsys_assesment logo
#                 # p.drawImage(logo, -18, -2, width=8.5 * inch, height=6.3 * inch, mask='auto')
#
#                 if student_rec.photo:
#                     photo = ImageReader(student_rec.photo)
#                     p.drawImage(photo, 70, 250, width=2 * inch, height=2 * inch, mask='auto')
#                 p.drawImage(front_backgroung, -18, -2, width=4.5 * inch, height=6.3 * inch, mask='auto')
#
#                 if school_rec.logo:
#                     logo = ImageReader(school_rec.logo)  # Getting edsys_assesment logo
#                     p.drawImage(logo, 5, 8, width=0.6 * inch, height=0.6 * inch, mask='auto')
#
#                 p.setFont('Helvetica-Bold', 18)
#                 p.setFillColor('#306598')
#                 p.drawString((300 / 2.0) - (len(str(student_rec.odoo_id)) * 6), 215, str(student_rec.odoo_id))
#                 # p.drawString(110, 215, str(student_rec.odoo_id))
#
#                 p.setFont('Helvetica-Bold', 16)
#                 p.setFillColor('#306598')
#                 student_name = student_rec.get_all_name()
#
#                 # if len(student_name) > 25:
#                 #     wrap_text = textwrap.wrap(student_name, width=25)
#                 #     p.drawString(30, 215, wrap_text[0])
#                 #     p.drawString(30, 195, wrap_text[1])
#                 # else:
#                 #     p.drawString(30, 215, str(student_name))
#
#                 count = 0
#                 y_cod = 215
#                 if len(student_name) > 25:
#                     wrap_text = textwrap.wrap(student_name, width=25)
#                     total_index = len(wrap_text)
#                     while (total_index > 0):
#                         p.drawString(30, y_cod - 20, wrap_text[count])
#                         total_index = total_index - 1
#                         count = count + 1
#                         y_cod = y_cod - 20
#                         # p.drawString(30, 195, wrap_text[1])
#                 else:
#                     p.drawString(30, 195, str(student_name))
#
#                 p.setFont('Helvetica-Bold', 12)
#                 p.drawString(30, 155, 'Academic Year:')
#
#                 p.setFillColor('#636565')
#                 p.drawString(130, 155, academicYear)
#
#                 p.setFillColor('#306598')
#                 p.drawString(30, 130, 'Class:')
#
#                 p.setFillColor('#636565')
#                 p.drawString(75, 130, str(student_rec.academic_class_section.class_name.class_name) + ',' + str(
#                     student_rec.academic_class_section.section_name.section_name))
#
#                 if student_medical.objects.filter(student__id=student_rec.id).exists():
#                     blood_group = student_medical.objects.get(student__id=student_rec.id).blood_group
#
#                 p.setFillColor('#306598')
#                 text_len = len(str(student_rec.academic_class_section.class_name.class_name) + ',' + str(
#                     student_rec.academic_class_section.section_name.section_name))
#                 if text_len >= 10:
#                     bg_x = (75 + (text_len) * 8)
#                 else:
#                     bg_x = (75 + (text_len) * 10)
#
#                 p.drawString(bg_x, 130, 'Blood Group:')
#                 p.setFillColor('#636565')
#                 bg_x = bg_x + (len('Blood Group:') * 7)
#                 p.drawString(bg_x, 130, str(blood_group))
#
#                 # p.setFillColor('#306598')
#                 # p.drawString(130, 130, 'Blood Group:')
#                 # p.setFillColor('#636565')
#                 # p.drawString(225, 130, str(blood_group))
#
#                 p.setFillColor('#306598')
#                 p.drawString(30, 105, 'Parent Name:')
#                 p.setFillColor('#636565')
#                 if student_rec.parent:
#                     p.drawString(130, 105, str(student_rec.parent.user.get_pura_name()))
#
#                 p.setFillColor('#306598')
#                 p.drawString(30, 80, 'Emergency      :')
#                 p.drawImage(contact_png, 97, 75, width=0.2 * inch, height=0.2 * inch, mask='auto')
#                 p.setFillColor('#636565')
#
#                 if student_rec.emergency_contact_num != None and student_rec.emergency_contact_num != '':
#                     p.drawString(130, 80, str(student_rec.emergency_contact_num))
#
#                 p.setFillColor(white)
#                 p.drawString(130, 18, 'Transport Type:   ' + str(student_rec.transport).title())
#
#                 if str(student_rec.transport).lower() == 'school':
#                     p.setFillColor('#306598')
#                     p.setFont('Helvetica-Bold', 9)
#                     p.drawString(80, 5, 'Pickup Bus:   ' + str(student_rec.bus_no))
#                     p.drawString(180, 5, 'Drop Bus:   ' + str(student_rec.drop_bus_no))
#                 p.showPage()  # Breaking the page and further data will be printed on next page
#             except:
#                 continue
#         p.save()  # saving the PDF once all done
#     return response


# @user_login_required
# def PrintID(request, std_id):
#     response = HttpResponse(content_type='application/pdf')
#     response['Content-Disposition'] = 'attachment; filename = "IDCard.pdf"'
#     student_rec = student_details.objects.get(id=std_id)
#
#     if student_rec.gender == True:
#         gender = 'Female'
#     else:
#         gender = 'Male'
#
#     student_rec.birth_date = student_rec.birth_date.strftime(
#         '%Y-%m-%d')  # It will format the birth date of the student over the PDF else birth date will be printed in Default format
#
#     #     school_id = student_rec.school_name_id
#     school_rec = school_details.objects.get(
#         id=1)  # Here 1 is the Id of first school and Only one school will be present
#     academicYear = str(student_rec.academic_class_section.year_name.year_name)
#     student_enrollment_num = str(student_rec.student_code)
#
#     p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
#
#     if school_rec.logo:
#         logo = ImageReader(school_rec.logo)  # Getting edsys_assesment logo
#         p.drawImage(logo, 3, 225, width=1 * inch, height=1 * inch, mask='auto')
#
#     p.setFillColor(red)
#     p.setFont('Times-Bold', 18)
#
#     p.setPageSize((400, 300))
#     p.drawString(90, 250, school_rec.school_name)
#     #  If student photo will be present then student photo will be printed else blank rectangular box will be there
#
#     p.setFillColor(black)
#     if student_rec.photo:
#         photo = ImageReader(student_rec.photo)
#         p.drawImage(photo, 30, 95, width=1.2 * inch, height=1.3 * inch, mask='auto')
#     else:
#         p.rect(30, 95, 1 * inch, 1.2 * inch)
#
#     p.setFont('Helvetica-Bold', 16)
#     p.drawString(135, 195, student_rec.first_name + ' ' + student_rec.last_name)
#     p.setFont('Helvetica-Bold', 14)
#     p.drawString(135, 175, 'Student Code:  ' + student_enrollment_num)
#
#     p.drawString(135, 155, 'Class:  ' + student_rec.academic_class_section.class_name.class_name)
#     p.drawString(265, 155, 'Section:  ' + student_rec.academic_class_section.section_name.section_name)
#     p.drawString(135, 135, 'DOB:  ' + student_rec.birth_date)
#     p.drawString(135, 115, 'Gender:  ' + gender)
#     p.drawString(135, 95, 'Parent Name:  ' + student_rec.father_name + ' ' + student_rec.last_name)
#     p.drawString(135, 75, 'Contact No:  ' + student_rec.parent.contact_num if student_rec.parent else '')
#     p.drawString(30, 197, academicYear)
#
#     p.setFont('Times-Bold', 10)
#     p.drawString(30, 55, 'Principal Signature')
#     p.setFillColor(red)
#     p.rect(0 * inch, 0 * inch, 10 * inch, 0.6 * inch, fill=1, stroke=False)
#     p.showPage()  # Breaking the page and further data will be printed on next page
#
#     # p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
#     p.setFillColor(red)
#     p.setFont('Times-Bold', 18)
#
#     if school_rec.logo:
#         p.drawImage(logo, 3, 225, width=1 * inch, height=1 * inch, mask='auto')
#     p.drawString(90, 250, school_rec.school_name)
#
#     # p.drawImage(logo, 15, 220   ,  width = 6*inch, height = 1.2*inch, mask = 'auto')
#     p.setFillColor(black)
#     p.setFont('Helvetica-Bold', 18)
#     p.drawString(90, 175, 'If Lost/Found Please Contact :  ')
#     p.drawString(90, 150, 'Tel No:  ' + school_rec.contact_num)
#     p.drawString(90, 125, 'Email:  ' + school_rec.email)
#     p.setFillColor(red)
#     p.rect(0, 0, 9 * inch, 0.6 * inch, fill=1, stroke=False)
#
#     p.setFillColor(white)  # for web site color setting to white
#     p.setStrokeColor(white)
#     p.setFont("Helvetica-Bold", 16)
#     p.drawString(65, 8, 'Website :  ' + school_rec.website)
#     p.showPage()  # Breaking the page and further data will be printed on next page
#     p.save()
#     return response


def PrintStaffID(request, usr_id):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename = "IDCard.pdf"'

    user_detail = AuthUser.objects.get(id=usr_id)
    school_rec = school_details.objects.get(id=1)
    department_list = department_details.objects.all()  # change department_list to department_recs

    if user_detail.staff_relation.all()[0].gender == True:
        gender = 'Female'
    else:
        gender = 'Male'

    school_rec = school_details.objects.get(
        id=1)  # Here 1 is the Id of first school and Only one school will be present

    logo = ImageReader('media/doc/schoolzenlogo.png')  # Getting schoolzen logo

    p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
    p.setFont('Times-Bold', 18)
    p.setPageSize((250, 350))
    p.setFillColor(purple, alpha=0.5)
    p.roundRect(2, 3, 250, 348, 22, stroke=0, fill=1)
    p.setFillColor(white)
    p.roundRect(6, 50, 240, 250, 0, fill=1, stroke=0)
    p.setFont('Times-Bold', 18)

    if user_detail.staff_relation.all()[0].photo:
        photo = ImageReader(user_detail.staff_relation.all()[0].photo)
        p.drawImage(photo, 10, 220, width=0.9 * inch, height=1 * inch, mask='auto')
    else:
        # p.rect(30, 95, 1 * inch, 1.2 * inch)
        p.rect(10, 220, 0.9 * inch, 1 * inch)
    if school_rec.logo:
        logo = ImageReader(school_rec.logo)  # Getting schoolzen logo
        p.drawImage(logo, 10, 300, width=0.6 * inch, height=0.7 * inch, mask='auto')
    p.drawString(80, 325, school_rec.school_name)

    p.setFillColor(black)
    p.setFont('Helvetica-Bold', 14)
    p.drawString(80, 280, 'Degination :')
    p.setFont('Helvetica', 12)
    p.drawString(80, 265, str(user_detail.role.name) if user_detail.role else ' ')

    p.setFont('Helvetica-Bold', 14)
    p.drawString(80, 244, 'Employee Code :   ')

    id = user_detail.staff_relation.all()[0].id
    # print "Id--",id
    p.setFont('Helvetica', 12)
    p.drawString(100, 229, '' + str(id))

    p.setFont('Helvetica-Bold', 14)
    p.drawString(80, 208, 'Emirates ID :   ')
    p.drawString(80, 172, 'Blood Group :   ')
    p.drawString(80, 156, 'Contact No:  ')
    p.setFont('Helvetica', 12)
    p.drawString(80, 141, str(user_detail.staff_relation.all()[0].contact_num))

    p.setFont('Helvetica-Bold', 14)
    p.drawString(80, 120, 'Pick up/Drop off point:  ')

    p.setFont('Helvetica-Bold', 16)
    p.drawString(60, 90, str(user_detail.first_name) + ' ' + str(user_detail.last_name))

    p.setFont('Times-Bold', 10)
    p.drawString(15, 55, 'Principal Signature')

    p.setFont("Times-Roman", 16)
    p.drawString(45, 35, school_rec.school_name)
    p.setFont("Times-Roman", 12)
    p.drawString(55, 22, school_rec.school_address + ", " + school_rec.city + ", " + str(school_rec.pin))
    p.drawString(65, 10, "Ph.  " + str(school_rec.contact_num))

    p.showPage()  # Breaking the page and further data will be printed on next page
    p.save()
    return response


def MultipleStaffIDCard(request):
    if request.method == 'POST':

        selected_staff_ids = request.POST.getlist('check')  # Getting Ids  of selected students

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename = "IDCard.pdf"'
        p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only

        school_rec = school_details.objects.get(
            id=1)  # Here 1 is the Id of first school and Only one school will be present

        for selected_staff_id in selected_staff_ids:
            user_detail = AuthUser.objects.get(id=selected_staff_id)

            p.setFont('Times-Bold', 18)
            p.setPageSize((250, 350))

            p.setFillColor(purple, alpha=0.5)
            p.roundRect(2, 3, 250, 348, 22, stroke=0, fill=1)
            p.setFillColor(white)
            p.roundRect(6, 50, 240, 250, 0, fill=1, stroke=0)

            p.setFont('Times-Bold', 18)

            if user_detail.staff_relation.all()[0].photo:
                photo = ImageReader(user_detail.staff_relation.all()[0].photo)
                p.drawImage(photo, 10, 220, width=0.9 * inch, height=1 * inch, mask='auto')
            else:
                # p.rect(30, 95, 1 * inch, 1.2 * inch)
                p.rect(10, 220, 0.9 * inch, 1 * inch)

            if school_rec.logo:
                logo = ImageReader(school_rec.logo)  # Getting schoolzen logo
                p.drawImage(logo, 10, 300, width=0.6 * inch, height=0.7 * inch, mask='auto')
            p.drawString(75, 320, school_rec.school_name)

            p.setFillColor(black)
            p.setFont('Helvetica-Bold', 14)
            p.drawString(80, 280, 'Degination :')
            p.setFont('Helvetica', 12)
            p.drawString(80, 265, str(user_detail.role.name) if user_detail.role else ' ')

            p.setFont('Helvetica-Bold', 14)
            p.drawString(80, 244, 'Employee Code :   ')

            id = user_detail.staff_relation.all()[0].id
            # print "Id--",id
            p.setFont('Helvetica', 12)
            p.drawString(100, 229, '' + str(id))

            p.setFont('Helvetica-Bold', 14)
            p.drawString(80, 208, 'Emirates ID :   ')
            p.drawString(80, 172, 'Blood Group :   ')
            p.drawString(80, 156, 'Contact No:  ')
            p.setFont('Helvetica', 12)
            p.drawString(80, 141, str(user_detail.staff_relation.all()[0].contact_num))

            p.setFont('Helvetica-Bold', 14)
            p.drawString(80, 120, 'Pick up/Drop off point:  ')

            p.setFont('Helvetica-Bold', 16)
            p.drawString(65, 88, str(user_detail.first_name) + ' ' + str(user_detail.last_name))

            p.setFont('Times-Bold', 10)
            p.drawString(12, 55, 'Principal Signature')

            p.setFont("Times-Roman", 16)
            p.drawString(45, 35, school_rec.school_name)
            p.setFont("Times-Roman", 12)
            p.drawString(55, 22, school_rec.school_address + ", " + school_rec.city + ", " + str(school_rec.pin))
            p.drawString(65, 10, "Ph.  " + str(school_rec.contact_num))

            p.showPage()  # Breaking the page and further data will be printed on next page
        p.save()  # saving the PDF once all done
    return response


@user_login_required
def StudentTemplateSelect(request):
    return render(request, "student_template_select.html")


@user_login_required
def StudentTemplate(request):
    type = request.POST.get('select')
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename = "IDCard.pdf"'
    p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
    p.setPageSize((300, 450))

    if type == 'school':
        front_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/Template_school.png')
    else:
        front_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/Template_own.png')
    back_file_name = settings.STATICFILES_DIRS[0] + str('/id_images/Template_back.png')
    # contact_png = settings.STATICFILES_DIRS[0] + str('/id_images/contact.png')

    # school_rec = school_details.objects.get(id=1)  # Here 1 is the Id of first school and Only one school will be present
    #
    front_backgroung = ImageReader(front_file_name)
    p.drawImage(front_backgroung, -18, -2, width=4.5 * inch, height=6.3 * inch, mask='auto')
    p.showPage()  # Breaking the page and further data will be printed on next page

    back_backgroung = ImageReader(back_file_name)  # Getting schoolzen logo

    p.drawImage(back_backgroung, -18, -2, width=4.5 * inch, height=6.3 * inch, mask='auto')
    p.setPageSize((300, 450))

    p.showPage()  # Breaking the page and further data will be printed on next page
    p.save()
    return response


# @user_login_required
# def StudentTemplate(request):
#     response = HttpResponse(content_type='application/pdf')
#     response['Content-Disposition'] = 'attachment; filename = "IDCard.pdf"'
#
#     school_rec = school_details.objects.get(
#         id=1)  # Here 1 is the Id of first school and Only one school will be present
#
#     p = canvas.Canvas(response)
#
#     if school_rec.logo:
#         logo = ImageReader(school_rec.logo)  # Getting edsys_assesment logo
#         p.drawImage(logo, 3, 225, width=1 * inch, height=1 * inch, mask='auto')
#
#     try:
#         academicYear = academic_year.objects.get(current_academic_year=1)
#     except:
#         messages.success(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
#         return render(request,'id_menu.html')
#
#     # Creating a single instance of PDF  and all Id will be printed over it only
#     p.setFillColor(red)
#     p.setFont('Times-Bold', 18)
#
#     p.setPageSize((400, 300))
#     p.drawString(90, 250, school_rec.school_name)
#     #  If student photo will be present then student photo will be printed else blank rectangular box will be there
#
#     p.setFillColor(black)
#     p.rect(30, 95, 1 * inch, 1.2 * inch)
#
#     p.setFont('Helvetica-Bold', 16)
#     p.drawString(135, 195, '')
#     p.setFont('Helvetica-Bold', 14)
#     p.drawString(135, 175, 'Student Code:  ')
#
#     p.drawString(135, 155, 'Class:  ')
#     p.drawString(265, 155, 'Section:  ')
#     p.drawString(135, 135, 'DOB:  ')
#     p.drawString(135, 115, 'Gender:  ')
#     p.drawString(135, 95, 'Parent Name:  ')
#     p.drawString(135, 75, 'Contact No:  ')
#     p.drawString(30, 197, '' + academicYear.year_name)
#
#     p.setFont('Times-Bold', 10)
#     p.drawString(30, 55, 'Principal Signature')
#     p.setFillColor(red)
#     p.rect(0 * inch, 0 * inch, 10 * inch, 0.6 * inch, fill=1, stroke=False)
#     p.showPage()  # Breaking the page and further data will be printed on next page
#
#     # p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
#     p.setFillColor(red)
#     p.setFont('Times-Bold', 18)
#
#     if school_rec.logo:
#         p.drawImage(logo, 3, 225, width=1 * inch, height=1 * inch, mask='auto')
#     p.drawString(90, 250, school_rec.school_name)
#
#     # p.drawImage(logo, 15, 220   ,  width = 6*inch, height = 1.2*inch, mask = 'auto')
#     p.setFillColor(black)
#     p.setFont('Helvetica-Bold', 18)
#     p.drawString(90, 175, 'If Lost/Found Please Contact :  ')
#     p.drawString(90, 150, 'Tel No:  ' + school_rec.contact_num)
#     p.drawString(90, 125, 'Email:  ' + school_rec.email)
#     p.setFillColor(red)
#     p.rect(0, 0, 9 * inch, 0.6 * inch, fill=1, stroke=False)
#
#     p.setFillColor(white)  # for web site color setting to white
#     p.setStrokeColor(white)
#     p.setFont("Helvetica-Bold", 16)
#     p.drawString(65, 8, 'Website :  ' + school_rec.website)
#     p.showPage()  # Breaking the page and further data will be printed on next page
#     p.save()
#     return response


def StaffTemplate(request):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename = "StaffIDTemplate.pdf"'

    # academicYear = academic_year.objects.get(current_academic_year=1)
    school_rec = school_details.objects.get(
        id=1)  # Here 1 is the Id of first school and Only one school will be present

    logo = ImageReader('media/doc/schoolzenlogo.png')  # Getting schoolzen logo

    p = canvas.Canvas(response)  # Creating a single instance of PDF  and all Id will be printed over it only
    p.setFont('Times-Bold', 18)
    p.setPageSize((250, 350))
    p.setFillColor(purple, alpha=0.5)
    p.roundRect(2, 3, 250, 348, 22, stroke=0, fill=1)
    p.setFillColor(white)
    p.roundRect(6, 50, 240, 250, 0, fill=1, stroke=0)

    p.setFont('Times-Bold', 18)

    p.rect(10, 220, 0.9 * inch, 1 * inch)
    if school_rec.logo:
        logo = ImageReader(school_rec.logo)  # Getting schoolzen logo
        p.drawImage(logo, 10, 300, width=0.6 * inch, height=0.7 * inch, mask='auto')
    p.drawString(80, 325, school_rec.school_name)

    p.setFillColor(black)
    p.setFont('Helvetica-Bold', 14)
    p.drawString(80, 280, 'Degination :')
    p.drawString(80, 244, 'Employee Code :   ')
    p.drawString(80, 208, 'Emirates ID :   ')
    p.drawString(80, 172, 'Blood Group :   ')
    p.drawString(80, 156, 'Contact No:  ')
    p.drawString(80, 120, 'Pick up/Drop off point:  ')
    p.setFont('Times-Bold', 10)
    p.drawString(15, 55, 'Principal Signature')

    p.setFont("Times-Roman", 16)
    p.drawString(45, 35, school_rec.school_name)
    p.setFont("Times-Roman", 12)
    p.drawString(55, 22, school_rec.school_address + ", " + school_rec.city + ", " + str(school_rec.pin))
    p.drawString(65, 10, "Ph.  " + school_rec.contact_num)

    p.showPage()  # Breaking the page and further data will be printed on next page
    p.save()
    return response

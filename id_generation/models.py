from __future__ import unicode_literals

from django.db import models
from datetime import date,datetime

# Create your models here.

class id_student_selected_template(models.Model):
    template = models.CharField(max_length=25, null=True)
    create_date = models.DateTimeField(default=datetime.now)
    update_date = models.DateTimeField(auto_now=True)

from __future__ import unicode_literals

from django.apps import AppConfig


class IdGenerationConfig(AppConfig):
    name = 'id_generation'

from django.conf.urls import include, url
from django.contrib import admin
from . import views
from django.contrib.auth.views import logout
from django.conf import settings
from django.conf.urls.static import static, serve


app_name = 'id_generation'

urlpatterns = [
                  url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
                  url(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT}),


    ######################################### ID Details ###################################

                  url(r'^id_menu/$', views.IdMenu, name='id_menu'),
                  url(r'^id_generator/$', views.IdGenerator, name='id_generator'),
                  url(r'^updated_student_template/$', views.UpdatedStudentTemplate, name='updated_student_template'),
                  url(r'^view_student_template/$', views.ViewStudentTemplate, name='view_student_template'),
                  url(r'^view_student_ID_list/$', views.IdGeneratorList, name='view_student_ID_list'),
                  url(r'^id_generate/(?P<std_id>[0-9]+)$', views.IdGenerate, name='id_generate'),
                  url(r'^student_template/$', views.StudentTemplate, name='student_template'),
                  url(r'^student_template_select/$', views.StudentTemplateSelect, name='student_template_select'),
                  url(r'^multiple_student_ID_list/$', views.MultipleStudentIDlist, name='multiple_student_ID_list'),
                  url(r'^print_back/$', views.PrintBack, name='print_back'),
                  url(r'^print_id/(?P<std_id>[0-9]+)$', views.PrintID, name='print_id'),

                  # url(r'^multiple_student_ID_list/$',views.MultipleStudentIDlist, name='multiple_student_ID_list'),
                  url(r'^multiple_ststaff_ID_print/$', views.MultipleStaffIDCard, name='multiple_ststaff_ID_print'),
                  # url(r'^student_template/$',views.StudentTemplate, name='student_template'),

                  url(r'^view_staff_list/$', views.ViewStaffList, name='view_staff_list'),
                  url(r'^staff_template/$', views.StaffTemplate, name='staff_template'),
                  url(r'^staff_id_generate/(?P<usr_id>[0-9]+)$', views.StaffIdGenerate, name='staff_id_generate'),
                  url(r'^print_staff_id/(?P<usr_id>[0-9]+)$', views.PrintStaffID, name='print_staff_id'),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

import MySQLdb

db = MySQLdb.connect(host="localhost", user="root", passwd="redbytes")
cursor = db.cursor()
sql = "CREATE DATABASE IF NOT EXISTS django_schoolzen"
cursor.execute(sql)
db.close()

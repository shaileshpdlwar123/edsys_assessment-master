from django.conf import settings
from registration.models import AuthUser

class EmailAuthBackend(object):
    def authenticate(self, username=None, password=None):
        try:
            user = AuthUser.objects.get(email=username)
            if user:
                return user
        except AuthUser.DoesNotExist:
            return None

    def get_user(self, user_id):
       try:
          return AuthUser.objects.get(pk=user_id)
       except AuthUser.DoesNotExist:
          return None
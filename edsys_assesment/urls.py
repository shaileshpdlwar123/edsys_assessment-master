"""schoolzen URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
import registration.urls
import transfer_certificate.urls
import strike_off.urls
import id_generation.urls
import medical.urls
import attendance.urls
import password_reset.urls
import masters.urls
import promotions.urls
import assessment.urls
import parent_portal.urls
import rest_api.urls
from registration import views
import os
from django.views.static import serve

admin.autodiscover()

urlpatterns = [
    url(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),

    url(r'^api/', include(rest_api.urls, namespace='api')),
    url(r'^school/', include(registration.urls, namespace='registration')),
    url(r'^$', views.Login, name='login'),
    url(r'^school/', include(transfer_certificate.urls, namespace='transfer_certificate')),
    url(r'^school/', include(strike_off.urls, namespace='strike_off')),
    url(r'^school/', include(id_generation.urls, namespace='id_generation')),
    url(r'^school/', include(medical.urls, namespace='medical')),
    url(r'^school/', include(password_reset.urls, namespace='password_reset')),
    url(r'^school/', include(attendance.urls, namespace='attendance')),
    url(r'^school/', include(masters.urls, namespace='masters')),
    url(r'^school/', include(promotions.urls, namespace='promotions')),
    url(r'^school/', include(assessment.urls, namespace='assessment')),
    url(r'^school/', include(parent_portal.urls, namespace='parent_portal')),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url('', include('social_django.urls', namespace='social')),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^sso/', include("djssoserver.urls")),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.ADMIN_URL_ENABLE:
    urlpatterns.append(url(r'^admin/', admin.site.urls))
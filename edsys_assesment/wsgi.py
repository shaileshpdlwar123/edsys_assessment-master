"""
WSGI config for schoolzen project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os
import sys
from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "edsys_assesment.settings_prod")
sys.path.append("/var/www/html/school/schoolzen_django")
application = get_wsgi_application()

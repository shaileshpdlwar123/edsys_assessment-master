from __future__ import unicode_literals

from django.db import models
from registration.models import *

# Create your models here.

class parent_student_details_update(models.Model):
    student = models.ForeignKey(student_details, on_delete=models.PROTECT)
    first_name = models.CharField(max_length=255)
    birth_date = models.DateField()
    gender = models.CharField(max_length=25)
    # student_code = models.CharField(max_length=25)
    mother_name = models.CharField(max_length=255)
    father_name = models.CharField(max_length=255)
    city = models.CharField(max_length=80)
    street = models.CharField(max_length=255)
    state = models.CharField(max_length=80)
    postcode = models.CharField(max_length=8)
    email = models.CharField(max_length=250)
    photo = models.FileField(upload_to=content_file_name_student)
    last_name = models.CharField(max_length=255)
    # joining_date = models.DateField()
    parent_mail = models.CharField(max_length=250, null=True)
    # attendance_percentage = models.FloatField(max_length=45, null=True)
    emirati = models.CharField(max_length=25, default="no")
    select = models.CharField(max_length=25, default="nonarab")
    sen = models.CharField(max_length=25, default="no")
    transport = models.CharField(max_length=25, default="school")

    bus_no = models.CharField(max_length=80, blank=True, null=True)
    pick_up_time = models.TimeField(null=True, blank=True)
    pick_up_point = models.CharField(max_length=100, blank=True, null=True)
    drop_off_point = models.CharField(max_length=100, blank=True, null=True)
    drop_bus_no = models.CharField(max_length=100, blank=True, null=True)
    drop_off_time = models.TimeField(null=True, blank=True)
    academic_class_section = models.ForeignKey(academic_class_section_mapping, null=True, on_delete=models.PROTECT,
                                               related_name='student_year_class_section_relation_parent')

    # is_active = models.BooleanField(default=True)
    create_date = models.DateTimeField(default=datetime.now)
    update_date = models.DateTimeField(default=datetime.now)

    # academic_class_section = models.ForeignKey(academic_class_section_mapping, null=True, on_delete=models.PROTECT,
    #                                            related_name='student_year_class_section_relation')

    # parent = models.ForeignKey(parents_details, null=True, related_name='parent_relation')
    # nationality = models.ForeignKey(nationality, on_delete=models.PROTECT)
    # religion = models.ForeignKey(religion, on_delete=models.PROTECT)
    # is_submitted = models.BooleanField(default=True)
    # is_approved = models.BooleanField(default=False)
    is_rejected = models.BooleanField(default=False)
    emergency_contact_num = models.CharField(max_length=16, blank=True, null=True)
    # subject_ids = models.ManyToManyField(subjects, related_name='optional_subject_relation')

    class Meta:
        permissions = (
            ('can_view_add_student', 'can view add student'),
            ('can_view_view_edit_student', 'can view view edit student'),
        )

    def __unicode__(self):
        return self.first_name

    def get_all_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s %s' % (self.first_name, self.father_name, self.last_name)
        return full_name.strip()

    # def to_dict(self):
    #     res = {
    #         'id': self.id,
    #         'first_name': self.get_all_name(),
    #         'class_name': self.academic_class_section.class_name,
    #         'section_name': self.academic_class_section.section_name,
    #         'academic_year': self.academic_class_section.year_name
    #     }
    #     return res


class updated_parent_details(models.Model):
    CONTACT_NUM = 'contact_num'
    DOB = 'birth_date'
    GENDER = 'gender'
    CODE = 'code'
    STREET = 'street'
    CITY = 'city'
    STATE = 'state'
    PINCODE = 'postcode'
    DEPARTMENT = 'department'
    SECONDARY_PARENT_IDS = 'secondary_parent_ids'
    msg = ''

    first_name = models.CharField(max_length=255,null=True,blank=True)
    middle_name = models.CharField(max_length=255,null=True,blank=True)
    last_name = models.CharField(max_length=255,null=True,blank=True)
    email = models.CharField(max_length=255,null=True,blank=True)


    parent_id = models.CharField(max_length=16)
    birth_date = models.DateField()
    gender = models.CharField(max_length=25)
    street = models.CharField(max_length=255)
    code = models.CharField(max_length=25)
    contact_num = models.CharField(max_length=16)
    photo = models.FileField(upload_to=content_file_name, null=True)
    city = models.CharField(max_length=80)
    state = models.CharField(max_length=80)
    postcode = models.CharField(max_length=8)
    is_active = models.BooleanField(default=True)
    create_date = models.DateTimeField(default=datetime.now)
    update_date = models.DateTimeField(default=datetime.now)
    is_rejected = models.BooleanField(default=False)
    emergency_contact_num = models.CharField(max_length=16, blank=True, null=True)
    # user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='updated_parent_relation')
    # secondary_parent_ids = OneToManyField(secondary_parent_details)


    def __unicode__(self):
        return self.code


class parent_notification_log(models.Model):
    user_id = models.ForeignKey(AuthUser, null=True, on_delete=models.PROTECT)
    message = models.CharField(max_length=500, null=True,blank=True)
    is_active = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)

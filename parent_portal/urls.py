from django.conf.urls import include, url
from django.contrib import admin
from . import views
from django.contrib.auth.views import logout
from django.conf import settings
from django.conf.urls.static import static, serve


app_name = 'parent_portal'


urlpatterns = [
    url(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),
    ######################################### ID Details ###################################
    url(r'^parent_portal_home/$',views.ParentPortalMenu, name='parent_portal_home'),
    url(r'^parents_students_menu/$',views.ParentsStudentsMenu, name='parents_students_menu'),
    url(r'^student_list/$',views.StudentList, name='student_list'),
    url(r'^parent_notification_log/$',views.ParentNotificationLog, name='parent_notification_log'),
    url(r'^view_student_detalis/(?P<std_id>[0-9]+)$', views.ViewStudentDetalis, name='view_student_detalis'),
    url(r'^child_details_updated_request/$',views.ChildUpdateRequest, name='child_details_updated_request'),
    url(r'^view_student_updatation_details/(?P<std_id>[0-9]+)$', views.UpdatedStudentDetails, name='view_student_updatation_details'),

    # url(r'^view_update_parent/(?P<parent_id>[0-9]+)$', views.ViewUpdateParent, name='view_update_parent'),
    url(r'^view_update_parent/$',views.ViewUpdateParent, name='view_update_parent'),
    url(r'^save_updated_parent_portal/$',views.SaveUpdatedParentPortal, name='save_updated_parent_portal'),

    url(r'^view_parent_updatation_details/(?P<std_id>[0-9]+)$', views.UpdatedParentDetails, name='view_parent_updatation_details'),
    # url(r'^view_student_ID_list/$',views.IdGeneratorList, name='view_student_ID_list'),
    # url(r'^id_generate/(?P<std_id>[0-9]+)$', views.IdGenerate, name='id_generate'),
    # url(r'^print_id/(?P<std_id>[0-9]+)$',views.PrintID, name='print_id'),
    # url(r'^multiple_student_ID_list/$',views.MultipleStudentIDlist, name='multiple_student_ID_list'),
    # url(r'^multiple_ststaff_ID_print/$',views.MultipleStaffIDCard, name='multiple_ststaff_ID_print'),
    # url(r'^student_template/$',views.StudentTemplate, name='student_template'),
    #
    # url(r'^view_staff_list/$',views.ViewStaffList, name='view_staff_list'),
    # url(r'^staff_template/$',views.StaffTemplate, name='staff_template'),
    # url(r'^staff_id_generate/(?P<usr_id>[0-9]+)$',views.StaffIdGenerate, name='staff_id_generate'),
    # url(r'^print_staff_id/(?P<usr_id>[0-9]+)$', views.PrintStaffID, name='print_staff_id'),

    ]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from django import forms
from registration.models import *
from medical.models import *
from django.contrib.admin.widgets import FilteredSelectMultiple
import itertools
from medical.models import *
from parent_portal.models import *
from django.contrib.auth import authenticate
from django.contrib.auth.models import Group
from django.contrib.auth.models import Permission


class ChildDetailsForm(forms.ModelForm):
    ClassDetail = class_details.objects.all()
    Sections = sections.objects.all()
    ParentsDetails = parents_details.objects.all()
    AcademicYear = academic_year.objects.all()
    Nationality = nationality.objects.all()
    Religion = religion.objects.all()
    NationalityGroup = nationality_group.objects.all()
    house_rec = house_master.objects.all()

    first_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only ', 'maxlength': '255'}))
    birth_date = forms.DateField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'type': 'date', 'max': "9999-12-31"}))

    gender = forms.TypedChoiceField(

        choices=(('Male', 'Male'), ('Female', 'Female')),
        widget=forms.RadioSelect
    )

    emirati = forms.TypedChoiceField(

        choices=(('yes', 'Yes'), ('no', 'No')),
        widget=forms.RadioSelect
    )

    select = forms.TypedChoiceField(

        choices=(('yes', 'Yes'), ('no', 'No')),
        widget=forms.RadioSelect
    )

    sen = forms.TypedChoiceField(

        choices=(('yes', 'Yes'), ('no', 'No')),
        widget=forms.RadioSelect
    )
    transport = forms.TypedChoiceField(required=False,

        choices=(('own', 'Own'), ('school', 'School')),
        widget=forms.RadioSelect
    )

    # house_name = forms.ModelChoiceField(required=True, queryset=house_rec,
    #                                     widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))

    # student_code = forms.CharField(required=False, widget=forms.TextInput(
    #     attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z0-9 ]+',
    #            'title': 'Enter Characters Only '}))
    # class_name = forms.ModelChoiceField(required=True, queryset=ClassDetail,
    #                                     widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    mother_name = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only ', 'maxlength': '255'}))
    father_name = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only ', 'maxlength': '255'}))
    street = forms.CharField(required=False, widget=forms.Textarea(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'rows': '2', 'style': 'resize:none',
               'maxlength': '255','title':'No Format Required'}))
    city = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only', 'maxlength': '80'}))
    state = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only', 'maxlength': '80'}))
    postcode = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'maxlength': '8', 'onkeypress': 'return isNumber(event)',
               'title': 'Enter Numbers Only '}))
    email = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'maxlength': '250'}))
    last_name = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only ', 'maxlength': '255'}))
    photo = forms.FileField(required=False, label='Upload Photo')
    # section_name = forms.ModelChoiceField(required=True, queryset=Sections,
    #                                       widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    # joining_date = forms.DateField(required=True, widget=forms.TextInput(
    #     attrs={'class': 'form-control', 'autocomplete': 'off', 'type': 'date', 'max': "9999-12-31"}))
    # year_name = forms.ModelChoiceField(required=True, queryset=AcademicYear,
    #                                    widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    # parent = forms.ModelChoiceField(required=False, queryset=ParentsDetails,
    #                                 widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    parent_mail = forms.CharField(required=False,
                                  widget=forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    # blood_group = forms.CharField(required=False, widget=forms.TextInput(
    #     attrs={'class': 'form-control viewOnlyAccess', 'autocomplete': 'off'}))
    # height = forms.CharField(required=False, widget=forms.TextInput(
    #     attrs={'class': 'form-control viewOnlyAccess', 'autocomplete': 'off'}))
    # weight = forms.CharField(required=False, widget=forms.TextInput(
    #     attrs={'class': 'form-control viewOnlyAccess', 'autocomplete': 'off'}))
    # under_medication = forms.CharField(required=False, widget=forms.TextInput(
    #     attrs={'class': 'form-control viewOnlyAccess', 'autocomplete': 'off'}))
    # allergic = forms.CharField(required=False, widget=forms.TextInput(
    #     attrs={'class': 'form-control viewOnlyAccess', 'autocomplete': 'off'}))
    # nationality = forms.ModelChoiceField( queryset=Nationality,
    #                                      widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    # religion = forms.ModelChoiceField( queryset=Religion,
    #                                   widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))

    # religion_mapping = forms.ModelMultipleChoiceField(required=False, queryset=Religion, widget=forms.SelectMultiple(
    #     attrs={'class': 'form-control', 'autocomplete': 'off'}))
    # nationality_mapping = forms.ModelMultipleChoiceField(required=False, queryset=Nationality,
    #                                                      widget=forms.SelectMultiple(
    #                                                          attrs={'class': 'form-control', 'autocomplete': 'off'}))
    # class_name_mapping = forms.ModelMultipleChoiceField(required=False, queryset=class_details.objects.all(),
    #                                                     widget=forms.SelectMultiple(
    #                                                         attrs={'class': 'form-control', 'autocomplete': 'off'}))
    # section_name_mapping = forms.ModelMultipleChoiceField(required=False, queryset=sections.objects.all(),
    #                                                       widget=forms.SelectMultiple(
    #                                                           attrs={'class': 'form-control', 'autocomplete': 'off'}))
    # subject_name_mapping = forms.ModelMultipleChoiceField(required=False, queryset=subjects.objects.all(),
    #                                                       widget=FilteredSelectMultiple("Subject", is_stacked=False))
    # student_name_mapping = forms.ModelMultipleChoiceField(required=False, queryset=student_details.objects.all(),
    #                                                       widget=FilteredSelectMultiple("StudentDetails",
    #                                                                                     is_stacked=False))
    # nationality_group_mapping = forms.ModelMultipleChoiceField(required=False, queryset=NationalityGroup,
    #                                                            widget=forms.SelectMultiple(
    #                                                                attrs={'class': 'form-control',
    #                                                                       'autocomplete': 'off'}))

    # gender_mapping = forms.ModelMultipleChoiceField(required=False, queryset=Nationality,
    #                                                      widget=forms.SelectMultiple(
    #                                                          attrs={'class': 'form-control', 'autocomplete': 'off'}))
    bus_no = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control transport_class', 'autocomplete': 'off', 'pattern': '[A-Za-z0-9 ]+',
               'title': 'Should Number/Alphanumeric', 'maxlength': '32'}))

    pick_up_time = forms.TimeField(required=False, widget=forms.TimeInput(
        attrs={'class': 'form-control transport_class col-sm-3 time ', 'type': 'time', 'data-fv-time': 'true','title':'Enter pick up time'}))

    pick_up_point = forms.CharField(required=False, widget=forms.Textarea(
        attrs={'class': 'form-control transport_class', 'autocomplete': 'off', 'rows': '2', 'style': 'resize:none', 'title':'Enter pick up point',
               'maxlength': '32'}))

    drop_bus_no = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control transport_class', 'autocomplete': 'off', 'pattern': '[A-Za-z0-9 ]+',
               'title': 'Should Number/Alphanumeric', 'maxlength': '32'}))

    drop_off_point = forms.CharField(required=False, widget=forms.Textarea(
        attrs={'class': 'form-control transport_class', 'autocomplete': 'off', 'rows': '2', 'style': 'resize:none', 'title':'Enter drop off point',
               'maxlength': '255'}))

    drop_off_time = forms.TimeField(required=False, widget=forms.TimeInput(
        attrs={'class': 'form-control transport_class col-sm-3 time ', 'type': 'time', 'data-fv-time': 'true','title':'Enter drop off time'}))
    emergency_contact_num = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control con_num', 'autocomplete': 'off', 'onkeypress': 'return isNumber(event)',
               'maxlength': '16', 'placeholder': "+XXX XXXXXXXXX"}))

    class Meta:
        model = parent_student_details_update
        fields = ['first_name', 'birth_date', 'gender', 'emirati', 'select', 'sen', 'transport',
                  'mother_name', 'father_name', 'street', 'city', 'state', 'postcode','emergency_contact_num',
                  'email', 'last_name', 'photo', 'parent_mail','bus_no','pick_up_time','pick_up_point','drop_bus_no','drop_off_point','drop_off_time']


class UpdatedParentDetailsForm(forms.ModelForm):
    first_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only ', 'maxlength': '255'}))
    contact_num = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'onkeypress': 'return isNumber(event)',
               'maxlength': '16', 'placeholder': "+XXX XXXXXXXXX"}))
    birth_date = forms.DateField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'type': 'date', 'max': "9999-12-31"}))
    gender = forms.TypedChoiceField(

        choices=((False, 'Male'), (True, 'Female')),
        widget=forms.RadioSelect
    )
    street = forms.CharField(required=False, widget=forms.Textarea(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'rows': '2', 'style': 'resize:none', 'title':'No Format Required',
               'maxlength': '255'}))
    email = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'maxlength': '250'}))
    code = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z0-9 ]+',
               'title': 'Only Characters and Numbers are allowed '}))
    middle_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only ', 'maxlength': '255'}))
    last_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only ', 'maxlength': '255'}))
    password = forms.CharField(required=False,
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    city = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only ', 'maxlength': '80'}))
    state = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'maxlength': '80', 'autocomplete': 'off', 'pattern': '[A-Za-z ]+',
               'title': 'Enter Characters Only '}))
    postcode = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'autocomplete': 'off', 'title': 'Enter Numbers Only ', 'maxlength': '8',
               'onkeypress': 'return isNumber(event)'}))
    emergency_contact_num = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control con_num', 'autocomplete': 'off', 'onkeypress': 'return isNumber(event)',
               'maxlength': '16', 'placeholder': "+XXX XXXXXXXXX"}))

    class Meta:
        model = updated_parent_details
        fields = ['first_name', 'birth_date', 'gender', 'street', 'email', 'code', 'middle_name', 'last_name',
                  'contact_num', 'city', 'state', 'postcode','emergency_contact_num']

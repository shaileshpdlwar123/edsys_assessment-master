from django.shortcuts import render, redirect, render_to_response
from registration import *
from django.contrib import messages
from django.contrib.auth.models import Group
from django.conf import settings
from django.forms.formsets import formset_factory
from django.forms.models import model_to_dict, inlineformset_factory
from django.db.models import Q
from datetime import datetime
import ast
import json
from django.http import JsonResponse
from registration.models import *
from registration.forms import *
import base64
from parent_portal.forms import *
from parent_portal.models import *
from parent_portal import *
from models import *
from django.forms.models import modelformset_factory
# Create your views here.
from registration.views import jwt_py2

def ParentPortalMenu(request):

    user = request.user

    roles = []
    roles = list(map(lambda u: u.name, user.role.all()))

    jwt_payload = {"uid": user.id,
                   "roles": roles,
                   "uname": user.first_name}

    jwt = jwt_py2(jwt_payload)
    jwt_url = str(settings.SERVER_HOST_NAME) + ':' + str(settings.JWT_PORT) + '/report_card/?jwt=' + jwt

    flag = False
    parect_flag = parent_notification_log.objects.filter(user_id_id=request.user.id, is_active=True)
    total_active_count = parect_flag.count()
    if parect_flag:
        flag = True

    else:
        flag = False

    return render(request, "parent_portal_menu.html",{'jwt': jwt_url,'flag':flag,'total_active_count':total_active_count})

def ParentsStudentsMenu(request):
    flag = False
    parect_flag = parent_notification_log.objects.filter(user_id_id=request.user.id, is_active=True)
    total_active_count = parect_flag.count()
    if parect_flag:
        flag = True

    else:
        flag = False
    return render(request, "parent_students_menu.html",{'flag':flag,'total_active_count':total_active_count})

def StudentList(request):
    user = request.user

    student_list = student_details.objects.filter(parent__id=user.parent_relation.all()[0].id)

    stud_rec = []
    if student_list.count()==1:
        stud_rec = student_list[0].id
        return redirect('/school/view_student_detalis/' + str(stud_rec))
    else:
        for obj in student_list:
            raw_dict = {}
            # raw_dict['photo'] = settings.SERVER_HOST_NAME.replace('school','media/'+str(obj.photo))
            raw_dict['photo'] = 'http://'+request.META['HTTP_HOST']+'/school/media/'+str(obj.photo)
            raw_dict['first_name'] = obj.first_name
            raw_dict['full_name'] = obj.get_all_name()
            raw_dict['last_name'] = obj.last_name
            raw_dict['class_name'] = obj.academic_class_section.class_name.class_name
            raw_dict['section_name'] = obj.academic_class_section.section_name.section_name
            raw_dict['student'] = obj
            stud_rec.append(raw_dict)

        flag = False
        parect_flag = parent_notification_log.objects.filter(user_id_id=request.user.id, is_active=True)
        total_active_count = parect_flag.count()
        if parect_flag:
            flag = True

        else:
            flag = False

        return render(request, "childrens_list.html",{'student_list':stud_rec,'flag':flag,'total_active_count':total_active_count})

def ViewStudentDetalis(request, std_id):
    class_list = class_details.objects.all()
    section_list = sections.objects.all()
    parent_list = parents_details.objects.all()
    religion_list = religion.objects.all()
    nationality_list = nationality.objects.all()
    academicyr_list = academic_year.objects.all()
    student_detail = student_details.objects.get(id=std_id)
    house_list = house_master.objects.all()
    student_detail_form = StudentDetailsForm()

    student_detail.joining_date = student_detail.joining_date.strftime('%Y-%m-%d')
    student_detail.birth_date = student_detail.birth_date.strftime('%Y-%m-%d')

    if not student_detail.pick_up_time ==None:
        student_detail.pick_up_time = student_detail.pick_up_time.strftime('%H:%M')
    else:
        student_detail.pick_up_time = None

    if not student_detail.drop_off_time == None:
        student_detail.drop_off_time = student_detail.drop_off_time.strftime('%H:%M')
    else:
        student_detail.drop_off_time = None


    medical_detail = None
    if student_medical.objects.filter(student=std_id):
        medical_detail = student_medical.objects.filter(student=std_id)[0]
    subject_recs = student_detail.subject_ids.all()
    mandatory_sub_list = []
    mandatory_sub_recs = mandatory_subject_mapping.objects.filter(
        academic_class_section_mapping_id=student_detail.academic_class_section.id)
    if mandatory_sub_recs:
        for mandatory_sub in mandatory_sub_recs:
            if mandatory_sub:
                mandatory_sub_list.append(mandatory_sub.subject)

    academic_class_section_mapping_classes = academic_class_section_mapping.objects.filter(
        year_name=student_detail.academic_class_section.year_name)
    class_obj_list = []
    section_obj_list = []
    raw_list = []
    for obj in academic_class_section_mapping_classes:
        raw_list.append(obj.class_name)
    class_obj_list = list(set(raw_list))

    raw_list = []
    for obj in academic_class_section_mapping.objects.filter(year_name=student_detail.academic_class_section.year_name,
                                                             class_name=student_detail.academic_class_section.class_name):
        raw_list.append(obj.section_name)
    section_obj_list = list(set(raw_list))

    if (student_detail.gender == 'Male'):
        male = 'checked'
        female = ""
    else:
        female = "checked"
        male = ""

    if (student_detail.emirati == 'yes'):
        emirati_yes = 'checked'
        emirati_no = ""
    else:
        emirati_no = "checked"
        emirati_yes = ""

    if (student_detail.select == 'yes' or student_detail.select == 'arab'):
        select_arab = 'checked'
        select_nonarab = ""
    else:
        select_nonarab = "checked"
        select_arab = ""

    if (student_detail.sen == 'yes'):
        sen_yes = 'checked'
        sen_no = ""
    else:
        sen_no = "checked"
        sen_yes = ""

    if (student_detail.transport == 'own'):
        transport_own = 'checked'
        transport_school = ""
    else:
        transport_school = "checked"
        transport_own = ""

    flag = False
    parect_flag = parent_notification_log.objects.filter(user_id_id=request.user.id, is_active=True)
    total_active_count = parect_flag.count()
    if parect_flag:
        flag = True

    else:
        flag = False

    return render(request, "student_edit.html",
                  {'student_detail': student_detail, 'parent_list': parent_list, 'religion_list': religion_list,
                   'nationality_list': nationality_list, 'class_list': class_list, 'section_list': section_list,
                   'academicyr_list': academicyr_list, 'female': female, 'male': male,
                   'student_detail_form': student_detail_form, 'class_list': class_obj_list,
                   'section_list': section_obj_list, 'medical_rec': medical_detail, 'subject_details': subject_recs,
                   'mandatory_sub': mandatory_sub_list,'emirati_yes':emirati_yes,'emirati_no':emirati_no,'select_arab':select_arab,'select_nonarab':select_nonarab,'sen_yes':sen_yes,'sen_no':sen_no,'transport_own':transport_own,'transport_school':transport_school,'flag':flag,'total_active_count':total_active_count})

from django.db.models import Q
def ChildUpdateRequest(request):
    if (request.method == 'POST'):

        registered = False
        if request.method == 'POST':
            parent_student_details_update.objects.filter(student_id=request.POST['student_id']).delete()
            # if not stu_rec:
            photo = request.POST.get('photo')
            id_photo_val = request.POST.get('id_photo_val')
            academic_year_name = request.POST.get('year_name')
            class_name = request.POST.get('class_name')
            section_name = request.POST.get('section_name')


            if request.POST['mydata'] and not request.FILES:
                dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
                # ext = filename.split('.')[-1]
                filename = "%s_%s.%s" % (request.POST.get('first_name'), dirname, 'png')
                raw_file_path_and_name = os.path.join('images', filename)

                data = str(request.POST['mydata'])
                raw_data = base64.b64decode(data)
                f = open(settings.MEDIA_ROOT + raw_file_path_and_name, 'wb')
                f.write(raw_data)
                f.close()

            if request.POST['id_photo_val']:
                dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
                # ext = filename.split('.')[-1]
                filename = "%s_%s.%s" % (request.POST.get('first_name'), dirname, 'png')
                raw_file_path_and_name = os.path.join('images', filename)

                data = str(request.POST['id_photo_val'])
                temp_data = data.split('base64,')[1]

                raw_data = base64.b64decode(temp_data)
                f = open(settings.MEDIA_ROOT + raw_file_path_and_name, 'wb')
                f.write(raw_data)
                f.close()

            modelDict = []
            student_detail_form = ChildDetailsForm(request.POST, request.FILES)

            if request.POST['email'] != "" and student_details.objects.filter(~Q(id=request.POST['student_id']),email=str(request.POST['email'])).exists():

            # if student_details.objects.filter(~Q(id=request.POST['student_id']), email=request.POST['email']).exists():

                student_detail_form = ChildDetailsForm()
                messages.success(request, "Email_id already exist.")
                return render(request, "student_registration.html", {'student_detail_form': student_detail_form})

            else:
                if student_detail_form.is_valid():
                    student_detail_obj = student_detail_form.save(commit=False)
                    student_detail_obj.street=(student_detail_obj.street).strip()
                    # student_detail_obj.is_active = 1
                    student_detail_obj.student_id = request.POST['student_id']
                    student_detail_obj.academic_class_section = academic_class_section_mapping.objects.get(
                        year_name=academic_year_name, class_name=class_name,
                        section_name=section_name)


                    student_detail_obj.save()
                    if request.FILES.get('photo')==None:
                        # student_detail_obj.photo = request.FILES['photo']
                        student_id = student_detail_obj.student_id
                        student_rec = student_details.objects.get(id=student_id)
                        parent_student_details_update.objects.filter(id=student_detail_obj.id).update(photo=student_rec.photo)

                    if request.POST['mydata'] and not request.FILES:
                        student_detail_obj.photo = raw_file_path_and_name
                        student_detail_obj.save()

                    if request.POST['id_photo_val']:
                        student_detail_obj.photo = raw_file_path_and_name
                        student_detail_obj.save()
    messages.success(request, "Student change request is send for Admin Approval.")

    return redirect('/school/student_list/')

def UpdatedStudentDetails(request, std_id):
    try:
        student_rec = parent_student_details_update.objects.get(student_id=std_id)
        rec_id = student_rec.student_id
        old_student_rec = student_details.objects.get(id=std_id)

        student_detail_form = StudentDetailsForm()
        student_detail = student_details.objects.get(id=std_id)
        student_detail.joining_date = student_detail.joining_date.strftime('%Y-%m-%d')

        class_list = class_details.objects.all()
        section_list = sections.objects.all()
        parent_list = parents_details.objects.all()
        religion_list = religion.objects.all()
        nationality_list = nationality.objects.all()
        academicyr_list = academic_year.objects.all()
        house_list = house_master.objects.all()

        # student_detail.joining_date = parent_student_details_update.joining_date.strftime('%Y-%m-%d')
        # student_detail.birth_date = parent_student_details_update.birth_date.strftime('%YYYY-%mm-%dd')
        old_student_rec.birth_date = old_student_rec.birth_date.strftime('%Y-%m-%d')
        student_rec.birth_date = student_rec.birth_date.strftime('%Y-%m-%d')

        # student_rec.joining_date = student_rec.joining_date.strftime('%Y-%m-%d')

        if not student_rec.pick_up_time == None:
            student_rec.pick_up_time = student_rec.pick_up_time.strftime('%H:%M')
        else:
            student_detail.pick_up_time = None

        if not student_rec.drop_off_time == None:
            student_rec.drop_off_time = student_rec.drop_off_time.strftime('%H:%M')
        else:
            student_detail.drop_off_time = None

        medical_detail = None
        if student_medical.objects.filter(student=std_id):
            medical_detail = student_medical.objects.filter(student=std_id)[0]
        subject_recs = student_detail.subject_ids.all()
        mandatory_sub_list = []
        mandatory_sub_recs = mandatory_subject_mapping.objects.filter(
            academic_class_section_mapping_id=student_detail.academic_class_section.id)
        if mandatory_sub_recs:
            for mandatory_sub in mandatory_sub_recs:
                if mandatory_sub:
                    mandatory_sub_list.append(mandatory_sub.subject)
        # print"vv"


        # mapped_class_objects = class_details.objects.filter(mapped_class=student_detail.academic_class_section)
        # mapped_section_objects = sections.objects.filter(mapped_section=student_detail.academic_class_section)
        #
        academic_class_section_mapping_classes = academic_class_section_mapping.objects.filter(
            year_name=student_detail.academic_class_section.year_name)
        class_obj_list = []
        section_obj_list = []
        raw_list = []
        for obj in academic_class_section_mapping_classes:
            raw_list.append(obj.class_name)
        class_obj_list = list(set(raw_list))

        raw_list = []
        for obj in academic_class_section_mapping.objects.filter(
                year_name=student_detail.academic_class_section.year_name,
                class_name=student_detail.academic_class_section.class_name):
            raw_list.append(obj.section_name)
        section_obj_list = list(set(raw_list))

        if (student_rec.gender == 'Male'):
            male = 'checked'
            female = ""
        else:
            female = "checked"
            male = ""

        if (student_rec.emirati == 'yes'):
            emirati_yes = 'checked'
            emirati_no = ""
        else:
            emirati_no = "checked"
            emirati_yes = ""

        if (student_rec.select == 'yes'):
            select_arab = 'checked'
            select_nonarab = ""
        else:
            select_nonarab = "checked"
            select_arab = ""

        if (student_rec.sen == 'yes'):
            sen_yes = 'checked'
            sen_no = ""
        else:
            sen_no = "checked"
            sen_yes = ""

        if (student_rec.transport == 'own'):
            transport_own = 'checked'
            transport_school = ""
        else:
            transport_school = "checked"
            transport_own = ""

        return render(request, "updated_student_details.html",
                      {'student_rec': student_rec, 'student_detail': student_detail, 'parent_list': parent_list, 'religion_list': religion_list,
                       'nationality_list': nationality_list, 'class_list': class_list, 'section_list': section_list,
                       'academicyr_list': academicyr_list, 'female': female, 'male': male,
                       'student_detail_form': student_detail_form, 'class_list': class_obj_list,
                       'section_list': section_obj_list, 'medical_rec': medical_detail, 'subject_details': subject_recs,
                       'mandatory_sub': mandatory_sub_list, 'emirati_no': emirati_no, 'emirati_yes': emirati_yes,
                       'select_arab': select_arab, 'select_nonarab': select_nonarab, 'sen_yes': sen_yes,
                       'sen_no': sen_no, 'transport_own': transport_own, 'transport_school': transport_school,
                       'house_list': house_list,'old_student_rec':old_student_rec})
    except:
        messages.warning(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
        return render(request, 'home.html')

            #
    # try:
    #         if request.POST['mydata'] and not request.FILES:
    #             dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
    #             # ext = filename.split('.')[-1]
    #             filename = "%s_%s.%s" % (request.POST.get('first_name'), dirname, 'png')
    #             raw_file_path_and_name = os.path.join('images', filename)
    #
    #             data = str(request.POST['mydata'])
    #             raw_data = base64.b64decode(data)
    #             f = open(settings.MEDIA_ROOT + raw_file_path_and_name, 'wb')
    #             f.write(raw_data)
    #             f.close()
    #
    #         if request.POST['id_photo_val']:
    #             dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
    #             # ext = filename.split('.')[-1]
    #             filename = "%s_%s.%s" % (request.POST.get('first_name'), dirname, 'png')
    #             raw_file_path_and_name = os.path.join('images', filename)
    #
    #             data = str(request.POST['id_photo_val'])
    #             temp_data = data.split('base64,')[1]
    #
    #             raw_data = base64.b64decode(temp_data)
    #             f = open(settings.MEDIA_ROOT + raw_file_path_and_name, 'wb')
    #             f.write(raw_data)
    #             f.close()
    #         student_detail_form = ChildDetailsForm(request.POST, request.FILES)
    #         # print ("errors",student_detail_form.errors)
    #         if student_detail_form.is_valid():
    #             # parent_student_details_update.objects.create(student_id=request.POST['student_id'],first_name= request.POST['first_name'], last_name = request.POST['last_name'],
    #             # father_name=request.POST['father_name'],mother_name=request.POST['mother_name'], birth_date=request.POST['birth_date'],
    #             # gender=request.POST['gender'],email=request.POST['email'],
    #             # parent_mail=request.POST['parent_mail'], street = request.POST['street'], city = request.POST['city'],
    #             # state= request.POST['state'],postcode=request.POST['postcode'], photo=request.FILES.get('photo'),
    #             # nationality_id=nationality.objects.get(pk=request.POST['nationality']),
    #             # religion_id=religion.objects.get(pk=request.POST['religion']),emirati = request.POST['emirati'],select=request.POST['select'], sen = request.POST['sen'],
    #             # transport=request.POST['transport'],update_approved=False, update_request=True)
    #
    #             student_id = request.POST['student_id']
    #             student_detail_obj = student_details.objects.get(id=student_id)
    #             student_detail_obj.first_name = request.POST['first_name']
    #             student_detail_obj.last_name = request.POST['last_name']
    #             student_detail_obj.father_name = request.POST['father_name']
    #             student_detail_obj.mother_name = request.POST['mother_name']
    #             student_detail_obj.email = request.POST['email']
    #             student_detail_obj.gender = request.POST['gender']
    #             student_detail_obj.emirati = request.POST['emirati']
    #             student_detail_obj.select = request.POST['select']
    #             student_detail_obj.sen = request.POST['sen']
    #             student_detail_obj.transport = request.POST['transport']
    #
    #             student_detail_obj.birth_date = request.POST['birth_date']
    #             # parent_code = request.POST['parent_name']
    #             # if parent_code:
    #             #     student_detail_obj.parent = parents_details.objects.get(pk=request.POST['parent_name'])
    #             student_detail_obj.parent_mail = request.POST['parent_mail']
    #             student_detail_obj.religion = religion.objects.get(pk=request.POST['religion'])
    #             student_detail_obj.nationality = nationality.objects.get(pk=request.POST['nationality'])
    #             # student_detail_obj.house_name = house_master.objects.get(pk=request.POST['house_name'])
    #
    #             student_detail_obj.street = request.POST['street']
    #             student_detail_obj.city = request.POST['city']
    #             student_detail_obj.state = request.POST['state']
    #             student_detail_obj.postcode = request.POST['postcode']
    #             # student_detail_obj.joining_date = request.POST['joining_date']
    #             student_photo = request.FILES.get('photo')
    #
    #             if student_photo == None:
    #                 pass
    #             else:
    #                 student_detail_obj.photo = request.FILES.get('photo')
    #
    #             if request.POST['mydata'] and not request.FILES:
    #                 student_detail_obj.photo = raw_file_path_and_name
    #             student_detail_obj.save()
    #
    #             if request.POST['id_photo_val']:
    #                 student_detail_obj.photo = raw_file_path_and_name
    #                 student_detail_obj.save()
    #
    #             messages.success(request, "Update Request Has Been Sent. Record Will Be Updated After Admin Approval.")
    #
    #     except Exception as e:
    #         messages.success(request, "Form have some error" + str(e))
    #         return redirect('/school/view_student/')

    # return redirect('/school/view_student/')




def ViewUpdateParent(request):
    user = request.user

    parent_rec = parents_details.objects.get(user=user.id)
    try:
        parent_rec.birth_date = parent_rec.birth_date.strftime('%Y-%m-%d')
    except:
        parent_rec.birth_date=None

    student_list = student_details.objects.filter(parent__id=parent_rec.id)
    if (parent_rec.gender == 'False'):
        male = 'checked'
        female = ""
    else:
        female = "checked"
        male = ""

    flag = False
    parect_flag = parent_notification_log.objects.filter(user_id_id=request.user.id, is_active=True)
    total_active_count = parect_flag.count()
    if parect_flag:
        flag = True

    else:
        flag = False
    secondary_parentdetail_formset = modelformset_factory(secondary_parent_details, form=SecondaryParentDetailsForm,extra=0)
    secondary_parentdetail_formset = secondary_parentdetail_formset(prefix='sec_parent',queryset=parent_rec.secondary_parent_ids.all())
    return render(request, "before_parent_update_details.html",
                  {'parent_detail': parent_rec, 'path':'http://'+request.META['HTTP_HOST']+'/school/media/','female': female, 'male': male,
                   'stu_rec':student_list,'secondary_parentdetail_formset':secondary_parentdetail_formset,'flag':flag,'total_active_count':total_active_count })


def SaveUpdatedParentPortal(request):

        if request.method == 'POST':
            par_id = request.POST['parent_id']

            updated_parent_rec = updated_parent_details.objects.filter(parent_id=request.POST['parent_id'])
            updated_parent_details.objects.filter(parent_id=request.POST['parent_id']).delete()
            if not updated_parent_rec:
                # parent_rec = updated_parent_details.objects.get(parent_id=request.POST['parent_id'])
                parent_detail_form = UpdatedParentDetailsForm(request.POST)
                if AuthUser.objects.filter(~Q(id=par_id),email=str(request.POST['email'])).exists():
                    student_detail_form = UpdatedParentDetailsForm()
                    stu_rec = parents_details.objects.get(user=par_id)
                    stu_rec.birth_date = stu_rec.birth_date.strftime('%Y-%m-%d')
                    if (stu_rec.gender == 'False'):
                        male = 'checked'
                        female = ""
                    else:
                        female = "checked"
                        male = ""

                    messages.success(request, "Email_id already exist.")
                    return render(request, "before_parent_update_details.html", {'parent_detail': stu_rec,  'female': female, 'male': male,'stu_rec':stu_rec})

                else:
                    if parent_detail_form.is_valid():

                        student_detail_obj = parent_detail_form.save(commit=False)
                        student_detail_obj.parent_id = request.POST['parent_id']
                        student_detail_obj.save()


                        if request.POST['profile_pic']:
                            dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
                            filename = "%s_%s.%s" % (request.user.first_name, dirname, 'png')
                            raw_file_path_and_name = os.path.join('images', filename)
                            data = str(request.POST['profile_pic'])
                            temp_data = data.split('base64,')[1]
                            raw_data = base64.b64decode(temp_data)
                            f = open(settings.MEDIA_ROOT + raw_file_path_and_name, 'wb')
                            f.write(raw_data)
                            f.close()
                            student_detail_obj.photo = raw_file_path_and_name
                            student_detail_obj.save()




                        messages.success(request, "Parent change request is send for Admin Approval.")
            # else:
            #     parent_detail_form = UpdatedParentDetailsForm(request.POST)
            #     if parent_detail_form.is_valid():
            #         updated_parent_details.objects.filter(parent_id=par_id).update(first_name=request.POST['first_name'],middle_name=request.POST['middle_name'],last_name=request.POST['last_name'],contact_num=request.POST['contact_num'],birth_date=request.POST['birth_date'],email=request.POST['email'],street=request.POST['street'],city=request.POST['city'],state=request.POST['state'],postcode=request.POST['postcode'],gender=request.POST['gender'])
            #     messages.success(request, "Parent change request is send for Admin Approval.")
        return redirect('/school/view_update_parent/')


def UpdatedParentDetails(request, std_id):

    try:
        stu_rec = updated_parent_details.objects.get(id=std_id)
        parent_rec_id = stu_rec.parent_id
        old_student_rec = parents_details.objects.get(user=parent_rec_id)
        parent_code = old_student_rec.code
        try:
            old_student_rec.birth_date = old_student_rec.birth_date.strftime('%Y-%m-%d')
        except:
            old_student_rec.birth_date=None

        stu_rec.birth_date = stu_rec.birth_date.strftime('%Y-%m-%d')
        if (stu_rec.gender == 'False'):
            male = 'checked'
            female = ""
        else:
            female = "checked"
            male = ""

        return render(request, "parent_update_details.html",{'parent_detail': stu_rec, 'female': female, 'male': male, 'stu_rec': stu_rec,'parent_code':parent_code,'old_student_rec':old_student_rec})

    except:
        messages.warning(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
        return render(request, 'home.html')

def ParentNotificationLog(request):
    user = request.user
    parent_notification_logs = parent_notification_log.objects.filter(user_id=user.id).order_by('-last_updated')
    for parent_rec in parent_notification_logs:
        parent_notification_log.objects.filter(id=parent_rec.id).update(is_active=False)
    return render(request, "parent_notification_log.html",{'parent_notification_logs':parent_notification_logs})
from django import forms
from registration.models import *
from attendance.models import *
from assessment.models import *



class GradeDetailsForm(forms.ModelForm):
    
    code = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off'}))
    grade_name = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off' ,'maxlength':'32','title':'Only Alpha-Numeric is Allowed With "-" and "_"', 'pattern':'^[a-zA-Z](?!\d*[-_]\d*$)[ \w-]*$'}))
    
    
    class Meta:
        model = grade_details
        fields =[ 'code','grade_name']


# class GradeSubDetailsForm(forms.ModelForm):
#     marks_greater = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control col-6 col-sm-3' , 'autocomplete': 'off'}))
#     marks_less = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control col-6 col-sm-3' , 'autocomplete': 'off'}))
#     grades = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control col-6 col-sm-3' , 'autocomplete': 'off'}))
#     color_code = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control col-6 col-sm-3' , 'autocomplete': 'off'}))
#     color = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control col-6 col-sm-3' , 'autocomplete': 'off'}))
#
#     class Meta:
#         model = grade_sub_details
#         fields =['marks_greater' , 'marks_less','grades','color_code','color']


class FrequencyDetailsForm(forms.ModelForm):
    frequency_name = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','title':'Only Alpha-Numeric is Allowed.', 'pattern': "[a-zA-Z0-9 ]+",'maxlength':'32'}))
    from_date = forms.DateField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off' ,'type':'date'}))
    to_date = forms.DateField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off' ,'type':'date'}))

    class Meta:
        model = frequency_details
        fields =[ 'frequency_name','from_date','to_date']


class ExamDetailsForm(forms.ModelForm):
    frequency_recs = frequency_details.objects.all()
    exam_recs = exam_type.objects.all()
    exam_code = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control', 'autocomplete': 'off','title':'Only Numbers or Alpha-Numeric is Allowed With "-" and "_"', 'pattern':'^[\d]+$|^(?!\d*[-_]*\d*$)[ \w_-]+$','maxlength':'32'}))
    exam_name = forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off','maxlength':'32','title':'Only Alpha-Numeric is Allowed With "-" and "_"', 'pattern':'^[\w](?!\d*[-_]+\d*$)[ \w-]+$'}))
    # frequency = forms.ModelChoiceField(required=True, queryset=frequency_recs,
    #                                      widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))
    exam_type = forms.ModelChoiceField(required=True, queryset=exam_recs,
                                       widget=forms.Select(attrs={'class': 'form-control', 'autocomplete': 'off'}))

    frequency_mapping = forms.ModelMultipleChoiceField(required=True, queryset=frequency_recs, widget=forms.SelectMultiple(
        attrs={'class': 'form-control', 'autocomplete': 'off'}))

    class Meta:
        model = exam_details
        fields =['exam_code','exam_name','exam_type','frequency_mapping']


class SkillExcelForm(forms.ModelForm):
    excel = forms.FileField(required=True, label='Upload File')
    class Meta:
        model = skill_excel
        fields = ['excel']

# class QuestionForm(forms.ModelForm):
#     image = forms.FileField(required=True,label='Upload question image')
#     class Meta:
#         model = question
#         fields = ['image']

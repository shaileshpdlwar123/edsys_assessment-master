from __future__ import unicode_literals

from django.db import models
from registration.models import *
from masters.models import *
import collections

class grade_details(models.Model):
    code = models.CharField(max_length=250)
    grade_name = models.CharField(max_length=250)
    is_archive = models.BooleanField(default=False)

    def __unicode__(self):
        return self.grade_name


class grade_sub_details(models.Model):
    grade = models.ForeignKey(grade_details,related_name='grade_name_rel')
    marks_greater = models.CharField(max_length=5)
    marks_less = models.CharField(max_length=5)
    grades = models.CharField(max_length=25)
    color_code = models.CharField(max_length=25)
    grade_point = models.CharField(max_length=5,null=True)


class frequency_details(models.Model):
    frequency_name = models.CharField(max_length=250)
    from_date = models.CharField(max_length=50)
    to_date = models.CharField(max_length=50)

    def __unicode__(self):
        return self.frequency_name

class sub_topic(models.Model):
    name = models.CharField(max_length=250)
    mark = models.CharField(max_length=250)

    def to_dict(self):
        res = {
            'name': self.name,
            'mark': self.mark,
            # 'color': self.color
        }
        return res

class topic(models.Model):
    topic_name = models.CharField(max_length=250)
    sub_topic_ids = OneToManyField(sub_topic)

    def to_dict(self):
        res = {
            'topic_name': self.topic_name,
            'sub_topic_ids': to_dict_list(self.sub_topic_ids.all()),
        }
        return res

class sub_strand(models.Model):
    sub_strand_name = models.CharField(max_length=250)
    topic_ids = OneToManyField(topic)

    def to_dict(self):
        res = {
            'sub_strand_name': self.sub_strand_name,
            'topic_ids': to_dict_list(self.topic_ids.all()),
        }
        return res

class strand(models.Model):
    strand_name = models.CharField(max_length=250)
    sub_strand_ids = OneToManyField(sub_strand)
    # topic_ids = OneToManyField(topic)

    def to_dict(self):
        res = {
            'strand_name': self.strand_name,
            'sub_strand_ids': to_dict_list(self.sub_strand_ids.all()),
        }
        return res

class scheme_frequecy(models.Model):
    frequency_name = models.ForeignKey(frequency_details, on_delete=models.PROTECT, null=True,related_name='scheme_frequency_rel')

class scheme(models.Model):
    code = models.CharField(max_length=250)
    scheme_name = models.CharField(max_length=250)
    is_submitted = models.BooleanField(default=False)
    subject_scheme_name = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True, blank=True)
    stand_ids = OneToManyField(strand)
    scheme_frequency_ids = OneToManyField(scheme_frequecy)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        # managed = False
        # db_table = 'scheme'

        permissions = (
            ('can_view_view_assessment', 'can view view assessment'),
            ('can_view_view_grade_categories', 'can view view grade categories'),
            ('can_view_view_cycle_categories', 'can view view cycle categories'),
            ('can_view_view_skill_categories', 'can view view skill categories'),
            ('can_view_view_exam_categories', 'can view view exam categories'),
            ('can_view_view_rubric_categories', 'can view view rubric categories'),
            ('can_view_view_configration_categories', 'can view view configration categories'),
            ('can_view_view_reported_categories', 'can view view reported categories'),
            ('can_view_view_target_categories', 'can view view target categories'),
            ('can_view_view_analysis', 'can view view analysis'),
            ('can_view_view_set_target_categories', 'can view view set_target categories'),
            ('can_view_add_grade_categories', 'can view add grade categories'),
            ('can_view_edit_grade_categories', 'can view edit grade categories'),
            ('can_view_view_skill_sub_definition', 'can view view skill sub definition'),
            ('can_view_view_skill_exam_mark', 'can view view skill exam mark'),
            ('can_view_view_exam_exam_mark', 'can view view exam exam mark'),
            ('can_view_view_rubric_exam_mark', 'can view view rubric exam mark'),

            ('can_view_view_skill_definition_list', 'can view view skill definition list'),
            ('can_view_view_skill_mapping_list', 'can view view skill mapping list'),
            ('can_view_view_skill_approval_list', 'can view view skill approval list'),
            ('can_view_view_skill_status_list', 'can view view skill status list'),

            ('can_view_view_create_exam_list', 'can view view create exam list'),
            ('can_view_view_exam_definition_list', 'can view view exam definition list'),
            ('can_view_view_exam_mapping_list', 'can view view exam mapping list'),
            ('can_view_view_exam_approval_list', 'can view view exam approval list'),
            ('can_view_view_exam_status_list', 'can view view exam status list'),

            ('can_view_view_rubric_category_list', 'can view view rubric category list'),
            ('can_view_view_rubric_score_list', 'can view view rubric score list'),
            ('can_view_view_rubric_definition_list', 'can view view rubric definition list'),
            ('can_view_view_rubric_mapping_list', 'can view view rubric mapping list'),
            ('can_view_view_rubric_approval_list', 'can view view rubric approval list'),
            ('can_view_view_rubric_status_list', 'can view view rubric status list'),


        )

    def to_dict(self):
        res = {
            'code': self.code,
            'scheme_name': self.scheme_name,
            'stand_ids': to_dict_list(self.stand_ids.all()),
        }
        return res


# class scheme_year_class_section_mapping:
#     section_name = models.ForeignKey(sections, on_delete=models.PROTECT, null=True, related_name='scheme_class_mapped_section')
#
# class scheme_year_class_mapping:
#     year = models.ForeignKey(academic_year, on_delete=models.PROTECT, null=True)
#     class_obj = models.ForeignKey(class_details, on_delete=models.PROTECT, null=True)
#     scction_ids = OneToManyField(scheme_year_class_section_mapping)

class scheme_mapping_details(models.Model):
    #academic_class_section_mapping = models.ForeignKey(academic_class_section_mapping,  on_delete=models.PROTECT)
    year = models.ForeignKey(academic_year, on_delete=models.PROTECT, null=True)
    class_obj = models.ForeignKey(class_details, on_delete=models.PROTECT, null=True)
    scheme_name = models.ForeignKey(scheme, on_delete=models.PROTECT, null=True)
    grade_name = models.ForeignKey(grade_details, on_delete=models.PROTECT, null=True)
    subject = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True, related_name='mapped_subject_relation')
    is_archived= models.BooleanField(default=False)

class scheme_em_subtopic_mark(models.Model):
    sub_topic = models.ForeignKey(sub_topic,  on_delete=models.PROTECT,null=True)
    subtopic_mark = models.CharField(max_length=5, null=True)


class scheme_subtopic_mark(models.Model):
    mark = models.CharField(max_length=5, null=True)
    subtopic = models.ForeignKey(scheme_em_subtopic_mark, on_delete=models.PROTECT, null=True)


class scheme_student_mark_mapping(models.Model):
    student = models.ForeignKey(student_details, on_delete=models.PROTECT, null=True)
    total_mark = models.CharField(max_length=5, null=True)
    out_off_mark = models.CharField(max_length=5, null=True)
    grade_mark = models.CharField(max_length=150, null=True)
    subtopic_ids = OneToManyField(scheme_subtopic_mark, related_name='scheme_subtopic_rel')

class scheme_academic_frequency_mapping(models.Model):
    academic_mapping = models.ForeignKey(academic_class_section_mapping, on_delete=models.PROTECT, null=True)
    scheme_mapping = models.ForeignKey(scheme_mapping_details, on_delete=models.PROTECT,  null=True)
    student_mark_ids = OneToManyField(scheme_student_mark_mapping)
    frequency = models.ForeignKey(frequency_details, null=True)
    is_submitted = models.BooleanField(default=False)
    is_reported_submitted = models.BooleanField(default=False)
    is_apprpved = models.BooleanField(default=False)
    is_rejected = models.BooleanField(default=False)
    is_archived = models.BooleanField(default=False)
    outoff_mark= models.CharField(max_length=5, null=True)
    submitted_by = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True, related_name='submitted_by_scheme_relation' )
    approved_by = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True, related_name='approved_by_scheme_relation' )
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    @property
    def sort_record(self):
        temp_list = []
        for obj in self.student_mark_ids.all():
            print(obj.student.first_name)
            temp_list.append(obj)

        return sorted(temp_list, key=lambda student: student.student.first_name)

    def getKey(customobj):
        return customobj.getKey()

    # @property
    # def sort_record(self):
    #     temp_list = []
    #     for obj in self.student_mark_ids.all():
    #         print(obj.student.first_name)
    #         temp_list.append(obj)
    #
    #     return sorted(temp_list, key=lambda student: student.student.first_name)

        # return sorted([obj for obj in self.student_mark_ids.all()] , reverse=False)

class skill_excel(models.Model):
    excel = models.FileField(upload_to='uploads/')


class exam_details(models.Model):
    exam_code = models.CharField(max_length=250)
    exam_name = models.CharField(max_length=250)
    exam_type = models.ForeignKey(exam_type, on_delete=models.PROTECT, null=True,related_name = 'exam_details_exam_type_relation')
    created = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
    last_modified = models.DateTimeField(auto_now=True, editable=False, null=False, blank=False)

    def __unicode__(self):
        return self.exam_name


class exam_details_frequency(models.Model):
    exam_details_name = models.ForeignKey(exam_details, null=True)
    frequency_details_name = models.ForeignKey(frequency_details, on_delete=models.PROTECT, null=True)
    created = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
    last_modified = models.DateTimeField(auto_now=True, editable=False, null=False, blank=False)


def to_dict_list(objects):
    """

    :param objects: object should define to_dict method
    :return: list of to_dict on each object
    """
    res = []
    if isinstance(objects, collections.Iterable):
        for object in objects:
            res.append(object.to_dict())
    elif objects:
        res.append(objects.to_dict())

    return res

class exam_sub_topic(models.Model):
    name = models.CharField(max_length=250)
    mark = models.CharField(max_length=250)

class exam_topic(models.Model):
    topic_name = models.CharField(max_length=250)
    sub_topic_ids = OneToManyField(exam_sub_topic)

class exam_sub_strand(models.Model):
    sub_strand_name = models.CharField(max_length=250)
    topic_ids = OneToManyField(exam_topic)

class exam_strand(models.Model):
    strand_name = models.CharField(max_length=250)
    sub_strand_ids = OneToManyField(exam_sub_strand)

    def __unicode__(self):
        return self.strand_name

class exam_scheme(models.Model):
    exam_scheme_code = models.CharField(max_length=250, null=True)
    exam_scheme_name = models.CharField(max_length=250, null=True)
    exam_id = models.ForeignKey(exam_details, on_delete=models.PROTECT, null=True)
    exam_type_id = models.ForeignKey(exam_type, on_delete=models.PROTECT, null=True)
    is_submitted = models.BooleanField(default=False)
    subject_exam_name = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True,blank=True)
    stand_ids = OneToManyField(exam_strand)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.exam_scheme_name

# class exam_mapping_details(models.Model):
#     academic_class_section_mapping = models.ForeignKey(academic_class_section_mapping,  on_delete=models.PROTECT,null=True,)
#     exam_scheme = models.ForeignKey(exam_scheme, on_delete=models.PROTECT, null=True)
#     grade_name = models.ForeignKey(grade_details, on_delete=models.PROTECT, null=True)
#     subject = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True)

############## Exam Scheme Mapping Update ###############

class exam_scheme_exam_name_mapping(models.Model):
    exam_name = models.ForeignKey(exam_details, on_delete=models.PROTECT, null=True,related_name='mapped_exam_name_relation')
    exam_scheme = models.ForeignKey(exam_scheme, on_delete=models.PROTECT, null=True,related_name='mapped_exam_scheme_name_relation')
    grade_name = models.ForeignKey(grade_details, on_delete=models.PROTECT, null=True, related_name='mapped_grading_scheme_name_relation')
    is_archived = models.BooleanField(default=False)


class exam_scheme_acd_mapping_details(models.Model):
    year = models.ForeignKey(academic_year, on_delete=models.PROTECT, null=True,related_name='mapped_year_mapping_relation')
    class_obj = models.ForeignKey(class_details, on_delete=models.PROTECT, null=True,related_name='mapped_class_mapping_relation')
    subject= models.ForeignKey(subjects, on_delete=models.PROTECT, null=True, related_name='mapped_subject_mapping_relation')
    exam_name_mapping_ids = OneToManyField(exam_scheme_exam_name_mapping,related_name='exam_scheme_mapp_exam_name_relation')


############################# OldOne ############################3


# class exam_scheme_mapping_details(models.Model):
#     #academic_class_mapping = models.ForeignKey(academic_class_section_mapping,  on_delete=models.PROTECT)
#     year = models.ForeignKey(academic_year, on_delete=models.PROTECT, null=True)
#     class_obj = models.ForeignKey(class_details, on_delete=models.PROTECT, null=True)
#     exam_scheme = models.ForeignKey(exam_scheme, on_delete=models.PROTECT, null=True)
#     exam_name_obj = models.ForeignKey(exam_details, on_delete=models.PROTECT, null=True)
#     grade_name = models.ForeignKey(grade_details, on_delete=models.PROTECT, null=True)
#     # subject = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True)
#
#
# class exam_scheme_subject_details(models.Model):
#     exam_scheme_obj = models.ForeignKey(exam_scheme_mapping_details, on_delete=models.PROTECT, null=True)
#     subject = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True)


class exam_scheme_em_subtopic_mark(models.Model):
    sub_topic = models.ForeignKey(exam_sub_topic,  on_delete=models.PROTECT,null=True)
    subtopic_mark = models.CharField(max_length=5, null=True)


class exam_scheme_subtopic_mark(models.Model):
    # sub_topic = models.ForeignKey(exam_sub_topic,  on_delete=models.PROTECT,null=True)
    # mark = models.CharField(max_length=5, null=True)
    mark = models.CharField(max_length=5, null=True)
    subtopic = models.ForeignKey(exam_scheme_em_subtopic_mark, on_delete=models.PROTECT, null=True)

class exam_scheme_student_mark_mapping(models.Model):
    student = models.ForeignKey(student_details, on_delete=models.PROTECT, null=True)
    total_mark = models.CharField(max_length=5, null=True)
    grade_mark = models.CharField(max_length=150, null=True)
    max_mark = models.CharField(max_length=5, null=True)
    subtopic_ids = OneToManyField(exam_scheme_subtopic_mark, related_name='exam_scheme_subtopic_rel')


class exam_scheme_academic_frequency_mapping(models.Model):
    academic_mapping = models.ForeignKey(academic_class_section_mapping,  null=True,on_delete=models.PROTECT)
    scheme_mapping = models.ForeignKey(exam_scheme_exam_name_mapping, on_delete=models.PROTECT, null=True,related_name='exam_scheme_mapping_relation')
    frequency = models.ForeignKey(frequency_details,  null=True)
    student_mark_ids = OneToManyField(exam_scheme_student_mark_mapping)
    is_submitted = models.BooleanField(default=False)
    is_apprpved = models.BooleanField(default=False)
    is_rejected = models.BooleanField(default=False)
    is_target_set = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    is_archived = models.BooleanField(default=False)
    submitted_by = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True,
                                     related_name='submitted_by_exam_relation')
    approved_by = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True,
                                    related_name='approved_by_exam_relation')

    @property
    def sort_record(self):
        temp_list = []
        for obj in self.student_mark_ids.all():
            print(obj.student.first_name)
            temp_list.append(obj)

        return sorted(temp_list, key=lambda student: student.student.first_name)



class rubric_sub_topic(models.Model):
    name = models.CharField(max_length=250)
    #mark = models.CharField(max_length=250)
    mark = models.ForeignKey(rubrics_master, on_delete=models.PROTECT, null=True,
                                       related_name='rubric_mark_relation')
    #color = models.CharField(max_length=250)

    def to_dict(self):
        res = {
            'name': self.name,
            'mark': self.mark,
            # 'color': self.color
        }
        return res

class rubric_topic(models.Model):
    topic_name = models.CharField(max_length=250)
    sub_topic_ids = OneToManyField(rubric_sub_topic)

    def to_dict(self):
        res = {
            'topic_name': self.topic_name,
            'sub_topic_ids': to_dict_list(self.sub_topic_ids.all()),
        }
        return res


class rubric_sub_strand(models.Model):
    sub_strand_name = models.CharField(max_length=250)
    topic_ids = OneToManyField(rubric_topic)

    def to_dict(self):
        res = {
            'sub_strand_name': self.sub_strand_name,
            'topic_ids': to_dict_list(self.topic_ids.all()),
        }
        return res



class rubric_strand(models.Model):
    strand_name = models.CharField(max_length=250)
    sub_strand_ids = OneToManyField(rubric_sub_strand)
    # topic_ids = OneToManyField(topic)

    def to_dict(self):
        res = {
            'strand_name': self.strand_name,
            'sub_strand_ids': to_dict_list(self.sub_strand_ids.all()),
        }
        return res



class rubric_scheme(models.Model):
    code = models.CharField(max_length=250)
    scheme_name = models.CharField(max_length=250)
    rubrics_category_name = models.ForeignKey(rubrics_category, on_delete=models.PROTECT, null=True)
    is_submitted = models.BooleanField(default=False)
    stand_ids = OneToManyField(rubric_strand)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def to_dict(self):
        res = {
            'code': self.code,
            'scheme_name': self.scheme_name,
            'stand_ids': to_dict_list(self.stand_ids.all()),
        }
        return res

class rubric_grade_details(models.Model):
    code = models.CharField(max_length=250)
    grade_name = models.CharField(max_length=250)

    def __unicode__(self):
        return self.grade_name


class rubric_grade_sub_details(models.Model):
    grade = models.ForeignKey(rubric_grade_details, models.DO_NOTHING)
    marks_greater = models.CharField(max_length=250)
    marks_less = models.CharField(max_length=250)
    grades = models.CharField(max_length=250)
    color_code = models.CharField(max_length=250)

class rubric_mapping_frequency(models.Model):
    frequency_name = models.ForeignKey(frequency_details, on_delete=models.PROTECT, null=True,related_name='rubric_frequency_rel')

class rubric_mapping_details(models.Model):
    # academic_class_section_mapping = models.ForeignKey(academic_class_section_mapping,  on_delete=models.PROTECT)
    year = models.ForeignKey(academic_year, on_delete=models.PROTECT, null=True)
    class_obj = models.ForeignKey(class_details, on_delete=models.PROTECT, null=True)
    rubric_name = models.ForeignKey(rubric_scheme, on_delete=models.PROTECT, null=True)
    grade_name = models.ForeignKey(grade_details, on_delete=models.PROTECT,  null=True)
    subject = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True, related_name='rubric_subject_relation')
    rubric_frequency_ids = OneToManyField(rubric_mapping_frequency)
    is_archived = models.BooleanField(default=False)


# class rubric_em_subtopic_mark(models.Model):
#     sub_topic = models.ForeignKey(rubric_sub_topic,  on_delete=models.PROTECT,null=True)
#     subtopic_mark = models.CharField(max_length=5,null=True)




    # mark = models.CharField(max_length=5, null=True)
    # subtopic = models.ForeignKey(rubric_em_subtopic_mark, on_delete=models.PROTECT, null=True)
    # # rubric_name = models.ForeignKey(rubrics_master, on_delete=models.PROTECT, null=True)
    # rubric_name = models.ForeignKey(rubric_sub_topic, on_delete=models.PROTECT, null=True)


class rubric_options(models.Model):
    options = models.ForeignKey(rubric_sub_topic, on_delete=models.PROTECT, null=True)
    questions = models.ForeignKey(rubric_topic, on_delete=models.PROTECT, null=True)

class rubric_student_mark_mapping(models.Model):
    student_option_ids = OneToManyField(rubric_options)
    student = models.ForeignKey(student_details, on_delete=models.PROTECT, null=True)
    total_mark = models.CharField(max_length=50, null=True)

# class rubric_student_mark_mapping_rubric_options(models.Model):
#     rubric_options = models.ForeignKey(rubric_options, on_delete=models.PROTECT, null=True,blank=True)
#     student_mark_mapping = models.ForeignKey(rubric_student_mark_mapping, on_delete=models.PROTECT,blank=True, null=True)

    # subtopic_ids = OneToManyField(rubric_subtopic_mark)

class rubric_academic_frequency_mapping(models.Model):
    academic_mapping = models.ForeignKey(academic_class_section_mapping, on_delete=models.PROTECT,null=True, related_name='acd_mapping_Relation')
    scheme_mapping = models.ForeignKey(rubric_mapping_details, on_delete=models.PROTECT,  null=True, related_name='rubric_mapping_relation')
    student_mark_ids = OneToManyField(rubric_student_mark_mapping)
    frequency = models.ForeignKey(frequency_details, null=True)
    rubrics_category_name = models.ForeignKey(rubrics_category, on_delete=models.PROTECT, null=True)
    is_submitted = models.BooleanField(default=False)
    is_apprpved = models.BooleanField(default=False)
    is_rejected = models.BooleanField(default=False)
    submitted_by = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True, related_name='submitted_by_rubric_relation' )
    approved_by = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True, related_name='approved_by_rubric_relation' )
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    is_archived = models.BooleanField(default=False)

    @property
    def sort_record(self):
        temp_list = []
        for obj in self.student_mark_ids.all():
            print(obj.student.first_name)
            temp_list.append(obj)

        return sorted(temp_list, key=lambda student: student.student.first_name)


    # color_code = models.CharField(max_length=250)


############### Reported Score ######################################

class reported_sub_details(models.Model):
    term_assessment = models.CharField(max_length=250,null=True,blank=True)
    weightage = models.CharField(max_length=5)
    exam_name = models.ForeignKey(exam_details,on_delete=models.PROTECT, null=True,related_name='report_creation_exam_name')
    frequency_name = models.ForeignKey(frequency_details,on_delete=models.PROTECT, null=True,related_name='report_creation_frequency_name')

class reported_score_details(models.Model):
    reported_score_id = OneToManyField(reported_sub_details)
    code = models.CharField(max_length=250)
    report_name = models.CharField(max_length=250)

    def __unicode__(self):
        return self.report_name





# class reported_exam_mapping_sub_details(models.Model):
#     exam= models.ForeignKey(exam_details, null=True)
#     frequency = models.ForeignKey(frequency_details, null=True,related_name='rep_exam_mapp_sub')
#     reported_score_sub = models.ForeignKey(reported_sub_details, null=True,related_name='reported_score_sub')
#
# class reported_exam_mapping(models.Model):
#     reported_exam_sub_id= OneToManyField(reported_exam_mapping_sub_details)
#     code = models.CharField(max_length=250)
#     report_exam_name = models.CharField(max_length=250)
#     reported_score = models.ForeignKey(reported_score_details, null=True,related_name='reported_score_exam_mapping')
#
#     def __unicode__(self):
#         return self.report_exam_name

class reported_exam_class_sub_mapping(models.Model):
    year = models.ForeignKey(academic_year, on_delete=models.PROTECT, null=True, related_name='year_rep_map')
    class_name = models.ForeignKey(class_details, on_delete=models.PROTECT, null=True, related_name='class_rep_map')
    subject = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True, related_name='subject_rep_map')
    exam_name = models.ForeignKey(exam_details, on_delete=models.PROTECT, null=True, related_name='exam_rep_map')
    frequency_name = models.ForeignKey(frequency_details, on_delete=models.PROTECT, null=True, related_name='frequency_rep_map')
    reported_exam_mapp = models.ForeignKey(reported_score_details, null=True,related_name='reported_score_mapping')
    grade_name = models.ForeignKey(grade_details, on_delete=models.PROTECT, null=True, related_name='exam_rep_grade')

class reported_system_required_exceptions(models.Model):
    year = models.ForeignKey(academic_year, on_delete=models.PROTECT, null=True, related_name='year_rep_sre')
    class_name = models.ForeignKey(class_details, on_delete=models.PROTECT, null=True, related_name='class_rep_sre')
    subject = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True, related_name='subject_rep_sre')
    exam_name = models.ForeignKey(exam_details, on_delete=models.PROTECT, null=True, related_name='exam_rep_sre')
    # report_sub_name = models.ForeignKey(reported_sub_details, on_delete=models.PROTECT, null=True, related_name='rel_report_sub')
    reported_exam_mapp = models.ForeignKey(reported_score_details, null=True, related_name='reported_score_sre')
    is_active = models.BooleanField(default=True)
    grade_name = models.ForeignKey(grade_details, on_delete=models.PROTECT, null=True, related_name='report_rep_grade')



class reported_user_defined_exceptions(models.Model):
    year = models.ForeignKey(academic_year, on_delete=models.PROTECT, null=True, related_name='year_user_sre')
    class_name = models.ForeignKey(class_details, on_delete=models.PROTECT, null=True, related_name='class_user_sre')
    section_name = models.ForeignKey(sections, on_delete=models.PROTECT, null=True, related_name='section_user_sre')
    subject = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True, related_name='subject_user_sre')
    exam_name = models.ForeignKey(exam_details, on_delete=models.PROTECT, null=True, related_name='exam_user_sre')
    reported_exam_mapp = models.ForeignKey(reported_score_details, null=True, related_name='reported_score_user_sre')
    frequency_name = models.ForeignKey(frequency_details, on_delete=models.PROTECT, null=True,related_name='frequency_user_map')


class sz_reported_score_academic_mapping(models.Model):
    academic_mapping = models.ForeignKey(academic_class_section_mapping, null=True, on_delete=models.PROTECT,
                                         related_name='rep_scr_acd_year')
    subject = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True, related_name='rep_scr_acd_sub')
    reported_exam_mapp = models.ForeignKey(reported_score_details, null=True, related_name='rep_scr_acd_rep_scr')
    grade_scheme = models.ForeignKey(grade_details, null=True, related_name='rep_scr_acd_grd')

    is_submitted = models.BooleanField(default=False)
    # is_apprpved = models.BooleanField(default=False)
    # is_rejected = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    submitted_by = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True,
                                     related_name='rep_scr_acd_sub_by')
    # approved_by = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True,
    #                                 related_name='rep_scr_acd_sub_by')

class sz_reported_score_acd_student_mapping(models.Model):
    rs_academic_mapping = models.ForeignKey(sz_reported_score_academic_mapping, null=True, on_delete=models.PROTECT, related_name='rep_scr_acd')
    student = models.ForeignKey(student_details, on_delete=models.PROTECT, null=True, related_name='rep_scr_acd_std_student')
    earned_mark = models.CharField(max_length=5, null=True)
    total_mark = models.CharField(max_length=5, null=True)
    weightage = models.CharField(max_length=5, null=True)
    grade_name = models.ForeignKey(grade_sub_details, null=True, related_name='rep_scr_acd_std_grd')


class sz_reported_score_acd_student_mark_mapping(models.Model):
    rs_academic_mapping = models.ForeignKey(sz_reported_score_academic_mapping, null=True, on_delete=models.PROTECT,related_name='rep_scr_acd_rec')
    term_ass = models.ForeignKey(reported_sub_details, null=True, on_delete=models.PROTECT,related_name='rep_scr_term_ass')
    obtained_mark = models.CharField(max_length=5, null=True)
    outoff_mark = models.CharField(max_length=5, null=True)


class reported_score_export_log(models.Model):
    uploaded_by = models.ForeignKey(AuthUser, on_delete=models.PROTECT, null=True)
    uploaded_on = models.DateTimeField(default=datetime.now)


############### Target Setting ######################################
class target_setting_sub_details(models.Model):
    exam_name = models.ForeignKey(exam_details, on_delete=models.PROTECT, related_name='target_exam_name')
    frequency_name = models.ForeignKey(frequency_details, on_delete=models.PROTECT, null=True,related_name='target_frequency_name')

class target_setting_details(models.Model):
    target_setting_id = OneToManyField(target_setting_sub_details)
    year_name = models.ForeignKey(academic_year, on_delete=models.PROTECT, related_name='target_year_name')
    class_name = models.ForeignKey(class_details, on_delete=models.PROTECT, null=True, related_name='target_class_name')
    subject_name = models.ForeignKey(subjects, on_delete=models.PROTECT, related_name='target_subject_name')
    report_score_name = models.ForeignKey(reported_score_details, on_delete=models.PROTECT, null=True, related_name='target_report_score_name')

#
# ############## Exam Scheme Mapping Update ###############
# class exam_scheme_subject_mapping_details(models.Model):
#     year_name = models.ForeignKey(academic_year, on_delete=models.PROTECT, null=True,related_name='mapped_year_mapping_relation')
#     class_name = models.ForeignKey(class_details, on_delete=models.PROTECT, null=True,related_name='mapped_class_mapping_relation')
#     subject_name = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True, related_name='mapped_subject_mapping_relation')
#
# class exam_scheme_subject_sub_mapping_details(models.Model):
#     exam_name = models.ForeignKey(exam_details, on_delete=models.PROTECT, null=True,related_name='mapped_exam_name_relation')
#     exam_scheme_name = models.ForeignKey(exam_scheme, on_delete=models.PROTECT, null=True,related_name='mapped_exam_scheme_name_relation')
#     grading_scheme_name = models.ForeignKey(grade_details, on_delete=models.PROTECT, null=True, related_name='mapped_grading_scheme_name_relation')
#     exam_scheme_subject_sub_mapping_ids = OneToManyField(exam_scheme_subject_mapping_details)


######## Target Setting Table ##############

class sz_target_setting_sub_sub_details(models.Model):
    frequency_name = models.ForeignKey(frequency_details, on_delete=models.PROTECT, null=True,blank=True,
                                       related_name='target_setting_sub_sub_frequency_name')
    exam_mapping_name = models.ForeignKey(exam_details, on_delete=models.PROTECT, null=True, blank=True,
                                          related_name='exam_details_rel')


class sz_target_setting_sub_details(models.Model):
    year_name = models.ForeignKey(academic_year, on_delete=models.PROTECT, null=True,blank=True,
                                  related_name='target_setting_sub_year')
    class_name = models.ForeignKey(class_details, on_delete=models.PROTECT, null=True,blank=True,
                                   related_name='target_setting_sub_class')
    subject_name = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True,blank=True,
                                     related_name='target_setting_sub_subject')
    target_setting_ids = OneToManyField(sz_target_setting_sub_sub_details, related_name='target_setting_mapp_relation')


class sz_target_setting_details(models.Model):
    target_rel_id = OneToManyField(sz_target_setting_sub_details,
                                   related_name='target_setting_sub_details_relation')
    code = models.CharField(max_length=250,null=True,blank=True)
    target_name = models.CharField(max_length=250,null=True,blank=True)


    def __unicode__(self):
        return self.target_name

class sz_set_target_setting_student(models.Model):
    student = models.ForeignKey(student_details, on_delete=models.PROTECT, null=True,blank=True)
    grade = models.ForeignKey(grade_sub_details, on_delete=models.PROTECT, null=True,blank=True)
    target_setting_details = models.ForeignKey(sz_target_setting_details, on_delete=models.PROTECT, null=True,blank=True)

class sz_set_stud_target_sub_sub_details(models.Model):
    frequency_name = models.ForeignKey(frequency_details, on_delete=models.PROTECT, null=True,blank=True,
                                           related_name='stud_target_frequency_name')
    exam_name = models.ForeignKey(exam_details, on_delete=models.PROTECT, null=True, blank=True,
                                      related_name='stud_target_exam_rel')
    grade_name = models.ForeignKey(grade_sub_details, on_delete=models.PROTECT, null=True, blank=True,
                                       related_name='stud_target_grade_sub_rel')

class sz_set_stud_target_sub_details(models.Model):
    student = models.ForeignKey(student_details, on_delete=models.PROTECT, null=True, blank=True)
    exam_grade = OneToManyField(sz_set_stud_target_sub_sub_details,
                                    related_name='stud_target_sub_sub_details')

class sz_set_stud_target_details(models.Model):
    academic_mapping = models.ForeignKey(academic_class_section_mapping, on_delete=models.PROTECT, null=True,
                                             blank=True)
    subject_name = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True, blank=True,
                                         related_name='stud_target_subject')
    target_name= models.ForeignKey(sz_target_setting_details, on_delete=models.PROTECT, null=True, blank=True,
                                         related_name='stud_target_setting_details')
    student_rel_id = OneToManyField(sz_set_stud_target_sub_details, related_name='stud_target_sub_details',)




#====================Target Setting New====================================

class sz_set_student_target_sub_details(models.Model):
    frequency_name = models.ForeignKey(frequency_details, on_delete=models.PROTECT, null=True,blank=True,
                                           related_name='stud_target_frequency_rel')
    exam_name = models.ForeignKey(exam_details, on_delete=models.PROTECT, null=True, blank=True,
                                      related_name='target_exam_rel')
    grade_name = models.ForeignKey(grade_sub_details, on_delete=models.PROTECT, null=True, blank=True,
                                       related_name='stud_target_grade_rel')


class sz_set_student_target_setting(models.Model):
    student = models.ForeignKey(student_details, on_delete=models.PROTECT, null=True, blank=True)
    target_sub_details = OneToManyField(sz_set_student_target_sub_details)


class sz_set_target_setting_frequency(models.Model):
    frequency_name = models.ForeignKey(frequency_details, on_delete=models.PROTECT, null=True, blank=True,
                                       related_name='target_frequency_rel')



class sz_set_target_setting(models.Model):
    academic_mapping = models.ForeignKey(academic_class_section_mapping, on_delete=models.PROTECT, null=True,
                                             blank=True)
    subject_name = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True, blank=True,
                                         related_name='target_subject_rel')
    students = OneToManyField(sz_set_student_target_setting)

    frequency_ids = OneToManyField(sz_set_target_setting_frequency, null=True,blank=True)

#*****************************Perodic_assesment_table******************************
def content_file_name_question_image(instance, filename):
    dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
    ext = filename.split('.')[-1]
    question_name = instance.question_name[0:10]
    filename = "%s_%s.%s" % (question_name, dirname, ext)
    return os.path.join('doc', filename)


class perodic_topics(models.Model):
   topic = models.CharField(max_length=256,null=True,blank=True)



class perodic_sub_strand(models.Model):
    sub_strand_name = models.CharField(max_length=250,null=True,blank=True)
    topic_ids = OneToManyField(perodic_topics)

    def to_dict(self):
        res = {
            'sub_strand_name': self.sub_strand_name,
            'topic_ids': to_dict_list(self.topic_ids.all()),
        }
        return res


class perodic_scheme_frequecy(models.Model):
    perodic_frequency_name = models.ForeignKey(frequency_details, on_delete=models.PROTECT, null=True,related_name='perodic_scheme_frequency_rel')



class perodic_strand(models.Model):
    strand_name = models.CharField(max_length=250,null=True,blank=True)
    sub_strand_ids = OneToManyField(perodic_sub_strand)

    def to_dict(self):
        res = {
            'strand_name': self.strand_name,
            'sub_strand_ids': to_dict_list(self.sub_strand_ids.all()),
        }
        return res

class perodic_assesment_data(models.Model):
    perodic_scheme_code = models.CharField(max_length=256,null=True,blank=True)
    perodic_scheme_name = models.CharField(max_length=256,null=True,blank=True)
    perodic_subject=models.ForeignKey(subjects, on_delete=models.PROTECT, null=True,blank=True,related_name ='perodic_subject_rel')
    perodic_strand = OneToManyField(perodic_strand)
    perodic_scheme_frequency_ids = OneToManyField(perodic_scheme_frequecy)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    is_submitted = models.BooleanField(default=False)



class answer(models.Model):
    answer = models.CharField(max_length=256,null=True,blank=True)
    correct_flag = models.BooleanField(default=False)
    postion = models.CharField(max_length=256,null=True,blank=True)



    def to_dict(self):
        res = {
            'id': self.id,
            'answer': self.answer,
            'correct_flag': self.correct_flag,
            'postion': self.postion,

        }
        return res

class question_list(models.Model):
    questions = models.CharField(max_length=256, null=True, blank=True)
    answer_response = models.CharField(max_length=256, null=True, blank=True)

class option_list(models.Model):
    option = models.CharField(max_length=256,null=True,blank=True)


class question(models.Model):
    ans = models.ManyToManyField(answer)
    possible_ans = models.ManyToManyField(option_list)
    question_name = models.CharField(max_length=256,null=True,blank=True)
    instruction =models.CharField(max_length=256,null=True,blank=True)
    question_list_option = models.ManyToManyField(question_list,null=True,blank=True)
    title = models.CharField(max_length=256,null=True,blank=True)
    correct_answer= models.CharField(max_length=256,null=True,blank=True)
    mark= models.CharField(max_length=256,null=True,blank=True)
    question_type = models.CharField(max_length=256,null=True,blank=True)
    question_photo = models.FileField(upload_to=content_file_name_question_image,null=True,blank=True)

    perodic_question_subject = models.ForeignKey(subjects, on_delete=models.PROTECT, null=True, blank=True,related_name='perodic_question_subject_rel')
    perodic_question_strand= models.ForeignKey(perodic_strand, on_delete=models.PROTECT, null=True, blank=True,related_name='perodic_strand_rel')
    perodic_question_substrand= models.ForeignKey(perodic_sub_strand, on_delete=models.PROTECT, null=True, blank=True,related_name='perodic_substrand_rel')
    perodic_question_topic= models.ForeignKey(perodic_topics, on_delete=models.PROTECT, null=True, blank=True,related_name='perodic_topic_rel')

    def to_dict(self):
        res = {
            'answer': to_dict_list(self.ans.all()),
            'question_type': self.question_type,
            'mark': self.mark,
            'question_name': self.question_name,
            'score': self.mark,
        }
        return res



class assesment(models.Model):
    title = models.CharField(max_length=256, null=True, blank=True)
    description = models.CharField(max_length=256, null=True, blank=True)
    duration =  models.TimeField(blank=True,null=True)
    created_by = models.CharField(max_length=256, null=True, blank=True)
    assessment_question = models.ManyToManyField(question,blank=True,null=True,related_name='assessment_question_rel')
    total_mark = models.CharField(max_length=256,null=True,blank=True)
    is_submit = models.BooleanField(default=False)
    assesment_date_time = models.DateTimeField(null=True,blank=True)
    is_active = models.BooleanField(default=False)
    assessment_code = models.CharField(max_length=256,null=True,blank=True)




class perodic_question_academic_class_section_mapping(models.Model):
    academic_class_section_mapping = models.ForeignKey(academic_class_section_mapping, on_delete=models.PROTECT, null=True, blank=True,related_name='perodic_academic_class_section_mapping')
    assesment = models.ForeignKey(assesment, on_delete=models.PROTECT, null=True, blank=True,related_name='perodic_question_rel')

    def to_dict(self):
        res = {
            'id': self.id,
            'title': self.assesment.title,
            'description': self.assesment.description,
            'duration': self.assesment.duration,
            'total_mark': self.assesment.total_mark,
            'is_submit': self.assesment.is_submit,
            'academic_class_section_mapping': self.academic_class_section_mapping.id,
            'assessment_question': to_dict_list(self.assesment.assessment_question.all())
        }
        return res

    def to_dict_brief(self):
        res = {
            "assessment_id": self.assesment.id,"assignment_code": "OC8DW7","assignedby": self.assesment.created_by,
            "assignedon": self.assesment.created_by,"duedate": "2018-10-30 00:00:00","title": self.assesment.title,
            "description": self.assesment.description,"durations": self.assesment.duration,
            "subject_id": "1", "createdby": "563","originallycreatedby": "563","copy_status": "0","vark": "0",
            "locked": "1","gradebook": "0","group_student_id": "795","user_id": "755",
            "createdby_name": self.assesment.created_by,"assignedby_name": "Anushree Dhople"

        }
        return res

    def to_dict_openassignments(self,student):
        res = {

                "id": self.id,"group_id": "313","student_id": student.id,
                "status":student.is_active,"group_left_request": "0",
                "createdAt": "2018-09-06 10:45:06","updatedAt": "2018-10-27 06:53:16",
                "group_name": "Redbytes Group","group_description": "All compulsary",
                "group_code": "AAW2HV","owner": "732","Archived": "0",
                "username": student.user.username,"email": student.email,

                "password": student.user.password,"emailVerified": "0",
                "registration_no": student.student_code,"grade":student.academic_class_section.class_name.class_name,
                "section":student.academic_class_section.section_name.section_name,"gender": student.gender,
                "nationality":student.nationality.nationality_name,"arabs":student.select,

                "emirates": student.emirati,"sen":student.sen,"dateof_birth": student.birth_date,"name": student.user.get_full_name(),
                "school": "2","type": student.user.role.get().name,"modifiedAt": student.update_date,"role_id": student.user.role.get().id,
                "assessment_id": self.assesment.id,"assignment_code":self.assesment.assessment_code ,
                "assignedby": "20","assignedon":self.assesment.assesment_date_time,"duedate": "2018-10-27 00:00:00",
                "title": self.assesment.title,"description":self.assesment.description,"durations": self.assesment.description,"subject_id": "sd","createdby":self.assesment.created_by,
                "originallycreatedby": "732","copy_status": "0","vark": "0","locked": "1","gradebook": "0",
                "group_student_id": "1006","user_id": student.user.id,"assignment_id": self.assesment.id,"createdby_name":self.assesment.created_by,"assignedby_name": "admin"

    }
        return res


class answer_by_student(models.Model):
    ans_by_student = models.CharField(max_length=256, null=True, blank=True)
    postion_by_student = models.CharField(max_length=256, null=True, blank=True)
    answer_id = models.ForeignKey(answer,on_delete=models.PROTECT, null=True, blank=True,related_name='student_given_ans_rel')

    def __unicode__(self):
        return self.ans_by_student


class student_answer(models.Model):
    particular_question = models.ForeignKey(question,on_delete=models.PROTECT, null=True, blank=True,related_name='perodic_student_question_rel')
    assessment = models.ForeignKey(assesment,on_delete=models.PROTECT, null=True, blank=True,related_name='perodic_student_answer_assessment_rel')
    student_given_answer = models.ManyToManyField(answer_by_student, null=True, blank=True)
    mark_obtain = models.CharField(max_length=256, null=True, blank=True)
    student_id = models.ForeignKey(student_details, on_delete=models.PROTECT, null=True, blank=True,related_name='perodic_student_answer_rel')

    # def __unicode__(self):
    #     return self.
    #



class student_grade(models.Model):

    time_spent = models.TimeField(blank=True,null=True)
    student_id = models.ForeignKey(student_details,on_delete=models.PROTECT, null=True, blank=True,related_name='perodic_student_rel')
    assessment = models.ForeignKey(assesment, on_delete=models.PROTECT, null=True, blank=True,related_name='perodic_student_assessment_rel')
    grade = models.CharField(max_length=256,null=True,blank=True)
    teachers_feedback = models.CharField(max_length=256,null=True,blank=True)
    approve_by =models.CharField(max_length=256,null=True,blank=True)
    outof = models.CharField(max_length=256,null=True,blank=True)





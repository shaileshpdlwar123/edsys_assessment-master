from django.db.models import Max
from django.db.models import Sum
from django.shortcuts import render
from django.shortcuts import render,redirect
from .forms import *
from django.forms.formsets import formset_factory
from django.contrib import messages
import ast
import datetime
from datetime import datetime,timedelta
from models import *
from datetime import datetime
from datetime import date
from django.conf import settings
from registration.decorators import user_login_required, user_has_permission, the_decorator, decorator, user_permission_required
import json
from django.http import HttpResponse
import itertools
from masters.models import *
import django_excel as excel
from django.http import JsonResponse
from masters.utils import *
import math
from collections import defaultdict
import decimal
from django.utils import timezone


@user_login_required
@user_permission_required('assessment.can_view_view_assessment', '/school/home/')


# @user_login_required
def AssessmentSubTile(request):
    xyz = [] #get_all_user_permissions(request)
    return render(request, "assessment_sub_tile.html",{'perms': list(xyz)})


@user_login_required
@user_permission_required('assessment.can_view_add_grade_categories', '/school/home/')
def AddGrade(request):    
    return render(request, "add_grade.html")


@user_login_required
@user_permission_required('assessment.add_grade_details', '/school/home/')
def SaveGrade(request):

    code = request.POST.getlist('code')
    code_name = request.POST.getlist('code_name')
    greater_mark = request.POST.getlist('greater_mark')
    less_mark = request.POST.getlist('less_mark')
    grade = request.POST.getlist('grade')
    color = request.POST.getlist('color')
    grade_point = request.POST.getlist('grade_point')

    if grade_details.objects.filter(grade_name=code_name[0]).exists():
        messages.success(request, "This Grade Name is Already Exists")
        return redirect('/school/grade_list/')

    form = grade_details(grade_name= code_name[0])
    form.save()

    # print 'form--', form.id
    grade_id = form.id

    if (grade_id >= 1) and (grade_id <= 9):
        grade_code = "G-000" + str(grade_id)
        grade_details.objects.filter(id = grade_id).update(code = grade_code)

    elif (grade_id >= 10) and (grade_id <= 99):
        grade_code = "G-00" + str(grade_id)
        grade_details.objects.filter(id=grade_id).update(code=grade_code)

    elif (grade_id >= 100) and (grade_id <= 999):
        grade_code = "G-0" + str(grade_id)
        grade_details.objects.filter(id=grade_id).update(code=grade_code)

    list_size = len(greater_mark)

    for count in range(list_size):
        grade_slab_form = grade_sub_details(grade_id = grade_id, marks_greater= greater_mark[count], marks_less = less_mark[count], grades =  grade[count], color_code =  color[count], grade_point = grade_point[count] )
        grade_slab_form.save()
    return redirect('/school/grade_list/')


@user_login_required
@user_permission_required('assessment.can_view_view_grade_categories', '/school/home/')
def GradeList(request):
    grade_list = grade_details.objects.all()
    return render(request, "grade_list.html" ,{'Gradelist':grade_list})

@user_login_required
def ArchiveGrade(request,grade_id):
    grd_archive = grade_details.objects.get(id=grade_id).is_archive
    if grd_archive:
        grade_details.objects.filter(id=grade_id).update(is_archive=False)
        messages.success(request, 'Grade Scheme is Un-Archived.')
    else:
        grade_details.objects.filter(id=grade_id).update(is_archive = True)
        messages.success(request, 'Grade Scheme is Archived.')
    return redirect('/school/edit_grade/'+str(grade_id))

@user_login_required
@user_permission_required('assessment.delete_grade_details', '/school/home/')
def DeleteGrade(request, grade_id):
    try:
        grade_details.objects.filter(id=grade_id).delete()
        # parent_student_details_update.objects.filter(student_id=std_id).delete()
        messages.success(request, 'Grade Scheme Deleted Successfully.')
    except:
        messages.warning(request,'This Grade Cannot Be Deleted Because Its Mapped Somewere.')
    return redirect('/school/grade_list/')
    # return render(request, "grade_list.html" ,{'Gradelist':grade_list})


@user_login_required
@user_permission_required('assessment.can_view_edit_grade_categories', '/school/home/')
def EditGrade(request, grade_id):
    is_mapped = False
    GradeSubDetail = grade_sub_details.objects.filter(grade = grade_id)
    GradeDetail = grade_details.objects.get(id = grade_id)
    schm_grade = scheme_mapping_details.objects.filter(grade_name =grade_id)
    exm_grade = exam_scheme_exam_name_mapping.objects.filter(grade_name = grade_id)
    rub_grade = rubric_mapping_details.objects.filter(grade_name=grade_id)

    if schm_grade or exm_grade or rub_grade:
        is_mapped = True
    return render(request, "grade_edit.html" ,{'GradeSubDetail':GradeSubDetail,'GradeDetail':GradeDetail, 'is_mapped':is_mapped})


@user_login_required
def SaveEditGrade(request):
    return redirect('/school/grade_list/')

@user_login_required
def ValidateGradeNameExist(request):
    flag = None
    grade_name = request.POST.get('grade_name', None)
    if grade_details.objects.filter(grade_name=grade_name).exists():
        flag = True
    else:
        flag=False
    # print "flag---> ",flag
    return JsonResponse(flag, safe=False)


@user_login_required
@user_permission_required('assessment.change_grade_sub_details', '/school/home/')
def SaveEditGradeSub(request):
    if request.is_ajax():
        if request.method == 'POST':
            data = request.POST['table']
            dict_data = ast.literal_eval(data)

            for obj in dict_data:
                Id = obj['id']

                marks_greater = obj['marks_greater']
                marks_less = obj['marks_less']
                grades = obj['grades']
                grade_point = obj['grade_points']
                # object = grades.split('+')
                color_code = obj['color_code']

                # color = obj['color']

                if (marks_less == '') or (marks_greater == '') or (grades == '') or (color_code == ''):
                    messages.success(request, "Records Not Updated... Please fill all the records")
                else:
                    # messages.success(request, "Records Updated.")
                    grade_sub_details.objects.filter(id=Id).update(marks_greater=marks_greater, marks_less=marks_less,
                                                                   grades=grades, color_code=color_code, grade_point=grade_point)

    return redirect('/school/grade_list/')


@user_login_required
@user_permission_required('assessment.can_view_view_cycle_categories', '/school/home/')
def FrequencyList(request):
    frequency_recs = frequency_details.objects.all()
    try:
        current_acd_year = academic_year.objects.get(current_academic_year=True)

        month_list=[]
        for rec in frequency_recs:
            freq_name = rec.frequency_name
            start_month = month.objects.get(id=rec.from_date)
            end_month = month.objects.get(id =rec.to_date)
            month_dict={
                'id':rec.id,
                'freq_name':freq_name,
                'start_month':start_month,
                'end_month':end_month
            }
            month_list.append(month_dict)
        return render(request, "frequency_list.html" ,{'Frequencylist':frequency_recs,'month_list':month_list})
    except:
        messages.warning(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
        return render(request,'assessment_sub_tile.html')


@user_login_required
def AddFrequency(request):    
    current_acd_year = academic_year.objects.get(current_academic_year=True)
    ay_start_month = current_acd_year.start_date.month
    ay_end_month = current_acd_year.end_date.month

    flag = True
    counter =0
    count =ay_start_month
    month_list_id =[]
    month_recs =[]
    month_list_id.append(ay_start_month)

    while flag:
        if count == ay_end_month:
            flag =False
            # print 'gg'
        else:
            if count != 12:
                count = count+1
                month_list_id.append(count)
            else:
                count = 1
                month_list_id.append(count)

    for id in month_list_id:
        counter += 1
        mon_obj = month.objects.get(id = id)
        mon_dic ={
            'counter':counter,
            'month_rec': mon_obj
        }
        month_recs.append(mon_dic)
    return render(request, "add_frequency.html" ,{'month_recs':month_recs})

@user_login_required
def ValidateFrequencyNameExist(request):
    flag = None
    frequency_name = request.POST.get('frequency_name', None)
    if frequency_details.objects.filter(frequency_name=frequency_name).exists():
        flag = True
    else:
        flag=False
    # print "flag---> ",flag
    return JsonResponse(flag, safe=False)

@user_login_required
def SaveFrequency(request):
    try:
     if(request.method=='POST'):
         name = request.POST.get('frequency_name')
         from_date = request.POST.get('from_date')
         to_date = request.POST.get('to_date')


         if frequency_details.objects.filter(frequency_name=name[0]).exists():
             messages.warning(request, "This Frequency Name is Already Exist")
             return redirect('/school/add_frequency/')

         elif frequency_details.objects.filter(from_date=from_date,to_date=to_date):
             current_acd_year = academic_year.objects.get(current_academic_year=True)
             ay_start_month = current_acd_year.start_date.month
             ay_end_month = current_acd_year.end_date.month

             flag = True
             counter = 0
             count = ay_start_month
             month_list_id = []
             month_recs = []
             month_list_id.append(ay_start_month)

             while flag:
                 if count == ay_end_month:
                     flag = False
                     # print 'gg'
                 else:
                     if count != 12:
                         count = count + 1
                         month_list_id.append(count)
                     else:
                         count = 1
                         month_list_id.append(count)

             for id in month_list_id:
                 counter += 1
                 mon_obj = month.objects.get(id=id)
                 mon_dic = {
                     'counter': counter,
                     'month_rec': mon_obj
                 }
                 month_recs.append(mon_dic)


             messages.warning(request, "Record with same From-To month already exists")
             return redirect('/school/add_frequency/')
             # return render(request, "add_frequency.html", {'month_recs': month_recs,'selected_name':name})

         frequency_details.objects.create(frequency_name=name,from_date=from_date,to_date=to_date)

         messages.success(request, "One record added successfully")
         return redirect('/school/frequency_list/')
    except:
         messages.warning(request, "Some Error Occurred. Please Try Again")
         return redirect('/school/frequency_list/')

@user_login_required
def DeleteFrequency(request, frequency_id):
    try:
        frequency_details.objects.get(id=frequency_id).delete()
        messages.success(request, 'Frequency Deleted Successfully.')
    except:
        messages.warning(request,'This Frequency Cannot Be Deleted Because Its Mapped Somewere.')
    return redirect('/school/frequency_list/')

@user_login_required
def EditFrequency(request, frequency_id):

    is_mapped = False
    frequency_recs = frequency_details.objects.get(id=frequency_id)

    schm_acd_mpp =  scheme_academic_frequency_mapping.objects.filter(frequency=frequency_id)
    exm_details = exam_details_frequency.objects.filter(frequency_details_name=frequency_id)
    rub_grade = rubric_academic_frequency_mapping.objects.filter(frequency=frequency_id)

    if schm_acd_mpp or exm_details or rub_grade:
        is_mapped = True

    current_acd_year = academic_year.objects.get(current_academic_year=True)

    ay_start_month = current_acd_year.start_date.month
    ay_end_month = current_acd_year.end_date.month

    flag = True
    counter =0
    count = ay_start_month
    month_list_id = []
    month_recs = []
    month_list_id.append(ay_start_month)
    while flag:
        if count == ay_end_month:
            flag = False
        else:
            if count != 12:
                count = count + 1
                month_list_id.append(count)
            else:
                count = 1
                month_list_id.append(count)

    for id in month_list_id:
        counter += 1
        mon_obj = month.objects.get(id=id)
        mon_dic = {
            'counter': counter,
            'month_rec': mon_obj
        }
        month_recs.append(mon_dic)

    start_month = month.objects.get(id=frequency_recs.from_date)
    end_month = month.objects.get(id=frequency_recs.to_date)

    return render(request, "frequency_edit.html", {'is_mapped':is_mapped,'FrequencyDetail':frequency_recs, 'start_month': start_month,'end_month':end_month, 'month_recs':month_recs})

@user_login_required
def SaveEditFrequency(request):
    try:
        name = request.POST['frequency_name']
        frequency_id = request.POST['frequency_id']
        from_dates = request.POST['from_date']
        to_dates = request.POST['to_date']
        # class_ids = request.POST.getlist('classes')

        if frequency_details.objects.filter(~Q(id=frequency_id),from_date=from_dates, to_date=to_dates):

            is_mapped = False
            frequency_recs = frequency_details.objects.get(id=frequency_id)

            schm_acd_mpp = scheme_academic_frequency_mapping.objects.filter(frequency=frequency_id)
            exm_details = exam_details.objects.filter(frequency=frequency_id)
            rub_grade = rubric_academic_frequency_mapping.objects.filter(frequency=frequency_id)

            if schm_acd_mpp or exm_details or rub_grade:
                is_mapped = True

            current_acd_year = academic_year.objects.get(current_academic_year=True)

            ay_start_month = current_acd_year.start_date.month
            ay_end_month = current_acd_year.end_date.month

            flag = True
            counter = 0
            count = ay_start_month
            month_list_id = []
            month_recs = []
            month_list_id.append(ay_start_month)
            while flag:
                if count == ay_end_month:
                    flag = False
                else:
                    if count != 12:
                        count = count + 1
                        month_list_id.append(count)
                    else:
                        count = 1
                        month_list_id.append(count)

            for id in month_list_id:
                counter += 1
                mon_obj = month.objects.get(id=id)
                mon_dic = {
                    'counter': counter,
                    'month_rec': mon_obj
                }
                month_recs.append(mon_dic)

            start_month = month.objects.get(id=frequency_recs.from_date)
            end_month = month.objects.get(id=frequency_recs.to_date)
            messages.warning(request, "Record with same From-To month already exists")


            return render(request, "frequency_edit.html",
                          {'is_mapped': is_mapped, 'FrequencyDetail': frequency_recs, 'start_month': start_month,
                           'end_month': end_month, 'month_recs': month_recs})


            # return redirect('/school/frequency_list/')
        else:

            frequency_obj =frequency_details.objects.get(id = frequency_id)
            frequency_details.objects.filter(id = frequency_id ).update(frequency_name = name, from_date = from_dates, to_date = to_dates )

    except:
        messages.warning(request,"Some Error Occurred. Please Try Agian.")

    return redirect('/school/frequency_list/')

@user_login_required
def CreateExam(request):
    # print "I am From Create Exam"
    frequency_recs=frequency_details.objects.all()

    frequency_form = FrequencyDetailsForm()
    return render(request, "create_exam.html",{'FrequencyDetails':frequency_recs})

@user_login_required
@user_permission_required('assessment.can_view_view_skill_categories', '/school/home/')
def SkillTrackerSubTile(request):
    xyz = [] #get_all_user_permissions(request)
    return render(request, "skill_tracker_sub_tile.html", {'perms': list(xyz)})

@user_login_required
def SchemeDefinition(request):
    return render(request, "scheme_definition.html")



@user_login_required
def SaveSkillTracker(request):
    try:
        if request.method == 'POST':
            scheme_code  = request.POST.get('scheme_code')
            scheme_name1 = request.POST.get('scheme_name')
            submit_rec = request.POST.get('submit_rec')
            val_dict =  json.loads(request.POST['data'])
            subject_name = request.POST.get('subject_name')
            frequency_data = json.loads(request.POST.get('frequency_data'))
            scheme_obj = scheme.objects.create(code=scheme_code, scheme_name=scheme_name1,subject_scheme_name_id=subject_name)
            frequency_size = len(frequency_data)
            for count in range(frequency_size):
                scheme_frequency_obj = scheme_frequecy.objects.create(frequency_name_id=frequency_data[count])
                scheme_obj.scheme_frequency_ids.add(scheme_frequency_obj.id)

            for data in val_dict:
                strand_data = strand.objects.create(strand_name=data['strand'])
                scheme_obj.stand_ids.add(strand_data.id)

                for substrand in data['substrand']:
                    sub_str = sub_strand.objects.create(sub_strand_name = substrand['substrand_name'])
                    strand_data.sub_strand_ids.add(sub_str.id)

                    for recs in substrand['topic']:
                        topic_name1 = recs['topic_name']
                        topic_table = topic.objects.create(topic_name=topic_name1)
                        sub_str.topic_ids.add(topic_table.id)

                        for subtopic_rec in recs['subtopic']:
                            sub_topic_name = subtopic_rec['value']
                            mark_val = subtopic_rec['mark']
                            # color_val = subtopic_rec['color']
                            sub_topic_table = sub_topic.objects.create(name = sub_topic_name, mark = mark_val)
                            topic_table.sub_topic_ids.add(sub_topic_table.id)

            # if submit_rec == 'true':
            scheme.objects.filter(id=scheme_obj.id).update(is_submitted=True)
            # messages.success(request, "Record Save and Submitted Successfully.")
        # else:
            messages.success(request, "Record Saved Successfully.")

    except:
        messages.warning(request, "Some Error Occurred. Please Try again.")
    return HttpResponse(json.dumps(scheme_obj.to_dict()), content_type='application/json')

@user_login_required
def ValidateSchemeCodeExist(request):
    flag = None
    scheme_code = request.POST.get('scheme_code', None)
    if scheme.objects.filter(code=scheme_code).exists():
        flag = True
    else:
        flag=False
    # print "flag---> ", flag
    return JsonResponse(flag, safe=False)


@user_login_required
def ValidateSchemeNameExist(request):
    flag = None
    scheme_name = request.POST.get('scheme_name', None)
    if scheme.objects.filter(scheme_name=scheme_name).exists():
        flag = True
    else:
        flag=False
    # print "flag---> ",flag
    return JsonResponse(flag, safe=False)


@user_login_required
@user_permission_required('assessment.can_view_view_skill_definition_list', '/school/home/')
def SchemeDefinitionList(request):
    scheme_recs = scheme.objects.all()

    return render(request, "scheme_list.html", {'scheme_recs': scheme_recs})

class make_incrementor(object):
    count = 0

    def __init__(self, start):
        self.count = start

    def inc(self, jump=1):
        self.count += jump
        return self.count

    def res(self):
        self.count = 0
        return self.count

    def disp(self):
        # self.count = 0
        return self.count

@user_login_required
def ViewGetSchemeFreq(request):
    freq_recs = []
    scheme_id = request.POST.get('scheme_id')
    scheme_details_recs = scheme.objects.get(id=scheme_id)
    fre_list = []
    frequency_list =  scheme_details_recs.scheme_frequency_ids.all()
    for f_rec in frequency_list:
        f_id = f_rec.frequency_name.id
        fre_list.append(f_id)
    freq_obj = frequency_details.objects.all()
    for rec in freq_obj:
        ab = {'id': rec.id, 'freq_name': rec.frequency_name, 'fre_list': fre_list}
        freq_recs.append(ab)
    return JsonResponse(freq_recs, safe=False)

@user_login_required
def EditSchemeDefinition(request, scheme_id):

    option_count = itertools.count(1)
    iterator_color= make_incrementor(0)
    iterator_mark= make_incrementor(0)
    iterator_subtopic = make_incrementor(0)
    iterator_topic= make_incrementor(0)
    iterator_topic_add = make_incrementor(0)
    iterator_topic_rem = make_incrementor(0)
    iterator_substrand = make_incrementor(0)
    iterator_substrand_add = make_incrementor(0)
    iterator_substrand_rem = make_incrementor(0)
    color_count= make_incrementor(0)
    mark_count= make_incrementor(0)
    subtopic_count= make_incrementor(0)
    scheme_recs = scheme.objects.get(id=scheme_id)
    subject_check = scheme_recs.subject_scheme_name
    substrand_list = []
    frequecy_recs = scheme_recs.scheme_frequency_ids.all()
    name_edit_flag = False

    name_edit_flag = scheme_academic_frequency_mapping.objects.filter(scheme_mapping__scheme_name_id = scheme_id).exists()

    if not subject_check == None:
        subject_rec = subjects.objects.get(id=subject_check.id)
        subject_list = subject_rec.strands_ids.all()

        for obj in subject_rec.strands_ids.all():
            for sub_obj in obj.substrands_ids.all():
                substrand_list.append(sub_obj)

        view_val = {
            'name_edit_flag':name_edit_flag,
            'iterator_color': iterator_color,
            'iterator_mark': iterator_mark,
            'iterator_subtopic': iterator_subtopic,
            'iterator_topic': iterator_topic,
            'iterator_topic_add': iterator_topic_add,
            'iterator_topic_rem': iterator_topic_rem,
            'iterator_substrand_add': iterator_substrand_add,
            'iterator_substrand_rem': iterator_substrand_rem,
            'iterator_substrand': iterator_substrand,
            'color_count': color_count,
            'mark_count': mark_count,
            'subtopic_count': subtopic_count,
            "scheme_recs": scheme_recs,
            'submitted': scheme_recs.is_submitted,
            'option_count': option_count,
            'st_top_count': make_incrementor(0),
            'st_top_count1': make_incrementor(0),
            'st_top_count2': make_incrementor(0),
            'subject_list': subject_list,
            'subject_rec': subject_rec,
            'substrand_list': substrand_list,
            'frequecy_recs':frequecy_recs,
        }

        return render(request, "edit_scheme_with_subject.html", view_val)


    view_val = {
        'name_edit_flag': name_edit_flag,
        'iterator_color':iterator_color,
        'iterator_mark': iterator_mark,
        'iterator_subtopic': iterator_subtopic,

        'iterator_topic':iterator_topic,
        'iterator_topic_add':iterator_topic_add,
        'iterator_topic_rem':iterator_topic_rem,

        'iterator_substrand_add':iterator_substrand_add,
        'iterator_substrand_rem':iterator_substrand_rem,
        'iterator_substrand':iterator_substrand,

        'color_count':color_count,
        'mark_count':mark_count,
        'subtopic_count':subtopic_count,

        "scheme_recs": scheme_recs,
        'submitted' : scheme_recs.is_submitted,

        'option_count': option_count,
        'st_top_count': make_incrementor(0),
        'st_top_count1': make_incrementor(0),
        'st_top_count2': make_incrementor(0),
        'frequecy_recs':frequecy_recs,
    }
    return render(request, "edit_scheme_definition.html", view_val)

def SchemeDefinitionUpdate(request):
    scheme_id = request.POST.get('scheme_id')
    stand_id = request.POST.get('stand_id')
    submit = request.POST.get('submit')
    scheme_name = request.POST.get('scheme_name')
    val_dict = json.loads(request.POST['data'])
    frequency_data = json.loads(request.POST.get('frequency_data'))
    scheme_recs = scheme.objects.get(id=scheme_id)
    freq_ids=[]

    # for freq in frequency_data:
    #     freq_ids.append(frequency_details.objects.get(id=freq).id)
    freq_ids = frequency_details.objects.filter(id__in=frequency_data).values_list('id',flat=1)

    if scheme_name:
        scheme.objects.filter(id=scheme_id).update(scheme_name = scheme_name)

    old_freq = []
    scheme_recs = scheme.objects.get(id=scheme_id)
    frequecy_recs = scheme_recs.scheme_frequency_ids.all()

    # for freq in frequecy_recs:
    #     old_freq.append(freq.frequency_name.id)

    old_freq = scheme_recs.scheme_frequency_ids.all().values_list('id', flat=1)

    if scheme_academic_frequency_mapping.objects.filter(scheme_mapping__scheme_name_id=scheme_id,
                                                        frequency_id__in=old_freq).exists():
        if not set(old_freq).issubset(freq_ids):
            messages.warning(request, "You can't delete old frequencies as there relation exists..")
            return HttpResponse(json.dumps(scheme_recs.to_dict()), content_type='application/json')
    try:

            for rec in old_freq:
                scheme_recs.scheme_frequency_ids.filter(frequency_name_id=rec).delete()
                scheme_recs.scheme_frequency_ids.remove(rec)


            frequency_size = len(frequency_data)
            for count in range(frequency_size):
                if not scheme_recs.scheme_frequency_ids.filter(frequency_name_id=frequency_data[count]).exists():
                    scheme_frequency_obj = scheme_frequecy.objects.create(frequency_name_id=frequency_data[count])
                    scheme_recs.scheme_frequency_ids.add(scheme_frequency_obj.id)
                else:
                    if scheme_recs.scheme_frequency_ids.all().count() > 1:
                        scheme_recs.scheme_frequency_ids.filter(frequency_name_id=frequency_data[count]).delete()
                        scheme_recs.scheme_frequency_ids.remove(frequency_data[count])
                    else:
                        messages.warning(request, "A single frequency can't be deleted.")
                        pass


            #------Only Select code ---------------------
            # for count in range(frequency_size):
            #     if not scheme_recs.scheme_frequency_ids.filter(frequency_name_id=frequency_data[count]).exists():
            #         scheme_frequency_obj = scheme_frequecy.objects.create(frequency_name_id=frequency_data[count])
            #         scheme_recs.scheme_frequency_ids.add(scheme_frequency_obj.id)

            for rec in val_dict:
                for data in rec:
                    if not data['stand_id']:
                        strand_data = strand.objects.create(strand_name=data['strand'])
                        scheme_recs.stand_ids.add(strand_data.id)
                    else:
                        strand_data = strand.objects.get(id=data['stand_id'])
                        strand_data.strand_name = data['strand']
                        strand_data.save()

                    for substrand in data['substrand']:
                        sub_str_create = None
                        try:
                            sub_str = sub_strand.objects.get(id=substrand['substrand_id'])
                        except:
                            sub_str_create = sub_strand.objects.create(sub_strand_name=substrand['substrand_name'])

                        if not sub_str_create:
                            sub_str.sub_strand_name = substrand['substrand_name']
                            sub_str.save()
                        else:
                            strand_data.sub_strand_ids.add(sub_str_create.id)

                        for recs in substrand['topic']:
                            topic_name1 = recs['topic_name']
                            topic_table_create = None
                            try:
                                topic_table = topic.objects.get(id=recs['topic_id'])
                            except:
                                topic_table_create = topic.objects.create(topic_name=topic_name1)

                            if not topic_table_create:
                                topic_table.topic_name = topic_name1
                                topic_table.save()
                            else:
                                if not sub_str_create:
                                    sub_str.topic_ids.add(topic_table_create.id)
                                else:
                                    sub_str_create.topic_ids.add(topic_table_create.id)

                            for subtopic_rec in recs['subtopic']:
                                sub_topic_table_create = None
                                sub_topic_name = subtopic_rec['value']
                                mark_val = subtopic_rec['mark']
                                # color_val = subtopic_rec['color']
                                try:
                                    sub_topic_table = sub_topic.objects.get(id=subtopic_rec['subtopic_ids'])
                                except:
                                    sub_topic_table_create = sub_topic.objects.create(name=sub_topic_name,
                                                                                      mark=mark_val)
                                if not sub_topic_table_create:
                                    sub_topic_table.name = sub_topic_name
                                    sub_topic_table.mark = mark_val
                                    # sub_topic_table.color=color_val
                                    sub_topic_table.save()
                                else:
                                    if not topic_table_create:
                                        topic_table.sub_topic_ids.add(sub_topic_table_create.id)
                                    else:
                                        topic_table_create.sub_topic_ids.add(sub_topic_table_create.id)
            scheme.objects.filter(id=scheme_recs.id).update(is_submitted=True, updated_on=str(datetime.now()))
            messages.success(request, "Record Updated Successfully.")
    except:
            messages.warning(request, "Some Error Occurred... Please Try Again")

    return HttpResponse(json.dumps(scheme_recs.to_dict()), content_type='application/json')

# @user_login_required
# def DeleteStrand(request):
#     scheme_id = request.POST.get('scheme_id')
#     stand_id = request.POST.get('stand_id')
#     scheme_recs = scheme.objects.get(id=scheme_id)
#     scheme_recs.stand_ids.remove(stand_id)
#     return redirect('/school/edit_scheme_definition/'+scheme_id)


@user_login_required
def DeleteStrand(request):
    scheme_id = request.POST.get('scheme_id')
    stand_id = request.POST.get('stand_id')
    scheme_recs = scheme.objects.get(id=scheme_id)
    strand_obj = strand.objects.get(id=stand_id)

    scheme_recs.stand_ids.remove(strand_obj)

    for sub_strand_rec in strand_obj.sub_strand_ids.all():
        sub_strand_obj = sub_strand.objects.get(id = sub_strand_rec.id)

        for topic_rec in sub_strand_obj.topic_ids.all():
            topic_obj = topic.objects.get(id=topic_rec.id)
            sub_strand_obj.topic_ids.remove(topic_rec)

            for sub_topic_rec in topic_obj.sub_topic_ids.all():
                sub_top_id = sub_topic_rec.id
                sub_topic.objects.filter(id=sub_top_id).delete()
                topic_rec.sub_topic_ids.remove(sub_top_id)

            topic.objects.filter(id=topic_obj.id).delete()
            strand_obj.sub_strand_ids.remove(sub_strand_rec)
        sub_strand.objects.filter(id=sub_strand_rec.id).delete()
    strand.objects.filter(id=stand_id).delete()

    messages.success(request, "Strand Deleted Successfully.")
    scheme_recs = scheme.objects.get(id=scheme_id)
    return HttpResponse(json.dumps(scheme_recs.to_dict()), content_type='application/json')

    # return redirect('/school/edit_scheme_definition/' + scheme_id)

@user_login_required
def DeleteSkillScheme(request,scheme_id):
    try:
        if not scheme_mapping_details.objects.filter(scheme_name=scheme_id).exists():
            scheme_recs = scheme.objects.get(id=scheme_id)

            scheme_recs.scheme_frequency_ids.all().delete()

            for strand_rec in scheme_recs.stand_ids.all():
                for sub_strand_rec in strand_rec.sub_strand_ids.all():
                    for topic_rec in sub_strand_rec.topic_ids.all():
                        topic_rec.sub_topic_ids.all().delete()
                        topic_rec.delete()
                    sub_strand_rec.topic_ids.all()
                strand_rec.sub_strand_ids.all().delete()

            scheme_recs.stand_ids.all().delete()
            scheme.objects.filter(id=scheme_id).delete()
            messages.success(request, "Scheme deleted successfully.")
        else:
            messages.warning(request, "Can't delete this record. Scheme is mapped in skill mapping.")
    except:
        messages.warning(request, "Can't delete this record. Some error occurred.")

    return redirect('/school/scheme_definition_list/')


@user_login_required
def DeleteExamScheme(request,scheme_id):
    try:
        if not exam_scheme_exam_name_mapping.objects.filter(exam_scheme = scheme_id).exists():
            scheme_recs = exam_scheme.objects.get(id=scheme_id)

            for strand_rec in scheme_recs.stand_ids.all():
                for sub_strand_rec in strand_rec.sub_strand_ids.all():
                    for topic_rec in sub_strand_rec.topic_ids.all():
                        topic_rec.sub_topic_ids.all().delete()
                        topic_rec.delete()
                    sub_strand_rec.topic_ids.all()
                strand_rec.sub_strand_ids.all().delete()

            scheme_recs.stand_ids.all().delete()
            exam_scheme.objects.filter(id=scheme_id).delete()
            messages.success(request, "Scheme deleted successfully.")
        else:
            messages.warning(request, "Can't delete this record. Scheme is mapped in exam mapping.")
    except:
        messages.warning(request, "Can't delete this record. Some error occurred.")

    return redirect('/school/exam_definition_list/')

@user_login_required
def DeleteRubricScheme(request,scheme_id):
    try:
        if not rubric_mapping_details.objects.filter(rubric_name = scheme_id).exists():
            scheme_recs = rubric_scheme.objects.get(id=scheme_id)

            for strand_rec in scheme_recs.stand_ids.all():
                for sub_strand_rec in strand_rec.sub_strand_ids.all():
                    for topic_rec in sub_strand_rec.topic_ids.all():
                        topic_rec.sub_topic_ids.all().delete()
                        topic_rec.delete()
                    sub_strand_rec.topic_ids.all()
                strand_rec.sub_strand_ids.all().delete()

            scheme_recs.stand_ids.all().delete()
            rubric_scheme.objects.filter(id=scheme_id).delete()
            messages.success(request, "Scheme deleted successfully.")
        else:
            messages.warning(request, "Can't delete this record. Scheme is mapped in rubric mapping.")
    except:
        messages.warning(request, "Can't delete this record. Some error occurred.")

    return redirect('/school/rubric_definition_list/')


@user_login_required
def DeleteSubStrand(request):
    # print "In function  DeleteSubStrand->"
    scheme_id = request.POST.get('scheme_id')
    stand_id = request.POST.get('strand_id')
    substrand_id = request.POST.get('substrand_id')

    strand_rec = strand.objects.get(id = stand_id)
    strand_rec.sub_strand_ids.remove(substrand_id)

    substrand_recs = sub_strand.objects.get(id=substrand_id)

    for topic_recs in substrand_recs.topic_ids.all():
        topic_rec = topic.objects.get(id=topic_recs.id)

        # topic_rec.sub_topic_ids.remove(substrand_recs.id)

        for subtopic in topic_rec.sub_topic_ids.all():
            sub_top_id = subtopic.id
            sub_topic.objects.filter(id=sub_top_id).delete()
            topic_rec.sub_topic_ids.remove(sub_top_id)
        topic.objects.filter(id=topic_rec.id).delete()
    sub_strand.objects.filter(id=substrand_id).delete()

    messages.success(request, "Sub-Strand Deleted Successfully.")
    scheme_recs = scheme.objects.get(id=scheme_id)
    return HttpResponse(json.dumps(scheme_recs.to_dict()), content_type='application/json')


    # return redirect('/school/edit_scheme_definition/'+scheme_id)


@user_login_required
def DeleteTopic(request):
    # print "In function DeleteTopic------->"
    scheme_id = request.POST.get('scheme_id')
    substrand_id = request.POST.get('substrand_id')
    topic_id = request.POST.get('topic_id')

    substrand_rec = sub_strand.objects.get(id=substrand_id)
    substrand_rec.topic_ids.remove(topic_id)

    topic_recs = topic.objects.get(id=topic_id)

    for subtopic in topic_recs.sub_topic_ids.all():
        sub_top_id = subtopic.id
        sub_topic.objects.filter(id = sub_top_id).delete()
        topic_recs.sub_topic_ids.remove(sub_top_id)

    topic.objects.filter(id=topic_id).delete()

    messages.success(request, "Topic Deleted Successfully.")
    scheme_recs = scheme.objects.get(id=scheme_id)
    return HttpResponse(json.dumps(scheme_recs.to_dict()), content_type='application/json')



    # return redirect('/school/edit_scheme_definition/'+scheme_id)

@user_login_required
def DeleteSubTopic(request):
    # print "In function  DeleteSubTopic---------->"
    scheme_id = request.POST.get('scheme_id')
    topic_id = request.POST.get('topic_id')
    subtopic_id = request.POST.get('subtopic_id')
    topic_recs = topic.objects.get(id=topic_id)
    topic_recs.sub_topic_ids.remove(subtopic_id)
    sub_topic.objects.filter(id= subtopic_id).delete()
    messages.success(request, "Sub-Topic Deleted Successfully.")
    scheme_recs = scheme.objects.get(id=scheme_id)
    return HttpResponse(json.dumps(scheme_recs.to_dict()), content_type='application/json')



    # return redirect('/school/edit_scheme_definition/'+scheme_id)


@user_login_required
def ViewSchemeMapping(request):

    # scheme_recs = scheme.objects.filter(is_submitted = True)
    scheme_recs = scheme.objects.filter()
    all_academicyr_recs = academic_year.objects.all()
    grade_recs = grade_details.objects.filter(is_archive = False)
    year_class_section_object = academic_class_section_mapping.objects.all()
    scheme_mapping_recs = scheme_mapping_details.objects.all()
    # frequency_recs = frequency_details.objects.all()

    if year_class_section_object:
        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id
            return render(request, 'view_scheme_mapping.html',
                          { "year_list": all_academicyr_recs,
                           'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id, 'scheme_recs':scheme_recs, 'grade_recs':grade_recs, 'scheme_mapping_recs':scheme_mapping_recs})
        except academic_year.DoesNotExist:
            return render(request, 'view_scheme_mapping.html',
                          {"year_list": all_academicyr_recs,  'scheme_recs':scheme_recs, 'grade_recs':grade_recs, 'scheme_mapping_recs':scheme_mapping_recs})
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
    # print  "Hii MApping...."
    return render(request, 'skill_tracker_sub_tile.html')

@user_login_required
def ViewSubjectDetails(request):
    subjectDict = []
    subjectRecs=[]
    optionalSubject = []
    year_name = request.POST.get('academic_year', None)
    class_name = json.loads( request.POST.get('class_name'))
    ac_class_id = []

    ac_recs = academic_class_section_mapping.objects.filter(Q(year_name_id=year_name) & Q(class_name_id__in =class_name))
    for rec in ac_recs:
        ac_class_id.append(rec.id)

    subject_recs = mandatory_subject_mapping.objects.filter(academic_class_section_mapping_id__in = ac_class_id)

    ab = {}
    for subject in subject_recs:
        #ab={'id':subject.id, 'subject_name': subject.subject.subject_name}
        subjectDict.append(subject.subject_id)
    subjectDict = set(subjectDict)
    subject_data = subjects.objects.filter(id__in=subjectDict)
    for subject in subject_data:
        ab={'id':subject.id, 'subject_name': subject.subject_name}
        subjectRecs.append(ab)

    student_recs = student_details.objects.filter(academic_class_section_id__in  = ac_recs)
    for student_rec in student_recs:
        for subject in student_rec.subject_ids.all():
            optionalSubject.append(subject)

    optionalSubject = set(optionalSubject)

    for subject in optionalSubject:
        ab={'id':subject.id, 'subject_name': subject.subject_name}
        if not any(d['id'] ==subject.id for d in subjectRecs):
            subjectRecs.append(ab)

    #json.dumps(scheme_recs.to_dict())
    return JsonResponse(subjectRecs,safe=False)


def ViewFrequencyValidity(request):
    month_flag = False
    frequency_name = request.POST.get('frequency_name', None)
    freq_rec = frequency_details.objects.get(id=frequency_name)
    # month_flag = int(freq_rec.from_date) <= int(todays_date().month)
    selected_month_rec = current_acd_month_rec(int(freq_rec.from_date))
    date_formate = str(datetime.now().date())
    current_month_date = date_formate.split('-')[0] +'-'+date_formate.split('-')[1]+'-'+str('01')
    month_to_check= selected_month_rec.split('-')[0] +'-'+selected_month_rec.split('-')[1]+'-'+str('01')

    month_flag = datetime.strptime(month_to_check, '%Y-%m-%d') <= datetime.strptime(current_month_date, '%Y-%m-%d')

    return JsonResponse(month_flag, safe=False)


@user_login_required
def ViewSingleSubjectDetails(request):
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name',None)
    data = request.POST.get('data',None)
    if data=='rubric_subject':
        rubric_subject_list = []
        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=year_name,section_name=section_name,class_name=class_name)
        rubric_subject_recs = rubric_academic_frequency_mapping.objects.filter(academic_mapping_id=year_class_section_obj.id)
        for rec in rubric_subject_recs:
            rubric_subject_list.append(rec.scheme_mapping.subject)
        set_rubric_subject = set(rubric_subject_list)
        subject_rec_list = []
        for subject_rec in set_rubric_subject:
            subject_dict = {'id': subject_rec.id, 'subject_name': subject_rec.subject_name}
            subject_rec_list.append(subject_dict)
        return JsonResponse(subject_rec_list, safe=False)

    elif(data=='exam_subject'):
        exam_mapped_subject_list = []
        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=year_name,
                                                                            section_name=section_name,
                                                                            class_name=class_name)
        exam_mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(
            academic_mapping_id=year_class_section_obj.id)
        for exam_mark_rec in exam_mark_recs:
            exam_sub_mark_recs = exam_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()
            for exam_sub_mark_rec in exam_sub_mark_recs:
                exam_mapped_subject_list.append(exam_sub_mark_rec.subject)
        exam_mapped_subject_list = set(exam_mapped_subject_list)
        subject_rec_list = []
        for subject_rec in exam_mapped_subject_list:
            subject_dict = {'id': subject_rec.id, 'subject_name': subject_rec.subject_name}
            subject_rec_list.append(subject_dict)
        return JsonResponse(subject_rec_list, safe=False)


@user_login_required
def SaveSchemeMapping(request):
    try:
        if request.method == 'POST':
            year  = request.POST.get('year_name')
            class_name_ids = request.POST.getlist('class_name')
            scheme_name = request.POST.get('scheme')
            subject_name = request.POST.get('subject_name')
            grade_name = request.POST.get('grade_name')

            for class_rec in class_name_ids:
                rec_list = []
                exam_rec = scheme_mapping_details.objects.filter(year_id=year, class_obj_id=class_rec, subject_id=subject_name, scheme_name_id=scheme_name, grade_name_id=grade_name)
                if exam_rec:
                    messages.warning(request,"Record Not Saved. Mapping Of All This Records Is Already Found. ")
                    pass

                elif scheme_mapping_details.objects.filter(~Q(scheme_name_id=scheme_name), year_id=year, class_obj_id=class_rec, subject_id=subject_name, grade_name_id=grade_name).exists():
                    messages.warning(request,"Record Not Saved. Mapping Of This All Records Is Already Found  With Some Other Scheme. ")
                    pass

                elif scheme_mapping_details.objects.filter(~Q(scheme_name_id=scheme_name,grade_name_id=grade_name), year_id=year, class_obj_id=class_rec, subject_id=subject_name).exists():
                    messages.warning(request,"Record Not Saved. Mapping Of This All Records Is Already Found  With Some Other Scheme. ")
                    pass

                elif scheme_mapping_details.objects.filter(~Q(grade_name_id=grade_name), year_id=year, class_obj_id=class_rec, subject_id=subject_name,scheme_name_id=scheme_name).exists():
                    messages.warning(request, "Record Not Saved. Mapping Of This All Records Is Already Found  With Some Other Grade Scheme. ")
                    pass
                else:
                    messages.success(request,"Record Mapped Successfully.")
                    scheme_mapping_details.objects.create(year_id=year, class_obj_id=class_rec, subject_id=subject_name,
                                                          scheme_name_id=scheme_name, grade_name_id=grade_name)
        return redirect('/school/view_scheme_mapping/')
    except:
        messages.warning(request, "Some Error Occurred. Please Try again.")
        return redirect('/school/view_scheme_mapping/')

@user_login_required
def SaveSchemeCopyMapping(request):
    try:
        if request.method == 'POST':
            acd_year = request.POST.get('acd_year')
            copy_year = request.POST.get('copy_year')

            scheme_recs = scheme_mapping_details.objects.filter(year_id=acd_year,is_archived=False)


            for rec in scheme_recs:
                scheme_rec = scheme_mapping_details.objects.filter(year_id=copy_year, class_obj_id=rec.class_obj.id, subject_id=rec.subject.id,
                                                      scheme_name_id=rec.scheme_name.id, grade_name_id=rec.grade_name.id,is_archived=False)
                if scheme_rec:
                    pass
                else:
                    scheme_rec = scheme_mapping_details.objects.create(year_id=copy_year, class_obj_id=rec.class_obj.id,subject_id=rec.subject.id,scheme_name_id=rec.scheme_name.id,grade_name_id=rec.grade_name.id)

        messages.success(request,"Record Mapped Successfully.")
        return redirect('/school/view_skill_scheme_mapping/')
    except:
        messages.warning(request, "Some Error Occurred. Please Try again.")
        return redirect('/school/view_skill_scheme_mapping/')

@user_login_required
def ViewSchemeMappingFilter(request):
    scheme_rec = []
    # print "test"

    if request.POST:
        val_scheme_filter = request.POST
        request.session['val_scheme_filter'] = val_scheme_filter
        year_name = request.POST.get('year_name', None)
    else:
        year_name = request.POST.get('year_name', None)
        # year_name = request.session.get('val_scheme_filter').get('year_name')



    year_class_section_object = academic_class_section_mapping.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    grade_recs = grade_details.objects.filter(is_archive = False)

    if year_class_section_object:
        try:
            # year_name = request.POST.get('year_name', None)
            if not year_name:
                current_academic_year = academic_year.objects.get(current_academic_year=1)
                selected_year_id = current_academic_year.id
            else:
                current_academic_year = academic_year.objects.get(id=year_name)
                selected_year_id = year_name

            mapping_list = scheme_mapping_details.objects.filter(year=selected_year_id)

            return render(request, 'view_scheme_mapping_filter.html',
                          { "year_list": all_academicyr_recs,
                           'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                            'grade_recs':grade_recs,'mapping_list':mapping_list})

        except academic_year.DoesNotExist:
            return redirect('/school/view_skill_scheme_mapping/')
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'exam_sub_tile.html')

@user_login_required
def CopySchemeMappingFilter(request):
    scheme_rec = []
    year_class_section_object = academic_class_section_mapping.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    grade_recs = grade_details.objects.filter(is_archive = True)

    if year_class_section_object:
        try:
            year_name = request.POST.get('year_name', None)
            current_academic_year = academic_year.objects.get(id=year_name)
            selected_year_id = year_name

            mapping_list = scheme_mapping_details.objects.filter(year=selected_year_id,is_archived=False)

            return render(request, 'copy_scheme_mapping_filter.html',
                          { "year_list": all_academicyr_recs,
                           'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                            'grade_recs':grade_recs,'mapping_list':mapping_list})

        except academic_year.DoesNotExist:
            return redirect('/school/view_skill_scheme_mapping/')
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'exam_sub_tile.html')

@user_login_required
def CopySchemeMapping(request):
    scheme_rec = []
    year_class_section_object = academic_class_section_mapping.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    grade_recs = grade_details.objects.filter(is_archive = False)

    if year_class_section_object:
        try:
            year_name = request.POST.get('year_name', None)
            if not year_name:
                current_academic_year = academic_year.objects.get(current_academic_year=1)
                selected_year_id = current_academic_year.id
            else:
                current_academic_year = academic_year.objects.get(id=year_name)
                selected_year_id = year_name

            mapping_list = scheme_mapping_details.objects.filter(year=selected_year_id,is_archived=False)

            return render(request, 'copy_skill_mapping.html',
                          { "year_list": all_academicyr_recs,
                           'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                            'grade_recs':grade_recs,'mapping_list':mapping_list})

        except academic_year.DoesNotExist:
            return render(request, 'view_mapping.html',
                          { "year_list": all_academicyr_recs})
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'exam_sub_tile.html')

@user_login_required
def EditSchemeMapping(request):
    edit_scheme_mapping_data = request.POST
    if request.method == 'POST':
        request.session['edit_scheme_mapping_data'] = edit_scheme_mapping_data
        form_vals = load_edit_scheme_mapping_recs(edit_scheme_mapping_data, request)
    else:
        form_vals = load_edit_scheme_mapping_recs(request.session.get('edit_scheme_mapping_data'), request)
    return render(request, "view_edit_scheme_mapping.html", form_vals)

def load_edit_scheme_mapping_recs(edit_scheme_mapping_data, request):
    mapping_id = edit_scheme_mapping_data.get('mapping_id1')
    scheme_mapping_rec = scheme_mapping_details.objects.get(id=mapping_id)
    grading_recs = grade_details.objects.all()
    form_vals = {'grading_recs':grading_recs,'scheme_mapping_rec': scheme_mapping_rec}

    return form_vals

def SchemeMappingUpdate(request):
    year_name = request.POST.get('year_name')
    grade_name = request.POST.get('grade_name')
    class_name = request.POST.get('class_name')
    scheme_name = request.POST.get('scheme_name')
    subject_name = request.POST.get('subject_name')
    scheme_mapping_rec = request.POST.get('scheme_mapping_rec')

    try:
        if not scheme_academic_frequency_mapping.objects.filter(scheme_mapping_id = scheme_mapping_rec).exists():
            scheme_existing_rec = scheme_mapping_details.objects.filter(subject_id =subject_name , grade_name_id = grade_name,scheme_name_id= scheme_name, class_obj_id = class_name, year_id = year_name)

            if scheme_existing_rec:
                if scheme_existing_rec[0].id == int(scheme_mapping_rec):
                    scheme_mapping_details.objects.filter(id = scheme_mapping_rec).update(grade_name_id=grade_name)
                    messages.success(request,"Record Updated")
                else:
                    messages.warning(request, "Record Not Updated. Mapping Already Found.")
            else:
                scheme_mapping_details.objects.filter(id=scheme_mapping_rec).update(grade_name_id=grade_name)
                messages.success(request, "Record Updated")
        else:
            messages.warning(request, "Record Cannot be Updated. Marks Has Already Been Entered For This Mapping Record.")

        # if scheme_mapping_details.objects.filter(subject_id =subject_name , grade_name_id = grade_name,scheme_name_id= scheme_name, class_obj_id = class_name, year_id = year_name).count() <= 1:

    except:
        messages.warning(request, "Some Error Occurred. Please Try again.")
    return redirect('/school/edit_scheme_mapping/')


@user_login_required
def DeleteSchemeMapping(request):
    try:
        mapping_id=request.GET.get('mapping_id')
        mark_entry = scheme_academic_frequency_mapping.objects.filter(scheme_mapping_id = mapping_id)
        if not  mark_entry:
            scheme_mapping_details.objects.filter(id=mapping_id).delete()
        else:
            messages.warning(request,"Record Not Deleted.. This May Be Used In Add Score Tables, Delete Them First And Try Again.")
    except:
            messages.warning(request, "Some Error Occurred. ")
    return redirect('/school/view_scheme_mapping_filter/')

@user_login_required
def SkillMappingArchive(request,rec_id):
    try:
        mark_entry = scheme_academic_frequency_mapping.objects.filter(scheme_mapping_id = rec_id)
        scheme_mapping_rec = scheme_mapping_details.objects.get(id=rec_id)
        mapping_recs = scheme_mapping_details.objects.filter(year= scheme_mapping_rec.year, class_obj = scheme_mapping_rec.class_obj, scheme_name = scheme_mapping_rec.scheme_name, subject = scheme_mapping_rec.subject)

        if mapping_recs.count() == 1:
            if scheme_mapping_rec.is_archived == False:
                if not  mark_entry:
                    scheme_mapping_details.objects.filter(id=rec_id).update(is_archived=True)
                    messages.success(request, "Record is Archived.")
                else:
                    for rec in mark_entry:
                        scheme_academic_frequency_mapping.objects.filter(id=rec.id).update(is_archived=True)
                    scheme_mapping_details.objects.filter(id=rec_id).update(is_archived=True)
                    messages.success(request,"Record is Archived And Mark Entries For This Mapping Is Also Updated As Archived.")
            else:
                if not  mark_entry:
                    scheme_mapping_details.objects.filter(id=rec_id).update(is_archived=False)
                    messages.success(request, "Record is Un-Archived.")
                else:
                    for rec in mark_entry:
                        scheme_academic_frequency_mapping.objects.filter(id=rec.id).update(is_archived=False)
                    scheme_mapping_details.objects.filter(id=rec_id).update(is_archived=False)
                    messages.success(request,"Record is Un-Archived And Mark Entries For This Mapping Is Also Updated As Un-Archived.")
        else:
            if scheme_mapping_rec.is_archived == False:
                messages.warning(request, "Archive is not allowed for duplicate records.")
            else:
                messages.warning(request, "Un-Archive is not allowed for duplicate records.")
    except:
            messages.warning(request, "An Unexpected Error Occurred.")
    return redirect('/school/view_scheme_mapping_filter/')

@user_login_required
def ExportSchemeRecords(request):
    try:
        exam_mapping_rec = scheme_mapping_details.objects.all()
        column_names = ['Academic Year','Class','Subject','Scheme Name','Grading Scheme']
        rows = exam_mapping_rec.values_list('year__year_name','class_obj__class_name','subject__subject_name','scheme_name__scheme_name','grade_name__grade_name')
        return export_users_xls('SchemeMappingRecords', column_names, rows)
    except:
        messages.warning(request, "Some Error Occurred...")
    return redirect('/school/view_scheme_mapping/')

@user_login_required
@user_permission_required('assessment.can_view_view_skill_exam_mark', '/school/home/')
def EnteredSchemeMarkList(request):
    current_academic_year = academic_year.objects.get(current_academic_year=1)
    user = request.user
    academic_frequency_rec = []
    mapped_class = []
    mapped_section = []

    if request.user.is_system_admin() or request.user.is_officer():
        for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
            mapped_class.append(mapped_object.class_name)
            mapped_section.append(mapped_object.section_name)
        academic_frequency_rec = scheme_academic_frequency_mapping.objects.all()
    else:
        if request.user.is_supervisor():
            supervisor_list = []
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj in academic_supervisor_class_section_mapping.objects.filter(academic_class_section_mapping=mapped_object, supervisor_id=user.id):
                    if obj:
                        for obj in scheme_academic_frequency_mapping.objects.filter(academic_mapping=mapped_object):
                            if obj not in supervisor_list:
                                supervisor_list.append(obj)
                                academic_frequency_rec.append(obj)

        elif request.user.is_subject_teacher():
            subject_teacher_list = []
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj1 in teacher_subject_mapping.objects.filter(academic_class_section_mapping=mapped_object,staff_id=request.user.id):
                    if obj1:
                        for obj in scheme_academic_frequency_mapping.objects.filter(academic_mapping=mapped_object,scheme_mapping__subject_id=obj1.subject.id):
                            if obj not in subject_teacher_list:
                                subject_teacher_list.append(obj)
                                academic_frequency_rec.append(obj)
        else:
            academic_frequency_rec = scheme_academic_frequency_mapping.objects.filter(Q(academic_mapping__assistant_teacher=user.id) | Q(academic_mapping__staff_id=user.id))
    return render(request, "entered_scheme_mark_list.html", {'mapping_rec':academic_frequency_rec})

def EnterSchemeMark(request):
    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    scheme_recs = scheme.objects.all()
    frequency_recs = frequency_details.objects.all()

    current_academic_year = None
    selected_year_id = None

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        class_list = current_academic_year_mapped_classes(current_academic_year, request.user)

    except:
        messages.error(request, "Please add academic year first.")
        return render(request, "enter_scheme_marks.html", {'class_list': all_class_recs, 'section_list': all_section_recs, "year_list": all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})
    return render(request, "enter_scheme_marks.html", {'class_list': class_list, 'section_list': all_section_recs, "year_list": all_academicyr_recs,'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,  'scheme_recs': scheme_recs,'frequency_recs':frequency_recs})



def ImportSkillMarks(request):
    skill_excel = SkillExcelForm(request.POST, request.FILES)
    success_flag = False
    init_flag = False
    acd_scheme_id = request.POST.get('acd_scheme_id')
    sub_topic_list =[]
    all_rec = []
    try:
        if skill_excel.is_valid():
            student_code = ''
            total_mark = 0
            prev_rec_id = 0
            file_recs = request.FILES['excel'].get_array()

            for sub_topic in file_recs[2:]:
                # for rec in sub_topic[9:]:
                sub_topic_list.append(sub_topic)
                # break

            for file_rec in file_recs[3:]:
                # mark_dict = {}
                # mark_dict['student_code'] = file_rec[4]
                # for i in file_rec [9:]:
                #     mark_dict['mark']=i
                all_rec.append(file_rec)


            for rec in all_rec:
                student_rec = scheme_student_mark_mapping.objects.filter(student__student_code=rec[4])
                for obj in student_rec:
                    count = 9
                    for student_mark in obj.subtopic_ids.all():
                        if str(student_mark.subtopic.sub_topic.name) == str(sub_topic_list[0][count]):
                            student_mark.mark = rec[count]
                            student_mark.save()
                            count = count + 1
                        else:
                            print 'else'
                            continue


        else:
            messages.warning(request,'Invaid file or file format')
    except Exception as e:
        messages.warning(request, 'Some error occurred.'+str(e))

    if success_flag:
        messages.success(request, 'Record updated successfully.')
    return redirect('/school/update_entered_scheme_mark/' + str(acd_scheme_id))

def ImportExamMarks(request):
    skill_excel = SkillExcelForm(request.POST, request.FILES)
    success_flag = False
    init_flag = False
    try:
        if skill_excel.is_valid():
            student_code = ''
            total_mark = 0
            prev_rec_id = 0
            file_recs = request.FILES['excel'].get_records()
            for file_rec in file_recs:
                exam_scheme_subtopic_mark.objects.filter(id=file_rec['Rec_ID']).update(mark = file_rec['Total Marks'])

                if init_flag:
                    if student_code == file_rec['Student Code']:
                        student_code = file_rec['Student Code']

                        try:
                            float(file_rec['Total Marks'])
                            total_mark += float(file_rec['Total Marks'])
                        except ValueError:
                            total_mark += 0

                        prev_rec_id = file_rec['Rec_ID']
                    else:
                        scheme_rec_id = exam_scheme_subtopic_mark.objects.get(id=prev_rec_id).exam_scheme_subtopic_rel.get().id
                        exam_scheme_student_mark_mapping.objects.filter(id = scheme_rec_id).update(total_mark = total_mark)

                        try:
                            float(file_rec['Total Marks'])
                            total_mark = float(file_rec['Total Marks'])
                        except ValueError:
                            total_mark = 0

                        prev_rec_id = file_rec['Rec_ID']
                        student_code = file_rec['Student Code']
                else:
                    student_code = file_rec['Student Code']
                    prev_rec_id = file_rec['Rec_ID']

                    try:
                        float(file_rec['Total Marks'])
                        total_mark = float(file_rec['Total Marks'])
                    except ValueError:
                        total_mark = 0

                    init_flag = True
                success_flag = True
            if file_recs[len(file_recs) - 1]['Rec_ID'] == file_rec['Rec_ID']:
                scheme_rec_id = exam_scheme_subtopic_mark.objects.get(id=prev_rec_id).exam_scheme_subtopic_rel.get().id
                exam_scheme_student_mark_mapping.objects.filter(id=scheme_rec_id).update(total_mark=total_mark)
        else:
            messages.warning(request,'Invaid file or file format')
    except:
        messages.warning(request, 'Some error occurred.')

    if success_flag:
        messages.success(request, 'Record updated successfully.')
    return redirect('/school/entered_exam_scheme_mark_list/')

@user_login_required
def EnterMarkSchemeStudentList(request):

    all_academicyr_recs = academic_year.objects.all()
    if request.POST:
        request.session['data'] = request.POST
        selected_year_id = request.POST.get('year_name')  # It will be used in HTML page as a value of selected Class
        selected_class_id = request.POST.get('class_name')  # It will be used in HTML page as a value of selected Class
        selected_section_id = request.POST.get('section_name')  # It will be used in HTML page as a value of selected Section
        frequency_name = request.POST.get('frequency_name')
        selected_subject_id = request.POST.get('subject_name')  # It will be used in HTML page as a value of selected Section
        selected_copy_from_id = request.POST.get('copy_from')
    else:
        selected_year_id = request.session['data'].get('year_name')  # It will be used in HTML page as a value of selected Class
        selected_class_id = request.session['data'].get('class_name')  # It will be used in HTML page as a value of selected Class
        selected_section_id = request.session['data'].get(
            'section_name')  # It will be used in HTML page as a value of selected Section
        frequency_name = request.session['data'].get('frequency_name')
        selected_subject_id = request.session['data'].get(
            'subject_name')  # It will be used in HTML page as a value of selected Section
        selected_copy_from_id = request.session['data'].get('copy_from')

    round_rec = assessment_round_master.objects.all()[0]
    round_select = round_rec.select
    round_decimal_place = round_rec.decimal_place

    selected_year_rec = academic_year.objects.get(id=selected_year_id)
    selected_year_name = selected_year_rec.year_name
    all_class_recs = mapped_user_classes(selected_year_rec,request)

    selected_class_rec = class_details.objects.get(id=selected_class_id)
    selected_class_name = selected_class_rec.class_name

    all_section_recs = current_class_mapped_sections(selected_year_rec,selected_class_rec,request.user)
    na_flag = assessment_configration_master.objects.filter(short_name='NA').exists()

    selected_section_rec = sections.objects.get(id=selected_section_id)
    selected_section_name = selected_section_rec.section_name

    frequency_data = frequency_details.objects.get(id = frequency_name)

    selected_subject_rec = subjects.objects.get(id=selected_subject_id)
    selected_subject_name = selected_subject_rec.subject_name


    year_class_section_obj1 = academic_class_section_mapping.objects.get(year_name=selected_year_id, section_name=selected_section_id,class_name=selected_class_id)


    assessment_conf_names = assessment_configration_master.objects.all()


    # conf_list = ['Ab', 'NA']
    conf_list = []
    conf_dict_list = []
    for conf in assessment_conf_names:
        # conf_dict = {}
        conf_list.append(str(conf.short_name))

        # conf_dict[conf.short_name] = conf.select
        # conf_dict_list.append(conf_dict)

    entered_value = '0-9.'
    for c in conf_list: entered_value = str(c) + str(entered_value)


    try:
        archive_record_count = scheme_mapping_details.objects.filter(year_id=selected_year_id, class_obj_id=selected_class_id,
                                                                     subject_id=selected_subject_id)
        scheme_rec1 = scheme_mapping_details.objects.get(year_id=selected_year_id, class_obj_id = selected_class_id,subject_id =  selected_subject_id, is_archived = False)
        scheme_id = scheme_rec1.scheme_name.id
    except:
        if archive_record_count.count() > 0:
            messages.warning(request, "New Mapping Not Found for The Selected Class And Section")
            return redirect('/school/enter_scheme_mark/')
        messages.warning(request, "Mapping Not Found for Selected Class And Section")
        return redirect('/school/enter_scheme_mark/')

    scheme_mapped_flag = False
    selected_scheme = scheme_rec1.scheme_name

    if selected_scheme.subject_scheme_name:
        scheme_mapped_flag = True

    subtopics_recs = []
    subtopic_recs = []
    list = []
    sub_strand_list = []
    filter_flag = True

    sub_strand_dict_list = []
    sub_strand_dict_id = []
    topic_dict_id = []
    topic_dict_list = []

    for strand in selected_scheme.stand_ids.all():
        for sub_strand in strand.sub_strand_ids.all():
            sub_strand_dict = {}
            sub_strand_temp_dict = {}
            if scheme_mapped_flag:
                sub_strand_temp_dict['sub_strand_name'] = subject_substrands.objects.get(
                    id=sub_strand.sub_strand_name).substrands_name
            else:
                sub_strand_temp_dict['sub_strand_name'] = sub_strand.sub_strand_name

            sub_strand_temp_dict['id'] = sub_strand.id
            sub_strand_list.append(sub_strand_temp_dict)
            if sub_strand.id not in sub_strand_dict_id:
                sub_strand_dict_id.append(sub_strand.id)
                if scheme_mapped_flag:
                    sub_strand_dict['sub_strand_name'] = subject_substrands.objects.get(
                        id=sub_strand.sub_strand_name).substrands_name
                    sub_strand_dict['topic_count'] = 0
                else:
                    sub_strand_dict['sub_strand_name'] = sub_strand.sub_strand_name
                    sub_strand_dict['topic_count'] = 0

            for topic in sub_strand.topic_ids.all():
                topic_dict = {}
                if topic.id not in topic_dict_id:
                    topic_dict['topic_name'] = topic.topic_name
                    topic_dict['subtopic_count'] = topic.sub_topic_ids.all().count()
                    topic_dict_id.append(topic.id)
                    topic_dict_list.append(topic_dict)
                    sub_strand_dict['topic_count'] += topic.sub_topic_ids.all().count()
            sub_strand_dict_list.append(sub_strand_dict)


    selected_scheme = scheme.objects.get(id =scheme_id)
    subtopics_recs = []

    for starand in selected_scheme.stand_ids.all():
        for sub_strand_recs in starand.sub_strand_ids.all():
            if sub_strand_recs:
                for topic_recs in sub_strand_recs.topic_ids.all():
                    if topic_recs:
                        for subtopics in topic_recs.sub_topic_ids.all():
                            subtopics_recs.append(subtopics)

    total_mark = 0
    for subtopic_rec in subtopics_recs:
        total_mark = int(subtopic_rec.mark) + total_mark

    year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id, section_name=selected_section_id, class_name=selected_class_id)

    scheme_rec = scheme_mapping_details.objects.get(year_id=selected_year_id, class_obj_id = selected_class_id,subject_id = selected_subject_id, is_archived = False)

    mark_recs = scheme_academic_frequency_mapping.objects.filter(academic_mapping=year_class_section_obj,scheme_mapping_id=scheme_rec.id, frequency_id = frequency_name)
    grade_colors = grade_sub_details.objects.filter(grade=scheme_rec.grade_name)

    if mark_recs:
        messages.warning(request,"Mark already entered, Update the entered marks.")
        # return redirect('/school/entered_scheme_mark_list/')
        return redirect('/school/update_entered_scheme_mark/' + str(mark_recs[0].id))

    if selected_copy_from_id:
        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,section_name=selected_section_id, class_name=selected_class_id)
        scheme_mapping_rec = scheme_mapping_details.objects.get(year=selected_year_id,class_obj=selected_class_id,subject=selected_subject_id, is_archived = False)
        scheme_acd_frequency = scheme_academic_frequency_mapping.objects.get(academic_mapping_id = year_class_section_obj.id,scheme_mapping_id=scheme_mapping_rec.id,frequency_id=selected_copy_from_id)
        copy_from_data = frequency_details.objects.get(id=selected_copy_from_id)
        academic_frequency_rec = scheme_academic_frequency_mapping.objects.get(id=scheme_acd_frequency.id)
        selected_scheme = scheme.objects.get(id=academic_frequency_rec.scheme_mapping.scheme_name_id)
        subtopics_recs = []
        subtopic_recs = []
        list = []
        grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)
        for data in academic_frequency_rec.student_mark_ids.all():
            for rec in data.subtopic_ids.all():
                if rec.subtopic.id not in list:
                    list.append(rec.subtopic.id)
                    subtopic_recs.append(rec.subtopic)
        form_vals = {
            'entered_value': entered_value,
            'conf_list': conf_list,
            'total_mark':total_mark,
            'Selected_Section_Id': selected_section_id,
            'selected_section_name': selected_section_name,
            'selected_section': selected_section_rec,
            'section_list': all_section_recs,
            'Selected_Class_Id': selected_class_id,
            'selected_class_name': selected_class_name,
            'selected_class': selected_class_rec,
            'class_list': all_class_recs,
            'Selected_Year_Id': selected_year_id,
            'selected_year_name': selected_year_name,
            'selected_year': selected_year_rec,
            'year_list': all_academicyr_recs,
            'frequency_data': frequency_data,
            'subtopics_recs': subtopics_recs,
            'selected_scheme': selected_scheme,
            'grade_colors': grade_colors,
            'selected_subject_id': selected_subject_id,
            'selected_subject_name': selected_subject_rec,
            'academic_frequency_rec': academic_frequency_rec,
            'subtopic_data':subtopic_recs,
            'copy_from_data':copy_from_data,

            'sub_strand_list': sub_strand_list,
            'sub_strand_dict_list': sub_strand_dict_list,
            'topic_dict_list': topic_dict_list,
            'filter_flag': True,
            'conf_dict_list':assessment_conf_names,
            'subtopic_recs_len': len(subtopics_recs),
            'na_flag': na_flag,
            'round_decimal_place':round_decimal_place,
            'round_select':round_select
        }
        return render(request, "enter_scheme_mark_new.html", form_vals)

    filtered_students =[]
    if scheme_rec.subject.type == 'Optional':
        optional_subject_students = scheme_rec.subject.optional_subject_relation.all()
        for students in optional_subject_students:
            student_rec = student_details.objects.filter(id=students.id, is_active=True, academic_class_section_id = year_class_section_obj.id)
            if student_rec:
                filtered_students.append(student_rec[0])
    else:
        filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True).order_by('first_name')

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    selected_year_id = current_academic_year.id
    scheme_recs = scheme.objects.all()
    frequency_recs = frequency_details.objects.all()

    #     Passing values to the HTML page through form_vals
    form_vals = {
        'entered_value': entered_value,
        'conf_list': conf_list,
        'total_mark': total_mark,
        'student_list': filtered_students,
        'Selected_Section_Id': selected_section_id,
        'selected_section_name': selected_section_name,
        'selected_section': selected_section_rec,
        'section_list': all_section_recs,
        'Selected_Class_Id': selected_class_id,
        'selected_class_name': selected_class_name,
        'selected_class': selected_class_rec,
        'class_list': all_class_recs,
        'Selected_Year_Id': selected_year_id,
        'selected_year_name': selected_year_name,
        'selected_year': selected_year_rec,
        'year_list': all_academicyr_recs,
        'frequency_recs': frequency_recs,
        'frequency_data': frequency_data,
        'scheme_recs': scheme_recs,
        'subtopics_recs': subtopics_recs,
        'selected_scheme': selected_scheme,
        'grade_colors': grade_colors,
        'selected_subject_id': selected_subject_id,
        'selected_subject_name': selected_subject_rec,

        'sub_strand_list': sub_strand_list,
        'sub_strand_dict_list': sub_strand_dict_list,
        'topic_dict_list': topic_dict_list,
        'filter_flag' : True,
        'conf_dict_list':assessment_conf_names,
        'subtopic_recs_len': len(subtopics_recs),
        'na_flag':na_flag,
        'round_decimal_place': round_decimal_place,
        'round_select': round_select,
    }
    return render(request, "enter_scheme_marks.html",form_vals)


@user_login_required
def SkillFilterAddStudentList(request):
    try:
        all_academicyr_recs = academic_year.objects.all()
        if request.POST:
            request.session['skill_filter_data'] = request.POST
            selected_year_id = request.POST.get('acd_year')
            selected_class_id = request.POST.get('class')
            selected_section_id = request.POST.get('section')
            frequency_name = request.POST.get('frequency')
            selected_subject_id = request.POST.get('sub')
            # selected_copy_from_id = request.POST.get('copy_from')
            topic_form = request.POST.get('topic_form')
            sub_strand_filter = request.POST.get('sub_strand_filter')
        else:
            selected_year_id = request.session['skill_filter_data'].get(
                'acd_year')  # It will be used in HTML page as a value of selected Class
            selected_class_id = request.session['skill_filter_data'].get(
                'class')  # It will be used in HTML page as a value of selected Class
            selected_section_id = request.session['skill_filter_data'].get(
                'section')  # It will be used in HTML page as a value of selected Section
            frequency_name = request.session['skill_filter_data'].get('frequency')
            selected_subject_id = request.session['skill_filter_data'].get(
                'sub')  # It will be used in HTML page as a value of selected Section
            # selected_copy_from_id = request.session['data'].get('copy_from')
            topic_form = request.session['skill_filter_data'].get('topic_form')
            sub_strand_filter = request.session['skill_filter_data'].get('sub_strand_filter')


        if topic_form == '' and sub_strand_filter == '':
            return redirect('/school/enter_mark_scheme_student_list/')

        selected_year_rec = academic_year.objects.get(id=selected_year_id)
        selected_year_name = selected_year_rec.year_name
        all_class_recs = mapped_user_classes(selected_year_rec, request)

        selected_class_rec = class_details.objects.get(id=selected_class_id)
        selected_class_name = selected_class_rec.class_name

        all_section_recs = current_class_mapped_sections(selected_year_rec, selected_class_rec, request.user)

        selected_section_rec = sections.objects.get(id=selected_section_id)
        selected_section_name = selected_section_rec.section_name

        frequency_data = frequency_details.objects.get(id=frequency_name)

        selected_subject_rec = subjects.objects.get(id=selected_subject_id)
        selected_subject_name = selected_subject_rec.subject_name

        year_class_section_obj1 = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                             section_name=selected_section_id,
                                                                             class_name=selected_class_id)

        assessment_conf_names = assessment_configration_master.objects.all()
        conf_list = ['Ab', 'NA']
        for conf in assessment_conf_names:
            conf_list.append(str(conf.short_name))

        entered_value = '0-9.'
        for c in conf_list: entered_value = str(c) + str(entered_value)

        try:
            archive_record_count = scheme_mapping_details.objects.filter(year_id=selected_year_id,
                                                                         class_obj_id=selected_class_id,
                                                                         subject_id=selected_subject_id)
            scheme_rec1 = scheme_mapping_details.objects.get(year_id=selected_year_id, class_obj_id=selected_class_id,
                                                             subject_id=selected_subject_id, is_archived=False)
            scheme_id = scheme_rec1.scheme_name.id
        except:
            if archive_record_count.count() > 0:
                messages.warning(request, "New Mapping Not Found for The Selected Class And Section")
                return redirect('/school/enter_scheme_mark/')
            messages.warning(request, "Mapping Not Found for Selected Class And Section")
            return redirect('/school/enter_scheme_mark/')

        scheme_mapped_flag = False
        selected_scheme = scheme_rec1.scheme_name

        if selected_scheme.subject_scheme_name:
            scheme_mapped_flag = True

        subtopics_recs = []
        subtopic_recs = []
        list = []
        sub_strand_list = []
        filter_flag = True

        sub_strand_dict_list = []
        sub_strand_dict_id = []
        topic_dict_id = []
        topic_dict_list = []


        sub_strand_data = sub_strand.objects.get(id=sub_strand_filter)

        topic_form_data = topic.objects.get(id=topic_form)

        sub_strand_dict = {}
        topic_dict = {}

        if scheme_mapped_flag:
            sub_strand_dict['sub_strand_name'] = subject_substrands.objects.get(
                id=sub_strand_data.sub_strand_name).substrands_name
        else:
            sub_strand_dict['sub_strand_name'] = sub_strand_data.sub_strand_name
        sub_strand_dict['id'] = sub_strand_data.id

        sub_strand_dict[
            'topic_count'] = topic_form_data.sub_topic_ids.all().count()  # used for row and colspan in table head of html
        sub_strand_dict_list.append(sub_strand_dict)

        topic_dict['topic_name'] = topic_form_data.topic_name
        topic_dict[
            'subtopic_count'] = topic_form_data.sub_topic_ids.all().count()  # used for row and colspan in table head of html
        topic_dict_list.append(topic_dict)

        # scheme_id = seheme_mapping_rec.scheme_name.id
        subtopics_recs = []
        subtopics_list = []
        subtopics_ids = []
        subtopic_recs = []
        list = []
        sub_strand_list = []
        topic_list = []

        for strand_obj in selected_scheme.stand_ids.all():
            for sub_strand_obj in strand_obj.sub_strand_ids.all():
                sub_strand_temp_dict = {}
                if scheme_mapped_flag:
                    sub_strand_temp_dict['sub_strand_name'] = subject_substrands.objects.get(
                        id=sub_strand_obj.sub_strand_name).substrands_name
                else:
                    sub_strand_temp_dict['sub_strand_name'] = sub_strand_obj.sub_strand_name

                sub_strand_temp_dict['id'] = sub_strand_obj.id
                sub_strand_list.append(sub_strand_temp_dict)

        for topic_rec_obj in sub_strand_data.topic_ids.all():
            topic_list.append(topic_rec_obj)

        topic_recs = topic.objects.get(id=topic_form)

        for subtopics in topic_recs.sub_topic_ids.all():
            subtopics_list.append(subtopics.id)

        selected_scheme = scheme.objects.get(id=scheme_id)
        subtopics_recs = []

        for starand in selected_scheme.stand_ids.all():
            for sub_strand_recs in starand.sub_strand_ids.all():
                if sub_strand_recs:
                    for topic_recs in sub_strand_recs.topic_ids.all():
                        if topic_recs:
                            for subtopics in topic_recs.sub_topic_ids.all():
                                subtopics_recs.append(subtopics)

        total_mark = 0
        for subtopic_rec in subtopics_recs:
            total_mark = int(subtopic_rec.mark) + total_mark

        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                            section_name=selected_section_id,
                                                                            class_name=selected_class_id)

        scheme_rec = scheme_mapping_details.objects.get(year_id=selected_year_id, class_obj_id=selected_class_id,
                                                        subject_id=selected_subject_id, is_archived=False)

        mark_recs = scheme_academic_frequency_mapping.objects.filter(academic_mapping=year_class_section_obj,
                                                                     scheme_mapping_id=scheme_rec.id,
                                                                     frequency_id=frequency_name)
        grade_colors = grade_sub_details.objects.filter(grade=scheme_rec.grade_name)

        if mark_recs:
            messages.warning(request, "Mark already entered, Update the entered marks.")
            # return redirect('/school/entered_scheme_mark_list/')
            return redirect('/school/update_entered_scheme_mark/' + str(mark_recs[0].id))

        # if selected_copy_from_id:
        #     year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,section_name=selected_section_id, class_name=selected_class_id)
        #     scheme_mapping_rec = scheme_mapping_details.objects.get(year=selected_year_id,class_obj=selected_class_id,subject=selected_subject_id, is_archived = False)
        #     scheme_acd_frequency = scheme_academic_frequency_mapping.objects.get(academic_mapping_id = year_class_section_obj.id,scheme_mapping_id=scheme_mapping_rec.id,frequency_id=selected_copy_from_id)
        #     copy_from_data = frequency_details.objects.get(id=selected_copy_from_id)
        #     academic_frequency_rec = scheme_academic_frequency_mapping.objects.get(id=scheme_acd_frequency.id)
        #     selected_scheme = scheme.objects.get(id=academic_frequency_rec.scheme_mapping.scheme_name_id)
        #     subtopics_recs = []
        #     subtopic_recs = []
        #     list = []
        #     grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)
        #     for data in academic_frequency_rec.student_mark_ids.all():
        #         for rec in data.subtopic_ids.all():
        #             if rec.subtopic.id not in list:
        #                 list.append(rec.subtopic.id)
        #                 subtopic_recs.append(rec.subtopic)
        #     form_vals = {
        #     'entered_value': entered_value,
        #     'conf_list': conf_list,
        #     'total_mark':total_mark,
        #     'Selected_Section_Id': selected_section_id,
        #     'selected_section_name': selected_section_name,
        #     'selected_section': selected_section_rec,
        #     'section_list': all_section_recs,
        #
        #     'Selected_Class_Id': selected_class_id,
        #     'selected_class_name': selected_class_name,
        #     'selected_class': selected_class_rec,
        #     'class_list': all_class_recs,
        #     'Selected_Year_Id': selected_year_id,
        #     'selected_year_name': selected_year_name,
        #     'selected_year': selected_year_rec,
        #     'year_list': all_academicyr_recs,
        #     'frequency_data': frequency_data,
        #     'subtopics_recs': subtopics_recs,
        #     'selected_scheme': selected_scheme,
        #
        #     'grade_colors': grade_colors,
        #     'selected_subject_id': selected_subject_id,
        #     'selected_subject_name': selected_subject_rec,
        #     'academic_frequency_rec': academic_frequency_rec,
        #     'subtopic_data':subtopic_recs,
        #     'copy_from_data':copy_from_data,
        #
        #     'subtopics_list': subtopics_list,
        #     'sub_strand_list': sub_strand_list,
        #     'sub_strand_dict_list': sub_strand_dict_list,
        #     'topic_dict_list': topic_dict_list,
        #     'filter_flag': False,
        #       'topic_list': topic_list,

        #     }
        #     return render(request, "enter_scheme_mark_new.html", form_vals)

        filtered_students = []
        if scheme_rec.subject.type == 'Optional':
            optional_subject_students = scheme_rec.subject.optional_subject_relation.all()
            for students in optional_subject_students:
                student_rec = student_details.objects.filter(id=students.id, is_active=True,
                                                             academic_class_section_id=year_class_section_obj.id)
                if student_rec:
                    filtered_students.append(student_rec[0])
        else:
            filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj,
                                                               is_active=True).order_by('first_name')

        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        scheme_recs = scheme.objects.all()
        frequency_recs = frequency_details.objects.all()

        #     Passing values to the HTML page through form_vals
        form_vals = {
            'entered_value': entered_value,
            'conf_list': conf_list,
            'total_mark': total_mark,
            'student_list': filtered_students,

            'Selected_Section_Id': selected_section_id,
            'selected_section_name': selected_section_name,
            'selected_section': selected_section_rec,
            'section_list': all_section_recs,

            'Selected_Class_Id': selected_class_id,
            'selected_class_name': selected_class_name,
            'selected_class': selected_class_rec,
            'class_list': all_class_recs,

            'Selected_Year_Id': selected_year_id,
            'selected_year_name': selected_year_name,
            'selected_year': selected_year_rec,
            'year_list': all_academicyr_recs,

            'frequency_recs': frequency_recs,
            'frequency_data': frequency_data,
            'scheme_recs': scheme_recs,
            'subtopics_recs': subtopics_recs,
            'selected_scheme': selected_scheme,
            'grade_colors': grade_colors,
            'selected_subject_id': selected_subject_id,
            'selected_subject_name': selected_subject_rec,

            'sub_strand_list': sub_strand_list,
            'sub_strand_dict_list': sub_strand_dict_list,
            'topic_dict_list': topic_dict_list,
            'filter_flag': False,
            'subtopics_list': subtopics_list,
            'topic_list': topic_list,
            'topic_rec': topic_form_data,
            'sub_strand_rec': sub_strand_dict,
            'conf_dict_list': assessment_conf_names,
            'subtopic_recs_len': len(subtopics_recs)
        }
        return render(request, "enter_scheme_marks.html", form_vals)
    except:
        messages.warning(request, 'Invalid Filter Selected.')
    return redirect('/school/entered_scheme_mark_list/')


@user_login_required
def FilterSchemeStudentList(request):
    if request.POST:
        request.session['val_data'] = request.POST
        rec_id = request.POST.get('academic_frequency_rec')
        topic_form = request.POST.get('topic_form')
        sub_strand_filter = request.POST.get('sub_strand_filter')
        form_vals = {}
    else:
        rec_id = request.session['val_data'].get('academic_frequency_rec')
        topic_form = request.session['val_data'].get('topic_form')
        sub_strand_filter = request.session['val_data'].get('sub_strand_filter')
        form_vals = {}

    if topic_form != '' and sub_strand_filter == '':
        messages.warning(request, 'Invalid Filter Selected.')
        return redirect('/school/update_entered_scheme_mark/' + str(rec_id))

    if topic_form == '' and sub_strand_filter != '':
        messages.warning(request, 'Invalid Filter Selected.')
        return redirect('/school/update_entered_scheme_mark/' + str(rec_id))
        # return redirect('/school/filter_scheme_student_list/')

    try:

        sub_strand_dict_list = []
        sub_strand_dict_id = []
        topic_dict_id = []
        topic_dict_list = []
        scheme_mapped_flag = False

        academic_frequency_rec = scheme_academic_frequency_mapping.objects.get(id=rec_id)
        skill_excel_form = SkillExcelForm()
        selected_scheme = scheme.objects.get(id=academic_frequency_rec.scheme_mapping.scheme_name.id)

        if selected_scheme.subject_scheme_name:
            scheme_mapped_flag = True

        sub_strand_data = sub_strand.objects.get(id=sub_strand_filter)

        topic_form_data = topic.objects.get(id=topic_form)

        sub_strand_dict = {}
        topic_dict = {}

        if scheme_mapped_flag:
            sub_strand_dict['sub_strand_name'] = subject_substrands.objects.get(
                id=sub_strand_data.sub_strand_name).substrands_name
        else:
            sub_strand_dict['sub_strand_name'] = sub_strand_data.sub_strand_name
        sub_strand_dict['id'] = sub_strand_data.id

        sub_strand_dict['topic_count'] = topic_form_data.sub_topic_ids.all().count()   # used for row and colspan in table head of html
        sub_strand_dict_list.append(sub_strand_dict)

        topic_dict['topic_name'] = topic_form_data.topic_name
        topic_dict['subtopic_count'] = topic_form_data.sub_topic_ids.all().count()    # used for row and colspan in table head of html
        topic_dict_list.append(topic_dict)

        # scheme_id = seheme_mapping_rec.scheme_name.id
        subtopics_recs = []
        subtopics_list = []
        subtopics_ids = []
        subtopic_recs = []
        list = []
        sub_strand_list = []
        topic_list = []
        filter_flag = '3'

        for strand in selected_scheme.stand_ids.all():
            for sub_strand_obj in strand.sub_strand_ids.all():
                sub_strand_temp_dict = {}
                if scheme_mapped_flag:
                    sub_strand_temp_dict['sub_strand_name'] = subject_substrands.objects.get(
                        id=sub_strand_obj.sub_strand_name).substrands_name
                else:
                    sub_strand_temp_dict['sub_strand_name'] = sub_strand_obj.sub_strand_name

                sub_strand_temp_dict['id'] = sub_strand_obj.id
                sub_strand_list.append(sub_strand_temp_dict)

        for topic_rec_obj in sub_strand_data.topic_ids.all():
            topic_list.append(topic_rec_obj)

        grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)

        topic_recs = topic.objects.get(id=topic_form)

        for subtopics in topic_recs.sub_topic_ids.all():
            subtopics_list.append(subtopics.id)

        # academic_frequency_rec = academic_frequency_rec.student_mark_ids.filter(subtopic_ids__subtopic__sub_topic__in=subtopic_recs)

        for data in academic_frequency_rec.student_mark_ids.all():
            for rec in data.subtopic_ids.all():
                if rec.subtopic.id not in list:
                    if rec.subtopic.sub_topic_id in subtopics_list:
                        list.append(rec.subtopic.id)
                        subtopic_recs.append(rec.subtopic)

        total_mark = 0
        for subtopic_rec in subtopic_recs:
            total_mark = int(subtopic_rec.subtopic_mark) + total_mark

        if academic_frequency_rec.is_submitted is True:
            color_code = '#1ce034'
        else:
            color_code = '#ffa012'

        assessment_conf_names = assessment_configration_master.objects.all()
        conf_list = ['Ab', 'NA']
        for conf in assessment_conf_names:
            conf_list.append(str(conf.short_name))

        entered_value = '0-9.'
        for c in conf_list: entered_value = str(c) + str(entered_value)

        iterator_substrand = make_incrementor(0)

        form_vals = {
            'entered_value': entered_value,
            'conf_list': conf_list,
            'total_mark': total_mark,
            'Selected_Section_Id': academic_frequency_rec.academic_mapping.section_name.id,
            'selected_section_name': academic_frequency_rec.academic_mapping.section_name,
            'selected_section': academic_frequency_rec.academic_mapping.section_name,
            # 'section_list': all_section_recs,

            'Selected_Class_Id': academic_frequency_rec.academic_mapping.class_name.id,
            'selected_class_name': academic_frequency_rec.academic_mapping.class_name.class_name,
            # 'selected_class': selected_class_rec,
            # 'class_list': all_class_recs,

            'Selected_Year_Id': academic_frequency_rec.academic_mapping.year_name.id,
            'selected_year_name': academic_frequency_rec.academic_mapping.year_name.year_name,
            'selected_year': academic_frequency_rec.academic_mapping.year_name,
            # 'year_list': all_academicyr_recs,

            'frequency_recs': academic_frequency_rec.frequency,
            'frequency_data': academic_frequency_rec.frequency.id,
            'scheme_recs': academic_frequency_rec.scheme_mapping.scheme_name,
            'subtopics_recs': subtopics_recs,
            # 'selected_scheme': selected_scheme,

            'academic_frequency_rec': academic_frequency_rec,
            'subtopic_data': subtopic_recs,
            'color_code': color_code,
            'grade_colors': grade_colors,
            'subject_recs': academic_frequency_rec.scheme_mapping.subject,
            'skill_excel_form': skill_excel_form,
            'sub_strand_list': sub_strand_list,

            'filter_flag': filter_flag,
            'subtopics_list': subtopics_list,
            'topic_rec': topic_form_data,
            'sub_strand_rec': sub_strand_dict,
            'topic_list': topic_list,
            'sub_strand_dict_list': sub_strand_dict_list,
            'topic_dict_list': topic_dict_list,
            'conf_dict_list': assessment_conf_names,
            'subtopic_recs_len': len(subtopics_recs),
            'rec_id':rec_id,
            'iterator_substrand': iterator_substrand,
            'edit_flag':True,
        }

    except Exception as e:
        # messages.warning(request, 'Invalid Filter Selected.')
        return redirect('/school/update_entered_scheme_mark/' + str(rec_id))
        # return redirect('/school/entered_scheme_mark_list/')

    return render(request, "edit_scheme_Entered_mark.html", form_vals)



# @user_login_required
# def SkillReportedScore(request, rec_id):
#
#     try:
#         academic_frequency_rec = scheme_academic_frequency_mapping.objects.get(id = rec_id)
#         skill_excel_form = SkillExcelForm()
#         scheme_mapped_flag = False
#         selected_scheme = scheme.objects.get(id=academic_frequency_rec.scheme_mapping.scheme_name.id)
#
#         if selected_scheme.subject_scheme_name:
#             scheme_mapped_flag = True
#
#         subtopics_recs = []
#         subtopic_recs =[]
#         list = []
#         sub_strand_list = []
#         filter_flag = '3'
#
#         sub_strand_dict_list = []
#         sub_strand_dict_id = []
#         topic_dict_id = []
#         topic_dict_list = []
#
#         for strand in selected_scheme.stand_ids.all():
#             for sub_strand in strand.sub_strand_ids.all():
#                 sub_strand_dict = {}
#                 sub_strand_temp_dict = {}
#                 if scheme_mapped_flag:
#                     sub_strand_temp_dict['sub_strand_name'] = subject_substrands.objects.get(id = sub_strand.sub_strand_name).substrands_name
#                 else:
#                     sub_strand_temp_dict['sub_strand_name'] = sub_strand.sub_strand_name
#
#                 sub_strand_temp_dict['id'] = sub_strand.id
#                 sub_strand_list.append(sub_strand_temp_dict)
#                 if sub_strand.id not in sub_strand_dict_id:
#                     sub_strand_dict_id.append(sub_strand.id)
#                     if scheme_mapped_flag:
#                         sub_strand_dict['sub_strand_name'] = subject_substrands.objects.get(id = sub_strand.sub_strand_name).substrands_name
#                         sub_strand_dict['topic_count'] = 0
#                     else:
#                         sub_strand_dict['sub_strand_name'] = sub_strand.sub_strand_name
#                         sub_strand_dict['topic_count'] = 0
#
#                 for topic in sub_strand.topic_ids.all():
#                     topic_dict = {}
#                     if topic.id not in topic_dict_id:
#                         topic_dict['topic_name'] = topic.topic_name
#                         topic_dict['subtopic_count'] = topic.sub_topic_ids.all().count()
#                         topic_dict_id.append(topic.id)
#                         topic_dict_list.append(topic_dict)
#                         sub_strand_dict['topic_count'] += topic.sub_topic_ids.all().count()
#                 sub_strand_dict_list.append(sub_strand_dict)
#
#         grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)
#
#         for data in academic_frequency_rec.student_mark_ids.all():
#             for rec in data.subtopic_ids.all():
#                 if rec.subtopic.id not in list:
#                     list.append(rec.subtopic.id)
#                     subtopic_recs.append(rec.subtopic)
#
#         total_mark = 0
#         for subtopic_rec in subtopic_recs:
#             total_mark = int(subtopic_rec.subtopic_mark) + total_mark
#
#         if academic_frequency_rec.is_submitted is True:
#             color_code = '#1ce034'
#         else:
#             color_code = '#ffa012'
#
#         assessment_conf_names = assessment_configration_master.objects.filter().values_list('short_name', flat=1)
#         conf_list = ['Ab', 'NA']
#         for conf in assessment_conf_names:
#             conf_list.append(str(conf))
#
#         entered_value = '0-9.'
#         for c in conf_list: entered_value = str(c) + str(entered_value)
#
#         form_vals = {
#             'entered_value': entered_value,
#             'conf_list': conf_list,
#             'total_mark': total_mark,
#             'Selected_Section_Id': academic_frequency_rec.academic_mapping.section_name.id,
#             'selected_section_name': academic_frequency_rec.academic_mapping.section_name,
#             'selected_section': academic_frequency_rec.academic_mapping.section_name,
#
#             'Selected_Class_Id': academic_frequency_rec.academic_mapping.class_name.id,
#             'selected_class_name': academic_frequency_rec.academic_mapping.class_name.class_name,
#
#             'Selected_Year_Id': academic_frequency_rec.academic_mapping.year_name.id,
#             'selected_year_name': academic_frequency_rec.academic_mapping.year_name.year_name,
#             'selected_year': academic_frequency_rec.academic_mapping.year_name,
#
#             'frequency_recs': academic_frequency_rec.frequency,
#             'frequency_data':  academic_frequency_rec.frequency.id,
#             'scheme_recs': academic_frequency_rec.scheme_mapping.scheme_name,
#             'subtopics_recs': subtopics_recs,
#
#             'academic_frequency_rec': academic_frequency_rec,
#             'subtopic_data': subtopic_recs,
#             'color_code':color_code,
#             'grade_colors':grade_colors,
#             'subject_recs': academic_frequency_rec.scheme_mapping.subject,
#
#             'skill_excel_form':skill_excel_form,
#             'sub_strand_list':sub_strand_list,
#             'filter_flag':filter_flag,
#             'sub_strand_dict_list':sub_strand_dict_list,
#             'topic_dict_list':topic_dict_list,
#         }
#         return render(request, "skill_reported_score.html",form_vals)
#
#     except:
#         messages.warning(request, 'Some Error occurred.')
#     return redirect('/school/entered_scheme_mark_list/')


@user_login_required
def FilterSkillReportedScore(request):
    if request.POST:
        request.session['val_reported_data'] = request.POST
        rec_id = request.POST.get('academic_frequency_rec')
        filter_form = request.POST.get('filter_form')
    else:
        rec_id = request.session['val_reported_data'].get('academic_frequency_rec')
        filter_form = request.session['val_reported_data'].get('filter_form')

    try:
        filter_flag_name = ''

        try:
            if int(filter_form) == 1:
                filter_flag_name = 'Sub-Strand'
            elif int(filter_form) == 2:
                filter_flag_name = 'Topic'
            else:
                filter_flag_name = 'Sub-Topic'
        except:
            filter_flag_name = 'Topic'

        sub_strand_dict_list = []
        topic_dict_list = []

        academic_frequency_rec = scheme_academic_frequency_mapping.objects.get(id=rec_id)
        grade_obj = scheme_academic_frequency_mapping.objects.get(id=rec_id).scheme_mapping.grade_name
        skill_excel_form = SkillExcelForm()
        selected_scheme = scheme.objects.get(id=academic_frequency_rec.scheme_mapping.scheme_name.id)

        greading_recs = grade_sub_details.objects.filter(grade=grade_obj)

        col_full_arr = []
        for color in greading_recs:  # getting grades and creating its dict for the backend color for Topic and sub-strand filter
            color_dic = {
                'mar_less': float(color.marks_greater),
                'mar_greater': float(color.marks_less),
                'grade': color.grades,
                'grade_obj': color
            }
            col_full_arr.append(color_dic)

        sub_strand_dict = {}
        subtopics_recs = []
        subtopics_list = []
        topic_recs = []
        subtopic_recs = []
        substrand_recs = []
        list_rec = []
        sub_strand_list = []
        topic_list = []
        filter_flag = filter_form

        if int(filter_form) == 2:
            rec_list = []

            for strand_rec in selected_scheme.stand_ids.all():
                for substrand_rec in strand_rec.sub_strand_ids.all():
                    for topic_rec in substrand_rec.topic_ids.all():
                        topic_recs.append(topic_rec.topic_name)
                        sub_topic_ids_list = topic_rec.sub_topic_ids.all().values_list('id', flat=True)
                        for acd_rec in academic_frequency_rec.student_mark_ids.all().order_by(
                                'student__first_name'):  # fetching records of student in Skill enter mark
                            mark_scored = 0
                            total_score = 0
                            total_score_chk = False
                            temp_dict = {}
                            weightage = 0
                            grade_obtained=''
                            for rec in acd_rec.subtopic_ids.filter(subtopic__sub_topic_id__in=sub_topic_ids_list):
                                total_score += float(rec.subtopic.subtopic_mark)
                                try:
                                    mark_scored += float(rec.mark)
                                    total_score_chk = True
                                except:

                                    if rec.mark:
                                        try:
                                            assessment_configration = assessment_configration_master.objects.get(short_name=rec.mark)
                                            if assessment_configration.select == 'exclude':
                                                total_score -= float(rec.subtopic.subtopic_mark)
                                                pass
                                            else:
                                                total_score_chk = True
                                                mark_scored += 0
                                        except:
                                            total_score_chk = True
                                            mark_scored += 0
                                            pass
                                    else:
                                        total_score_chk = True
                                        mark_scored += 0

                            if total_score_chk:
                                if total_score != 0:
                                    weightage = round((float(mark_scored) * 100 / total_score),2)  # calculating the %  of their marks to get color and grade from grading table

                                for color_rec in col_full_arr:
                                    if weightage == 100:
                                        if len(col_full_arr) > 0:
                                            grade_obtained =  col_full_arr[-1]['grade_obj']
                                            break

                                    if color_rec['mar_less'] <= weightage < color_rec['mar_greater']:  # getting grade of the specific weightage
                                        grade_obtained = color_rec['grade_obj']
                                        break
                                    # grade_color = color_rec['color']

                            # mark_scored = 0
                            # total_score = 0
                            # temp_dict = {}
                            # weightage = 0
                            # for rec in acd_rec.subtopic_ids.filter(subtopic__sub_topic_id__in=sub_topic_ids_list):
                            #     total_score += float(rec.subtopic.subtopic_mark)
                            #     try:
                            #         mark_scored += float(rec.mark)
                            #     except:
                            #         mark_scored += 0
                            #
                            # if total_score != 0:
                            #     weightage = round((float(mark_scored) * 100 / total_score),
                            #                       2)  # calculating the %  of their marks to get color and grade from grading table
                            #
                            # for color_rec in col_full_arr:
                            #     if color_rec['mar_less'] <= weightage <= color_rec[
                            #         'mar_greater']:  # getting grade of the specific weightage
                            #         grade_obtained = color_rec['grade_obj']
                            #         # grade_color = color_rec['color']

                            temp_dict['total_score'] = total_score
                            temp_dict['mark_scored'] = mark_scored
                            temp_dict['topic_name'] = topic_rec.topic_name
                            temp_dict['grade_obtained'] = grade_obtained
                            rec_list.append({'stud_id': acd_rec.student, 'stud_rec': temp_dict})

            final_dict = {}
            for i in rec_list:  # collectively arranging the students data as per their mark obtained in enter mark
                if i["stud_id"] not in final_dict:
                    final_dict[i["stud_id"]] = {'rec': [
                        {'total_score': i['stud_rec']['total_score'], 'mark_scored': i['stud_rec']['mark_scored'],
                         'topic_name': i['stud_rec']['topic_name'], 'grade_obtained': i['stud_rec']['grade_obtained']}]}
                else:
                    final_dict[i["stud_id"]]['rec'].append(
                        {'total_score': i['stud_rec']['total_score'], 'mark_scored': i['stud_rec']['mark_scored'],
                         'topic_name': i['stud_rec']['topic_name'], 'grade_obtained': i['stud_rec']['grade_obtained']})

            if academic_frequency_rec.is_submitted is True:
                color_code = '#1ce034'
            else:
                color_code = '#ffa012'

            grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)

            form_vals = {

                'Selected_Section_Id': academic_frequency_rec.academic_mapping.section_name.id,
                'selected_section_name': academic_frequency_rec.academic_mapping.section_name,
                'selected_section': academic_frequency_rec.academic_mapping.section_name,

                'Selected_Class_Id': academic_frequency_rec.academic_mapping.class_name.id,
                'selected_class_name': academic_frequency_rec.academic_mapping.class_name.class_name,

                'Selected_Year_Id': academic_frequency_rec.academic_mapping.year_name.id,
                'selected_year_name': academic_frequency_rec.academic_mapping.year_name.year_name,
                'selected_year': academic_frequency_rec.academic_mapping.year_name,

                'frequency_recs': academic_frequency_rec.frequency,
                'frequency_data': academic_frequency_rec.frequency.id,
                'scheme_recs': academic_frequency_rec.scheme_mapping.scheme_name,
                'subtopics_recs': subtopics_recs,

                'academic_frequency_rec': academic_frequency_rec,
                'subtopic_data': subtopic_recs,
                'color_code': color_code,
                'grade_colors': grade_colors,

                'subject_recs': academic_frequency_rec.scheme_mapping.subject,
                'skill_excel_form': skill_excel_form,
                'sub_strand_list': sub_strand_list,
                'filter_flag': filter_flag,

                'subtopics_list': subtopics_list,
                'sub_strand_rec': sub_strand_dict,
                'topic_list': topic_list,
                'sub_strand_dict_list': sub_strand_dict_list,
                'topic_dict_list': topic_dict_list,
                'filter_type': filter_form,

                'final_dict': final_dict,
                'topic_recs': topic_recs,
                'filter_flag_name': filter_flag_name,
            }
            return render(request, "skill_reported_score.html", form_vals)



        if int(filter_form) == 1:
            final_list = []
            rec_list = []

            for strand_rec in selected_scheme.stand_ids.all():
                for substrand_rec in strand_rec.sub_strand_ids.all():
                    try:
                       sub_strand_id = int(substrand_rec.sub_strand_name)
                       get_sub_strand_name = subject_substrands.objects.get(id=sub_strand_id)
                       substrand_recs.append(get_sub_strand_name.substrands_name)
                    except:
                        substrand_recs.append(substrand_rec.sub_strand_name)

                    for acd_rec in academic_frequency_rec.student_mark_ids.all().order_by('student__first_name'):
                        sub_topic_ids_list = []
                        for topic_rec in substrand_rec.topic_ids.all():
                            for top_id in topic_rec.sub_topic_ids.all().values_list('id', flat=True):
                                sub_topic_ids_list.append(top_id)

                        mark_scored = 0
                        total_score = 0
                        temp_dict = {}
                        weightage = 0
                        total_score_chk = False
                        grade_obtained = ''

                        for rec in acd_rec.subtopic_ids.filter(subtopic__sub_topic_id__in=sub_topic_ids_list):
                            total_score += float(rec.subtopic.subtopic_mark)
                            try:
                                mark_scored += float(rec.mark)
                                total_score_chk = True
                            except:
                                if rec.mark:
                                    try:
                                        assessment_configration = assessment_configration_master.objects.get(
                                            short_name=rec.mark)
                                        if str(assessment_configration.select) == 'exclude':
                                            total_score -= float(rec.subtopic.subtopic_mark)
                                            pass
                                        else:
                                            total_score_chk = True
                                            mark_scored += 0
                                    except:
                                        total_score_chk = True
                                        mark_scored += 0
                                        pass
                                else:
                                    total_score_chk = True
                                    mark_scored += 0

                        if total_score_chk:
                            if total_score != 0:
                                weightage = round((float(mark_scored) * 100 / total_score),
                                                  2)  # calculating the %  of their marks to get color and grade from grading table

                            for color_rec in col_full_arr:
                                if weightage == 100:
                                    if len(col_full_arr) > 0:
                                        grade_obtained = col_full_arr[-1]['grade_obj']
                                        break

                                if color_rec['mar_less'] <= weightage < color_rec['mar_greater']:  # getting grade rec of the specific weightage
                                    grade_obtained = color_rec['grade_obj']
                                    break

                        temp_dict['total_score'] = total_score
                        temp_dict['mark_scored'] = mark_scored
                        temp_dict['substrand_name'] = substrand_rec.sub_strand_name
                        temp_dict['grade_obtained'] = grade_obtained
                        rec_list.append({'stud_id': acd_rec.student, 'stud_rec': temp_dict})

            final_dict = {}
            for i in rec_list:  # collectively arranging the students data as per their mark obtained in enter mark
                if i["stud_id"] not in final_dict:
                    final_dict[i["stud_id"]] = {'rec': [
                        {'total_score': i['stud_rec']['total_score'], 'mark_scored': i['stud_rec']['mark_scored'],
                         'substrand_name': i['stud_rec']['substrand_name'],
                         'grade_obtained': i['stud_rec']['grade_obtained']}]}
                else:
                    final_dict[i["stud_id"]]['rec'].append(
                        {'total_score': i['stud_rec']['total_score'], 'mark_scored': i['stud_rec']['mark_scored'],
                         'substrand_name': i['stud_rec']['substrand_name'],
                         'grade_obtained': i['stud_rec']['grade_obtained']})

            if academic_frequency_rec.is_submitted is True:
                color_code = '#1ce034'
            else:
                color_code = '#ffa012'

            grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)

            form_vals = {

                'Selected_Section_Id': academic_frequency_rec.academic_mapping.section_name.id,
                'selected_section_name': academic_frequency_rec.academic_mapping.section_name,
                'selected_section': academic_frequency_rec.academic_mapping.section_name,

                'Selected_Class_Id': academic_frequency_rec.academic_mapping.class_name.id,
                'selected_class_name': academic_frequency_rec.academic_mapping.class_name.class_name,

                'Selected_Year_Id': academic_frequency_rec.academic_mapping.year_name.id,
                'selected_year_name': academic_frequency_rec.academic_mapping.year_name.year_name,
                'selected_year': academic_frequency_rec.academic_mapping.year_name,

                'frequency_recs': academic_frequency_rec.frequency,
                'frequency_data': academic_frequency_rec.frequency.id,
                'scheme_recs': academic_frequency_rec.scheme_mapping.scheme_name,
                'subtopics_recs': subtopics_recs,

                'academic_frequency_rec': academic_frequency_rec,
                'subtopic_data': subtopic_recs,
                'color_code': color_code,
                'grade_colors': grade_colors,

                'subject_recs': academic_frequency_rec.scheme_mapping.subject,
                'skill_excel_form': skill_excel_form,
                'sub_strand_list': sub_strand_list,
                'filter_flag': filter_flag,

                'subtopics_list': subtopics_list,
                'sub_strand_rec': sub_strand_dict,
                'topic_list': topic_list,
                'sub_strand_dict_list': sub_strand_dict_list,
                'topic_dict_list': topic_dict_list,
                'filter_type': filter_form,

                'final_dict': final_dict,
                'substrand_recs': substrand_recs,
                'filter_flag_name': filter_flag_name,
            }
            return render(request, "skill_reported_score.html", form_vals)

        elif int(filter_form) == 3:

            for data in academic_frequency_rec.student_mark_ids.all().order_by('student__first_name'):
                for rec in data.subtopic_ids.all():
                    if rec.subtopic.id not in list_rec:
                        list_rec.append(rec.subtopic.id)
                        subtopic_recs.append(rec.subtopic)

            if academic_frequency_rec.is_submitted is True:
                color_code = '#1ce034'
            else:
                color_code = '#ffa012'

            grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)

            form_vals = {

                'Selected_Section_Id': academic_frequency_rec.academic_mapping.section_name.id,
                'selected_section_name': academic_frequency_rec.academic_mapping.section_name,
                'selected_section': academic_frequency_rec.academic_mapping.section_name,

                'Selected_Class_Id': academic_frequency_rec.academic_mapping.class_name.id,
                'selected_class_name': academic_frequency_rec.academic_mapping.class_name.class_name,

                'Selected_Year_Id': academic_frequency_rec.academic_mapping.year_name.id,
                'selected_year_name': academic_frequency_rec.academic_mapping.year_name.year_name,
                'selected_year': academic_frequency_rec.academic_mapping.year_name,

                'frequency_recs': academic_frequency_rec.frequency,
                'frequency_data': academic_frequency_rec.frequency.id,
                'scheme_recs': academic_frequency_rec.scheme_mapping.scheme_name,
                'subtopics_recs': subtopics_recs,

                'academic_frequency_rec': academic_frequency_rec,
                'subtopic_data': subtopic_recs,
                'color_code': color_code,
                'grade_colors': grade_colors,

                'subject_recs': academic_frequency_rec.scheme_mapping.subject,
                'skill_excel_form': skill_excel_form,
                'sub_strand_list': sub_strand_list,
                'filter_flag': filter_flag,

                'subtopics_list': subtopics_list,
                'sub_strand_rec': sub_strand_dict,
                'topic_list': topic_list,
                'sub_strand_dict_list': sub_strand_dict_list,
                'topic_dict_list': topic_dict_list,
                'filter_type': filter_form,
                'filter_flag_name': filter_flag_name,
            }
            return render(request, "skill_reported_score.html", form_vals)
        else:
            messages.warning(request, 'Filtration Not Match.')
            return redirect('/school/entered_scheme_mark_list/')

    except Exception as e:
        messages.warning(request, 'Invalid Filter Selected.')

    return redirect('/school/entered_scheme_mark_list/')


@user_login_required
def SubmitSkillReportedScore(request, rec_id):
    try:
        rec = scheme_academic_frequency_mapping.objects.get(id=rec_id)
        if rec.is_apprpved==True:
            scheme_academic_frequency_mapping.objects.filter(id=rec_id).update(is_reported_submitted=True)
        else:
            messages.warning(request, 'Selected marks are not approved.')

    except:
        messages.warning(request, 'Some Error occurred.')
    return redirect('/school/filter_skill_reported_score/')



@user_login_required
def ExportSkillReportedScore(request):
    if request.POST:
        request.session['val_reported_data'] = request.POST
        rec_id = request.POST.get('academic_frequency_rec')
        filter_form = request.POST.get('filter_form')
        export_type = request.POST.get('export_type')
    else:
        rec_id = request.session['val_reported_data'].get('academic_frequency_rec')
        filter_form = request.session['val_reported_data'].get('filter_form')
        export_type = request.session['val_reported_data'].get('export_type')

    try:
        sub_strand_dict_list = []
        topic_dict_list = []

        academic_frequency_rec = scheme_academic_frequency_mapping.objects.get(id=rec_id)
        grade_obj = scheme_academic_frequency_mapping.objects.get(id=rec_id).scheme_mapping.grade_name
        skill_excel_form = SkillExcelForm()
        selected_scheme = scheme.objects.get(id=academic_frequency_rec.scheme_mapping.scheme_name.id)

        greading_recs = grade_sub_details.objects.filter(grade=grade_obj)

        col_full_arr = []
        for color in greading_recs:  # getting grades and creating its dict for the backend color for Topic and sub-strand filter
            color_dic = {
                'mar_less': float(color.marks_greater),
                'mar_greater': float(color.marks_less),
                'grade': color.grades,
                'grade_obj': color
            }
            col_full_arr.append(color_dic)

        topic_recs = []
        substrand_recs = []

        if int(filter_form) == 2:
            rec_list = []

            for strand_rec in selected_scheme.stand_ids.all():
                for substrand_rec in strand_rec.sub_strand_ids.all():
                    for topic_rec in substrand_rec.topic_ids.all():
                        topic_recs.append(topic_rec.topic_name)
                        sub_topic_ids_list = topic_rec.sub_topic_ids.all().values_list('id', flat=True)
                        for acd_rec in academic_frequency_rec.student_mark_ids.all().order_by(
                                'student__first_name'):  # fetching records of student in Skill enter mark
                            mark_scored = 0
                            total_score = 0
                            temp_dict = {}
                            weightage = 0
                            for rec in acd_rec.subtopic_ids.filter(subtopic__sub_topic_id__in=sub_topic_ids_list):
                            #     total_score += float(rec.subtopic.subtopic_mark)
                            #     try:
                            #         mark_scored += float(rec.mark)
                            #     except:
                            #         mark_scored += 0
                            #
                            # if total_score != 0:
                            #     weightage = round((float(mark_scored) * 100 / total_score),2)  # calculating the %  of their marks to get color and grade from grading table
                            #
                            # for color_rec in col_full_arr:
                            #     if color_rec['mar_less'] <= weightage <= color_rec['mar_greater']:  # getting grade of the specific weightage
                            #         grade_obtained = color_rec['grade_obj']
                            #         # grade_color = color_rec['color']

                                total_score += float(rec.subtopic.subtopic_mark)
                                try:
                                    mark_scored += float(rec.mark)
                                    total_score_chk = True
                                except:

                                    if rec.mark:
                                        try:
                                            assessment_configration = assessment_configration_master.objects.get(
                                                short_name=rec.mark)
                                            if assessment_configration.select == 'exclude':
                                                total_score_chk = False
                                                grade_obtained = ''
                                                total_score -= float(rec.subtopic.subtopic_mark)
                                            else:
                                                total_score_chk = True
                                                mark_scored += 0
                                        except:
                                            total_score_chk = True
                                            mark_scored += 0
                                            pass
                                    else:
                                        total_score_chk = True
                                        mark_scored += 0

                                if total_score_chk:
                                    if total_score != 0:
                                        weightage = round((float(mark_scored) * 100 / total_score),
                                                          2)  # calculating the %  of their marks to get color and grade from grading table

                                    for color_rec in col_full_arr:
                                        if weightage == 100:
                                            if len(col_full_arr) > 0:
                                                grade_obtained = col_full_arr[-1]['grade_obj']
                                                break

                                        if color_rec['mar_less'] <= weightage < color_rec[
                                            'mar_greater']:  # getting grade of the specific weightage
                                            grade_obtained = color_rec['grade_obj']
                                            break
                                        # grade_color = color_rec['color']

                            temp_dict['total_score'] = total_score
                            temp_dict['mark_scored'] = mark_scored
                            temp_dict['sub_strand_name'] = substrand_rec.sub_strand_name
                            temp_dict['topic_name'] = topic_rec.topic_name
                            temp_dict['grade_obtained'] = grade_obtained
                            rec_list.append({'stud_id': acd_rec.student, 'stud_rec': temp_dict})

            final_dict = {}
            for i in rec_list:  # collectively arranging the students data as per their mark obtained in enter mark
                if i["stud_id"] not in final_dict:
                    final_dict[i["stud_id"]] = {'rec': [
                        {'total_score': i['stud_rec']['total_score'], 'mark_scored': i['stud_rec']['mark_scored'],
                         'topic_name': i['stud_rec']['topic_name'], 'grade_obtained': i['stud_rec']['grade_obtained'],
                         'sub_strand_name': i['stud_rec']['sub_strand_name']}]}
                else:
                    final_dict[i["stud_id"]]['rec'].append(
                        {'total_score': i['stud_rec']['total_score'], 'mark_scored': i['stud_rec']['mark_scored'],
                         'topic_name': i['stud_rec']['topic_name'], 'grade_obtained': i['stud_rec']['grade_obtained'],
                         'sub_strand_name': i['stud_rec']['sub_strand_name']})

            column_names = ['Academic Year', 'Class', 'Section', 'Student Name', 'Student Code','Scodoo Id', 'Subject', 'Frequency',
                            'Level 2', 'Level 3', 'Grade']

            final_list = []
            for student in final_dict:
                for stud_acd_rec in final_dict[student]['rec']:
                    temp_list = []
                    temp_list.append(student.academic_class_section.year_name.year_name)
                    temp_list.append(student.academic_class_section.class_name.class_name)
                    temp_list.append(student.academic_class_section.section_name.section_name)
                    temp_list.append(student.get_all_name())
                    temp_list.append(student.student_code)
                    temp_list.append(student.odoo_id)
                    temp_list.append(academic_frequency_rec.scheme_mapping.subject.subject_name)
                    temp_list.append(academic_frequency_rec.frequency.frequency_name)
                    # temp_list.append(stud_acd_rec['sub_strand_name'])

                    try:
                        sub_strand_id = int(stud_acd_rec['sub_strand_name'])
                        get_sub_strand_name = subject_substrands.objects.get(id=sub_strand_id)
                        temp_list.append(get_sub_strand_name.substrands_name)
                    except:
                        temp_list.append(stud_acd_rec['sub_strand_name'])

                    temp_list.append(stud_acd_rec['topic_name'])
                    temp_list.append(stud_acd_rec['grade_obtained'].grades if stud_acd_rec['grade_obtained'] != '' else '')
                    final_list.append(temp_list)
            if export_type == 'pdf':
                final_list.insert(0,column_names)
                return export_pdf('export_skill_entry', final_list)
            else:
                return export_freezd_column_xls('export_skill_entry', column_names, final_list)

        if int(filter_form) == 1:
            final_list = []
            rec_list = []

            for strand_rec in selected_scheme.stand_ids.all():
                for substrand_rec in strand_rec.sub_strand_ids.all():
                    substrand_recs.append(substrand_rec.sub_strand_name)
                    for acd_rec in academic_frequency_rec.student_mark_ids.all().order_by('student__first_name'):
                        sub_topic_ids_list = []
                        for topic_rec in substrand_rec.topic_ids.all():
                            for top_id in topic_rec.sub_topic_ids.all().values_list('id', flat=True):
                                sub_topic_ids_list.append(top_id)

                        mark_scored = 0
                        total_score = 0
                        temp_dict = {}
                        weightage = 0
                        for rec in acd_rec.subtopic_ids.filter(subtopic__sub_topic_id__in=sub_topic_ids_list):
                        #     total_score += float(rec.subtopic.subtopic_mark)
                        #     try:
                        #         mark_scored += float(rec.mark)
                        #     except:
                        #         mark_scored += 0
                        #
                        # if total_score != 0:
                        #     weightage = round((float(mark_scored) * 100 / total_score),
                        #                       2)  # calculating the %  of their marks to get color and grade from grading table
                        #
                        # for color_rec in col_full_arr:
                        #     if color_rec['mar_less'] <= weightage <= color_rec['mar_greater']:  # getting grade rec of the specific weightage
                        #         grade_obtained = color_rec['grade_obj']

                            total_score += float(rec.subtopic.subtopic_mark)
                            try:
                                mark_scored += float(rec.mark)
                                total_score_chk = True
                            except:

                                if rec.mark:
                                    try:
                                        assessment_configration = assessment_configration_master.objects.get(
                                            short_name=rec.mark)
                                        if assessment_configration.select == 'exclude':
                                            total_score_chk = False
                                            grade_obtained = ''
                                            total_score -= float(rec.subtopic.subtopic_mark)
                                        else:
                                            total_score_chk = True
                                            mark_scored += 0
                                    except:
                                        total_score_chk = True
                                        mark_scored += 0
                                        pass
                                else:
                                    total_score_chk = True
                                    mark_scored += 0

                            if total_score_chk:
                                if total_score != 0:
                                    weightage = round((float(mark_scored) * 100 / total_score),
                                                      2)  # calculating the %  of their marks to get color and grade from grading table

                                for color_rec in col_full_arr:
                                    if weightage == 100:
                                        if len(col_full_arr) > 0:
                                            grade_obtained = col_full_arr[-1]['grade_obj']
                                            break

                                    if color_rec['mar_less'] <= weightage < color_rec[
                                        'mar_greater']:  # getting grade of the specific weightage
                                        grade_obtained = color_rec['grade_obj']
                                        break

                        temp_dict['total_score'] = total_score
                        temp_dict['mark_scored'] = mark_scored
                        temp_dict['substrand_name'] = substrand_rec.sub_strand_name
                        temp_dict['grade_obtained'] = grade_obtained
                        rec_list.append({'stud_id': acd_rec.student, 'stud_rec': temp_dict})

            final_dict = {}
            for i in rec_list:  # collectively arranging the students data as per their mark obtained in enter mark
                if i["stud_id"] not in final_dict:
                    final_dict[i["stud_id"]] = {'rec': [
                        {'total_score': i['stud_rec']['total_score'], 'mark_scored': i['stud_rec']['mark_scored'],
                         'substrand_name': i['stud_rec']['substrand_name'],
                         'grade_obtained': i['stud_rec']['grade_obtained']}]}
                else:
                    final_dict[i["stud_id"]]['rec'].append(
                        {'total_score': i['stud_rec']['total_score'], 'mark_scored': i['stud_rec']['mark_scored'],
                         'substrand_name': i['stud_rec']['substrand_name'],
                         'grade_obtained': i['stud_rec']['grade_obtained']})

            column_names = ['Academic Year', 'Class', 'Section', 'Student Name', 'Student Code','Scodoo Id', 'Subject', 'Frequency',
                            'Level 2', 'Grade']

            final_list = []
            for student in final_dict:
                for stud_acd_rec in final_dict[student]['rec']:
                    temp_list = []
                    temp_list.append(student.academic_class_section.year_name.year_name)
                    temp_list.append(student.academic_class_section.class_name.class_name)
                    temp_list.append(student.academic_class_section.section_name.section_name)
                    temp_list.append(student.get_all_name())
                    temp_list.append(student.student_code)
                    temp_list.append(student.odoo_id)
                    temp_list.append(academic_frequency_rec.scheme_mapping.subject.subject_name)
                    temp_list.append(academic_frequency_rec.frequency.frequency_name)
                    # temp_list.append(stud_acd_rec['substrand_name'])

                    try:
                        sub_strand_id = int(stud_acd_rec['substrand_name'])
                        get_sub_strand_name = subject_substrands.objects.get(id=sub_strand_id)
                        temp_list.append(get_sub_strand_name.substrands_name)
                    except:
                        temp_list.append(stud_acd_rec['substrand_name'])


                    temp_list.append(stud_acd_rec['grade_obtained'].grades if stud_acd_rec['grade_obtained'] != '' else '')
                    final_list.append(temp_list)

            if export_type == 'pdf':
                final_list.insert(0,column_names)
                return export_pdf('export_skill_entry', final_list)
            else:
                return export_freezd_column_xls('export_skill_entry', column_names, final_list)

        elif int(filter_form) == 3:

            scheme_rec = scheme_academic_frequency_mapping.objects.get(id=rec_id)

            column_names = ['Academic Year', 'Class', 'Section', 'Student Name', 'Student Code','Scodoo Id' ,'Subject', 'Frequency',
                            'Level 2', 'Level 3', 'Level 4', 'Grade']
            filtered_students = []
            weightage = 0
            for student_rec in scheme_rec.student_mark_ids.all():
                for rec in student_rec.subtopic_ids.all():
                    rec_list = []
                    rec_list.append(scheme_rec.academic_mapping.year_name.year_name)
                    rec_list.append(scheme_rec.academic_mapping.class_name.class_name)
                    rec_list.append(scheme_rec.academic_mapping.section_name.section_name)
                    rec_list.append(student_rec.student.get_all_name())
                    rec_list.append(student_rec.student.student_code)
                    rec_list.append(student_rec.student.odoo_id)
                    rec_list.append(scheme_rec.scheme_mapping.subject.subject_name)
                    rec_list.append(scheme_rec.frequency.frequency_name)

                    for sub_rec in rec.subtopic.sub_topic.topic_set.all():
                        for sub_sub_rec in sub_rec.sub_strand_set.all():
                            try:
                                sub_strand_id = int(sub_sub_rec.sub_strand_name)
                                sub_strand_rec = subject_substrands.objects.get(id=sub_strand_id)
                                rec_list.append(sub_strand_rec.substrands_name)
                            except:
                                rec_list.append(sub_sub_rec.sub_strand_name)
                        rec_list.append(sub_rec.topic_name)
                    rec_list.append(rec.subtopic.sub_topic.name)

                    try:
                        max_mark = int(rec.subtopic.subtopic_mark)
                        weightage = round((float(rec.mark) * 100 / int(max_mark)),2)  # calculating the %  of their marks to get color and grade from grading table
                    except:
                        weightage = 0
                    for color_rec in col_full_arr:
                        if color_rec['mar_less'] <= weightage <= color_rec['mar_greater']:  # getting grade of the specific weightage
                            grade_obtained = color_rec['grade_obj'].grades
                            break

                    rec_list.append(grade_obtained)
                    filtered_students.append(rec_list)

            if export_type == 'pdf':
                filtered_students.insert(0,column_names)
                return export_pdf('export_skill_entry', filtered_students)
            else:
                return export_freezd_column_xls('export_skill_entry', column_names, filtered_students)
        else:
            messages.warning(request, 'Filtration Not Match.')
            return redirect('/school/entered_scheme_mark_list/')

    except:
        messages.warning(request, 'Invalid Filter Selected.')

    return redirect('/school/entered_scheme_mark_list/')


@user_login_required
def ExportEnterMarkSchemeStudentList(request):
    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    selected_year_id = request.POST.get('year_name')  # It will be used in HTML page as a value of selected Class
    selected_year_rec = academic_year.objects.get(id=selected_year_id)
    selected_year_name = selected_year_rec.year_name

    selected_class_id = request.POST.get('class_name')  # It will be used in HTML page as a value of selected Class
    selected_class_rec = class_details.objects.get(id=selected_class_id)
    selected_class_name = selected_class_rec.class_name

    selected_section_id = request.POST.get('section_name')  # It will be used in HTML page as a value of selected Section
    selected_section_rec = sections.objects.get(id=selected_section_id)
    selected_section_name = selected_section_rec.section_name

    # scheme_id = request.POST.get('scheme_name')

    frequency_name = request.POST.get('frequency_name')
    frequency_data = frequency_details.objects.get(id = frequency_name)

    selected_subject_id = request.POST.get('subject_name')  # It will be used in HTML page as a value of selected Section
    selected_subject_rec = subjects.objects.get(id=selected_subject_id)
    selected_subject_name = selected_subject_rec.subject_name

    selected_copy_from_id = request.POST.get('copy_from')

    #############
    year_class_section_obj1 = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                         section_name=selected_section_id,
                                                                         class_name=selected_class_id)
    try:
        scheme_rec1 = scheme_mapping_details.objects.get(year_id=selected_year_id, class_obj_id = selected_class_id,subject_id =  selected_subject_id,is_archived = False)
        scheme_id = scheme_rec1.scheme_name.id
    except:
        messages.warning(request, "Mapping Not Found for Selected Class And Section")
        return redirect('/school/enter_scheme_mark/')

    ############

    selected_scheme = scheme.objects.get(id =scheme_id)
    subtopics_recs = []

    for starand in selected_scheme.stand_ids.all():
        for sub_strand_recs in starand.sub_strand_ids.all():
            if sub_strand_recs:
                for topic_recs in sub_strand_recs.topic_ids.all():
                    if topic_recs:
                        for subtopics in topic_recs.sub_topic_ids.all():
                            subtopics_recs.append(subtopics)


    year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id, section_name=selected_section_id, class_name=selected_class_id)

    scheme_rec = scheme_mapping_details.objects.get(year_id=selected_year_id, class_obj_id = selected_class_id,subject_id = selected_subject_id,is_archived = False)

    mark_recs =  scheme_academic_frequency_mapping.objects.filter(scheme_mapping_id=scheme_rec.id, frequency_id = frequency_name)
    grade_colors = grade_sub_details.objects.filter(grade=scheme_rec.grade_name)

    if mark_recs:
        messages.warning(request,"Mark Has Already Been Entered For That Scheme and Students. You Can Update Them Now.")
        return redirect('/school/entered_scheme_mark_list/')


    filtered_students =[]

    if scheme_rec.subject.type == 'Optional':
        optional_subject_students = scheme_rec.subject.optional_subject_relation.all()
        for students in optional_subject_students:
            student_rec = student_details.objects.filter(id=students.id, is_active=True, academic_class_section_id = year_class_section_obj.id)
            if student_rec:
                filtered_students.append(student_rec[0])
    else:
        filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True)

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    selected_year_id = current_academic_year.id
    scheme_recs = scheme.objects.all()
    frequency_recs = frequency_details.objects.all()

    #     Passing values to the HTML page through form_vals
    form_vals = {
        'student_list': filtered_students,
        'Selected_Section_Id': selected_section_id,
        'selected_section_name': selected_section_name,
        'selected_section': selected_section_rec,
        'section_list': all_section_recs,

        'Selected_Class_Id': selected_class_id,
        'selected_class_name': selected_class_name,
        'selected_class': selected_class_rec,
        'class_list': all_class_recs,

        'Selected_Year_Id': selected_year_id,
        'selected_year_name': selected_year_name,
        'selected_year': selected_year_rec,
        'year_list': all_academicyr_recs,

        'frequency_recs': frequency_recs,
        'frequency_data': frequency_data,
        'scheme_recs': scheme_recs,
        'subtopics_recs': subtopics_recs,
        'selected_scheme': selected_scheme,
        'grade_colors': grade_colors,
        'selected_subject_id': selected_subject_id,
        'selected_subject_name': selected_subject_rec,
        # 'selected_strand': strand_rec,
    }
    return render(request, "enter_scheme_marks.html",form_vals)
    # return render(request, "enter_scheme_marks.html", {'students_recs':students_recs})

@user_login_required
def ViewMappedFrequency(request):
    year_name = request.POST['year_name']
    class_name = request.POST['class_name']
    section_name = request.POST['section_name']
    scheme = request.POST['scheme_name']
    acd_ids = []
    frequencyList = []
    dict = {}
    academic_rec = academic_class_section_mapping.objects.get(year_name=year_name, section_name=section_name, class_name=class_name)
    scheme_academic_recs = scheme_mapping_details.objects.filter(academic_class_section_mapping_id = academic_rec.id, scheme_name_id = scheme,is_archived = False )
    for scheme_academic_rec in scheme_academic_recs:
        acd_ids.append(scheme_academic_rec.frequency_id)
    acd_ids = set(acd_ids)
    frequency_recs = frequency_details.objects.filter(id__in = acd_ids)
    for frequency_rec in frequency_recs:
        dict={'id':frequency_rec.id, 'class_name': frequency_rec.frequency_name}
        frequencyList.append(dict)
    return JsonResponse(frequencyList,safe=False)



@user_login_required
def MappedScheme(request):
    schemeList = []
    schemeIds = []
    academic_year = request.GET.get('academic_year', None)
    class_name = request.GET.get('class_name', None)
    section_name = request.GET.get('section_name', None)

    acd_mapping_rec = academic_class_section_mapping.objects.get(class_name_id=class_name, section_name_id=section_name,year_name_id=academic_year)
    scheme_recs = scheme_mapping_details.objects.filter(academic_class_section_mapping_id = acd_mapping_rec.id,is_archived = False)
    for scheme_rec in scheme_recs:
        schemeIds.append(scheme_rec.scheme_name.id)
    schemeIds = set(schemeIds)

    schemes = scheme.objects.filter(id__in = schemeIds)
    dict = {}
    for schem_data in schemes:
        dict={'id':schem_data.id, 'scheme_name': schem_data.scheme_name}
        schemeList.append(dict)

    return JsonResponse(schemeList,safe=False)

@user_login_required
def GetSubStrandTopic(request):
    topic_list = []
    sub_strand_rec = request.GET.get('sub_strand', None)
    exam_filter = request.GET.get('exam_filter', None)

    if exam_filter:
        sub_strand_obj = exam_sub_strand.objects.get(id=sub_strand_rec)
    else:
        sub_strand_obj = sub_strand.objects.get(id=sub_strand_rec)

    # try:
    #     sub_strand_obj = sub_strand.objects.get(id = sub_strand_rec)
    # except:
    #     sub_strand_obj = exam_sub_strand.objects.get(id=sub_strand_rec)

    # sub_strand_obj = exam_sub_strand.objects.get(id=sub_strand_rec)
    dict = {}
    for topic_rec in sub_strand_obj.topic_ids.all():
        dict = {'id': topic_rec.id, 'topic_name': topic_rec.topic_name}
        topic_list.append(dict)

    return JsonResponse(topic_list,safe=False)


@user_login_required
def RubricGetStrandSubStrand(request):
    topic_list = []
    sub_strand_rec = request.GET.get('strand', None)

    # try:
    #     sub_strand_obj = sub_strand.objects.get(id = sub_strand_rec)
    # except:
    strand_obj = rubric_strand.objects.get(id=sub_strand_rec)
    dict = {}

    for sub_strand_rec in strand_obj.sub_strand_ids.all():
        dict = {'id': sub_strand_rec.id, 'topic_name': sub_strand_rec.sub_strand_name}
        topic_list.append(dict)

    return JsonResponse(topic_list,safe=False)

@user_login_required
def SaveSchemeMark(request):
    if request.method == 'POST':
        year_id = request.POST.get('acd_year')
        class_id = request.POST.get('class')
        section_id = request.POST.get('section')
        scheme_id = request.POST.get('scheme')
        frequency_id= request.POST.get('frequency')
        subject_id = request.POST.get('sub')
        outoff_mark = request.POST.get('outoff')
        topic_form = request.POST.get('topic_form1')
        sub_strand_filter = request.POST.get('id_sub_strand_form1')
        academic_mapping = academic_class_section_mapping.objects.get(year_name=year_id, class_name=class_id, section_name=section_id)
        scheme_rec = scheme_mapping_details.objects.get(year_id=year_id, class_obj_id=class_id,subject_id=subject_id, is_archived=False)
        mark_recs = scheme_academic_frequency_mapping.objects.filter(academic_mapping=academic_mapping,scheme_mapping_id=scheme_rec.id,frequency_id=frequency_id)
        if mark_recs:
            messages.warning(request, "Mark already entered, Update the entered marks.")
            return redirect('/school/update_entered_scheme_mark/' + str(mark_recs[0].id))

        seheme_mapping_rec = scheme_mapping_details.objects.get(year_id=year_id, class_obj_id=class_id,subject_id=subject_id, is_archived = False)
        scheme_id = seheme_mapping_rec.scheme_name.id
        data = json.loads(request.POST.get('data'))
        scheme_rec = scheme.objects.get(id=scheme_id)
        subtopic_list = []
        for starand in scheme_rec.stand_ids.all():
            for sub_strand_recs in starand.sub_strand_ids.all():
                if sub_strand_recs:
                    for topic_recs in sub_strand_recs.topic_ids.all():
                        if topic_recs:
                            for subtopics in topic_recs.sub_topic_ids.all():
                                subtopic_list.append(subtopics)

        academic_frequency_mapping_obj = scheme_academic_frequency_mapping.objects.create(academic_mapping= academic_mapping,scheme_mapping_id = seheme_mapping_rec.id, frequency_id=frequency_id,outoff_mark=outoff_mark)
        dic = {}
        for count , rec in enumerate(data[0]):
            obj = None
            temp = rec.split('-')
            obj = scheme_em_subtopic_mark.objects.create(sub_topic_id = temp[0], subtopic_mark = data[0][rec])
            id = obj.id
            dic[temp[0]]=id
        for track, rec in enumerate(data):
            if not 'studentId' in rec:
                pass
            else:
                student_mark_mapping_obj = scheme_student_mark_mapping.objects.create(student_id = rec['studentId'], total_mark = rec['totalMarks'], out_off_mark = rec['out_off_mark'])
                academic_frequency_mapping_obj.student_mark_ids.add(student_mark_mapping_obj.id)

                for count , subtopic in enumerate(subtopic_list):
                    count = count+1
                    st_id = dic[rec['sub_topic_id_' + str(count)]]
                    scheme_subtopic_obj = scheme_subtopic_mark.objects.create(mark = rec['subtopicMark_' + str(count)], subtopic_id = int(st_id))
                    student_mark_mapping_obj.subtopic_ids.add(scheme_subtopic_obj.id)
                    
    rec_id = (academic_frequency_mapping_obj.id)
    if topic_form == '' or sub_strand_filter == '':
        return redirect('/school/update_entered_scheme_mark/'+str(academic_frequency_mapping_obj.id))
    try:
        sub_strand_dict_list = []
        sub_strand_dict_id = []
        topic_dict_id = []
        topic_dict_list = []
        scheme_mapped_flag = False
        academic_frequency_rec = scheme_academic_frequency_mapping.objects.get(id=rec_id)
        skill_excel_form = SkillExcelForm()
        selected_scheme = scheme.objects.get(id=academic_frequency_rec.scheme_mapping.scheme_name.id)
        if selected_scheme.subject_scheme_name:
            scheme_mapped_flag = True
        sub_strand_data = sub_strand.objects.get(id=sub_strand_filter)
        topic_form_data = topic.objects.get(id=topic_form)
        sub_strand_dict = {}
        topic_dict = {}
        if scheme_mapped_flag:
            sub_strand_dict['sub_strand_name'] = subject_substrands.objects.get(
                id=sub_strand_data.sub_strand_name).substrands_name
        else:
            sub_strand_dict['sub_strand_name'] = sub_strand_data.sub_strand_name
        sub_strand_dict['id'] = sub_strand_data.id
        sub_strand_dict['topic_count'] = topic_form_data.sub_topic_ids.all().count()   # used for row and colspan in table head of html
        sub_strand_dict_list.append(sub_strand_dict)
        topic_dict['topic_name'] = topic_form_data.topic_name
        topic_dict['subtopic_count'] = topic_form_data.sub_topic_ids.all().count()    # used for row and colspan in table head of html
        topic_dict_list.append(topic_dict)
        subtopics_recs = []
        subtopics_list = []
        subtopics_ids = []
        subtopic_recs = []
        list = []
        sub_strand_list = []
        topic_list = []
        filter_flag = '3'
        for strand in selected_scheme.stand_ids.all():
            for sub_strand_obj in strand.sub_strand_ids.all():
                sub_strand_temp_dict = {}
                if scheme_mapped_flag:
                    sub_strand_temp_dict['sub_strand_name'] = subject_substrands.objects.get(
                        id=sub_strand_obj.sub_strand_name).substrands_name
                else:
                    sub_strand_temp_dict['sub_strand_name'] = sub_strand_obj.sub_strand_name

                sub_strand_temp_dict['id'] = sub_strand_obj.id
                sub_strand_list.append(sub_strand_temp_dict)

        for topic_rec_obj in sub_strand_data.topic_ids.all():
            topic_list.append(topic_rec_obj)
        grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)
        topic_recs = topic.objects.get(id=topic_form)
        for subtopics in topic_recs.sub_topic_ids.all():
            subtopics_list.append(subtopics.id)
        for data in academic_frequency_rec.student_mark_ids.all():
            for rec in data.subtopic_ids.all():
                if rec.subtopic.id not in list:
                    if rec.subtopic.sub_topic_id in subtopics_list:
                        list.append(rec.subtopic.id)
                        subtopic_recs.append(rec.subtopic)
        total_mark = 0
        for subtopic_rec in subtopic_recs:
            total_mark = int(subtopic_rec.subtopic_mark) + total_mark
        if academic_frequency_rec.is_submitted is True:
            color_code = '#1ce034'
        else:
            color_code = '#ffa012'
        assessment_conf_names = assessment_configration_master.objects.all()
        conf_list = ['Ab', 'NA']
        for conf in assessment_conf_names:
            conf_list.append(str(conf.short_name))
        entered_value = '0-9.'
        for c in conf_list: entered_value = str(c) + str(entered_value)
        iterator_substrand = make_incrementor(0)
        form_vals = {
            'entered_value': entered_value,
            'conf_list': conf_list,
            'total_mark': total_mark,
            'Selected_Section_Id': academic_frequency_rec.academic_mapping.section_name.id,
            'selected_section_name': academic_frequency_rec.academic_mapping.section_name,
            'selected_section': academic_frequency_rec.academic_mapping.section_name,
            'Selected_Class_Id': academic_frequency_rec.academic_mapping.class_name.id,
            'selected_class_name': academic_frequency_rec.academic_mapping.class_name.class_name,
            'Selected_Year_Id': academic_frequency_rec.academic_mapping.year_name.id,
            'selected_year_name': academic_frequency_rec.academic_mapping.year_name.year_name,
            'selected_year': academic_frequency_rec.academic_mapping.year_name,
            'frequency_recs': academic_frequency_rec.frequency,
            'frequency_data': academic_frequency_rec.frequency.id,
            'scheme_recs': academic_frequency_rec.scheme_mapping.scheme_name,
            'subtopics_recs': subtopics_recs,
            'academic_frequency_rec': academic_frequency_rec,
            'subtopic_data': subtopic_recs,
            'color_code': color_code,
            'grade_colors': grade_colors,
            'subject_recs': academic_frequency_rec.scheme_mapping.subject,
            'skill_excel_form': skill_excel_form,
            'sub_strand_list': sub_strand_list,
            'filter_flag': filter_flag,
            'subtopics_list': subtopics_list,
            'topic_rec': topic_form_data,
            'sub_strand_rec': sub_strand_dict,
            'topic_list': topic_list,
            'sub_strand_dict_list': sub_strand_dict_list,
            'topic_dict_list': topic_dict_list,
            'conf_dict_list': assessment_conf_names,
            'subtopic_recs_len': len(subtopics_recs),
            'rec_id':rec_id,
            'iterator_substrand': iterator_substrand,
            'edit_flag':True,
        }
    except Exception as e:
        return redirect('/school/update_entered_scheme_mark/' + str(rec_id))
    return render(request, "edit_scheme_Entered_mark.html", form_vals)


@user_login_required
def SaveSkillEnterMark(request):
    if request.method == 'POST':
        data = json.loads(request.POST.get('data'))
        year_id = request.POST.get('id_academic_year')
        class_id = request.POST.get('id_class_name')
        section_id = request.POST.get('id_section_name')
        subject_id= request.POST.get('id_subject_name')
        frequency_id= request.POST.get('id_frequency')
        outoff_mark= request.POST.get('outoff')

        academic_mapping = academic_class_section_mapping.objects.get(year_name=year_id, class_name=class_id, section_name=section_id)
        scheme_rec = scheme_mapping_details.objects.get(year_id=year_id, class_obj_id=class_id,subject_id=subject_id, is_archived=False)
        mark_recs = scheme_academic_frequency_mapping.objects.filter(academic_mapping=academic_mapping,scheme_mapping_id=scheme_rec.id,frequency_id=frequency_id)
        if mark_recs:
            try:
                academic_mapping = academic_class_section_mapping.objects.get(year_name=year_id, class_name=class_id,section_name=section_id)
                data = json.loads(request.POST.get('data'))
                seheme_mapping_rec = scheme_mapping_details.objects.get(year_id=year_id, class_obj_id=class_id,subject_id=subject_id, is_archived=False)
                academic_frequency_mapping_obj = scheme_academic_frequency_mapping.objects.filter(academic_mapping=academic_mapping, scheme_mapping_id=seheme_mapping_rec.id,frequency_id=frequency_id)[0]
                ac_id = academic_frequency_mapping_obj.id
                dic = {}
                for count, rec in enumerate(data[0]):
                    obj = None
                    temp = rec.split('-')
                    scheme_em_subtopic_mark.objects.filter(id=temp[0]).update(subtopic_mark=data[0][rec])
                    id = temp[0]
                    dic[temp[1]] = id

                data_count = 2
                all_recs = academic_frequency_mapping_obj.student_mark_ids.all()
                for recs in all_recs:

                    recs.total_mark = data[data_count]['totalMarks']
                    # recs.out_off_mark = data[data_count]['maxMarks']
                    recs.save()
                    for count, subtopic in enumerate(recs.subtopic_ids.all()):
                        count = count + 1
                        subtopic.mark = data[data_count]['subtopicMark_' + str(count)]
                        subtopic.save()
                    data_count += 1


                scheme_academic_frequency_mapping.objects.filter(scheme_mapping_id=seheme_mapping_rec.id).update(
                    updated_on=str(datetime.now()))
                messages.success(request, "Record Updated Successfully.")
                return JsonResponse
            except:
                messages.warning(request, "Some Error Occurred. Please Try Again.")
                return JsonResponse


        seheme_mapping_rec = scheme_mapping_details.objects.get(year_id=year_id, class_obj_id=class_id,subject_id=subject_id, is_archived = False)
        scheme_id = seheme_mapping_rec.scheme_name.id

        scheme_rec = scheme.objects.get(id=scheme_id)
        subtopic_list = []
        for starand in scheme_rec.stand_ids.all():
            for sub_strand_recs in starand.sub_strand_ids.all():
                if sub_strand_recs:
                    for topic_recs in sub_strand_recs.topic_ids.all():
                        if topic_recs:
                            for subtopics in topic_recs.sub_topic_ids.all():
                                subtopic_list.append(subtopics)

        academic_frequency_mapping_obj = scheme_academic_frequency_mapping.objects.create(academic_mapping= academic_mapping,scheme_mapping_id = seheme_mapping_rec.id, frequency_id=frequency_id,outoff_mark=outoff_mark)

        dic = {}
        for count , rec in enumerate(data[0]):
            obj = None
            temp = rec.split('-')
            obj = scheme_em_subtopic_mark.objects.create(sub_topic_id = temp[0], subtopic_mark = data[0][rec])
            id = obj.id
            dic[temp[0]]=id

        for track, rec in  enumerate(data):
            if track == 0 or track == 1:
                pass
            else:
                student_mark_mapping_obj = scheme_student_mark_mapping.objects.create(student_id = rec['studentId'], total_mark = rec['totalMarks'], out_off_mark = rec['out_off_mark'])
                academic_frequency_mapping_obj.student_mark_ids.add(student_mark_mapping_obj.id)

                for count , subtopic in enumerate(subtopic_list):
                    count = count+1
                    st_id = dic[rec['sub_topic_id_' + str(count)]]
                    scheme_subtopic_obj = scheme_subtopic_mark.objects.create(mark = rec['subtopicMark_' + str(count)], subtopic_id = int(st_id))
                    student_mark_mapping_obj.subtopic_ids.add(scheme_subtopic_obj.id)

    return JsonResponse


@user_login_required
def UpdateEnteredSchemeMark(request, rec_id):

    try:
        academic_frequency_rec = scheme_academic_frequency_mapping.objects.get(id = rec_id)
        skill_excel_form = SkillExcelForm()
        scheme_mapped_flag = False
        round_rec = assessment_round_master.objects.all()[0]
        round_select = round_rec.select
        round_decimal_place = round_rec.decimal_place
        selected_scheme = scheme.objects.get(id=academic_frequency_rec.scheme_mapping.scheme_name.id)
        na_flag = assessment_configration_master.objects.filter(short_name='NA').exists()

        if selected_scheme.subject_scheme_name:
            scheme_mapped_flag = True

        subtopics_recs = []
        subtopic_recs =[]
        list = []
        sub_strand_list = []
        filter_flag = True

        sub_strand_dict_list = []
        sub_strand_dict_id = []
        topic_dict_id = []
        topic_dict_list = []

        for strand in selected_scheme.stand_ids.all():
            for sub_strand in strand.sub_strand_ids.all():
                sub_strand_dict = {}
                sub_strand_temp_dict = {}
                if scheme_mapped_flag:
                    sub_strand_temp_dict['sub_strand_name'] = subject_substrands.objects.get(id = sub_strand.sub_strand_name).substrands_name
                else:
                    sub_strand_temp_dict['sub_strand_name'] = sub_strand.sub_strand_name

                sub_strand_temp_dict['id'] = sub_strand.id
                sub_strand_list.append(sub_strand_temp_dict)
                if sub_strand.id not in sub_strand_dict_id:
                    sub_strand_dict_id.append(sub_strand.id)
                    if scheme_mapped_flag:
                        sub_strand_dict['sub_strand_name'] = subject_substrands.objects.get(id = sub_strand.sub_strand_name).substrands_name
                        sub_strand_dict['topic_count'] = 0
                    else:
                        sub_strand_dict['sub_strand_name'] = sub_strand.sub_strand_name
                        sub_strand_dict['topic_count'] = 0

                for topic in sub_strand.topic_ids.all():
                    topic_dict = {}
                    if topic.id not in topic_dict_id:
                        topic_dict['topic_name'] = topic.topic_name
                        topic_dict['subtopic_count'] = topic.sub_topic_ids.all().count()
                        topic_dict_id.append(topic.id)
                        topic_dict_list.append(topic_dict)
                        sub_strand_dict['topic_count'] += topic.sub_topic_ids.all().count()
                sub_strand_dict_list.append(sub_strand_dict)

        grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)

        for data in academic_frequency_rec.student_mark_ids.all():
            for rec in data.subtopic_ids.all():
                if rec.subtopic.id not in list:
                    list.append(rec.subtopic.id)
                    subtopic_recs.append(rec.subtopic)

        total_mark = 0
        for subtopic_rec in subtopic_recs:
            total_mark = int(subtopic_rec.subtopic_mark) + total_mark

        if academic_frequency_rec.is_submitted is True:
            color_code = '#1ce034'
        else:
            color_code = '#ffa012'

        assessment_conf_names = assessment_configration_master.objects.all()
        conf_list = ['Ab', 'NA']
        for conf in assessment_conf_names:
            conf_list.append(str(conf.short_name))

        entered_value = '0-9.'
        for c in conf_list: entered_value = str(c) + str(entered_value)

        form_vals = {
            'entered_value': entered_value,
            'conf_list': conf_list,
            'total_mark': total_mark,
            'Selected_Section_Id': academic_frequency_rec.academic_mapping.section_name.id,
            'selected_section_name': academic_frequency_rec.academic_mapping.section_name,
            'selected_section': academic_frequency_rec.academic_mapping.section_name,

            'Selected_Class_Id': academic_frequency_rec.academic_mapping.class_name.id,
            'selected_class_name': academic_frequency_rec.academic_mapping.class_name.class_name,

            'Selected_Year_Id': academic_frequency_rec.academic_mapping.year_name.id,
            'selected_year_name': academic_frequency_rec.academic_mapping.year_name.year_name,
            'selected_year': academic_frequency_rec.academic_mapping.year_name,

            'frequency_recs': academic_frequency_rec.frequency,
            'frequency_data':  academic_frequency_rec.frequency.id,
            'scheme_recs': academic_frequency_rec.scheme_mapping.scheme_name,
            'subtopics_recs': subtopics_recs,

            'academic_frequency_rec': academic_frequency_rec,
            'subtopic_data': subtopic_recs,
            'color_code':color_code,
            'grade_colors':grade_colors,
            'subject_recs': academic_frequency_rec.scheme_mapping.subject,

            'skill_excel_form':skill_excel_form,
            'sub_strand_list':sub_strand_list,
            'filter_flag':filter_flag,
            'sub_strand_dict_list':sub_strand_dict_list,
            'topic_dict_list':topic_dict_list,

            'conf_dict_list': assessment_conf_names,
            'subtopic_recs_len': len(subtopics_recs),
            'na_flag':na_flag,
            'rec_id':rec_id,
            'edit_flag':False,
            'round_select':round_select,
            'round_decimal_place':round_decimal_place
        }
        return render(request, "edit_scheme_Entered_mark.html",form_vals)

    except:
        messages.warning(request, 'Some Error occurred.')
    return redirect('/school/entered_scheme_mark_list/')


@user_login_required
def UpdateSchemeMark(request):

    if request.method == 'POST':
        year_id = request.POST.get('acd_year')
        class_id = request.POST.get('class')
        section_id = request.POST.get('section')
        scheme_id = request.POST.get('scheme')
        frequency_id= request.POST.get('frequency')
        subject_id = request.POST.get('sub')
        sub_list_check = json.loads(request.POST.get('sub_list_check'))

        try:
            academic_mapping = academic_class_section_mapping.objects.get(year_name=year_id, class_name=class_id, section_name=section_id)
            data = json.loads(request.POST.get('data'))

            seheme_mapping_rec = scheme_mapping_details.objects.get(year_id=year_id, class_obj_id=class_id,
                                                                    subject_id=subject_id,is_archived = False)
            academic_frequency_mapping_obj = scheme_academic_frequency_mapping.objects.filter(
                academic_mapping=academic_mapping, scheme_mapping_id=seheme_mapping_rec.id, frequency_id=frequency_id)[0]

            ac_id = academic_frequency_mapping_obj.id

            for key, value in data[0].iteritems():
                scheme_em_subtopic_mark.objects.filter(id=int(key.split('-')[0])).update(subtopic_mark=str(value))

            for rec in data:
                if 'studentId' not  in rec:
                    pass
                else:
                    for i in range(1, ((len(rec) - 3) / 3)+1):
                        try:
                            scheme_subtopic_mark.objects.filter(id=rec['rec_id_' + str(i)]).update(mark=rec['subtopicMark_' + str(i)])
                        except Exception as e:
                            continue

            messages.success(request, "Record Updated Successfully.")
            return redirect('/school/update_entered_scheme_mark/' + str(ac_id))
        except Exception as e:
            messages.warning(request, "Some Error Occurred. Please Try Again. : ",str(e))
        return redirect('/school/entered_scheme_mark_list/')

@user_login_required
def SubmitSchemeMark(request):
    rec_id = request.POST.get('rec_id')
    max_mrk_val = request.POST.get('max_mrk_val')

    try:


        acd_freq_rec = scheme_academic_frequency_mapping.objects.get(id=rec_id)
        greading_recs = acd_freq_rec.scheme_mapping.grade_name.grade_name_rel.all()

        grade_arr = []
        for grade_rec in greading_recs:  # getting grades and creating its dict for the backend grade
            grade_dict = {
                'mar_less': int(grade_rec.marks_greater),
                'mar_greater': int(grade_rec.marks_less),
                'grade': grade_rec.grades,
            }
            grade_arr.append(grade_dict)

        for rec in acd_freq_rec.student_mark_ids.all():
            weightage = 0

            if rec.out_off_mark != None and rec.out_off_mark != '':
                if float(rec.out_off_mark) != 0:
                # if int(max_mrk_val) !=0:
                    weightage = ((int(float(rec.total_mark)) * 100) / int(float(rec.out_off_mark)))

            for grd_rec in grade_arr:
                if grd_rec['mar_less'] <= weightage <= grd_rec['mar_greater']:   #getting grade of the specific weightage
                    grade = grd_rec['grade']
                    scheme_student_mark_mapping.objects.filter(id=rec.id).update(grade_mark=grade)
                    break

        scheme_academic_frequency_mapping.objects.filter(id=rec_id).update(is_submitted = True, submitted_by_id = request.user.id, updated_on=str(datetime.now()))
        messages.success(request, "Record submitted successfully..")
        return HttpResponse(
            json.dumps({'error': 'Record submitted successfully..'}), content_type="application/json")
    except:
        messages.warning(request, "Record not submitted. Some error occoured.")
    return HttpResponse(
            json.dumps({'error': 'Record not submitted. Some error occoured'}), content_type="application/json")


@user_login_required
def ViewSubjectScheme(request):
    finalDict = []
    subject_name = request.POST.get('subject_name', None)
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name')

    ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name, year_name_id=year_name, section_name_id=section_name)

    scheme_mapping_rec = scheme_mapping_details.objects.filter(academic_class_section_mapping_id=ac_recs.id,subject_id=subject_name,is_archived = False)
    # print "-->"
    ab = {}
    for rec in scheme_mapping_rec:
        if not any(d['id'] == rec.scheme_name_id for d in finalDict):
            ab={'id':rec.scheme_name_id, 'value': rec.scheme_name.scheme_name}
            finalDict.append(ab)
    return JsonResponse(finalDict,safe=False)

@user_login_required
def ViewSkillSubjectFrequency(request):
    finalDict = []
    subject_name = request.POST.get('subject_name', None)
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name')

    ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name, year_name_id=year_name,
                                                         section_name_id=section_name)

    # scheme_mapping_rec = scheme_mapping_details.objects.filter(year_id=year_name, class_obj_id=class_name, subject_id=subject_name,
    #                                       is_archived=False)[0]

    fr_map_recs = scheme_academic_frequency_mapping.objects.filter(academic_mapping=ac_recs,
                                                                   scheme_mapping__subject_id=subject_name)
    freq_rec = []
    for acd_rec_freq in fr_map_recs:
        for freq in acd_rec_freq.scheme_mapping.scheme_name.scheme_frequency_ids.all():
            freq_rec.append(freq.frequency_name)

    ab = {}
    for rec in freq_rec:
        if not any(d['id'] == rec.id for d in finalDict):
            ab = {'id': rec.id, 'value': rec.frequency_name}
            finalDict.append(ab)

    return JsonResponse(finalDict, safe=False)

@user_login_required
def ViewExamSubjectFrequency(request):
    finalDict = []
    subject_name = request.POST.get('subject_name', None)
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name',None)

    ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name, year_name_id=year_name,section_name_id=section_name)
    exam_scheme_academic_recs = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping=ac_recs)
    frequencies_rec = []
    for acd_obj in exam_scheme_academic_recs:
        if acd_obj.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject.id == int(subject_name):
            exam_details_frequency_rec = exam_details_frequency.objects.filter(
                exam_details_name=acd_obj.scheme_mapping.exam_name)
            for frequencies in exam_details_frequency_rec:
                ab = {}
                ab['id'] = frequencies.frequency_details_name.id
                ab['value'] = frequencies.frequency_details_name.frequency_name
                if ab not in frequencies_rec:
                    frequencies_rec.append(ab)
    return JsonResponse(frequencies_rec, safe=False)

@user_login_required
def ViewRubricSubjectFrequency(request):
    finalDict = []
    subject_name = request.POST.get('subject_name', None)
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name')

    ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name, year_name_id=year_name, section_name_id=section_name)

    rubric_map_recs = rubric_academic_frequency_mapping.objects.filter(academic_mapping=ac_recs,scheme_mapping__subject_id=subject_name)

    ab = {}
    for rub_rec in rubric_map_recs:
        for rec in rub_rec.scheme_mapping.rubric_frequency_ids.all():
            if not any(d['id'] == rec.frequency_name.id for d in finalDict):
                ab = {'id': rec.frequency_name.id, 'value': rec.frequency_name.frequency_name}
                finalDict.append(ab)

    return JsonResponse(finalDict,safe=False)

@user_login_required
def ViewSubjectSchemeFrequency(request):
    finalDict = []
    subject_name = request.POST.get('subject_name', None)
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name', None)
    scheme_name = request.POST.get('id_scheme', None)

    ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name, year_name_id=year_name, section_name_id=section_name)

    scheme_mapping_rec = scheme_mapping_details.objects.get(academic_class_section_mapping_id=ac_recs.id, subject_id=subject_name, scheme_name=scheme_name,is_archived = False)
    fr_map_recs = scheme_academic_frequency_mapping.objects.filter(scheme_mapping_id=scheme_mapping_rec.id)

    ab = {}
    for rec in fr_map_recs:
        if not any(d['id'] == rec.frequency_id for d in finalDict):
            ab={'id':rec.frequency_id, 'value': rec.frequency.frequency_name}
            finalDict.append(ab)
    return JsonResponse(finalDict,safe=False)



@user_login_required
@user_permission_required('assessment.can_view_view_skill_approval_list', '/school/home/')
def ViewSchemeAprovalList(request):
    # print  "In view_aproval_list function"
    frequency_recs = frequency_details.objects.all()
    all_academicyr_recs = academic_year.objects.filter(is_active=True,current_academic_year=1)
    year_class_section_object = academic_class_section_mapping.objects.all()
    mapped_class = []
    mapped_section = []

    if year_class_section_object:
        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id


            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                mapped_class.append(mapped_object.class_name)
                mapped_section.append(mapped_object.section_name)

            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=selected_year_id).values_list('id')

            # scheme_recs = scheme_mapping_details.objects.filter(year=current_academic_year).values_list('id')
            scheme_mark_recs = scheme_academic_frequency_mapping.objects.filter(Q(academic_mapping__in = ac_recs, is_archived= False) & Q( is_submitted = True, is_apprpved=False))

            rec_list =[]
            for scheme_mark_rec in scheme_mark_recs:
                supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(academic_class_section_mapping = scheme_mark_rec.academic_mapping, subject = scheme_mark_rec.scheme_mapping.subject)
                teacher_recs = teacher_subject_mapping.objects.filter(academic_class_section_mapping = scheme_mark_rec.academic_mapping, subject = scheme_mark_rec.scheme_mapping.subject)
                mapped_data = {
                    'supervisor_recs':supervisor_recs,
                    'teacher_recs':teacher_recs,
                    'scheme_mark_rec': scheme_mark_rec
                }
                rec_list.append(mapped_data)
            return render(request, 'scheme_aproval_list.html', {'scheme_mark_recs':rec_list,"year_list": all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,'frequency_recs':frequency_recs,'ClassDetails': list(set(mapped_class)),'SectionDetails': list(set(mapped_section))})

        except academic_year.DoesNotExist:
            messages.warning(request, "Academic Year Does not Exist.")
            return render(request, 'skill_tracker_sub_tile.html')
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
    return render(request, "skill_tracker_sub_tile.html")
# academic_supervisor_class_section_mapping.objects.filter(academic_class_section_mapping = scheme_mark_rec[0].scheme_mapping.academic_class_section_mapping, subject = scheme_mark_rec[0].scheme_mapping.subject)[0]

@user_login_required
def ViewFilteredSchemeAprovalList(request):
    all_academicyr_recs = academic_year.objects.filter(is_active=True,current_academic_year=1)

    if request.POST:
        request.session['username'] = request.POST
        year_name = request.POST.get('year_name', None)
        class_name = request.POST.get('class_name', None)
        section_name = request.POST.get('section_name', None)
        subject_name = request.POST.get('subject_name', None)
        frequency_name = request.POST.get('frequency_name', None)
    else:
        year_name = request.session['username'].get('year_name', None)
        class_name = request.session['username'].get('class_name', None)
        section_name = request.session['username'].get('section_name', None)
        subject_name = request.session['username'].get('subject_name', None)
        frequency_name = request.session['username'].get('frequency_name', None)

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        class_rec = class_details.objects.get(id=class_name)
        subjects_rec = subjects.objects.get(id=subject_name)
        section_rec = sections.objects.get(id=section_name)
        frequency_rec = frequency_details.objects.get(id=frequency_name)

        mapped_class = []
        mapped_section = []

        for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
            mapped_class.append(mapped_object.class_name)

        for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year,class_name=class_rec):
            mapped_section.append(mapped_object.section_name)


        try:
            ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name, year_name_id=year_name,
                                                                 section_name_id=section_name)
            get_scheme_name = scheme_mapping_details.objects.get(year_id=ac_recs.year_name.id,
                                                                 class_obj_id=ac_recs.class_name.id,
                                                                 subject=subject_name, is_archived=False)
            get_scheme_id = get_scheme_name.scheme_name.id
            scheme_rec = scheme.objects.get(id=get_scheme_id)
        except:
            messages.warning(request, "Mapping Not Found for Selected Class And Section")
            return redirect('/school/view_scheme_aproval_list/')

        seheme_mapping_rec = scheme_mapping_details.objects.get(year_id=ac_recs.year_name.id,
                                                                class_obj_id=ac_recs.class_name.id,
                                                                subject=subject_name, scheme_name=get_scheme_id,
                                                                is_archived=False)

        scheme_mark_recs = scheme_academic_frequency_mapping.objects.filter(academic_mapping=ac_recs,
                                                                            scheme_mapping_id__subject_id=subject_name,
                                                                            frequency_id=frequency_name, is_submitted=True,
                                                                            is_apprpved=False, is_archived=False)



        rec_list = []
        for scheme_mark_rec in scheme_mark_recs:
            supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
                academic_class_section_mapping=ac_recs,
                subject=scheme_mark_rec.scheme_mapping.subject)
            teacher_recs = teacher_subject_mapping.objects.filter(
                academic_class_section_mapping=ac_recs,
                subject=scheme_mark_rec.scheme_mapping.subject)
            mapped_data = {
                'supervisor_recs': supervisor_recs,
                'teacher_recs': teacher_recs,
                'scheme_mark_rec': scheme_mark_rec
            }
            rec_list.append(mapped_data)
        #
        fr_map_recs = scheme_academic_frequency_mapping.objects.filter(academic_mapping=ac_recs,
                                                                       scheme_mapping__subject_id=subject_name)
        frequency_recs = []
        for acd_rec_freq in fr_map_recs:
            for freq in acd_rec_freq.scheme_mapping.scheme_name.scheme_frequency_ids.all():
                if freq.frequency_name not in frequency_recs:
                    frequency_recs.append(freq.frequency_name)

        scheme_mapped_subject_list = []
        scheme_mark_recs_subject = scheme_academic_frequency_mapping.objects.filter(
            academic_mapping_id=ac_recs.id)
        for scheme_mark_rec in scheme_mark_recs_subject:
            scheme_mapped_subject_list.append(scheme_mark_rec.scheme_mapping.subject)
        scheme_mapped_subject_list = set(scheme_mapped_subject_list)

        form_vals = {
            'frequency_rec': frequency_rec,
            'scheme_rec': scheme_rec,
            'section_rec': section_rec,
            "year_list": all_academicyr_recs,
            'selected_year_name': current_academic_year,
            'Selected_Year_Id': selected_year_id,
            'subjects_rec': subjects_rec,
            'scheme_mark_recs': rec_list,
            'frequency_recs': frequency_recs,
            'class_rec': class_rec,
            'ClassDetails': list(set(mapped_class)),
            'SectionDetails': list(set(mapped_section)),
            'subject_recs': scheme_mapped_subject_list
        }

        return render(request, "scheme_aproval_list.html", form_vals)
    except:
        messages.warning(request, "Some error occourred. Please try again.")
    return redirect('/school/view_scheme_aproval_list/')



@user_login_required
def FilterExamAprovalList(request):
    all_academicyr_recs = academic_year.objects.all()
    year_name = request.POST.get('year_name', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name', None)
    subject_name = request.POST.get('subject_name', None)
    frequency_name = request.POST.get('frequency_name', None)
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        class_rec = class_details.objects.get(id=class_name)
        subjects_rec = subjects.objects.get(id=subject_name)
        section_rec = sections.objects.get(id=section_name)
        frequency_rec = frequency_details.objects.get(id=frequency_name)
        mapped_class = []
        mapped_section = []

        for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
            mapped_class.append(mapped_object.class_name)
        for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year,class_name=class_rec):
            mapped_section.append(mapped_object.section_name)
        ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name, year_name_id=year_name,section_name_id=section_name)
        exam_scheme_academic_recs = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping=ac_recs)
        frequencies_rec = []
        for acd_obj in exam_scheme_academic_recs:
            if acd_obj.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject.id == int(subject_name):
                exam_details_frequency_rec = exam_details_frequency.objects.filter(exam_details_name=acd_obj.scheme_mapping.exam_name)
                for frequencies in exam_details_frequency_rec:
                    frequencies_rec.append(frequencies.frequency_details_name)

        exam_mapped_subject_list = []
        exam_mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping_id=ac_recs.id)
        for exam_mark_rec in exam_mark_recs:
            exam_sub_mark_recs = exam_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()
            for exam_sub_mark_rec in exam_sub_mark_recs:
                exam_mapped_subject_list.append(exam_sub_mark_rec.subject)
        exam_mapped_subject_list = set(exam_mapped_subject_list)
    except:
        messages.warning(request, "Mapping Not Found for Selected Class And Section")
        return redirect('/school/view_exam_scheme_aproval_list/')

    exam_name=exam_scheme_acd_mapping_details.objects.filter(year_id=ac_recs.year_name.id,class_obj=class_rec,subject=subjects_rec).values_list('exam_name_mapping_ids', flat=True)
    scheme_mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping_id = ac_recs.id,
                                                                             frequency_id=frequency_name,scheme_mapping_id__in=exam_name,
                                                                             is_submitted = True, is_apprpved=False ,is_archived=False)
    rec_list = []
    for scheme_mark_rec in scheme_mark_recs:
        supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
            academic_class_section_mapping=scheme_mark_rec.academic_mapping,
            subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)
        teacher_recs = teacher_subject_mapping.objects.filter(
            academic_class_section_mapping=scheme_mark_rec.academic_mapping,
            subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)
        mapped_data = {
            'supervisor_recs': supervisor_recs,
            'teacher_recs': teacher_recs,
            'scheme_mark_rec': scheme_mark_rec
        }
        rec_list.append(mapped_data)
    form_vals = {
        'frequency_rec':frequency_rec,
        'class_rec': class_rec,
        'section_rec': section_rec,
        "year_list": all_academicyr_recs,
        'selected_year_name': current_academic_year,
        'Selected_Year_Id': selected_year_id,
        'subjects_rec':subjects_rec,
        'scheme_mark_recs':rec_list,
        'ClassDetails': list(set(mapped_class)),
        'SectionDetails': list(set(mapped_section)),
        'frequency_recs':set(frequencies_rec),
        'subject_recs':exam_mapped_subject_list
    }
    return render(request, "exam_scheme_approval_list.html", form_vals)

@user_login_required
def AproveRejectScheme(request):
    current_academic_year = academic_year.objects.get(current_academic_year=1)

    # print "In AproveRejectScheme function"
    selected_recs = request.POST.getlist('check')  # Getting Ids  of selected records
    task = request.POST.get('task', None)

    if task =='approve':
        for rec in selected_recs:
            scheme_academic_frequency_mapping.objects.filter(id = rec).update(is_apprpved=True, is_rejected=False, approved_by_id = request.user.id)
            messages.success(request, "Selected Records Has Been Approved.")
            # print "a"
    elif task == 'reject':
        for rec in selected_recs:
            scheme_academic_frequency_mapping.objects.filter(id=rec).update(is_submitted=False, is_rejected=True, approved_by_id = request.user.id)
            messages.warning(request, "Approval of Selected Records Has Been Rejected.")
            # print 'r'
    else:
        messages.warning(request, "Some Error Occurred.")

    return redirect('/school/view_scheme_aproval_list/')

def  load_status(mapp_table=None, freq_rec_table = None,year_name=None,user_name=None,class_name=None,subject_teacher=None, status_name=None):
    if class_name == None:
        class_name = ''
    if subject_teacher == None:
        subject_teacher = ''
    if status_name == None:
        status_name = ''

    def load_status_fun(status_name):
        # if status_name == '1':
        #     ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name)
        #     scheme_ids = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs).values_list(
        #         'id')
        #
        #     scheme_ay_recs = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs)
        #     scheme_mark_ids = freq_rec_table.objects.filter(
        #         scheme_mapping__in=scheme_ay_recs).values_list('scheme_mapping')
        #
        #     diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))
        #
        #     for diff_rec in diff_recs:
        #         diff_list.append(diff_rec[0])
        #
        #     scheme_mapping_recs = mapp_table.objects.filter(id__in=diff_list)
        #     scheme_mark_recs = []
        #
        #     return scheme_mapping_recs, scheme_mark_recs

        if status_name == '2':
            if user_name.is_system_admin():
                ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,
                                                                        )
                scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs, is_submitted=False,
                                                                 is_archived=False)

            elif user_name.is_subject_teacher():
                ac_recs = []
                for mapped_object in academic_class_section_mapping.objects.filter(year_name_id=year_name,
                                                                                   ):
                    for obj in teacher_subject_mapping.objects.filter(academic_class_section_mapping=mapped_object,
                                                                      staff_id=user_name):
                        if obj:
                            ac_recs.append(mapped_object)
                scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs, is_submitted=False,
                                                                 is_archived=False, submitted_by=user_name)
            else:
                ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,

                                                                        staff__id=user_name.id)


            # scheme_ids = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')
            #
            # scheme_ay_recs = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs)
                scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs, is_submitted=False,is_archived= False,submitted_by=user_name)

            # for diff_rec in scheme_mark_ids:
            #     diff_list.append(diff_rec[0])

            scheme_mapping_recs = []  #scheme_mapping_details.objects.filter(id__in=diff_list)
            # scheme_mark_recs = freq_rec_table.objects.filter(scheme_mapping__in=diff_list)

            return scheme_mapping_recs, scheme_mark_recs

        if status_name == '3':
            if user_name.is_system_admin():
                ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,)
                scheme_mark_recs = freq_rec_table.objects.filter(Q(academic_mapping__in=ac_recs, is_archived=False),
                                                                 (Q(is_submitted=True, is_apprpved=False)) or (
                                                                     Q(is_rejected=True, is_apprpved=False)))


            elif user_name.is_subject_teacher():
                ac_recs = []
                for mapped_object in academic_class_section_mapping.objects.filter(year_name_id=year_name,
                                                                                   ):
                    for obj in teacher_subject_mapping.objects.filter(academic_class_section_mapping=mapped_object,
                                                                      staff_id=user_name):
                        if obj:
                            ac_recs.append(mapped_object)
                scheme_mark_recs = freq_rec_table.objects.filter(Q(academic_mapping__in=ac_recs, is_archived=False),
                                                                 (Q(is_submitted=True, is_apprpved=False)) or (
                                                                     Q(is_rejected=True, is_apprpved=False))(
                                                                     submitted_by=user_name))

            else:
                ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,

                                                                        staff__id=user_name.id)

            # scheme_ids = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')

            # scheme_ay_recs = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs)
                scheme_mark_recs = freq_rec_table.objects.filter(Q(academic_mapping__in=ac_recs, is_archived=False),
                                                             (Q(is_submitted=True, is_apprpved=False)) or (
                                                                 Q(is_rejected=True, is_apprpved=False))(
                                                                 submitted_by=user_name))

            # scheme_mark_recs = freq_rec_table.objects.filter(Q(academic_mapping__in=ac_recs,is_archived= False),
            # (Q(is_submitted = True, is_apprpved = False)) or (Q(is_rejected= True, is_apprpved = False))(submitted_by=user_name))

            # for diff_rec in scheme_mark_ids:
            #     diff_list.append(diff_rec[0])

            scheme_mapping_recs = []  #mapp_table.objects.filter(id__in=diff_list)
            # scheme_mark_recs = freq_rec_table.objects.filter(scheme_mapping__in=diff_list)

            return scheme_mapping_recs, scheme_mark_recs


        if status_name == '4':

            if user_name.is_system_admin():
                ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,
                                                                        )
                scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs, is_rejected=False,
                                                                 is_apprpved=True, is_archived=False)

            elif user_name.is_subject_teacher():
                ac_recs = []
                for mapped_object in academic_class_section_mapping.objects.filter(year_name_id=year_name,
                                                                                   ):
                    for obj in teacher_subject_mapping.objects.filter(academic_class_section_mapping=mapped_object,
                                                                      staff_id=user_name):
                        if obj:
                            ac_recs.append(mapped_object)
                scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs, is_rejected=False,
                                                                 is_apprpved=True, is_archived=False,
                                                                 submitted_by=user_name)

            else:
                ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,

                                                                        staff__id=user_name.id)

            # scheme_ids = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')
            #
            # scheme_ay_recs = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs)
                scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs,is_rejected = False, is_apprpved=True,is_archived= False,submitted_by=user_name)


            # for diff_rec in scheme_mark_ids:
            #     diff_list.append(diff_rec[0])

            scheme_mapping_recs = []  #mapp_table.objects.filter(id__in=diff_list)
            # scheme_mark_recs = freq_rec_table.objects.filter(scheme_mapping__in = diff_list)

            return scheme_mapping_recs, scheme_mark_recs

    def load_class_status_fun(class_name,status_name):

        if status_name == '2':
            if user_name.is_system_admin():
                ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,
                                                                        class_name_id=class_name)
                scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs, is_submitted=False,
                                                                 is_archived=False)

            elif user_name.is_subject_teacher():
                ac_recs = []
                for mapped_object in academic_class_section_mapping.objects.filter(year_name_id=year_name,
                                                                                   class_name_id=class_name):
                    for obj in teacher_subject_mapping.objects.filter(academic_class_section_mapping=mapped_object,
                                                                      staff_id=user_name):
                        if obj:
                            ac_recs.append(mapped_object)

                scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs, is_submitted=False,
                                                                 is_archived=False, submitted_by=user_name)
            else:
                ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,
                                                                        class_name_id=class_name,
                                                                        staff__id=user_name.id)

            #ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name, class_name_id=class_name)
            # scheme_ids = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')
            #
            # scheme_ay_recs = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs)
                scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs, is_submitted=False,is_archived= False,submitted_by=user_name)

            # for diff_rec in scheme_mark_ids:
            #     diff_list.append(diff_rec[0])

            scheme_mapping_recs = []  # scheme_mapping_details.objects.filter(id__in=diff_list)
            # scheme_mark_recs = freq_rec_table.objects.filter(scheme_mapping__in=diff_list)

            return scheme_mapping_recs, scheme_mark_recs

        if status_name == '3':
            if user_name.is_system_admin():
                ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,
                                                                        class_name_id=class_name)
                scheme_mark_recs = freq_rec_table.objects.filter(Q(academic_mapping__in=ac_recs, is_archived=False),
                                                                 (Q(is_submitted=True, is_apprpved=False)) or (
                                                                     Q(is_rejected=True, is_apprpved=False))
                                                                     )


            elif user_name.is_subject_teacher():
                ac_recs = []
                for mapped_object in academic_class_section_mapping.objects.filter(year_name_id=year_name,
                                                                                   class_name_id=class_name):
                    for obj in teacher_subject_mapping.objects.filter(academic_class_section_mapping=mapped_object,
                                                                      staff_id=user_name):
                        if obj:
                            ac_recs.append(mapped_object)
                scheme_mark_recs = freq_rec_table.objects.filter(Q(academic_mapping__in=ac_recs, is_archived=False),
                                                                 (Q(is_submitted=True, is_apprpved=False)) or (
                                                                     Q(is_rejected=True, is_apprpved=False))(
                                                                     submitted_by=user_name))

            else:
                ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,
                                                                        class_name_id=class_name,
                                                                        staff__id=user_name.id)


            # scheme_ids = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')

            # scheme_ay_recs = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs)
                scheme_mark_recs = freq_rec_table.objects.filter(Q(academic_mapping__in=ac_recs,is_archived= False),
                                                             (Q(is_submitted=True, is_apprpved=False)) or (
                                                             Q(is_rejected=True, is_apprpved=False))(submitted_by=user_name))

            # for diff_rec in scheme_mark_ids:
            #     diff_list.append(diff_rec[0])

            scheme_mapping_recs = []  # mapp_table.objects.filter(id__in=diff_list)
            # scheme_mark_recs = freq_rec_table.objects.filter(scheme_mapping__in=diff_list)

            return scheme_mapping_recs, scheme_mark_recs

        if status_name == '4':
            if user_name.is_system_admin():
                ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,
                                                                        class_name_id=class_name)
                scheme_mark_recs = freq_rec_table.objects.filter(
                    academic_mapping__in=ac_recs, is_rejected=False, is_apprpved=True, is_archived=False)


            elif user_name.is_subject_teacher():
                ac_recs = []
                for mapped_object in academic_class_section_mapping.objects.filter(year_name_id=year_name,
                                                                                   class_name_id=class_name):
                    for obj in teacher_subject_mapping.objects.filter(academic_class_section_mapping=mapped_object,
                                                                      staff_id=user_name):
                        if obj:
                            ac_recs.append(mapped_object)
                scheme_mark_recs = freq_rec_table.objects.filter(
                    academic_mapping__in=ac_recs, is_rejected=False, is_apprpved=True, is_archived=False,
                    submitted_by=user_name)

            else:
                ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,
                                                                        class_name_id=class_name,
                                                                        staff__id=user_name.id)


            # scheme_ids = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')
            #
            # scheme_ay_recs = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs)
                scheme_mark_recs = freq_rec_table.objects.filter(
                academic_mapping__in=ac_recs, is_rejected=False, is_apprpved=True,is_archived= False,submitted_by=user_name)

            # for diff_rec in scheme_mark_ids:
            #     diff_list.append(diff_rec[0])

            scheme_mapping_recs = []  # mapp_table.objects.filter(id__in=diff_list)
            # scheme_mark_recs = freq_rec_table.objects.filter(scheme_mapping__in = diff_list)

            return scheme_mapping_recs, scheme_mark_recs


    diff_list = []

    if class_name=='' and subject_teacher=='' and status_name!='':
        scheme_mapping_recs,scheme_mark_recs  = load_status_fun(status_name)

    if class_name!='' and subject_teacher=='' and status_name!='':
        scheme_mapping_recs,scheme_mark_recs  = load_class_status_fun(class_name,status_name)

    if class_name!='' and subject_teacher=='' and status_name=='':
        if user_name.is_system_admin():
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name, class_name_id=class_name)
            scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs, is_archived=False)

        elif user_name.is_subject_teacher():
            ac_recs = []
            for mapped_object in academic_class_section_mapping.objects.filter(year_name_id=year_name,class_name_id=class_name):
                for obj in teacher_subject_mapping.objects.filter(academic_class_section_mapping=mapped_object,
                                                                  staff_id=user_name):
                    if obj:
                        ac_recs.append(mapped_object)
            scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs, is_archived=False,
                                                             submitted_by=user_name)
        else:
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,class_name_id=class_name,staff__id=user_name.id)


        # scheme_ids = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')

        # scheme_ay_recs = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs)
            scheme_ay_recs=[]
            scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs,is_archived= False,submitted_by=user_name)
        a = []
        # diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))

        # diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))
        #
        # for diff_rec in diff_recs:
        #     diff_list.append(diff_rec[0])
        #
        # scheme_mapping_recs = mapp_table.objects.filter(id__in=diff_list)
        # scheme_mark_recs = freq_rec_table.objects.filter(scheme_mapping__in=scheme_ay_recs)

    if class_name!='' and subject_teacher!='' and status_name!='':
        scheme_mapping_recs,scheme_mark_recs  = load_class_status_fun(class_name,status_name)

        rec_list = []
        final_list = []
        rows = []
        for scheme_mark_rec in scheme_mark_recs:
            teacher_ids = []
            tchr_rec = teacher_subject_mapping.objects.filter(
                academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                subject=scheme_mark_rec.scheme_mapping.subject)
            for tchr in tchr_rec:
                teacher_ids.append(int(tchr.staff_id.id))

            if int(subject_teacher) in teacher_ids:
                teacher_recs = teacher_subject_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                mapped_data = {
                    'supervisor_recs': supervisor_recs,
                    'teacher_recs': teacher_recs,
                    'scheme_mark_rec': scheme_mark_rec
                }
                rec_list.append(mapped_data)
                tchr_list = []
                supervisor_recs_list = []
                status = ''

                tchr_name = ''
                sup_name = ''
                count = 1
                for rec in teacher_recs:
                    tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
                    count = count + 1
                    # tchr_list.append(rec.staff_id.get_pura_name())

                count = 1
                for rec in supervisor_recs:
                    sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
                    count = count + 1
                    supervisor_recs_list.append(rec.supervisor_id.get_pura_name())

                if scheme_mark_rec.is_submitted == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Under Review'
                elif scheme_mark_rec.is_apprpved == 1 and scheme_mark_rec.is_rejected == 0:
                    status = 'Final'
                elif scheme_mark_rec.is_rejected == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Draft'
                elif scheme_mark_rec.is_apprpved == 0 and scheme_mark_rec.is_rejected == 0:
                    status = 'Draft'

                dict_rec = {}
                dict_rec['Submitted_By'] = scheme_mark_rec.submitted_by
                dict_rec['Subject_Teacher'] = tchr_name
                dict_rec['Supervisor'] = sup_name
                dict_rec['Academic_Year'] = scheme_mark_rec.academic_mapping.year_name.year_name
                dict_rec['Class'] = scheme_mark_rec.academic_mapping.class_name.class_name
                dict_rec['Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
                dict_rec['Subject'] = scheme_mark_rec.scheme_mapping.subject.subject_name
                dict_rec['Grade_Scheme'] = scheme_mark_rec.scheme_mapping.grade_name.grade_name
                dict_rec['Scheme'] = scheme_mark_rec.scheme_mapping.scheme_name.scheme_name
                if scheme_mark_rec.approved_by is None:
                    dict_rec['Approved_By'] = ''
                else:
                    dict_rec['Approved_By'] = scheme_mark_rec.approved_by
                # dict_rec['Frequency'] = scheme_mark_rec.frequency.frequency_name
                dict_rec['Status'] = status
                final_list.append(dict_rec)

        mapping_rec_list = []

        # for scheme_mapping_rec in scheme_mapping_recs:
        #     teacher_ids = []
        #     tchr_rec = teacher_subject_mapping.objects.filter(
        #         academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #         subject=scheme_mapping_rec.subject)
        #     for tchr in tchr_rec:
        #         teacher_ids.append(int(tchr.staff_id.id))
        #
        #     if int(subject_teacher) in teacher_ids:
        #
        #         supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         teacher_recs = teacher_subject_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         mapped_data = {
        #             'supervisor_recs': supervisor_recs,
        #             'teacher_recs': teacher_recs,
        #             'scheme_mapping_rec': scheme_mapping_rec
        #         }
        #         mapping_rec_list.append(mapped_data)
        #
        #         tchr_list = []
        #         supervisor_recs_list = []
        #         tchr_name = ''
        #         sup_name = ''
        #
        #         count = 1
        #         for rec in teacher_recs:
        #             tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
        #             count = count + 1
        #             # tchr_list.append(rec.staff_id.get_pura_name())
        #         count = 1
        #         for rec in supervisor_recs:
        #             sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
        #             count = count + 1
        #             supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
        #
        #         dict_rec = {}
        #         dict_rec['Submitted_By'] = 'Not Submitted'
        #         dict_rec['Subject_Teacher'] = tchr_name
        #         dict_rec['Supervisor'] = sup_name
        #         dict_rec['Academic_Year'] = scheme_mapping_rec.academic_class_section_mapping.year_name.year_name
        #         dict_rec['Class'] = scheme_mapping_rec.academic_class_section_mapping.class_name.class_name
        #         dict_rec['Section'] = scheme_mapping_rec.academic_class_section_mapping.section_name.section_name
        #         dict_rec['Subject'] = scheme_mapping_rec.subject.subject_name
        #         dict_rec['Grade_Scheme'] = scheme_mapping_rec.grade_name.grade_name
        #         dict_rec['Scheme'] = scheme_mapping_rec.scheme_name.scheme_name
        #         dict_rec['Approved_By'] = 'Not Approved'
        #         dict_rec['Status'] = 'Not Started'
        #         final_list.append(dict_rec)
        for rec in final_list:
            ab = [rec['Submitted_By'], rec['Subject_Teacher'], rec['Supervisor'], rec['Academic_Year'],
                  rec['Class'],
                  rec['Section'], rec['Subject'], rec['Grade_Scheme'], rec['Scheme'], rec['Approved_By'],
                  rec['Status']]
            if ab not in rows:
                rows.append(ab)
        status_flag = True
        return rows, final_list

    if class_name=='' and subject_teacher!='' and status_name!='':
        scheme_mapping_recs, scheme_mark_recs = load_status_fun(status_name)
        rec_list = []
        final_list = []
        rows = []
        for scheme_mark_rec in scheme_mark_recs:
            teacher_ids = []
            tchr_rec = teacher_subject_mapping.objects.filter(
                academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                subject=scheme_mark_rec.scheme_mapping.subject)
            for tchr in tchr_rec:
                teacher_ids.append(int(tchr.staff_id.id))

            if int(subject_teacher) in teacher_ids:
                teacher_recs = teacher_subject_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                mapped_data = {
                    'supervisor_recs': supervisor_recs,
                    'teacher_recs': teacher_recs,
                    'scheme_mark_rec': scheme_mark_rec
                }
                rec_list.append(mapped_data)
                tchr_list = []
                supervisor_recs_list = []
                status = ''

                tchr_name = ''
                sup_name = ''
                count = 1
                for rec in teacher_recs:
                    tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
                    count = count + 1
                    # tchr_list.append(rec.staff_id.get_pura_name())

                count = 1
                for rec in supervisor_recs:
                    sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
                    count = count + 1
                    supervisor_recs_list.append(rec.supervisor_id.get_pura_name())

                if scheme_mark_rec.is_submitted == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Under Review'
                elif scheme_mark_rec.is_apprpved == 1 and scheme_mark_rec.is_rejected == 0:
                    status = 'Final'
                elif scheme_mark_rec.is_rejected == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Draft'
                elif scheme_mark_rec.is_apprpved == 0 and scheme_mark_rec.is_rejected == 0:
                    status = 'Draft'

                dict_rec = {}
                dict_rec['Submitted_By'] = scheme_mark_rec.submitted_by
                dict_rec['Subject_Teacher'] = tchr_name
                dict_rec['Supervisor'] = sup_name
                dict_rec['Academic_Year'] = scheme_mark_rec.academic_mapping.year_name.year_name
                dict_rec['Class'] = scheme_mark_rec.academic_mapping.class_name.class_name
                dict_rec['Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
                dict_rec['Subject'] = scheme_mark_rec.scheme_mapping.subject.subject_name
                dict_rec['Grade_Scheme'] = scheme_mark_rec.scheme_mapping.grade_name.grade_name
                dict_rec['Scheme'] = scheme_mark_rec.scheme_mapping.scheme_name.scheme_name
                if scheme_mark_rec.approved_by is None:
                    dict_rec['Approved_By'] = 'Not Approved'
                elif not scheme_mark_rec.approved_by.username:
                    dict_rec['Approved_By'] = 'Not Approved'
                else:
                    dict_rec['Approved_By'] = scheme_mark_rec.approved_by
                # dict_rec['Frequency'] = scheme_mark_rec.frequency.frequency_name
                dict_rec['Status'] = status
                final_list.append(dict_rec)

        mapping_rec_list = []

        # for scheme_mapping_rec in scheme_mapping_recs:
        #     teacher_ids = []
        #     tchr_rec = teacher_subject_mapping.objects.filter(
        #         academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #         subject=scheme_mapping_rec.subject)
        #     for tchr in tchr_rec:
        #         teacher_ids.append(int(tchr.staff_id.id))
        #
        #     if int(subject_teacher) in teacher_ids:
        #
        #         supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         teacher_recs = teacher_subject_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         mapped_data = {
        #             'supervisor_recs': supervisor_recs,
        #             'teacher_recs': teacher_recs,
        #             'scheme_mapping_rec': scheme_mapping_rec
        #         }
        #         mapping_rec_list.append(mapped_data)
        #
        #         tchr_list = []
        #         supervisor_recs_list = []
        #         tchr_name = ''
        #         sup_name = ''
        #
        #         count = 1
        #         for rec in teacher_recs:
        #             tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
        #             count = count + 1
        #             # tchr_list.append(rec.staff_id.get_pura_name())
        #         count = 1
        #         for rec in supervisor_recs:
        #             sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
        #             count = count + 1
        #             supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
        #
        #         dict_rec = {}
        #         dict_rec['Submitted_By'] = 'Not Submitted'
        #         dict_rec['Subject_Teacher'] = tchr_name
        #         dict_rec['Supervisor'] = sup_name
        #         dict_rec['Academic_Year'] = scheme_mapping_rec.academic_class_section_mapping.year_name.year_name
        #         dict_rec['Class'] = scheme_mapping_rec.academic_class_section_mapping.class_name.class_name
        #         dict_rec['Section'] = scheme_mapping_rec.academic_class_section_mapping.section_name.section_name
        #         dict_rec['Subject'] = scheme_mapping_rec.subject.subject_name
        #         dict_rec['Grade_Scheme'] = scheme_mapping_rec.grade_name.grade_name
        #         dict_rec['Scheme'] = scheme_mapping_rec.scheme_name.scheme_name
        #         dict_rec['Approved_By'] = 'Not Approved'
        #         dict_rec['Status'] = 'Not Started'
        #         final_list.append(dict_rec)
        for rec in final_list:
            ab = [rec['Submitted_By'], rec['Subject_Teacher'], rec['Supervisor'], rec['Academic_Year'],
                  rec['Class'],
                  rec['Section'], rec['Subject'], rec['Grade_Scheme'], rec['Scheme'], rec['Approved_By'],
                  rec['Status']]
            if ab not in rows:
                rows.append(ab)
        status_flag = True
        return rows,final_list

    if class_name=='' and subject_teacher!='' and status_name=='':
        if user_name.is_system_admin():
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name)
            scheme_mark_recs = freq_rec_table.objects.filter(is_archived=False)

        elif user_name.is_subject_teacher():
            ac_recs = []
            for mapped_object in academic_class_section_mapping.objects.filter(year_name_id=year_name):
                for obj in teacher_subject_mapping.objects.filter(academic_class_section_mapping=mapped_object,
                                                                  staff_id=user_name):
                    if obj:
                        ac_recs.append(mapped_object)
            scheme_mark_recs = freq_rec_table.objects.filter(is_archived=False, submitted_by=user_name)
        else:
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,staff__id=user_name.id)


        # scheme_ids = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')
            scheme_mark_recs = freq_rec_table.objects.filter(is_archived= False,submitted_by=user_name)
        # diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))
        #
        # for diff_rec in diff_recs:
        #     diff_list.append(diff_rec[0])
            #
        scheme_mapping_recs = []
        # scheme_mark_recs = freq_rec_table.objects.all()

        rec_list = []
        final_list = []
        rows = []
        for scheme_mark_rec in scheme_mark_recs:
            teacher_ids = []
            tchr_rec = teacher_subject_mapping.objects.filter(
                academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                subject=scheme_mark_rec.scheme_mapping.subject)
            for tchr in tchr_rec:
                teacher_ids.append(int(tchr.staff_id.id))

            if int(subject_teacher) in teacher_ids:
                teacher_recs = teacher_subject_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                mapped_data = {
                    'supervisor_recs': supervisor_recs,
                    'teacher_recs': teacher_recs,
                    'scheme_mark_rec': scheme_mark_rec
                }
                rec_list.append(mapped_data)
                tchr_list = []
                supervisor_recs_list = []
                status = ''

                tchr_name = ''
                sup_name = ''
                count = 1
                for rec in teacher_recs:
                    tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
                    count = count + 1
                    # tchr_list.append(rec.staff_id.get_pura_name())

                count = 1
                for rec in supervisor_recs:
                    sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
                    count = count + 1
                    supervisor_recs_list.append(rec.supervisor_id.get_pura_name())

                if scheme_mark_rec.is_submitted == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Under Review'
                elif scheme_mark_rec.is_apprpved == 1 and scheme_mark_rec.is_rejected == 0:
                    status = 'Final'
                elif scheme_mark_rec.is_rejected == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Draft'
                elif scheme_mark_rec.is_apprpved == 0 and scheme_mark_rec.is_rejected == 0:
                    status = 'Draft'

                dict_rec = {}
                dict_rec['Submitted_By'] = scheme_mark_rec.submitted_by
                dict_rec['Subject_Teacher'] = tchr_name
                dict_rec['Supervisor'] = sup_name
                dict_rec['Academic_Year'] = scheme_mark_rec.academic_mapping.year_name.year_name
                dict_rec['Class'] = scheme_mark_rec.academic_mapping.class_name.class_name
                dict_rec['Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
                dict_rec['Subject'] = scheme_mark_rec.scheme_mapping.subject.subject_name
                dict_rec['Grade_Scheme'] = scheme_mark_rec.scheme_mapping.grade_name.grade_name
                dict_rec['Scheme'] = scheme_mark_rec.scheme_mapping.scheme_name.scheme_name
                if scheme_mark_rec.approved_by is None:
                    dict_rec['Approved_By'] = 'Not Approved'
                elif not scheme_mark_rec.approved_by.username:
                    dict_rec['Approved_By'] = 'Not Approved'
                else:
                    dict_rec['Approved_By'] = scheme_mark_rec.approved_by
                # dict_rec['Frequency'] = scheme_mark_rec.frequency.frequency_name
                dict_rec['Status'] = status
                final_list.append(dict_rec)

        mapping_rec_list = []

        # for scheme_mapping_rec in scheme_mapping_recs:
        #     teacher_ids = []
        #     tchr_rec = teacher_subject_mapping.objects.filter(
        #         academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #         subject=scheme_mapping_rec.subject)
        #     for tchr in tchr_rec:
        #         teacher_ids.append(int(tchr.staff_id.id))
        #
        #     if int(subject_teacher) in teacher_ids:
        #
        #         supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         teacher_recs = teacher_subject_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         mapped_data = {
        #             'supervisor_recs': supervisor_recs,
        #             'teacher_recs': teacher_recs,
        #             'scheme_mapping_rec': scheme_mapping_rec
        #         }
        #         mapping_rec_list.append(mapped_data)
        #
        #         tchr_list = []
        #         supervisor_recs_list = []
        #         tchr_name = ''
        #         sup_name = ''
        #
        #         count = 1
        #         for rec in teacher_recs:
        #             tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
        #             count = count + 1
        #             # tchr_list.append(rec.staff_id.get_pura_name())
        #         count = 1
        #         for rec in supervisor_recs:
        #             sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
        #             count = count + 1
        #             supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
        #
        #         dict_rec = {}
        #         dict_rec['Submitted_By'] = 'Not Submitted'
        #         dict_rec['Subject_Teacher'] = tchr_name
        #         dict_rec['Supervisor'] = sup_name
        #         dict_rec['Academic_Year'] = scheme_mapping_rec.academic_class_section_mapping.year_name.year_name
        #         dict_rec['Class'] = scheme_mapping_rec.academic_class_section_mapping.class_name.class_name
        #         dict_rec['Section'] = scheme_mapping_rec.academic_class_section_mapping.section_name.section_name
        #         dict_rec['Subject'] = scheme_mapping_rec.subject.subject_name
        #         dict_rec['Grade_Scheme'] = scheme_mapping_rec.grade_name.grade_name
        #         dict_rec['Scheme'] = scheme_mapping_rec.scheme_name.scheme_name
        #         dict_rec['Approved_By'] = 'Not Approved'
        #         dict_rec['Status'] = 'Not Started'
        #         final_list.append(dict_rec)
        for rec in final_list:
            ab = [rec['Submitted_By'], rec['Subject_Teacher'], rec['Supervisor'], rec['Academic_Year'],
                  rec['Class'],
                  rec['Section'], rec['Subject'], rec['Grade_Scheme'], rec['Scheme'], rec['Approved_By'],
                  rec['Status']]
            if ab not in rows:
                rows.append(ab)
        status_flag = True
        return rows,final_list

    if class_name != '' and subject_teacher != '' and status_name == '':
        if user_name.is_system_admin():
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name, class_name_id=class_name)
            scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs, is_archived=False,)

        elif user_name.is_subject_teacher():
            ac_recs = []
            for mapped_object in academic_class_section_mapping.objects.filter(year_name_id=year_name,class_name_id=class_name):
                for obj in teacher_subject_mapping.objects.filter(academic_class_section_mapping=mapped_object,staff_id=user_name):
                    if obj:
                        ac_recs.append(mapped_object)
            scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs, is_archived=False,
                                                             submitted_by=user_name)
        else:
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,class_name_id=class_name, staff__id=user_name.id)

        # scheme_ids = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')

        # scheme_ay_recs = mapp_table.objects.filter(academic_mapping__in=ac_recs)
            scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs,is_archived= False,submitted_by=user_name)
        # diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))

        # diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))
        #
        # for diff_rec in diff_recs:
        #     diff_list.append(diff_rec[0])

        scheme_mapping_recs = []
        # scheme_mark_recs = freq_rec_table.objects.filter(scheme_mapping__in=scheme_ay_recs)

        rec_list = []
        final_list = []
        rows = []
        for scheme_mark_rec in scheme_mark_recs:
            teacher_ids = []
            tchr_rec = teacher_subject_mapping.objects.filter(
                academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                subject=scheme_mark_rec.scheme_mapping.subject)
            for tchr in tchr_rec:
                teacher_ids.append(int(tchr.staff_id.id))

            if int(subject_teacher) in teacher_ids:
                teacher_recs = teacher_subject_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                mapped_data = {
                    'supervisor_recs': supervisor_recs,
                    'teacher_recs': teacher_recs,
                    'scheme_mark_rec': scheme_mark_rec
                }
                rec_list.append(mapped_data)
                tchr_list = []
                supervisor_recs_list = []
                status = ''

                tchr_name = ''
                sup_name = ''
                count = 1
                for rec in teacher_recs:
                    tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
                    count = count + 1
                    # tchr_list.append(rec.staff_id.get_pura_name())

                count = 1
                for rec in supervisor_recs:
                    sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
                    count = count + 1
                    supervisor_recs_list.append(rec.supervisor_id.get_pura_name())

                if scheme_mark_rec.is_submitted == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Under Review'
                elif scheme_mark_rec.is_apprpved == 1 and scheme_mark_rec.is_rejected == 0:
                    status = 'Final'
                elif scheme_mark_rec.is_rejected == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Draft'
                elif scheme_mark_rec.is_apprpved == 0 and scheme_mark_rec.is_rejected == 0:
                    status = 'Draft'

                dict_rec = {}
                dict_rec['Submitted_By'] = scheme_mark_rec.submitted_by
                dict_rec['Subject_Teacher'] = tchr_name
                dict_rec['Supervisor'] = sup_name
                dict_rec['Academic_Year'] = scheme_mark_rec.academic_mapping.year_name.year_name
                dict_rec['Class'] = scheme_mark_rec.academic_mapping.class_name.class_name
                dict_rec['Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
                dict_rec['Subject'] = scheme_mark_rec.scheme_mapping.subject.subject_name
                dict_rec['Grade_Scheme'] = scheme_mark_rec.scheme_mapping.grade_name.grade_name
                dict_rec['Scheme'] = scheme_mark_rec.scheme_mapping.scheme_name.scheme_name
                if scheme_mark_rec.approved_by is None:
                    dict_rec['Approved_By'] = 'Not Approved'
                elif not scheme_mark_rec.approved_by.username:
                    dict_rec['Approved_By'] = 'Not Approved'
                else:
                    dict_rec['Approved_By'] = scheme_mark_rec.approved_by
                # dict_rec['Frequency'] = scheme_mark_rec.frequency.frequency_name
                dict_rec['Status'] = status
                final_list.append(dict_rec)

        mapping_rec_list = []

        # for scheme_mapping_rec in scheme_mapping_recs:
        #     teacher_ids = []
        #     tchr_rec = teacher_subject_mapping.objects.filter(
        #         academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #         subject=scheme_mapping_rec.subject)
        #     for tchr in tchr_rec:
        #         teacher_ids.append(int(tchr.staff_id.id))
        #
        #     if int(subject_teacher) in teacher_ids:
        #
        #         supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         teacher_recs = teacher_subject_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         mapped_data = {
        #             'supervisor_recs': supervisor_recs,
        #             'teacher_recs': teacher_recs,
        #             'scheme_mapping_rec': scheme_mapping_rec
        #         }
        #         mapping_rec_list.append(mapped_data)
        #
        #         tchr_list = []
        #         supervisor_recs_list = []
        #         tchr_name = ''
        #         sup_name = ''
        #
        #         count = 1
        #         for rec in teacher_recs:
        #             tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
        #             count = count + 1
        #             # tchr_list.append(rec.staff_id.get_pura_name())
        #         count = 1
        #         for rec in supervisor_recs:
        #             sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
        #             count = count + 1
        #             supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
        #
        #         dict_rec = {}
        #         dict_rec['Submitted_By'] = 'Not Submitted'
        #         dict_rec['Subject_Teacher'] = tchr_name
        #         dict_rec['Supervisor'] = sup_name
        #         dict_rec['Academic_Year'] = scheme_mapping_rec.academic_class_section_mapping.year_name.year_name
        #         dict_rec['Class'] = scheme_mapping_rec.academic_class_section_mapping.class_name.class_name
        #         dict_rec['Section'] = scheme_mapping_rec.academic_class_section_mapping.section_name.section_name
        #         dict_rec['Subject'] = scheme_mapping_rec.subject.subject_name
        #         dict_rec['Grade_Scheme'] = scheme_mapping_rec.grade_name.grade_name
        #         dict_rec['Scheme'] = scheme_mapping_rec.scheme_name.scheme_name
        #         dict_rec['Approved_By'] = 'Not Approved'
        #         dict_rec['Status'] = 'Not Started'
        #         final_list.append(dict_rec)
        for rec in final_list:
            ab = [rec['Submitted_By'], rec['Subject_Teacher'], rec['Supervisor'], rec['Academic_Year'],
                  rec['Class'],rec['Section'], rec['Subject'], rec['Grade_Scheme'], rec['Scheme'], rec['Approved_By'],rec['Status']]
            if ab not in rows:
                rows.append(ab)
        return rows, final_list


    if (class_name==None and subject_teacher==None and status_name==None) or (class_name=='' and subject_teacher=='' and status_name==''):
        if user_name.is_system_admin():
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name)
            scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs, is_archived=False)

        elif user_name.is_subject_teacher():
            ac_recs = []
            for mapped_object in academic_class_section_mapping.objects.filter(year_name_id=year_name):
                for obj in teacher_subject_mapping.objects.filter(academic_class_section_mapping=mapped_object,staff_id=user_name):
                    if obj:
                        ac_recs.append(mapped_object)

            scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs, is_archived=False,
                                                             submitted_by=user_name)
        else:
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name, staff__id=user_name.id)

        # scheme_ids = mapp_table.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')
            scheme_mark_recs = freq_rec_table.objects.filter(academic_mapping__in=ac_recs,is_archived= False,submitted_by=user_name)
        # diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))
        #
        # for diff_rec in diff_recs:
        #     diff_list.append(diff_rec[0])
    #
        scheme_mapping_recs = []
        # scheme_mark_recs = freq_rec_table.objects.all()

    rec_list = []
    final_list = []
    rows = []
    for scheme_mark_rec in scheme_mark_recs:
        supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(academic_class_section_mapping=scheme_mark_rec.academic_mapping,subject=scheme_mark_rec.scheme_mapping.subject)
        teacher_recs = teacher_subject_mapping.objects.filter(academic_class_section_mapping=scheme_mark_rec.academic_mapping,subject=scheme_mark_rec.scheme_mapping.subject)
        mapped_data = {
            'supervisor_recs': supervisor_recs,
            'teacher_recs': teacher_recs,
            'scheme_mark_rec': scheme_mark_rec
        }
        rec_list.append(mapped_data)
        tchr_list = []
        supervisor_recs_list = []
        status = ''

        tchr_name = ''
        sup_name = ''
        count = 1
        for rec in teacher_recs:
            tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
            count = count + 1
            # tchr_list.append(rec.staff_id.get_pura_name())

        count = 1
        for rec in supervisor_recs:
            sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
            count = count + 1
            supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
        # for rec in teacher_recs:
        #     tchr_list.append(rec.staff_id.get_pura_name())
        #
        # for rec in supervisor_recs:
        #     supervisor_recs_list.append(rec.supervisor_id.get_pura_name())

        if scheme_mark_rec.is_submitted == 1 and scheme_mark_rec.is_apprpved == 0:
            status = 'Under Review'
        elif scheme_mark_rec.is_apprpved == 1 and scheme_mark_rec.is_rejected == 0:
            status = 'Final'
        elif scheme_mark_rec.is_rejected == 1 and scheme_mark_rec.is_apprpved == 0:
            status = 'Draft'
        elif scheme_mark_rec.is_apprpved == 0 and scheme_mark_rec.is_rejected == 0:
            status = 'Draft'

        dict_rec = {}
        dict_rec['Submitted_By'] = scheme_mark_rec.submitted_by
        dict_rec['Subject_Teacher'] = tchr_name
        dict_rec['Supervisor'] = sup_name
        dict_rec['Academic_Year'] = scheme_mark_rec.academic_mapping.year_name.year_name
        dict_rec['Class'] = scheme_mark_rec.academic_mapping.class_name.class_name
        dict_rec['Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
        dict_rec['Subject'] = scheme_mark_rec.scheme_mapping.subject.subject_name
        dict_rec['Grade_Scheme'] = scheme_mark_rec.scheme_mapping.grade_name.grade_name
        dict_rec['Scheme'] = scheme_mark_rec.scheme_mapping.scheme_name.scheme_name
        if scheme_mark_rec.approved_by is None:
            dict_rec['Approved_By'] = 'Not Approved'
        elif not scheme_mark_rec.approved_by.username:
            dict_rec['Approved_By'] = 'Not Approved'
        else:
            dict_rec['Approved_By'] = scheme_mark_rec.approved_by
        # dict_rec['Frequency'] = scheme_mark_rec.frequency.frequency_name
        dict_rec['Status'] = status
        final_list.append(dict_rec)

    mapping_rec_list = []
    # for scheme_mapping_rec in scheme_mapping_recs:
    #     supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
    #         academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
    #         subject=scheme_mapping_rec.subject)
    #     teacher_recs = teacher_subject_mapping.objects.filter(
    #         academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
    #         subject=scheme_mapping_rec.subject)
    #     mapped_data = {
    #         'supervisor_recs': supervisor_recs,
    #         'teacher_recs': teacher_recs,
    #         'scheme_mapping_rec': scheme_mapping_rec
    #     }
    #     mapping_rec_list.append(mapped_data)
    #
    #     tchr_list = []
    #     supervisor_recs_list = []
    #     tchr_name = ''
    #     sup_name = ''
    #
    #     count = 1
    #     for rec in teacher_recs:
    #         tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
    #         count = count + 1
    #         # tchr_list.append(rec.staff_id.get_pura_name())
    #     count = 1
    #     for rec in supervisor_recs:
    #         sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
    #         count = count + 1
    #         supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
    #
    #     dict_rec = {}
    #     dict_rec['Submitted_By'] = 'Not Submitted'
    #     dict_rec['Subject_Teacher'] = tchr_name
    #     dict_rec['Supervisor'] = sup_name
    #     dict_rec['Academic_Year'] = scheme_mapping_rec.academic_class_section_mapping.year_name.year_name
    #     dict_rec['Class'] = scheme_mapping_rec.academic_class_section_mapping.class_name.class_name
    #     dict_rec['Section'] = scheme_mapping_rec.academic_class_section_mapping.section_name.section_name
    #     dict_rec['Subject'] = scheme_mapping_rec.subject.subject_name
    #     dict_rec['Grade_Scheme'] = scheme_mapping_rec.grade_name.grade_name
    #     dict_rec['Scheme'] = scheme_mapping_rec.scheme_name.scheme_name
    #     dict_rec['Approved_By'] = 'Not Approved'
    #     dict_rec['Status'] = 'Not Started'
    #     final_list.append(dict_rec)
    for rec in final_list:
        ab = [rec['Submitted_By'], rec['Subject_Teacher'], rec['Supervisor'], rec['Academic_Year'], rec['Class'],
              rec['Section'], rec['Subject'], rec['Grade_Scheme'], rec['Scheme'], rec['Approved_By'], rec['Status']]
        rows.append(ab)
    return rows, final_list

@user_login_required
@user_permission_required('assessment.can_view_view_skill_status_list', '/school/home/')
def SchemeStatus(request):
    # print "In SchemeStatus function"
    user_name =request.user
    staff_list =[]
    all_academicyr_recs = academic_year.objects.all()
#    all_teacher_recs = teacher_subject_mapping.objects.all()
    if request.user.is_system_admin():
        all_teacher_recs = teacher_subject_mapping.objects.all()
    else:
        all_teacher_recs = teacher_subject_mapping.objects.filter(staff_id=request.user)
    for teacher_recs in all_teacher_recs:
        if teacher_recs.staff_id not in staff_list:
            staff_list.append(teacher_recs.staff_id)
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        class_list = mapped_user_classes(current_academic_year,request)
        #class_list = current_academic_year_mapped_classes(current_academic_year, request.user)
    except:
        messages.warning(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
        return render(request,'assessment_sub_tile.html')
    selected_year_id = current_academic_year.id
    rows, final_list = load_status(scheme_mapping_details ,scheme_academic_frequency_mapping,selected_year_id,user_name)

    form_vals = {
        # 'scheme_mark_recs': rec_list,
        # 'scheme_mapping_recs': mapping_rec_list,
        "year_list": all_academicyr_recs,
        "all_teacher_recs": staff_list,
        'selected_year_name': current_academic_year,
        'Selected_Year_Id': selected_year_id,
        'option_count' : itertools.count(1),
        'rows':rows,
        'final_list':final_list,
        'class_list':class_list
    }
    return render(request, "scheme_status.html",form_vals)


def load_scheme_status_function(scheme_val_dict,request):
    # print "In SchemeStatus function"
    user_name = request.user
    staff_list = []
    class_list = class_details.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    if request.user.is_system_admin():
        all_teacher_recs = teacher_subject_mapping.objects.all()
    else:
        all_teacher_recs = teacher_subject_mapping.objects.filter(staff_id=request.user)
    #all_teacher_recs = teacher_subject_mapping.objects.all()

    for teacher_recs in all_teacher_recs:
        if teacher_recs.staff_id not in staff_list:
            staff_list.append(teacher_recs.staff_id)

    all_academicyr_recs = academic_year.objects.all()
    year_name = scheme_val_dict.get('year_name', None)
    class_name = scheme_val_dict.get('class_name', None)
    status_name = scheme_val_dict.get('status_name', None)
    subject_tchr_name = scheme_val_dict.get('subject_tchr_name', None)

    rows, final_list = load_status(scheme_mapping_details, scheme_academic_frequency_mapping, year_name,user_name, class_name,
                                   subject_tchr_name, status_name)

    if subject_tchr_name:
        subject_tchr_rec = AuthUser.objects.get(id=subject_tchr_name)
    else:
        subject_tchr_rec= ''
    if class_name:
        class_rec = class_details.objects.get(id=class_name)
    else:
        class_rec = ''
    if status_name:
        if status_name =='1':
            status_rec = 'Not Started'
            status_id = '1'
        if status_name =='2':
            status_rec = 'Draft'
            status_id = '2'
        if status_name =='3':
            status_rec = 'Under Review'
            status_id = '3'
        if status_name =='4':
            status_rec = 'Final'
            status_id = '4'
    else:
        status_rec = ''
        status_id = ''
    current_academic_year = academic_year.objects.get(current_academic_year=1)
    class_list = mapped_user_classes(current_academic_year, request)
    #class_list = current_academic_year_mapped_classes(current_academic_year, user)
    selected_year_id = current_academic_year.id
    form_vals = {
        'final_list':final_list,
        'rows':rows,
        # 'scheme_mark_recs': rec_list,
        "year_list": all_academicyr_recs,
        'selected_year_name': current_academic_year,
        'Selected_Year_Id': selected_year_id,
        'subject_tchr_rec': subject_tchr_rec,
        'status_id':status_id,
        'status_rec':status_rec,
        "all_teacher_recs": staff_list,
        # 'section_rec': section_rec,
        # 'scheme_rec': scheme_rec,
        'class_rec' :class_rec,
        'class_list':class_list
    }
    return form_vals

@user_login_required
def SchemeStatusList(request):
    scheme_val_dict = request.POST
    form_vals = {}
    flag = False

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
    except:
        messages.warning(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
        return render(request, 'assessment_sub_tile.html')

    if request.method == 'POST':
        request.session['scheme_val_dict'] = scheme_val_dict
        form_vals = load_scheme_status_function(scheme_val_dict, request)
    else:
        form_vals = load_scheme_status_function(request.session.get('scheme_val_dict'), request)

    return render(request, "scheme_status.html", form_vals)


@user_login_required
def ExportSchemeStatusRecords(request):

    year_name = request.POST.get('e_year_name',None)
    class_name = request.POST.get('e_class_name', None)
    # scheme_name = request.POST.get('e_exam_scheme_name', None)
    status_name = request.POST.get('e_status_name', None)
    subject_tchr_name = request.POST.get('e_subject_tchr_name', None)
    user_name = request.user

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
    except:
        messages.warning(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
        return render(request,'assessment_sub_tile.html')
    selected_year_id = current_academic_year.id

    rows, final_list = load_status(scheme_mapping_details ,scheme_academic_frequency_mapping,year_name,user_name, class_name, subject_tchr_name, status_name)

    column_names = ['Submitted By','Subject Teacher','Supervisor','Academic Year', 'Class', 'Section', 'Subject','Grade Scheme', 'Scheme', 'Approved By','Status']
    return export_users_xls('scheme_status', column_names, rows)


@user_login_required
@user_permission_required('assessment.can_view_view_exam_categories', '/school/home/')
def ExamSubTile(request):
    xyz = [] #get_all_user_permissions(request)
    return render(request, "exam_sub_tile.html",{'perms': list(xyz)})


@user_login_required
def CreateExam(request):
    exam_details_form = ExamDetailsForm()
    frequency_recs = frequency_details.objects.all()
    return render(request, "create_exam.html", {'exam_details_form': exam_details_form,'frequency_recs':frequency_recs})


@user_login_required
@user_permission_required('assessment.can_view_view_create_exam_list', '/school/home/')
def ExamList(request):
    exam_recs = exam_details.objects.all()
    rec_list = []
    for rec in exam_recs:
        frequency_recs = exam_details_frequency.objects.filter(exam_details_name=rec.id)
        mapped_data = {
            'frequency_recs': frequency_recs,
            'exam_recs': rec,
        }
        rec_list.append(mapped_data)

    return render(request, "exam_list.html", {'exam_recs': exam_recs,'exam_data':rec_list})


@user_login_required
def SaveExam(request):
    if request.method == 'POST':
        frequency_mapping_ids = request.POST.getlist('frequency_mapping')

        exam_details_form = ExamDetailsForm(request.POST)
        if exam_details_form.is_valid():
            if exam_details.objects.filter(exam_code=request.POST['exam_code']).exists():
                messages.success(request, "Exam Code already Exist.")
                return render(request, "create_exam.html", {'exam_details_form': exam_details_form})

            elif exam_details.objects.filter(exam_name=request.POST['exam_name']).exists():
                messages.success(request, "Exam Name already Exist.")
                return render(request, "create_exam.html", {'exam_details_form': exam_details_form})

            else:
                exam_details_form_obj = exam_details_form.save(commit=False)
                exam_details_form_obj.save()

                frequency_recs = frequency_details.objects.filter(id__in=frequency_mapping_ids)

                for frequecy_rec in frequency_recs:
                    if frequecy_rec:
                        exam_details_frequency.objects.create(exam_details_name_id=exam_details_form_obj.id, frequency_details_name_id=frequecy_rec.id)

                return redirect('/school/exam_list/')
        else:
            messages.success(request, "Form have some error.")
            return redirect('/school/exam_list/')


@user_login_required
def ExamUpdate(request, exam_id):
    exam_details_recs = exam_details.objects.get(id=exam_id)
    frequency_details_recs = exam_details_frequency.objects.filter(exam_details_name=exam_details_recs)
    frequency_recs = frequency_details.objects.all()
    exam_type_recs = exam_type.objects.all()

    return render(request, "edit_exam.html",{'exam_details_recs': exam_details_recs,'frequency_recs':frequency_recs,'exam_type_recs':exam_type_recs,'frequency_details_recs':frequency_details_recs})


@user_login_required
def SaveUpdatedExam(request):
    if (request.method == 'POST'):
        # exam_details_form = ExamDetailsForm(request.POST)
        # if exam_details_form.is_valid():
            exam_id = request.POST['exam_id']
            # frequency_id = request.POST['frequency']
            exam_type_id = request.POST['exam_type']
            frequency_mapping_ids = request.POST.getlist('frequency_mapping')

            # frequency_instence = frequency_details.objects.get(id=frequency_id)
            exam_instence = exam_type.objects.get(id=exam_type_id)

            exam_details_recs = exam_details.objects.get(id=exam_id)
            if exam_details.objects.filter(~Q(id = exam_id),exam_name=request.POST['exam_name']).exists():
                messages.success(request, "Exam Name already Exist.")
                return render(request, "edit_exam.html", {'exam_details_recs': exam_details_recs})
            else:
                obj = exam_details.objects.get(pk=exam_id)
                obj.exam_code = request.POST['exam_code']
                obj.exam_name = request.POST['exam_name']
                # obj.frequency = frequency_instence
                obj.exam_type = exam_instence
                obj.save()

                # for frequency_rec in frequency_mapping_ids:
                #     if frequency_rec:
                #         exam_details_frequency.objects.filter(exam_details_name=exam_id,frequency_details_name=frequency_rec).delete()

                frequency_recs = frequency_details.objects.filter(id__in=frequency_mapping_ids)
                for frequecy_rec in frequency_recs:
                    if exam_details_frequency.objects.filter(exam_details_name_id=exam_id, frequency_details_name=frequecy_rec.id).exists():
                        pass
                    else:
                        exam_details_frequency.objects.create(exam_details_name_id=exam_id,frequency_details_name_id=frequecy_rec.id)


                messages.success(request, "One record updated successfully")
                return redirect('/school/exam_list/')
        # else:
        #     messages.success(request, "Form have some errors")
            return redirect('/school/exam_list/')


@user_login_required
def ExamDefinition(request):
    exam_details_recs = exam_details.objects.all()
    exam_type_recs = exam_type.objects.all()

    subjectDict = []
    subjectRecs = []
    optionalSubject = []
    current_acd = academic_year.objects.get(current_academic_year=1)
    ac_recs = academic_class_section_mapping.objects.filter(year_name=current_acd)
    subject_recs = mandatory_subject_mapping.objects.filter(academic_class_section_mapping__in=ac_recs)

    for subject in subject_recs:
        subjectDict.append(subject.subject_id)

    subjectDict = set(subjectDict)
    subject_data = subjects.objects.filter(id__in=subjectDict)

    for subject in subject_data:
        subjectRecs.append(subject)

    student_recs = student_details.objects.filter(academic_class_section__in=ac_recs)
    for student_rec in student_recs:
        for subject in student_rec.subject_ids.all():
            optionalSubject.append(subject)

    optionalSubject = set(optionalSubject)
    for op_subject in optionalSubject:
        subjectRecs.append(op_subject)

    # subject_recs = subjects.objects.all()
    return render(request, "exam_definition_basic.html",
                  {'exam_details_recs': exam_details_recs, 'exam_type_recs': exam_type_recs,'subject_recs':subjectRecs})

@user_login_required
def ExamDefinitionFilter(request):
    filter_data = request.POST
    if request.method == 'POST':
        request.session['filter_data'] = filter_data
        form_vals = filter_exam_data(filter_data, request)

        if form_vals['selected_subject_name'] == '':
            return render(request, "exam_definition_without_subject.html", form_vals)
        else:
            return render(request, "exam_definition.html", form_vals)

    else:
        form_vals = filter_exam_data(request.session.get('filter_data'), request)

        if form_vals['selected_subject_name'] == '':
            return render(request, "exam_definition_without_subject.html", form_vals)
        else:
            return render(request, "exam_definition.html", form_vals)


def filter_exam_data(filter_data, request):
    selected_scheme_code = filter_data.get('scheme_code')
    selected_scheme_name = filter_data.get('scheme_name')
    selected_subject_name = filter_data.get('subject_name')
    subject_recs = subjects.objects.all()

    if (selected_subject_name == ''):
        form_vals = {'selected_scheme_code':selected_scheme_code,'selected_scheme_name':selected_scheme_name,'subject_recs':subject_recs,'selected_subject_name':selected_subject_name}
        return form_vals
    subject_rec = subjects.objects.get(id=selected_subject_name)
    subject_list = subject_rec.strands_ids.all()

    form_vals = {'subject_list': subject_list,'selected_scheme_code':selected_scheme_code,'selected_scheme_name':selected_scheme_name,'selected_subject_name':subject_rec,'subject_recs':subject_recs}
    return form_vals



# @user_login_required
# def ViewExamMapping(request):
#     exam_scheme_recs = exam_scheme.objects.filter(is_submitted=True)
#     exam_mapping_recs = exam_mapping_details.objects.all()
#     year_class_section_object = academic_class_section_mapping.objects.all()
#     all_academicyr_recs = academic_year.objects.all()
#     grade_recs = grade_details.objects.filter(is_archive = False)
#     # frequency_recs = frequency_details.objects.all()
#
#     if year_class_section_object:
#         try:
#             current_academic_year = academic_year.objects.get(current_academic_year=1)
#             selected_year_id = current_academic_year.id
#             return render(request, 'view_exam_mapping.html',
#                           { "year_list": all_academicyr_recs,
#                            'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
#                             'exam_mapping_recs': exam_mapping_recs,'exam_scheme_recs':exam_scheme_recs,'grade_recs':grade_recs})
#         except academic_year.DoesNotExist:
#             return render(request, 'view_exam_mapping.html',
#                           { "year_list": all_academicyr_recs,
#                            'exam_mapping_recs': exam_mapping_recs})
#     else:
#         messages.success(request, "Academic Year Class Section Mapping Not Found.")
#         return render(request, 'exam_sub_tile.html')



# @user_login_required
# def SaveExamMapping(request):
#     if request.method == 'POST':
#         year_name = request.POST['year_name']
#         class_name = request.POST['class_name']
#         section_name = request.POST.getlist('section_name')
#         exam_scheme1 = request.POST['exam_scheme']
#         grade_name = request.POST['grade_name']
#         # frequency_name = request.POST['frequency_name']
#         subject_name = request.POST['subject_name']
#
#         sub_rec = subjects.objects.get(id=subject_name)
#         acd_recs = academic_class_section_mapping.objects.filter(
#             Q(class_name_id=class_name, year_name_id=year_name) & Q(section_name_id__in=section_name))
#
#         scheme_recs = exam_mapping_details.objects.filter(exam_scheme=exam_scheme1)
#         scheme_data = exam_scheme.objects.get(id=exam_scheme1)
#
#
#         # acd_data = exam_mapping_details.objects.filter(
#         #     Q(academic_class_section_mapping__in=acd_recs) & Q(subject_id=subject_name))
#         #
#         # for data in acd_data:
#         #     if data.exam_scheme == scheme_data:
#
#         if scheme_recs:
#             if scheme_recs[0].subject.id == long(subject_name):
#                 if acd_recs:
#                     for acd_rec in acd_recs:
#                         acd_data = exam_mapping_details.objects.filter(
#                             Q(academic_class_section_mapping__in=acd_recs) & Q(subject_id=subject_name))
#
#                         ay_subject_scheme_recs = exam_mapping_details.objects.filter(
#                             academic_class_section_mapping_id=acd_rec.id, exam_scheme=exam_scheme1,
#                             grade_name=grade_name,
#                             subject_id=subject_name)
#
#                         if not ay_subject_scheme_recs:
#                             if sub_rec.type == 'Optional':
#                                 optionalSubjectList = []
#                                 try:
#                                     student_recs = student_details.objects.filter(academic_class_section=acd_rec.id,
#                                                                                   is_active=True)
#                                     for student_rec in student_recs:
#                                         for subject_recs in student_rec.subject_ids.all():
#                                             optionalSubjectList.append(subject_recs.id)
#
#                                                 if long(subject_name) in optionalSubjectList:
#                                                     data = exam_mapping_details.objects.filter(
#                                                         academic_class_section_mapping_id=acd_rec.id, exam_scheme=exam_scheme1,
#                                                         grade_name=grade_name,
#                                                         subject_id=subject_name)
#                                                     if data:
#                                                         messages.warning(request,
#                                                                          "Record Already Mapped, Try With New Records. ")
#                                                         pass
#                                                     else:
#                                                         exam_mapping_details.objects.create(
#                                                             academic_class_section_mapping_id=acd_rec.id,
#                                                             exam_scheme_id=exam_scheme1, grade_name_id=grade_name,
#                                                              subject_id=subject_name)
#                                                 else:
#                                                     pass
#                                             except:
#                                                 messages.success(request, "Record Not Saved. Some Error Occurred, Try Again. ")
#                                         else:
#                                             try:
#                                                 mandatoray_subject_recs = mandatory_subject_mapping.objects.filter(
#                                                     academic_class_section_mapping_id=acd_rec.id, subject_id=subject_name)
#
#                                                 if mandatoray_subject_recs:
#                                                     data = exam_mapping_details.objects.filter(
#                                                         academic_class_section_mapping_id=acd_rec.id,
#                                                         exam_scheme=exam_scheme1, grade_name=grade_name,
#
#                                                         subject_id=subject_name)
#                                                     if data:
#                                                         messages.warning(request,
#                                                                          "Record Already Mapped, Try With New Records. ")
#                                                         pass
#                                                     else:
#                                                         exam_mapping_details.objects.create(
#                                                             academic_class_section_mapping_id=acd_rec.id,
#                                                             exam_scheme_id=exam_scheme1, grade_name_id=grade_name,
#
#                                                 subject_id=subject_name)
#                                     else:
#                                         pass
#                                 except:
#                                     messages.warning(request, "Record Not Saved. Some Error Occurred. Try Again. ")
#                         else:
#                             messages.warning(request,
#                                              "Record Already Mapped For This Academic, Subject, Scheme And Grade.")
#             else:
#                 messages.warning(request, "A Scheme Can Be Mapped With Only One Subject. Please Try Again. ")
#         else:
#             if acd_recs:
#                 for acd_rec in acd_recs:
#                     if sub_rec.type == 'Optional':
#                         optionalSubjectList = []
#                         try:
#                             student_recs = student_details.objects.filter(academic_class_section=acd_rec.id,
#                                                                           is_active=True)
#                             for student_rec in student_recs:
#                                 for subject_recs in student_rec.subject_ids.all():
#                                     optionalSubjectList.append(subject_recs.id)
#
#                             if long(subject_name) in optionalSubjectList:
#                                 data = exam_mapping_details.objects.filter(
#                                     academic_class_section_mapping_id=acd_rec.id, exam_scheme=exam_scheme1,
#                                     grade_name=grade_name, subject_id=subject_name)
#                                 if data:
#                                     messages.warning(request, "Record Already Mapped, Try With New Records. ")
#                                     pass
#                                 else:
#                                     exam_mapping_details.objects.create(
#                                         academic_class_section_mapping_id=acd_rec.id,
#                                         exam_scheme_id=exam_scheme1,
#                                         grade_name_id=grade_name,
#
#                                         subject_id=subject_name)
#                             else:
#                                 pass
#                         except:
#                             messages.success(request, "Record Not Saved. Some Error Occurred, Try Again. ")
#                     else:
#                         try:
#                             mandatoray_subject_recs = mandatory_subject_mapping.objects.filter(
#                                 academic_class_section_mapping_id=acd_rec.id, subject_id=subject_name)
#
#                             if mandatoray_subject_recs:
#                                 data = exam_mapping_details.objects.filter(
#                                     academic_class_section_mapping_id=acd_rec.id,
#                                     exam_scheme=exam_scheme1, grade_name=grade_name,
#
#                                     subject_id=subject_name)
#                                 if data:
#                                     messages.warning(request, "Record Already Mapped, Try With New Records. ")
#                                     pass
#                                 else:
#                                     exam_mapping_details.objects.create(
#                                         academic_class_section_mapping_id=acd_rec.id,
#                                         exam_scheme_id=exam_scheme1,
#                                         grade_name_id=grade_name,
#
#                                         subject_id=subject_name)
#                             else:
#                                 pass
#                         except:
#                             messages.warning(request, "Record Not Saved. Some Error Occurred. Try Again. ")
#
#     return redirect('/school/view_exam_mapping/')


@user_login_required
def SaveExamDetails(request):
    try:
        if request.method == 'POST':
            scheme_code  = request.POST.get('scheme_code')
            scheme_name1 = request.POST.get('scheme_name')
            id_exam_name = request.POST.get('id_exam_name')
            id_exam_type = request.POST.get('id_exam_type')
            submit_rec = request.POST.get('submit_rec')
            subject_name = request.POST.get('subject_name')

            val_dict =  json.loads(request.POST['data'])
            scheme_obj = exam_scheme.objects.create(exam_scheme_code=scheme_code, exam_scheme_name=scheme_name1,exam_id_id=id_exam_name,exam_type_id_id=id_exam_type,subject_exam_name_id=subject_name)

            for data in val_dict:
                strand_data = exam_strand.objects.create(strand_name=data['strand'])
                scheme_obj.stand_ids.add(strand_data.id)

                for substrand in data['substrand']:
                    sub_str = exam_sub_strand.objects.create(sub_strand_name = substrand['substrand_name'])
                    strand_data.sub_strand_ids.add(sub_str.id)

                    for recs in substrand['topic']:
                        topic_name1 = recs['topic_name']
                        topic_table = exam_topic.objects.create(topic_name=topic_name1)
                        sub_str.topic_ids.add(topic_table.id)

                        for subtopic_rec in recs['subtopic']:
                            sub_topic_name = subtopic_rec['value']
                            mark_val = subtopic_rec['mark']
                            sub_topic_table = exam_sub_topic.objects.create(name = sub_topic_name, mark = mark_val)
                            topic_table.sub_topic_ids.add(sub_topic_table.id)

            #if submit_rec == 'true':
            exam_scheme.objects.filter(id=scheme_obj.id).update(is_submitted=True)
            messages.success(request, "Record Saved Successfully.")
    except:
        messages.warning(request, "Some Error Occurred. Please Try again.")
    # return redirect('/school/exam_definition_list/')
    return HttpResponse(json.dumps( {'success': " Exam Added saved successfully."}))


@user_login_required
def ViewExamDetails(request):
    finalDict = []
    exam_id = request.GET.get('exam_id', None)
    exam_name_obj = exam_details.objects.get(id=exam_id)
    exam_name = exam_name_obj.exam_name
    finalDict.append(exam_name)
    return JsonResponse(finalDict, safe=False)

@user_login_required
@user_permission_required('assessment.can_view_view_exam_definition_list', '/school/home/')
def ExamDefinitionList(request):
    exam_recs = exam_scheme.objects.all()
    return render(request, "exam_definition_list.html", {'exam_recs': exam_recs})

@user_login_required
def EditExamDefinition(request, scheme_id):

    option_count = itertools.count(1)
    iterator_color= make_incrementor(0)
    iterator_mark= make_incrementor(0)
    iterator_subtopic = make_incrementor(0)

    iterator_topic= make_incrementor(0)
    iterator_topic_add = make_incrementor(0)
    iterator_topic_rem = make_incrementor(0)
    iterator_substrand = make_incrementor(0)
    iterator_substrand_add = make_incrementor(0)
    iterator_substrand_rem = make_incrementor(0)
    color_count= make_incrementor(0)
    mark_count= make_incrementor(0)
    subtopic_count= make_incrementor(0)
    exam_details_recs = exam_details.objects.all()
    exam_type_recs = exam_type.objects.all()

    exam_rec = exam_scheme.objects.get(id=scheme_id)

    subject_check = exam_rec.subject_exam_name

    substrand_list = []
    data = []

    edit_scheme = exam_scheme_academic_frequency_mapping.objects.filter(scheme_mapping__exam_scheme_id=scheme_id).exists()

    name_edit_flag = exam_scheme_academic_frequency_mapping.objects.filter(scheme_mapping__exam_scheme_id=scheme_id,is_submitted=True).exists()

    if not subject_check==None:
        subject_rec = subjects.objects.get(id=subject_check.id)
        subject_list = subject_rec.strands_ids.all()

        for obj in subject_rec.strands_ids.all():
            # rec = subject_strands.objects.get(id=obj.id)
            for sub_obj in obj.substrands_ids.all():
                substrand_list.append(sub_obj)

        view_val = {
            'edit_scheme':edit_scheme,
            'name_edit_flag':name_edit_flag,
            'iterator_color': iterator_color,
            'iterator_mark': iterator_mark,
            'iterator_subtopic': iterator_subtopic,

            'iterator_topic': iterator_topic,
            'iterator_topic_add': iterator_topic_add,
            'iterator_topic_rem': iterator_topic_rem,

            'iterator_substrand_add': iterator_substrand_add,
            'iterator_substrand_rem': iterator_substrand_rem,
            'iterator_substrand': iterator_substrand,

            'color_count': color_count,
            'mark_count': mark_count,
            'subtopic_count': subtopic_count,

            'exam_rec': exam_rec,
            'exam_details_recs': exam_details_recs,
            'exam_type_recs': exam_type_recs,
            'submitted': exam_rec.is_submitted,

            'option_count': option_count,
            'st_top_count': make_incrementor(0),
            'st_top_count1': make_incrementor(0),
            'st_top_count2': make_incrementor(0),
            'subject_list':subject_list,
            'subject_rec':subject_rec,
            'substrand_list':substrand_list,
        }

        return render(request, "edit_exam_definition_subject.html", view_val)

    view_val = {
        'edit_scheme': edit_scheme,
        'name_edit_flag': name_edit_flag,
        'iterator_color':iterator_color,
        'iterator_mark': iterator_mark,
        'iterator_subtopic': iterator_subtopic,

        'iterator_topic':iterator_topic,
        'iterator_topic_add':iterator_topic_add,
        'iterator_topic_rem':iterator_topic_rem,

        'iterator_substrand_add':iterator_substrand_add,
        'iterator_substrand_rem':iterator_substrand_rem,
        'iterator_substrand':iterator_substrand,

        'color_count':color_count,
        'mark_count':mark_count,
        'subtopic_count':subtopic_count,

        'exam_rec': exam_rec,
        'exam_details_recs': exam_details_recs,
        'exam_type_recs': exam_type_recs,
        'submitted' : exam_rec.is_submitted,

        'option_count': option_count,
        'st_top_count': make_incrementor(0),
        'st_top_count1': make_incrementor(0),
        'st_top_count2': make_incrementor(0),
    }

    return render(request, "edit_exam_definition.html", view_val)

def SaveEditExamDefinition(request):
    scheme_id = request.POST.get('scheme_id')
    stand_id = request.POST.get('stand_id')
    submit = request.POST.get('submit')
    scheme_name = request.POST.get('scheme_name')


    val_dict = json.loads(request.POST['data'])

    scheme_recs = exam_scheme.objects.get(id = scheme_id)

    if scheme_name:
        exam_scheme.objects.filter(id = scheme_id).update(exam_scheme_name=scheme_name)

    try:
        # if submit == 'true':
        for rec in val_dict:
            for data in rec:
                if not data['stand_id']:
                    strand_data = exam_strand.objects.create(strand_name=data['strand'])
                    scheme_recs.stand_ids.add(strand_data.id)
                else:
                    strand_data = exam_strand.objects.get(id=data['stand_id'])
                    strand_data.strand_name = data['strand']
                    strand_data.save()

                for substrand in data['substrand']:
                    sub_str_create = None
                    try:
                        sub_str = exam_sub_strand.objects.get(id=substrand['substrand_id'])
                    except:
                        sub_str_create = exam_sub_strand.objects.create(sub_strand_name=substrand['substrand_name'])

                    if not sub_str_create:
                        sub_str.sub_strand_name = substrand['substrand_name']
                        sub_str.save()
                    else:
                        strand_data.sub_strand_ids.add(sub_str_create.id)

                    for recs in substrand['topic']:
                        topic_name1 = recs['topic_name']
                        topic_table_create = None
                        try:
                            topic_table = exam_topic.objects.get(id=recs['topic_id'])
                        except:
                            topic_table_create = exam_topic.objects.create(topic_name=topic_name1)

                        if not topic_table_create:
                            topic_table.topic_name = topic_name1
                            topic_table.save()
                        else:
                            if not sub_str_create:
                                sub_str.topic_ids.add(topic_table_create.id)
                            else:
                                sub_str_create.topic_ids.add(topic_table_create.id)

                        for subtopic_rec in recs['subtopic']:
                            sub_topic_table_create = None
                            sub_topic_name = subtopic_rec['value']
                            mark_val = subtopic_rec['mark']
                            # color_val = subtopic_rec['color']
                            try:
                                sub_topic_table = exam_sub_topic.objects.get(id=subtopic_rec['subtopic_ids'])
                            except:
                                sub_topic_table_create = exam_sub_topic.objects.create(name=sub_topic_name, mark=mark_val)
                            if not sub_topic_table_create:
                                sub_topic_table.name = sub_topic_name
                                sub_topic_table.mark = mark_val
                                # sub_topic_table.color=color_val
                                sub_topic_table.save()
                            else:
                                if not topic_table_create:
                                    topic_table.sub_topic_ids.add(sub_topic_table_create.id)
                                else:
                                    topic_table_create.sub_topic_ids.add(sub_topic_table_create.id)
        exam_scheme.objects.filter(id=scheme_recs.id).update(is_submitted=True)
        messages.success(request, "Record Submitted Successfully.")

        # if submit == 'false':
        #     for data in val_dict:
        #         if not stand_id:
        #             strand_data = exam_strand.objects.create(strand_name=data['strand'])
        #             scheme_recs.stand_ids.add(strand_data.id)
        #         else:
        #             strand_data = exam_strand.objects.get(id=stand_id)
        #             strand_data.strand_name = data['strand']
        #             strand_data.save()
        #
        #         for substrand in data['substrand']:
        #             sub_str_create = None
        #             try:
        #                 sub_str = exam_sub_strand.objects.get(id=substrand['substrand_id'])
        #             except:
        #                 sub_str_create = exam_sub_strand.objects.create(sub_strand_name=substrand['substrand_name'])
        #
        #             if not sub_str_create:
        #                 sub_str.sub_strand_name = substrand['substrand_name']
        #                 sub_str.save()
        #             else:
        #                 strand_data.sub_strand_ids.add(sub_str_create.id)
        #
        #             for recs in substrand['topic']:
        #                 topic_name1 = recs['topic_name']
        #                 topic_table_create = None
        #                 try:
        #                     topic_table = exam_topic.objects.get(id=recs['topic_id'])
        #                 except:
        #                     topic_table_create = exam_topic.objects.create(topic_name=topic_name1)
        #
        #                 if not topic_table_create:
        #                     topic_table.topic_name = topic_name1
        #                     topic_table.save()
        #                 else:
        #                     if not sub_str_create:
        #                         sub_str.topic_ids.add(topic_table_create.id)
        #                     else:
        #                         sub_str_create.topic_ids.add(topic_table_create.id)
        #
        #                 for subtopic_rec in recs['subtopic']:
        #                     sub_topic_table_create = None
        #                     sub_topic_name = subtopic_rec['value']
        #                     mark_val = subtopic_rec['mark']
        #                     # color_val = subtopic_rec['color']
        #                     try:
        #                         sub_topic_table = exam_sub_topic.objects.get(id=subtopic_rec['subtopic_ids'])
        #                     except:
        #                         sub_topic_table_create = exam_sub_topic.objects.create(name=sub_topic_name, mark=mark_val)
        #                     if not sub_topic_table_create:
        #                         sub_topic_table.name = sub_topic_name
        #                         sub_topic_table.mark = mark_val
        #                         # sub_topic_table.color=color_val
        #                         sub_topic_table.save()
        #                     else:
        #                         if not topic_table_create:
        #                             topic_table.sub_topic_ids.add(sub_topic_table_create.id)
        #                         else:
        #                             topic_table_create.sub_topic_ids.add(sub_topic_table_create.id)
        #     messages.success(request, "Record Updated Successfully.")
    except:
        messages.warning(request, "Some Error Occurred... Please Try Again")

    # return HttpResponse(json.dumps(scheme_recs.to_dict()), content_type='application/json')
    return redirect('/school/edit_exam_definition/'+scheme_id)



@user_login_required
def ViewExamSchemeSubject(request):
    finalDict = []
    subject_flag = False
    id_exam_scheme = request.GET.get('id_exam_scheme', None)
    id_academic_year = request.GET.get('id_academic_year', None)
    # class_val = json.loads(request.POST.get('class_name_rec'))
    # class_val = request.POST.GET.getlist('class_name_rec')

    # id_exam_scheme = request.POST.get('id_exam_scheme', None)
    # id_academic_year = request.POST.get('id_academic_year', None)
    # # class_val = json.loads(request.POST.get('class_name_rec'))




    exam_scheme_rec = exam_scheme.objects.get(id=id_exam_scheme)
    try:
        ab = {'id': exam_scheme_rec.subject_exam_name.id, 'value': exam_scheme_rec.subject_exam_name.subject_name}
        finalDict.append(ab)
        subject_flag = True
        finalDict.append(subject_flag)
    except:

        subjectDict = []
        # subjectRecs = []
        optionalSubject = []

        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        subject_recs = mandatory_subject_mapping.objects.filter(
            academic_class_section_mapping__year_name=selected_year_id)

        ab = {}
        for subject in subject_recs:
            # ab={'id':subject.id, 'subject_name': subject.subject.subject_name}
            subjectDict.append(subject.subject_id)
        subjectDict = set(subjectDict)
        subject_data = subjects.objects.filter(id__in=subjectDict)
        for subject in subject_data:
            ab = {'id': subject.id, 'value': subject.subject_name}
            finalDict.append(ab)

        student_recs = student_details.objects.filter(academic_class_section__year_name=selected_year_id)
        for student_rec in student_recs:
            for subject in student_rec.subject_ids.all():
                optionalSubject.append(subject)

        optionalSubject = set(optionalSubject)

        for subject in optionalSubject:
            ab = {'id': subject.id, 'value': subject.subject_name}
            finalDict.append(ab)
        finalDict.append(subject_flag)

    return JsonResponse(finalDict,safe=False)

def ViewGetScheme(request):
    finalDict = []
    subject_flag = False
    class_name = request.POST.get('class_name', None)
    subject_name = json.loads(request.POST.get('subject_name', None))
    academic_year = request.POST.get('academic_year', None)

    exam_scheme_rec = exam_scheme.objects.filter(subject_exam_name_id__in = subject_name)
    if not exam_scheme_rec:
        exam_scheme_rec = exam_scheme.objects.filter(subject_exam_name=None)

    for rec in exam_scheme_rec:
        ab = {'id': rec.id, 'scheme': rec.exam_scheme_name}
        finalDict.append(ab)
    return JsonResponse(finalDict,safe=False)

@user_login_required
def DeleteExamDetails(request, exam_id):
    try:
        exam_details.objects.filter(id = exam_id).delete()
        messages.success(request, "record deleted successfully")
    except:
        messages.warning(request, "Can't delete exam relation exists somewhere.")
    return redirect('/school/exam_list/')


@user_login_required
def DeleteExamSchemeMapping(request):
    if request.method == 'POST':
        try:
            check=request.POST.getlist('check')
            for rec in check:
                id = rec.split(',')[0]
                # print "I am in Delete Mapping function"
                exam_mapping_details.objects.filter(id=id).delete()
        except:
                messages.warning(request, "Record Not Deleted.. This May Be Used In Other Tables, Delete Them First And Try Again. ")
    return redirect('/school/view_exam_mapping/')




# @user_login_required
# def ExportExamSchemeRecords(request):
#     try:
#
#         exam_mapping_rec = exam_mapping_details.objects.all()
#         column_names = ['Year Name','Class','Section','Subject','Exam Scheme Name','Grading Scheme']
#         rows = exam_mapping_rec.values_list('academic_class_section_mapping__year_name__year_name','academic_class_section_mapping__class_name__class_name','academic_class_section_mapping__section_name__section_name','subject__subject_name','exam_scheme__exam_scheme_name','grade_name__grade_name')
#
#         return export_users_xls('exam_mapping', column_names, rows)
#
#     except:
#         messages.warning(request, "Some Error Occurred...")
#     return redirect('/school/view_exam_mapping/')




@user_login_required
def ExamDeleteSubTopic(request):
    scheme_id = request.POST.get('scheme_id')
    topic_id = request.POST.get('topic_id')
    subtopic_id = request.POST.get('subtopic_id')
    topic_recs = exam_topic.objects.get(id=topic_id)
    topic_recs.sub_topic_ids.remove(subtopic_id)
    exam_sub_topic.objects.filter(id= subtopic_id).delete()
    return redirect('/school/edit_exam_definition/'+scheme_id)


@user_login_required
def ExamDeleteTopic(request):
    scheme_id = request.POST.get('scheme_id')
    substrand_id = request.POST.get('substrand_id')
    topic_id = request.POST.get('topic_id')
    substrand_rec = exam_sub_strand.objects.get(id=substrand_id)
    substrand_rec.topic_ids.remove(topic_id)
    topic_recs = exam_topic.objects.get(id=topic_id)

    for subtopic in topic_recs.sub_topic_ids.all():
        sub_top_id = subtopic.id
        exam_sub_topic.objects.filter(id = sub_top_id).delete()
        topic_recs.sub_topic_ids.remove(sub_top_id)

    exam_topic.objects.filter(id=topic_id).delete()

    return redirect('/school/edit_exam_definition/'+scheme_id)


@user_login_required
def ExamDeleteSubStrand(request):
    scheme_id = request.POST.get('scheme_id')
    stand_id = request.POST.get('strand_id')
    substrand_id = request.POST.get('substrand_id')

    strand_rec = exam_strand.objects.get(id = stand_id)
    strand_rec.sub_strand_ids.remove(substrand_id)

    substrand_recs = exam_sub_strand.objects.get(id=substrand_id)

    for topic_recs in substrand_recs.topic_ids.all():
        topic_rec = exam_topic.objects.get(id=topic_recs.id)
        topic_rec.sub_topic_ids.remove(substrand_recs.id)

        for subtopic in topic_rec.sub_topic_ids.all():
            sub_top_id = subtopic.id
            exam_sub_topic.objects.filter(id=sub_top_id).delete()
            topic_rec.sub_topic_ids.remove(sub_top_id)
        exam_topic.objects.filter(id=topic_rec.id).delete()
    exam_sub_strand.objects.filter(id=substrand_id).delete()

    # scheme_recs = exam_scheme.objects.get(id=scheme_id)
    # scheme_recs.stand_ids.remove(stand_id)
    return redirect('/school/edit_exam_definition/'+scheme_id)

# @user_login_required
# def ExamDeleteStrand(request):
#     scheme_id = request.POST.get('scheme_id')
#     stand_id = request.POST.get('stand_id')
#     scheme_recs = exam_scheme.objects.get(id=scheme_id)
#     scheme_recs.stand_ids.remove(stand_id)
#     return redirect('/school/edit_exam_definition/'+scheme_id)

@user_login_required
def ExamDeleteStrand(request):
    scheme_id = request.POST.get('scheme_id')
    stand_id = request.POST.get('stand_id')
    scheme_recs = exam_scheme.objects.get(id=scheme_id)
    strand_obj = exam_strand.objects.get(id=stand_id)

    scheme_recs.stand_ids.remove(strand_obj)

    for sub_strand_rec in strand_obj.sub_strand_ids.all():
        sub_strand_obj = exam_sub_strand.objects.get(id=sub_strand_rec.id)

        for topic_rec in sub_strand_obj.topic_ids.all():
            topic_obj = exam_topic.objects.get(id=topic_rec.id)
            sub_strand_obj.topic_ids.remove(topic_rec)

            for sub_topic_rec in topic_obj.sub_topic_ids.all():
                sub_top_id = sub_topic_rec.id
                exam_sub_topic.objects.filter(id=sub_top_id).delete()
                topic_rec.sub_topic_ids.remove(sub_top_id)

            exam_topic.objects.filter(id=topic_obj.id).delete()
            strand_obj.sub_strand_ids.remove(sub_strand_rec)
        exam_sub_strand.objects.filter(id=sub_strand_rec.id).delete()
    exam_strand.objects.filter(id=stand_id).delete()

    return redirect('/school/edit_exam_definition/'+scheme_id)

# def DeleteSubStrand(request):
#     # print "In function  DeleteSubStrand->"
#     scheme_id = request.POST.get('scheme_id')
#     stand_id = request.POST.get('strand_id')
#     substrand_id = request.POST.get('substrand_id')


@user_login_required
def EnterExamSchemeMark(request):
    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    scheme_recs = exam_scheme.objects.all()
    frequency_recs = frequency_details.objects.all()

    current_academic_year = None
    selected_year_id = None

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
    except:
        messages.error(request, "Please add academic year first.")
        return render(request, "add_exam_scheme_marks.html", {'class_list': all_class_recs, 'section_list': all_section_recs, "year_list": all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})
    return render(request, "add_exam_scheme_marks.html", {'filter_flag':True,'class_list': all_class_recs, 'section_list': all_section_recs, "year_list": all_academicyr_recs,'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,  'scheme_recs': scheme_recs,'frequency_recs':frequency_recs})


def ViewGetScheme(request):
    finalDict = []
    class_name = request.POST.get('class_name', None)
    subject_name = json.loads(request.POST.get('subject_name', None))
    academic_year = request.POST.get('academic_year', None)

    exam_scheme_rec = exam_scheme.objects.filter(subject_exam_name_id__in = subject_name)
    if not exam_scheme_rec:
        exam_scheme_rec = exam_scheme.objects.filter(subject_exam_name=None)

    for rec in exam_scheme_rec:
        ab = {'id': rec.id, 'scheme': rec.exam_scheme_name}
        finalDict.append(ab)
    return JsonResponse(finalDict,safe=False)

def GetExamName(request):
    finalDict = []
    class_name = request.POST.get('class_name', None)
    subject_name = request.POST.get('subject_name', None)
    year_name = request.POST.get('academic_year', None)
    exam_dict = {}
    try:
        exam_scheme_rec = exam_scheme_acd_mapping_details.objects.get(year_id = year_name, class_obj_id = class_name, subject_id = subject_name)
        for rec in exam_scheme_rec.exam_name_mapping_ids.filter(is_archived=False):
            exam_dict = {'id': rec.exam_name.id, 'scheme': rec.exam_name.exam_name}
            finalDict.append(exam_dict)
        return JsonResponse(finalDict,safe=False)
    except:
        return JsonResponse(finalDict, safe=False)

def GetExamFrequency(request):
    finalDict = []
    exam_name = request.POST.get('exam_name', None)
    exam_frequency_rec = exam_details_frequency.objects.filter(exam_details_name_id = exam_name)

    for rec in exam_frequency_rec:
        ab = {'id': rec.frequency_details_name.id, 'frequency': rec.frequency_details_name.frequency_name}
        finalDict.append(ab)
    return JsonResponse(finalDict,safe=False)

@user_login_required
def ViewMappedExamFrequency(request):
    year_name = request.POST['year_name']
    class_name = request.POST['class_name']
    section_name = request.POST['section_name']
    scheme = request.POST['scheme_name']
    acd_ids = []
    frequencyList = []
    dict = {}
    academic_rec = academic_class_section_mapping.objects.get(year_name=year_name, section_name=section_name, class_name=class_name)
    scheme_academic_recs = exam_mapping_details.objects.filter(academic_class_section_mapping_id = academic_rec.id, exam_scheme_id = scheme )
    for scheme_academic_rec in scheme_academic_recs:
        acd_ids.append(scheme_academic_rec.frequency_id)
    acd_ids = set(acd_ids)
    frequency_recs = frequency_details.objects.filter(id__in = acd_ids)
    for frequency_rec in frequency_recs:
        dict={'id':frequency_rec.id, 'class_name': frequency_rec.frequency_name}
        frequencyList.append(dict)
    return JsonResponse(frequencyList,safe=False)

@user_login_required
def ExamMappedScheme(request):
    schemeList = []
    schemeIds = []
    academic_year = request.GET.get('academic_year', None)
    class_name = request.GET.get('class_name', None)
    section_name = request.GET.get('section_name', None)

    acd_mapping_rec = academic_class_section_mapping.objects.get(class_name_id=class_name, section_name_id=section_name,year_name_id=academic_year)
    scheme_recs = exam_mapping_details.objects.filter(academic_class_section_mapping_id = acd_mapping_rec.id)
    for scheme_rec in scheme_recs:
        schemeIds.append(scheme_rec.exam_scheme.id)
    schemeIds = set(schemeIds)

    schemes = exam_scheme.objects.filter(id__in = schemeIds)
    dict = {}
    for schem_data in schemes:
        dict={'id':schem_data.id, 'scheme_name': schem_data.exam_scheme_name}
        schemeList.append(dict)
    return JsonResponse(schemeList,safe=False)



@user_login_required
def ExamSchemeStudentList(request):
    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    skill_excel_form = SkillExcelForm()
    round_rec = assessment_round_master.objects.all()[0]
    round_select = round_rec.select
    round_decimal_place = round_rec.decimal_place

    # filtered_class_ids = reported_exam_class_sub_mapping.objects.all().values_list('class_name',flat=1).distinct()
    # all_class_recs = class_details.objects.filter(id__in = filtered_class_ids)



    if request.POST:
        request.session['exam_add_data'] = request.POST
        selected_year_id = request.POST.get('year_name')  # It will be used in HTML page as a value of selected Class
        selected_class_id = request.POST.get('class_name')  # It will be used in HTML page as a value of selected Class
        selected_section_id = request.POST.get('section_name')  # It will be used in HTML page as a value of selected Section
        selected_subject_id = request.POST.get('subject_name')  # It will be used in HTML page as a value of selected Section
        selected_copy_from_id = request.POST.get('copy_from')
        exam_name = request.POST.get('exam_name')
        frequency_name = request.POST.get('frequency_name')
    else:
        selected_year_id = request.session['exam_add_data'].get('year_name')  # It will be used in HTML page as a value of selected Class
        selected_class_id = request.session['exam_add_data'].get('class_name')  # It will be used in HTML page as a value of selected Class
        selected_section_id = request.session['exam_add_data'].get('section_name')  # It will be used in HTML page as a value of selected Section
        selected_subject_id = request.session['exam_add_data'].get('subject_name')  # It will be used in HTML page as a value of selected Section
        selected_copy_from_id = request.session['exam_add_data'].get('copy_from')
        exam_name = request.session['exam_add_data'].get('exam_name')
        frequency_name = request.session['exam_add_data'].get('frequency_name')
    selected_year_rec = academic_year.objects.get(id=selected_year_id)
    selected_year_name = selected_year_rec.year_name

    selected_class_rec = class_details.objects.get(id=selected_class_id)
    selected_class_name = selected_class_rec.class_name

    selected_section_rec = sections.objects.get(id=selected_section_id)
    selected_section_name = selected_section_rec.section_name

    all_section_recs = current_class_mapped_sections(selected_year_rec, selected_class_rec, request.user)
    all_class_recs = mapped_user_classes(selected_year_rec, request)

    selected_subject_rec = subjects.objects.get(id=selected_subject_id)
    selected_subject_name = selected_subject_rec.subject_name
    selected_exam_name = exam_details.objects.get(id=exam_name)

    frequency_data = frequency_details.objects.get(id=frequency_name)

    frequency_recs = frequency_details.objects.all()

    assessment_conf_names = assessment_configration_master.objects.all()
    na_flag = assessment_configration_master.objects.filter(short_name='NA').exists()
    # conf_list = ['Ab','NA']
    conf_list = []
    for conf in assessment_conf_names:
        conf_list.append(str(conf.short_name))

    entered_value ='0-9.'
    for c in conf_list: entered_value = str(c) + str(entered_value)



    try:
        exam_scheme_rec = exam_scheme_acd_mapping_details.objects.get(year_id=selected_year_id, class_obj_id=selected_class_id, subject_id=selected_subject_id)
        archive_record_count = exam_scheme_rec.exam_name_mapping_ids.filter(exam_name_id=exam_name).count()
        exam_scheme_subdetails_obj = exam_scheme_rec.exam_name_mapping_ids.get(exam_name_id=exam_name,is_archived=False)
        scheme_rec = exam_scheme_subdetails_obj.exam_scheme
        scheme_id = scheme_rec.id
    except:
        if archive_record_count > 0:
            messages.warning(request, "New Mapping Not Found for The Selected Class And Section")
            return redirect('/school/enter_scheme_mark/')
        messages.warning(request, "Mapping Not Found for Selected Subject")
        return redirect('/school/enter_exam_scheme_mark/')

    selected_scheme = exam_scheme.objects.get(id =scheme_id)
    scheme_mapped_flag = False

    if selected_scheme.subject_exam_name:
        scheme_mapped_flag = True

    subtopics_recs = []

    subtopics_recs = []
    subtopic_recs = []
    list = []
    sub_strand_list = []
    filter_flag = True

    sub_strand_dict_list = []
    sub_strand_dict_id = []
    topic_dict_id = []
    topic_dict_list = []

    for strand_rec in selected_scheme.stand_ids.all():
        for sub_strand_rec in strand_rec.sub_strand_ids.all():
            sub_strand_dict = {}
            sub_strand_temp_dict = {}
            if scheme_mapped_flag:
                sub_strand_temp_dict['sub_strand_name'] = subject_substrands.objects.get(
                    id=sub_strand_rec.sub_strand_name).substrands_name
            else:
                sub_strand_temp_dict['sub_strand_name'] = sub_strand_rec.sub_strand_name

            sub_strand_temp_dict['id'] = sub_strand_rec.id
            sub_strand_list.append(sub_strand_temp_dict)
            if sub_strand_rec.id not in sub_strand_dict_id:
                sub_strand_dict_id.append(sub_strand_rec.id)
                if scheme_mapped_flag:
                    sub_strand_dict['sub_strand_name'] = subject_substrands.objects.get(
                        id=sub_strand_rec.sub_strand_name).substrands_name
                    sub_strand_dict['topic_count'] = 0
                else:
                    sub_strand_dict['sub_strand_name'] = sub_strand_rec.sub_strand_name
                    sub_strand_dict['topic_count'] = 0

            for topic_rec in sub_strand_rec.topic_ids.all():
                topic_dict = {}
                if topic_rec.id not in topic_dict_id:
                    topic_dict['topic_name'] = topic_rec.topic_name
                    topic_dict['subtopic_count'] = topic_rec.sub_topic_ids.all().count()
                    topic_dict_id.append(topic_rec.id)
                    topic_dict_list.append(topic_dict)
                    sub_strand_dict['topic_count'] += topic_rec.sub_topic_ids.all().count()
            sub_strand_dict_list.append(sub_strand_dict)



    for starand in selected_scheme.stand_ids.all():
        for sub_strand_recs in starand.sub_strand_ids.all():
            if sub_strand_recs:
                for topic_recs in sub_strand_recs.topic_ids.all():
                    if topic_recs:
                        for subtopics in topic_recs.sub_topic_ids.all():
                            subtopics_recs.append(subtopics)

    total_mark = 0
    for subtopic_rec in subtopics_recs:
        total_mark = int(subtopic_rec.mark) + total_mark

    year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,section_name=selected_section_id, class_name=selected_class_id)
    mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(scheme_mapping_id = exam_scheme_subdetails_obj.id ,frequency_id = frequency_name,academic_mapping_id = year_class_section_obj.id,is_archived=False)
    grade_colors = grade_sub_details.objects.filter(grade=exam_scheme_subdetails_obj.grade_name)

    if mark_recs:
        messages.warning(request,"Mark already entered, Update the entered marks.")
        return redirect('/school/update_entered_exam_scheme_mark/' + str(mark_recs[0].id))

    if selected_copy_from_id:
        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id, section_name=selected_section_id,class_name=selected_class_id)
        exam_scheme_rec = exam_scheme_acd_mapping_details.objects.filter(year_id=selected_year_id,class_obj_id=selected_class_id, subject_id=selected_subject_id)
        rec = exam_scheme_rec[0].exam_name_mapping_ids.filter(exam_name=exam_name,is_archived=False)[0]
        exam_scheme_rec = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping_id=year_class_section_obj.id,scheme_mapping_id=rec.id,frequency_id=selected_copy_from_id,is_archived=False)

        if exam_scheme_rec:
            scheme_rec = exam_scheme_academic_frequency_mapping.objects.get(academic_mapping_id=year_class_section_obj.id,scheme_mapping_id=exam_scheme_rec[0].scheme_mapping.id,frequency_id=selected_copy_from_id,is_archived=False)
            copy_from_data = frequency_details.objects.get(id=selected_copy_from_id)
            academic_frequency_rec = exam_scheme_academic_frequency_mapping.objects.get(id=scheme_rec.id)
            subtopics_recs = []
            subtopic_recs = []
            list = []
            grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)
            for data in academic_frequency_rec.student_mark_ids.all():
                for rec in data.subtopic_ids.all():
                    if rec.subtopic.id not in list:
                        list.append(rec.subtopic.id)
                        subtopic_recs.append(rec.subtopic)

            form_vals = {
                'total_mark': total_mark,
                'conf_list': conf_list,
                'entered_value': entered_value,
                'Selected_Section_Id': selected_section_id,
                'selected_section_name': selected_section_name,
                'selected_section': selected_section_rec,
                'section_list': all_section_recs,
                'Selected_Class_Id': selected_class_id,
                'selected_class_name': selected_class_name,
                'selected_class': selected_class_rec,
                'class_list': all_class_recs,
                'Selected_Year_Id': selected_year_id,
                'selected_year_name': selected_year_name,
                'selected_year': selected_year_rec,
                'year_list': all_academicyr_recs,
                'frequency_recs': frequency_recs,
                'frequency_data': frequency_data,
                'subtopics_recs': subtopics_recs,
                'grade_colors': grade_colors,
                'selected_subject_id': selected_subject_id,
                'selected_subject_name': selected_subject_rec,

                'academic_frequency_rec': academic_frequency_rec,
                'subtopic_data': subtopic_recs,
                'copy_from_data': copy_from_data,
                'exam_rec': exam_scheme_subdetails_obj.exam_name,

                'conf_dict_list': assessment_conf_names,
                'subtopic_recs_len': len(subtopics_recs),

                'sub_strand_list': sub_strand_list,
                'sub_strand_dict_list': sub_strand_dict_list,
                'topic_dict_list': topic_dict_list,
                'filter_flag': True,
                'conf_dict_list': assessment_conf_names,
                'subtopic_recs_len': len(subtopics_recs),
                'na_flag': na_flag,
                'skill_excel_form':skill_excel_form,
                'round_select':round_select,
                'round_decimal_place':round_decimal_place
            }

            return render(request, "enter_exam_scheme_mark_new.html", form_vals)
        else:
            messages.warning(request,"No Data Found For Selected Record")
            return redirect('/school/enter_exam_scheme_mark/')

    filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True).order_by('first_name')
    current_academic_year = academic_year.objects.get(current_academic_year=1)
    selected_year_id = current_academic_year.id

    form_vals = {
        'conf_list': conf_list,
        'entered_value': entered_value,
        'total_mark': total_mark,
        'student_list': filtered_students,
        'Selected_Section_Id': selected_section_id,
        'selected_section_name': selected_section_name,
        'selected_section': selected_section_rec,
        'section_list': all_section_recs,
        'Selected_Class_Id': selected_class_id,
        'selected_class_name': selected_class_name,
        'selected_class': selected_class_rec,
        'class_list': all_class_recs,
        'selected_subject_id': selected_subject_id,
        'selected_subject_name': selected_subject_rec,
        'Selected_Year_Id': selected_year_id,
        'selected_year_name': selected_year_name,
        'selected_year': selected_year_rec,
        'year_list': all_academicyr_recs,
        'frequency_recs': frequency_recs,
        'frequency_data': frequency_data,
        'scheme_recs': scheme_rec,
        'subtopics_recs': subtopics_recs,
        'selected_scheme': selected_scheme,

        'grade_colors': grade_colors,
        'exam_rec': exam_scheme_subdetails_obj.exam_name,
        'conf_dict_list':assessment_conf_names,
        'subtopic_recs_len': len(subtopics_recs),

        'sub_strand_list': sub_strand_list,
        'sub_strand_dict_list': sub_strand_dict_list,
        'topic_dict_list': topic_dict_list,
        'filter_flag': True,
        'conf_dict_list': assessment_conf_names,
        'subtopic_recs_len': len(subtopics_recs),

        'na_flag': na_flag,
        'skill_excel_form':skill_excel_form,
        'round_select':round_select,
        'round_decimal_place':round_decimal_place,

    }
    return render(request, "add_exam_scheme_marks.html",form_vals)

#
# @user_login_required
# def ExportExamStudentMarks(request):
#     try :
#         topic_form = request.POST.get('topic_form')
#         total_mark = request.POST.get('maxMarks')
#         total_mark= int(total_mark)
#         year_name = request.POST.get('year_name')
#         class_name = request.POST.get('class_name')
#         section_name = request.POST.get('section_name')
#         subject = request.POST.get('subject')
#         frequency = request.POST.get('frequency')
#         exam_name = request.POST.get('exam_name')
#         sub_strand_filter = request.POST.get('sub_strand_filter')
#         all_rec = []
#         subtopics_recs = []
#         frequency_data = frequency_details.objects.get(id=frequency)
#         exam_scheme_rec = exam_scheme_acd_mapping_details.objects.get(year_id=year_name,class_obj_id=class_name,subject_id=subject)
#         archive_record_count = exam_scheme_rec.exam_name_mapping_ids.filter(exam_name_id=exam_name).count()
#         exam_scheme_subdetails_obj = exam_scheme_rec.exam_name_mapping_ids.get(exam_name_id=exam_name,is_archived=False)
#         scheme_rec = exam_scheme_subdetails_obj.exam_scheme
#         scheme_id = scheme_rec.id
#         selected_scheme = exam_scheme.objects.get(id=scheme_id)
#         scheme_mapped_flag = False
#         subtopics_recs = []
#         subtopics_recs = []
#         subtopic_recs = []
#         list = []
#         sub_strand_list = []
#         filter_flag = True
#         sub_strand_dict_list = []
#         sub_strand_dict_id = []
#         topic_dict_id = []
#         topic_dict_list = []
#
#         if selected_scheme.subject_exam_name:
#             scheme_mapped_flag = True
#         for strand_rec in selected_scheme.stand_ids.all():
#             for sub_strand_rec in strand_rec.sub_strand_ids.all():
#                 sub_strand_dict = {}
#                 sub_strand_temp_dict = {}
#                 if scheme_mapped_flag:
#                     sub_strand_temp_dict['sub_strand_name'] = subject_substrands.objects.get(
#                         id=sub_strand_rec.sub_strand_name).substrands_name
#                 else:
#                     sub_strand_temp_dict['sub_strand_name'] = sub_strand_rec.sub_strand_name
#
#                 sub_strand_temp_dict['id'] = sub_strand_rec.id
#                 sub_strand_list.append(sub_strand_temp_dict)
#                 if sub_strand_rec.id not in sub_strand_dict_id:
#                     sub_strand_dict_id.append(sub_strand_rec.id)
#                     if scheme_mapped_flag:
#                         sub_strand_dict['sub_strand_name'] = subject_substrands.objects.get(
#                             id=sub_strand_rec.sub_strand_name).substrands_name
#                         sub_strand_dict['topic_count'] = 0
#                     else:
#                         sub_strand_dict['sub_strand_name'] = sub_strand_rec.sub_strand_name
#                         sub_strand_dict['topic_count'] = 0
#
#                 for topic_rec in sub_strand_rec.topic_ids.all():
#                     topic_dict = {}
#                     if topic_rec.id not in topic_dict_id:
#                         topic_dict['topic_name'] = topic_rec.topic_name
#                         topic_dict['subtopic_count'] = topic_rec.sub_topic_ids.all().count()
#                         topic_dict_id.append(topic_rec.id)
#                         topic_dict_list.append(topic_dict)
#                         sub_strand_dict['topic_count'] += topic_rec.sub_topic_ids.all().count()
#                 sub_strand_dict_list.append(sub_strand_dict)
#         for starand in selected_scheme.stand_ids.all():
#             for sub_strand_recs in starand.sub_strand_ids.all():
#                 if sub_strand_recs:
#                     for topic_recs in sub_strand_recs.topic_ids.all():
#                         if topic_recs:
#                             for subtopics in topic_recs.sub_topic_ids.all():
#                                 subtopics_recs.append(str(subtopics.name))
#         year_class_section_obj = academic_class_section_mapping.objects.get(year_name=year_name,section_name=section_name,class_name=class_name)
#         filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj,is_active=True)
#         sub_strand_name = sub_strand_dict_list[0]['sub_strand_name']
#
#         for student_rec in filtered_students:
#             rec_list = []
#
#             rec_list.append(student_rec.student_code)
#             rec_list.append(student_rec.academic_class_section.year_name)
#             rec_list.append(student_rec.academic_class_section.class_name)
#             rec_list.append(student_rec.academic_class_section.section_name)
#             rec_list.append(student_rec.first_name)
#             rec_list.append(student_rec.odoo_id)
#             rec_list.append(exam_scheme_rec.subject.subject_name)
#             rec_list.append(frequency_data.frequency_name)
#             all_rec.append(rec_list)
#
#             column_names = ['Student Code','Academic Year','Class','Section','Student Name','SCODOO Id','Subject','Frequency']
#             if len(subtopics_recs) > 0 :
#                 cloumns_list = column_names + subtopics_recs
#             else:
#                 cloumns_list = column_names
#         return export_users_xls('StudentMark', cloumns_list, all_rec)
#     except:
#         messages.warning(request, "Some Error Occurred. Try Again.")
#     return redirect('/school/all_set_student_target_list/')
#
#

@user_login_required
def ExamFilterAddStudentList(request):
    try:
        all_academicyr_recs = academic_year.objects.all()
        if request.POST:
            request.session['exam_filter_data'] = request.POST
            selected_year_id = request.POST.get('acd_year')
            selected_class_id = request.POST.get('class')
            selected_section_id = request.POST.get('section')
            frequency_name = request.POST.get('frequency')
            selected_subject_id = request.POST.get('sub')
            # selected_copy_from_id = request.POST.get('copy_from')
            topic_form = request.POST.get('topic_form')
            sub_strand_filter = request.POST.get('sub_strand_filter')
            exam_name = request.POST.get('exm_name_form')
        else:
            selected_year_id = request.session['exam_filter_data'].get(
                'acd_year')  # It will be used in HTML page as a value of selected Class
            selected_class_id = request.session['exam_filter_data'].get(
                'class')  # It will be used in HTML page as a value of selected Class
            selected_section_id = request.session['exam_filter_data'].get(
                'section')  # It will be used in HTML page as a value of selected Section
            frequency_name = request.session['exam_filter_data'].get('frequency')
            selected_subject_id = request.session['exam_filter_data'].get(
                'sub')  # It will be used in HTML page as a value of selected Section
            # selected_copy_from_id = request.session['data'].get('copy_from')
            topic_form = request.session['exam_filter_data'].get('topic_form')
            sub_strand_filter = request.session['exam_filter_data'].get('sub_strand_filter')
            exam_name = request.session['exam_filter_data'].get('exm_name_form')


        if topic_form == '' and sub_strand_filter == '':
            return redirect('/school/exam_scheme_student_list/')

        selected_year_rec = academic_year.objects.get(id=selected_year_id)
        selected_year_name = selected_year_rec.year_name
        all_class_recs = mapped_user_classes(selected_year_rec, request)

        selected_class_rec = class_details.objects.get(id=selected_class_id)
        selected_class_name = selected_class_rec.class_name

        all_section_recs = current_class_mapped_sections(selected_year_rec, selected_class_rec, request.user)

        selected_section_rec = sections.objects.get(id=selected_section_id)
        selected_section_name = selected_section_rec.section_name

        frequency_data = frequency_details.objects.get(id=frequency_name)

        selected_subject_rec = subjects.objects.get(id=selected_subject_id)
        selected_subject_name = selected_subject_rec.subject_name

        exam_rec = exam_details.objects.get(id=exam_name);


        assessment_conf_names = assessment_configration_master.objects.all()
        conf_list = ['Ab', 'NA']
        for conf in assessment_conf_names:
            conf_list.append(str(conf.short_name))

        entered_value = '0-9.'
        for c in conf_list: entered_value = str(c) + str(entered_value)

        try:
            exam_scheme_rec = exam_scheme_acd_mapping_details.objects.get(year_id=selected_year_id,
                                                                          class_obj_id=selected_class_id,
                                                                          subject_id=selected_subject_id)

            archive_record_count = exam_scheme_rec.exam_name_mapping_ids.filter(exam_name_id=exam_name).count()
            exam_scheme_subdetails_obj = exam_scheme_rec.exam_name_mapping_ids.get(exam_name_id=exam_name,
                                                                                   is_archived=False)
            scheme_rec = exam_scheme_subdetails_obj.exam_scheme
            scheme_id = scheme_rec.id
        except:
            if archive_record_count > 0:
                messages.warning(request, "New Mapping Not Found for The Selected Class And Section")
                return redirect('/school/enter_scheme_mark/')
            messages.warning(request, "Mapping Not Found for Selected Subject")
            return redirect('/school/enter_exam_scheme_mark/')

        selected_scheme = exam_scheme.objects.get(id=scheme_id)
        scheme_mapped_flag = False

        if selected_scheme.subject_exam_name:
            scheme_mapped_flag = True

        subtopics_recs = []
        subtopic_recs = []
        list = []
        sub_strand_list = []
        filter_flag = True

        sub_strand_dict_list = []
        sub_strand_dict_id = []
        topic_dict_id = []
        topic_dict_list = []


        sub_strand_data = exam_sub_strand.objects.get(id=sub_strand_filter)

        topic_form_data = exam_topic.objects.get(id=topic_form)

        sub_strand_dict = {}
        topic_dict = {}

        if scheme_mapped_flag:
            sub_strand_dict['sub_strand_name'] = subject_substrands.objects.get(
                id=sub_strand_data.sub_strand_name).substrands_name
        else:
            sub_strand_dict['sub_strand_name'] = sub_strand_data.sub_strand_name
        sub_strand_dict['id'] = sub_strand_data.id

        sub_strand_dict[
            'topic_count'] = topic_form_data.sub_topic_ids.all().count()  # used for row and colspan in table head of html
        sub_strand_dict_list.append(sub_strand_dict)

        topic_dict['topic_name'] = topic_form_data.topic_name
        topic_dict[
            'subtopic_count'] = topic_form_data.sub_topic_ids.all().count()  # used for row and colspan in table head of html
        topic_dict_list.append(topic_dict)

        # scheme_id = seheme_mapping_rec.scheme_name.id
        subtopics_recs = []
        subtopics_list = []
        subtopics_ids = []
        subtopic_recs = []
        list = []
        sub_strand_list = []
        topic_list = []

        for strand_obj in selected_scheme.stand_ids.all():
            for sub_strand_obj in strand_obj.sub_strand_ids.all():
                sub_strand_temp_dict = {}
                if scheme_mapped_flag:
                    sub_strand_temp_dict['sub_strand_name'] = subject_substrands.objects.get(
                        id=sub_strand_obj.sub_strand_name).substrands_name
                else:
                    sub_strand_temp_dict['sub_strand_name'] = sub_strand_obj.sub_strand_name

                sub_strand_temp_dict['id'] = sub_strand_obj.id
                sub_strand_list.append(sub_strand_temp_dict)

        for topic_rec_obj in sub_strand_data.topic_ids.all():
            topic_list.append(topic_rec_obj)

        topic_recs = exam_topic.objects.get(id=topic_form)

        for subtopics in topic_recs.sub_topic_ids.all():
            subtopics_list.append(subtopics.id)

        selected_scheme = exam_scheme.objects.get(id=scheme_id)
        subtopics_recs = []

        for starand in selected_scheme.stand_ids.all():
            for sub_strand_recs in starand.sub_strand_ids.all():
                if sub_strand_recs:
                    for topic_recs in sub_strand_recs.topic_ids.all():
                        if topic_recs:
                            for subtopics in topic_recs.sub_topic_ids.all():
                                subtopics_recs.append(subtopics)

        total_mark = 0
        for subtopic_rec in subtopics_recs:
            total_mark = int(subtopic_rec.mark) + total_mark


        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                            section_name=selected_section_id,
                                                                            class_name=selected_class_id)
        mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(
            scheme_mapping_id=exam_scheme_subdetails_obj.id, frequency_id=frequency_name,
            academic_mapping_id=year_class_section_obj.id, is_archived=False)
        grade_colors = grade_sub_details.objects.filter(grade=exam_scheme_subdetails_obj.grade_name)

        if mark_recs:
            messages.warning(request, "Mark already entered, Update the entered marks.")
            # return redirect('/school/enter_exam_scheme_mark/')
            return redirect('/school/update_entered_exam_scheme_mark/' + str(mark_recs[0].id))

        # if selected_copy_from_id:
        #     year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,section_name=selected_section_id, class_name=selected_class_id)
        #     scheme_mapping_rec = scheme_mapping_details.objects.get(year=selected_year_id,class_obj=selected_class_id,subject=selected_subject_id, is_archived = False)
        #     scheme_acd_frequency = scheme_academic_frequency_mapping.objects.get(academic_mapping_id = year_class_section_obj.id,scheme_mapping_id=scheme_mapping_rec.id,frequency_id=selected_copy_from_id)
        #     copy_from_data = frequency_details.objects.get(id=selected_copy_from_id)
        #     academic_frequency_rec = scheme_academic_frequency_mapping.objects.get(id=scheme_acd_frequency.id)
        #     selected_scheme = scheme.objects.get(id=academic_frequency_rec.scheme_mapping.scheme_name_id)
        #     subtopics_recs = []
        #     subtopic_recs = []
        #     list = []
        #     grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)
        #     for data in academic_frequency_rec.student_mark_ids.all():
        #         for rec in data.subtopic_ids.all():
        #             if rec.subtopic.id not in list:
        #                 list.append(rec.subtopic.id)
        #                 subtopic_recs.append(rec.subtopic)
        #     form_vals = {
        #     'entered_value': entered_value,
        #     'conf_list': conf_list,
        #     'total_mark':total_mark,
        #     'Selected_Section_Id': selected_section_id,
        #     'selected_section_name': selected_section_name,
        #     'selected_section': selected_section_rec,
        #     'section_list': all_section_recs,
        #
        #     'Selected_Class_Id': selected_class_id,
        #     'selected_class_name': selected_class_name,
        #     'selected_class': selected_class_rec,
        #     'class_list': all_class_recs,
        #     'Selected_Year_Id': selected_year_id,
        #     'selected_year_name': selected_year_name,
        #     'selected_year': selected_year_rec,
        #     'year_list': all_academicyr_recs,
        #     'frequency_data': frequency_data,
        #     'subtopics_recs': subtopics_recs,
        #     'selected_scheme': selected_scheme,
        #
        #     'grade_colors': grade_colors,
        #     'selected_subject_id': selected_subject_id,
        #     'selected_subject_name': selected_subject_rec,
        #     'academic_frequency_rec': academic_frequency_rec,
        #     'subtopic_data':subtopic_recs,
        #     'copy_from_data':copy_from_data,
        #
        #     'subtopics_list': subtopics_list,
        #     'sub_strand_list': sub_strand_list,
        #     'sub_strand_dict_list': sub_strand_dict_list,
        #     'topic_dict_list': topic_dict_list,
        #     'filter_flag': False,
        #       'topic_list': topic_list,

        #     }
        #     return render(request, "enter_scheme_mark_new.html", form_vals)

        # filtered_students = []
        # if scheme_rec.subject.type == 'Optional':
        #     optional_subject_students = scheme_rec.subject.optional_subject_relation.all()
        #     for students in optional_subject_students:
        #         student_rec = student_details.objects.filter(id=students.id, is_active=True,
        #                                                      academic_class_section_id=year_class_section_obj.id)
        #         if student_rec:
        #             filtered_students.append(student_rec[0])
        # else:
        filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj,
                                                               is_active=True)
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        # scheme_recs = scheme.objects.all()
        frequency_recs = frequency_details.objects.all()

        #     Passing values to the HTML page through form_vals
        form_vals = {
            'entered_value': entered_value,
            'conf_list': conf_list,
            'total_mark': total_mark,
            'student_list': filtered_students,

            'Selected_Section_Id': selected_section_id,
            'selected_section_name': selected_section_name,
            'selected_section': selected_section_rec,
            'section_list': all_section_recs,

            'Selected_Class_Id': selected_class_id,
            'selected_class_name': selected_class_name,
            'selected_class': selected_class_rec,
            'class_list': all_class_recs,

            'Selected_Year_Id': selected_year_id,
            'selected_year_name': selected_year_name,
            'selected_year': selected_year_rec,
            'year_list': all_academicyr_recs,

            'frequency_recs': frequency_recs,
            'frequency_data': frequency_data,
            'scheme_recs': scheme_rec,
            'subtopics_recs': subtopics_recs,
            'selected_scheme': selected_scheme,
            'grade_colors': grade_colors,
            'selected_subject_id': selected_subject_id,
            'selected_subject_name': selected_subject_rec,

            'sub_strand_list': sub_strand_list,
            'sub_strand_dict_list': sub_strand_dict_list,
            'topic_dict_list': topic_dict_list,
            'filter_flag': False,
            'subtopics_list': subtopics_list,
            'topic_list': topic_list,
            'topic_rec': topic_form_data,
            'sub_strand_rec': sub_strand_dict,
            'conf_dict_list': assessment_conf_names,
            'subtopic_recs_len': len(subtopics_recs),
            'exam_rec':exam_rec
        }
        return render(request, "add_exam_scheme_marks.html", form_vals)
    except:
        messages.warning(request, 'Invalid Filter Selected.')
    return redirect('/school/enter_exam_scheme_mark/')

@user_login_required
def SaveExamSchemeMark(request):
        if request.method == 'POST':
            year_id = request.POST.get('acd_year')
            class_id = request.POST.get('class')
            section_id = request.POST.get('section')
            subject_id = request.POST.get('sub')
            frequency_rec_id = request.POST.get('frequency')
            exam_name = request.POST.get('exm_name')
            data = json.loads(request.POST.get('data'))


            subtopic_list = []

            try:
                data_rec = []
                exam_scheme_rec = exam_scheme_acd_mapping_details.objects.get(year_id=year_id,class_obj_id=class_id,subject_id = subject_id)
                exam_scheme_subdetails_obj = exam_scheme_rec.exam_name_mapping_ids.get(exam_name_id=exam_name,is_archived=False)
                scheme_rec = exam_scheme_subdetails_obj.exam_scheme
                scheme_id = scheme_rec.id
                exam_scheme_rec = exam_scheme.objects.get(id=scheme_id)

            except:
                messages.warning(request, "Mapping Not Found for Selected Subject")
                return redirect('/school/enter_exam_scheme_mark/')

            year_class_section_obj = academic_class_section_mapping.objects.get(year_name=year_id,section_name=section_id,class_name=class_id)
            mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(scheme_mapping_id=exam_scheme_subdetails_obj.id, frequency_id=frequency_rec_id,academic_mapping_id=year_class_section_obj.id, is_archived=False)

            if mark_recs:
                messages.warning(request, "Mark already entered, Update the entered marks.")
                return redirect('/school/update_entered_exam_scheme_mark/' + str(mark_recs[0].id))



            for starand in exam_scheme_rec.stand_ids.all():
                for sub_strand_recs in starand.sub_strand_ids.all():
                    if sub_strand_recs:
                        for topic_recs in sub_strand_recs.topic_ids.all():
                            if topic_recs:
                                for subtopics in topic_recs.sub_topic_ids.all():
                                    subtopic_list.append(subtopics)

            # seheme_mapping_rec = exam_scheme_mapping_details.objects.get(year_id=year_id,class_obj_id=class_id,exam_scheme=scheme_id)
            academic_mapping = academic_class_section_mapping.objects.get(year_name=year_id, class_name=class_id, section_name=section_id)

            academic_frequency_mapping_obj = exam_scheme_academic_frequency_mapping.objects.create(scheme_mapping_id = exam_scheme_subdetails_obj.id,frequency_id = frequency_rec_id, academic_mapping_id=academic_mapping.id)

            dic = {}
            for count , rec in enumerate(data[0]):
                obj = None
                temp = rec.split('-')
                obj = exam_scheme_em_subtopic_mark.objects.create(sub_topic_id = temp[0], subtopic_mark = data[0][rec])
                id = obj.id
                dic[temp[0]]=id

            for track, rec in  enumerate(data):
                if track == 0 or track == 1 :
                    pass
                else:

                    # if len(rec) > 1:
                    student_mark_mapping_obj = exam_scheme_student_mark_mapping.objects.create(student_id = rec['studentId'], total_mark = rec['totalMarks'], max_mark = rec['maxMarks'])
                    academic_frequency_mapping_obj.student_mark_ids.add(student_mark_mapping_obj.id)

                    for count , subtopic in enumerate(subtopic_list):
                        count = count+1
                        st_id = dic[rec['sub_topic_id_' + str(count)]]
                        scheme_subtopic_obj = exam_scheme_subtopic_mark.objects.create(mark = rec['subtopicMark_' + str(count)], subtopic_id = int(st_id))
                        student_mark_mapping_obj.subtopic_ids.add(scheme_subtopic_obj.id)

        return redirect('/school/update_entered_exam_scheme_mark/' + str(academic_frequency_mapping_obj.id))

@user_login_required
@user_permission_required('assessment.can_view_view_exam_exam_mark', '/school/home/')
def EnteredExamSchemeMarkList(request):
    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    scheme_recs = exam_scheme.objects.all()
    frequency_recs = frequency_details.objects.all()

    current_academic_year = None
    selected_year_id = None

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
    except:
        messages.error(request, "Please add academic year first.")
        return redirect('/school/exam_sub_tile/')
    #     return render(request, "add_exam_scheme_marks.html",
    #                   {'class_list': all_class_recs, 'section_list': all_section_recs, "year_list": all_academicyr_recs,
    #                    'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})
    # return render(request, "add_exam_scheme_marks.html",
    #               {'filter_flag': True, 'class_list': all_class_recs, 'section_list': all_section_recs,
    #                "year_list": all_academicyr_recs, 'selected_year_name': current_academic_year,
    #                'Selected_Year_Id': selected_year_id, 'scheme_recs': scheme_recs, 'frequency_recs': frequency_recs})

    # current_academic_year = academic_year.objects.get(current_academic_year=1)
    user = request.user
    academic_frequency_rec = []
    mapped_class = []
    mapped_section = []
    if request.user.is_system_admin() or request.user.is_officer():
        for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
            mapped_class.append(mapped_object.class_name)
            mapped_section.append(mapped_object.section_name)
        academic_frequency_rec = exam_scheme_academic_frequency_mapping.objects.all().order_by('is_submitted')
    else:
        if request.user.is_supervisor():
            supervisor_list = []
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj in academic_supervisor_class_section_mapping.objects.filter(
                        academic_class_section_mapping=mapped_object, supervisor_id=user.id):
                    if obj:
                        for obj in exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping=mapped_object):
                            if obj not in supervisor_list:
                                supervisor_list.append(obj)
                                academic_frequency_rec.append(obj)

        elif request.user.is_subject_teacher():
            subject_teacher_list = []
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj1 in teacher_subject_mapping.objects.filter(academic_class_section_mapping=mapped_object,
                                                                  staff_id=request.user.id):
                    if obj1:
                        for obj in exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping=mapped_object):
                            if obj.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject.id == int(obj1.subject.id):
                                if obj not in subject_teacher_list:
                                    subject_teacher_list.append(obj)
                                    academic_frequency_rec.append(obj)
        else:
            academic_frequency_rec = exam_scheme_academic_frequency_mapping.objects.filter(
                Q(academic_mapping__assistant_teacher=user.id) | Q(academic_mapping__staff_id=user.id))
    return render(request, "exam_mark_list.html", {'mapping_rec':academic_frequency_rec,'class_list': all_class_recs,'section_list': all_section_recs,"year_list": all_academicyr_recs,'selected_year_name': current_academic_year,'Selected_Year_Id': selected_year_id,'scheme_recs': scheme_recs,'frequency_recs': frequency_recs})

@user_login_required
def UpdateExamEnteredSchemeMark(request, rec_id):

    try:
        academic_frequency_rec = exam_scheme_academic_frequency_mapping.objects.get(id = rec_id)
        skill_excel_form = SkillExcelForm()
        round_rec = assessment_round_master.objects.all()[0]
        round_select = round_rec.select
        round_decimal_place = round_rec.decimal_place

        scheme_mapped_flag = False
        selected_scheme = exam_scheme.objects.get(id=academic_frequency_rec.scheme_mapping.exam_scheme.id)

        if selected_scheme.subject_exam_name:
            scheme_mapped_flag = True

        subtopics_recs = []
        subtopic_recs = []
        list_rec = []
        sub_strand_list = []
        filter_flag = True

        sub_strand_dict_list = []
        sub_strand_dict_id = []
        topic_dict_id = []
        topic_dict_list = []

        for strand_rec in selected_scheme.stand_ids.all():
            for sub_strand_rec in strand_rec.sub_strand_ids.all():
                sub_strand_dict = {}
                sub_strand_temp_dict = {}
                if scheme_mapped_flag:
                    sub_strand_temp_dict['sub_strand_name'] = subject_substrands.objects.get(
                        id=sub_strand_rec.sub_strand_name).substrands_name
                else:
                    sub_strand_temp_dict['sub_strand_name'] = sub_strand_rec.sub_strand_name

                sub_strand_temp_dict['id'] = sub_strand_rec.id
                sub_strand_list.append(sub_strand_temp_dict)
                if sub_strand_rec.id not in sub_strand_dict_id:
                    sub_strand_dict_id.append(sub_strand_rec.id)
                    if scheme_mapped_flag:
                        sub_strand_dict['sub_strand_name'] = subject_substrands.objects.get(
                            id=sub_strand_rec.sub_strand_name).substrands_name
                        sub_strand_dict['topic_count'] = 0
                    else:
                        sub_strand_dict['sub_strand_name'] = sub_strand_rec.sub_strand_name
                        sub_strand_dict['topic_count'] = 0

                for topic_rec in sub_strand_rec.topic_ids.all():
                    topic_dict = {}
                    if topic_rec.id not in topic_dict_id:
                        topic_dict['topic_name'] = topic_rec.topic_name
                        topic_dict['subtopic_count'] = topic_rec.sub_topic_ids.all().count()
                        topic_dict_id.append(topic_rec.id)
                        topic_dict_list.append(topic_dict)
                        sub_strand_dict['topic_count'] += topic_rec.sub_topic_ids.all().count()
                sub_strand_dict_list.append(sub_strand_dict)

        # subtopics_recs = []
        # subtopics_ids = []
        # subtopic_recs =[]
        # list_rec = []

        grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)

        for data in academic_frequency_rec.student_mark_ids.all():
            for rec in data.subtopic_ids.all():
                if rec.subtopic.id not in list_rec:
                    list_rec.append(rec.subtopic.id)
                    subtopic_recs.append(rec.subtopic)

        total_mark = 0
        for subtopic_rec in subtopic_recs:
            total_mark = int(subtopic_rec.subtopic_mark) + total_mark

        assessment_conf_names = assessment_configration_master.objects.all()
        # conf_list = ['Ab', 'NA']
        conf_list = []
        for conf in assessment_conf_names:
            conf_list.append(str(conf.short_name))

        entered_value = '0-9.'
        for c in conf_list: entered_value = str(c) + str(entered_value)

        if academic_frequency_rec.is_submitted is True:
            color_code = '#1ce034'
        else:
            color_code = '#ffa012'

        form_vals = {
            'conf_list': conf_list,
            'entered_value': entered_value,
            'total_mark':total_mark,
            'Selected_Section_Id': academic_frequency_rec.academic_mapping.section_name.id,
            'selected_section_name':academic_frequency_rec.academic_mapping.section_name.section_name,

            'Selected_Class_Id': academic_frequency_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].class_obj.id,
            'selected_class_name': academic_frequency_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].class_obj.class_name,

            'Selected_Year_Id': academic_frequency_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].year.id,
            'selected_year_name': academic_frequency_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].year.year_name,

            'frequency_recs': academic_frequency_rec.frequency,
            'frequency_data':  academic_frequency_rec.frequency.id,
            'exam_recs': academic_frequency_rec.scheme_mapping.exam_name,
            'subject_recs': academic_frequency_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject.subject_name,
            'subject_id_rec':academic_frequency_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject.id,
            'subtopics_recs': subtopics_recs,

            'academic_frequency_rec': academic_frequency_rec,
            'subtopic_data': subtopic_recs,
            'color_code': color_code,
            'grade_colors': grade_colors,
            'skill_excel_form':skill_excel_form,

            'sub_strand_list': sub_strand_list,
            'filter_flag': filter_flag,
            'sub_strand_dict_list': sub_strand_dict_list,
            'topic_dict_list': topic_dict_list,

            'conf_dict_list': assessment_conf_names,
            'subtopic_recs_len': len(subtopics_recs),
            'edit_flag':False,
            'round_select':round_select,
            'round_decimal_place':round_decimal_place,
        }
        return render(request, "edit_exam_scheme_entered_mark.html",form_vals)

    except:
        messages.warning(request, 'Some Error occurred.')
    return redirect('/school/entered_exam_scheme_mark_list/')

@user_login_required
def FilterExamStudentList(request):

    if request.POST:
        request.session['val_exam_data'] = request.POST
        rec_id = request.POST.get('academic_frequency_rec')
        topic_form = request.POST.get('topic_form')
        sub_strand_filter = request.POST.get('sub_strand_filter')
    else:
        rec_id = request.session['val_exam_data'].get('academic_frequency_rec')
        topic_form = request.session['val_exam_data'].get('topic_form')
        sub_strand_filter = request.session['val_exam_data'].get('sub_strand_filter')

    try:
        sub_strand_dict_list = []
        sub_strand_dict_id = []
        topic_dict_id = []
        topic_dict_list = []
        scheme_mapped_flag = False

        academic_frequency_rec = exam_scheme_academic_frequency_mapping.objects.get(id=rec_id)
        skill_excel_form = SkillExcelForm()
        selected_scheme = exam_scheme.objects.get(id=academic_frequency_rec.scheme_mapping.exam_scheme.id)

        if selected_scheme.subject_exam_name:
            scheme_mapped_flag = True

        sub_strand_data = exam_sub_strand.objects.get(id=sub_strand_filter)


        topic_form_data = exam_topic.objects.get(id=topic_form)

        sub_strand_dict = {}
        topic_dict = {}

        if scheme_mapped_flag:
            sub_strand_dict['sub_strand_name'] = subject_substrands.objects.get(id=sub_strand_data.sub_strand_name).substrands_name
        else:
            sub_strand_dict['sub_strand_name'] = sub_strand_data.sub_strand_name
        sub_strand_dict['id'] = sub_strand_data.id

        sub_strand_dict['topic_count'] = topic_form_data.sub_topic_ids.all().count()
        sub_strand_dict_list.append(sub_strand_dict)

        topic_dict['topic_name'] = topic_form_data.topic_name
        topic_dict['subtopic_count'] = topic_form_data.sub_topic_ids.all().count()
        topic_dict_list.append(topic_dict)

        # scheme_id = seheme_mapping_rec.scheme_name.id
        subtopics_recs = []
        subtopics_list = []
        subtopics_ids = []
        subtopic_recs = []
        list = []
        sub_strand_list = []
        topic_list = []
        filter_flag = False

        for strand_rec in selected_scheme.stand_ids.all():
            for sub_strand_obj in strand_rec.sub_strand_ids.all():
                sub_strand_temp_dict = {}
                if scheme_mapped_flag:
                    sub_strand_temp_dict['sub_strand_name'] = subject_substrands.objects.get(id=sub_strand_obj.sub_strand_name).substrands_name
                else:
                    sub_strand_temp_dict['sub_strand_name'] = sub_strand_obj.sub_strand_name

                sub_strand_temp_dict['id'] = sub_strand_obj.id
                sub_strand_list.append(sub_strand_temp_dict)


        for topic_rec_obj in sub_strand_data.topic_ids.all():
            topic_list.append(topic_rec_obj)

        grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)

        topic_recs = exam_topic.objects.get(id=topic_form)

        for subtopics in topic_recs.sub_topic_ids.all():
            subtopics_list.append(subtopics.id)

        for data in academic_frequency_rec.student_mark_ids.all():
            for rec in data.subtopic_ids.all():
                if rec.subtopic.id not in list:
                    if rec.subtopic.sub_topic_id in subtopics_list:
                        list.append(rec.subtopic.id)
                        subtopic_recs.append(rec.subtopic)

        total_mark = 0
        for subtopic_rec in subtopic_recs:
            total_mark = int(subtopic_rec.subtopic_mark) + total_mark

        if academic_frequency_rec.is_submitted is True:
            color_code = '#1ce034'
        else:
            color_code = '#ffa012'

        assessment_conf_names = assessment_configration_master.objects.all()
        conf_list = ['Ab', 'NA']
        for conf in assessment_conf_names:
            conf_list.append(str(conf.short_name))

        entered_value = '0-9.'
        for c in conf_list: entered_value = str(c) + str(entered_value)

        form_vals = {
            'conf_list': conf_list,
            'entered_value': entered_value,
            'total_mark':total_mark,
            'Selected_Section_Id': academic_frequency_rec.academic_mapping.section_name.id,
            'selected_section_name':academic_frequency_rec.academic_mapping.section_name.section_name,

            'Selected_Class_Id': academic_frequency_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].class_obj.id,
            'selected_class_name': academic_frequency_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].class_obj.class_name,

            'Selected_Year_Id': academic_frequency_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].year.id,
            'selected_year_name': academic_frequency_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].year.year_name,

            'frequency_recs': academic_frequency_rec.frequency,
            'frequency_data':  academic_frequency_rec.frequency.id,
            'exam_recs': academic_frequency_rec.scheme_mapping.exam_name,
            'subject_recs': academic_frequency_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject.subject_name,
            'subject_id_rec':academic_frequency_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject.id,
            'subtopics_recs': subtopics_recs,

            'academic_frequency_rec': academic_frequency_rec,
            'subtopic_data': subtopic_recs,
            'color_code': color_code,
            'grade_colors': grade_colors,
            'skill_excel_form':skill_excel_form,

            'sub_strand_list': sub_strand_list,
            'filter_flag':filter_flag,
            'subtopics_list':subtopics_list,
            'topic_rec':topic_form_data,
            'sub_strand_rec':sub_strand_dict,
            'topic_list':topic_list,
            'sub_strand_dict_list':sub_strand_dict_list,
            'topic_dict_list':topic_dict_list,

            'conf_dict_list': assessment_conf_names,
            'subtopic_recs_len': len(subtopics_recs),
            'edit_flag':True,
        }

    except:
        messages.warning(request,'Invalid Filter Selected.')
        return redirect('/school/entered_exam_scheme_mark_list/')

    return render(request, "edit_exam_scheme_entered_mark.html", form_vals)


@user_login_required
def FilterRubricStudentList(request):

    if request.POST:
        request.session['val_exam_data'] = request.POST
        rec_id = request.POST.get('academic_frequency_rec')
        sub_strand_filter = request.POST.get('strand_filter')
        strand_filter = request.POST.get('sub_strand_filter')
    else:
        rec_id = request.session['val_exam_data'].get('academic_frequency_rec')
        sub_strand_filter = request.session['val_exam_data'].get('strand_filter')
        strand_filter = request.session['val_exam_data'].get('sub_strand_filter')

    try:
        sub_strand_dict_list = []
        sub_strand_dict_id = []
        topic_dict_id = []
        strand_dict_list = []
        scheme_mapped_flag = False

        # rubric_catgory_na_obj = rubrics_master.objects.filter(rubrics_category_name=rubrics_category)[0]
        academic_frequency_rec = rubric_academic_frequency_mapping.objects.get(id=rec_id)

        rubrics_category = academic_frequency_rec.rubrics_category_name.id
        rubric_catgory_na_obj = rubrics_master.objects.filter(rubrics_category_name=rubrics_category)[0]

        skill_excel_form = SkillExcelForm()
        selected_scheme = rubric_scheme.objects.get(id=academic_frequency_rec.scheme_mapping.rubric_name.id)

        # if selected_scheme.subject_exam_name:
        #     scheme_mapped_flag = True

        sub_strand_data = rubric_sub_strand.objects.get(id=sub_strand_filter)

        strand_form_data = rubric_strand.objects.get(id=strand_filter)

        sub_strand_dict = {}
        strand_dict = {}

        # # if scheme_mapped_flag:
        # #     sub_strand_dict['sub_strand_name'] = subject_substrands.objects.get(id=sub_strand_data.sub_strand_name).substrands_name
        # # else:
        # sub_strand_dict['sub_strand_name'] = sub_strand_data.sub_strand_name
        # sub_strand_dict['id'] = sub_strand_data.id
        #
        # sub_strand_dict['topic_count'] = topic_form_data.sub_topic_ids.all().count()
        # sub_strand_dict_list.append(sub_strand_dict)
        #
        # topic_dict['topic_name'] = topic_form_data.topic_name
        # topic_dict['subtopic_count'] = topic_form_data.sub_topic_ids.all().count()
        # topic_dict_list.append(topic_dict)


        # if scheme_mapped_flag:
        #     sub_strand_dict['sub_strand_name'] = subject_substrands.objects.get(id=sub_strand_data.sub_strand_name).substrands_name
        # else:
        sub_strand_dict['sub_strand_name'] = strand_form_data.strand_name
        sub_strand_dict['id'] = strand_form_data.id

        sub_strand_dict['topic_count'] = sub_strand_data.topic_ids.all().count()
        sub_strand_dict_list.append(sub_strand_dict)

        strand_dict['sub_strand_name'] = sub_strand_data.sub_strand_name
        strand_dict['substrand_count'] = sub_strand_data.topic_ids.all().count()
        strand_dict_list.append(strand_dict)



        # scheme_id = seheme_mapping_rec.scheme_name.id
        subtopics_recs = []
        topics_list = []
        subtopics_ids = []
        topic_recs = []
        rec_list = []
        strand_list = []
        substrand_list = []
        filter_flag = False


        for strand_rec in selected_scheme.stand_ids.all():
            strand_temp_dict = {}

            strand_temp_dict['strand_name'] = strand_rec.strand_name
            strand_temp_dict['id'] = strand_rec.id
            strand_list.append(strand_temp_dict)


            # for sub_strand_obj in strand_rec.sub_strand_ids.all():
            #     sub_strand_temp_dict = {}
            #     if scheme_mapped_flag:
            #         sub_strand_temp_dict['sub_strand_name'] = subject_substrands.objects.get(id=sub_strand_obj.sub_strand_name).substrands_name
            #     else:
            #         sub_strand_temp_dict['sub_strand_name'] = sub_strand_obj.sub_strand_name
            #
            #     sub_strand_temp_dict['id'] = sub_strand_obj.id
            #     strand_list.append(sub_strand_temp_dict)





        # for strand_rec in selected_scheme.stand_ids.all():
        #     for sub_strand_obj in strand_rec.sub_strand_ids.all():
        #         sub_strand_temp_dict = {}
        #         if scheme_mapped_flag:
        #             sub_strand_temp_dict['sub_strand_name'] = subject_substrands.objects.get(id=sub_strand_obj.sub_strand_name).substrands_name
        #         else:
        #             sub_strand_temp_dict['sub_strand_name'] = sub_strand_obj.sub_strand_name
        #
        #         sub_strand_temp_dict['id'] = sub_strand_obj.id
        #         sub_strand_list.append(sub_strand_temp_dict)


        for substrand_rec_obj in strand_form_data.sub_strand_ids.all():
            substrand_list.append(substrand_rec_obj)

        grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)

        # topic_recs = rubric_sub_strand.objects.get(id=sub_strand_data)

        for topics_obj in sub_strand_data.topic_ids.all():
            topics_list.append(topics_obj.id)

        for data in academic_frequency_rec.student_mark_ids.all():
            for rec in data.student_option_ids.all():
                if rec.questions.id not in rec_list:
                    if rec.questions.id in topics_list:
                        rec_list.append(rec.questions.id)
                        topic_recs.append(rec.questions)

        # total_mark = 0
        # for subtopic_rec in topic_recs:
        #     total_mark = int(subtopic_rec.subtopic_mark) + total_mark

        if academic_frequency_rec.is_submitted is True:
            color_code = '#1ce034'
        else:
            color_code = '#ffa012'


        form_vals = {
            'Selected_Section_Id': academic_frequency_rec.academic_mapping.section_name.id,
            'selected_section_name':academic_frequency_rec.academic_mapping.section_name.section_name,

            'Selected_Class_Id': academic_frequency_rec.scheme_mapping.class_obj.id,
            'selected_class_name': academic_frequency_rec.scheme_mapping.class_obj.class_name,

            'Selected_Year_Id': academic_frequency_rec.scheme_mapping.year.id,
            'selected_year_name': academic_frequency_rec.scheme_mapping.year.year_name,

            'frequency_recs': academic_frequency_rec.frequency,
            'frequency_data':  academic_frequency_rec.frequency.id,
            # 'exam_recs': academic_frequency_rec.scheme_mapping.exam_name,
            'subject_recs': academic_frequency_rec.scheme_mapping.subject,
            'category_recs': academic_frequency_rec.rubrics_category_name,
            # 'subject_id_rec':academic_frequency_rec.scheme_mapping.subject.id,
            'subtopics_recs': subtopics_recs,

            'academic_frequency_rec': academic_frequency_rec,
            'subtopic_list': topic_recs,
            'color_code': color_code,
            'grade_colors': grade_colors,
            'skill_excel_form':skill_excel_form,



            'strand_list': strand_list,
            'filter_flag':filter_flag,
            'subtopics_list':topics_list,
            'topic_rec':sub_strand_data,
            'sub_strand_rec':sub_strand_dict,
            'sub_strand_list':substrand_list,
            'rubric_catgory_na': rubric_catgory_na_obj,


            'strand_dict_list':sub_strand_dict_list,
            'sub_strand_dict_list':strand_dict_list,
            # 'sub_strand_rec':strand_form_data

            # 'academic_frequency_rec': academic_frequency_rec,
            # 'subtopic_data': subtopic_recs,
            # 'color_code': color_code,
            # 'grade_colors': grade_colors,
            # 'rubric_recs': rubric_recs,
            # # 'subtopic_recs':subtopic_recs,
            # 'subtopic_list': subtopic_list,
            # 'rubric_catgory_na': rubric_catgory_na_obj,
            #
            # 'strand_dict_list': strand_dict_list,
            # 'sub_strand_dict_list': sub_strand_dict_list,
            # 'filter_flag': True,
            # 'strand_list': strand_list,
            # 'sub_strand_list': sub_strand_list,

        }

    except:
        messages.warning(request,'Invalid Filter Selected.')
        return redirect('/school/rubric_enter_mark_list/')

    return render(request, "rubric_edit_entered_mark.html", form_vals)


@user_login_required
def UpdateExamSchemeMark(request):

    if request.method == 'POST':
        academic_frequency_mapping_obj_id = request.POST.get('acd_freq_id')
        sub_list_check = json.loads(request.POST.get('sub_list_check'))

        try:
            data = json.loads(request.POST.get('data'))

            academic_frequency_mapping_obj = exam_scheme_academic_frequency_mapping.objects.get(id=academic_frequency_mapping_obj_id)
            ac_id = academic_frequency_mapping_obj.id

            dic = {}
            for count, rec in enumerate(data[0]):
                obj = None
                temp = rec.split('-')
                exam_scheme_em_subtopic_mark.objects.filter(id=temp[0]).update(subtopic_mark=data[0][rec])
                id = temp[0]
                dic[temp[1]] = id

            data_count = 2
            all_recs = academic_frequency_mapping_obj.student_mark_ids.all()
            for recs in all_recs:
                mark_list = []
                if sub_list_check:
                    for count, subtopic in enumerate(recs.subtopic_ids.all()):
                        if unicode(subtopic.subtopic.sub_topic_id) in sub_list_check:
                            count = count + 1
                            subtopic.mark = data[data_count]['subtopicMark_' + str(count)]
                            subtopic.save()

                    for count, subtopic in enumerate(recs.subtopic_ids.all()):
                        try:
                            mark_list.append(int(subtopic.mark))
                        except:
                            continue
                        count = count + 1
                    total = sum(mark_list)
                    recs.total_mark = total
                    recs.save()


                    data_count += 1
                else:
                    recs.total_mark = data[data_count]['totalMarks']
                    recs.max_mark = data[data_count]['maxMarks']
                    recs.save()
                    for count, subtopic in enumerate(recs.subtopic_ids.all()):
                        count = count + 1
                        subtopic.mark = data[data_count]['subtopicMark_' + str(count)]
                        subtopic.save()
                    data_count += 1

            # data_count = 2
            # all_recs = academic_frequency_mapping_obj.student_mark_ids.all()
            # for recs in all_recs:
            #     recs.total_mark = data[data_count]['totalMarks']
            #     recs.max_mark = data[data_count]['maxMarks']
            #     recs.save()
            #     for count, subtopic in enumerate(recs.subtopic_ids.all()):
            #         count = count + 1
            #         subtopic.mark = data[data_count]['subtopicMark_'+str(count)]
            #         subtopic.save()
            #     data_count += 1

            exam_scheme_academic_frequency_mapping.objects.filter(id = academic_frequency_mapping_obj_id).update(updated_on=str(datetime.now()))
            messages.success(request, "Record Updated Successfully.")
        except:
            messages.warning(request, "Some Error Occurred. Please Try Again.")
        return redirect('/school/update_entered_exam_scheme_mark/'+str(ac_id))

@user_login_required
def SubmitExamSchemeMark(request):
    rec_id = request.POST.get('rec_id')

    if exam_scheme_academic_frequency_mapping.objects.filter(id=rec_id,is_submitted=True).exists():
        messages.warning(request, "This record is already submitted.")
        return HttpResponse(
            json.dumps({'error': 'This record is already submitted.'}),
            content_type="application/json")


        # messages.warning(request, "This record is already submitted.")
        # return redirect('/school/update_entered_exam_scheme_mark/' + str(rec_id))

    acd_freq_rec = exam_scheme_academic_frequency_mapping.objects.get(id=rec_id)
    greading_recs = acd_freq_rec.scheme_mapping.grade_name.grade_name_rel.all()

    try:

        grade_arr = []
        for grade_rec in greading_recs:  # getting grades and creating its dict for the backend grade
            grade_dict = {
                'mar_less': int(grade_rec.marks_greater),
                'mar_greater': int(grade_rec.marks_less),
                'grade': grade_rec.grades,
            }
            grade_arr.append(grade_dict)

        for rec in acd_freq_rec.student_mark_ids.all():
            weightage = 0


            if rec.max_mark != None and rec.max_mark !='':
                if int(rec.max_mark) !=0:
                    if rec.total_mark != '' and rec.total_mark != None:
                        weightage = ((int(float(rec.total_mark)) * 100) / int(float(rec.max_mark)))


            for grd_rec in grade_arr:
                if grd_rec['mar_less'] <= weightage <= grd_rec['mar_greater']:  # getting grade of the specific weightage
                    grade = grd_rec['grade']
                    exam_scheme_student_mark_mapping.objects.filter(id=rec.id).update(grade_mark=grade)
                    break

        exam_scheme_academic_frequency_mapping.objects.filter(id=rec_id).update(is_submitted=True, submitted_by=request.user.id, updated_on=str(datetime.now()))
        messages.success(request, "Record submitted successfully.")
        return HttpResponse(
            json.dumps({'error': 'Record submitted successfully.'}), content_type="application/json")
    except:
        messages.warning(request, "Some error occoured. Record not submitted.")
    return HttpResponse(
            json.dumps({'error': 'Some error occoured. Record not submitted.'}),content_type="application/json")


# @user_login_required
# def ViewExamSchemeAprovalList(request):
#     all_academicyr_recs = academic_year.objects.all()
#     year_class_section_object = academic_class_section_mapping.objects.all()
#     #scheme_mapping_recs = scheme_mapping_details.objects.all()
#     if year_class_section_object:
#         try:
#             current_academic_year = academic_year.objects.get(current_academic_year=1)
#             selected_year_id = current_academic_year.id
#
#             ac_recs = academic_class_section_mapping.objects.filter(year_name_id=selected_year_id).values_list('id')
#
#             scheme_recs = exam_mapping_details.objects.filter(
#                 academic_class_section_mapping_id__in=ac_recs).values_list('id')
#             scheme_mark_rec = exam_scheme_academic_frequency_mapping.objects.filter(
#                 Q(scheme_mapping_id__in=scheme_recs) & Q(is_submitted=True, is_apprpved=False))
#
#             return render(request, 'exam_scheme_approval_list.html', {"year_list": all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,'scheme_mark_rec':scheme_mark_rec})
#         except academic_year.DoesNotExist:
#             messages.warning(request, "Academic Year Does not Exist.")
#             return render(request, 'exam_sub_tile.html')
#     else:
#         messages.success(request, "Academic Year Class Section Mapping Not Found.")
#
#     return render(request, "exam_sub_tile.html")


@user_login_required
@user_permission_required('assessment.can_view_view_exam_approval_list', '/school/home/')
def ViewExamSchemeAprovalList(request):
    # print  "In view_aproval_list function"
    frequency_recs = frequency_details.objects.all()
    # all_academicyr_recs = academic_year.objects.all()
    year_class_section_object = academic_class_section_mapping.objects.all()
    all_academicyr_recs = academic_year.objects.filter(is_active=True, current_academic_year=1)
    exam_scheme_recs = exam_scheme.objects.all()
    mapped_class = []
    mapped_section = []
    if year_class_section_object:
        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id

            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                mapped_class.append(mapped_object.class_name)
                mapped_section.append(mapped_object.section_name)

            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=selected_year_id).values_list('id')

            # scheme_recs = exam_mapping_details.objects.filter(academic_class_section_mapping_id__in=ac_recs).values_list('id')
            scheme_recs = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping_id__in=ac_recs,is_archived=False).values_list('academic_mapping')


            scheme_mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(Q(academic_mapping_id__in = scheme_recs) & Q( is_submitted = True, is_apprpved=False,is_archived=False))

            rec_list =[]
            for scheme_mark_rec in scheme_mark_recs:
                supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(academic_class_section_mapping = scheme_mark_rec.academic_mapping, subject = scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)
                # scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject.subject_name
                teacher_recs = teacher_subject_mapping.objects.filter(academic_class_section_mapping = scheme_mark_rec.academic_mapping, subject = scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)
                mapped_data = {
                    'supervisor_recs':supervisor_recs,
                    'teacher_recs':teacher_recs,
                    'scheme_mark_rec': scheme_mark_rec,
                    # 'scheme_recs':scheme_recs
                }
                rec_list.append(mapped_data)
            return render(request, 'exam_scheme_approval_list.html', {'scheme_mark_recs':rec_list,"year_list": all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,'frequency_recs':frequency_recs,'exam_scheme_recs':exam_scheme_recs,'ClassDetails': list(set(mapped_class)),'SectionDetails': list(set(mapped_section))})

        except academic_year.DoesNotExist:
            messages.warning(request, "Academic Year Does not Exist.")
            return render(request, 'exam_sub_tile.html')
    else:
        messages.warning(request, "Academic Year Class Section Mapping Not Found.")

    return render(request, "exam_sub_tile.html")




@user_login_required
def ViewExamSubjectScheme(request):
    finalDict = []
    subject_name = request.POST.get('subject_name', None)
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name')

    ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name, year_name_id=year_name, section_name_id=section_name)

    scheme_mapping_rec = exam_mapping_details.objects.filter(academic_class_section_mapping_id=ac_recs.id,subject_id=subject_name)
    ab = {}
    for rec in scheme_mapping_rec:
        if not any(d['id'] == rec.exam_scheme_id for d in finalDict):
            ab={'id':rec.exam_scheme_id, 'value': rec.exam_scheme.exam_scheme_name}
            finalDict.append(ab)
    return JsonResponse(finalDict,safe=False)


@user_login_required
def ViewExamSubjectSchemeFrequency(request):
    finalDict = []
    subject_name = request.POST.get('subject_name', None)
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name', None)
    scheme_name = request.POST.get('id_scheme', None)
    ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name, year_name_id=year_name, section_name_id=section_name)
    scheme_mapping_rec = exam_mapping_details.objects.filter(academic_class_section_mapping_id=ac_recs.id, subject_id=subject_name, exam_scheme_id=scheme_name)

    frequency_rec = exam_scheme_academic_frequency_mapping.objects.filter(scheme_mapping_id = scheme_mapping_rec,is_archived=False)

    ab = {}
    for rec in frequency_rec:
        if not any(d['id'] == rec.frequency_id for d in finalDict):
            ab={'id':rec.frequency_id, 'value': rec.frequency.frequency_name}
            finalDict.append(ab)
    return JsonResponse(finalDict,safe=False)


@user_login_required
def ViewExamFilteredSchemeAprovalList(request):
    all_academicyr_recs = academic_year.objects.all()
    year_class_section_object = academic_class_section_mapping.objects.all()
    scheme_mapping_recs = scheme_mapping_details.objects.all()

    year_name = request.POST.get('year_name', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name', None)
    scheme_name = request.POST.get('scheme', None)
    subject_name = request.POST.get('subject_name', None)
    frequency_name = request.POST.get('frequency_name', None)

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    selected_year_id = current_academic_year.id

    section_rec = sections.objects.get(id=section_name)
    scheme_rec = exam_scheme.objects.get(id=scheme_name)
    frequency_rec = frequency_details.objects.get(id = frequency_name)
    subjects_rec = subjects.objects.get(id = subject_name )

    ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name, year_name_id=year_name,
                                                         section_name_id=section_name)

    seheme_mapping_rec = exam_mapping_details.objects.get(academic_class_section_mapping_id=ac_recs.id,
                                                          exam_scheme=scheme_name)

    # academic_frequency_mapping_obj = scheme_academic_frequency_mapping.objects.get(
    #     scheme_mapping_id=seheme_mapping_rec.id)

    scheme_mark_rec = exam_scheme_academic_frequency_mapping.objects.filter(scheme_mapping_id=seheme_mapping_rec.id,
                                                                       is_submitted=True, is_apprpved=False,is_archived=False)

    form_vals = {
        'frequency_rec':frequency_rec,
        'scheme_rec': scheme_rec,
        'section_rec': section_rec,
        "year_list": all_academicyr_recs,
        'selected_year_name': current_academic_year,
        'Selected_Year_Id': selected_year_id,
        'subjects_rec':subjects_rec,
        'scheme_mark_rec':scheme_mark_rec
    }

    return render(request, "exam_scheme_approval_list.html", form_vals)

    # return render(request, "exam_scheme_approval_list.html", {"year_list": all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id, 'subjects_rec':subjects_rec, 'scheme_mark_rec':scheme_mark_rec})


@user_login_required
def ExamAproveRejectScheme(request):
    all_academicyr_recs = academic_year.objects.all()
    year_class_section_object = academic_class_section_mapping.objects.all()
    scheme_mapping_recs = scheme_mapping_details.objects.all()
    current_academic_year = academic_year.objects.get(current_academic_year=1)
    selected_year_id = current_academic_year.id

    # selected_recs = request.POST.getlist('check')
    task = request.POST.get('task', None)

    if task =='approve':
        id_approve = request.POST.get('id_approve')
        # for rec in selected_recs:
        exam_scheme_academic_frequency_mapping.objects.filter(id=id_approve).update(is_apprpved=True, is_rejected=False,
                                                                            approved_by_id=request.user.id)
        messages.success(request, "Selected Records Has Been Approved.")
    elif task == 'reject':
        # for rec in selected_recs:
        id_reject = request.POST.get('id_reject')
        exam_scheme_academic_frequency_mapping.objects.filter(id=id_reject).update(is_submitted=False, is_rejected=True,
                                                                            approved_by_id=request.user.id)
        messages.warning(request, "Approval of Selected Records Has Been Rejected.")
    else:
        messages.warning(request, "Some Error Occurred.")

    return render(request, 'exam_scheme_approval_list.html',
                  {"year_list": all_academicyr_recs, 'selected_year_name': current_academic_year,
                   'Selected_Year_Id': selected_year_id})
    #return redirect('/school/view_exam_scheme_aproval_list/')


# @user_login_required
# def ExamSchemeStatus(request):
#     all_academicyr_recs = academic_year.objects.all()
#     # scheme_mark_rec = exam_scheme_academic_frequency_mapping.objects.all()
#     scheme_mark_rec = exam_scheme_academic_frequency_mapping.objects.filter((Q(is_rejected=True) | Q(is_apprpved=True)))
#
#     if all_academicyr_recs:
#         try:
#             year_class_section_object = academic_class_section_mapping.objects.all()
#             scheme_mapping_recs = scheme_mapping_details.objects.all()
#             current_academic_year = academic_year.objects.get(current_academic_year=1)
#             selected_year_id = current_academic_year.id
#             return render(request, "exam_scheme_status.html",
#                           {"year_list": all_academicyr_recs, 'selected_year_name': current_academic_year,
#                            'Selected_Year_Id': selected_year_id,'scheme_mark_rec':scheme_mark_rec})
#         except:
#             messages.warning(request, "Academic Year Does not Exist.")
#             return render(request, 'exam_sub_tile.html')
#
#     else:
#         messages.success(request, "Academic Year Class Section Mapping Not Found.")
#         return render(request, "exam_sub_tile.html")



def load_exam_status(year_name=None,class_name=None,subject_teacher=None, status_name=None):
    if class_name == None:
        class_name = ''
    if subject_teacher == None:
        subject_teacher = ''
    if status_name == None:
        status_name = ''

    def load_status_fun(status_name):
        if status_name == '1':
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name)
            scheme_ids = exam_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs).values_list(
                'id')

            scheme_ay_recs = exam_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs)
            scheme_mark_ids = exam_scheme_academic_frequency_mapping.objects.filter(
                scheme_mapping__in=scheme_ay_recs,is_archived=False).values_list('scheme_mapping')

            diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))

            for diff_rec in diff_recs:
                diff_list.append(diff_rec[0])

            scheme_mapping_recs = exam_mapping_details.objects.filter(id__in=diff_list)
            scheme_mark_recs = []

            return scheme_mapping_recs, scheme_mark_recs

        if status_name == '2':
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name)

            scheme_mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping__in=ac_recs, is_submitted=False,is_archived=False)
            scheme_mapping_recs = []
            return scheme_mapping_recs, scheme_mark_recs

        if status_name == '3':
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name)
            scheme_mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(Q(academic_mapping__in=ac_recs,is_archived=False),
                                                             (Q(is_submitted=True, is_apprpved=False)) or (
                                                             Q(is_rejected=True, is_apprpved=False)))
            scheme_mapping_recs = []
            return scheme_mapping_recs, scheme_mark_recs

        if status_name == '4':

            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name)
            scheme_mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(
                academic_mapping__in=ac_recs, is_rejected=False, is_apprpved=True,is_archived=False)
            scheme_mapping_recs = []
            return scheme_mapping_recs, scheme_mark_recs

    def load_class_status_fun(class_name,status_name):

        if status_name == '2':
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,class_name_id=class_name)
            scheme_mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping__in=ac_recs,
                                                                               is_submitted=False,is_archived=False)
            scheme_mapping_recs = []
            return scheme_mapping_recs, scheme_mark_recs

        if status_name == '3':

            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,class_name_id=class_name)
            scheme_mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(Q(academic_mapping__in=ac_recs,is_archived=False),
                                                                               (Q(is_submitted=True,
                                                                                  is_apprpved=False)) or (
                                                                               Q(is_rejected=True,
                                                                                 is_apprpved=False)))
            scheme_mapping_recs = []
            return scheme_mapping_recs, scheme_mark_recs

        if status_name == '4':

            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,class_name_id=class_name)
            scheme_mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(
                academic_mapping__in=ac_recs, is_rejected=False, is_apprpved=True,is_archived=False)
            scheme_mapping_recs = []
            return scheme_mapping_recs, scheme_mark_recs

    diff_list = []

    if class_name=='' and subject_teacher=='' and status_name!='':
        scheme_mapping_recs,scheme_mark_recs  = load_status_fun(status_name)

    if class_name!='' and subject_teacher=='' and status_name!='':
        scheme_mapping_recs,scheme_mark_recs  = load_class_status_fun(class_name,status_name)

    if class_name!='' and subject_teacher=='' and status_name=='':
        ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,class_name_id=class_name)
        # scheme_ids = exam_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')

        scheme_mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping__in=ac_recs,is_archived=False)

    if class_name!='' and subject_teacher!='' and status_name!='':
        scheme_mapping_recs,scheme_mark_recs  = load_class_status_fun(class_name,status_name)

        rec_list = []
        final_list = []
        rows = []
        for scheme_mark_rec in scheme_mark_recs:
            teacher_ids = []
            tchr_rec = teacher_subject_mapping.objects.filter(
                academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)
            for tchr in tchr_rec:
                teacher_ids.append(int(tchr.staff_id.id))

            if int(subject_teacher) in teacher_ids:
                teacher_recs = teacher_subject_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)

                supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)

                mapped_data = {
                    'supervisor_recs': supervisor_recs,
                    'teacher_recs': teacher_recs,
                    'scheme_mark_rec': scheme_mark_rec
                }
                rec_list.append(mapped_data)
                tchr_list = []
                supervisor_recs_list = []
                status = ''

                tchr_name = ''
                sup_name = ''
                count = 1
                for rec in teacher_recs:
                    tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
                    count = count + 1
                    # tchr_list.append(rec.staff_id.get_pura_name())

                count = 1
                for rec in supervisor_recs:
                    sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
                    count = count + 1
                    supervisor_recs_list.append(rec.supervisor_id.get_pura_name())

                if scheme_mark_rec.is_submitted == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Under Review'
                elif scheme_mark_rec.is_apprpved == 1 and scheme_mark_rec.is_rejected == 0:
                    status = 'Final'
                elif scheme_mark_rec.is_rejected == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Draft'
                elif scheme_mark_rec.is_apprpved == 0 and scheme_mark_rec.is_rejected == 0:
                    status = 'Draft'

                dict_rec = {}
                dict_rec['Submitted_By'] = scheme_mark_rec.submitted_by
                dict_rec['Subject_Teacher'] = tchr_name
                dict_rec['Supervisor'] = sup_name
                dict_rec[
                    'Academic_Year'] = scheme_mark_rec.academic_mapping.year_name.year_name
                dict_rec['Class'] = scheme_mark_rec.academic_mapping.class_name.class_name
                dict_rec[
                    'Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
                dict_rec['Subject'] = scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject
                dict_rec['Grade_Scheme'] = scheme_mark_rec.scheme_mapping.grade_name.grade_name
                dict_rec['Scheme'] = scheme_mark_rec.scheme_mapping.exam_scheme.exam_scheme_name
                if scheme_mark_rec.approved_by is None:
                    dict_rec['Approved_By'] = 'Not Approved'
                elif not scheme_mark_rec.approved_by.username:
                    dict_rec['Approved_By'] = 'Not Approved'
                else:
                    dict_rec['Approved_By'] = scheme_mark_rec.approved_by
                # dict_rec['Frequency'] = scheme_mark_rec.frequency.frequency_name
                dict_rec['Status'] = status
                final_list.append(dict_rec)

        mapping_rec_list = []

        # for scheme_mapping_rec in scheme_mapping_recs:
        #     teacher_ids = []
        #     tchr_rec = teacher_subject_mapping.objects.filter(
        #         academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #         subject=scheme_mapping_rec.subject)
        #     for tchr in tchr_rec:
        #         teacher_ids.append(int(tchr.staff_id.id))
        #
        #     if int(subject_teacher) in teacher_ids:
        #
        #         supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         teacher_recs = teacher_subject_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         mapped_data = {
        #             'supervisor_recs': supervisor_recs,
        #             'teacher_recs': teacher_recs,
        #             'scheme_mapping_rec': scheme_mapping_rec
        #         }
        #         mapping_rec_list.append(mapped_data)
        #
        #         tchr_list = []
        #         supervisor_recs_list = []
        #         tchr_name = ''
        #         sup_name = ''
        #
        #         count = 1
        #         for rec in teacher_recs:
        #             tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
        #             count = count + 1
        #             # tchr_list.append(rec.staff_id.get_pura_name())
        #         count = 1
        #         for rec in supervisor_recs:
        #             sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
        #             count = count + 1
        #             supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
        #
        #         dict_rec = {}
        #         dict_rec['Submitted_By'] = 'Not Submitted'
        #         dict_rec['Subject_Teacher'] = tchr_name
        #         dict_rec['Supervisor'] = sup_name
        #         dict_rec['Academic_Year'] = scheme_mapping_rec.academic_class_section_mapping.year_name.year_name
        #         dict_rec['Class'] = scheme_mapping_rec.academic_class_section_mapping.class_name.class_name
        #         dict_rec['Section'] = scheme_mapping_rec.academic_class_section_mapping.section_name.section_name
        #         dict_rec['Subject'] = scheme_mapping_rec.subject.subject_name
        #         dict_rec['Grade_Scheme'] = scheme_mapping_rec.grade_name.grade_name
        #         dict_rec['Scheme'] = scheme_mapping_rec.exam_scheme.exam_scheme_name
        #         dict_rec['Approved_By'] = 'Not Approved'
        #         dict_rec['Status'] = 'Not Started'
        #         final_list.append(dict_rec)
        for rec in final_list:
            ab = [rec['Submitted_By'], rec['Subject_Teacher'], rec['Supervisor'], rec['Academic_Year'],
                  rec['Class'],
                  rec['Section'], rec['Subject'], rec['Grade_Scheme'], rec['Scheme'], rec['Approved_By'],
                  rec['Status']]
            rows.append(ab)
        return rows, final_list

    if class_name=='' and subject_teacher!='' and status_name!='':
        scheme_mapping_recs, scheme_mark_recs = load_status_fun(status_name)
        rec_list = []
        final_list = []
        rows = []
        for scheme_mark_rec in scheme_mark_recs:
            teacher_ids = []
            tchr_rec = teacher_subject_mapping.objects.filter(
                academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)

            # tchr_rec = teacher_subject_mapping.objects.filter(
            #     academic_class_section_mapping=scheme_mark_rec.academic_mapping,
            #     subject=scheme_mark_rec.subject_obj)
            for tchr in tchr_rec:
                teacher_ids.append(int(tchr.staff_id.id))

            if int(subject_teacher) in teacher_ids:
                teacher_recs = teacher_subject_mapping.objects.filter(
                academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)

                supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)

                mapped_data = {
                    'supervisor_recs': supervisor_recs,
                    'teacher_recs': teacher_recs,
                    'scheme_mark_rec': scheme_mark_rec
                }
                rec_list.append(mapped_data)
                tchr_list = []
                supervisor_recs_list = []
                status = ''

                tchr_name = ''
                sup_name = ''
                count = 1
                for rec in teacher_recs:
                    tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
                    count = count + 1
                    # tchr_list.append(rec.staff_id.get_pura_name())

                count = 1
                for rec in supervisor_recs:
                    sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
                    count = count + 1
                    supervisor_recs_list.append(rec.supervisor_id.get_pura_name())

                if scheme_mark_rec.is_submitted == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Under Review'
                elif scheme_mark_rec.is_apprpved == 1 and scheme_mark_rec.is_rejected == 0:
                    status = 'Final'
                elif scheme_mark_rec.is_rejected == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Draft'
                elif scheme_mark_rec.is_apprpved == 0 and scheme_mark_rec.is_rejected == 0:
                    status = 'Draft'

                dict_rec = {}
                dict_rec['Submitted_By'] = scheme_mark_rec.submitted_by
                dict_rec['Subject_Teacher'] = tchr_name
                dict_rec['Supervisor'] = sup_name
                dict_rec[
                    'Academic_Year'] = scheme_mark_rec.academic_mapping.year_name.year_name
                dict_rec['Class'] = scheme_mark_rec.academic_mapping.class_name.class_name
                dict_rec[
                    'Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
                dict_rec['Subject'] = scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject.subject_name
                dict_rec['Grade_Scheme'] = scheme_mark_rec.scheme_mapping.grade_name.grade_name
                dict_rec['Scheme'] = scheme_mark_rec.scheme_mapping.exam_scheme.exam_scheme_name
                if scheme_mark_rec.approved_by is None:
                    dict_rec['Approved_By'] = ' '
                elif not scheme_mark_rec.approved_by.username:
                    dict_rec['Approved_By'] = ' '
                else:
                    dict_rec['Approved_By'] = scheme_mark_rec.approved_by.get_pura_name()
                # dict_rec['Frequency'] = scheme_mark_rec.frequency.frequency_name
                dict_rec['Status'] = status
                final_list.append(dict_rec)

        mapping_rec_list = []

        # for scheme_mapping_rec in scheme_mapping_recs:
        #     teacher_ids = []
        #     tchr_rec = teacher_subject_mapping.objects.filter(
        #         academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #         subject=scheme_mapping_rec.subject)
        #     for tchr in tchr_rec:
        #         teacher_ids.append(int(tchr.staff_id.id))
        #
        #     if int(subject_teacher) in teacher_ids:
        #
        #         supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         teacher_recs = teacher_subject_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         mapped_data = {
        #             'supervisor_recs': supervisor_recs,
        #             'teacher_recs': teacher_recs,
        #             'scheme_mapping_rec': scheme_mapping_rec
        #         }
        #         mapping_rec_list.append(mapped_data)
        #
        #         tchr_list = []
        #         supervisor_recs_list = []
        #         tchr_name = ''
        #         sup_name = ''
        #
        #         count = 1
        #         for rec in teacher_recs:
        #             tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
        #             count = count + 1
        #             # tchr_list.append(rec.staff_id.get_pura_name())
        #         count = 1
        #         for rec in supervisor_recs:
        #             sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
        #             count = count + 1
        #             supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
        #
        #         dict_rec = {}
        #         dict_rec['Submitted_By'] = 'Not Submitted'
        #         dict_rec['Subject_Teacher'] = tchr_name
        #         dict_rec['Supervisor'] = sup_name
        #         dict_rec['Academic_Year'] = scheme_mapping_rec.academic_class_section_mapping.year_name.year_name
        #         dict_rec['Class'] = scheme_mapping_rec.academic_class_section_mapping.class_name.class_name
        #         dict_rec['Section'] = scheme_mapping_rec.academic_class_section_mapping.section_name.section_name
        #         dict_rec['Subject'] = scheme_mapping_rec.subject.subject_name
        #         dict_rec['Grade_Scheme'] = scheme_mapping_rec.grade_name.grade_name
        #         dict_rec['Scheme'] = scheme_mapping_rec.exam_scheme.exam_scheme_name
        #         dict_rec['Approved_By'] = ' '
        #         dict_rec['Status'] = 'Not Started'
        #         final_list.append(dict_rec)
        for rec in final_list:
            ab = [rec['Submitted_By'], rec['Subject_Teacher'], rec['Supervisor'], rec['Academic_Year'],
                  rec['Class'],
                  rec['Section'], rec['Subject'], rec['Grade_Scheme'], rec['Scheme'], rec['Approved_By'],
                  rec['Status']]
            if ab not in rows:
                rows.append(ab)
        return rows,final_list

    if class_name=='' and subject_teacher!='' and status_name=='':

        # ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name)
        # scheme_ids = exam_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')
        # scheme_mark_ids = exam_scheme_academic_frequency_mapping.objects.filter().values_list('scheme_mapping')
        # diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))
        #
        # for diff_rec in diff_recs:
        #     diff_list.append(diff_rec[0])
        #     #
        # scheme_mapping_recs = exam_mapping_details.objects.filter(id__in=diff_list)
        scheme_mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(is_archived=False)

        rec_list = []
        final_list = []
        rows = []
        for scheme_mark_rec in scheme_mark_recs:
            teacher_ids = []
            tchr_rec = teacher_subject_mapping.objects.filter(
                academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)
            for tchr in tchr_rec:
                teacher_ids.append(int(tchr.staff_id.id))

            if int(subject_teacher) in teacher_ids:
                teacher_recs = teacher_subject_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)

                supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)

                mapped_data = {
                    'supervisor_recs': supervisor_recs,
                    'teacher_recs': teacher_recs,
                    'scheme_mark_rec': scheme_mark_rec
                }
                rec_list.append(mapped_data)
                tchr_list = []
                supervisor_recs_list = []
                status = ''

                tchr_name = ''
                sup_name = ''
                count = 1
                for rec in teacher_recs:
                    tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
                    count = count + 1
                    # tchr_list.append(rec.staff_id.get_pura_name())

                count = 1
                for rec in supervisor_recs:
                    sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
                    count = count + 1
                    supervisor_recs_list.append(rec.supervisor_id.get_pura_name())

                if scheme_mark_rec.is_submitted == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Under Review'
                elif scheme_mark_rec.is_apprpved == 1 and scheme_mark_rec.is_rejected == 0:
                    status = 'Final'
                elif scheme_mark_rec.is_rejected == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Draft'
                elif scheme_mark_rec.is_apprpved == 0 and scheme_mark_rec.is_rejected == 0:
                    status = 'Draft'

                dict_rec = {}
                dict_rec['Submitted_By'] = scheme_mark_rec.submitted_by
                dict_rec['Subject_Teacher'] = tchr_name
                dict_rec['Supervisor'] = sup_name
                dict_rec[
                    'Academic_Year'] = scheme_mark_rec.academic_mapping.year_name.year_name
                dict_rec['Class'] = scheme_mark_rec.academic_mapping.class_name.class_name
                dict_rec[
                    'Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
                dict_rec['Subject'] = scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject
                dict_rec['Grade_Scheme'] = scheme_mark_rec.scheme_mapping.grade_name.grade_name
                dict_rec['Scheme'] = scheme_mark_rec.scheme_mapping.exam_scheme.exam_scheme_name
                if scheme_mark_rec.approved_by is None:
                    dict_rec['Approved_By'] = ' '
                elif not scheme_mark_rec.approved_by.username:
                    dict_rec['Approved_By'] = ' '
                else:
                    dict_rec['Approved_By'] = scheme_mark_rec.approved_by.get_pura_name()
                # dict_rec['Frequency'] = scheme_mark_rec.frequency.frequency_name
                dict_rec['Status'] = status
                final_list.append(dict_rec)

        mapping_rec_list = []

        # for scheme_mapping_rec in scheme_mapping_recs:
        #     teacher_ids = []
        #     tchr_rec = teacher_subject_mapping.objects.filter(
        #         academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #         subject=scheme_mapping_rec.subject)
        #     for tchr in tchr_rec:
        #         teacher_ids.append(int(tchr.staff_id.id))
        #
        #     if int(subject_teacher) in teacher_ids:
        #
        #         supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         teacher_recs = teacher_subject_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         mapped_data = {
        #             'supervisor_recs': supervisor_recs,
        #             'teacher_recs': teacher_recs,
        #             'scheme_mapping_rec': scheme_mapping_rec
        #         }
        #         mapping_rec_list.append(mapped_data)
        #
        #         tchr_list = []
        #         supervisor_recs_list = []
        #         tchr_name = ''
        #         sup_name = ''
        #
        #         count = 1
        #         for rec in teacher_recs:
        #             tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
        #             count = count + 1
        #             # tchr_list.append(rec.staff_id.get_pura_name())
        #         count = 1
        #         for rec in supervisor_recs:
        #             sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
        #             count = count + 1
        #             supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
        #
        #         dict_rec = {}
        #         dict_rec['Submitted_By'] = 'Not Submitted'
        #         dict_rec['Subject_Teacher'] = tchr_name
        #         dict_rec['Supervisor'] = sup_name
        #         dict_rec['Academic_Year'] = scheme_mapping_rec.academic_class_section_mapping.year_name.year_name
        #         dict_rec['Class'] = scheme_mapping_rec.academic_class_section_mapping.class_name.class_name
        #         dict_rec['Section'] = scheme_mapping_rec.academic_class_section_mapping.section_name.section_name
        #         dict_rec['Subject'] = scheme_mapping_rec.subject.subject_name
        #         dict_rec['Grade_Scheme'] = scheme_mapping_rec.grade_name.grade_name
        #         dict_rec['Scheme'] = scheme_mapping_rec.exam_scheme.exam_scheme_name
        #         dict_rec['Approved_By'] = 'Not Approved'
        #         dict_rec['Status'] = 'Not Started'
        #         final_list.append(dict_rec)
        for rec in final_list:
            ab = [rec['Submitted_By'], rec['Subject_Teacher'], rec['Supervisor'], rec['Academic_Year'],
                  rec['Class'],
                  rec['Section'], rec['Subject'], rec['Grade_Scheme'], rec['Scheme'], rec['Approved_By'],
                  rec['Status']]
            if ab not in rows:
                rows.append(ab)
        return rows,final_list

    if class_name != '' and subject_teacher != '' and status_name == '':

        ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name, class_name_id=class_name)
        # scheme_ids = exam_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')

        # scheme_ay_recs = exam_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs)
        scheme_mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping__in=ac_recs,is_archived=False)
        # scheme_mark_ids = exam_scheme_academic_frequency_mapping.objects.filter(
        #     scheme_mapping__in=scheme_ay_recs).values_list('scheme_mapping')
        # diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))

        # diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))

        # for diff_rec in diff_recs:
        #     diff_list.append(diff_rec[0])

        # scheme_mapping_recs = exam_mapping_details.objects.filter(id__in=diff_list)
        # scheme_mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(scheme_mapping__in=scheme_ay_recs)

        rec_list = []
        final_list = []
        rows = []
        for scheme_mark_rec in scheme_mark_recs:
            teacher_ids = []
            tchr_rec = teacher_subject_mapping.objects.filter(
                academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)
            for tchr in tchr_rec:
                teacher_ids.append(int(tchr.staff_id.id))

            if int(subject_teacher) in teacher_ids:
                teacher_recs = teacher_subject_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)

                supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)

                mapped_data = {
                    'supervisor_recs': supervisor_recs,
                    'teacher_recs': teacher_recs,
                    'scheme_mark_rec': scheme_mark_rec
                }
                rec_list.append(mapped_data)
                tchr_list = []
                supervisor_recs_list = []
                status = ''

                tchr_name = ''
                sup_name = ''
                count = 1
                for rec in teacher_recs:
                    tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
                    count = count + 1
                    # tchr_list.append(rec.staff_id.get_pura_name())

                count = 1
                for rec in supervisor_recs:
                    sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
                    count = count + 1
                    supervisor_recs_list.append(rec.supervisor_id.get_pura_name())

                if scheme_mark_rec.is_submitted == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Under Review'
                elif scheme_mark_rec.is_apprpved == 1 and scheme_mark_rec.is_rejected == 0:
                    status = 'Final'
                elif scheme_mark_rec.is_rejected == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Draft'
                elif scheme_mark_rec.is_apprpved == 0 and scheme_mark_rec.is_rejected == 0:
                    status = 'Draft'

                dict_rec = {}
                dict_rec['Submitted_By'] = scheme_mark_rec.submitted_by
                dict_rec['Subject_Teacher'] = tchr_name
                dict_rec['Supervisor'] = sup_name
                dict_rec[
                    'Academic_Year'] = scheme_mark_rec.academic_mapping.year_name.year_name
                dict_rec['Class'] = scheme_mark_rec.academic_mapping.class_name.class_name
                dict_rec[
                    'Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
                dict_rec['Subject'] = scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject
                dict_rec['Grade_Scheme'] = scheme_mark_rec.scheme_mapping.grade_name.grade_name
                dict_rec['Scheme'] = scheme_mark_rec.scheme_mapping.exam_scheme.exam_scheme_name
                if scheme_mark_rec.approved_by is None:
                    dict_rec['Approved_By'] = ' '
                elif not scheme_mark_rec.approved_by.username:
                    dict_rec['Approved_By'] = ' '
                else:
                    dict_rec['Approved_By'] = scheme_mark_rec.approved_by.get_pura_name()
                # dict_rec['Frequency'] = scheme_mark_rec.frequency.frequency_name
                dict_rec['Status'] = status
                final_list.append(dict_rec)

        mapping_rec_list = []

        # for scheme_mapping_rec in scheme_mapping_recs:
        #     teacher_ids = []
        #     tchr_rec = teacher_subject_mapping.objects.filter(
        #         academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #         subject=scheme_mapping_rec.subject)
        #     for tchr in tchr_rec:
        #         teacher_ids.append(int(tchr.staff_id.id))
        #
        #     if int(subject_teacher) in teacher_ids:
        #
        #         supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         teacher_recs = teacher_subject_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         mapped_data = {
        #             'supervisor_recs': supervisor_recs,
        #             'teacher_recs': teacher_recs,
        #             'scheme_mapping_rec': scheme_mapping_rec
        #         }
        #         mapping_rec_list.append(mapped_data)
        #
        #         tchr_list = []
        #         supervisor_recs_list = []
        #         tchr_name = ''
        #         sup_name = ''
        #
        #         count = 1
        #         for rec in teacher_recs:
        #             tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
        #             count = count + 1
        #             # tchr_list.append(rec.staff_id.get_pura_name())
        #         count = 1
        #         for rec in supervisor_recs:
        #             sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
        #             count = count + 1
        #             supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
        #
        #         dict_rec = {}
        #         dict_rec['Submitted_By'] = 'Not Submitted'
        #         dict_rec['Subject_Teacher'] = tchr_name
        #         dict_rec['Supervisor'] = sup_name
        #         dict_rec['Academic_Year'] = scheme_mapping_rec.academic_class_section_mapping.year_name.year_name
        #         dict_rec['Class'] = scheme_mapping_rec.academic_class_section_mapping.class_name.class_name
        #         dict_rec['Section'] = scheme_mapping_rec.academic_class_section_mapping.section_name.section_name
        #         dict_rec['Subject'] = scheme_mapping_rec.subject.subject_name
        #         dict_rec['Grade_Scheme'] = scheme_mapping_rec.grade_name.grade_name
        #         dict_rec['Scheme'] = scheme_mapping_rec.exam_scheme.exam_scheme_name
        #         dict_rec['Approved_By'] = 'Not Approved'
        #         dict_rec['Status'] = 'Not Started'
        #         final_list.append(dict_rec)
        for rec in final_list:
            ab = [rec['Submitted_By'], rec['Subject_Teacher'], rec['Supervisor'], rec['Academic_Year'],
                  rec['Class'],
                  rec['Section'], rec['Subject'], rec['Grade_Scheme'], rec['Scheme'], rec['Approved_By'],
                  rec['Status']]
            if ab not in rows:
                rows.append(ab)
        return rows, final_list

    if (class_name==None and subject_teacher==None and status_name==None) or (class_name=='' and subject_teacher=='' and status_name==''):
        ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name)
    #     scheme_ids = exam_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')
    #     scheme_mark_ids = exam_scheme_academic_frequency_mapping.objects.filter().values_list('scheme_mapping')
    #     diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))
    #
    #     for diff_rec in diff_recs:
    #         diff_list.append(diff_rec[0])
    # #
    #     scheme_mapping_recs = exam_mapping_details.objects.filter(id__in=diff_list)
        scheme_mark_recs = exam_scheme_academic_frequency_mapping.objects.filter(is_archived=False)

    rec_list = []
    final_list = []
    rows = []
    for scheme_mark_rec in scheme_mark_recs:
        supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
            academic_class_section_mapping=scheme_mark_rec.academic_mapping,
            subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)
        teacher_recs = teacher_subject_mapping.objects.filter(
            academic_class_section_mapping=scheme_mark_rec.academic_mapping,
            subject=scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject)
        mapped_data = {
            'supervisor_recs': supervisor_recs,
            'teacher_recs': teacher_recs,
            'scheme_mark_rec': scheme_mark_rec
        }
        rec_list.append(mapped_data)
        tchr_list = []
        supervisor_recs_list = []
        status = ''

        tchr_name = ''
        sup_name = ''
        count = 1
        for rec in teacher_recs:
            tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
            count = count + 1

        count = 1
        for rec in supervisor_recs:
            sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
            count = count + 1
            supervisor_recs_list.append(rec.supervisor_id.get_pura_name())

        if scheme_mark_rec.is_submitted == 1 and scheme_mark_rec.is_apprpved == 0:
            status = 'Under Review'
        elif scheme_mark_rec.is_apprpved == 1 and scheme_mark_rec.is_rejected == 0:
            status = 'Final'
        elif scheme_mark_rec.is_rejected == 1 and scheme_mark_rec.is_apprpved == 0:
            status = 'Draft'
        elif scheme_mark_rec.is_apprpved == 0 and scheme_mark_rec.is_rejected == 0:
            status = 'Draft'

        dict_rec = {}
        dict_rec['Submitted_By'] = scheme_mark_rec.submitted_by
        dict_rec['Subject_Teacher'] = tchr_name
        dict_rec['Supervisor'] = sup_name
        dict_rec['Academic_Year'] = scheme_mark_rec.academic_mapping.year_name.year_name
        dict_rec['Class'] = scheme_mark_rec.academic_mapping.class_name.class_name
        dict_rec['Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
        dict_rec['Subject'] = scheme_mark_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject
        dict_rec['Grade_Scheme'] = scheme_mark_rec.scheme_mapping.grade_name.grade_name
        dict_rec['Scheme'] = scheme_mark_rec.scheme_mapping.exam_scheme.exam_scheme_name
        if scheme_mark_rec.approved_by is None:
            dict_rec['Approved_By'] = ' '
        elif not scheme_mark_rec.approved_by.username:
            dict_rec['Approved_By'] = ' '
        else:
            dict_rec['Approved_By'] = scheme_mark_rec.approved_by.get_pura_name()
        # dict_rec['Frequency'] = scheme_mark_rec.frequency.frequency_name
        dict_rec['Status'] = status
        final_list.append(dict_rec)

    # mapping_rec_list = []
    # for scheme_mapping_rec in scheme_mapping_recs:
    #     supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
    #         academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
    #         subject=scheme_mapping_rec.subject)
    #     teacher_recs = teacher_subject_mapping.objects.filter(
    #         academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
    #         subject=scheme_mapping_rec.subject)
    #     mapped_data = {
    #         'supervisor_recs': supervisor_recs,
    #         'teacher_recs': teacher_recs,
    #         'scheme_mapping_rec': scheme_mapping_rec
    #     }
    #     mapping_rec_list.append(mapped_data)
    #
    #     tchr_list = []
    #     supervisor_recs_list = []
    #     tchr_name = ''
    #     sup_name = ''
    #
    #     count = 1
    #     for rec in teacher_recs:
    #         tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
    #         count = count + 1
    #         # tchr_list.append(rec.staff_id.get_pura_name())
    #     count = 1
    #     for rec in supervisor_recs:
    #         sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
    #         count = count + 1
    #         supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
    #
    #     dict_rec = {}
    #     dict_rec['Submitted_By'] = 'Not Submitted'
    #     dict_rec['Subject_Teacher'] = tchr_name
    #     dict_rec['Supervisor'] = sup_name
    #     dict_rec['Academic_Year'] = scheme_mapping_rec.academic_class_section_mapping.year_name.year_name
    #     dict_rec['Class'] = scheme_mapping_rec.academic_class_section_mapping.class_name.class_name
    #     dict_rec['Section'] = scheme_mapping_rec.academic_class_section_mapping.section_name.section_name
    #     dict_rec['Subject'] = scheme_mapping_rec.subject.subject_name
    #     dict_rec['Grade_Scheme'] = scheme_mapping_rec.grade_name.grade_name
    #     dict_rec['Scheme'] = scheme_mapping_rec.exam_scheme.exam_scheme_name
    #     dict_rec['Approved_By'] = 'Not Approved'
    #     dict_rec['Status'] = 'Not Started'
    #     final_list.append(dict_rec)
    for rec in final_list:
        ab = [rec['Submitted_By'], rec['Subject_Teacher'], rec['Supervisor'], rec['Academic_Year'], rec['Class'],
              rec['Section'], rec['Subject'], rec['Grade_Scheme'], rec['Scheme'], rec['Approved_By'], rec['Status']]
        rows.append(ab)
    return rows, final_list

@user_login_required
@user_permission_required('assessment.can_view_view_exam_status_list', '/school/home/')
def ExamSchemeStatus(request):
    # print "In SchemeStatus function"
    all_academicyr_recs = academic_year.objects.all()
    class_list = class_details.objects.all()
    staff_list =[]
    all_teacher_recs = teacher_subject_mapping.objects.all()
    for teacher_recs in all_teacher_recs:
        if teacher_recs.staff_id not in staff_list:
            staff_list.append(teacher_recs.staff_id)
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        class_list = current_academic_year_mapped_classes(current_academic_year, request.user)
    except:
        messages.warning(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
        return render(request, 'assessment_sub_tile.html')
    selected_year_id = current_academic_year.id
    rows, final_list = load_exam_status(selected_year_id)

    form_vals = {
        "year_list": all_academicyr_recs,
        "all_teacher_recs": staff_list,
        'selected_year_name': current_academic_year,
        'Selected_Year_Id': selected_year_id,
        'option_count': itertools.count(1),
        'rows': rows,
        'final_list': final_list,
        'class_list': class_list
    }

    return render(request, "exam_scheme_status.html",form_vals)


def load_exam_status_function(exam_val_dict,user):
    staff_list = []
    class_list = class_details.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    all_teacher_recs = teacher_subject_mapping.objects.all()
    for teacher_recs in all_teacher_recs:
        if teacher_recs.staff_id not in staff_list:
            staff_list.append(teacher_recs.staff_id)

    all_academicyr_recs = academic_year.objects.all()
    year_name = exam_val_dict.get('year_name', None)
    class_name = exam_val_dict.get('class_name', None)
    status_name = exam_val_dict.get('status_name', None)
    subject_tchr_name = exam_val_dict.get('subject_tchr_name', None)

    rows, final_list = load_exam_status(year_name, class_name, subject_tchr_name, status_name)

    if subject_tchr_name:
        subject_tchr_rec = AuthUser.objects.get(id=subject_tchr_name)
    else:
        subject_tchr_rec = ''
    if class_name:
        class_rec = class_details.objects.get(id=class_name)
    else:
        class_rec = ''
    if status_name:
        if status_name == '1':
            status_rec = 'Not Started'
            status_id = '1'
        if status_name == '2':
            status_rec = 'Draft'
            status_id = '2'
        if status_name == '3':
            status_rec = 'Under Review'
            status_id = '3'
        if status_name == '4':
            status_rec = 'Final'
            status_id = '4'
    else:
        status_rec = ''
        status_id = ''
    current_academic_year = academic_year.objects.get(current_academic_year=1)
    class_list = current_academic_year_mapped_classes(current_academic_year, user)
    selected_year_id = current_academic_year.id
    form_vals = {
        'final_list': final_list,
        'rows': rows,
        # 'scheme_mark_recs': rec_list,
        "year_list": all_academicyr_recs,
        'selected_year_name': current_academic_year,
        'Selected_Year_Id': selected_year_id,
        'subject_tchr_rec': subject_tchr_rec,
        'status_id': status_id,
        'status_rec': status_rec,
        "all_teacher_recs": staff_list,
        # 'section_rec': section_rec,
        # 'scheme_rec': scheme_rec,
        'class_rec': class_rec,
        'class_list': class_list
    }
    return form_vals


@user_login_required
def ExamSchemeStatusList(request):
    exam_val_dict = request.POST
    form_vals = {}
    flag = False

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
    except:
        messages.warning(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
        return render(request, 'assessment_sub_tile.html')

    if request.method == 'POST':
        request.session['exam_val_dict'] = exam_val_dict
        form_vals = load_exam_status_function(exam_val_dict, request.user)
    else:
        form_vals = load_exam_status_function(request.session.get('exam_val_dict'), request.user)

    return render(request, "exam_scheme_status.html", form_vals)

@user_login_required
def ValidateExamCodeExist(request):
    flag = None
    scheme_code = request.POST.get('scheme_code', None)
    if exam_scheme.objects.filter(exam_scheme_code=scheme_code).exists():
        flag = True
    else:
        flag=False
    return JsonResponse(flag, safe=False)


@user_login_required
def ValidateExamSchemeNameExist(request):
    flag = None
    scheme_name = request.POST.get('scheme_name', None)
    if exam_scheme.objects.filter(exam_scheme_name=scheme_name).exists():
        flag = True
    else:
        flag=False
    return JsonResponse(flag, safe=False)

@user_login_required
def ExportStatusRecords(request):
    year_name = request.POST.get('e_year_name', None)
    class_name = request.POST.get('e_class_name', None)
    # scheme_name = request.POST.get('e_exam_scheme_name', None)
    status_name = request.POST.get('e_status_name', None)
    subject_tchr_name = request.POST.get('e_subject_tchr_name', None)

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
    except:
        messages.warning(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
        return render(request, 'assessment_sub_tile.html')
    selected_year_id = current_academic_year.id

    rows, final_list = load_exam_status(year_name, class_name,subject_tchr_name, status_name)

    column_names = ['Submitted By', 'Subject Teacher', 'Supervisor', 'Academic Year', 'Class', 'Section', 'Subject',
                    'Grade Scheme', 'Scheme', 'Approved By', 'Status']

    return export_users_xls('exam_status', column_names, rows)

@user_login_required
@user_permission_required('assessment.can_view_view_rubric_categories', '/school/home/')
def RubricsSubTile(request):
    xyz = [] #get_all_user_permissions(request)
    return render(request, "rubrics_sub_tile.html", {'perms': list(xyz)})



@user_login_required
@user_permission_required('assessment.can_view_view_rubric_definition_list', '/school/home/')
def RubricDefinitionList(request):
    scheme_recs = rubric_scheme.objects.all()
    return render(request, "rubric_list.html",{'scheme_recs':scheme_recs})

@user_login_required
def RubricDefinition(request):
    rubrics_category_recs = rubrics_category.objects.all()
    return render(request, "rubric_definition_basic.html",{'rubrics_category_recs':rubrics_category_recs})


@user_login_required
def SaveRubric(request):
    try:
        if request.method == 'POST':

            scheme_code  = request.POST.get('scheme_code')
            scheme_name1 = request.POST.get('scheme_name')
            id_rubrics_catgory = request.POST.get('id_rubrics_catgory')

            rubric_category_instance = rubrics_category.objects.get(id=id_rubrics_catgory)

            submit_rec = request.POST.get('submit_rec')
            val_dict =  json.loads(request.POST['data'])
            scheme_obj = rubric_scheme.objects.create(code=scheme_code, scheme_name=scheme_name1,rubrics_category_name=rubric_category_instance)

            for data in val_dict:
                strand_data = rubric_strand.objects.create(strand_name=data['strand'])
                scheme_obj.stand_ids.add(strand_data.id)

                for substrand in data['substrand']:
                    sub_str = rubric_sub_strand.objects.create(sub_strand_name = substrand['substrand_name'])
                    strand_data.sub_strand_ids.add(sub_str.id)

                    for recs in substrand['topic']:
                        topic_name1 = recs['topic_name']
                        topic_table = rubric_topic.objects.create(topic_name=topic_name1)
                        sub_str.topic_ids.add(topic_table.id)

                        for subtopic_rec in recs['subtopic']:
                            sub_topic_name = subtopic_rec['value']
                            mark_val = subtopic_rec['mark']
                            try:
                                mark_rec = rubrics_master.objects.get(id=mark_val)
                                sub_topic_table = rubric_sub_topic.objects.create(name=sub_topic_name, mark=mark_rec)
                                topic_table.sub_topic_ids.add(sub_topic_table.id)
                            except:
                                sub_topic_table = rubric_sub_topic.objects.create(name = sub_topic_name)
                                topic_table.sub_topic_ids.add(sub_topic_table.id)

            if submit_rec == 'true':
                rubric_scheme.objects.filter(id=scheme_obj.id).update(is_submitted=True)
                messages.success(request, "Record Save and Submitted Successfully.")
            else:
                messages.success(request, "Record Saved Successfully.")

    except:
        messages.warning(request, "Some Error Occurred. Please Try again.")
    # return HttpResponse(json.dumps(scheme_obj.to_dict()), content_type='application/json')
    return HttpResponse(json.dumps({'success': " Rubrics Added saved successfully."}))


@user_login_required
def EditRubricDefinition(request, scheme_id):
    # print scheme_id
    # print "c",scheme_id
    option_count = itertools.count(1)

    iterator_color= make_incrementor(0)
    iterator_mark= make_incrementor(0)
    iterator_subtopic = make_incrementor(0)
    # iterator_subtopic = iterator_subtopic.res()

    iterator_topic= make_incrementor(0)
    iterator_topic_add = make_incrementor(0)
    iterator_topic_rem = make_incrementor(0)
    # num =0
    iterator_substrand = make_incrementor(0)
    iterator_substrand_add = make_incrementor(0)
    iterator_substrand_rem = make_incrementor(0)
    # iterator= 1
    color_count= make_incrementor(0)
    mark_count= make_incrementor(0)
    subtopic_count= make_incrementor(0)
    scheme_recs = rubric_scheme.objects.get(id=scheme_id)

    rubrics_category_id = scheme_recs.rubrics_category_name.id

    rubric_recs = rubrics_master.objects.filter(rubrics_category_name=rubrics_category_id).exclude(rubrics_name='NA')

    #######
    mark = []
    for obj in rubric_recs:
        mark.append(int(obj.rubrics_mark))

    selected_mark = sorted(mark)[-1]

    selected_option = rubrics_master.objects.get(rubrics_mark=selected_mark,
                                                 rubrics_category_name=rubrics_category_id)

    ####

    view_val = {
        'iterator_color':iterator_color,
        'iterator_mark': iterator_mark,
        'iterator_subtopic': iterator_subtopic,

        'iterator_topic':iterator_topic,
        'iterator_topic_add':iterator_topic_add,
        'iterator_topic_rem':iterator_topic_rem,

        'iterator_substrand_add':iterator_substrand_add,
        'iterator_substrand_rem':iterator_substrand_rem,
        'iterator_substrand':iterator_substrand,

        'color_count':color_count,
        'mark_count':mark_count,
        'subtopic_count':subtopic_count,

        "scheme_recs": scheme_recs,
        'submitted' : scheme_recs.is_submitted,

        'option_count': option_count,
        'st_top_count': make_incrementor(0),
        'st_top_count1': make_incrementor(0),
        'st_top_count2': make_incrementor(0),
        'rubric_recs':rubric_recs,
        'selected_option':selected_option,
    }


    # print "F"
    # data  =scheme_recs.to_dict()
    # return HttpResponse(json.dumps(scheme_recs.to_dict()), content_type='application/json')
    return render(request, "edit_rubric_definition.html", view_val)
    # return render(request, "edit_scheme_definition.html", {"sc_id":scheme_id})


def RubricDefinitionUpdate(request):
    scheme_id = request.POST.get('scheme_id')
    stand_id = request.POST.get('stand_id')
    submit = request.POST.get('submit')
    val_dict = json.loads(request.POST['data'])

    scheme_recs = rubric_scheme.objects.get(id=scheme_id)
    try:
        if submit == 'true':
            for rec in val_dict:
                for data in rec:
                    if not stand_id:
                        strand_data = rubric_strand.objects.create(strand_name=data['strand'])
                        scheme_recs.stand_ids.add(strand_data.id)
                    else:
                        strand_data = rubric_strand.objects.get(id=stand_id)
                        strand_data.strand_name = data['strand']
                        strand_data.save()

                    for substrand in data['substrand']:
                        sub_str_create = None
                        try:
                            sub_str = rubric_sub_strand.objects.get(id=substrand['substrand_id'])
                        except:
                            sub_str_create = rubric_sub_strand.objects.create(sub_strand_name=substrand['substrand_name'])

                        if not sub_str_create:
                            sub_str.sub_strand_name = substrand['substrand_name']
                            sub_str.save()
                        else:
                            strand_data.sub_strand_ids.add(sub_str_create.id)

                        for recs in substrand['topic']:
                            topic_name1 = recs['topic_name']
                            topic_table_create = None
                            try:
                                topic_table = rubric_topic.objects.get(id=recs['topic_id'])
                            except:
                                topic_table_create = rubric_topic.objects.create(topic_name=topic_name1)

                            if not topic_table_create:
                                topic_table.topic_name = topic_name1
                                topic_table.save()
                            else:
                                if not sub_str_create:
                                    sub_str.topic_ids.add(topic_table_create.id)
                                else:
                                    sub_str_create.topic_ids.add(topic_table_create.id)

                            for subtopic_rec in recs['subtopic']:
                                sub_topic_table_create = None
                                sub_topic_name = subtopic_rec['value']
                                mark_val = subtopic_rec['mark']
                                mark_rec = rubrics_master.objects.get(id=mark_val)
                                # color_val = subtopic_rec['color']
                                try:
                                    sub_topic_table = rubric_sub_topic.objects.get(id=subtopic_rec['subtopic_ids'])
                                except:
                                    sub_topic_table_create = rubric_sub_topic.objects.create(name=sub_topic_name, mark=mark_rec)
                                if not sub_topic_table_create:
                                    sub_topic_table.name = sub_topic_name
                                    sub_topic_table.mark = mark_rec
                                    # sub_topic_table.color=color_val
                                    sub_topic_table.save()
                                else:
                                    if not topic_table_create:
                                        topic_table.sub_topic_ids.add(sub_topic_table_create.id)
                                    else:
                                        topic_table_create.sub_topic_ids.add(sub_topic_table_create.id)
            rubric_scheme.objects.filter(id=scheme_recs.id).update(is_submitted=True, updated_on=str(datetime.now()))
            messages.success(request, "Record Submitted Successfully.")

        if submit == 'false':
            for data in val_dict:
                if not stand_id:
                    strand_data = rubric_strand.objects.create(strand_name=data['strand'])
                    scheme_recs.stand_ids.add(strand_data.id)
                else:
                    strand_data = rubric_strand.objects.get(id=stand_id)
                    strand_data.strand_name = data['strand']
                    strand_data.save()

                for substrand in data['substrand']:
                    sub_str_create = None
                    try:
                        sub_str = rubric_sub_strand.objects.get(id=substrand['substrand_id'])
                    except:
                        sub_str_create = rubric_sub_strand.objects.create(sub_strand_name=substrand['substrand_name'])

                    if not sub_str_create:
                        sub_str.sub_strand_name = substrand['substrand_name']
                        sub_str.save()
                    else:
                        strand_data.sub_strand_ids.add(sub_str_create.id)

                    for recs in substrand['topic']:
                        topic_name1 = recs['topic_name']
                        topic_table_create = None
                        try:
                            topic_table = rubric_topic.objects.get(id=recs['topic_id'])
                        except:
                            topic_table_create = rubric_topic.objects.create(topic_name=topic_name1)

                        if not topic_table_create:
                            topic_table.topic_name = topic_name1
                            topic_table.save()
                        else:
                            if not sub_str_create:
                                sub_str.topic_ids.add(topic_table_create.id)
                            else:
                                sub_str_create.topic_ids.add(topic_table_create.id)

                        for subtopic_rec in recs['subtopic']:
                            sub_topic_table_create = None
                            sub_topic_name = subtopic_rec['value']
                            mark_val = subtopic_rec['mark']
                            mark_rec = rubrics_master.objects.get(id=mark_val)

                            # color_val = subtopic_rec['color']
                            try:
                                sub_topic_table = rubric_sub_topic.objects.get(id=subtopic_rec['subtopic_ids'])
                            except:
                                sub_topic_table_create = rubric_sub_topic.objects.create(name=sub_topic_name, mark=mark_rec)
                            if not sub_topic_table_create:
                                sub_topic_table.name = sub_topic_name


                                sub_topic_table.mark = mark_rec
                                # sub_topic_table.color=color_val
                                sub_topic_table.save()
                            else:
                                if not topic_table_create:
                                    topic_table.sub_topic_ids.add(sub_topic_table_create.id)
                                else:
                                    topic_table_create.sub_topic_ids.add(sub_topic_table_create.id)
            rubric_scheme.objects.filter(id=scheme_recs.id).update(updated_on=str(datetime.now()))
            messages.success(request, "Record Updated Successfully.")
    except:
        messages.warning(request, "Some Error Occurred... Please Try Again")
    return HttpResponse(json.dumps({'success': " Rubrics Updated successfully."}))

    # return HttpResponse(json.dumps(scheme_recs.to_dict()), content_type='application/json')


@user_login_required
@user_permission_required('assessment.can_view_view_rubric_mapping_list', '/school/home/')
def ViewRubricMapping(request):

    scheme_recs = rubric_scheme.objects.filter(is_submitted = True)
    all_academicyr_recs = academic_year.objects.all()
    grade_recs = grade_details.objects.filter(is_archive = False)
    year_class_section_object = academic_class_section_mapping.objects.all()
    scheme_mapping_recs = rubric_mapping_details.objects.all()
    frequency_recs = frequency_details.objects.all()
    # frequency_recs = frequency_details.objects.all()

    if year_class_section_object:
        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id
            return render(request, 'view_rubric_mapping.html', { "year_list": all_academicyr_recs,'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id, 'scheme_recs':scheme_recs, 'grade_recs':grade_recs, 'scheme_mapping_recs':scheme_mapping_recs,'frequency_recs':frequency_recs})
        except academic_year.DoesNotExist:
            return render(request, 'view_rubric_mapping.html',{"year_list": all_academicyr_recs,  'scheme_recs':scheme_recs, 'grade_recs':grade_recs, 'scheme_mapping_recs':scheme_mapping_recs,'frequency_recs':frequency_recs})
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
    return render(request, 'rubrics_sub_tile.html')


@user_login_required
def SaveRubricMapping(request):
    try:
        if request.method == 'POST':
            year  = request.POST.get('year_name')
            class_name_ids = request.POST.getlist('class_name')
            frequency_mapping_ids = request.POST.getlist('frequency_mapping')
            scheme_name = request.POST.get('scheme')
            subject_name = request.POST.get('subject_name')
            grade_name = request.POST.get('grade_name')
            frequency_size = len(frequency_mapping_ids)
            flag_success=False
            flag_warning=False
            subject_not_mapped_flag = False
            subject_not_mapped_list = []

            for class_rec in class_name_ids:

                    if not mandatory_subject_mapping.objects.filter(academic_class_section_mapping__year_name_id=year,
                                                                    academic_class_section_mapping__class_name_id=class_rec,
                                                                    subject_id=subject_name):

                        subject_dict = {
                            'Class Name': class_details.objects.get(id=class_rec).class_name,
                            'Subject Name': subjects.objects.get(id=subject_name).subject_name
                        }
                        subject_not_mapped_list.append(subject_dict)

                        subject_not_mapped_flag = True
                        pass
                    else:
                        rec_list = []
                        exam_rec = rubric_mapping_details.objects.filter(year_id=year, class_obj_id=class_rec, subject_id=subject_name, rubric_name_id=scheme_name, grade_name_id=grade_name)
                        if exam_rec:
                            if exam_rec.count() == 1:
                                if exam_rec[0].is_archived == True:
                                    rubric_obj = rubric_mapping_details.objects.create(year_id=year, class_obj_id=class_rec,
                                                                                       subject_id=subject_name,
                                                                                       rubric_name_id=scheme_name,
                                                                                       grade_name_id=grade_name)
                                    for count in range(frequency_size):
                                        rubric_frequency_obj = rubric_mapping_frequency.objects.create(
                                            frequency_name_id=frequency_mapping_ids[count])
                                        rubric_obj.rubric_frequency_ids.add(rubric_frequency_obj.id)
                                    flag_success = True
                                else:
                                    flag_warning = True
                                    continue
                            else:
                                flag_warning=True
                                continue
                        elif rubric_mapping_details.objects.filter(~Q(rubric_name_id=scheme_name), year_id=year, class_obj_id=class_rec, subject_id=subject_name, grade_name_id=grade_name).exists():
                            flag_warning = True
                            continue
                        elif rubric_mapping_details.objects.filter(~Q(rubric_name_id=scheme_name,grade_name_id=grade_name), year_id=year, class_obj_id=class_rec, subject_id=subject_name).exists():
                            flag_warning = True
                            continue
                        elif rubric_mapping_details.objects.filter(~Q(grade_name_id=grade_name), year_id=year, class_obj_id=class_rec, subject_id=subject_name,rubric_name_id=scheme_name).exists():
                            flag_warning = True
                            continue
                        else:
                            rubric_obj = rubric_mapping_details.objects.create(year_id=year, class_obj_id=class_rec, subject_id=subject_name,rubric_name_id=scheme_name, grade_name_id=grade_name)
                            for count in range(frequency_size):
                                rubric_frequency_obj = rubric_mapping_frequency.objects.create(frequency_name_id=frequency_mapping_ids[count])
                                rubric_obj.rubric_frequency_ids.add(rubric_frequency_obj.id)
                            flag_success=True
            if flag_warning:
                messages.warning(request,
                                 "Mapping Of This Records Is Already Found  With Some Other Grade Scheme.")
            if subject_not_mapped_flag:
                for list in (subject_not_mapped_list):
                    messages.warning(request, str(list['Class Name']) + " " + "is not mapped with subject" + " " + str(
                        list['Subject Name']))

            if flag_success:
                messages.success(request, "Record Mapped Successfully.")
        return redirect('/school/view_rubric_mapping/')
    except:
        messages.warning(request, "Some Error Occurred. Please Try again.")
        return redirect('/school/view_rubric_mapping/')

    # if request.method == 'POST':
    #     year_name = request.POST['year_name']
    #     class_name = request.POST.getlist('class_name')
    #     # section_name = request.POST.getlist('section_name')
    #     scheme_id = request.POST['scheme']
    #     grade_name = request.POST['grade_name']
    #     subject_name = request.POST['subject_name']
    #
    #     sub_rec = subjects.objects.get(id = subject_name)
    #     acd_recs = academic_class_section_mapping.objects.filter(Q(class_name_id=class_name,  year_name_id=year_name) & Q(section_name_id__in =section_name))
    #
    #     scheme_recs = rubric_mapping_details.objects.filter(rubric_name=scheme_id)
    #
    #
    #     # grade_recs = grade_details.objects.filter(scheme_name=grade_name)
    #     # ay_subject_scheme_recs = scheme_mapping_details.objects.filter(academic_class_section_mapping_id= acd_rec.id, scheme_name =scheme_id, grade_name=grade_name, subject_id = subject_name)
    #
    #     if scheme_recs:
    #         if scheme_recs[0].subject.id == long(subject_name):
    #             if acd_recs:
    #                 for acd_rec in acd_recs:
    #                     ay_subject_scheme_recs = rubric_mapping_details.objects.filter(grade_name=grade_name)
    #
    #                     if not ay_subject_scheme_recs:
    #                         rec = rubric_mapping_details.objects.filter(
    #                             academic_class_section_mapping_id=acd_rec.id, rubric_name=scheme_id,subject_id=subject_name)
    #
    #                         if rec:
    #                             messages.warning(request, "Rubric Scheme Is Already Mapped For That Class & Section With Diffrent Grade. ")
    #                             return redirect('/school/view_rubric_mapping/')
    #                         else:
    #                             if sub_rec.type == 'Optional':
    #                                 optionalSubjectList = []
    #                                 try:
    #                                     # student_recs = student_details.objects.filter(academic_class_section=acd_rec.id, is_active=True)
    #                                     student_recs = student_details.objects.filter(academic_class_section=acd_rec.id, is_active=True)
    #                                     for student_rec in student_recs:
    #                                         for subject_recs in student_rec.subject_ids.all():
    #                                             optionalSubjectList.append(subject_recs.id)
    #
    #                                     if long(subject_name) in optionalSubjectList:
    #                                         data = rubric_mapping_details.objects.filter(academic_class_section_mapping_id= acd_rec.id, rubric_name =scheme_id, grade_name=grade_name, subject_id = subject_name)
    #                                         if data:
    #                                             messages.warning(request, "Record Already Mapped, Try With New Records. ")
    #                                             pass
    #                                         else:
    #                                             rubric_mapping_details.objects.create(academic_class_section_mapping_id= acd_rec.id, rubric_name_id =scheme_id, grade_name_id=grade_name, subject_id = subject_name)
    #                                     else:
    #                                         pass
    #                                 except:
    #                                     messages.warning(request, "Record Not Saved. Some Error Occurred, Try Again. ")
    #                             else:
    #                                 try:
    #                                     mandatoray_subject_recs = mandatory_subject_mapping.objects.filter(academic_class_section_mapping_id = acd_rec.id, subject_id =subject_name)
    #
    #                                     if mandatoray_subject_recs:
    #                                         data = rubric_mapping_details.objects.filter(academic_class_section_mapping_id=acd_rec.id,
    #                                                                                      rubric_name=scheme_id, grade_name=grade_name,
    #                                                                                      subject_id=subject_name)
    #                                         if data:
    #                                             messages.warning(request, "Record Already Mapped, Try With New Records. ")
    #                                             pass
    #                                         else:
    #                                             rubric_mapping_details.objects.create(academic_class_section_mapping_id=acd_rec.id,
    #                                                                                   rubric_name_id=scheme_id, grade_name_id=grade_name,
    #                                                                                   subject_id=subject_name)
    #                                     else:
    #                                         pass
    #                                 except:
    #                                     messages.warning(request, "Record Not Saved. Some Error Occurred. Try Again. ")
    #                     else:
    #                         if ay_subject_scheme_recs[0].rubric_name_id == long(scheme_id):
    #                             # if ay_subject_scheme_recs[0].
    #                                 if sub_rec.type == 'Optional':
    #                                     optionalSubjectList = []
    #                                     try:
    #                                         # student_recs = student_details.objects.filter(academic_class_section=acd_rec.id, is_active=True)
    #                                         student_recs = student_details.objects.filter(academic_class_section=acd_rec.id,
    #                                                                                       is_active=True)
    #                                         for student_rec in student_recs:
    #                                             for subject_recs in student_rec.subject_ids.all():
    #                                                 optionalSubjectList.append(subject_recs.id)
    #
    #                                         if long(subject_name) in optionalSubjectList:
    #                                             data = rubric_mapping_details.objects.filter(
    #                                                 academic_class_section_mapping_id=acd_rec.id, rubric_name=scheme_id,
    #                                                 grade_name=grade_name,
    #                                                 subject_id=subject_name)
    #                                             if data:
    #                                                 messages.warning(request,
    #                                                                  "Record Already Mapped, Try With New Records. ")
    #                                                 pass
    #                                             else:
    #                                                 rubric_mapping_details.objects.create(
    #                                                     academic_class_section_mapping_id=acd_rec.id,
    #                                                     rubric_name_id=scheme_id, grade_name_id=grade_name,
    #                                                     subject_id=subject_name)
    #                                         else:
    #                                             pass
    #                                     except:
    #                                         messages.warning(request, "Record Not Saved. Some Error Occurred, Try Again. ")
    #                                 else:
    #                                     try:
    #                                         mandatoray_subject_recs = mandatory_subject_mapping.objects.filter(
    #                                             academic_class_section_mapping_id=acd_rec.id, subject_id=subject_name)
    #
    #                                         if mandatoray_subject_recs:
    #                                             data = rubric_mapping_details.objects.filter(
    #                                                 academic_class_section_mapping_id=acd_rec.id,
    #                                                 rubric_name=scheme_id, grade_name=grade_name,
    #                                                 subject_id=subject_name)
    #                                             if data:
    #                                                 messages.warning(request,
    #                                                                  "Record Already Mapped, Try With New Records. ")
    #                                                 pass
    #                                             else:
    #                                                 rubric_mapping_details.objects.create(
    #                                                     academic_class_section_mapping_id=acd_rec.id,
    #                                                     rubric_name_id=scheme_id, grade_name_id=grade_name,
    #                                                     subject_id=subject_name)
    #                                         else:
    #                                             pass
    #                                     except:
    #                                         messages.warning(request, "Record Not Saved. Some Error Occurred. Try Again. ")
    #                         else:
    #                             messages.warning(request,"A Rubrics Scheme Can Be Mapped With Only One Grade. Please Try Again. ")
    #         else:
    #             messages.warning(request, "A Rubrics Scheme Can Be Mapped With Only One Subject. Please Try Again. ")
    #     else:
    #         if acd_recs:
    #             for acd_rec in acd_recs:
    #                 if sub_rec.type == 'Optional':
    #                     optionalSubjectList = []
    #                     try:
    #                         # student_recs = student_details.objects.filter(academic_class_section=acd_rec.id, is_active=True)
    #                         student_recs = student_details.objects.filter(academic_class_section=acd_rec.id,
    #                                                                       is_active=True)
    #                         for student_rec in student_recs:
    #                             for subject_recs in student_rec.subject_ids.all():
    #                                 optionalSubjectList.append(subject_recs.id)
    #
    #                         if long(subject_name) in optionalSubjectList:
    #                             data = rubric_mapping_details.objects.filter(
    #                                 academic_class_section_mapping_id=acd_rec.id, rubric_name=scheme_id,
    #                                 grade_name=grade_name, subject_id=subject_name)
    #                             if data:
    #                                 messages.warning(request, "Record Already Mapped, Try With New Records. ")
    #                                 pass
    #                             else:
    #                                 rubric_mapping_details.objects.create(academic_class_section_mapping_id=acd_rec.id,
    #                                                                       rubric_name_id=scheme_id,
    #                                                                       grade_name_id=grade_name,
    #                                                                       subject_id=subject_name)
    #                         else:
    #                             pass
    #                     except:
    #                         messages.warning(request, "Record Not Saved. Some Error Occurred, Try Again. ")
    #                 else:
    #                     try:
    #                         mandatoray_subject_recs = mandatory_subject_mapping.objects.filter(
    #                             academic_class_section_mapping_id=acd_rec.id, subject_id=subject_name)
    #
    #                         if mandatoray_subject_recs:
    #                             data = rubric_mapping_details.objects.filter(
    #                                 academic_class_section_mapping_id=acd_rec.id,
    #                                 rubric_name=scheme_id, grade_name=grade_name,
    #                                 subject_id=subject_name)
    #                             if data:
    #                                 messages.warning(request, "Record Already Mapped, Try With New Records. ")
    #                                 pass
    #                             else:
    #                                 rubric_mapping_details.objects.create(academic_class_section_mapping_id=acd_rec.id,
    #                                                                       rubric_name_id=scheme_id,
    #                                                                       grade_name_id=grade_name,
    #                                                                       subject_id=subject_name)
    #                         else:
    #                             pass
    #                     except:
    #                         messages.warning(request, "Record Not Saved. Some Error Occurred. Try Again. ")
    #
    # return redirect('/school/view_rubric_mapping/')

@user_login_required
def ViewRubricMappingFilter(request):
    scheme_rec = []
    year_class_section_object = academic_class_section_mapping.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    grade_recs = grade_details.objects.filter(is_archive = False)

    if year_class_section_object:
        try:
            year_name = request.POST.get('year_name', None)
            if not year_name:
                current_academic_year = academic_year.objects.get(current_academic_year=1)
                selected_year_id = current_academic_year.id
            else:
                current_academic_year = academic_year.objects.get(id=year_name)
                selected_year_id = year_name

            mapping_list = rubric_mapping_details.objects.filter(year=selected_year_id)

            return render(request, 'view_rubric_mapping_filter.html',
                          { "year_list": all_academicyr_recs,
                           'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                            'grade_recs':grade_recs,'mapping_list':mapping_list})

        except academic_year.DoesNotExist:
            return render(request, 'view_rubric_mapping.html',
                          {"year_list": all_academicyr_recs})
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'rubrics_sub_tile.html')

@user_login_required
def EditRubricMapping(request):
    edit_mapping_data = request.POST
    if request.method == 'POST':
        request.session['edit_mapping_data'] = edit_mapping_data
        form_vals = load_edit_mapping_recs(edit_mapping_data, request)
    else:
        form_vals = load_edit_mapping_recs(request.session.get('edit_mapping_data'), request)
    return render(request, "view_edit_rubric_mapping.html", form_vals)

def load_edit_mapping_recs(edit_mapping_data, request):
    mapping_id = edit_mapping_data.get('mapping_id1')
    scheme_mapping_rec = rubric_mapping_details.objects.get(id=mapping_id)# It will be used in HTML page as a value of selected Class
    grading_recs = grade_details.objects.all()
    form_vals = {'scheme_mapping_rec': scheme_mapping_rec, 'grading_recs':grading_recs}
    return form_vals

def RubricMappingUpdate(request):
    year_name = request.POST.get('year_name')
    grade_name = request.POST.get('grade_name')
    class_name = request.POST.get('class_name')
    rubric_scheme_name = request.POST.get('rubric_scheme_name')
    subject_name = request.POST.get('subject_name')
    exist_rubric_mapping_rec = request.POST.get('exist_rubric_mapping_rec')

    try:
        if not rubric_academic_frequency_mapping.objects.filter(scheme_mapping_id = exist_rubric_mapping_rec).exists():
            scheme_existing_rec = rubric_mapping_details.objects.filter(subject_id=subject_name,
                                                                        grade_name_id=grade_name,
                                                                        rubric_name_id=rubric_scheme_name,
                                                                        class_obj_id=class_name, year_id=year_name)
            if scheme_existing_rec:
                if scheme_existing_rec[0].id == int(exist_rubric_mapping_rec):
                    rubric_mapping_details.objects.filter(id=exist_rubric_mapping_rec).update(grade_name_id=grade_name)
                    messages.success(request,"Record Updated")
                else:
                    messages.warning(request, "Record Not Updated. Mapping Already Found.")
            else:
                rubric_mapping_details.objects.filter(id=exist_rubric_mapping_rec).update(grade_name_id=grade_name)
                messages.success(request, "Record Updated")
        else:
            messages.warning(request, "Record Cannot be Updated. Marks Has Already Been Entered For This Mapping Record.")
    except:
        messages.warning(request, "Some Error Occurred. Please Try again.")
    return redirect('/school/edit_rubric_mapping/')

@user_login_required
def ExportRubricRecords(request):
    try:

        rubric_mapping_rec = rubric_mapping_details.objects.all()
        column_names = ['Year Name','Class','Section','Subject','Rubric Name','Grade Scheme']
        rows = rubric_mapping_rec.values_list('year__year_name','class_obj__class_name','subject__subject_name','rubric_name__scheme_name','grade_name__grade_name')

        return export_users_xls('rubrics_mapping', column_names, rows)

    except:
        messages.warning(request, "Some Error Occurred...")
    return redirect('/school/view_rubric_mapping/')

@user_login_required
def DeleteRubricMapping(request):
    try:
        mapping_id=request.GET.get('mapping_id')
        mark_entry = rubric_academic_frequency_mapping.objects.filter(scheme_mapping_id =mapping_id)
        if not  mark_entry:
            rubric_mapping_details.objects.filter(id=mapping_id).delete()
        else:
            messages.warning(request,"Record Not Deleted.. This May Be Already Used In Add Score Tables. ")
    except:
            messages.warning(request, "Record Not Deleted.. This May Be Used In Add Score Tables, Delete Them First And Try Again. ")
    return redirect('/school/view_rubric_mapping_filter/')

@user_login_required
def RubricMappingArchive(request,rec_id):
    try:
        mark_entry = rubric_academic_frequency_mapping.objects.filter(scheme_mapping_id = rec_id)
        scheme_mapping_rec = rubric_mapping_details.objects.get(id=rec_id)
        mapping_recs = rubric_mapping_details.objects.filter(year= scheme_mapping_rec.year, class_obj = scheme_mapping_rec.class_obj, rubric_name = scheme_mapping_rec.rubric_name, subject = scheme_mapping_rec.subject)

        if mapping_recs.count() == 1:
            if scheme_mapping_rec.is_archived == False:
                if not  mark_entry:
                    rubric_mapping_details.objects.filter(id=rec_id).update(is_archived=True)
                    messages.success(request, "Record is Archived.")
                else:
                    for rec in mark_entry:
                        rubric_academic_frequency_mapping.objects.filter(id=rec.id).update(is_archived=True)
                    rubric_mapping_details.objects.filter(id=rec_id).update(is_archived=True)
                    messages.success(request,"Record is Archived And Mark Entries For This Mapping Is Also Updated As Archived.")
            else:
                if not  mark_entry:
                    rubric_mapping_details.objects.filter(id=rec_id).update(is_archived=False)
                    messages.success(request, "Record is Un-Archived.")
                else:
                    for rec in mark_entry:
                        rubric_academic_frequency_mapping.objects.filter(id=rec.id).update(is_archived=False)
                    rubric_mapping_details.objects.filter(id=rec_id).update(is_archived=False)
                    messages.success(request,"Record is Un-Archived And Mark Entries For This Mapping Is Also Updated As Un-Archived.")
        else:
            if scheme_mapping_rec.is_archived == False:
                messages.warning(request, "Archive is not allowed for duplicate records.")
            else:
                messages.warning(request, "Un-Archive is not allowed for duplicate records.")
    except:
            messages.warning(request, "An Unexpected Error Occurred.")
    return redirect('/school/view_rubric_mapping_filter/')

@user_login_required
def ValidateRubricCodeExist(request):
    flag = None
    scheme_code = request.POST.get('scheme_code', None)
    if rubric_scheme.objects.filter(code=scheme_code).exists():
        flag = True
    else:
        flag=False
    # print "flag---> ", flag
    return JsonResponse(flag, safe=False)

@user_login_required
def ValidateRubricNameExist(request):
    flag = None
    scheme_name = request.POST.get('scheme_name', None)
    if rubric_scheme.objects.filter(scheme_name=scheme_name).exists():
        flag = True
    else:
        flag=False
    # print "flag---> ",flag
    return JsonResponse(flag, safe=False)




@user_login_required
def RubricDeleteSubTopic(request):
    # print "In function  DeleteSubTopic---------->"
    scheme_id = request.POST.get('scheme_id')
    topic_id = request.POST.get('topic_id')
    subtopic_id = request.POST.get('subtopic_id')
    topic_recs = rubric_topic.objects.get(id=topic_id)
    topic_recs.sub_topic_ids.remove(subtopic_id)
    rubric_sub_topic.objects.filter(id= subtopic_id).delete()
    return redirect('/school/edit_rubric_definition/'+scheme_id)


@user_login_required
def RubricDeleteTopic(request):
    # print "In function DeleteTopic------->"
    scheme_id = request.POST.get('scheme_id')
    substrand_id = request.POST.get('substrand_id')
    topic_id = request.POST.get('topic_id')

    substrand_rec = rubric_sub_strand.objects.get(id=substrand_id)
    substrand_rec.topic_ids.remove(topic_id)

    topic_recs = rubric_topic.objects.get(id=topic_id)

    for subtopic in topic_recs.sub_topic_ids.all():
        sub_top_id = subtopic.id
        rubric_sub_topic.objects.filter(id = sub_top_id).delete()
        topic_recs.sub_topic_ids.remove(sub_top_id)

    # print"--Top--"
    rubric_topic.objects.filter(id=topic_id).delete()

    return redirect('/school/edit_rubric_definition/'+scheme_id)


@user_login_required
def RubricDeleteSubStrand(request):
    # print "In function  DeleteSubStrand->"
    scheme_id = request.POST.get('scheme_id')
    stand_id = request.POST.get('strand_id')
    substrand_id = request.POST.get('substrand_id')

    strand_rec = rubric_strand.objects.get(id = stand_id)
    strand_rec.sub_strand_ids.remove(substrand_id)

    substrand_recs = rubric_sub_strand.objects.get(id=substrand_id)

    for topic_recs in substrand_recs.topic_ids.all():
        # print"uu"
        topic_rec = rubric_topic.objects.get(id=topic_recs.id)
        # topic_rec.sub_topic_ids.remove(substrand_recs.id)

        for subtopic in topic_rec.sub_topic_ids.all():
            sub_top_id = subtopic.id
            rubric_sub_topic.objects.filter(id=sub_top_id).delete()
            topic_rec.sub_topic_ids.remove(sub_top_id)
        rubric_topic.objects.filter(id=topic_rec.id).delete()
    rubric_sub_strand.objects.filter(id=substrand_id).delete()
        # print "Hh"

    # scheme_recs = scheme.objects.get(id=scheme_id)
    # scheme_recs.stand_ids.remove(stand_id)
    return redirect('/school/edit_rubric_definition/'+scheme_id)


# @user_login_required
# def RubricDeleteStrand(request):
#     # print "In function DeleteStrand--->"
#     scheme_id = request.POST.get('scheme_id')
#     stand_id = request.POST.get('stand_id')
#     scheme_recs = rubric_scheme.objects.get(id=scheme_id)
#     scheme_recs.stand_ids.remove(stand_id)
#     return redirect('/school/edit_rubric_definition/'+scheme_id)

@user_login_required
def RubricDeleteStrand(request):
    scheme_id = request.POST.get('scheme_id')
    stand_id = request.POST.get('stand_id')
    scheme_recs = rubric_scheme.objects.get(id=scheme_id)
    strand_obj = rubric_strand.objects.get(id=stand_id)

    scheme_recs.stand_ids.remove(strand_obj)

    for sub_strand_rec in strand_obj.sub_strand_ids.all():
        sub_strand_obj = rubric_sub_strand.objects.get(id=sub_strand_rec.id)

        for topic_rec in sub_strand_obj.topic_ids.all():
            topic_obj = rubric_topic.objects.get(id=topic_rec.id)
            sub_strand_obj.topic_ids.remove(topic_rec)

            for sub_topic_rec in topic_obj.sub_topic_ids.all():
                sub_top_id = sub_topic_rec.id
                rubric_sub_topic.objects.filter(id=sub_top_id).delete()
                topic_rec.sub_topic_ids.remove(sub_top_id)

            rubric_topic.objects.filter(id=topic_obj.id).delete()
            strand_obj.sub_strand_ids.remove(sub_strand_rec)
        rubric_sub_strand.objects.filter(id=sub_strand_rec.id).delete()
    rubric_strand.objects.filter(id=stand_id).delete()

    return redirect('/school/edit_exam_definition/' + scheme_id)



@user_login_required
def RubricGradeList(request):
    grade_list = rubric_grade_details.objects.filter(is_archive = False)
    return render(request, "rubric_grade_list.html" ,{'Gradelist':grade_list})


@user_login_required
def AddRubricGrade(request):
    return render(request, "add_rubric_grade.html")


@user_login_required
def SaveRubricGrade(request):

    code = request.POST.getlist('code')
    code_name = request.POST.getlist('code_name')
    greater_mark = request.POST.getlist('greater_mark')
    less_mark = request.POST.getlist('less_mark')
    grade = request.POST.getlist('grade')
    color = request.POST.getlist('color')

    if rubric_grade_details.objects.filter(grade_name=code_name[0]).exists():
        messages.success(request, "This Grade Name is Already Exists")
        return redirect('/school/rubric_grade_list/')

    form = rubric_grade_details(grade_name= code_name[0])
    form.save()

    # print 'form--', form.id
    grade_id = form.id

    if (grade_id >= 1) and (grade_id <= 9):
        grade_code = "G-000" + str(grade_id)
        rubric_grade_details.objects.filter(id = grade_id).update(code = grade_code)

    elif (grade_id >= 10) and (grade_id <= 99):
        grade_code = "G-00" + str(grade_id)
        rubric_grade_details.objects.filter(id=grade_id).update(code=grade_code)

    elif (grade_id >= 100) and (grade_id <= 999):
        grade_code = "G-0" + str(grade_id)
        rubric_grade_details.objects.filter(id=grade_id).update(code=grade_code)

    list_size = len(greater_mark)

    for count in range(list_size):
        grade_slab_form = rubric_grade_sub_details(grade_id = grade_id, marks_greater= greater_mark[count], marks_less = less_mark[count], grades =  grade[count],color_code = color[count] )
        grade_slab_form.save()
    return redirect('/school/rubric_grade_list/')


@user_login_required
def EditRubricGrade(request, grade_id):
    GradeSubDetail = rubric_grade_sub_details.objects.filter(grade = grade_id)
    GradeDetail = rubric_grade_details.objects.get(id = grade_id)
    return render(request, "rubric_grade_edit.html" ,{'GradeSubDetail':GradeSubDetail,'GradeDetail':GradeDetail})


@user_login_required
def SaveRubricEditGradeSub(request):
    if request.is_ajax():
        if request.method == 'POST':
            data = request.POST['table']
            dict_data = ast.literal_eval(data)

            for obj in dict_data:
                Id = obj['id']

                marks_greater = obj['marks_greater']
                marks_less = obj['marks_less']
                grades = obj['grades']
                # color_code = obj['color_code']
                # color = obj['color']

                if (marks_less == '') or (marks_greater == '') or (grades == ''):
                    messages.success(request, "Records Not Updated... Please fill all the records")
                else:
                    # messages.success(request, "Records Updated.")
                    rubric_grade_sub_details.objects.filter(id=Id).update(marks_greater=marks_greater, marks_less=marks_less,
                                                                   grades=grades)

    return redirect('/school/rubric_grade_list/')


@user_login_required
@user_permission_required('assessment.can_view_view_rubric_exam_mark', '/school/home/')
def RubricEnterMarkList(request):

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    user = request.user
    academic_frequency_rec = []
    mapped_class = []
    mapped_section = []
    if request.user.is_system_admin() or request.user.is_officer():
        for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
            mapped_class.append(mapped_object.class_name)
            mapped_section.append(mapped_object.section_name)
        academic_frequency_rec = rubric_academic_frequency_mapping.objects.all()
    else:
        if request.user.is_supervisor():
            supervisor_list = []
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj in academic_supervisor_class_section_mapping.objects.filter(academic_class_section_mapping=mapped_object, supervisor_id=user.id):
                    if obj:
                        for obj in rubric_academic_frequency_mapping.objects.filter(academic_mapping=mapped_object):
                            if obj not in supervisor_list:
                                supervisor_list.append(obj)
                                academic_frequency_rec.append(obj)

        elif request.user.is_subject_teacher():
            subject_teacher_list = []
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj in teacher_subject_mapping.objects.filter(academic_class_section_mapping=mapped_object,
                                                                  staff_id=request.user.id):
                    if obj:
                        for obj in rubric_academic_frequency_mapping.objects.filter(academic_mapping=mapped_object):
                            if obj not in subject_teacher_list:
                                subject_teacher_list.append(obj)
                                academic_frequency_rec.append(obj)
        else:
            academic_frequency_rec = rubric_academic_frequency_mapping.objects.filter(
                Q(academic_mapping__assistant_teacher=user.id) | Q(academic_mapping__staff_id=user.id))
    return render(request, "rubric_enter_mark_list.html", {'mapping_rec':academic_frequency_rec})


def RubricEnterMark(request):
    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    rubrics_category_recs = rubrics_category.objects.all()
    scheme_recs = rubric_scheme.objects.all()
    frequency_recs = frequency_details.objects.all()
    current_academic_year = None
    selected_year_id = None

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
    except:
        messages.error(request, "Please add academic year first.")
        return render(request, "rubric_enter_marks.html", {'class_list': all_class_recs, 'section_list': all_section_recs, "year_list": all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})
    return render(request, "rubric_enter_marks.html", {'class_list': all_class_recs, 'section_list': all_section_recs, "year_list": all_academicyr_recs,'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,  'scheme_recs': scheme_recs,'frequency_recs':frequency_recs,'rubrics_category_recs':rubrics_category_recs})

@user_login_required
def RubricMappedScheme(request):
    schemeList = []
    schemeIds = []
    academic_year = request.GET.get('academic_year', None)
    class_name = request.GET.get('class_name', None)
    section_name = request.GET.get('section_name', None)

    acd_mapping_rec = academic_class_section_mapping.objects.get(class_name_id=class_name, section_name_id=section_name,year_name_id=academic_year)
    scheme_recs = rubric_mapping_details.objects.filter(academic_class_section_mapping_id = acd_mapping_rec.id)
    for scheme_rec in scheme_recs:
        schemeIds.append(scheme_rec.rubric_name.id)
    schemeIds = set(schemeIds)

    schemes = rubric_scheme.objects.filter(id__in = schemeIds)
    dict = {}
    for schem_data in schemes:
        dict={'id':schem_data.id, 'scheme_name': schem_data.scheme_name}
        schemeList.append(dict)
    return JsonResponse(schemeList,safe=False)



@user_login_required
def RubricStudenMarktList(request):
    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()


    if request.POST:
        request.session['exam_add_data'] = request.POST
        selected_year_id = request.POST.get('year_name')  # It will be used in HTML page as a value of selected Class
        selected_class_id = request.POST.get('class_name')  # It will be used in HTML page as a value of selected Class
        selected_section_id = request.POST.get('section_name')  # It will be used in HTML page as a value of selected Section
        selected_subject_id = request.POST.get('subject_name')  # It will be used in HTML page as a value of selected Section
        selected_rubrics_catgory_id = request.POST.get('rubrics_catgory')
        frequency_name = request.POST.get('frequency_name')
    else:
        selected_year_id = request.session['exam_add_data'].get('year_name')  # It will be used in HTML page as a value of selected Class
        selected_class_id = request.session['exam_add_data'].get('class_name')  # It will be used in HTML page as a value of selected Class
        selected_section_id = request.session['exam_add_data'].get('section_name')  # It will be used in HTML page as a value of selected Section
        selected_subject_id = request.session['exam_add_data'].get('subject_name')  # It will be used in HTML page as a value of selected Section
        selected_rubrics_catgory_id = request.session['exam_add_data'].get('rubrics_catgory')  # It will be used in HTML page as a value of selected Section
        frequency_name = request.session['exam_add_data'].get('frequency_name')




    #selected_year_id = request.POST.get('year_name')  # It will be used in HTML page as a value of selected Class
    selected_year_rec = academic_year.objects.get(id=selected_year_id)
    selected_year_name = selected_year_rec.year_name

   # selected_class_id = request.POST.get('class_name')  # It will be used in HTML page as a value of selected Class
    selected_class_rec = class_details.objects.get(id=selected_class_id)
    selected_class_name = selected_class_rec.class_name

   # selected_section_id = request.POST.get('section_name')  # It will be used in HTML page as a value of selected Section
    selected_section_rec = sections.objects.get(id=selected_section_id)
    selected_section_name = selected_section_rec.section_name

    #selected_subject_id = request.POST.get('subject_name')  # It will be used in HTML page as a value of selected Section
    selected_subject_rec = subjects.objects.get(id=selected_subject_id)
    selected_subject_name = selected_subject_rec.subject_name

    #selected_rubrics_catgory_id = request.POST.get('rubrics_catgory')  # It will be used in HTML page as a value of selected Section
    selected_rubrics_catgory_rec = rubrics_category.objects.get(id=selected_rubrics_catgory_id)
    selected_selected_rubrics_name = selected_rubrics_catgory_rec.rubric_catgory
    rubric_catgory_na_obj = rubrics_master.objects.filter(rubrics_category_name=selected_rubrics_catgory_rec.id)[0]

    year_class_section_obj1 = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                        section_name=selected_section_id,
                                                                        class_name=selected_class_id)
    try:
        scheme_rec_count = rubric_mapping_details.objects.filter(year_id=selected_year_id, class_obj_id = selected_class_id,subject_id=selected_subject_rec).count()
        scheme_rec = rubric_mapping_details.objects.get(year_id=selected_year_id, class_obj_id = selected_class_id,subject_id=selected_subject_rec,is_archived=False)
        scheme_id = scheme_rec.rubric_name.id
    except:
        if scheme_rec_count >0:
            messages.warning(request, "New Mapping Not Found For The Selected Record")
            return redirect('/school/enter_rubric_mark/')

        messages.warning(request, "Mapping Not Found For Selected Subject")
        return redirect('/school/enter_rubric_mark/')

    # scheme_id = request.POST.get('scheme_name')
    #frequency_name = request.POST.get('frequency_name')
    frequency_data = frequency_details.objects.get(id = frequency_name)
    selected_scheme = rubric_scheme.objects.get(id =scheme_id)
    #grade_colors = rubric_grade_sub_details.objects.filter(grade=scheme_id.grade_name)

    subtopics_recs = []

    for starand in selected_scheme.stand_ids.all():
            for sub_strand_recs in starand.sub_strand_ids.all():
                if sub_strand_recs:
                    for topic_recs in sub_strand_recs.topic_ids.all():
                        if topic_recs:
                            subtopics_recs.append(topic_recs)
                            # for subtopics in topic_recs.sub_topic_ids.all():
                            #     subtopics_recs.append(subtopics)


    year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id, section_name=selected_section_id, class_name=selected_class_id)
    # try:
    #     scheme_rec = rubric_mapping_details.objects.get(academic_class_section_mapping_id= year_class_section_obj,subject=selected_subject_id)
    # except:
    #     messages.warning(request,  "Mapping Not Found for Selected Subject")
    #     return redirect('/school/enter_rubric_mark/')

    mark_recs =  rubric_academic_frequency_mapping.objects.filter(academic_mapping_id=year_class_section_obj1.id,scheme_mapping_id=scheme_rec.id, frequency_id = frequency_name)
    grade_colors = grade_sub_details.objects.filter(grade=scheme_rec.grade_name)
    rubric_recs = rubrics_master.objects.filter(rubrics_category_name=selected_rubrics_catgory_rec)

    if mark_recs:
        messages.warning(request,"Mark already entered, Update the entered marks.")
        # return redirect('/school/enter_rubric_mark/')
        return redirect('/school/rubric_update_entered_mark/' + str(mark_recs[0].id))



    filtered_students =[]

    if scheme_rec.subject.type == 'Optional':
        optional_subject_students = scheme_rec.subject.optional_subject_relation.all()
        for students in optional_subject_students:
            student_rec = student_details.objects.filter(id=students.id, is_active=True, academic_class_section_id = year_class_section_obj.id)
            if student_rec:
                filtered_students.append(student_rec[0])
    else:
        filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True).order_by('first_name')

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    selected_year_id = current_academic_year.id
    scheme_recs = rubric_scheme.objects.all()
    frequency_recs = frequency_details.objects.all()

    #    Passing values to the HTML page through form_vals
    form_vals = {
        'student_list': filtered_students,
        'Selected_Section_Id': selected_section_id,
        'selected_section_name': selected_section_name,
        'selected_section': selected_section_rec,
        'section_list': all_section_recs,

        'selected_class_id': selected_class_id,
        'selected_class_name': selected_class_name,
        'selected_class': selected_class_rec,
        'class_list': all_class_recs,

        'selected_subject_id': selected_subject_id,
        'selected_subject_name': selected_subject_rec,

        'selected_rubrics_catgory_id': selected_rubrics_catgory_id,
        'selected_selected_rubrics_name': selected_selected_rubrics_name,



        'Selected_Year_Id': selected_year_id,
        'selected_year_name': selected_year_name,
        'selected_year': selected_year_rec,
        'year_list': all_academicyr_recs,

        'frequency_recs': frequency_recs,
        'frequency_data': frequency_data,
        'scheme_recs': scheme_recs,
        'subtopics_recs': subtopics_recs,
        'selected_scheme': selected_scheme,
        'grade_colors': grade_colors,
        'rubric_recs': rubric_recs,
        'rubric_catgory_na': rubric_catgory_na_obj,
    }
    return render(request, "rubric_enter_marks.html",form_vals)


@user_login_required
def SaveRubricMark(request):
    if request.method == 'POST':
        year_id = request.POST.get('acd_year')
        class_id = request.POST.get('class')
        section_id = request.POST.get('section')
        subject_id = request.POST.get('subject')
        #scheme_id = request.POST.get('scheme')
        frequency_id= request.POST.get('frequency')
        rubrics_catgory_id = request.POST.get('category')
        academic_mapping = academic_class_section_mapping.objects.get(year_name=year_id, class_name=class_id,section_name=section_id)
        try:
            scheme_rec_count = rubric_mapping_details.objects.filter(year_id=year_id,class_obj_id=class_id,subject_id=subject_id).count()
            scheme_rec = rubric_mapping_details.objects.get(year_id=year_id, class_obj_id=class_id,subject_id=subject_id, is_archived=False)
            scheme_id = scheme_rec.rubric_name.id
        except:
            if scheme_rec_count > 0:
                messages.warning(request, "New Mapping Not Found For The Selected Record")
                return redirect('/school/enter_rubric_mark/')

            messages.warning(request, "Mapping Not Found For Selected Subject")
            return redirect('/school/enter_rubric_mark/')
        mark_recs = rubric_academic_frequency_mapping.objects.filter(academic_mapping=academic_mapping.id,scheme_mapping_id=scheme_rec.id,frequency_id=frequency_id)
        if mark_recs:
            messages.warning(request, "Mark already entered, Update the entered marks.")
            return redirect('/school/rubric_update_entered_mark/' + str(mark_recs[0].id))


        category_instance = rubrics_category.objects.get(id=rubrics_catgory_id)
        scheme_rec1 = rubric_mapping_details.objects.get(year_id=year_id, class_obj_id=class_id,subject_id=subject_id,is_archived=False)
        scheme_id = scheme_rec1.rubric_name.id

        data = json.loads(request.POST.get('data'))
        scheme_rec = rubric_scheme.objects.get(id=scheme_id)
        subtopic_list = []

        for starand in scheme_rec.stand_ids.all():
            for sub_strand_recs in starand.sub_strand_ids.all():
                if sub_strand_recs:
                    for topic_recs in sub_strand_recs.topic_ids.all():
                        if topic_recs:
                            subtopic_list.append(topic_recs)
                            # for subtopics in topic_recs.sub_topic_ids.all():
                            #     subtopic_list.append(subtopics)

        # seheme_mapping_rec = rubric_mapping_details.objects.get(year_id=year_id, class_obj_id=class_id, rubric_name=scheme_id)
        academic_frequency_mapping_obj = rubric_academic_frequency_mapping.objects.create(academic_mapping=academic_mapping,scheme_mapping_id = scheme_rec1.id, rubrics_category_name=category_instance,frequency_id=frequency_id )

        for track, rec in  enumerate(data):
            student_mark_mapping_obj = rubric_student_mark_mapping.objects.create(student_id = rec['studentId'], total_mark = rec['totalMarks'])
            academic_frequency_mapping_obj.student_mark_ids.add(student_mark_mapping_obj.id)

            for count , subtopic in enumerate(subtopic_list):
                count = count+1
                try:
                    opt_val = rubric_sub_topic.objects.get(id=rec['subtopicMark_' + str(count)])
                    ques_val = rubric_topic.objects.get(id=rec['sub_topic_id_' + str(count)])
                    scheme_subtopic_obj = rubric_options.objects.create(options=opt_val,questions = ques_val)
                    student_mark_mapping_obj.student_option_ids.add(scheme_subtopic_obj)
                    # rubric_student_mark_mapping_rubric_options.objects.create(student_mark_mapping=student_mark_mapping_obj, rubric_options=scheme_subtopic_obj)
                except:
                    ques_val = rubric_topic.objects.get(id=rec['sub_topic_id_' + str(count)])
                    scheme_subtopic_obj = rubric_options.objects.create(questions=ques_val)
                    student_mark_mapping_obj.student_option_ids.add(scheme_subtopic_obj)

        return redirect('/school/rubric_update_entered_mark/'+str(academic_frequency_mapping_obj.id))

@user_login_required
def RubricUpdateEnteredMark(request, rec_id):

    academic_frequency_rec = rubric_academic_frequency_mapping.objects.get(id = rec_id)
    rubrics_category = academic_frequency_rec.rubrics_category_name.id
    rubric_catgory_na_obj = rubrics_master.objects.filter(rubrics_category_name=rubrics_category)[0]

    subtopics_recs = []
    subtopics_ids = []
    subtopic_recs =[]
    list = []

    strand_dict_list = []
    strand_list = []
    sub_strand_list = []
    strand_dict_id = []
    sub_strand_dict_id = []
    sub_strand_dict_list = []


    scheme_rec = rubric_scheme.objects.get(id=academic_frequency_rec.scheme_mapping.rubric_name.id)
    subtopic_list = []

    for starand_rec in scheme_rec.stand_ids.all():
        strand_dict = {}
        strand_temp_dict = {}
        strand_temp_dict_list = []

        strand_temp_dict['strand_name'] = starand_rec.strand_name
        strand_temp_dict['id'] = starand_rec.id

        strand_list.append(strand_temp_dict)

        if starand_rec.id not in strand_dict_id:
            strand_dict_id.append(starand_rec.id)

            strand_dict['strand_name'] = starand_rec.strand_name
            # strand_dict['sub_strand_name'] = 0
            strand_dict['substrand_count'] = 0


        for sub_strand_recs in starand_rec.sub_strand_ids.all():
            sub_strand_dict = {}

            if sub_strand_recs.id not in sub_strand_dict_id:
                sub_strand_list.append(sub_strand_recs)
                sub_strand_dict_id.append(sub_strand_recs.id)
                sub_strand_dict['sub_strand_name'] = sub_strand_recs.sub_strand_name
                sub_strand_dict['topic_count'] = sub_strand_recs.topic_ids.all().count()
                strand_temp_dict_list.append(sub_strand_dict)
                sub_strand_dict_list.append(sub_strand_dict)
                strand_dict['substrand_count'] += sub_strand_recs.topic_ids.all().count()
        strand_dict_list.append(strand_dict)



            # if sub_strand_recs:
            #     for topic_recs in sub_strand_recs.topic_ids.all():
            #         if topic_recs:
            #             subtopic_list.append(topic_recs)

    for starand_rec in scheme_rec.stand_ids.all():
        for sub_strand_recs in starand_rec.sub_strand_ids.all():
            if sub_strand_recs:
                for topic_recs in sub_strand_recs.topic_ids.all():
                    if topic_recs:
                        subtopic_list.append(topic_recs)

    grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)


    if academic_frequency_rec.is_submitted is True:
        color_code = '#1ce034'
    else:
        color_code = '#ffa012'
    rubric_recs = rubrics_master.objects.filter(rubrics_category_name=rubrics_category)
    form_vals = {
        # 'student_list': filtered_students,
        'Selected_Section_Id': academic_frequency_rec.academic_mapping.section_name.id,
        'selected_section_name': academic_frequency_rec.academic_mapping.section_name,
        'selected_section': academic_frequency_rec.academic_mapping.section_name,
        # 'section_list': all_section_recs,

        'Selected_Class_Id': academic_frequency_rec.academic_mapping.class_name.id,
        'selected_class_name': academic_frequency_rec.academic_mapping.class_name.class_name,
        # 'selected_class': selected_class_rec,
        # 'class_list': all_class_recs,

        'Selected_Year_Id': academic_frequency_rec.academic_mapping.year_name.id,
        'selected_year_name': academic_frequency_rec.academic_mapping.year_name.year_name,
        'selected_year': academic_frequency_rec.academic_mapping.year_name,
        # 'year_list': all_academicyr_recs,

        'frequency_recs': academic_frequency_rec.frequency,
        'frequency_data':  academic_frequency_rec.frequency.id,

        'category_recs': academic_frequency_rec.rubrics_category_name,



        'scheme_recs': academic_frequency_rec.scheme_mapping.rubric_name,
        'subject_recs': academic_frequency_rec.scheme_mapping.subject,
        'subtopics_recs': subtopics_recs,
        # 'selected_scheme': selected_scheme,

        'academic_frequency_rec': academic_frequency_rec,
        'subtopic_data': subtopic_recs,
        'color_code':color_code,
        'grade_colors':grade_colors,
        'rubric_recs':rubric_recs,
        # 'subtopic_recs':subtopic_recs,
        'subtopic_list':subtopic_list,
        'rubric_catgory_na': rubric_catgory_na_obj,

        'strand_dict_list':strand_dict_list,
        'sub_strand_dict_list':sub_strand_dict_list,
        'filter_flag': True,
        'strand_list':strand_list,
        'sub_strand_list':sub_strand_list,


    }
    return render(request, "rubric_edit_entered_mark.html",form_vals)


###---------- Before Rubric Strand and Topic Filter backup ---------------
# @user_login_required
# def RubricUpdateEnteredMark(request, rec_id):
#
#     academic_frequency_rec = rubric_academic_frequency_mapping.objects.get(id = rec_id)
#     rubrics_category = academic_frequency_rec.rubrics_category_name.id
#     rubric_catgory_na_obj = rubrics_master.objects.filter(rubrics_category_name=rubrics_category)[0]
#
#     subtopics_recs = []
#     subtopics_ids = []
#     subtopic_recs =[]
#     list = []
#
#     scheme_rec = rubric_scheme.objects.get(id=academic_frequency_rec.scheme_mapping.rubric_name.id)
#     subtopic_list = []
#
#     for starand in scheme_rec.stand_ids.all():
#         for sub_strand_recs in starand.sub_strand_ids.all():
#             if sub_strand_recs:
#                 for topic_recs in sub_strand_recs.topic_ids.all():
#                     if topic_recs:
#                         subtopic_list.append(topic_recs)
#
#     grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)
#
#
#     if academic_frequency_rec.is_submitted is True:
#         color_code = '#1ce034'
#     else:
#         color_code = '#ffa012'
#     rubric_recs = rubrics_master.objects.filter(rubrics_category_name=rubrics_category)
#     form_vals = {
#         # 'student_list': filtered_students,
#         'Selected_Section_Id': academic_frequency_rec.academic_mapping.section_name.id,
#         'selected_section_name': academic_frequency_rec.academic_mapping.section_name,
#         'selected_section': academic_frequency_rec.academic_mapping.section_name,
#         # 'section_list': all_section_recs,
#
#         'Selected_Class_Id': academic_frequency_rec.academic_mapping.class_name.id,
#         'selected_class_name': academic_frequency_rec.academic_mapping.class_name.class_name,
#         # 'selected_class': selected_class_rec,
#         # 'class_list': all_class_recs,
#
#         'Selected_Year_Id': academic_frequency_rec.academic_mapping.year_name.id,
#         'selected_year_name': academic_frequency_rec.academic_mapping.year_name.year_name,
#         'selected_year': academic_frequency_rec.academic_mapping.year_name,
#         # 'year_list': all_academicyr_recs,
#
#         'frequency_recs': academic_frequency_rec.frequency,
#         'frequency_data':  academic_frequency_rec.frequency.id,
#
#         'category_recs': academic_frequency_rec.rubrics_category_name,
#
#
#
#         'scheme_recs': academic_frequency_rec.scheme_mapping.rubric_name,
#         'subject_recs': academic_frequency_rec.scheme_mapping.subject,
#         'subtopics_recs': subtopics_recs,
#         # 'selected_scheme': selected_scheme,
#
#         'academic_frequency_rec': academic_frequency_rec,
#         'subtopic_data': subtopic_recs,
#         'color_code':color_code,
#         'grade_colors':grade_colors,
#         'rubric_recs':rubric_recs,
#         # 'subtopic_recs':subtopic_recs,
#         'subtopic_list':subtopic_list,
#         'rubric_catgory_na': rubric_catgory_na_obj,
#
#     }
#     return render(request, "rubric_edit_entered_mark.html",form_vals)




@user_login_required
def UpdateRubricMark(request):

    if request.method == 'POST':
        year_id = request.POST.get('acd_year')
        class_id = request.POST.get('class')
        section_id = request.POST.get('section')
        # scheme_id = request.POST.get('scheme')
        subject_id = request.POST.get('subject')

        frequency_id= request.POST.get('frequency')
        cat_name_id = request.POST.get('cat_name')
        sub_list_check = json.loads(request.POST.get('sub_list_check'))

        seheme_mapping_rec = rubric_mapping_details.objects.get(year_id=year_id, class_obj_id=class_id,subject=subject_id,is_archived=False)

        try:
            data = json.loads(request.POST.get('data'))
            academic_frequency_mapping_obj = rubric_academic_frequency_mapping.objects.get(scheme_mapping_id = seheme_mapping_rec.id, frequency_id=frequency_id)
            ac_id = academic_frequency_mapping_obj.id

            data_count = 0
            all_recs = academic_frequency_mapping_obj.student_mark_ids.all()

            if sub_list_check:
                for recs in all_recs:
                    # recs.total_mark = data[data_count]['totalMarks']
                    # recs.save()
                    for count, subtopic in enumerate(recs.student_option_ids.all()):
                        count = count + 1
                        if str(subtopic.questions.id) in sub_list_check:
                            try:
                                rec = rubric_sub_topic.objects.get(id=data[data_count]['subtopicMark_' + str(count)])
                                subtopic.options = rec
                                subtopic.save()
                            except:
                                subtopic.options = None
                                subtopic.save()
                    data_count += 1

            else:

                for recs in all_recs:
                    recs.total_mark = data[data_count]['totalMarks']
                    recs.save()
                    for count, subtopic in enumerate(recs.student_option_ids.all()):
                        count = count + 1
                        try:
                            rec = rubric_sub_topic.objects.get(id=data[data_count]['subtopicMark_' + str(count)])
                            subtopic.options = rec
                            subtopic.save()
                        except:
                            subtopic.options = None
                            subtopic.save()
                    data_count += 1

            rubric_academic_frequency_mapping.objects.filter(scheme_mapping_id = seheme_mapping_rec.id).update(updated_on=str(datetime.now()))
            messages.success(request, "Record Updated Successfully.")
            return redirect('/school/rubric_update_entered_mark/' + str(ac_id))
        except:
            messages.warning(request, "Some Error Occurred. Please Try Again.")
        return redirect('/school/rubrics_sub_tile/')

@user_login_required
def SubmitRubricMark(request):
    rec_id = request.POST.get('rec_id')
    rubric_academic_frequency_mapping.objects.filter(id=rec_id).update(is_submitted = True, submitted_by_id = request.user.id, updated_on=str(datetime.now()))
    messages.success(request, "Record Successfully Submitted.")
    return redirect('/school/rubric_enter_mark_list/')

@user_login_required
@user_permission_required('assessment.can_view_view_rubric_approval_list', '/school/home/')
def ViewRubricAprovalList(request):
    # print  "In view_aproval_list function"

    all_academicyr_recs = academic_year.objects.filter(is_active=True, current_academic_year=1)
    frequency_recs = frequency_details.objects.all()
    year_class_section_object = academic_class_section_mapping.objects.all()
    class_list = class_details.objects.all()
    section_list = sections.objects.all()

    if year_class_section_object:
        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id
            class_list = current_academic_year_mapped_classes(current_academic_year, request.user)
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=selected_year_id).values_list('id')

            scheme_recs = rubric_mapping_details.objects.filter(year=current_academic_year,is_archived=False).values_list('id')
            scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(Q(scheme_mapping_id__in = scheme_recs,is_archived=False) & Q( is_submitted = True, is_apprpved=False))

            rec_list =[]
            for scheme_mark_rec in scheme_mark_recs:
                supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(academic_class_section_mapping = scheme_mark_rec.academic_mapping, subject = scheme_mark_rec.scheme_mapping.subject)
                teacher_recs = teacher_subject_mapping.objects.filter(academic_class_section_mapping = scheme_mark_rec.academic_mapping, subject = scheme_mark_rec.scheme_mapping.subject)
                mapped_data = {
                    'supervisor_recs':supervisor_recs,
                    'teacher_recs':teacher_recs,
                    'scheme_mark_rec': scheme_mark_rec
                }
                rec_list.append(mapped_data)
            return render(request, 'rubric_aproval_list.html', {'class_list':class_list,'section_list':section_list,'scheme_mark_recs':rec_list,"year_list": all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,'frequency_recs':frequency_recs})

        except academic_year.DoesNotExist:
            messages.warning(request, "Academic Year Does not Exist.")
            return render(request, 'rubrics_sub_tile.html')
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
    return render(request, "rubrics_sub_tile.html")

@user_login_required
def ViewSubjectRubric(request):
    finalDict = []
    subject_name = request.POST.get('subject_name', None)
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name')

    ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name, year_name_id=year_name, section_name_id=section_name)

    scheme_mapping_rec = rubric_mapping_details.objects.filter(academic_class_section_mapping_id=ac_recs.id,subject_id=subject_name,is_archived=False)
    # print "-->"
    ab = {}
    for rec in scheme_mapping_rec:
        if not any(d['id'] == rec.rubric_name_id for d in finalDict):
            ab={'id':rec.rubric_name_id, 'value': rec.rubric_name.scheme_name}
            finalDict.append(ab)
    return JsonResponse(finalDict,safe=False)


def ViewSubjectRubricFrequency(request):
    finalDict = []
    subject_name = request.POST.get('subject_name', None)
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name', None)
    scheme_name = request.POST.get('id_scheme', None)

    ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name, year_name_id=year_name, section_name_id=section_name)

    scheme_mapping_rec = rubric_mapping_details.objects.get(academic_class_section_mapping_id=ac_recs.id, subject_id=subject_name, rubric_name=scheme_name,is_archived=False)
    fr_map_recs = rubric_academic_frequency_mapping.objects.filter(scheme_mapping_id=scheme_mapping_rec.id)

    ab = {}
    for rec in fr_map_recs:
        if not any(d['id'] == rec.frequency_id for d in finalDict):
            ab={'id':rec.frequency_id, 'value': rec.frequency.frequency_name}
            finalDict.append(ab)
    return JsonResponse(finalDict,safe=False)

def load_rubric_approve_status_function(run_app_val_dict,request):
    all_academicyr_recs = academic_year.objects.all()
    year_class_section_object = academic_class_section_mapping.objects.all()
    scheme_mapping_recs = scheme_mapping_details.objects.all()
    frequency_recs = frequency_details.objects.all()
    error_flag_status =False

    year_name = run_app_val_dict.get('year_name', None)
    class_name = run_app_val_dict.get('class_name', None)
    section_name = run_app_val_dict.get('section_name', None)
    # scheme_name = run_app_val_dict.get('scheme', None)
    subject_name = run_app_val_dict.get('subject_name', None)
    frequency_name = run_app_val_dict.get('frequency_name', None)

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    selected_year_id = current_academic_year.id
    class_list = current_academic_year_mapped_classes(selected_year_id,request.user)
    section_list = current_class_mapped_sections(current_academic_year, class_name,request.user)

    section_rec = sections.objects.get(id=section_name)
    # scheme_rec = rubric_scheme.objects.get(id=scheme_name)
    subjects_rec = subjects.objects.get(id=subject_name)
    class_rec = class_details.objects.get(id=class_name)
    section_rec = sections.objects.get(id=section_name)
    frequency_rec = frequency_details.objects.get(id=frequency_name)
    #frequency=rubric_mapping_frequency.objects.get(frequency_name=frequency_rec)
    ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name, year_name_id=year_name,
                                                         section_name_id=section_name)
    try:
        frequency_recs = []
        ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name, year_name_id=year_name,
                                                             section_name_id=section_name)

        rubric_map_recs = rubric_academic_frequency_mapping.objects.filter(academic_mapping=ac_recs,scheme_mapping__subject_id=subject_name)

        ab = {}
        for rub_rec in rubric_map_recs:
            for rec in rub_rec.scheme_mapping.rubric_frequency_ids.all():
                if not any(d['id'] == rec.frequency_name.id for d in frequency_recs):
                    ab = {'id': rec.frequency_name.id, 'frequency_name': rec.frequency_name.frequency_name}
                    frequency_recs.append(ab)

        get_rubrics_names = rubric_mapping_details.objects.get(year_id=year_name,subject_id=subjects_rec,class_obj_id=class_name )
        get_rubrics_name_frequency=get_rubrics_names.rubric_frequency_ids.get(frequency_name=frequency_rec)
        get_rubrics_name=rubric_mapping_details.objects.get(year_id=year_name,subject_id=subjects_rec,class_obj_id=class_name,rubric_frequency_ids=get_rubrics_name_frequency)

    except:
        form_val={}
        error_flag_status = True
        return form_val, error_flag_status

    get_rubrics_id = get_rubrics_name.rubric_name.id
    scheme_rec = rubric_scheme.objects.get(id=get_rubrics_id)

    seheme_mapping_rec = rubric_mapping_details.objects.filter(year_id=year_name,class_obj_id=class_name,
                                                            rubric_name=get_rubrics_id,subject_id=subjects_rec)
    rec_list=[]
    for rec in seheme_mapping_rec:
        if rec.id not in rec_list:
            rec_list.append(rec.id)
    scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(scheme_mapping_id__in=rec_list,
                                                                        frequency_id=frequency_name, is_submitted=True,
                                                                        is_apprpved=False,is_archived=False)
    # subjectDict = []
    # subjectRecs = []
    # optionalSubject = []
    #
    # subject_recs = mandatory_subject_mapping.objects.filter(academic_class_section_mapping_id=ac_recs.id)
    #
    # ab = {}
    # for subject in subject_recs:
    #     subjectDict.append(subject.subject_id)
    #
    # subjectDict = set(subjectDict)
    # subject_data = subjects.objects.filter(id__in=subjectDict)
    #
    # for subject in subject_data:
    #     ab = {'id': subject.id, 'subject_name': subject.subject_name}
    #     subjectRecs.append(ab)
    #
    # student_recs = student_details.objects.filter(academic_class_section_id=ac_recs)
    # for student_rec in student_recs:
    #     for subject in student_rec.subject_ids.all():
    #         optionalSubject.append(subject)
    #
    # optionalSubject = set(optionalSubject)
    # for subject in optionalSubject:
    #     ab = {'id': subject.id, 'subject_name': subject.subject_name}
    #     subjectRecs.append(ab)
    scheme_mapped_subject_list = []
    scheme_mark_recs_subject = rubric_academic_frequency_mapping.objects.filter(
        academic_mapping_id=ac_recs.id)
    for scheme_mark_rec in scheme_mark_recs_subject:
        scheme_mapped_subject_list.append(scheme_mark_rec.scheme_mapping.subject)
    scheme_mapped_subject_list = set(scheme_mapped_subject_list)

    rec_list = []
    for scheme_mark_rec in scheme_mark_recs:
        acd_rec=academic_class_section_mapping.objects.filter(class_name=scheme_mark_rec.scheme_mapping.class_obj,
                                                           year_name=scheme_mark_rec.scheme_mapping.year,
                                                              section_name=section_rec)
        supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
            academic_class_section_mapping=acd_rec[0],

            subject=scheme_mark_rec.scheme_mapping.subject)
        teacher_recs = teacher_subject_mapping.objects.filter(
            academic_class_section_mapping=acd_rec[0],
            subject=scheme_mark_rec.scheme_mapping.subject)
        mapped_data = {
            'supervisor_recs': supervisor_recs,
            'teacher_recs': teacher_recs,
            'scheme_mark_rec': scheme_mark_rec
        }
        rec_list.append(mapped_data)

    form_vals = {
        'frequency_rec': frequency_rec,
        'scheme_rec': scheme_rec,
        'section_rec': section_rec,
        "year_list": all_academicyr_recs,
        'selected_year_name': current_academic_year,
        'Selected_Year_Id': selected_year_id,
        'subjects_rec': subjects_rec,
        'scheme_mark_recs': rec_list,
        'class_list':class_list,
        'section_list':section_list,
        'class_rec':class_rec,
        'subject_recs':scheme_mapped_subject_list,
        'frequency_recs':frequency_recs
    }
    return form_vals,error_flag_status

@user_login_required
def ViewFilteredRubricAprovalList(request):
    run_app_val_dict = request.POST
    form_vals = {}
    error_flag_status = False


    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
    except:
        messages.warning(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
        return render(request, 'assessment_sub_tile.html')

    if request.method == 'POST':
        request.session['run_app_val_dict'] = run_app_val_dict
        form_vals,error_flag_status = load_rubric_approve_status_function(run_app_val_dict, request)
        if error_flag_status:
            messages.warning(request, "Mapping or records do not exist for select records.")
            return redirect('/school/view_rubric_aproval_list/')
    else:
        form_vals,error_flag_status = load_rubric_approve_status_function(request.session.get('run_app_val_dict'), request)
        if error_flag_status:
            messages.warning(request, "Mapping or records do not exist for select records.")
            return redirect('/school/view_rubric_aproval_list/')

    return render(request, "rubric_aproval_list.html", form_vals)

@user_login_required
def AproveRejectRubric(request):
    all_academicyr_recs = academic_year.objects.all()
    year_class_section_object = academic_class_section_mapping.objects.all()
    scheme_mapping_recs = scheme_mapping_details.objects.all()

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    selected_year_id = current_academic_year.id
    # print "In AproveRejectScheme function"
    selected_recs = request.POST.getlist('check')  # Getting Ids  of selected records
    task = request.POST.get('task', None)

    if task =='approve':
        for rec in selected_recs:
            rubric_academic_frequency_mapping.objects.filter(id = rec).update(is_apprpved=True, is_rejected=False, approved_by_id = request.user.id)
            messages.success(request, "Selected Records Has Been Approved.")
            # print "a"
    elif task == 'reject':
        for rec in selected_recs:
            rubric_academic_frequency_mapping.objects.filter(id=rec).update(is_submitted=False, is_rejected=True, approved_by_id = request.user.id)
            messages.warning(request, "Approval of Selected Records Has Been Rejected.")
            # print 'r'
    else:
        messages.warning(request, "Some Error Occurred.")

    # return render(request, "rubric_aproval_list.html",
    #               {"year_list": all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})
    return redirect('/school/view_rubric_aproval_list/')



def load_rubric_status(year_name=None,class_name=None,subject_teacher=None, status_name=None):
    status_flag = False
    if class_name == None:
        class_name = ''
    if subject_teacher == None:
        subject_teacher = ''
    if status_name == None:
        status_name = ''

    def load_status_fun(status_name):

        # if status_name == '1':
        #
        #
        #     ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name)
        #     scheme_ids = rubric_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs).values_list(
        #         'id')
        #
        #     scheme_ay_recs = rubric_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs)
        #     scheme_mark_ids = rubric_academic_frequency_mapping.objects.filter(
        #         scheme_mapping__in=scheme_ay_recs).values_list('scheme_mapping')
        #
        #     diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))
        #
        #     for diff_rec in diff_recs:
        #         diff_list.append(diff_rec[0])
        #
        #     scheme_mapping_recs = rubric_mapping_details.objects.filter(id__in=diff_list)
        #     scheme_mark_recs = []
        #
        #     return scheme_mapping_recs, scheme_mark_recs
            # scheme_mapping_recs = mapp_table.objects.filter(id__in=diff_list)

        if status_name == '2':
            scheme_mapping_recs=[]
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name)
            # scheme_ids = rubric_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')
            #
            # scheme_ay_recs = rubric_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs)
            scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(academic_mapping__in=ac_recs, is_submitted=False,is_archived=False)

            # diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))

            # for diff_rec in scheme_mark_ids:
            #     diff_list.append(diff_rec[0])
            #
            # scheme_mapping_recs = []  #scheme_mapping_details.objects.filter(id__in=diff_list)
            # scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(scheme_mapping__in=diff_list)

            return scheme_mapping_recs, scheme_mark_recs


        if status_name == '3':


            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name)
            # scheme_ids = rubric_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs).values_list(
            #     'id')
            #
            # scheme_ay_recs = rubric_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs)
            scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(Q(academic_mapping__in=ac_recs),
                                                                               (Q(is_submitted = True, is_apprpved = False,is_archived=False)) or (Q(is_rejected= True, is_apprpved = False,is_archived=False)))


            # for diff_rec in scheme_mark_ids:
            #     diff_list.append(diff_rec[0])

            scheme_mapping_recs = []  #mapp_table.objects.filter(id__in=diff_list)
            # scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(scheme_mapping__in=diff_list)

            return scheme_mapping_recs, scheme_mark_recs

            # scheme_mapping_recs = mapp_table.objects.filter(id__in=diff_list)

        if status_name == '4':

            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name)
            # scheme_ids = rubric_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')
            #
            # scheme_ay_recs = rubric_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs)
            scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(
                academic_mapping__in=ac_recs,is_rejected = False, is_apprpved=True,is_archived=False)

            # for diff_rec in scheme_mark_ids:
            #     diff_list.append(diff_rec[0])

            scheme_mapping_recs = []  #mapp_table.objects.filter(id__in=diff_list)
            # scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(scheme_mapping__in = diff_list)

            return scheme_mapping_recs, scheme_mark_recs

    def load_class_status_fun(class_name,status_name):

        if status_name == '2':
            scheme_mapping_recs=[]
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name, class_name_id=class_name)
            # scheme_ids = rubric_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')
            #
            # scheme_ay_recs = rubric_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs)
            scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(academic_mapping__in=ac_recs, is_submitted=False,is_archived=False)

            # diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))

            # for diff_rec in scheme_mark_ids:
            #     diff_list.append(diff_rec[0])
            #
            # scheme_mapping_recs = []  #scheme_mapping_details.objects.filter(id__in=diff_list)
            # scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(scheme_mapping__in=diff_list)

            return scheme_mapping_recs, scheme_mark_recs


        if status_name == '3':
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name, class_name_id=class_name)
            # scheme_ids = rubric_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs).values_list(
            #     'id')
            #
            # scheme_ay_recs = rubric_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs)
            scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(Q(academic_mapping__in=ac_recs),
                                                                               (Q(is_submitted = True, is_apprpved = False,is_archived=False)) or (Q(is_rejected= True, is_apprpved = False,is_archived=False)))


            # for diff_rec in scheme_mark_ids:
            #     diff_list.append(diff_rec[0])

            scheme_mapping_recs = []  #mapp_table.objects.filter(id__in=diff_list)
            # scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(scheme_mapping__in=diff_list)

            return scheme_mapping_recs, scheme_mark_recs

            # scheme_mapping_recs = mapp_table.objects.filter(id__in=diff_list)

        if status_name == '4':
            ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name, class_name_id=class_name)
            # scheme_ids = rubric_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')
            #
            # scheme_ay_recs = rubric_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs)
            scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(
                academic_mapping__in=ac_recs,is_rejected = False, is_apprpved=True,is_archived=False)

            # for diff_rec in scheme_mark_ids:
            #     diff_list.append(diff_rec[0])

            scheme_mapping_recs = []  #mapp_table.objects.filter(id__in=diff_list)
            # scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(scheme_mapping__in = diff_list)

            return scheme_mapping_recs, scheme_mark_recs

    diff_list = []

    if class_name=='' and subject_teacher=='' and status_name!='':
        scheme_mapping_recs,scheme_mark_recs  = load_status_fun(status_name)

    if class_name!='' and subject_teacher=='' and status_name!='':
        scheme_mapping_recs,scheme_mark_recs  = load_class_status_fun(class_name,status_name)

    if class_name!='' and subject_teacher=='' and status_name=='':
        ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name,class_name_id=class_name)

        scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(academic_mapping__in=ac_recs,is_archived=False)

    if class_name!='' and subject_teacher!='' and status_name!='':
        scheme_mapping_recs,scheme_mark_recs  = load_class_status_fun(class_name,status_name)

        rec_list = []
        final_list = []
        rows = []
        for scheme_mark_rec in scheme_mark_recs:
            teacher_ids = []
            tchr_rec = teacher_subject_mapping.objects.filter(
                academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                subject=scheme_mark_rec.scheme_mapping.subject)
            for tchr in tchr_rec:
                teacher_ids.append(int(tchr.staff_id.id))

            if int(subject_teacher) in teacher_ids:
                teacher_recs = teacher_subject_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                mapped_data = {
                    'supervisor_recs': supervisor_recs,
                    'teacher_recs': teacher_recs,
                    'scheme_mark_rec': scheme_mark_rec
                }
                rec_list.append(mapped_data)
                tchr_list = []
                supervisor_recs_list = []
                status = ''

                tchr_name = ''
                sup_name = ''
                count = 1
                for rec in teacher_recs:
                    tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
                    count = count + 1
                    # tchr_list.append(rec.staff_id.get_pura_name())

                count = 1
                for rec in supervisor_recs:
                    sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
                    count = count + 1
                    supervisor_recs_list.append(rec.supervisor_id.get_pura_name())

                if scheme_mark_rec.is_submitted == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Under Review'
                elif scheme_mark_rec.is_apprpved == 1 and scheme_mark_rec.is_rejected == 0:
                    status = 'Final'
                elif scheme_mark_rec.is_rejected == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Draft'
                elif scheme_mark_rec.is_apprpved == 0 and scheme_mark_rec.is_rejected == 0:
                    status = 'Draft'

                dict_rec = {}
                dict_rec['Submitted_By'] = scheme_mark_rec.submitted_by
                dict_rec['Subject_Teacher'] = tchr_name
                dict_rec['Supervisor'] = sup_name
                dict_rec['Academic_Year'] = scheme_mark_rec.academic_mapping.year_name.year_name
                dict_rec['Class'] = scheme_mark_rec.academic_mapping.class_name.class_name
                dict_rec['Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
                dict_rec['Subject'] = scheme_mark_rec.scheme_mapping.subject.subject_name
                dict_rec['Grade_Scheme'] = scheme_mark_rec.scheme_mapping.grade_name.grade_name
                dict_rec['Scheme'] = scheme_mark_rec.scheme_mapping.rubric_name.scheme_name
                if scheme_mark_rec.approved_by is None:
                    dict_rec['Approved_By'] = ' '
                elif not scheme_mark_rec.approved_by.username:
                    dict_rec['Approved_By'] = ' '
                else:
                    dict_rec['Approved_By'] = scheme_mark_rec.approved_by.get_pura_name()
                # dict_rec['Frequency'] = scheme_mark_rec.frequency.frequency_name
                dict_rec['Status'] = status
                final_list.append(dict_rec)

        mapping_rec_list = []

        # for scheme_mapping_rec in scheme_mapping_recs:
        #     teacher_ids = []
        #     tchr_rec = teacher_subject_mapping.objects.filter(
        #         academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #         subject=scheme_mapping_rec.subject)
        #     for tchr in tchr_rec:
        #         teacher_ids.append(int(tchr.staff_id.id))
        #
        #     if int(subject_teacher) in teacher_ids:
        #
        #         supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         teacher_recs = teacher_subject_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         mapped_data = {
        #             'supervisor_recs': supervisor_recs,
        #             'teacher_recs': teacher_recs,
        #             'scheme_mapping_rec': scheme_mapping_rec
        #         }
        #         mapping_rec_list.append(mapped_data)
        #
        #         tchr_list = []
        #         supervisor_recs_list = []
        #         tchr_name = ''
        #         sup_name = ''
        #
        #         count = 1
        #         for rec in teacher_recs:
        #             tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
        #             count = count + 1
        #             # tchr_list.append(rec.staff_id.get_pura_name())
        #         count = 1
        #         for rec in supervisor_recs:
        #             sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
        #             count = count + 1
        #             supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
        #
        #         dict_rec = {}
        #         dict_rec['Submitted_By'] = 'Not Submitted'
        #         dict_rec['Subject_Teacher'] = tchr_name
        #         dict_rec['Supervisor'] = sup_name
        #         dict_rec['Academic_Year'] = scheme_mapping_rec.academic_class_section_mapping.year_name.year_name
        #         dict_rec['Class'] = scheme_mapping_rec.academic_class_section_mapping.class_name.class_name
        #         dict_rec['Section'] = scheme_mapping_rec.academic_class_section_mapping.section_name.section_name
        #         dict_rec['Subject'] = scheme_mapping_rec.subject.subject_name
        #         dict_rec['Grade_Scheme'] = scheme_mapping_rec.grade_name.grade_name
        #         dict_rec['Scheme'] = scheme_mapping_rec.rubric_name.scheme_name
        #         dict_rec['Approved_By'] = 'Not Approved'
        #         dict_rec['Status'] = 'Not Started'
        #         final_list.append(dict_rec)
        for rec in final_list:
            ab = [rec['Submitted_By'], rec['Subject_Teacher'], rec['Supervisor'], rec['Academic_Year'],
                  rec['Class'],
                  rec['Section'], rec['Subject'], rec['Grade_Scheme'], rec['Scheme'], rec['Approved_By'],
                  rec['Status']]
            if ab not in rows:
                rows.append(ab)
        status_flag = True
        return rows, final_list, status_flag

    if class_name=='' and subject_teacher!='' and status_name!='':
        scheme_mapping_recs, scheme_mark_recs = load_status_fun(status_name)
        rec_list = []
        final_list = []
        rows = []
        for scheme_mark_rec in scheme_mark_recs:
            teacher_ids = []
            tchr_rec = teacher_subject_mapping.objects.filter(
                academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                subject=scheme_mark_rec.scheme_mapping.subject)
            for tchr in tchr_rec:
                teacher_ids.append(int(tchr.staff_id.id))

            if int(subject_teacher) in teacher_ids:
                teacher_recs = teacher_subject_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                mapped_data = {
                    'supervisor_recs': supervisor_recs,
                    'teacher_recs': teacher_recs,
                    'scheme_mark_rec': scheme_mark_rec
                }
                rec_list.append(mapped_data)
                tchr_list = []
                supervisor_recs_list = []
                status = ''

                tchr_name = ''
                sup_name = ''
                count = 1
                for rec in teacher_recs:
                    tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
                    count = count + 1
                    # tchr_list.append(rec.staff_id.get_pura_name())

                count = 1
                for rec in supervisor_recs:
                    sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
                    count = count + 1
                    supervisor_recs_list.append(rec.supervisor_id.get_pura_name())

                if scheme_mark_rec.is_submitted == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Under Review'
                elif scheme_mark_rec.is_apprpved == 1 and scheme_mark_rec.is_rejected == 0:
                    status = 'Final'
                elif scheme_mark_rec.is_rejected == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Draft'
                elif scheme_mark_rec.is_apprpved == 0 and scheme_mark_rec.is_rejected == 0:
                    status = 'Draft'

                dict_rec = {}
                dict_rec['Submitted_By'] = scheme_mark_rec.submitted_by
                dict_rec['Subject_Teacher'] = tchr_name
                dict_rec['Supervisor'] = sup_name
                dict_rec['Academic_Year'] = scheme_mark_rec.academic_mapping.year_name.year_name
                dict_rec['Class'] = scheme_mark_rec.academic_mapping.class_name.class_name
                dict_rec['Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
                dict_rec['Subject'] = scheme_mark_rec.scheme_mapping.subject.subject_name
                dict_rec['Grade_Scheme'] = scheme_mark_rec.scheme_mapping.grade_name.grade_name
                dict_rec['Scheme'] = scheme_mark_rec.scheme_mapping.rubric_name.scheme_name
                if scheme_mark_rec.approved_by is None:
                    dict_rec['Approved_By'] = ' '
                elif not scheme_mark_rec.approved_by.username:
                    dict_rec['Approved_By'] = ' '
                else:
                    dict_rec['Approved_By'] = scheme_mark_rec.approved_by.get_pura_name()
                # dict_rec['Frequency'] = scheme_mark_rec.frequency.frequency_name
                dict_rec['Status'] = status
                final_list.append(dict_rec)

        mapping_rec_list = []

        # for scheme_mapping_rec in scheme_mapping_recs:
        #     teacher_ids = []
        #     tchr_rec = teacher_subject_mapping.objects.filter(
        #         academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #         subject=scheme_mapping_rec.subject)
        #     for tchr in tchr_rec:
        #         teacher_ids.append(int(tchr.staff_id.id))
        #
        #     if int(subject_teacher) in teacher_ids:
        #
        #         supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         teacher_recs = teacher_subject_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         mapped_data = {
        #             'supervisor_recs': supervisor_recs,
        #             'teacher_recs': teacher_recs,
        #             'scheme_mapping_rec': scheme_mapping_rec
        #         }
        #         mapping_rec_list.append(mapped_data)
        #
        #         tchr_list = []
        #         supervisor_recs_list = []
        #         tchr_name = ''
        #         sup_name = ''
        #
        #         count = 1
        #         for rec in teacher_recs:
        #             tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
        #             count = count + 1
        #             # tchr_list.append(rec.staff_id.get_pura_name())
        #         count = 1
        #         for rec in supervisor_recs:
        #             sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
        #             count = count + 1
        #             supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
        #
        #         dict_rec = {}
        #         dict_rec['Submitted_By'] = 'Not Submitted'
        #         dict_rec['Subject_Teacher'] = tchr_name
        #         dict_rec['Supervisor'] = sup_name
        #         dict_rec['Academic_Year'] = scheme_mapping_rec.academic_class_section_mapping.year_name.year_name
        #         dict_rec['Class'] = scheme_mapping_rec.academic_class_section_mapping.class_name.class_name
        #         dict_rec['Section'] = scheme_mapping_rec.academic_class_section_mapping.section_name.section_name
        #         dict_rec['Subject'] = scheme_mapping_rec.subject.subject_name
        #         dict_rec['Grade_Scheme'] = scheme_mapping_rec.grade_name.grade_name
        #         dict_rec['Scheme'] = scheme_mapping_rec.rubric_name.scheme_name
        #         dict_rec['Approved_By'] = 'Not Approved'
        #         dict_rec['Status'] = 'Not Started'
        #         final_list.append(dict_rec)
        for rec in final_list:
            ab = [rec['Submitted_By'], rec['Subject_Teacher'], rec['Supervisor'], rec['Academic_Year'],
                  rec['Class'],
                  rec['Section'], rec['Subject'], rec['Grade_Scheme'], rec['Scheme'], rec['Approved_By'],
                  rec['Status']]
            if ab not in rows:
                rows.append(ab)
        status_flag = True
        return rows,final_list,status_flag

    if class_name=='' and subject_teacher!='' and status_name=='':

        ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name)
        # scheme_ids = rubric_mapping_details.objects.filter(academic_class_section_mapping__in=ac_recs).values_list('id')
        # scheme_mark_ids = rubric_academic_frequency_mapping.objects.filter().values_list('scheme_mapping')
        # diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))
        #
        # for diff_rec in diff_recs:
        #     diff_list.append(diff_rec[0])
        #     #
        # scheme_mapping_recs = rubric_mapping_details.objects.filter(id__in=diff_list)
        scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(academic_mapping__in=ac_recs,is_archived=False)

        rec_list = []
        final_list = []
        rows = []
        for scheme_mark_rec in scheme_mark_recs:
            teacher_ids = []
            tchr_rec = teacher_subject_mapping.objects.filter(
                academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                subject=scheme_mark_rec.scheme_mapping.subject)
            for tchr in tchr_rec:
                teacher_ids.append(int(tchr.staff_id.id))

            if int(subject_teacher) in teacher_ids:
                teacher_recs = teacher_subject_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                mapped_data = {
                    'supervisor_recs': supervisor_recs,
                    'teacher_recs': teacher_recs,
                    'scheme_mark_rec': scheme_mark_rec
                }
                rec_list.append(mapped_data)
                tchr_list = []
                supervisor_recs_list = []
                status = ''

                tchr_name = ''
                sup_name = ''
                count = 1
                for rec in teacher_recs:
                    tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
                    count = count + 1
                    # tchr_list.append(rec.staff_id.get_pura_name())

                count = 1
                for rec in supervisor_recs:
                    sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
                    count = count + 1
                    supervisor_recs_list.append(rec.supervisor_id.get_pura_name())

                if scheme_mark_rec.is_submitted == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Under Review'
                elif scheme_mark_rec.is_apprpved == 1 and scheme_mark_rec.is_rejected == 0:
                    status = 'Final'
                elif scheme_mark_rec.is_rejected == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Draft'
                elif scheme_mark_rec.is_apprpved == 0 and scheme_mark_rec.is_rejected == 0:
                    status = 'Draft'

                dict_rec = {}
                dict_rec['Submitted_By'] = scheme_mark_rec.submitted_by
                dict_rec['Subject_Teacher'] = tchr_name
                dict_rec['Supervisor'] = sup_name
                dict_rec['Academic_Year'] = scheme_mark_rec.academic_mapping.year_name.year_name
                dict_rec['Class'] = scheme_mark_rec.academic_mapping.class_name.class_name
                dict_rec['Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
                dict_rec['Subject'] = scheme_mark_rec.scheme_mapping.subject.subject_name
                dict_rec['Grade_Scheme'] = scheme_mark_rec.scheme_mapping.grade_name.grade_name
                dict_rec['Scheme'] = scheme_mark_rec.scheme_mapping.rubric_name.scheme_name
                if scheme_mark_rec.approved_by is None:
                    dict_rec['Approved_By'] = ' '
                elif not scheme_mark_rec.approved_by.username:
                    dict_rec['Approved_By'] = ' '
                else:
                    dict_rec['Approved_By'] = scheme_mark_rec.approved_by.get_pura_name()
                # dict_rec['Frequency'] = scheme_mark_rec.frequency.frequency_name
                dict_rec['Status'] = status
                final_list.append(dict_rec)

        mapping_rec_list = []

        # for scheme_mapping_rec in scheme_mapping_recs:
        #     teacher_ids = []
        #     tchr_rec = teacher_subject_mapping.objects.filter(
        #         academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #         subject=scheme_mapping_rec.subject)
        #     for tchr in tchr_rec:
        #         teacher_ids.append(int(tchr.staff_id.id))
        #
        #     if int(subject_teacher) in teacher_ids:
        #
        #         supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         teacher_recs = teacher_subject_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mapping_rec.academic_class_section_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         mapped_data = {
        #             'supervisor_recs': supervisor_recs,
        #             'teacher_recs': teacher_recs,
        #             'scheme_mapping_rec': scheme_mapping_rec
        #         }
        #         mapping_rec_list.append(mapped_data)
        #
        #         tchr_list = []
        #         supervisor_recs_list = []
        #         tchr_name = ''
        #         sup_name = ''
        #
        #         count = 1
        #         for rec in teacher_recs:
        #             tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
        #             count = count + 1
        #             # tchr_list.append(rec.staff_id.get_pura_name())
        #         count = 1
        #         for rec in supervisor_recs:
        #             sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
        #             count = count + 1
        #             supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
        #
        #         dict_rec = {}
        #         dict_rec['Submitted_By'] = 'Not Submitted'
        #         dict_rec['Subject_Teacher'] = tchr_name
        #         dict_rec['Supervisor'] = sup_name
        #         dict_rec['Academic_Year'] = scheme_mapping_rec.academic_class_section_mapping.year_name.year_name
        #         dict_rec['Class'] = scheme_mapping_rec.academic_class_section_mapping.class_name.class_name
        #         dict_rec['Section'] = scheme_mapping_rec.academic_class_section_mapping.section_name.section_name
        #         dict_rec['Subject'] = scheme_mapping_rec.subject.subject_name
        #         dict_rec['Grade_Scheme'] = scheme_mapping_rec.grade_name.grade_name
        #         dict_rec['Scheme'] = scheme_mapping_rec.rubric_name.scheme_name
        #         dict_rec['Approved_By'] = 'Not Approved'
        #         dict_rec['Status'] = 'Not Started'
        #         final_list.append(dict_rec)
        for rec in final_list:
            ab = [rec['Submitted_By'], rec['Subject_Teacher'], rec['Supervisor'], rec['Academic_Year'],
                  rec['Class'],
                  rec['Section'], rec['Subject'], rec['Grade_Scheme'], rec['Scheme'], rec['Approved_By'],
                  rec['Status']]
            if ab not in rows:
                rows.append(ab)
        status_flag = True
        return rows,final_list,status_flag

    if class_name != '' and subject_teacher != '' and status_name == '':

        ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name, class_name_id=class_name)
        scheme_ids = rubric_mapping_details.objects.filter(year=year_name,class_obj = class_name).values_list('id')

        scheme_ay_recs = rubric_mapping_details.objects.filter(year=year_name,class_obj = class_name)
        scheme_mark_ids = rubric_academic_frequency_mapping.objects.filter(
            scheme_mapping__in=scheme_ay_recs,is_archived=False).values_list('scheme_mapping')
        # diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))

        diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))

        for diff_rec in diff_recs:
            diff_list.append(diff_rec[0])

        scheme_mapping_recs = rubric_mapping_details.objects.filter(id__in=diff_list)
        scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(scheme_mapping__in=scheme_ay_recs,is_archived=False)

        rec_list = []
        final_list = []
        rows = []
        for scheme_mark_rec in scheme_mark_recs:
            teacher_ids = []
            tchr_rec = teacher_subject_mapping.objects.filter(
                academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                subject=scheme_mark_rec.scheme_mapping.subject)
            for tchr in tchr_rec:
                teacher_ids.append(int(tchr.staff_id.id))

            if int(subject_teacher) in teacher_ids:
                teacher_recs = teacher_subject_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
                    academic_class_section_mapping=scheme_mark_rec.academic_mapping,
                    subject=scheme_mark_rec.scheme_mapping.subject)

                mapped_data = {
                    'supervisor_recs': supervisor_recs,
                    'teacher_recs': teacher_recs,
                    'scheme_mark_rec': scheme_mark_rec
                }
                rec_list.append(mapped_data)
                tchr_list = []
                supervisor_recs_list = []
                status = ''

                tchr_name = ''
                sup_name = ''
                count = 1
                for rec in teacher_recs:
                    tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
                    count = count + 1
                    # tchr_list.append(rec.staff_id.get_pura_name())

                count = 1
                for rec in supervisor_recs:
                    sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
                    count = count + 1
                    supervisor_recs_list.append(rec.supervisor_id.get_pura_name())

                if scheme_mark_rec.is_submitted == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Under Review'
                elif scheme_mark_rec.is_apprpved == 1 and scheme_mark_rec.is_rejected == 0:
                    status = 'Final'
                elif scheme_mark_rec.is_rejected == 1 and scheme_mark_rec.is_apprpved == 0:
                    status = 'Draft'
                elif scheme_mark_rec.is_apprpved == 0 and scheme_mark_rec.is_rejected == 0:
                    status = 'Draft'

                dict_rec = {}
                dict_rec['Submitted_By'] = scheme_mark_rec.submitted_by
                dict_rec['Subject_Teacher'] = tchr_name
                dict_rec['Supervisor'] = sup_name
                dict_rec['Academic_Year'] = scheme_mark_rec.scheme_mapping.year.year_name
                dict_rec['Class'] = scheme_mark_rec.scheme_mapping.class_obj.class_name
                dict_rec['Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
                dict_rec['Subject'] = scheme_mark_rec.scheme_mapping.subject.subject_name
                dict_rec['Grade_Scheme'] = scheme_mark_rec.scheme_mapping.grade_name.grade_name
                dict_rec['Scheme'] = scheme_mark_rec.scheme_mapping.rubric_name.scheme_name
                if scheme_mark_rec.approved_by is None:
                    dict_rec['Approved_By'] = ' '
                elif not scheme_mark_rec.approved_by.username:
                    dict_rec['Approved_By'] = ' '
                else:
                    dict_rec['Approved_By'] = scheme_mark_rec.approved_by.get_pura_name()
                # dict_rec['Frequency'] = scheme_mark_rec.frequency.frequency_name
                dict_rec['Status'] = status
                final_list.append(dict_rec)

        mapping_rec_list = []

        # for scheme_mapping_rec in scheme_mapping_recs:
        #     teacher_ids = []
        #     tchr_rec = teacher_subject_mapping.objects.filter(
        #         academic_class_section_mapping=scheme_mark_rec.academic_mapping,
        #         subject=scheme_mapping_rec.subject)
        #     for tchr in tchr_rec:
        #         teacher_ids.append(int(tchr.staff_id.id))
        #
        #     if int(subject_teacher) in teacher_ids:
        #
        #         supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mark_rec.academic_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         teacher_recs = teacher_subject_mapping.objects.filter(
        #             academic_class_section_mapping=scheme_mark_rec.academic_mapping,
        #             subject=scheme_mapping_rec.subject)
        #         mapped_data = {
        #             'supervisor_recs': supervisor_recs,
        #             'teacher_recs': teacher_recs,
        #             'scheme_mapping_rec': scheme_mapping_rec
        #         }
        #         mapping_rec_list.append(mapped_data)
        #
        #         tchr_list = []
        #         supervisor_recs_list = []
        #         tchr_name = ''
        #         sup_name = ''
        #
        #         count = 1
        #         for rec in teacher_recs:
        #             tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
        #             count = count + 1
        #             # tchr_list.append(rec.staff_id.get_pura_name())
        #         count = 1
        #         for rec in supervisor_recs:
        #             sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
        #             count = count + 1
        #             supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
        #
        #         dict_rec = {}
        #         dict_rec['Submitted_By'] = 'Not Submitted'
        #         dict_rec['Subject_Teacher'] = tchr_name
        #         dict_rec['Supervisor'] = sup_name
        #         dict_rec['Academic_Year'] = scheme_mark_rec.academic_mapping.year_name.year_name
        #         dict_rec['Class'] = scheme_mark_rec.academic_mapping.class_name.class_name
        #         dict_rec['Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
        #         dict_rec['Subject'] = scheme_mapping_rec.subject.subject_name
        #         dict_rec['Grade_Scheme'] = scheme_mapping_rec.grade_name.grade_name
        #         dict_rec['Scheme'] = scheme_mapping_rec.rubric_name.scheme_name
        #         dict_rec['Approved_By'] = 'Not Approved'
        #         dict_rec['Status'] = 'Not Started'
        #         final_list.append(dict_rec)
        for rec in final_list:
            ab = [rec['Submitted_By'], rec['Subject_Teacher'], rec['Supervisor'], rec['Academic_Year'],rec['Class'],
                  rec['Section'], rec['Subject'], rec['Grade_Scheme'], rec['Scheme'], rec['Approved_By'],rec['Status']]
            if ab not in rows:
                rows.append(ab)
        status_flag = True
        return rows, final_list, status_flag

    if (class_name==None and subject_teacher==None and status_name==None) or (class_name=='' and subject_teacher=='' and status_name==''):

        ac_recs = academic_class_section_mapping.objects.filter(year_name_id=year_name)
        # scheme_ids = rubric_mapping_details.objects.filter(year=year_name).values_list('id')
        # scheme_mark_ids = rubric_academic_frequency_mapping.objects.filter().values_list('scheme_mapping')
        # diff_recs = list(set(scheme_ids) - set(scheme_mark_ids))
        #
        # for diff_rec in diff_recs:
        #     diff_list.append(diff_rec[0])
        #     #
        # scheme_mapping_recs = rubric_mapping_details.objects.filter(id__in=diff_list)
        scheme_mark_recs = rubric_academic_frequency_mapping.objects.filter(academic_mapping__in=ac_recs,is_archived=False)

    rec_list = []
    final_list = []
    rows = []
    for scheme_mark_rec in scheme_mark_recs:
        supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
            academic_class_section_mapping=scheme_mark_rec.academic_mapping,
            subject=scheme_mark_rec.scheme_mapping.subject)
        teacher_recs = teacher_subject_mapping.objects.filter(
            academic_class_section_mapping=scheme_mark_rec.academic_mapping,
            subject=scheme_mark_rec.scheme_mapping.subject)
        mapped_data = {
            'supervisor_recs': supervisor_recs,
            'teacher_recs': teacher_recs,
            'scheme_mark_rec': scheme_mark_rec
        }
        rec_list.append(mapped_data)
        tchr_list = []
        supervisor_recs_list  = []
        status = ''

        tchr_name = ''
        sup_name = ''
        count = 1
        for rec in teacher_recs:
            tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
            count = count + 1
            # tchr_list.append(rec.staff_id.get_pura_name())

        count = 1
        for rec in supervisor_recs:
            sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
            count = count + 1
            supervisor_recs_list.append(rec.supervisor_id.get_pura_name())

        if scheme_mark_rec.is_submitted == 1 and scheme_mark_rec.is_apprpved == 0:
            status = 'Under Review'
        elif scheme_mark_rec.is_apprpved == 1 and scheme_mark_rec.is_rejected == 0:
            status = 'Final'
        elif scheme_mark_rec.is_rejected == 1 and scheme_mark_rec.is_apprpved == 0:
            status = 'Draft'
        elif scheme_mark_rec.is_apprpved == 0 and scheme_mark_rec.is_rejected == 0:
            status = 'Draft'

        dict_rec = {}
        dict_rec['Submitted_By'] = scheme_mark_rec.submitted_by
        dict_rec['Subject_Teacher'] = tchr_name
        dict_rec['Supervisor'] = sup_name
        dict_rec['Academic_Year'] = scheme_mark_rec.academic_mapping.year_name.year_name
        dict_rec['Class'] = scheme_mark_rec.academic_mapping.class_name.class_name
        dict_rec['Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
        dict_rec['Subject'] = scheme_mark_rec.scheme_mapping.subject.subject_name
        dict_rec['Grade_Scheme'] = scheme_mark_rec.scheme_mapping.grade_name.grade_name
        dict_rec['Scheme'] = scheme_mark_rec.scheme_mapping.rubric_name.scheme_name
        if scheme_mark_rec.approved_by is None:
            dict_rec['Approved_By'] = ' '
        elif not scheme_mark_rec.approved_by.username:
            dict_rec['Approved_By'] = ' '
        else:
            dict_rec['Approved_By'] = scheme_mark_rec.approved_by.get_pura_name()
        # dict_rec['Frequency'] = scheme_mark_rec.frequency.frequency_name
        dict_rec['Status'] = status
        final_list.append(dict_rec)

    mapping_rec_list = []
    # for scheme_mapping_rec in scheme_mapping_recs:
    #     supervisor_recs = academic_supervisor_class_section_mapping.objects.filter(
    #         academic_class_section_mapping=scheme_mark_rec.academic_mapping,
    #         subject=scheme_mapping_rec.subject)
    #     teacher_recs = teacher_subject_mapping.objects.filter(
    #         academic_class_section_mapping=scheme_mark_rec.academic_mapping,
    #         subject=scheme_mapping_rec.subject)
    #     mapped_data = {
    #         'supervisor_recs': supervisor_recs,
    #         'teacher_recs': teacher_recs,
    #         'scheme_mapping_rec': scheme_mapping_rec
    #     }
    #     mapping_rec_list.append(mapped_data)
    #
    #     tchr_list = []
    #     supervisor_recs_list = []
    #     tchr_name = ''
    #     sup_name = ''
    #
    #     count = 1
    #     for rec in teacher_recs:
    #         tchr_name += str(count) + ') ' + rec.staff_id.get_pura_name() + '\n'
    #         count = count + 1
    #         # tchr_list.append(rec.staff_id.get_pura_name())
    #     count = 1
    #     for rec in supervisor_recs:
    #         sup_name += str(count) + ') ' + rec.supervisor_id.get_pura_name() + '\n'
    #         count = count + 1
    #         supervisor_recs_list.append(rec.supervisor_id.get_pura_name())
    #
    #     dict_rec = {}
    #     dict_rec['Submitted_By'] = 'Not Submitted'
    #     dict_rec['Subject_Teacher'] = tchr_name
    #     dict_rec['Supervisor'] = sup_name
    #     dict_rec['Academic_Year'] = scheme_mapping_rec.year.year_name
    #     dict_rec['Class'] = scheme_mapping_rec.class_obj.class_name
    #     dict_rec['Section'] = scheme_mark_rec.academic_mapping.section_name.section_name
    #     dict_rec['Subject'] = scheme_mapping_rec.subject.subject_name
    #     dict_rec['Grade_Scheme'] = scheme_mapping_rec.grade_name.grade_name
    #     dict_rec['Scheme'] = scheme_mapping_rec.rubric_name.scheme_name
    #     dict_rec['Approved_By'] = 'Not Approved'
    #     dict_rec['Status'] = 'Not Started'
    #     final_list.append(dict_rec)
    for rec in final_list:
        ab = [rec['Submitted_By'], rec['Subject_Teacher'], rec['Supervisor'], rec['Academic_Year'], rec['Class'],
              rec['Section'], rec['Subject'], rec['Grade_Scheme'], rec['Scheme'], rec['Approved_By'], rec['Status']]
        rows.append(ab)
    return rows, final_list, status_flag


@user_login_required
@user_permission_required('assessment.can_view_view_rubric_status_list', '/school/home/')
def RubricStatus(request):
    # print "In SchemeStatus function"
    all_academicyr_recs = academic_year.objects.all()
    class_list = class_details.objects.all()

    staff_list =[]
    all_teacher_recs = teacher_subject_mapping.objects.all()
    for teacher_recs in all_teacher_recs:
        if teacher_recs.staff_id not in staff_list:
            staff_list.append(teacher_recs.staff_id)
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        class_list = current_academic_year_mapped_classes(current_academic_year, request.user)
    except:
        messages.warning(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
        return render(request, 'assessment_sub_tile.html')
    selected_year_id = current_academic_year.id
    rows, final_list, status_flag = load_rubric_status(selected_year_id)

    form_vals = {
        "year_list": all_academicyr_recs,
        "all_teacher_recs": staff_list,
        'selected_year_name': current_academic_year,
        'Selected_Year_Id': selected_year_id,
        'option_count': itertools.count(1),
        'rows': rows,
        'final_list': final_list,
        'class_list': class_list
    }

    return render(request, "rubric_status.html",form_vals)

def load_rubric_status_function(val_dict,user):
    # print "In SchemeStatus function"

    staff_list = []
    class_list = class_details.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    all_teacher_recs = teacher_subject_mapping.objects.all()
    for teacher_recs in all_teacher_recs:
        if teacher_recs.staff_id not in staff_list:
            staff_list.append(teacher_recs.staff_id)

    all_academicyr_recs = academic_year.objects.all()
    year_name = val_dict.get('year_name', None)
    class_name = val_dict.get('class_name', None)
    status_name = val_dict.get('status_name', None)
    subject_tchr_name = val_dict.get('subject_tchr_name', None)

    rows, final_list,status_flag = load_rubric_status(year_name,class_name,subject_tchr_name, status_name)


    if subject_tchr_name:
        subject_tchr_rec = AuthUser.objects.get(id=subject_tchr_name)
    else:
        subject_tchr_rec= ''
    if class_name:
        class_rec = class_details.objects.get(id=class_name)
    else:
        class_rec = ''
    if status_name:
        if status_name =='1':
            status_rec = 'Not Started'
            status_id = '1'
        if status_name =='2':
            status_rec = 'Draft'
            status_id = '2'
        if status_name =='3':
            status_rec = 'Under Review'
            status_id = '3'
        if status_name =='4':
            status_rec = 'Final'
            status_id = '4'
    else:
        status_rec = ''
        status_id = ''
    current_academic_year = academic_year.objects.get(current_academic_year=1)
    class_list = current_academic_year_mapped_classes(current_academic_year, user)
    selected_year_id = current_academic_year.id
    form_vals = {
        'final_list':final_list,
        'rows':rows,
        # 'scheme_mark_recs': rec_list,
        "year_list": all_academicyr_recs,
        'selected_year_name': current_academic_year,
        'Selected_Year_Id': selected_year_id,
        'subject_tchr_rec': subject_tchr_rec,
        'status_id':status_id,
        'status_rec':status_rec,
        "all_teacher_recs": staff_list,
        # 'section_rec': section_rec,
        # 'scheme_rec': scheme_rec,
        'class_rec' :class_rec,
        'class_list':class_list
    }
    return form_vals

@user_login_required
def RubricStatusList(request):
    val_dict = request.POST
    form_vals = {}
    flag = False

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
    except:
        messages.warning(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
        return render(request, 'assessment_sub_tile.html')

    if request.method == 'POST':
        request.session['val_dict'] = val_dict
        form_vals = load_rubric_status_function(val_dict, request.user)
    else:
        form_vals = load_rubric_status_function(request.session.get('val_dict'), request.user)

    return render(request, "rubric_status.html", form_vals)


@user_login_required
def ExportRubricStatusRecords(request):
    year_name = request.POST.get('e_year_name', None)
    class_name = request.POST.get('e_class_name', None)
    # scheme_name = request.POST.get('e_exam_scheme_name', None)
    status_name = request.POST.get('e_status_name', None)
    subject_tchr_name = request.POST.get('e_subject_tchr_name', None)

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
    except:
        messages.warning(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
        return render(request, 'assessment_sub_tile.html')
    selected_year_id = current_academic_year.id

    rows, final_list, status_flag = load_rubric_status(year_name, class_name, subject_tchr_name, status_name)

    column_names = ['Submitted By', 'Subject Teacher', 'Supervisor', 'Academic Year', 'Class', 'Section', 'Subject',
                    'Grade Scheme', 'Scheme', 'Approved By', 'Status']

    return export_users_xls('rubric_status', column_names, rows)

@user_login_required
def ExamAproveReject(request):
    current_academic_year = academic_year.objects.get(current_academic_year=1)

    # print "In AproveRejectScheme function"
    selected_recs = request.POST.getlist('check')  # Getting Ids  of selected records
    task = request.POST.get('task', None)

    if task =='approve':
        for rec in selected_recs:
            exam_scheme_academic_frequency_mapping.objects.filter(id = rec).update(is_apprpved=True, is_rejected=False, approved_by_id = request.user.id)
            messages.success(request, "Selected Records Has Been Approved.")
            # print "a"
    elif task == 'reject':
        for rec in selected_recs:
            exam_scheme_academic_frequency_mapping.objects.filter(id=rec).update(is_submitted=False, is_rejected=True, approved_by_id = request.user.id)
            messages.warning(request, "Approval of Selected Records Has Been Rejected.")
            # print 'r'
    else:
        messages.warning(request, "Some Error Occurred.")

    return redirect('/school/view_exam_scheme_aproval_list/')


@user_login_required
def RubricDefinitionFilter(request):
    ruberic_def_filter_data = request.POST
    if request.POST:
        selected_scheme_code = request.POST.get('scheme_code')
        selected_scheme_name = request.POST.get('scheme_name')
        id_rubrics_catgory = request.POST.get('rubrics_catgory')
        request.session['ruberic_def_filter_data'] = ruberic_def_filter_data
    else:
        ruberic_def_filter_data=request.session.get('ruberic_def_filter_data')
        selected_scheme_code = ruberic_def_filter_data.get('scheme_code')
        selected_scheme_name = ruberic_def_filter_data.get('scheme_name')
        id_rubrics_catgory = ruberic_def_filter_data.get('rubrics_catgory')
    rubrics_category_instance = rubrics_category.objects.get(id=id_rubrics_catgory)

    rubric_recs = rubrics_master.objects.filter(rubrics_category_name=rubrics_category_instance).exclude(rubrics_name='NA')

    if rubric_recs.count() == 0:
        messages.warning(request, "Categories Mapping Not Found!")
        return redirect('/school/rubric_definition/')

    else:
        mark = []
        for obj in rubric_recs:
           mark.append(int(obj.rubrics_mark))

        selected_mark = sorted(mark)[-1]

        selected_option = rubrics_master.objects.get(rubrics_mark=selected_mark,rubrics_category_name=rubrics_category_instance)


        return render(request, "rubric_definition.html",{'rubric_recs':rubric_recs,'selected_option':selected_option,'rubrics_category_instance':rubrics_category_instance,'selected_scheme_code':selected_scheme_code,'selected_scheme_name':selected_scheme_name})


@user_login_required
def ViewCopyFrequencyDetails(request):
    frequency_list=[]
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name')
    subject_name = request.POST.get('subject_name')
    # frequency_name = request.POST.get('frequency_name')
    ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name,  year_name_id=year_name, section_name_id =section_name)

    scheme_acd_recs = scheme_academic_frequency_mapping.objects.filter(academic_mapping=ac_recs.id,scheme_mapping__subject_id=subject_name,is_submitted=True)

    for frequency_rec in scheme_acd_recs:
        frequency_dict = {'id':frequency_rec.frequency.id,'frequency_name':frequency_rec.frequency.frequency_name}
        frequency_list.append(frequency_dict)
    return JsonResponse(frequency_list,safe=False)



@user_login_required
def UpdateEnteredCopySchemeMark(request, rec_id):

    academic_frequency_rec = scheme_academic_frequency_mapping.objects.get(id = rec_id)
    selected_scheme = scheme.objects.get(id=academic_frequency_rec.scheme_mapping.scheme_name_id)

    subtopics_recs = []
    subtopics_ids = []
    subtopic_recs =[]
    list = []

    grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)

    for data in academic_frequency_rec.student_mark_ids.all():
        for rec in data.subtopic_ids.all():
            if rec.subtopic.id not in list:
                list.append(rec.subtopic.id)
                subtopic_recs.append(rec.subtopic)

    if academic_frequency_rec.is_submitted is True:
        color_code = '#1ce034'
    else:
        color_code = '#ffa012'

    form_vals = {
        # 'student_list': filtered_students,
        'Selected_Section_Id': academic_frequency_rec.scheme_mapping.academic_class_section_mapping.section_name.id,
        'selected_section_name': academic_frequency_rec.scheme_mapping.academic_class_section_mapping.section_name,
        'selected_section': academic_frequency_rec.scheme_mapping.academic_class_section_mapping.section_name,
        # 'section_list': all_section_recs,

        'Selected_Class_Id': academic_frequency_rec.scheme_mapping.academic_class_section_mapping.class_name.id,
        'selected_class_name': academic_frequency_rec.scheme_mapping.academic_class_section_mapping.class_name.class_name,
        # 'selected_class': selected_class_rec,
        # 'class_list': all_class_recs,

        'Selected_Year_Id': academic_frequency_rec.scheme_mapping.academic_class_section_mapping.year_name.id,
        'selected_year_name': academic_frequency_rec.scheme_mapping.academic_class_section_mapping.year_name.year_name,
        'selected_year': academic_frequency_rec.scheme_mapping.academic_class_section_mapping.year_name,
        # 'year_list': all_academicyr_recs,

        'frequency_recs': academic_frequency_rec.frequency,
        'frequency_data':  academic_frequency_rec.frequency.id,
        'scheme_recs': academic_frequency_rec.scheme_mapping.scheme_name,
        'subtopics_recs': subtopics_recs,
        # 'selected_scheme': selected_scheme,

        'academic_frequency_rec': academic_frequency_rec,
        'subtopic_data': subtopic_recs,
        'color_code':color_code,
        'grade_colors':grade_colors,
        'subject_recs': academic_frequency_rec.scheme_mapping.subject,

    }

    return render(request, "view_copy_scheme.html",form_vals)


@user_login_required
def NewEnterMarkSchemeStudentList(request):
    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    selected_year_id = request.POST.get('year_name')  # It will be used in HTML page as a value of selected Class
    selected_year_rec = academic_year.objects.get(id=selected_year_id)
    selected_year_name = selected_year_rec.year_name

    selected_class_id = request.POST.get('class_name')  # It will be used in HTML page as a value of selected Class
    selected_class_rec = class_details.objects.get(id=selected_class_id)
    selected_class_name = selected_class_rec.class_name

    selected_section_id = request.POST.get('section_name')  # It will be used in HTML page as a value of selected Section
    selected_section_rec = sections.objects.get(id=selected_section_id)
    selected_section_name = selected_section_rec.section_name

    #scheme_id = request.POST.get('scheme_name')
    frequency_name = request.POST.get('frequency_name')
    frequency_data = frequency_details.objects.get(id = frequency_name)

    selected_subject_id = request.POST.get('subject_name')  # It will be used in HTML page as a value of selected Section
    selected_subject_rec = subjects.objects.get(id=selected_subject_id)
    selected_subject_name = selected_subject_rec.subject_name

    selected_copy_from_id = request.POST.get('copy_from')  # It will be used in HTML page as a value of selected Section
    #selected_subject_rec = subjects.objects.get(id=selected_subject_id)
    #selected_subject_name = selected_subject_rec.subject_name

    #############
    year_class_section_obj1 = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                         section_name=selected_section_id,
                                                                         class_name=selected_class_id)
    try:
        scheme_rec1 = scheme_mapping_details.objects.get(academic_class_section_mapping_id=year_class_section_obj1,
                                                       subject_id=selected_subject_id)
        scheme_id = scheme_rec1.scheme_name.id
    except:
        messages.warning(request, "Mapping Not Found for Selected Class And Section")
        return redirect('/school/enter_scheme_mark/')

    ############

    selected_scheme = scheme.objects.get(id =scheme_id)
    subtopics_recs = []

    for starand in selected_scheme.stand_ids.all():
            for sub_strand_recs in starand.sub_strand_ids.all():
                if sub_strand_recs:
                    for topic_recs in sub_strand_recs.topic_ids.all():
                        if topic_recs:
                            for subtopics in topic_recs.sub_topic_ids.all():
                                subtopics_recs.append(subtopics)

    #filtering student records to pass HTML page
    # print "---"
    # subject_rec = selected_scheme.mapped_subject_relation.all()
    year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id, section_name=selected_section_id, class_name=selected_class_id)

    scheme_rec = scheme_mapping_details.objects.get(academic_class_section_mapping_id= year_class_section_obj, scheme_name =scheme_id)

    mark_recs =  scheme_academic_frequency_mapping.objects.filter(scheme_mapping_id=scheme_rec.id, frequency_id = frequency_name)
    grade_colors = grade_sub_details.objects.filter(grade=scheme_rec.grade_name)

    if mark_recs:
        messages.warning(request,"Mark Has Already Been Entered For That Scheme and Students. You Can Update Them Now.")
        return redirect('/school/entered_scheme_mark_list/')

    if selected_copy_from_id:
        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                            section_name=selected_section_id,
                                                                            class_name=selected_class_id)
        scheme_rec = scheme_mapping_details.objects.get(academic_class_section_mapping_id=year_class_section_obj,
                                                        scheme_name=scheme_id)
        mark_recs = scheme_academic_frequency_mapping.objects.filter(scheme_mapping_id=scheme_rec.id,
                                                                     frequency_id=selected_copy_from_id)
        rec_id = mark_recs[0].id
        return redirect('/school/view_entered_copy_scheme_mark/' + str(rec_id))

        # messages.warning(request, "---------------")
        # return redirect('/school/entered_scheme_mark_list/')

    # sub_rec.optional_subject_relation.all()
    filtered_students =[]

    if scheme_rec.subject.type == 'Optional':
        optional_subject_students = scheme_rec.subject.optional_subject_relation.all()
        for students in optional_subject_students:
            student_rec = student_details.objects.filter(id=students.id, is_active=True, academic_class_section_id = year_class_section_obj.id)
            if student_rec:
                filtered_students.append(student_rec[0])
    else:
        filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True)

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    selected_year_id = current_academic_year.id
    scheme_recs = scheme.objects.all()
    frequency_recs = frequency_details.objects.all()

    #     Passing values to the HTML page through form_vals
    form_vals = {
        'student_list': filtered_students,
        'Selected_Section_Id': selected_section_id,
        'selected_section_name': selected_section_name,
        'selected_section': selected_section_rec,
        'section_list': all_section_recs,

        'Selected_Class_Id': selected_class_id,
        'selected_class_name': selected_class_name,
        'selected_class': selected_class_rec,
        'class_list': all_class_recs,

        'Selected_Year_Id': selected_year_id,
        'selected_year_name': selected_year_name,
        'selected_year': selected_year_rec,
        'year_list': all_academicyr_recs,

        'frequency_recs': frequency_recs,
        'frequency_data': frequency_data,
        'scheme_recs': scheme_recs,
        'subtopics_recs': subtopics_recs,
        'selected_scheme': selected_scheme,
        'grade_colors': grade_colors,
        'selected_subject_id': selected_subject_id,
        'selected_subject_name': selected_subject_rec,
        # 'selected_strand': strand_rec,
    }
    return render(request, "enter_scheme_marks.html",form_vals)
    # return render(request, "enter_scheme_marks.html", {'students_recs':students_recs})

@user_login_required
def ViewCopyFrequencyExamDetails(request):
    frequency_list=[]
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name')
    subject_name = request.POST.get('subject_name')
    exam_name = request.POST.get('exam_name')
    ac_recs = academic_class_section_mapping.objects.get(class_name_id=class_name,  year_name_id=year_name, section_name_id =section_name)
    subject_rec = exam_scheme_acd_mapping_details.objects.get(year_id = year_name,class_obj_id = class_name,subject_id=subject_name)
    exam_name_rec = subject_rec.exam_name_mapping_ids.get(exam_name=exam_name,is_archived=False)
    exam_name_recs = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping_id=ac_recs,scheme_mapping_id=exam_name_rec.id, is_submitted=True,is_archived=False)
    for fre_rec in exam_name_recs:
        frequency_dict = {'id': fre_rec.frequency.id, 'frequency_name': fre_rec.frequency.frequency_name}
        frequency_list.append(frequency_dict)
    return JsonResponse(frequency_list, safe=False)


@user_login_required
def ValidateMonthExist(request):
    flag = None
    from_date = request.POST.get('from_date', None)
    to_date = request.POST.get('to_date', None)

    if frequency_details.objects.filter(from_date=from_date, to_date=to_date).exists():

        flag = True
    else:
        flag=False
    # print "flag---> ",flag
    return JsonResponse(flag, safe=False)



@user_login_required
def ViewMappedSubjectScheme(request):
    finalDict = []
    # subject_name = request.POST.get('subject_name', None)
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    # section_name = request.POST.get('section_name')


    scheme_mapping_rec = scheme_mapping_details.objects.filter(year_id =year_name, class_obj_id = class_name)
    for rec in scheme_mapping_rec:
        if not any(d['id'] == rec.subject_id for d in finalDict):
            ab={'id':rec.subject_id, 'value': rec.subject.subject_name}
            finalDict.append(ab)
    return JsonResponse(finalDict,safe=False)



@user_login_required
def ViewExamSchemeMapping(request):
    exam_scheme_recs = exam_scheme.objects.filter(is_submitted=True)
    exam_name_list = exam_details.objects.all()

    exam_mapping_recs = exam_scheme_exam_name_mapping.objects.all()
    year_class_section_object = academic_class_section_mapping.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    grade_recs = grade_details.objects.filter(is_archive = False)
    # frequency_recs = frequency_details.objects.all()

    if year_class_section_object:
        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id
            return render(request, 'view_exam_scheme_mapping.html',
                          { "year_list": all_academicyr_recs,
                           'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                            'exam_mapping_recs': exam_mapping_recs,'exam_scheme_recs':exam_scheme_recs,'grade_recs':grade_recs,'exam_name_list':exam_name_list})
        except academic_year.DoesNotExist:
            return render(request, 'view_exam_scheme_mapping.html',
                          { "year_list": all_academicyr_recs,
                           'exam_mapping_recs': exam_mapping_recs})
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'exam_sub_tile.html')

# @user_login_required
# def ViewExamSchemeSubjectMapping(request):
#     exam_scheme_recs = exam_scheme.objects.filter(is_submitted=True)
#     exam_name_list = exam_details.objects.all()
#     exam_mapping_recs = exam_mapping_details.objects.all()
#     year_class_section_object = academic_class_section_mapping.objects.all()
#     all_academicyr_recs = academic_year.objects.all()
#     grade_recs = grade_details.objects.filter(is_archive = False)
#
#     if year_class_section_object:
#         try:
#             current_academic_year = academic_year.objects.get(current_academic_year=1)
#             selected_year_id = current_academic_year.id
#             return render(request, 'view_exam_scheme_subject_mapping.html',
#                           { "year_list": all_academicyr_recs,
#                            'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
#                             'exam_mapping_recs': exam_mapping_recs,'exam_scheme_recs':exam_scheme_recs,'grade_recs':grade_recs,'exam_name_list':exam_name_list})
#         except academic_year.DoesNotExist:
#             return render(request, 'view_exam_scheme_subject_mapping.html',
#                           { "year_list": all_academicyr_recs,
#                            'exam_mapping_recs': exam_mapping_recs})
#     else:
#         messages.success(request, "Academic Year Class Section Mapping Not Found.")
#         return render(request, 'exam_sub_tile.html')

# @user_login_required
# def SaveExamSchemeMapping(request):
#     try:
#         if request.method == 'POST':
#             year  = request.POST.get('year_name')
#             class_name_ids = request.POST.getlist('class_name')
#             exam_name = request.POST.get('exam_name')
#             exam_scheme = request.POST.get('exam_scheme')
#             subject_name_ids = request.POST.getlist('subject_name')
#             grade_name = request.POST.get('grade_name')
#             flag = 0
#
#             for class_rec in class_name_ids:
#                 rec_list = []
#                 class_rec_list = []
#                 exam_rec = exam_scheme_mapping_details.objects.filter(year_id=year, class_obj_id=class_rec,exam_scheme_id=exam_scheme,exam_name_obj_id=exam_name,grade_name_id=grade_name)
#                 test_flag = False
#                 for subject_rec in subject_name_ids:
#                     sub_rec = exam_scheme_subject_details.objects.filter(exam_scheme_obj_id=exam_rec,subject_id=subject_rec)
#
#                     if sub_rec:
#                         pass
#
#                     elif exam_scheme_subject_details.objects.filter(~Q(exam_scheme_obj_id=exam_scheme),subject_id=subject_rec,exam_scheme_obj__class_obj =class_rec ).exists():
#                         class_rec_list.append(class_rec)
#                         pass
#
#                     else:
#                         if not test_flag:
#                             check = exam_scheme_mapping_details.objects.filter(year_id=year, class_obj_id=class_rec,
#                                                                             exam_scheme_id=exam_scheme, exam_name_obj_id=exam_name,
#                                                                             grade_name_id=grade_name)
#                             if check:
#                                obj = exam_scheme_mapping_details.objects.get(year_id=year, class_obj_id=class_rec,
#                                                                              exam_scheme_id=exam_scheme,
#                                                                              exam_name_obj_id=exam_name,
#                                                                              grade_name_id=grade_name)
#                                pass
#                             else:
#
#                                 obj = exam_scheme_mapping_details.objects.create(year_id=year, class_obj_id=class_rec,
#                                                                                  exam_scheme_id=exam_scheme,
#                                                                                  exam_name_obj_id=exam_name,
#                                                                                  grade_name_id=grade_name)
#                                 test_flag = True
#
#                         exam_scheme_subject_details.objects.create(exam_scheme_obj = obj, subject_id=subject_rec)
#
#         return redirect('/school/view_exam_scheme_mapping/')
#     except:
#         messages.warning(request, "Some Error Occurred. Please Try again.")
#         return redirect('/school/view_exam_scheme_mapping/')

@user_login_required
@user_permission_required('assessment.can_view_view_exam_mapping_list', '/school/home/')
def ViewExamSchemeSubjectMapping(request):
    exam_scheme_recs = exam_scheme.objects.filter(is_submitted=True)
    exam_name_list = exam_details.objects.all()
    exam_mapping_recs = exam_scheme_exam_name_mapping.objects.all()
    year_class_section_object = academic_class_section_mapping.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    grade_recs = grade_details.objects.filter(is_archive = False)

    if year_class_section_object:
        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id
            return render(request, 'view_exam_scheme_subject_mapping.html',
                          { "year_list": all_academicyr_recs,
                           'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                            'exam_mapping_recs': exam_mapping_recs,'exam_scheme_recs':exam_scheme_recs,'grade_recs':grade_recs,'exam_name_list':exam_name_list})
        except academic_year.DoesNotExist:
            return render(request, 'view_exam_scheme_subject_mapping.html',
                          { "year_list": all_academicyr_recs,
                           'exam_mapping_recs': exam_mapping_recs})
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'exam_sub_tile.html')

###------ Before archive functinality -----------------####
# def SaveExamSchemeMapping(request):
#     try:
#         if request.method == 'POST':
#             year = request.POST.get('year_name')
#             class_name_ids = request.POST.getlist('class_name')
#             subject_name_ids = request.POST.getlist('subject_name')
#             data_list = json.loads(request.POST.get('json_data'))
#             over_flag = False
#             override_list = []
#
#             exam_scheme_recs = exam_scheme.objects.filter(is_submitted=True)
#             exam_name_list = exam_details.objects.all()
#             exam_mapping_recs = exam_scheme_exam_name_mapping.objects.all()
#             year_class_section_object = academic_class_section_mapping.objects.all()
#             all_academicyr_recs = academic_year.objects.all()
#             grade_recs = grade_details.objects.filter(is_archive=False)
#
#
#             for class_rec in class_name_ids:
#                 for subject_rec in subject_name_ids:
#                     exam_scheme_rec = exam_scheme_acd_mapping_details.objects.filter(year_id=year,
#                                                                                      class_obj_id=class_rec,
#                                                                                      subject_id=subject_rec)
#                     if exam_scheme_rec:
#                         for rec in data_list:
#                             if not exam_scheme_rec[0].exam_name_mapping_ids.filter(exam_scheme=rec['scheme']).exists():
#                                 if exam_scheme_rec[0].exam_name_mapping_ids.filter(exam_name=rec['exam']).exists():
#
#                                     over_rec = exam_scheme_rec[0].exam_name_mapping_ids.get(exam_name=rec['exam'])
#                                     over_dict ={
#                                     'year_name':exam_scheme_rec[0].year,
#                                     'class_name':exam_scheme_rec[0].class_obj,
#                                     'subject_name':exam_scheme_rec[0].subject,
#                                     'exam_name':over_rec.exam_name,
#                                     }
#                                     override_list.append(over_dict)
#                                     over_flag=True
#                                     pass
#                                 else:
#                                     exam_name_mapping_obj = exam_scheme_exam_name_mapping.objects.create(
#                                         exam_name_id=rec['exam'], exam_scheme_id=rec['scheme'],
#                                         grade_name_id=rec['grade'])
#                                     exam_scheme_rec[0].exam_name_mapping_ids.add(exam_name_mapping_obj)
#                     else:
#                         exam_scheme_rec = exam_scheme_acd_mapping_details.objects.create(year_id=year,
#                                                                                          class_obj_id=class_rec,
#                                                                                          subject_id=subject_rec)
#                         for rec in data_list:
#                             if exam_scheme_rec.exam_name_mapping_ids.filter(exam_name=rec['exam']).exists():
#                                 over_rec = exam_scheme_rec.exam_name_mapping_ids.get(exam_name=rec['exam'])
#                                 over_dict = {
#                                     'year_name': exam_scheme_rec.year,
#                                     'class_name': exam_scheme_rec.class_obj,
#                                     'subject_name': exam_scheme_rec.subject,
#                                     'exam_name': over_rec.exam_name,
#                                 }
#                                 override_list.append(over_dict)
#                                 over_flag = True
#                                 pass
#                             else:
#                                 exam_name_mapping_obj = exam_scheme_exam_name_mapping.objects.create(
#                                     exam_name_id=rec['exam'], exam_scheme_id=rec['scheme'],
#                                     grade_name_id=rec['grade'])
#                                 exam_scheme_rec.exam_name_mapping_ids.add(exam_name_mapping_obj)
#
#         current_academic_year = academic_year.objects.get(current_academic_year=1)
#         selected_year_id = current_academic_year.id
#         # return redirect('/school/view_mapping_details/')
#         return render(request, 'view_exam_scheme_subject_mapping.html',
#                       {"year_list": all_academicyr_recs,
#                        'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
#                        'exam_mapping_recs': exam_mapping_recs, 'exam_scheme_recs': exam_scheme_recs,
#                        'grade_recs': grade_recs, 'exam_name_list': exam_name_list, 'over_flag': over_flag,
#                        'override_list': override_list})
#
#         #return redirect('/school/view_exam_scheme_subject_mapping/')
#     except:
#         messages.warning(request, "Some Error Occurred. Please Try again.")
#         return redirect('/school/view_exam_scheme_subject_mapping/')




def SaveExamSchemeMapping(request):
    try:
        if request.method == 'POST':
            year = request.POST.get('year_name')
            class_name_ids = request.POST.getlist('class_name')
            subject_name_ids = request.POST.getlist('subject_name')
            data_list = json.loads(request.POST.get('json_data'))
            over_flag = False
            save_flag = True
            override_list = []

            exam_scheme_recs = exam_scheme.objects.filter(is_submitted=True)
            exam_name_list = exam_details.objects.all()
            exam_mapping_recs = exam_scheme_exam_name_mapping.objects.all()
            year_class_section_object = academic_class_section_mapping.objects.all()
            all_academicyr_recs = academic_year.objects.all()
            grade_recs = grade_details.objects.filter(is_archive=False)


            for class_rec in class_name_ids:
                for subject_rec in subject_name_ids:

                    acd_class_sec_rec = academic_class_section_mapping.objects.filter(year_name=year,
                                                                                      class_name=class_rec)

                    if not mandatory_subject_mapping.objects.filter(academic_class_section_mapping__in=acd_class_sec_rec, subject=subject_rec).exists():
                        messages.warning(request, "Class not mapped to the selected subject.")
                        continue


                    exam_scheme_rec = exam_scheme_acd_mapping_details.objects.filter(year_id=year,
                                                                                     class_obj_id=class_rec,
                                                                                     subject_id=subject_rec)
                    if exam_scheme_rec:
                        for rec in data_list:

                            # To solve bugID 2157 this line was commented on 28th June 2018
                            # if not exam_scheme_rec[0].exam_name_mapping_ids.filter(exam_scheme=rec['scheme'],is_archived=False).exists():

                            if exam_scheme_rec[0].exam_name_mapping_ids.filter(exam_name=rec['exam'],is_archived=False).exists():

                                over_rec = exam_scheme_rec[0].exam_name_mapping_ids.get(exam_name=rec['exam'],is_archived=False)

                                over_dict = {
                                    'year_name': exam_scheme_rec[0].year,
                                    'class_name': exam_scheme_rec[0].class_obj,
                                    'subject_name': exam_scheme_rec[0].subject,

                                    'exam_id': over_rec.exam_name.id,
                                    'exam_name': over_rec.exam_name,
                                    'rec_id': over_rec.id,
                                    'scheme_id': rec['scheme'],
                                    'grade_id': rec['grade'],
                                }
                                override_list.append(over_dict)
                                over_flag=True

                            else:
                                exam_name_mapping_obj = exam_scheme_exam_name_mapping.objects.create(
                                    exam_name_id=rec['exam'], exam_scheme_id=rec['scheme'],
                                    grade_name_id=rec['grade'])
                                exam_scheme_rec[0].exam_name_mapping_ids.add(exam_name_mapping_obj)
                    else:
                        exam_scheme_rec = exam_scheme_acd_mapping_details.objects.create(year_id=year,
                                                                                         class_obj_id=class_rec,
                                                                                         subject_id=subject_rec)
                        for rec in data_list:
                            if exam_scheme_rec.exam_name_mapping_ids.filter(exam_name=rec['exam']).exists():
                                over_rec = exam_scheme_rec.exam_name_mapping_ids.get(exam_name=rec['exam'])
                                over_dict = {
                                    'year_name': exam_scheme_rec[0].year,
                                    'class_name': exam_scheme_rec[0].class_obj,
                                    'subject_name': exam_scheme_rec[0].subject,


                                    'exam_id': over_rec.exam_name.id,
                                    'exam_name': over_rec.exam_name,
                                    'rec_id': over_rec.id,
                                    'scheme_id': rec['scheme'],
                                    'grade_id': rec['grade'],
                                }
                                override_list.append(over_dict)
                                over_flag = True
                                pass
                            else:
                                exam_name_mapping_obj = exam_scheme_exam_name_mapping.objects.create(
                                    exam_name_id=rec['exam'], exam_scheme_id=rec['scheme'],
                                    grade_name_id=rec['grade'])
                                exam_scheme_rec.exam_name_mapping_ids.add(exam_name_mapping_obj)

        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        messages.success(request, "Record Updated Successfully")
        # return redirect('/school/view_mapping_details/')
        return render(request, 'view_exam_scheme_subject_mapping.html',
                      {"year_list": all_academicyr_recs,'save_flag':save_flag,
                       'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                       'exam_mapping_recs': exam_mapping_recs, 'exam_scheme_recs': exam_scheme_recs,
                       'grade_recs': grade_recs, 'exam_name_list': exam_name_list, 'over_flag': over_flag,
                       'override_list': override_list})

        #return redirect('/school/view_exam_scheme_subject_mapping/')
    except:
        messages.warning(request, "Some Error Occurred. Please Try again.")
        return redirect('/school/view_exam_scheme_subject_mapping/')


@user_login_required
def ViewMappingDetails(request):
    year_class_section_object = academic_class_section_mapping.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    if year_class_section_object:
        try:
            year_name = request.POST.get('year_name', None)
            if not year_name:
                current_academic_year = academic_year.objects.get(current_academic_year=1)
                selected_year_id = current_academic_year.id
            else:
                current_academic_year = academic_year.objects.get(id=year_name)
                selected_year_id = year_name

            exam_mapping_recs = exam_scheme_acd_mapping_details.objects.filter(year=selected_year_id)

            return render(request, 'view_mapping.html',
                          { "year_list": all_academicyr_recs,
                           'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,'exam_mapping_recs':exam_mapping_recs})

        except academic_year.DoesNotExist:
            return render(request, 'view_mapping.html',
                          { "year_list": all_academicyr_recs})
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'exam_sub_tile.html')



@user_login_required
def ExportExamSchemeRecords(request):
    year_class_section_object = academic_class_section_mapping.objects.all()
    if year_class_section_object:
        try:
            year = request.POST.get('year_name_hidden')
            exam_mapping_recs = exam_scheme_acd_mapping_details.objects.filter(year=year)
            column_names = ['Year Name', 'Class', 'Subject', 'Exam Name', 'Exam Scheme Name', 'Grading Scheme']
            rows = []
            for rec in exam_mapping_recs:
                for obj in rec.exam_name_mapping_ids.all():
                    rec_list = []
                    rec_list.append(rec.year.year_name)
                    rec_list.append(rec.class_obj.class_name)
                    rec_list.append(rec.subject.subject_name)
                    rec_list.append(obj.exam_name.exam_name)
                    rec_list.append(obj.exam_scheme.exam_scheme_name)
                    rec_list.append(obj.grade_name.grade_name)
                    rows.append(rec_list)
            return export_users_xls('ExamMappingList', column_names, rows)
        except:
            messages.warning(request, "Some Error Occurred...")
            return render(request, 'exam_sub_tile.html')
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'exam_sub_tile.html')

def ExamMappingArchive(request,rec_id):
    try:
        mark_entry = exam_scheme_academic_frequency_mapping.objects.filter(scheme_mapping_id = rec_id)
        scheme_mapping_rec = exam_scheme_exam_name_mapping.objects.get(id=rec_id)
        scheme_parent_rec = scheme_mapping_rec.exam_scheme_mapp_exam_name_relation.all()[0]
        mapping_recs = scheme_parent_rec.exam_name_mapping_ids.filter(exam_name= scheme_mapping_rec.exam_name, exam_scheme = scheme_mapping_rec.exam_scheme)

        if mapping_recs.count() == 1:
            if scheme_mapping_rec.is_archived == False:
                if not  mark_entry:
                    exam_scheme_exam_name_mapping.objects.filter(id=rec_id).update(is_archived=True)
                    messages.success(request, "Record is Archived.")
                else:
                    for rec in mark_entry:
                        exam_scheme_academic_frequency_mapping.objects.filter(id=rec.id).update(is_archived=True)
                    exam_scheme_exam_name_mapping.objects.filter(id=rec_id).update(is_archived=True)
                    messages.success(request,"Record is Archived And Mark Entries For This Mapping Is Also Updated As Archived.")
            else:
                if not  mark_entry:
                    exam_scheme_exam_name_mapping.objects.filter(id=rec_id).update(is_archived=False)
                    messages.success(request, "Record is Un-Archived.")
                else:
                    for rec in mark_entry:
                        exam_scheme_academic_frequency_mapping.objects.filter(id=rec.id).update(is_archived=False)
                    exam_scheme_exam_name_mapping.objects.filter(id=rec_id).update(is_archived=False)
                    messages.success(request,"Record is Un-Archived And Mark Entries For This Mapping Is Also Updated As Un-Archived.")
        else:
            if scheme_mapping_rec.is_archived == False:
                messages.warning(request, "Archive is not allowed for duplicate records.")
            else:
                messages.warning(request, "Un-Archive is not allowed for duplicate records.")
    except:
            messages.warning(request, "An Unexpected Error Occurred.")
    return redirect('/school/view_mapping_details/')

@user_login_required
def ViewMappingFilter(request):
    all_academicyr_recs = academic_year.objects.all()
    exam_scheme_rec = []
    try:
        year = request.POST.get('year_name')
        current_academic_year = academic_year.objects.get(id=year)
        selected_year_id = current_academic_year.id

        exam_mapping_list = exam_scheme_mapping_details.objects.filter(year=year)

        for exam_mapping_rec_obj in exam_mapping_list:
            rec_id = exam_mapping_rec_obj.id

            data = exam_scheme_subject_details.objects.filter(exam_scheme_obj=rec_id)

            for rec_obj in data:
                exam_scheme_rec.append(rec_obj)

        return render(request, 'view_mapping.html',
                      { "year_list": all_academicyr_recs,
                       'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                        'exam_mapping_list':exam_mapping_list,'exam_scheme_rec':exam_scheme_rec})
    except academic_year.DoesNotExist:
        return render(request, 'view_mapping.html',
                      { "year_list": all_academicyr_recs,
                       'exam_mapping_recs': exam_mapping_list})

@user_login_required
def DeleteExamMapping(request):
    flag=""
    try:
        mapping_id = request.GET.get('mapping_id')
        acd_id = request.GET.get('acd_id')
        all_academicyr_recs = academic_year.objects.all()

        mark_entry = exam_scheme_academic_frequency_mapping.objects.filter(scheme_mapping_id=mapping_id,is_archived=False)

        if not mark_entry:
            rec = exam_scheme_acd_mapping_details.objects.get(id=acd_id)

            rec_count = rec.exam_name_mapping_ids.filter(is_archived=False).count()
            if rec_count == 1:
                exam_scheme_acd_mapping_details.objects.filter(id=acd_id).delete()
                exam_scheme_exam_name_mapping.objects.filter(id=mapping_id).delete()

            else:
                 exam_scheme_exam_name_mapping.objects.filter(id=mapping_id).delete()
            flag="success"
            #return redirect('/school/view_mapping_details/')
        else:
            flag="error"
            messages.warning(request,
                             "Marks already entered, mapping cannot be removed")

    except:
        messages.warning(request,
                         "Marks already entered, mapping cannot be removed")
    return JsonResponse(flag, safe=False)


def OverrideExamMapping(request):
    try:
        override_array = json.loads(request.POST.get('override_array'))
        mapping_exist_flag = False
        override_flag = False


        for rec in override_array:
            if exam_scheme_academic_frequency_mapping.objects.filter(scheme_mapping =rec['maping_id']).exists():
                mapping_exist_flag = True
                continue

            exam_scheme_exam_name_mapping.objects.filter(id= rec['maping_id']).update(exam_scheme = rec['scheme_id'], grade_name = rec['grade_id'])
            override_flag = True

        if override_flag and mapping_exist_flag:
            messages.success(request, "Some Record Overrided Successfully And Some Record cannot Be Overrided Because Mark Has Entered For It.")
            return HttpResponse(json.dumps({'error': "Some Record Overrided Successfully And Some Record cannot Be Overrided Because Mark Has Entered For It."}),
                                content_type="application/json")
        if override_flag:
            messages.success(request, "Some Record Overrided Successfully.")
            return HttpResponse(json.dumps({'error': "Some Record Overrided Successfully."}),
                                content_type="application/json")
        if mapping_exist_flag :
            messages.success(request, "Some Record cannot Be Overrided Because Mark Has Entered For It.")
        return HttpResponse(json.dumps({'error': "Record Overrided Successfully."}),content_type="application/json")
    except:
        return HttpResponse(json.dumps({'error': "Some Error Occurred. Recored Not Updated."}), content_type="application/json")

@user_login_required
def EditExamMapping(request):
    edit_exam_mapping_data = request.POST
    if request.method == 'POST':
        request.session['edit_exam_mapping_data'] = edit_exam_mapping_data
        form_vals = load_edit_exam_mapping_recs(edit_exam_mapping_data, request)
    else:
        form_vals = load_edit_exam_mapping_recs(request.session.get('edit_exam_mapping_data'), request)
    return render(request, "edit_exam_mapping.html", form_vals)

def load_edit_exam_mapping_recs(edit_exam_mapping_data, request):
    acd_id = edit_exam_mapping_data.get('acd_id')
    exam_name_id = edit_exam_mapping_data.get('exam_name_id')

    exam_acd_rec = exam_scheme_acd_mapping_details.objects.get(id=acd_id)
    exam_scheme_subdetails_obj = exam_acd_rec.exam_name_mapping_ids.get(id=exam_name_id)
    grading_recs = grade_details.objects.all()
    form_vals = {'exam_acd_rec':exam_acd_rec,'exam_scheme_subdetails_obj': exam_scheme_subdetails_obj,'grading_recs':grading_recs,}
    return form_vals


def ExamMappingUpdate(request):
    year_name = request.POST.get('year_name')
    grade_name = request.POST.get('grade_name')
    class_name = request.POST.get('class_name')
    exam_name = request.POST.get('exam_name')
    exam_scheme_name = request.POST.get('exam_scheme_name')
    subject_name = request.POST.get('subject_name')
    exist_exam_child_mapping_rec = request.POST.get('exist_exam_child_mapping_rec')

    try:
        if not exam_scheme_academic_frequency_mapping.objects.filter(scheme_mapping_id = exist_exam_child_mapping_rec,is_archived=False).exists():
            exam_mapping_existing_parent_rec = exam_scheme_acd_mapping_details.objects.get(year_id=year_name,
                                                                                 class_obj_id=class_name,
                                                                                 subject_id=subject_name)

            exam_existing_clild_rec = exam_mapping_existing_parent_rec.exam_name_mapping_ids.filter(exam_name_id=exam_name, exam_scheme_id=exam_scheme_name, grade_name_id= grade_name,is_archived=False)

            if exam_existing_clild_rec:
                if exam_existing_clild_rec[0].id == int(exist_exam_child_mapping_rec):
                    exam_scheme_exam_name_mapping.objects.filter(id=exist_exam_child_mapping_rec).update(grade_name_id=grade_name)
                    messages.success(request,"Record Updated")
                else:
                    messages.warning(request, "Record Not Updated. Mapping Already Found.")
            else:
                exam_scheme_exam_name_mapping.objects.filter(id=exist_exam_child_mapping_rec).update(grade_name_id=grade_name)
                messages.success(request, "Record Updated")
        else:
            messages.warning(request, "Record Cannot be Updated. Marks Has Already Been Entered For This Mapping Record.")
    except:
        messages.warning(request, "Some Error Occurred. Please Try again.")
    return redirect('/school/edit_exam_mapping/')

def ViewMappedSubject(request):
    subject_recs=[]
    year_name = request.POST.get('academic_year')
    class_name = request.POST.get('class_name')
    exam_scheme_obj = exam_scheme_acd_mapping_details.objects.filter(year_id = year_name, class_obj_id = class_name)

    for exam_obj in exam_scheme_obj:
        if exam_obj.exam_name_mapping_ids.filter(is_archived = False):
            ab = {'id': exam_obj.subject.id, 'subject_name': exam_obj.subject.subject_name}
            subject_recs.append(ab)
    return JsonResponse(subject_recs,safe=False)


def ViewMappedSubjectDetails(request):
    modelDict = []
    academic_year = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name', None)
    ClassSectionMap = []
    if request.user.is_system_admin():
            subjectDict = []
            subjectRecs=[]
            optionalSubject = []
            year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_year,section_name=section_name,class_name=class_name)
            subject_recs = mandatory_subject_mapping.objects.filter(academic_class_section_mapping=year_class_section_obj)
            for subject in subject_recs:
                subjectDict.append(subject.subject_id)
            subjectDict = set(subjectDict)
            subject_data = subjects.objects.filter(id__in=subjectDict)
            for subject in subject_data:
                manadatory_dict={'id':subject.id, 'subject_name': subject.subject_name}
                subjectRecs.append(manadatory_dict)

            # student_recs = student_details.objects.filter(academic_class_section=year_class_section_obj)
            # for student_rec in student_recs:
            #     for subject in student_rec.subject_ids.all():
            #         optionalSubject.append(subject)
            # optionalSubject = set(optionalSubject)
            # 
            # for subject in optionalSubject:
            #     optional_dict={'id':subject.id, 'subject_name': subject.subject_name}
            #     subjectRecs.append(optional_dict)
            return JsonResponse(subjectRecs,safe=False)

    else:

        if request.user.is_supervisor():
            raw_list = []
            for mapped_object in academic_class_section_mapping.objects.filter(class_name=class_name, year_name=academic_year,section_name=section_name):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(supervisor_id=request.user.id):
                    raw_list.append(obj)
                    ClassSectionMap = list(set(raw_list))

        elif request.user.is_subject_teacher():
            raw_list = []
            for mapped_object in academic_class_section_mapping.objects.filter(class_name=class_name, year_name=academic_year,section_name=section_name):
                for obj in mapped_object.subject_teacher_academic_class_section_relation.all().filter(staff_id=request.user.id):
                    raw_list.append(obj)
                    ClassSectionMap = list(set(raw_list))

        else:
            subjectDict = []
            subjectRecs = []
            optionalSubject = []
            year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_year,
                                                                                section_name=section_name,
                                                                                class_name=class_name)
            subject_recs = mandatory_subject_mapping.objects.filter(
                academic_class_section_mapping=year_class_section_obj)
            for subject in subject_recs:
                subjectDict.append(subject.subject_id)
            subjectDict = set(subjectDict)
            subject_data = subjects.objects.filter(id__in=subjectDict)
            for subject in subject_data:
                manadatory_dict = {'id': subject.id, 'subject_name': subject.subject_name}
                subjectRecs.append(manadatory_dict)

            # student_recs = student_details.objects.filter(academic_class_section=year_class_section_obj)
            # for student_rec in student_recs:
            #     for subject in student_rec.subject_ids.all():
            #         optionalSubject.append(subject)
            # optionalSubject = set(optionalSubject)
            #
            # for subject in optionalSubject:
            #     optional_dict = {'id': subject.id, 'subject_name': subject.subject_name}
            #     subjectRecs.append(optional_dict)
            return JsonResponse(subjectRecs, safe=False)

    for obj in ClassSectionMap:
        subject_dict = {'id': obj.subject.id, 'subject_name': obj.subject.subject_name}
        modelDict.append(subject_dict)
    return JsonResponse(modelDict, safe=False)

def ViewGetExamFreq(request):
    freq_recs=[]
    exam_id = request.POST.get('exam_id')
    exam_details_recs = exam_details.objects.get(id=exam_id)
    fre_list = []
    frequency_list = exam_details_frequency.objects.filter(exam_details_name=exam_details_recs)
    for f_rec in frequency_list:
        f_id = f_rec.frequency_details_name.id
        fre_list.append(f_id)
    freq_obj = frequency_details.objects.all()
    for rec in freq_obj:
        ab={'id':rec.id, 'freq_name': rec.frequency_name,'fre_list':fre_list}
        freq_recs.append(ab)
    return JsonResponse(freq_recs,safe=False)

def ViewGetSubject(request):
    subject_recs=[]
    scheme_name = request.POST.get('scheme_name')
    subject_name = json.loads(request.POST.get('subject_name'))
    subject_mapped_flag = False  # Flag to check mappes subject to give message on UI

    if exam_scheme.objects.get(id=scheme_name).subject_exam_name == None:    #True if subject not mapped to a scheme so already selected is sending back
        subject_obj = subjects.objects.filter(id__in=subject_name)
        selected_list = list(subjects.objects.filter(id__in=subject_name).values_list('id',flat=1))
        for rec in subject_obj:
            ab = {'id': rec.id, 'subject': rec.subject_name,'selected_list':selected_list,'flag':subject_mapped_flag }
            subject_recs.append(ab)
        return JsonResponse(subject_recs, safe=False)

    else:   #False if subject mapped to a scheme so new subject is sending back
        subject_mapped_flag = True
        subject_obj = exam_scheme.objects.get(id=scheme_name).subject_exam_name
        selected_list = [exam_scheme.objects.get(id=scheme_name).subject_exam_name.id]
        ab = {'id': subject_obj.id, 'subject': subject_obj.subject_name,'selected_list':selected_list,'flag':subject_mapped_flag}
        subject_recs.append(ab)
    return JsonResponse(subject_recs, safe=False)


@user_login_required
def ViewExamMappingMandatoryOptionalSubject(request):
    subjectDict = []
    subjectRecs=[]
    optionalSubject = []
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    # request.POST.getlist('classes')
    section_name = json.loads( request.POST.get('section_name'))
    ac_class_id = []

    ac_recs = academic_class_section_mapping.objects.filter(Q(class_name_id=class_name,  year_name_id=year_name) & Q(section_name_id__in =section_name))
    for rec in ac_recs:
        ac_class_id.append(rec.id)

    subject_recs = mandatory_subject_mapping.objects.filter(academic_class_section_mapping_id__in = ac_class_id)

    ab = {}
    for subject in subject_recs:
        # ab={'id':subject.id, 'subject_name': subject.subject.subject_name}
        subjectDict.append(subject.subject_id)
    subjectDict = set(subjectDict)
    subject_data = subjects.objects.filter(id__in=subjectDict)
    for subject in subject_data:
        ab={'id':subject.id, 'subject_name': subject.subject_name}
        subjectRecs.append(ab)

    student_recs = student_details.objects.filter(academic_class_section_id__in  = ac_recs)
    for student_rec in student_recs:
        for subject in student_rec.subject_ids.all():
            optionalSubject.append(subject)

    optionalSubject = set(optionalSubject)

    for subject in optionalSubject:
        ab={'id':subject.id, 'subject_name': subject.subject_name}
        subjectRecs.append(ab)

    # json.dumps(scheme_recs.to_dict())
    return JsonResponse(subjectRecs,safe=False)


@user_login_required
def ViewMappedExamSubjectScheme(request):
    finalDict = []
    scheme_id = request.POST.get('scheme_id', None)
    academic_year = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)

    rec = exam_scheme_mapping_details.objects.get(exam_scheme=scheme_id,year=academic_year,class_obj=class_name)

    subject_recs = exam_scheme_subject_details.objects.filter(exam_scheme_obj=rec.id)
    ab = {}
    for subject in subject_recs:
        ab={'id':subject.subject.id, 'subject_name': subject.subject.subject_name}
        finalDict.append(ab)
    return JsonResponse(finalDict,safe=False)

@user_login_required
def ViewSubStrandsDetails(request):
    finalDict = []
    subject_name = request.GET.get('subject_name', None)
    strand_name = request.GET.get('strand_name', None)

    # subject_rec = subjects.objects.get(id=subject_name)
    # subject_list = subject_rec.strands_ids.all()

    strand_rec = subject_strands.objects.get(id=strand_name)
    sub_strand_list = strand_rec.substrands_ids.all()


    # class_name = request.POST.get('class_name', None)
    # rec = exam_scheme_mapping_details.objects.get(exam_scheme=scheme_id,year=academic_year,class_obj=class_name)
    #
    # subject_recs = exam_scheme_subject_details.objects.filter(exam_scheme_obj=rec.id)
    ab = {}
    for sub_rec in sub_strand_list:
        ab={'id':sub_rec.id, 'substrands_name': sub_rec.substrands_name}
        finalDict.append(ab)
    return JsonResponse(finalDict,safe=False)


######################## Reported Score Work ########################
@user_login_required
@user_permission_required('assessment.can_view_view_reported_categories', '/school/home/')
def ReportedScoreSubTile(request):
    return render(request, "reported_score_sub_tile.html")

def ReportedScoreList(request):
    reported_score_recs = reported_score_details.objects.all()
    return render(request, "reported_score_list.html",{'reported_score_recs':reported_score_recs})


def ReportedScoreCreation(request):
    exam_name_ids = exam_scheme_exam_name_mapping.objects.filter(is_archived = False).values_list('exam_name').distinct()
    exam_name_recs = exam_details.objects.filter(id__in = exam_name_ids)
    return render(request, "reported_score_creation.html",{'exam_name_recs':exam_name_recs})

def SaveReportedScore(request):

    code_name = request.POST.getlist('code_name')
    term_assessment = request.POST.getlist('term_assessment')
    weightage = request.POST.getlist('weightage')
    exam_name_rec = request.POST.getlist('exam_name')
    frequency_name = request.POST.getlist('frequency_name')


    form = reported_score_details.objects.create(report_name= code_name[0])

    grade_id = form.id
    if (grade_id >= 1) and (grade_id <= 9):
        report_code = "R-000" + str(grade_id)
        reported_score_details.objects.filter(id = grade_id).update(code = report_code)

    elif (grade_id >= 10) and (grade_id <= 99):
        report_code = "R-00" + str(grade_id)
        reported_score_details.objects.filter(id=grade_id).update(code=report_code)

    elif (grade_id >= 100) and (grade_id <= 999):
        report_code = "R-0" + str(grade_id)
        reported_score_details.objects.filter(id=grade_id).update(code=report_code)

    else:
        report_code = "R-" + str(grade_id)
        reported_score_details.objects.filter(id=grade_id).update(code=report_code)

    list_size = len(exam_name_rec)
    for count in range(list_size):
        exam_name_instance = exam_details.objects.get(id = exam_name_rec[count])
        frequency_instance = frequency_details.objects.get(id = frequency_name[count])
        grade_slab_form = reported_sub_details.objects.create(term_assessment=term_assessment[count],weightage=weightage[count],exam_name = exam_name_instance,frequency_name=frequency_instance)
        form.reported_score_id.add(grade_slab_form.id)
    return redirect('/school/reported_score_list/')


def UpdateReportedScore(request):

    data = json.loads(request.POST.get('data_value'))
    rep_obj_id = request.POST.get('rep_obj')

    try:
        for rec in data:
            rec['term_assessment'] = rec['term_assessment'].replace('+', ' ')
            try:
                reported_sub_details.objects.filter(id=rec['id']).update(term_assessment=rec['term_assessment'],
                                                                         weightage=rec['weightage'],
                                                                         exam_name=rec['exam_type'],
                                                                         frequency_name=rec['frequency'])
            except:
                rep_obj = reported_score_details.objects.get(id=rep_obj_id)
                rep_sub_obj = reported_sub_details.objects.create(term_assessment=rec['term_assessment'],
                                                                         weightage=rec['weightage'],
                                                                         exam_name_id=rec['exam_type'],
                                                                         frequency_name_id=rec['frequency'])
                rep_obj.reported_score_id.add(rep_sub_obj)


        messages.success(request, "Record Updated Successfully")
    except:
        messages.warning(request, "Some Error Occurred. Please Try again.")
    return redirect('/school/reported_score_list/')


def EditReportedScore(request, rec_id):
    reported_score_obj = reported_score_details.objects.get(id=rec_id)
    exam_name_ids = exam_scheme_exam_name_mapping.objects.filter(is_archived=False).values_list('exam_name').distinct()
    exam_name_recs = exam_details.objects.filter(id__in=exam_name_ids)
    # exam_name_recs = exam_details.objects.all()
    # frequency_recs = frequency_details.objects.all()
    report_exist_in_mapping_flag = sz_reported_score_academic_mapping.objects.filter(
        reported_exam_mapp=reported_score_obj).exists()

    return render(request, "reported_score_edit.html",
                  {'is_mapped': report_exist_in_mapping_flag, 'reported_score_obj': reported_score_obj,
                   'exam_name_recs': exam_name_recs})

def DeleteReportedScore(request):
        obj_id = request.POST.get('obj_id')
        reported_score_recs=reported_sub_details.objects.filter(id=obj_id).delete()
        return render(request, "/school/delete_reported_score/", {'reported_score_recs': reported_score_recs})
        # return redirect('/school/delete_reported_score/')


def ViewExamClassMappedSubjects(request):
    subject_recs = []
    subject_ids = []
    year_name = request.POST.get('academic_year', None)
    class_name = json.loads(request.POST.get('class_name'))
    acd_mapping_recs = exam_scheme_acd_mapping_details.objects.filter(year_id=year_name, class_obj_id=class_name)

    for exam_obj in acd_mapping_recs:
        if exam_obj.exam_name_mapping_ids.filter(is_archived=False):
            if exam_obj.subject.id not in subject_ids:
                subject_ids.append(exam_obj.subject.id)
                ab = {'id': exam_obj.subject.id, 'subject_name': exam_obj.subject.subject_name}
                subject_recs.append(ab)

    return JsonResponse(subject_recs, safe=False)


def ViewExamClassSubjectsMappedGrade(request):
    grade_recs = []
    grades_ids = []
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name')
    subject_name = request.POST.get('subject_name')
    acd_mapping_recs = exam_scheme_acd_mapping_details.objects.filter(year_id=year_name, class_obj=class_name,
                                                                      subject_id=subject_name)

    for acd_mapping_rec in acd_mapping_recs:
        for rec in acd_mapping_rec.exam_name_mapping_ids.filter(is_archived=False):
            if rec.grade_name.id not in grades_ids:
                grades_ids.append(rec.grade_name.id)
                ab = {'id': rec.grade_name.id, 'grade_name': rec.grade_name.grade_name}
                grade_recs.append(ab)

    return JsonResponse(grade_recs, safe=False)


def ViewReportedScoreMappedClasses(request):
    class_recs = []
    class_ids = []
    year_name = request.POST.get('academic_year', None)
    reported_score = request.POST.get('reported_score')

    reported_score_rec = reported_score_details.objects.get(id=reported_score)
    reported_exam_list = reported_score_rec.reported_score_id.all().values_list('exam_name_id', flat=1)
    acd_mapping_recs = exam_scheme_acd_mapping_details.objects.filter(year_id=year_name)

    for acd_mapping_rec in acd_mapping_recs:
        if acd_mapping_rec.class_obj.id not in class_ids:
            exam_ids = acd_mapping_rec.exam_name_mapping_ids.all().values_list('exam_name_id', flat=1)
            if len(set(exam_ids).intersection(reported_exam_list)) > 0:
                ab = {'id': acd_mapping_rec.class_obj.id, 'class_name': acd_mapping_rec.class_obj.class_name}
                class_recs.append(ab)
                class_ids.append(acd_mapping_rec.class_obj.id)

    return JsonResponse(class_recs, safe=False)


def ViewReportedScoreMappedSubjects(request):
    subject_recs = []
    subject_ids = []
    year_name = request.POST.get('academic_year', None)
    class_name = json.loads(request.POST.get('class_name', None))
    reported_score = request.POST.get('reported_score')

    reported_score_rec = reported_score_details.objects.get(id=reported_score)
    reported_exam_list = reported_score_rec.reported_score_id.all().values_list('exam_name_id', flat=1)
    acd_mapping_recs = exam_scheme_acd_mapping_details.objects.filter(year_id=year_name, class_obj__in=class_name)

    for acd_mapping_rec in acd_mapping_recs:
        if acd_mapping_rec.subject.id not in subject_ids:
            exam_ids = acd_mapping_rec.exam_name_mapping_ids.all().values_list('exam_name_id', flat=1)
            if len(set(exam_ids).intersection(reported_exam_list)) > 0:
                ab = {'id': acd_mapping_rec.subject.id, 'subject_name': acd_mapping_rec.subject.subject_name}
                subject_recs.append(ab)
                subject_ids.append(acd_mapping_rec.subject.id)
    return JsonResponse(subject_recs, safe=False)


def ViewReportedScoreMappedGrade(request):
    grade_recs = []
    grades_ids = []
    year_name = request.POST.get('academic_year', None)
    class_name = json.loads(request.POST.get('class_name'))
    subject_name = request.POST.get('subject_name')

    reported_score = request.POST.get('reported_score')
    reported_score_rec = reported_score_details.objects.get(id=reported_score)

    reported_exam_list = reported_score_rec.reported_score_id.all().values_list('exam_name_id', flat=1)
    # for exam_rec in reported_score_rec.reported_score_id.all():
    #     reported_exam_list.append(exam_rec.id)
    #     pass

    acd_mapping_recs = exam_scheme_acd_mapping_details.objects.filter(year_id=year_name, class_obj__in=class_name,
                                                                      subject_id=subject_name)

    for acd_mapping_rec in acd_mapping_recs:
        for rec in acd_mapping_rec.exam_name_mapping_ids.filter(is_archived=False):
            if rec.exam_name.id in reported_exam_list:
                if rec.grade_name.id not in grades_ids:
                    grades_ids.append(rec.grade_name.id)
                    ab = {'id': rec.grade_name.id, 'grade_name': rec.grade_name.grade_name}
                    grade_recs.append(ab)

    return JsonResponse(grade_recs, safe=False)

def ReportedScoreExamSelect(request):
    reported_score_recs = reported_score_details.objects.all()
    return render(request, "reported_score_exam_seclection.html",{'reported_score_recs':reported_score_recs})

def ReportedScoreExamMapping(request):
    code_name = request.POST.get('code')
    exam_name = request.POST.get('name')
    reported_score = request.POST.get('reported_score')
    reported_score_obj = reported_score_details.objects.get(id=reported_score)
    return render(request, "reported_score_exam_mapping.html",{'code_name':code_name,'exam_name':exam_name,'reported_score_obj':reported_score_obj})


@user_login_required
def ViewFrequencyDetails(request):
    finalDict = []
    exam_id = request.GET.get('exam_id', None)
    exam_rec = exam_details_frequency.objects.filter(exam_details_name=exam_id)
    for obj in exam_rec:
        exam_data={'id':obj.frequency_details_name.id, 'frequency_name': obj.frequency_details_name.frequency_name}
        finalDict.append(exam_data)
    return JsonResponse(finalDict,safe=False)

@user_login_required
def GetSystemFrequency(request):
    finalDict = []
    exam_id = request.GET.get('exam_id', None)
    report_id = request.GET.get('report_id', None)
    report_rec = reported_score_details.objects.get(id=report_id)
    rep_data = report_rec.reported_score_id.filter(exam_name=exam_id)
    for obj in rep_data:
        exam_data = {'id': obj.frequency_name.id, 'frequency_name': obj.frequency_name.frequency_name}
        finalDict.append(exam_data)
    return JsonResponse(finalDict, safe=False)

# @user_login_required
# def GetExamFrequency(request):
#     finalDict = []
#     exam_id = request.GET.get('exam_id', None)
#     report_rec = reported_score_details.objects.get(id=report_id)
#     rep_data = report_rec.reported_score_id.filter(exam_name=exam_id)
#     for obj in rep_data:
#         exam_data = {'id': obj.frequency_name.id, 'frequency_name': obj.frequency_name.frequency_name}
#         finalDict.append(exam_data)
#     return JsonResponse(finalDict, safe=False)

def ReportedScoreClassSectionMapping(request):
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
    except:
        messages.warning(request, "Current Academic Year Not Found.")
        return redirect('/school/reported_score_class_section_mapping/')

    class_rec = []
    class_id = []

    exam_obj = exam_scheme_acd_mapping_details.objects.filter(year = current_academic_year)

    for rec in exam_obj:
        if rec.exam_name_mapping_ids.filter(is_archived = False):
            if rec.class_obj.id not in class_id:
                class_id.append(rec.class_obj.id)
                class_rec.append(rec.class_obj)

    reported_score_recs = reported_score_details.objects.all()
    reported_class_sub_mapping_recs = reported_exam_class_sub_mapping.objects.all()
    grade_recs = grade_details.objects.filter(is_archive = False)
    return render(request, "reported_score_class_section_mapping_new.html", {'current_academic_year':current_academic_year,'class_recs':class_rec,'reported_score_recs':reported_score_recs,'reported_class_sub_mapping_recs':reported_class_sub_mapping_recs,'grade_recs':grade_recs})


def SaveReportedScoreClassSectionMapping(request):
    if request.method == 'POST':
        system_required_exp_flag = False
        try:
            year_id = request.POST.get('year_name')
            class_name = json.loads(request.POST.get('class_name'))
            repoted_name = request.POST.get('repoted_name')
            data_list = json.loads(request.POST.get('json_data'))
            reported_score_rec = reported_score_details.objects.get(id=repoted_name)

            for data_rec in data_list:
                for class_rec in class_name:
                    for rec in reported_score_rec.reported_score_id.all():
                        exam_scheme_mapping_obj_rec = exam_scheme_acd_mapping_details.objects.filter(year_id=year_id,
                                                                                                     # class_obj_id=data_rec['class_id'],
                                                                                                     class_obj_id=class_rec,
                                                                                                     subject_id=data_rec[
                                                                                                         'subject_id'])

                        if exam_scheme_mapping_obj_rec:

                            if exam_scheme_mapping_obj_rec[0].exam_name_mapping_ids.filter(exam_name=rec.exam_name,
                                                                                           is_archived=False).exists():

                                if reported_exam_class_sub_mapping.objects.filter(year_id=year_id,
                                                                                  # class_name_id=data_rec['class_id'],
                                                                                  class_name_id=class_rec,
                                                                                  subject_id=data_rec['subject_id'],
                                                                                  exam_name=rec.exam_name,
                                                                                  frequency_name=rec.frequency_name,
                                                                                  reported_exam_mapp=reported_score_rec).exists():
                                    pass
                                else:
                                    reported_exam_class_sub_mapping.objects.create(year_id=year_id,
                                                                                   # class_name_id=data_rec['class_id'],
                                                                                   class_name_id=class_rec,
                                                                                   subject_id=data_rec['subject_id'],
                                                                                   exam_name=rec.exam_name,
                                                                                   frequency_name=rec.frequency_name,
                                                                                   reported_exam_mapp=reported_score_rec,
                                                                                   grade_name_id=data_rec['grade_id'])
                            else:
                                if not reported_system_required_exceptions.objects.filter(year_id=year_id,
                                                                                          class_name_id=class_rec,
                                                                                          # class_name_id=data_rec['class_id'],
                                                                                          subject_id=data_rec['subject_id'],
                                                                                          exam_name=rec.exam_name,
                                                                                          reported_exam_mapp=reported_score_rec,
                                                                                          is_active=False).exists():

                                    if not reported_system_required_exceptions.objects.filter(year_id=year_id,
                                                                                              class_name_id=class_rec,
                                                                                              # class_name_id=data_rec['class_id'],
                                                                                              subject_id=data_rec['subject_id'],
                                                                                              exam_name=rec.exam_name,
                                                                                              reported_exam_mapp=reported_score_rec,
                                                                                              is_active=True).exists():

                                        reported_system_required_exceptions.objects.create(year_id=year_id,
                                                                                           class_name_id=class_rec,
                                                                                           # class_name_id=data_rec['class_id'],
                                                                                           subject_id=data_rec['subject_id'],
                                                                                           exam_name=rec.exam_name,
                                                                                           reported_exam_mapp=reported_score_rec,
                                                                                           grade_name_id=data_rec['grade_id'])
                                    system_required_exp_flag = True

            if system_required_exp_flag:
                return redirect('/school/reported_score_system_required_exp/' + str(reported_score_rec.id))

        except:
            messages.warning(request, "Current Academic Year Not Found.")
            return redirect('/school/reported_score_sub_tile/')
    return redirect('/school/reported_score_class_section_mapping/')

# def SaveReportedScoreClassSectionMapping(request):
#     if request.method == 'POST':
#         system_required_exp_flag = False
#         try:
#             year_id = request.POST.get('year_name')
#             repoted_name = request.POST.get('repoted_name')
#             data_list = json.loads(request.POST.get('json_data'))
#             reported_score_rec = reported_score_details.objects.get(id = repoted_name)
#
#             for data_rec in data_list:
#                 for rec in reported_score_rec.reported_score_id.all():
#                     exam_scheme_mapping_obj_rec = exam_scheme_acd_mapping_details.objects.filter(year_id=year_id,class_obj_id=data_rec['class_id'],subject_id=data_rec['subject_id'])
#                     if exam_scheme_mapping_obj_rec[0].exam_name_mapping_ids.filter(exam_name=rec.exam_name).exists():
#                         if reported_exam_class_sub_mapping.objects.filter(year_id=year_id, class_name_id=data_rec['class_id'], subject_id=data_rec['subject_id'],exam_name=rec.exam_name, frequency_name=rec.frequency_name,reported_exam_mapp=reported_score_rec).exists():
#                             pass
#                         else:
#                             reported_exam_class_sub_mapping.objects.create(year_id=year_id, class_name_id=data_rec['class_id'], subject_id=data_rec['subject_id'],exam_name=rec.exam_name,frequency_name=rec.frequency_name, reported_exam_mapp=reported_score_rec,grade_name_id=data_rec['grade_id'])
#                     else:
#                         exam_scheme_mapping_obj_rec = exam_scheme_acd_mapping_details.objects.filter(year_id=year_id,class_obj_id=data_rec['class_id'],subject_id=data_rec['subject_id'])
#                         for exam_rec in exam_scheme_mapping_obj_rec[0].exam_name_mapping_ids.all():
#                             if reported_exam_class_sub_mapping.objects.filter(year_id=year_id,class_name_id=data_rec['class_id'], subject_id=data_rec['subject_id'], exam_name=exam_rec.exam_name, reported_exam_mapp=reported_score_rec).exists():
#                                 pass
#                             else:
#                                 if not reported_system_required_exceptions.objects.filter(year_id=year_id,class_name_id=data_rec['class_id'], subject_id=data_rec['subject_id'],exam_name=exam_rec.exam_name,reported_exam_mapp=reported_score_rec, is_active=True).exists():
#                                     if reported_system_required_exceptions.objects.filter(year_id=year_id,class_name_id=data_rec['class_id'], subject_id=data_rec['subject_id'],reported_exam_mapp=reported_score_rec, is_active=True,report_sub_name_id = rec.id).exists():
#                                         pass
#                                     else:
#                                         report_system_obj = reported_system_required_exceptions.objects.create(year_id=year_id,class_name_id=data_rec['class_id'], subject_id=data_rec['subject_id'],exam_name=exam_rec.exam_name,reported_exam_mapp=reported_score_rec,grade_name_id=data_rec['grade_id'],report_sub_name_id = rec.id)
#                                 system_required_exp_flag = True
#
#             if system_required_exp_flag:
#                 return redirect('/school/reported_score_system_required_exp/' + str(reported_score_rec.id))
#         except:
#             messages.warning(request, "Current Academic Year Not Found.")
#             return redirect('/school/reported_score_sub_tile/')
#     return redirect('/school/reported_score_class_section_mapping/')


def DeleteReportedScoreClassSectionMapping(request):
    if request.method == 'POST':
        check = request.POST.getlist('check')

        for rec_id in check:
            reported_exam_class_sub_mapping.objects.get(id=rec_id).delete()
    return redirect('/school/reported_score_class_section_mapping/')

def ReportedScoreSystemRequiredExp(request,rec_id):
    reported_score_rec = reported_score_details.objects.get(id=rec_id)
    system_required_exp = reported_system_required_exceptions.objects.filter(reported_exam_mapp=reported_score_rec, is_active = True)
    class_rec = []
    class_id = []

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    exam_obj = exam_scheme_acd_mapping_details.objects.filter(year=current_academic_year)

    for rec in exam_obj:
        if rec.exam_name_mapping_ids.filter(is_archived = False):
            if rec.class_obj.id not in class_id:
                class_id.append(rec.class_obj.id)
                class_rec.append(rec.class_obj)

    reported_score_recs = reported_score_details.objects.all()
    reported_class_sub_mapping_recs = reported_exam_class_sub_mapping.objects.all()

    return render(request, "reported_score_sys_exception.html",
                  {'current_academic_year': current_academic_year, 'class_recs': class_rec,
                   'reported_score_recs': reported_score_recs,
                   'reported_score_rec': reported_score_rec,
                   'reported_class_sub_mapping_recs': reported_class_sub_mapping_recs,
                   'system_required_exp': system_required_exp})

def SaveSystemRequiredExp(request):

    reported_score = request.POST.get('reported_score')
    data = json.loads(request.POST.get('json_data'))
    current_academic_year = academic_year.objects.get(current_academic_year=1)

    for rec in data:
        if not reported_exam_class_sub_mapping.objects.filter(
            year_id=current_academic_year.id, class_name_id=rec['cls'], subject_id=rec['subj'], exam_name_id=rec['exam'],
            frequency_name_id=rec['frequency'], reported_exam_mapp_id=reported_score).exists():

            reported_exam_class_sub_mapping.objects.create(
                year_id=current_academic_year.id, class_name_id=rec['cls'], subject_id=rec['subj'], exam_name_id=rec['exam'],
                frequency_name_id=rec['frequency'], reported_exam_mapp_id=reported_score,grade_name_id=rec['grad'])
            reported_system_required_exceptions.objects.filter(id = rec['obj_id']).update(is_active = False)



    # return redirect('/school/reported_score_class_section_mapping/')
    return redirect('/school/user_defined_exp/' + str(reported_score))



def ReportedScoreExport(request):
    reported_score_list = []
    reported_score_obj = None
    reported_score_recs = reported_score_details.objects.all()
    column_names = ['Code','Reported Score Name','Exam Type','Weightage']
    try:
        for reported_score_rec in reported_score_recs:
            for rec in reported_score_rec.reported_score_id.all():
                rec_list = []
                rec_list.append(reported_score_rec.code)
                rec_list.append(reported_score_rec.report_name)
                rec_list.append(rec.exam_type.exam_type_name)
                rec_list.append(rec.weightage)
                reported_score_list.append(rec_list)
        reported_score_obj = reported_score_export_log.objects.create(uploaded_by = request.user)
        return export_users_xls('ReportedScoreDetails', column_names, reported_score_list)
    except:
        pass
    return redirect('/school/reported_score_list/')

def ReportedScoreExportLog(request):
    reported_score_log_recs = reported_score_export_log.objects.all()
    return render(request, "reported_score_export_log.html", {'reported_score_log_recs': reported_score_log_recs})

def ReportedScoreStatus(request):
    reported_class_sub_mapping_recs = reported_exam_class_sub_mapping.objects.all()

    report_class_list = []
    class_ids = []
    subject_ids = []
    report_ids = []
    grade_ids = []

    final_list = []

    for rec in reported_class_sub_mapping_recs:
        if rec.class_name.id not in class_ids or rec.subject.id not in subject_ids or rec.reported_exam_mapp.id not in report_ids or rec.grade_name.id not in grade_ids:
            section_list = current_class_mapped_sections(rec.year.id, rec.class_name.id, request.user)

            for section_rec in section_list:
                temp_list = []
                ac_recs = academic_class_section_mapping.objects.get(class_name_id=rec.class_name,
                                                                     year_name_id=rec.year,
                                                                     section_name=section_rec['id'])

                temp_list.append(rec.year)
                temp_list.append(rec.class_name)
                temp_list.append(section_rec['section_name'])
                temp_list.append(rec.subject)
                temp_list.append(rec.reported_exam_mapp)
                temp_list.append(rec.grade_name)

                if sz_reported_score_academic_mapping.objects.filter(academic_mapping=ac_recs, subject=rec.subject,
                                                                     reported_exam_mapp=rec.reported_exam_mapp,
                                                                     grade_scheme=rec.grade_name).exists():

                    status_rec = sz_reported_score_academic_mapping.objects.get(academic_mapping=ac_recs, subject=rec.subject,
                                                                      reported_exam_mapp=rec.reported_exam_mapp,
                                                                      grade_scheme=rec.grade_name)
                    temp_list.append('Submitted')
                    temp_list.append(status_rec.submitted_by.get_pura_name())
                    temp_list.append(status_rec.created_on)
                else:
                    temp_list.append('Not-Submitted')
                    temp_list.append('')
                    temp_list.append('')
                final_list.append(temp_list)

            class_ids.append(rec.class_name.id)
            subject_ids.append(rec.subject.id)
            report_ids.append(rec.reported_exam_mapp.id)
            grade_ids.append(rec.grade_name.id)


# def ReportedScoreStatus(request):
    reported_score_status_recs = sz_reported_score_academic_mapping.objects.all()
    return render(request, "reported_score_status.html", {'reported_score_status_recs': reported_score_status_recs,'final_list':final_list})

def ReportedScoreCalculationBulkView(request):
    reported_score_recs = reported_score_details.objects.all()
    current_academic_year = academic_year.objects.get(current_academic_year=1)
    return render(request, "reported_score_calculation_bulk_view.html", {'reported_score_recs': reported_score_recs,'current_academic_year':current_academic_year})


def load_bulk_view_recs(val_dict_bulk_view, request):
    year_name = val_dict_bulk_view.get('year_name')  # It will be used in HTML page as a value of selected Class
    reported_score = val_dict_bulk_view.get('reported_score')  # It will be used in HTML page as a value of selected Section
    acd_year = academic_year.objects.get(id=year_name)

    reported_score_obj = reported_score_details.objects.get(id=reported_score)

    reported_exam_class_sub_mapping_recs = reported_exam_class_sub_mapping.objects.filter(reported_exam_mapp=reported_score_obj)
    class_list = []
    subject_list = []
    report_list = []

    for obj in reported_score_obj.reported_score_id.all():
        report_list.append(obj.term_assessment)

    for rec in reported_exam_class_sub_mapping_recs:
        class_list.append(rec.class_name.class_name)
        subject_list.append(rec.subject.subject_name)

    rec_list = []
    for rec in reported_score_obj.reported_score_id.all():

        freq_obj = reported_exam_class_sub_mapping.objects.filter(reported_exam_mapp=reported_score_obj).filter(exam_name = rec.exam_name).values_list('frequency_name', flat=True).distinct()
        freq_rec = frequency_details.objects.filter(id__in = freq_obj).values_list('frequency_name', flat=True)

        system_exception = reported_system_required_exceptions.objects.filter(reported_exam_mapp=reported_score_obj, is_active = False).filter(exam_name=rec.exam_name)

        dict_rec = {
            'id':rec.id,
            'weightage': rec.weightage,
            'exam_name': rec.exam_name,
            'freq': rec.frequency_name,
            'term_assessment': rec.term_assessment,
            'frequency_name': freq_rec,
            'system_exception' : system_exception,
            'prev_frequency':rec.frequency_name
        }
        rec_list.append(dict_rec)

    form_vals = {'acd_year': acd_year,
                 'subject_list': set(subject_list),
                 'class_list': set(class_list),
                 'rec_list':rec_list,
                 'reported_score_obj': reported_score_obj,
                 }
    return form_vals

@user_login_required
def ReportedScoreCalculationBulkViewRecs(request):
    val_dict_bulk_view = request.POST

    if request.method == 'POST':
        request.session['val_dict_bulk_view'] = val_dict_bulk_view
        form_vals = load_bulk_view_recs(val_dict_bulk_view, request)
    else:
        form_vals = load_bulk_view_recs(request.session.get('val_dict_bulk_view'), request)

    return render(request, "reported_score_calculation_bulk_view_recs.html", form_vals)


@user_login_required
def ReportedScoreSubmissionSave(request):
        if request.method == 'POST':
            year_id = request.POST.get('acd_year')
            class_id = request.POST.get('class')
            section_id = request.POST.get('section')
            subject_id = request.POST.get('sub')
            rep_score = request.POST.get('rep_scr')
            grade_name = request.POST.get('grd')
            data = json.loads(request.POST.get('data'))

            reported_sub_list = []

            reported_score_obj = reported_score_details.objects.get(id=rep_score)
            acd_mapp_obj = academic_class_section_mapping.objects.get(year_name_id=year_id, section_name_id=section_id,class_name_id=class_id)

            for rep_rec in reported_score_obj.reported_score_id.all():
                reported_sub_list.append(rep_rec)

            approve_flag = False

            for exam_rec in reported_sub_list:
                exam_scheme_rec = exam_scheme_acd_mapping_details.objects.get(year_id=year_id, class_obj_id=class_id,subject_id=subject_id)
                scheme_mapping_id = exam_scheme_rec.exam_name_mapping_ids.get(exam_name_id=exam_rec.exam_name.id)
                exam_scheme_rec = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping_id=acd_mapp_obj.id,scheme_mapping_id=scheme_mapping_id.id,frequency_id=exam_rec.frequency_name.id,is_apprpved=False)
                if exam_scheme_rec:
                    approve_flag=True

            if approve_flag:
                messages.warning(request, "Please approve the marks for the underlying exams.")
                return redirect('/school/reported_score_submission/')

            academic_frequency_mapping_obj = sz_reported_score_academic_mapping.objects.create(academic_mapping = acd_mapp_obj, subject_id =subject_id,
                                                                                               reported_exam_mapp =reported_score_obj,
                                                                                               is_submitted = True,
                                                                                               grade_scheme_id = grade_name,
                                                                                               submitted_by = request.user)

            for rec in data:
                rs_acd_student_mapping = sz_reported_score_acd_student_mapping.objects.create(student_id = rec['student'], earned_mark = rec['total'],
                                                                                                total_mark = rec['max'], weightage = rec['weightage'],
                                                                                                grade_name_id = rec['grade'],rs_academic_mapping = academic_frequency_mapping_obj)

                for count , subtopic in enumerate(reported_sub_list):
                    count = count+1
                    scheme_subtopic_obj = sz_reported_score_acd_student_mark_mapping.objects.create(
                        rs_academic_mapping = academic_frequency_mapping_obj, term_ass_id = rec['rep_obj_' + str(count)],
                        obtained_mark = rec['total_mark_' + str(count)],outoff_mark = rec['max_score_' + str(count)])


        return redirect('/school/reported_score_sub_tile/')


def ReportedScoreCalculationBulkViewUpdate(request):
    data = json.loads(request.POST.get('data'))
    for rec in data:
        reported_sub_details.objects.filter(id = rec['exam_obj_id']).update(term_assessment = rec['term_assessment'],weightage = rec['weightage'])

    return redirect('/school/reported_score_calculation_bulk_view_recs/')


@user_login_required
def ViewReportedMappedSubjects(request):
    finalDict = []
    subject_id_list = []
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    reported_mapped_subjects = reported_exam_class_sub_mapping.objects.filter(year=year_name, class_name=class_name).values_list('subject',flat=1).distinct()
    subjects_recs = subjects.objects.filter(id__in =reported_mapped_subjects)
    for rec in subjects_recs:
        subject_dict = {'id': rec.id, 'subject_name': rec.subject_name}
        finalDict.append(subject_dict)
    return JsonResponse(finalDict, safe=False)

@user_login_required
def ViewSubjectMappedReportedScore(request):
    finalDict = []
    subject_id_list = []
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    subject = request.POST.get('subject', None)
    reported_mapped= reported_exam_class_sub_mapping.objects.filter(year=year_name, class_name=class_name, subject = subject).values_list('reported_exam_mapp',flat=1).distinct()
    reported_recs = reported_score_details.objects.filter(id__in =reported_mapped)
    for rec in reported_recs:
        reported_dict = {'id': rec.id, 'report': rec.report_name}
        finalDict.append(reported_dict)
    return JsonResponse(finalDict, safe=False)



def ReportedScoreSubmission(request):
    try:
        academic_year_obj = academic_year.objects.get(current_academic_year=1)
    except:
        messages.warning(request, "Current Academic Year Not Found.")
        return redirect('/school/reported_score_sub_tile/')

    class_rec = []
    filtered_class_ids = reported_exam_class_sub_mapping.objects.filter(year = academic_year_obj).values_list('class_name', flat=1).distinct()
    class_rec = class_details.objects.filter(id__in=filtered_class_ids)
    # class_id = []
    #
    # exam_obj = exam_scheme_acd_mapping_details.objects.filter(year = academic_year_obj)
    #
    # for rec in exam_obj:
    #     if rec.exam_name_mapping_ids.filter(is_archived = False):
    #         if rec.class_obj.id not in class_id:
    #             class_id.append(rec.class_obj.id)
    #             class_rec.append(rec.class_obj)

    reported_score_recs = reported_score_details.objects.all()

    return render(request, "reported_score_submission.html", {'year_name':academic_year_obj,'class_list':class_rec,'reported_score_recs':reported_score_recs})


# def MappedGradesScheme(request):
#     year_name = request.POST.get('academic_year')
#     reported_score = request.POST.get('reported_score')
#     subject_name = request.POST.get('subject_name')
#     section_name = request.POST.get('section_name')
#     class_name = request.POST.get('class_name')
#
#     grade_scheme_recs = []
#     grade_scheme_data = []
#
#     academic_mapping = academic_class_section_mapping.objects.get(year_name_id=year_name, class_name_id=class_name,
#                                                                   section_name_id=section_name)
#
#     reported_score_obj = reported_score_details.objects.get(id=reported_score)
#     reported_mapping_recs = reported_exam_class_sub_mapping.objects.filter(reported_exam_mapp=reported_score_obj,
#                                                                            subject_id=subject_name,
#                                                                            class_name_id=class_name)
#     for rec in reported_mapping_recs:
#         if exam_scheme_acd_mapping_details.objects.filter(year=rec.year, class_obj=rec.class_name,
#                                                           subject=rec.subject).exists():
#             exam_mapping_parent_obj = exam_scheme_acd_mapping_details.objects.get(year=rec.year,
#                                                                                   class_obj=rec.class_name,
#                                                                                   subject=rec.subject)
#
#             if exam_mapping_parent_obj.exam_name_mapping_ids.filter(exam_name=rec.exam_name).exists():
#                 exam_mapping_child_obj = exam_mapping_parent_obj.exam_name_mapping_ids.get(exam_name=rec.exam_name)
#                 if exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping=academic_mapping,
#                                                                          scheme_mapping=exam_mapping_child_obj,
#                                                                          frequency=rec.frequency_name,is_submitted = True).exists():
#                     enter_mark_obj = exam_scheme_academic_frequency_mapping.objects.get(
#                         academic_mapping=academic_mapping, scheme_mapping=exam_mapping_child_obj,
#                         frequency=rec.frequency_name,is_submitted = True)
#
#                     if enter_mark_obj.scheme_mapping.grade_name.id not in grade_scheme_data:
#                         grade_scheme_data.append(enter_mark_obj.scheme_mapping.grade_name.id)
#                         grade_dict = {'id':enter_mark_obj.scheme_mapping.grade_name.id, 'grade_name':enter_mark_obj.scheme_mapping.grade_name.grade_name}
#                         grade_scheme_recs.append(grade_dict)
#
#     return JsonResponse(grade_scheme_recs,safe=False)


def load_reported_score_submission(val_dict_submission, request):
    year_name = val_dict_submission.get('year_name')
    reported_score = val_dict_submission.get('reported_score')
    subject_name = val_dict_submission.get('subject_name')
    section_name = val_dict_submission.get('section_name')
    class_name = val_dict_submission.get('class_name')
    # greading_scheme = val_dict_submission.get('greading_scheme')
    export = val_dict_submission.get('export')

    class_rec = class_details.objects.get(id=class_name)
    section_rec = sections.objects.get(id=section_name)
    subject_rec = subjects.objects.get(id=subject_name)
    acd_year = academic_year.objects.get(id=year_name)

    round_master = assessment_round_master.objects.all()[0]

    try:
        greading_scheme = reported_exam_class_sub_mapping.objects.filter(year_id=year_name, class_name_id=class_name,subject_id=subject_name,reported_exam_mapp_id=reported_score)[0].grade_name.id
    except:
        form_vals = {}
        export_flag = 2

        return form_vals, export_flag

    greading_recs = grade_sub_details.objects.filter(grade_id=greading_scheme)
    grade_rec = grade_sub_details.objects.filter(grade_id=greading_scheme)[0].grade

    reported_score_recs = reported_score_details.objects.all()
    reported_mapped = reported_exam_class_sub_mapping.objects.filter(year=year_name, class_name=class_name,
                                                                     subject=subject_name).values_list('reported_exam_mapp',flat=1).distinct()
    reported_score_recs = reported_score_details.objects.filter(id__in=reported_mapped)

    recs_is_saved = False
    export_flag = False

    academic_mapping = academic_class_section_mapping.objects.get(year_name_id=year_name, class_name_id=class_name,
                                                                  section_name_id=section_name)
    reported_score_obj = reported_score_details.objects.get(id=reported_score)
    if sz_reported_score_academic_mapping.objects.filter(academic_mapping=academic_mapping, subject=subject_rec,
                                                         reported_exam_mapp=reported_score_obj,
                                                         grade_scheme=grade_rec).exists():
        recs_is_saved = True

    col_full_arr = []
    for color in greading_recs:  # getting grades and creating its dict for the backend grade
        color_dic = {
            'mar_less': float(color.marks_greater),
            'mar_greater': float(color.marks_less),
            'grade': color.grades,
            'grade_obj': color
        }
        col_full_arr.append(color_dic)

    filtered_class_ids = reported_exam_class_sub_mapping.objects.filter(year=year_name).values_list('class_name',
                                                                                                    flat=1).distinct()
    class_list = class_details.objects.filter(id__in=filtered_class_ids)
    section_list = current_class_mapped_sections(acd_year, class_rec, request.user)

    # exam_scheme_obj = exam_scheme_acd_mapping_details.objects.filter(year_id=year_name, class_obj_id=class_name)

    reported_mapped_subjects = reported_exam_class_sub_mapping.objects.filter(year=year_name,
                                                                              class_name=class_name).values_list('subject', flat=1).distinct()
    subject_list = subjects.objects.filter(id__in=reported_mapped_subjects)
    reported_dict = {}
    reported_rec = {}

    grade_scheme_recs = []

    for weig_rec in reported_score_obj.reported_score_id.all():
        reported_dict[weig_rec.exam_name, weig_rec.frequency_name] = weig_rec.weightage
        reported_rec[weig_rec.exam_name, weig_rec.frequency_name] = weig_rec.id
    reported_mapping_recs = reported_exam_class_sub_mapping.objects.filter(reported_exam_mapp=reported_score_obj,
                                                                           subject_id=subject_name,
                                                                           class_name_id=class_name)

    student_recs = []
    for rec in reported_mapping_recs:
        if exam_scheme_acd_mapping_details.objects.filter(year=rec.year, class_obj=rec.class_name,
                                                          subject=rec.subject).exists():
            exam_mapping_parent_obj = exam_scheme_acd_mapping_details.objects.get(year=rec.year,
                                                                                  class_obj=rec.class_name,
                                                                                  subject=rec.subject)

            if exam_mapping_parent_obj.exam_name_mapping_ids.filter(exam_name=rec.exam_name,
                                                                    is_archived=False).exists():
                exam_mapping_child_obj = exam_mapping_parent_obj.exam_name_mapping_ids.get(exam_name=rec.exam_name,
                                                                                           is_archived=False)

                if exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping=academic_mapping,
                                                                         scheme_mapping=exam_mapping_child_obj,
                                                                         frequency=rec.frequency_name,
                                                                         is_submitted=True, is_archived=False).exists():

                    enter_mark_obj = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping=academic_mapping,
                                                                          scheme_mapping=exam_mapping_child_obj,
                                                                          frequency=rec.frequency_name,
                                                                          is_submitted=True, is_archived=False)[0]

                    if enter_mark_obj.scheme_mapping.grade_name not in grade_scheme_recs:
                        grade_scheme_recs.append(enter_mark_obj.scheme_mapping.grade_name)

                    student_list = []
                    for student_rec in enter_mark_obj.student_mark_ids.all():
                        rep_obj = reported_rec[enter_mark_obj.scheme_mapping.exam_name, enter_mark_obj.frequency]
                        rec_dict = {  # creating sub dict for static records
                            'student': student_rec.student,
                            'total_mark': student_rec.total_mark,
                            'max_score': student_rec.max_mark,
                            'freq': enter_mark_obj.frequency,
                            'exam': enter_mark_obj.scheme_mapping.exam_name,
                            'rep_obj': rep_obj,
                        }
                        student_list.append(rec_dict)
                    student_recs.append(student_list)

    final_list = []

    for data in range(len(student_recs)):
        for rec in range(len(student_recs[data])):
            final_list.append(student_recs[data][rec])

    final_dict = {}
    for i in final_list:
        if i["student"] not in final_dict:
            final_dict[i["student"]] = {'rec': [
                {'max_score': i['max_score'], 'total_mark': i['total_mark'], 'freq': i['freq'], 'exam': i['exam'],
                 'rep_obj': i['rep_obj']}]}
        else:
            final_dict[i["student"]]['rec'].append(
                {'max_score': i['max_score'], 'total_mark': i['total_mark'], 'freq': i['freq'], 'exam': i['exam'],
                 'rep_obj': i['rep_obj']})

    rec_list = []
    for i in final_dict:
        tot = 0
        max = 0
        weightage = 0.0
        grade = ''
        for rec in final_dict[i]["rec"]:
            if (rec['max_score'] == '' or rec['max_score'] == None):
                rec['max_score'] = 0

            if (rec['total_mark'] == '' or rec['total_mark'] == None):
                rec['total_mark'] = 0

            rep_percentage = reported_dict[rec['exam'], rec['freq']]
            tot = tot + float(rec['total_mark'])
            max = max + float(rec['max_score'])
            if float(rec['max_score']) != 0:  # getting weightage and applying the formula of grades
                try:
                    if round_master.select:
                        weightage = weightage + float_round(((((float(float(rec['total_mark'])) / float(
                            rec['max_score'])) * 100) * float(rep_percentage)) / 100), int(round_master.decimal_place),
                                                            ceil)
                    else:
                        weightage = weightage + float_round(((((float(float(rec['total_mark'])) / float(
                            rec['max_score'])) * 100) * float(rep_percentage)) / 100), int(round_master.decimal_place),
                                                            floor)
                except:
                    weightage = weightage + round((((float(float(rec['total_mark'])) / float(
                        rec['max_score'])) * 100) * float(rep_percentage)) / 100, 2)

                # weightage = weightage + math.ceil((((float(float(rec['total_mark']))/float(rec['max_score']))*100)*float(rep_percentage))/100)
            else:
                weightage = weightage + 0.0

        # if weightage != 0:
        for color_rec in col_full_arr:
            if color_rec['mar_less'] <= weightage <= color_rec[
                'mar_greater']:  # getting grade of the specific weightage
                grade = color_rec['grade_obj']

        ab = {
            'max': max,
            'total': tot,
            'stud': i,
            'data': final_dict[i]["rec"],
            'weightage': float(weightage),
            'grade': grade,
            # 'rep_obj':rep_obj,
        }
        rec_list.append(ab)

    final_export_list = []
    if export == 'true':  # If record is already submitted it will export the record as csv file
        column_names = ['Student Code', 'Student Name', 'Class', 'Section']
        # ['Earned Score','Total Score','Weightage','Grade']
        for exam_rec in reported_score_obj.reported_score_id.all():
            column_names.append(str(exam_rec.term_assessment) + '(' + str(exam_rec.weightage) + ')')
            column_names.append('Max Score')
        column_names.append('Earned Score')
        column_names.append('Total Score')
        column_names.append('Weightage')
        column_names.append('Grade')

        for student in rec_list:  # creating list of records
            student_export_list = []
            count = 0
            student_export_list.append(student['stud'].student_code)
            student_export_list.append(student['stud'].first_name + ' ' + student['stud'].last_name)
            student_export_list.append(student['stud'].academic_class_section.class_name.class_name)
            student_export_list.append(student['stud'].academic_class_section.section_name.section_name)

            for exam_rec in reported_score_obj.reported_score_id.all():
                val = student['data'][count]
                if val['freq'].frequency_name == exam_rec.frequency_name.frequency_name and val[
                    'exam'].exam_name == exam_rec.exam_name.exam_name:
                    student_export_list.append(val['total_mark'])
                    student_export_list.append(val['max_score'])
                    count = count + 1
                else:
                    student_export_list.append('')
                    student_export_list.append('')

            student_export_list.append(student['total'])
            student_export_list.append(student['max'])
            student_export_list.append(student['weightage'])
            student_export_list.append(student['grade'].grades)

            final_export_list.append(student_export_list)
        export_flag = True
        form_vals = {
            'column_names': column_names,
            'final_export_list': final_export_list
        }

        return form_vals, export_flag

    form_vals = {
        'subject_rec': subject_rec,
        'class_rec': class_rec,
        'section_rec': section_rec,
        'year_name': acd_year,
        'reported_score_obj': reported_score_obj,
        'student_recs': student_recs,
        'final_dict': final_dict,
        'list': rec_list,
        'total_count': make_incrementor(0),
        'th_count': make_incrementor(0),
        'class_list': class_list,
        'section_list': section_list,
        'subject_list': subject_list,
        'reported_score_recs': reported_score_recs,
        'grade_scheme_recs': grade_scheme_recs,
        'grade_rec': grade_rec,
        'count_init': make_incrementor(0),
        'recs_is_saved': recs_is_saved,
    }
    return form_vals, export_flag


@user_login_required
def ReportedScoreSubmissionView(request):
    val_dict_submission = request.POST
    export_flag = False

    if request.method == 'POST':
        request.session['val_dict_submission'] = val_dict_submission
        form_vals, export_flag = load_reported_score_submission(val_dict_submission, request)
    else:
        form_vals, export_flag = load_reported_score_submission(request.session.get('val_dict_submission'), request)
    if export_flag == 2:
        messages.warning(request, "Mapping Not Found.")
        return redirect('/school/reported_score_submission/')
    if export_flag:
        reported_score_obj = reported_score_export_log.objects.create(uploaded_by=request.user)
        return export_users_xls('ReportedScoreDetails', form_vals['column_names'], form_vals['final_export_list'])
    return render(request, "reported_score_submission.html", form_vals)

###################################### Target Setting ################################################################

@user_login_required
def TargetSettingSubTile(request):
    # print "In function"
    return render(request, "target_setting_sub_tile.html")

@user_login_required
def CheckTargetCode(request):
    code = request.GET.get('code')
    data = {
        'is_taken': sz_target_setting_details.objects.filter(code=code).exists()
        }
    if data['is_taken']:
        data['error_message'] = 'Target Code already Exists!'
    return JsonResponse(data)

@user_login_required
def CheckTargetName(request):
    name = request.GET.get('name')
    data = {
        'is_taken': sz_target_setting_details.objects.filter(target_name=name).exists()
        }
    if data['is_taken']:
        data['error_message'] = 'Target Name already Exists!'
    return JsonResponse(data)


def TargetSettingCreation(request):
    report_score_recs = reported_score_details.objects.all()
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
    except:
        messages.warning(request, "Current Academic Year Not Found.")
        return redirect('/school/target_setting_list/')
    class_rec = []
    obj = exam_scheme_acd_mapping_details.objects.filter(year=current_academic_year)

    for rec in obj:
        if rec.exam_name_mapping_ids.filter(is_archived = False):
            if rec.class_obj not in class_rec:
                class_rec.append(rec.class_obj)
    acd_recs = academic_class_section_mapping.objects.filter(year_name=current_academic_year)
    exam_recs = exam_details.objects.all()
    return render(request, "define_target_setting.html",{'exam_recs':exam_recs,'current_academic_year':current_academic_year,'acd_recs':acd_recs,'report_score_recs':report_score_recs,'class_recs':class_rec})


@user_login_required
def ViewAcademicDetail(request):
    modelDict = []
    academic_year_id = request.GET.get('academic_year', None)
    academic_class_section_mapping_classes = []

    if request.user.is_system_admin():
        academic_class_section_mapping_classes = academic_class_section_mapping.objects.filter(
            year_name=academic_year_id)
    else:
        # academic_class_section_mapping_classes = academic_class_section_mapping.objects.filter(year_name=academic_year_id, staff=request.user)
        if request.user.is_supervisor():
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=academic_year_id):
                for obj in mapped_object.supervisor_academic_class_section_relation.all().filter(
                        supervisor_id=request.user.id):
                    academic_class_section_mapping_classes.append(mapped_object)


        # elif request.user.is_subject_teacher():
        #
        #     current_academic_year = academic_year.objects.get(current_academic_year=1)
        #     for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
        #         for obj in teacher_subject_mapping.objects.filter(academic_class_section_mapping=mapped_object,
        #                                                           staff_id=request.user.id):
        #             if obj:
        #                 for obj in student_details.objects.filter(academic_class_section=mapped_object):
        #                     student_list.append(obj)


        else:
            academic_class_section_mapping_classes = academic_class_section_mapping.objects.filter(
                year_name=academic_year_id).filter(Q(assistant_teacher=request.user.id) | Q(staff_id=request.user.id))

    raw_list = []
    for obj in academic_class_section_mapping_classes:
        raw_list.append(obj.class_name)
    class_obj_list = list(set(raw_list))
    for obj in class_obj_list:
        modelDict.append(obj.to_dict())
    return JsonResponse(modelDict, safe=False)


# def SaveTargetSetting(request):
#     year_name = request.POST.get('year_name')
#     class_name = request.POST.get('class_name')
#     subject_name = request.POST.get('subject_name')
#     report_score_name = request.POST.get('report_score_name')
#     exam_name_recs = request.POST.getlist('exam_name')
#     frequency_name_recs = request.POST.getlist('frequency_name')
#     form = target_setting_details.objects.create(year_name_id= year_name,class_name_id=class_name,subject_name_id=subject_name,report_score_name_id=report_score_name)
#     list_size = len(exam_name_recs)
#     for count in range(list_size):
#         exam_name_obj = exam_details.objects.get(id = exam_name_recs[count])
#         frequency_name_obj = frequency_details.objects.get(id = frequency_name_recs[count])
#         target_sub_form = target_setting_sub_details.objects.create(exam_name = exam_name_obj,frequency_name=frequency_name_obj)
#         form.target_setting_id.add(target_sub_form.id)
#     return redirect('/school/target_setting_list/')
@user_login_required
@user_permission_required('assessment.can_view_view_target_categories', '/school/home/')
def TargetSettingList(request):
    #sz_target_setting_sub_sub_details.objects.all().delete()
    #sz_target_setting_sub_details.objects.all().delete()
    #sz_target_setting_details.objects.all().delete()
    target_setting_recs = sz_target_setting_details.objects.all()
    return render(request, "target_setting_list.html",{'target_setting_recs':target_setting_recs})

def getSelectedClassesRec(request):
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
    except:
        messages.warning(request, "Current Academic Year Not Found.")
        return redirect('/school/target_setting_list/')
    class_rec = []
    class_list=[]
    obj = exam_scheme_acd_mapping_details.objects.filter(year=current_academic_year)
    for rec in obj:
        if rec.exam_name_mapping_ids.filter(is_archived=False):
            if rec.class_obj not in class_rec:
                class_rec.append(rec.class_obj)
    for rec in set(class_rec):
        class_list.append(model_to_dict(rec))
    return JsonResponse(class_list,safe=False)


def EditTargetSetting(request, rec_id):
    target_setting_rec = sz_target_setting_details.objects.filter(id=rec_id)
    classes=target_setting_rec[0].target_rel_id.all()
    class_list=[]
    exam_list=[]
    year_subject=[]
    frequency_list=[]
    for cls in classes:
        if not year_subject:
            dict={'subject_details':cls.subject_name,
                  'year_details':cls.year_name}
            year_subject.append(dict)
            #year_subject.append(cls.subject_name)
        class_list.append(cls.class_name)
        exams=cls.target_setting_ids.all()
        for exam in exams:
            if exam.frequency_name not in frequency_list:
                frequency_list.append(exam.frequency_name)
                exam_list.append(exam)
            else:
                pass
    class_rec = []
    obj = exam_scheme_acd_mapping_details.objects.filter(year=cls.year_name)
    for rec in obj:
        if rec.exam_name_mapping_ids.filter(is_archived=False):
            if rec.class_obj not in class_rec:
                class_rec.append(rec.class_obj)
    return render(request, "edit_target_setting.html", {'target_setting_recs':target_setting_rec,'class_list':class_list, 'flag': make_incrementor(0),
                                                        'class_rec':class_rec,'year_subject':year_subject,'exam_list':exam_list})

@user_login_required
def ViewMappedSubjectList(request):
     finalDict = []
     subject_id_list = []
     year_name = request.POST.get('academic_year', None)
     class_name = request.POST.get('class_name', None)
     target_setting_recs = sz_target_setting_sub_details.objects.filter(year_name_id =year_name, class_name_id = class_name)
     for rec in target_setting_recs:
         if rec.subject_name.id not in subject_id_list:
             subject_id_list.append(rec.subject_name.id)
             subject_dict={'id':rec.subject_name.id, 'value': rec.subject_name.subject_name}
             finalDict.append(subject_dict)
     return JsonResponse(finalDict,safe=False)

@user_login_required
def SetStudentTarget1(request):
    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    selected_year_id = request.POST.get('year_name')  # It will be used in HTML page as a value of selected Class
    current_academic_year = academic_year.objects.get(id=selected_year_id)
    selected_year_name = current_academic_year.year_name

    selected_class_id = request.POST.get('class_name')  # It will be used in HTML page as a value of selected Class
    selected_class_rec = class_details.objects.get(id=selected_class_id)
    selected_class_name = selected_class_rec.class_name

    selected_section_id = request.POST.get('section_name')  # It will be used in HTML page as a value of selected Section
    selected_section_rec = sections.objects.get(id=selected_section_id)
    selected_section_name = selected_section_rec.section_name

    selected_subject_id = request.POST.get('subject_name')  # It will be used in HTML page as a value of selected Section
    selected_subject_rec = subjects.objects.get(id=selected_subject_id)
    selected_subject_name=selected_subject_rec.subject_name

    selected_target_id = request.POST.get('target_name')  # It will be used in HTML page as a value of selected Section
    selected_target_rec = sz_target_setting_details.objects.get(id=selected_target_id)
    selected_target_name = selected_target_rec.target_name
    try:
        year_class_section_obj = academic_class_section_mapping.objects.get(year_name_id=selected_year_id,
                                                                        section_name_id=selected_section_id,
                                                                        class_name_id=selected_class_id)
    except Exception as e :
        messages.warning(request, "Class year section mapping not found")
        return redirect('/school/set_student_target/')
    if sz_set_stud_target_details.objects.filter(academic_mapping=year_class_section_obj, target_name_id=selected_target_id,
                                                 subject_name_id=selected_subject_id).exists():
        messages.warning(request, "Student target is already exists for given class and subject")
        return redirect('/school/set_student_target/')
    else:
        target_setting_rec = selected_target_rec.target_rel_id.filter(year_name_id=selected_year_id,
                                                                      class_name_id=selected_class_id,
                                                                      subject_name_id=selected_subject_id)
        dict = {}
        freq = {}
        main_dict = []
        cnt = 0
        for target_rec in target_setting_rec:
            for target in target_rec.target_setting_ids.all():
                if cnt == 0:
                    dict['exam'] = {'exam_id': target.exam_mapping_name.id, 'exam_name': target.exam_mapping_name.exam_name}
                    freq_dict = {'freq_id': target.frequency_name.id, 'freq_name': target.frequency_name.frequency_name}
                    freq['sub_freq'] = []
                    freq['sub_freq'].append(freq_dict)
                    dict['frequency'] = freq
                else:
                    if target.exam_mapping_name.exam_name in dict['exam']['exam_name']:
                        freq_dict = {}
                        freq_dict = {'freq_id': target.frequency_name.id, 'freq_name': target.frequency_name.frequency_name}
                        freq['sub_freq'].append(freq_dict)
                        dict['frequency'].update(freq)
                    else:
                        main_dict.append(dict)
                        dict = {}
                        freq = {}
                        freq['sub_freq'] = []
                        dict['exam'] = (
                            {'exam_id': target.exam_mapping_name.id, 'exam_name': target.exam_mapping_name.exam_name})
                        freq_dict = {'freq_id': target.frequency_name.id, 'freq_name': target.frequency_name.frequency_name}
                        freq['sub_freq'].append(freq_dict)
                        dict['frequency'] = (freq)
                cnt = cnt + 1
            main_dict.append(dict)

        grade_dict = []
        for target_rec in target_setting_rec:
            for exam in target_rec.target_setting_ids.all():
                grade_list = []
                examdict = {}
                exam_scheme = exam_scheme_acd_mapping_details.objects.filter(year_id=selected_year_id,
                                                                             class_obj_id=selected_class_id,
                                                                             subject_id=selected_subject_id)
                for obj in exam_scheme:
                    exam_grade = obj.exam_name_mapping_ids.filter(is_archived=False).filter(exam_name_id=exam.exam_mapping_name.id)
                    if exam_grade:
                        grades = grade_sub_details.objects.filter(grade=exam_grade[0].grade_name)
                        for grd in grades:
                            if grd.grades not in grade_list:
                                grade_list.append(grd)
                examdict['exam'] = exam
                examdict['grades'] = grade_list
                grade_dict.append(examdict)

        filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True)
        # Passing values to the HTML page through form_vals
        form_vals = {
            'student_list': filtered_students,
            'Selected_Section_Id': selected_section_id,
            'selected_section_name': selected_section_name,
            'selected_section': selected_section_rec,
            'section_list': all_section_recs,
            'Selected_Class_Id': selected_class_id,
            'selected_class_name': selected_class_name,

            'current_academic_year': current_academic_year,
            'class_list': all_class_recs,
            'Selected_Year_Id': selected_year_id,
            'selected_year_name': selected_year_name,
            'year_list': all_academicyr_recs,
            'selected_subject_id': selected_subject_id,
            'selected_subject_name': selected_subject_name,
            'selected_target_id': selected_target_id,
            'selected_target_name': selected_target_name,
            'main_dict': main_dict,
            'grade_dict': grade_dict,

        }
        return render(request, "set_student_target.html", form_vals)

@user_login_required
def SaveStudentTarget(request):
    if request.method == 'POST':
        year_id = request.POST.get('acd_year')
        class_id = request.POST.get('class')
        section_id = request.POST.get('section')
        subject_id = request.POST.get('sub')
        target_id=request.POST.get('target')
        try:
            academic_rec = academic_class_section_mapping.objects.get(year_name_id=year_id, section_name_id=section_id,
                                                              class_name_id=class_id)
        except:
            messages.warning(request, "Current Academic Year Mapping Not Found.")
            return redirect('/school/set_student_target/')
        data = json.loads(request.POST.get('data'))
        try:
            if sz_set_stud_target_details.objects.filter(academic_mapping=academic_rec,target_name_id=target_id,
                                                                              subject_name_id=subject_id).exists():
                messages.warning(request, "Student target is already exists for given class and subject")
                return redirect('/school/set_student_target/')
            else:
                if data:
                    student_target_main_obj = sz_set_stud_target_details.objects.create(academic_mapping=academic_rec,
                                                                target_name_id=target_id,subject_name_id=subject_id)
                    rec_cnt = 0
                    for rec in data:
                        if (rec_cnt == 0):
                            pass
                        else:
                            student = rec['studentId']
                            student_obj = sz_set_stud_target_sub_details.objects.create(student_id=student)
                            cnt = int(rec['counter'])
                            for i in range(1,cnt+1):
                                exam = rec['exam_' + str(i)]
                                grades = rec['grades_' + str(i)]
                                freq = rec['freq_' + str(i)]
                                sub_sub_details = sz_set_stud_target_sub_sub_details.objects.create(frequency_name_id=freq,
                                                                                                    exam_name_id=exam,
                                                                                                    grade_name_id=grades)
                                student_obj.exam_grade.add(sub_sub_details)
                            student_target_main_obj.student_rel_id.add(student_obj)
                        rec_cnt += 1
                    messages.success(request, "One Record Added")
                    return redirect('/school/set_student_target_list/')
        except Exception as e:
            messages.warning(request, "Some Error Occurred. Please Try again.")
            return redirect('/school/set_student_target/')

@user_login_required
def EditSaveStudentTarget(request):
    data = json.loads(request.POST.get('data'))
    if data:
        for rec in data:
                student = rec['studentId']
                student_obj = sz_set_stud_target_sub_details.objects.filter(student_id=student)
                for student in student_obj:
                    cnt = int(rec['counter'])
                    for i in range(1, cnt + 1):
                        exam = rec['exam_' + str(i)]
                        grades = rec['grades_' + str(i)]
                        freq = rec['freq_' + str(i)]
                        sub_sub_details=student.exam_grade.filter(frequency_name_id=freq,
                                                                                            exam_name_id=exam).update(grade_name_id=grades)

        messages.success(request, "One record updated Successfully")
        return redirect('/school/set_student_target_list/')

@user_login_required
def EditStudentTarget(request,rec_id):
    student_target_rec = sz_set_stud_target_details.objects.filter(id=rec_id)

    dict={}

    target_setting_rec = sz_target_setting_details.objects.get(id=student_target_rec[0].target_name.id)
    target_rec1=target_setting_rec.target_rel_id.filter(year_name=student_target_rec[0].academic_mapping.year_name,class_name=student_target_rec[0].academic_mapping.class_name,subject_name=student_target_rec[0].subject_name)
    dict = {}
    freq = {}
    main_dict = []
    cnt = 0
    for target_rec in target_rec1:
        for target in target_rec.target_setting_ids.all():
            if cnt == 0:
                dict['exam'] = {'exam_id': target.exam_mapping_name.id, 'exam_name': target.exam_mapping_name.exam_name}
                freq_dict = {'freq_id': target.frequency_name.id, 'freq_name': target.frequency_name.frequency_name}
                freq['sub_freq'] = []
                freq['sub_freq'].append(freq_dict)
                dict['frequency'] = freq
            else:
                if target.exam_mapping_name.exam_name in dict['exam']['exam_name']:
                    freq_dict = {}
                    freq_dict = {'freq_id': target.frequency_name.id, 'freq_name': target.frequency_name.frequency_name}
                    freq['sub_freq'].append(freq_dict)
                    dict['frequency'].update(freq)
                else:
                    main_dict.append(dict)
                    dict = {}
                    freq = {}
                    freq['sub_freq'] = []
                    dict['exam'] = (
                    {'exam_id': target.exam_mapping_name.id, 'exam_name': target.exam_mapping_name.exam_name})
                    freq_dict = {'freq_id': target.frequency_name.id, 'freq_name': target.frequency_name.frequency_name}
                    freq['sub_freq'].append(freq_dict)
                    dict['frequency'] = (freq)
            cnt = cnt + 1
        main_dict.append(dict)
    grade_details = []
    grade_dict = []
    for target_rec in target_rec1:
        for exam in target_rec.target_setting_ids.all():
            grade_list = []
            examdict = {}
            exam_scheme = exam_scheme_acd_mapping_details.objects.filter(year=student_target_rec[0].academic_mapping.year_name,
                                                                         class_obj_id=student_target_rec[0].academic_mapping.class_name,
                                                                         subject_id=student_target_rec[0].subject_name)
            for obj in exam_scheme:
                exam_grade = obj.exam_name_mapping_ids.filter(is_archived=False).filter(exam_name_id=exam.exam_mapping_name.id)
                if exam_grade:
                    grades = grade_sub_details.objects.filter(grade=exam_grade[0].grade_name)
                    for grd in grades:
                        if grd.grades not in grade_list:
                            grade_list.append(grd)
            examdict['exam'] = exam
            examdict['grades'] = grade_list
            grade_dict.append(examdict)
    for student in student_target_rec:
        student_filter=student.student_rel_id.all()
        for rec in student_filter:
            obj=rec.exam_grade.all()


    return render(request, "edit_student_target.html",
                  {
                      'student_target_rec': student_target_rec,
                      'student_filter':student_filter,
                      'main_dict':main_dict,
                      'grade_dict':grade_dict,
                      'flag': make_incrementor(0)
                  })


# @user_login_required
# def UpdateJsonList(request, rec_id):
#
#     academic_frequency_rec = scheme_academic_frequency_mapping.objects.get(id = rec_id)
#
#
#     selected_scheme = scheme.objects.get(id=academic_frequency_rec.scheme_mapping.scheme_name.id)
#     # scheme_id = seheme_mapping_rec.scheme_name.id
#     subtopics_recs = []
#     subtopics_ids = []
#     subtopic_recs =[]
#     list = []
#
#
    # student_list = []
    # for student_rec in academic_frequency_rec.student_mark_ids.all():
    #     student_dict = {
    #         'id': student_rec.student.id,
    #         'first_name': student_rec.student.first_name,
    #         'year_name': student_rec.student.academic_class_section.year_name.year_name,
    #         'class_name': student_rec.student.academic_class_section.class_name.class_name,
    #         'section_name': student_rec.student.academic_class_section.section_name.section_name,
    #
    #     }
    #     student_list.append(student_dict)
    # return JsonResponse(student_list, safe=False)
#
#     grade_colors = grade_sub_details.objects.filter(grade=academic_frequency_rec.scheme_mapping.grade_name)
#
#     for data in academic_frequency_rec.student_mark_ids.all():
#         for rec in data.subtopic_ids.all():
#             if rec.subtopic.id not in list:
#                 list.append(rec.subtopic.id)
#                 subtopic_recs.append(rec.subtopic)
#
#     if academic_frequency_rec.is_submitted is True:
#         color_code = '#1ce034'
#     else:
#         color_code = '#ffa012'
#
#     form_vals = {
#         # 'student_list': filtered_students,
#         'Selected_Section_Id': academic_frequency_rec.academic_mapping.section_name.id,
#         'selected_section_name': academic_frequency_rec.academic_mapping.section_name,
#         'selected_section': academic_frequency_rec.academic_mapping.section_name,
#         # 'section_list': all_section_recs,
#
#         'Selected_Class_Id': academic_frequency_rec.academic_mapping.class_name.id,
#         'selected_class_name': academic_frequency_rec.academic_mapping.class_name.class_name,
#         # 'selected_class': selected_class_rec,
#         # 'class_list': all_class_recs,
#
#         'Selected_Year_Id': academic_frequency_rec.academic_mapping.year_name.id,
#         'selected_year_name': academic_frequency_rec.academic_mapping.year_name.year_name,
#         'selected_year': academic_frequency_rec.academic_mapping.year_name,
#         # 'year_list': all_academicyr_recs,
#
#         'frequency_recs': academic_frequency_rec.frequency,
#         'frequency_data':  academic_frequency_rec.frequency.id,
#         'scheme_recs': academic_frequency_rec.scheme_mapping.scheme_name,
#         'subtopics_recs': subtopics_recs,
#         # 'selected_scheme': selected_scheme,
#
#         'academic_frequency_rec': academic_frequency_rec,
#         'subtopic_data': subtopic_recs,
#         'color_code':color_code,
#         'grade_colors':grade_colors,
#         'subject_recs': academic_frequency_rec.scheme_mapping.subject,
#     }
#
#     return render(request, "edit_scheme_Entered_mark.html",form_vals)
@user_login_required
def ViewMappedReportedExamName(request):
    finalDict = []
    class_name = json.loads(request.POST.get('class_name', None))
    subject_name = json.loads(request.POST.get('subject_name', None))
    academic_year = request.POST.get('academic_year', None)
    #reported_recs = reported_exam_class_sub_mapping.objects.filter(year_id = academic_year,class_name_id=class_name[0],subject_id=subject_name)
    reported_recs = exam_scheme_acd_mapping_details.objects.filter(Q(year_id=academic_year, subject_id=subject_name) & Q(class_obj_id__in=class_name))

    for rec in reported_recs:
        records=rec.exam_name_mapping_ids.filter(is_archived=False)
        for r in records:
            rec_dict = {'id': r.exam_name.id, 'exam_name_rec': r.exam_name.exam_name}
            if rec_dict not in finalDict:
                finalDict.append(rec_dict)
    return JsonResponse(finalDict,safe=False)

@user_login_required
def ViewMappedReportedFrequency(request):
    finalDict = []
    exam_id = request.GET.get('exam_id', None)
    exam_report_rec = exam_details_frequency.objects.filter(exam_details_name__id=exam_id)
    for rec in exam_report_rec:
        rec_dict={'id':rec.frequency_details_name.id, 'frequency_name': rec.frequency_details_name.frequency_name}
        finalDict.append(rec_dict)
    return JsonResponse(finalDict,safe=False)

@user_login_required
def GetReportScoreName(request):
    id_subject_name = request.POST.get('id_subject_name', None)
    class_name = json.loads( request.POST.get('class_name'))
    academic_value = json.loads( request.POST.get('academic_value'))
    report_score_list = []
    report_score_ids= []
    reported_score_recs = reported_exam_class_sub_mapping.objects.filter(Q(year_id=academic_value,subject_id=id_subject_name) & Q(class_name_id__in =class_name))
    for report_rec in reported_score_recs:
        if report_rec.reported_exam_mapp.id not in report_score_ids:
            report_score_ids.append(report_rec.reported_exam_mapp.id)
            report_dict = {'id': report_rec.reported_exam_mapp.id, 'report_score_name': report_rec.reported_exam_mapp.report_name}
            report_score_list.append(report_dict)
    return JsonResponse(report_score_list,safe=False)

@user_login_required
def SaveTargetSetting(request):
    try:
        if request.method == 'POST':
            code = request.POST.get('code')
            setting_name = request.POST.get('setting_name')
            year = request.POST.get('year_name')
            class_name_ids = request.POST.getlist('class_name')
            subject_name = request.POST.get('subject_name')
            data_list = json.loads(request.POST.get('json_data'))
            flag=''
            target_rec = sz_target_setting_details.objects.filter(code=code, target_name=setting_name)
            if target_rec:
                messages.warning(request, "Target code & name already exists!")
                return redirect('/school/target_setting_creation/')
            else:
                target_rec = sz_target_setting_details.objects.create(code=code, target_name=setting_name)
                for class_rec in class_name_ids:
                    target_sub_details = sz_target_setting_sub_details.objects.create(year_name_id=year,class_name_id=class_rec,subject_name_id=subject_name)
                    target_rec.target_rel_id.add(target_sub_details)
                    for rec in data_list:
                        exam_scheme_rec = exam_scheme_acd_mapping_details.objects.filter(year_id=year,class_obj_id=class_rec,subject_id=subject_name)
                        get_grading_rec =  exam_scheme_rec[0].exam_name_mapping_ids.filter(is_archived=False).filter(exam_name=rec['exam_name'])
                        if get_grading_rec:
                            rec_id = get_grading_rec[0].exam_name.id
                            sub_sub_obj = sz_target_setting_sub_sub_details.objects.create(
                                    frequency_name_id=rec['frequency_name'], exam_mapping_name_id=rec_id)
                            target_sub_details.target_setting_ids.add(sub_sub_obj)
                            flag = 'success'

            if flag:
                messages.success(request, "Record added successfully")
            return redirect('/school/target_setting_list/')
    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again.")
        return redirect('/school/target_setting_creation/')

@user_login_required
@user_permission_required('assessment.can_view_view_set_target_categories', '/school/home/')
def SetStudentTarget(request):
    subject_list=[]
    class_list=[]
    target_name_list=[]
    target_setting = sz_target_setting_sub_details.objects.all()
    for target in target_setting:
        if target.class_name not in class_list:
            class_list.append(target.class_name)
        if target.subject_name not in subject_list:
            subject_list.append(target.subject_name)
    target_obj=sz_target_setting_details.objects.all()
    for t in target_obj:
        if t not in target_name_list:
            target_name_list.append(t)


    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    scheme_recs = scheme.objects.all()
    frequency_recs = frequency_details.objects.all()

    current_academic_year = None
    selected_year_id = None

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
    except:
        messages.error(request, "Please add academic year first.")
        return render(request, "set_student_target.html", {'class_list': class_list, 'subject_list': subject_list, "year_list": all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,'target_name_list':target_name_list})
    return render(request, "set_student_target.html", {'class_list': class_list, 'subject_list': subject_list, "year_list": all_academicyr_recs,'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,  'scheme_recs': scheme_recs,'frequency_recs':frequency_recs,'target_name_list':target_name_list})

@user_login_required
def SetStudentTargetList(request):
    #sz_set_stud_target_sub_sub_details.objects.all().delete()
    #sz_set_stud_target_sub_details.objects.all().delete()
    #sz_set_stud_target_details.objects.all().delete()

    student_target_recs = sz_set_stud_target_details.objects.all()
    return render(request, "set_student_target_list.html", {'student_target_recs': student_target_recs})

@user_login_required
def GetSubjectName(request):
    subject_list = []
    subject_id_list=[]
    academic_year = request.GET.get('academic_year', None)
    class_name = request.GET.get('class_name', None)

    subject_recs = sz_target_setting_sub_details.objects.filter(year_name_id = academic_year,class_name=class_name)
    for rec in subject_recs:
        if rec.subject_name.id not in subject_id_list:
            subject_id_list.append(rec.subject_name.id)
            rect_dict = {'id': rec.subject_name.id, 'subject_name': rec.subject_name.subject_name}
            subject_list.append(rect_dict)
    return JsonResponse(subject_list,safe=False)

def GetTargetName(request):
    target_list=[]
    academic_year = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    subject_name = request.POST.get('subject_name', None)
    target_sub_recs = sz_target_setting_sub_details.objects.filter(year_name_id=academic_year, class_name=class_name,subject_name=subject_name)
    for rec in target_sub_recs:
        rect_dict = rec.target_setting_sub_details_relation.all()
        for sub_rec in rect_dict:
            rect_dict = {'id': sub_rec.id, 'target_name': sub_rec.target_name}
            if rect_dict not in target_list:
                target_list.append(rect_dict)
    return JsonResponse(target_list, safe=False)

@user_login_required
def DefineScheme(request):
    frequency_mapping_ids = request.POST.getlist('frequency_mapping', None)
    frequency_recs = frequency_details.objects.all()
    subject_recs = subjects.objects.all()

    frequency_id_list = []
    frequency_obj_list = []
    mandatory_subject_list = []
    subject_list = []
    optional_subject_list = []

    if frequency_mapping_ids:
        for freq_id in frequency_mapping_ids:
            if freq_id != "":
                frequency_id_list.append(int(freq_id))

        frequency_obj_list = frequency_details.objects.filter(id__in=frequency_id_list)

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        academic_class_section_recs = academic_class_section_mapping.objects.filter(year_name_id=selected_year_id)
        mandatory_subject_recs = mandatory_subject_mapping.objects.filter(
            academic_class_section_mapping__in=academic_class_section_recs)

        for mandatory_subject_rec in mandatory_subject_recs:
            mandatory_subject_list.append(mandatory_subject_rec.subject_id)
        mandatory_subject_list_set = set(mandatory_subject_list)
        subject_mandatory_recs = subjects.objects.filter(id__in=mandatory_subject_list_set)

        for subject_mandatory_rec in subject_mandatory_recs:
            subject_list.append(subject_mandatory_rec)

        student_recs = student_details.objects.filter(academic_class_section__in=academic_class_section_recs)
        for student_rec in student_recs:
            for subject in student_rec.subject_ids.all():
                optional_subject_list.append(subject)

        optional_subject_list_set = set(optional_subject_list)
        for optional_subject_rec in optional_subject_list_set:
            subject_list.append(optional_subject_rec)

    except:
        messages.success(request, "Current Academic Year Not Not Found. Please Create One And Try Again.")
        return redirect('/school/define_scheme/')

    return render(request, "define_scheme.html",{'subject_recs':subject_list,'frequency_recs':frequency_recs,'frequency_id_list':frequency_id_list,'frequency_obj_list':frequency_obj_list})


def ViewSelectedFreq(request):
    frequency_recs = []
    selected_list = json.loads(request.POST.get('selected_list'))
    freq_list = frequency_details.objects.all()
    for rec in freq_list:
        freq_dict = {'id': rec.id, 'freq': rec.frequency_name, 'selected_list': selected_list}
        frequency_recs.append(freq_dict)
    return JsonResponse(frequency_recs, safe=False)

def ViewSelectedTargetFreq(request):
    section_recs = []
    selected_list = json.loads(request.POST.get('selected_list'))
    sec_list = []
    for sec_rec in selected_list:
        rec = sections.objects.get(id=sec_rec)
        sec_list.append(rec)
    for rec in sec_list:
        sec_dict={'id':rec.id,'section_name':rec.section_name,'selected_list': selected_list}
        section_recs.append(sec_dict)
    return JsonResponse(section_recs, safe=False)


def CheckSubjectSubstrand(request):
    subject_name = request.GET.get('subject_name')
    Flag = False
    subject_obj = subjects.objects.get(id=subject_name)
    if subject_obj.strands_ids.all().count() > 0:
        for substrand in subject_obj.strands_ids.all():
            if substrand.substrands_ids.all().count() > 0:
                Flag = True
    return JsonResponse(Flag, safe=False)

@user_login_required
def DefineSchemeFilter(request):
    if request.POST:
        request.session['data'] = request.POST
        selected_scheme_code = request.POST.get('scheme_code')
        selected_scheme_name = request.POST.get('scheme_name')
        frequency_mapping_ids = request.POST.getlist('frequency_mapping',None)
        request.session['frequency_mapping_ids']=frequency_mapping_ids
        selected_subject_name = request.POST.get('subject_name')
    else:
        selected_scheme_code = request.session['data'].get('scheme_code')
        selected_scheme_name = request.session['data'].get('scheme_name')
        frequency_mapping_ids = request.session['frequency_mapping_ids']
        selected_subject_name = request.session['data'].get('subject_name')
    subject_recs = subjects.objects.all()
    frequency_recs = frequency_details.objects.all()
    frequency_id_list =[]
    # frequency_obj_list =[]
    if frequency_mapping_ids:
        for freq_id in frequency_mapping_ids:
            if freq_id!="":
                frequency_id_list.append(int(freq_id))

        frequency_obj_list = frequency_details.objects.filter(id__in = frequency_id_list)

    if(selected_subject_name==''):
        return render(request, "define_scheme_without_subject.html",{'selected_scheme_code':selected_scheme_code,'selected_scheme_name':selected_scheme_name,'subject_recs':subject_recs,'frequency_recs':frequency_recs,'frequency_id_list':frequency_id_list,'frequency_obj_list':frequency_obj_list})

    subject_rec = subjects.objects.get(id=selected_subject_name)
    subject_list = subject_rec.strands_ids.all()
    return render(request, "define_scheme_with_subject.html",{'subject_list':subject_list,'selected_scheme_code':selected_scheme_code,'selected_scheme_name':selected_scheme_name,'selected_subject_name':subject_rec,'subject_recs':subject_recs,'frequency_recs':frequency_recs,'frequency_id_list':frequency_id_list,'frequency_obj_list':frequency_obj_list})

@user_login_required
@user_permission_required('assessment.can_view_view_skill_mapping_list', '/school/home/')
def ViewSkillSchemeMapping(request):
    year_class_section_object = academic_class_section_mapping.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    grade_recs = grade_details.objects.filter(is_archive = False)
    scheme_recs = scheme.objects.all()
    if year_class_section_object:
        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id
            return render(request, 'view_skill_scheme_mapping.html',
                          { "year_list": all_academicyr_recs,'scheme_recs':scheme_recs,
                           'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,'grade_recs':grade_recs})
        except academic_year.DoesNotExist:
            return render(request, 'skill_tracker_sub_tile.html')
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'skill_tracker_sub_tile.html')


def ViewGetSkillScheme(request):
    skill_scheme_list = []
    report_score_ids = []
    subject_name = json.loads(request.POST.get('subject_name', None))
    for subject_rec in subject_name:
        skill_scheme_rec = scheme.objects.filter(subject_scheme_name_id=subject_rec)
        if skill_scheme_rec:
            for rec in skill_scheme_rec:
                skill_dict = {'id': rec.id, 'scheme': rec.scheme_name}
                skill_scheme_list.append(skill_dict)

            skill_scheme_rec = scheme.objects.filter(subject_scheme_name=None)
            for rec in skill_scheme_rec:
                if rec.id not in report_score_ids:
                    report_score_ids.append(rec.id)
                    skill_dict = {'id': rec.id, 'scheme': rec.scheme_name}
                    skill_scheme_list.append(skill_dict)
        else:
            skill_scheme_rec = scheme.objects.filter(subject_scheme_name=None)
            for rec in skill_scheme_rec:
                if rec.id not in report_score_ids:
                    report_score_ids.append(rec.id)
                    skill_dict = {'id': rec.id, 'scheme': rec.scheme_name}
                    skill_scheme_list.append(skill_dict)
    return JsonResponse(skill_scheme_list, safe=False)


def SaveSkillSchemeMapping(request):
    if request.method == 'POST':
        year = request.POST.get('year_name')
        year_class_section_object = academic_class_section_mapping.objects.all()
        all_academicyr_recs = academic_year.objects.all()
        class_name_ids = request.POST.getlist('class_name')
        subject_name_ids = request.POST.getlist('subject_name')
        data_list = json.loads(request.POST.get('json_data'))
        mapping_flag = False
        over_flag = False
        override_list=[]
        subject_not_mapped_flag = False
        subject_not_mapped_list = []
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        for class_rec in class_name_ids:
            for subject_rec in subject_name_ids:
                if not mandatory_subject_mapping.objects.filter(academic_class_section_mapping__year_name_id=year,academic_class_section_mapping__class_name_id=class_rec,subject_id=subject_rec):

                    subject_dict = {
                        'Class Name': class_details.objects.get(id=class_rec).class_name,
                        'Subject Name': subjects.objects.get(id=subject_rec).subject_name
                    }
                    subject_not_mapped_list.append(subject_dict)

                    subject_not_mapped_flag = True
                    pass
                else:
                    for rec in data_list:
                        # scheme_mapping_details.objects.filter(~Q(subject_id=subject_rec),scheme_name_id=rec['scheme'],
                        #                                       year_id=year, class_obj_id=class_rec)


                        exam_scheme_rec = scheme_mapping_details.objects.filter(year_id=year, class_obj_id=class_rec,
                                                                                subject_id=subject_rec,
                                                                                scheme_name_id=rec['scheme'])
                        if exam_scheme_rec:
                            if exam_scheme_rec.count() == 1:
                                if exam_scheme_rec[0].is_archived == True:
                                    # scheme_mapping_details.objects.create(year_id=year,class_obj_id=class_rec,
                                    #                                       subject_id=subject_rec,
                                    #                                       scheme_name_id=rec['scheme'],
                                    #                                       grade_name_id=rec['grade'])

                                    continue
                                else:
                                    grade_scheme = grade_details.objects.filter(id=rec['grade']).values_list("grade_name",flat=1)
                                    scheme_name = scheme.objects.filter(id=rec['scheme']).values_list("scheme_name", flat=1)
                                    over_dict = {
                                        'Year Name': exam_scheme_rec[0].year.year_name,
                                        'Class Name': exam_scheme_rec[0].class_obj.class_name,
                                        'Subject Name': exam_scheme_rec[0].subject.subject_name,
                                        'Scheme Name': exam_scheme_rec[0].scheme_name.scheme_name,
                                        # 'Scheme Name': scheme_name[0],
                                        # 'Grading Scheme': grade_scheme[0],
                                        'Grading Scheme': exam_scheme_rec[0].grade_name.grade_name,
                                    }

                                    override_list.append(over_dict)

                                    mapping_flag = True
                                    continue
                            else:

                                scheme_name = scheme.objects.filter(id=rec['scheme']).values_list("scheme_name", flat=1)

                                grade_scheme = grade_details.objects.filter(id=rec['grade']).values_list("grade_name",
                                                                                                         flat=1)
                                over_dict = {
                                    'Year Name': exam_scheme_rec[0].year.year_name,
                                    'Class Name': exam_scheme_rec[0].class_obj.class_name,
                                    'Subject Name': exam_scheme_rec[0].subject.subject_name,
                                    'Scheme Name': exam_scheme_rec[0].scheme_name.scheme_name,
                                    # 'Scheme Name': scheme_name[0],
                                    'Grading Scheme':exam_scheme_rec[0].grade_name.grade_name,

                                    # 'Grading Scheme': grade_scheme[0],
                                }

                                override_list.append(over_dict)
                                mapping_flag = True
                                continue
                        elif scheme_mapping_details.objects.filter(~Q(scheme_name_id=rec['scheme']), year_id=year,
                                                                   class_obj_id=class_rec, subject_id=subject_rec).exists():

                            scheme_rec = scheme_mapping_details.objects.filter(~Q(scheme_name_id=rec['scheme']), year_id=year,
                                                                  class_obj_id=class_rec, subject_id=subject_rec)
                            scheme_name = scheme.objects.filter(id=rec['scheme']).values_list("scheme_name",flat=1)
                            grade_scheme = grade_details.objects.filter(id=rec['grade']).values_list("grade_name",flat=1)

                            over_dict = {
                                'Year Name': scheme_rec[0].year.year_name,
                                'Class Name': scheme_rec[0].class_obj.class_name,
                                'Subject Name': scheme_rec[0].subject.subject_name,
                                'Scheme Name': scheme_rec[0].scheme_name.scheme_name,
                                'Grading Scheme': scheme_rec[0].grade_name.grade_name,
                            }

                            override_list.append(over_dict)
                            mapping_flag = True
                            pass
                        else:
                            mapped_scheme_subject = scheme.objects.filter(id=rec['scheme'], subject_scheme_name=subject_rec)
                            if mapped_scheme_subject:
                                rec_data = scheme_mapping_details.objects.filter(~Q(scheme_name_id=rec['scheme']),
                                                                                 year_id=year, class_obj_id=class_rec,
                                                                                 subject_id=subject_rec)
                                if rec_data:
                                    pass
                                else:
                                    exam_scheme_rec = scheme_mapping_details.objects.create(year_id=year,
                                                                                            class_obj_id=class_rec,
                                                                                            subject_id=subject_rec,
                                                                                            scheme_name_id=rec['scheme'],
                                                                                            grade_name_id=rec['grade'])
                            else:
                                rec_data_data = scheme_mapping_details.objects.filter(scheme_name_id=rec['scheme'],
                                                                                      year_id=year, class_obj_id=class_rec)
                                if rec_data_data:
                                    pass
                                else:
                                    exam_scheme_rec = scheme_mapping_details.objects.create(year_id=year,
                                                                                            class_obj_id=class_rec,
                                                                                        subject_id=subject_rec,
                                                                                        scheme_name_id=rec['scheme'],
                                                                                        grade_name_id=rec['grade'])
        if mapping_flag:
            for list in (override_list):
                messages.warning(request, "Mapping already Exists for given"+" "+"Academic Year:"+str(list['Year Name'])+" "+"Class Name :"+str(list['Class Name'])+" "+"Subject Name :"+str(list['Subject Name'])+" "+"Scheme Name :"+str(list['Scheme Name'])+" "+"Grading Scheme:"+str(list['Grading Scheme']))

        if subject_not_mapped_flag:
            for list in (subject_not_mapped_list):
                messages.warning(request, str(list['Class Name'] )+" " +"is not mapped with subject"+" "+str(list['Subject Name']))
                # messages.warning(request, "Mapping Not Exists for given" + " " + "Academic Year:" + str(list['Year Name']) + " " + "Class Name :" + str(list['Class Name']))

        return redirect('/school/view_scheme_mapping_filter/')
            # return render(request, 'view_skill_scheme_mapping.html', {'mapping_flag': mapping_flag, 'override_list':override_list,'Selected_Year_Id':selected_year_id,'selected_year_name': current_academic_year})


    messages.success(request, "One record added successfully")
    return redirect('/school/view_scheme_mapping_filter/')






#####-------- Scheme Mapping Save backup 04-May-2018 ---------------######################

# def SaveSkillSchemeMapping(request):
#     if request.method == 'POST':
#         year = request.POST.get('year_name')
#         class_name_ids = request.POST.getlist('class_name')
#         subject_name_ids = request.POST.getlist('subject_name')
#         data_list = json.loads(request.POST.get('json_data'))
#         mapping_flag=False
#         over_flag = False
#
#         for class_rec in class_name_ids:
#             for subject_rec in subject_name_ids:
#                 for rec in data_list:
#                     exam_scheme_rec = scheme_mapping_details.objects.filter(year_id=year, class_obj_id=class_rec,subject_id=subject_rec,scheme_name_id=rec['scheme'])
#                     if exam_scheme_rec:
#                         if exam_scheme_rec.count() == 1:
#                             if exam_scheme_rec[0].is_archived == True:
#                                 scheme_mapping_details.objects.create(year_id=year,
#                                                                       class_obj_id=class_rec,
#                                                                       subject_id=subject_rec,
#                                                                       scheme_name_id=rec['scheme'],
#                                                                       grade_name_id=rec['grade'])
#                                 continue
#                             else:
#                                 mapping_flag = True
#                                 continue
#                         else:
#                             mapping_flag=True
#                             continue
#                     elif scheme_mapping_details.objects.filter(~Q(scheme_name_id=rec['scheme']), year_id=year, class_obj_id=class_rec, subject_id=subject_rec).exists():
#                         pass
#                     else:
#                         mapped_scheme_subject = scheme.objects.filter(id=rec['scheme'], subject_scheme_name=subject_rec)
#                         if mapped_scheme_subject:
#                             rec_data = scheme_mapping_details.objects.filter(~Q(scheme_name_id=rec['scheme']), year_id=year, class_obj_id=class_rec,subject_id=subject_rec)
#                             if rec_data:
#                                 pass
#                             else:
#                                 exam_scheme_rec = scheme_mapping_details.objects.create(year_id=year,class_obj_id=class_rec,subject_id=subject_rec,scheme_name_id=rec['scheme'],grade_name_id=rec['grade'])
#                         else:
#                             rec_data_data = scheme_mapping_details.objects.filter(scheme_name_id=rec['scheme'],year_id=year, class_obj_id=class_rec)
#                             if rec_data_data:
#                                 pass
#                             else:
#                                 exam_scheme_rec = scheme_mapping_details.objects.create(year_id=year,class_obj_id=class_rec,subject_id=subject_rec,scheme_name_id=rec['scheme'],grade_name_id=rec['grade'])
#         if mapping_flag:
#             messages.warning(request, "Mapping already Exists for given class,subject and scheme name.")
#     return redirect('/school/view_skill_scheme_mapping/')





# def SaveSkillSchemeMapping(request):
#     if request.method == 'POST':
#         year = request.POST.get('year_name')
#         class_name_ids = request.POST.getlist('class_name')
#         subject_name_ids = request.POST.getlist('subject_name')
#         data_list = json.loads(request.POST.get('json_data'))
#         mapping_flag=False
#         for class_rec in class_name_ids:
#             for subject_rec in subject_name_ids:
#                 for rec in data_list:
#                     exam_scheme_rec = scheme_mapping_details.objects.filter(year_id=year, class_obj_id=class_rec,subject_id=subject_rec,scheme_name_id=rec['scheme'])
#                     if exam_scheme_rec:
#                         if exam_scheme_rec.count() == 1:
#                             if exam_scheme_rec[0].is_archived == True:
#                                 scheme_mapping_details.objects.create(year_id=year,
#                                                                       class_obj_id=class_rec,
#                                                                       subject_id=subject_rec,
#                                                                       scheme_name_id=rec['scheme'],
#                                                                       grade_name_id=rec['grade'])
#                                 continue
#                             else:
#                                 mapping_flag = True
#                                 continue
#                         else:
#                             # print 222222222
#                             mapping_flag=True
#                             continue
#                     elif scheme_mapping_details.objects.filter(~Q(scheme_name_id=rec['scheme']), year_id=year, class_obj_id=class_rec, subject_id=subject_rec).exists():
#                         pass
#                     else:
#                         mapped_scheme_subject = scheme.objects.filter(id=rec['scheme'], subject_scheme_name=subject_rec)
#                         if mapped_scheme_subject:
#                             rec_data = scheme_mapping_details.objects.filter(~Q(scheme_name_id=rec['scheme']), year_id=year, class_obj_id=class_rec,subject_id=subject_rec)
#                             if rec_data:
#                                 pass
#                             else:
#                                 exam_scheme_rec = scheme_mapping_details.objects.create(year_id=year,class_obj_id=class_rec,subject_id=subject_rec,scheme_name_id=rec['scheme'],grade_name_id=rec['grade'])
#                         else:
#                             rec_data_data = scheme_mapping_details.objects.filter(scheme_name_id=rec['scheme'],year_id=year, class_obj_id=class_rec)
#                             if rec_data_data:
#                                 pass
#                             else:
#                                 exam_scheme_rec = scheme_mapping_details.objects.create(year_id=year,class_obj_id=class_rec,subject_id=subject_rec,scheme_name_id=rec['scheme'],grade_name_id=rec['grade'])
#         if mapping_flag:
#             messages.warning(request, "Mapping already Exists for given class,subject and scheme name.")
#     return redirect('/school/view_skill_scheme_mapping/')



##----------- Backup Befor Archive Re-Mapping----------------------#####################333
# def SaveSkillSchemeMapping(request):
#     if request.method == 'POST':
#         year = request.POST.get('year_name')
#         class_name_ids = request.POST.getlist('class_name')
#         subject_name_ids = request.POST.getlist('subject_name')
#         data_list = json.loads(request.POST.get('json_data'))
#         mapping_flag=False
#         for class_rec in class_name_ids:
#             for subject_rec in subject_name_ids:
#                 for rec in data_list:
#                     exam_scheme_rec = scheme_mapping_details.objects.filter(year_id=year, class_obj_id=class_rec,subject_id=subject_rec,scheme_name_id=rec['scheme'])
#                     if exam_scheme_rec:
#                         mapping_flag=True
#                         continue
#                     elif scheme_mapping_details.objects.filter(~Q(scheme_name_id=rec['scheme']), year_id=year, class_obj_id=class_rec, subject_id=subject_rec).exists():
#                         pass
#                     else:
#                         mapped_scheme_subject = scheme.objects.filter(id=rec['scheme'], subject_scheme_name=subject_rec)
#                         if mapped_scheme_subject:
#                             rec_data = scheme_mapping_details.objects.filter(~Q(scheme_name_id=rec['scheme']), year_id=year, class_obj_id=class_rec,subject_id=subject_rec)
#                             if rec_data:
#                                 pass
#                             else:
#                                 exam_scheme_rec = scheme_mapping_details.objects.create(year_id=year,class_obj_id=class_rec,subject_id=subject_rec,scheme_name_id=rec['scheme'],grade_name_id=rec['grade'])
#                         else:
#                             rec_data_data = scheme_mapping_details.objects.filter(scheme_name_id=rec['scheme'],year_id=year, class_obj_id=class_rec)
#                             if rec_data_data:
#                                 pass
#                             else:
#                                 exam_scheme_rec = scheme_mapping_details.objects.create(year_id=year,class_obj_id=class_rec,subject_id=subject_rec,scheme_name_id=rec['scheme'],grade_name_id=rec['grade'])
#     if mapping_flag:
#         messages.warning(request, "Mapping already Exists for given class,subject and scheme name.")
#     return redirect('/school/view_skill_scheme_mapping/')


@user_login_required
def GetMappedFrequency(request):
    year_name = request.POST['academic_year']
    class_name = request.POST['class_name']
    subject_name = request.POST['subject_name']
    get_scheme_name_obj = scheme_mapping_details.objects.get(year=year_name,class_obj=class_name,subject=subject_name, is_archived = False)
    scheme_name_id = get_scheme_name_obj.scheme_name.id
    scheme_recs = scheme.objects.get(id=scheme_name_id)
    frequecy_recs = scheme_recs.scheme_frequency_ids.all()
    frequency_list = []
    for frequency_rec in frequecy_recs:
        frequency_dict={'id':frequency_rec.frequency_name.id, 'frequency_name': frequency_rec.frequency_name.frequency_name}
        frequency_list.append(frequency_dict)
    return JsonResponse(frequency_list,safe=False)

@user_login_required
def GetRubricMappedFrequency(request):
    year_name = request.POST['academic_year']
    class_name = request.POST['class_name']
    subject_name = request.POST['subject_name']


    get_scheme_name_obj = rubric_mapping_details.objects.get(year=year_name,class_obj=class_name,subject=subject_name,is_archived=False)
    rubric_mapping_id = get_scheme_name_obj.id
    rubric_mapp_id = rubric_mapping_details.objects.get(id=rubric_mapping_id)
    frequecy_recs = rubric_mapp_id.rubric_frequency_ids.all()
    frequency_list = []
    for frequency_rec in frequecy_recs:
        frequency_dict={'id':frequency_rec.frequency_name.id, 'frequency_name': frequency_rec.frequency_name.frequency_name}
        frequency_list.append(frequency_dict)
    return JsonResponse(frequency_list,safe=False)

@user_login_required
def ViewReportScoreDetails(request):
    reported_class_sub_mapping_recs = reported_exam_class_sub_mapping.objects.all()
    report_class_list = []
    class_ids = []
    subject_ids = []
    report_ids = []


    for rec in reported_class_sub_mapping_recs:
        if rec.class_name.id not in class_ids or rec.subject.id not in subject_ids or rec.reported_exam_mapp.id not in report_ids:
            section_list = current_class_mapped_sections(rec.year.id,rec.class_name.id,request.user)
            class_ids.append(rec.class_name.id)
            subject_ids.append(rec.subject.id)
            report_ids.append(rec.reported_exam_mapp.id)
            report_class_list.append({'rec':rec,'section_list':section_list})

    return render(request, 'view_report_score_details.html',{ 'count_init': make_incrementor(0),'reported_class_sub_mapping_recs': reported_class_sub_mapping_recs,'report_class_list':report_class_list})




# @user_login_required
# def GetReportScoreName(request):
#     id_subject_name = request.POST.get('id_subject_name', None)
#     class_name = json.loads( request.POST.get('class_name'))
#     academic_value = json.loads( request.POST.get('academic_value'))
#     report_score_list = []
#     report_score_ids= []
#     reported_score_recs = reported_exam_class_sub_mapping.objects.filter(Q(year_id=academic_value,subject_id=id_subject_name) & Q(class_name_id__in =class_name))
#     for report_rec in reported_score_recs:
#         if report_rec.reported_exam_mapp.id not in report_score_ids:
#             report_score_ids.append(report_rec.reported_exam_mapp.id)
#             report_dict = {'id': report_rec.reported_exam_mapp.id, 'report_score_name': report_rec.reported_exam_mapp.report_name}
#             report_score_list.append(report_dict)
#     return JsonResponse(report_score_list,safe=False)

@user_login_required
def ExamDeleteFrequency(request):
    try:
        exam_id=request.GET.get('exam_id')
        frequency_id=request.GET.get('frequency_id')
        flag = False
        exam_recs = exam_scheme_exam_name_mapping.objects.filter(exam_name=exam_id,is_archived = False)
        for rec in exam_recs:
            data = exam_scheme_academic_frequency_mapping.objects.filter(frequency_id=frequency_id,scheme_mapping=rec.id,is_archived=False)
            if data:
                flag = True
        if flag ==True:
            pass
        else:
            exam_details_frequency.objects.filter(exam_details_name_id=exam_id, frequency_details_name_id=frequency_id).delete()
            flag = False
    except:
            messages.warning(request, "Some Error Occurred. ")
    return JsonResponse(flag, safe=False)

@user_login_required
def ViewReportDetails(request):
    academic_year_id = request.GET.get('academic_year', None)
    report_name_recs = reported_exam_class_sub_mapping.objects.filter(year_id = academic_year_id)
    skill_scheme_list = []
    report_score_ids = []
    for rec in report_name_recs:
        if rec.reported_exam_mapp.id not in report_score_ids:
            report_score_ids.append(rec.reported_exam_mapp.id)
            skill_dict = {'id': rec.reported_exam_mapp.id, 'report_name': rec.reported_exam_mapp.report_name}
            skill_scheme_list.append(skill_dict)
    return JsonResponse(skill_scheme_list, safe=False)
#=========================================================================================
@user_login_required
def ExportSkillEntry(request):
    try:

        is_submit = request.POST.get('is_submit')
        rec_id = request.POST.get('rec_id')
        all_rec= []
        sub_topic_list = []
        sub_strand_list = []
        sub_strand_dict_list = []
        sub_strand_dict_id = []
        topic_dict_id = []
        topic_dict_list = []
        max_mark_list = []
        list =[]


        column_names = ['Roll No.','Academic Year', 'Class', 'Section', 'Student Code','Scodoo Id','Student Name',  'Subject',
                        'Frequency']
        academic_frequency_rec = scheme_academic_frequency_mapping.objects.filter(id=rec_id)

        selected_scheme = scheme.objects.get(id=academic_frequency_rec[0].scheme_mapping.scheme_name.id)
        scheme_mapped_flag = False

        if selected_scheme.subject_scheme_name:
            scheme_mapped_flag = True


        for strand in selected_scheme.stand_ids.all():
            for sub_strand in strand.sub_strand_ids.all():
                sub_strand_dict = {}
                sub_strand_temp_dict = {}
                if scheme_mapped_flag:
                    sub_strand_temp_dict['sub_strand_name'] = subject_substrands.objects.get(
                        id=sub_strand.sub_strand_name).substrands_name
                else:
                    sub_strand_temp_dict['sub_strand_name'] = sub_strand.sub_strand_name

                sub_strand_temp_dict['id'] = sub_strand.id
                sub_strand_list.append(sub_strand_temp_dict)
                if sub_strand.id not in sub_strand_dict_id:
                    sub_strand_dict_id.append(sub_strand.id)
                    if scheme_mapped_flag:
                        sub_strand_dict['sub_strand_name'] = subject_substrands.objects.get(
                            id=sub_strand.sub_strand_name).substrands_name
                        sub_strand_dict['subtopic_count'] = 0
                    else:
                        sub_strand_dict['sub_strand_name'] = sub_strand.sub_strand_name
                        sub_strand_dict['subtopic_count'] = 0

                for topic in sub_strand.topic_ids.all():
                    topic_dict = {}
                    if topic.id not in topic_dict_id:
                        topic_dict['topic_name'] = topic.topic_name
                        topic_dict['subtopic_count'] = topic.sub_topic_ids.all().count()
                        topic_dict_id.append(topic.id)
                        topic_dict_list.append(topic_dict)
                        sub_strand_dict['subtopic_count'] += topic.sub_topic_ids.all().count()
                sub_strand_dict_list.append(sub_strand_dict)

        for academic_frequency_rec_obj in academic_frequency_rec:

            for stud in academic_frequency_rec_obj.student_mark_ids.all().order_by('student__first_name'):
                rec_list = []
                rec_list.append(stud.student.roll_no)
                rec_list.append(academic_frequency_rec_obj.academic_mapping.year_name.year_name)
                rec_list.append(academic_frequency_rec_obj.academic_mapping.class_name.class_name)
                rec_list.append(academic_frequency_rec_obj.academic_mapping.section_name.section_name)
                export_class_name = academic_frequency_rec_obj.academic_mapping.class_name.class_name
                export_section_name = academic_frequency_rec_obj.academic_mapping.section_name.section_name

                rec_list.append(stud.student.student_code)
                rec_list.append(stud.student.odoo_id)
                rec_list.append(stud.student.get_all_name())
                rec_list.append(academic_frequency_rec_obj.scheme_mapping.subject.subject_name)
                rec_list.append(academic_frequency_rec_obj.frequency.frequency_name)
                all_rec.append(rec_list)
                for subtopic in stud.subtopic_ids.all():
                    # if subtopic.subtopic.sub_topic.name not in sub_topic_list:
                    if subtopic.subtopic.id not in list:
                        list.append(subtopic.subtopic.id)
                        sub_topic_list.append(subtopic.subtopic.sub_topic.name)
                        max_mark_list.append(subtopic.subtopic.subtopic_mark)
                    if subtopic.mark == "":
                        rec_list.append(" ")
                    else:

                        rec_list.append(subtopic.mark)


        column_names=column_names+sub_topic_list
        a=[]
        export_file_name = "Data_" + str(export_class_name) + "_" + str(export_section_name)
        return export_Skill_Marks_locked_validation_column_xls(export_file_name, column_names, all_rec,[0,1,2,3,4,5,6,7,8],topic_dict_list,sub_strand_dict_list,max_mark_list)
    except Exception as e:
        messages.success(request, "Some Error Occurred. Please Try again."+str(e))

    return redirect('/school/entered_scheme_mark_list/')


#=================================================================================================
@user_login_required
def ExportExamEntry(request):
    try:
        acd_freq_id = request.POST.get('acd_id')
        is_submit = request.POST.get('is_submit')
        if is_submit=='True':

            scheme_rec = exam_scheme_academic_frequency_mapping.objects.get(id=acd_freq_id)

            column_names = ['Academic Year', 'Class', 'Section', 'Student Name', 'Student Code','Scodoo Id', 'Subject', 'Frequency',
                            'Level 2', 'Level 3', 'Level 4','Overall Grade', 'Total Marks', 'Max Marks']
            filtered_students = []
            for student_rec in scheme_rec.student_mark_ids.all().order_by('student__first_name'):
                for rec in student_rec.subtopic_ids.all():
                    rec_list = []
                    rec_list.append(scheme_rec.academic_mapping.year_name.year_name)
                    rec_list.append(scheme_rec.academic_mapping.class_name.class_name)
                    rec_list.append(scheme_rec.academic_mapping.section_name.section_name)
                    rec_list.append(student_rec.student.get_all_name())
                    rec_list.append(student_rec.student.student_code)
                    rec_list.append(student_rec.student.odoo_id)
                    rec_list.append(scheme_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject.subject_name)
                    rec_list.append(scheme_rec.frequency.frequency_name)

                    export_class_name = scheme_rec.academic_mapping.class_name.class_name
                    export_section_name = scheme_rec.academic_mapping.section_name.section_name

                    for sub_rec in rec.subtopic.sub_topic.exam_topic_set.all():
                        for sub_sub_rec in sub_rec.exam_sub_strand_set.all():
                            try:
                                sub_strand_id = int(sub_sub_rec.sub_strand_name)
                                sub_strand_rec = subject_substrands.objects.get(id=sub_strand_id)
                                rec_list.append(sub_strand_rec.substrands_name)
                            except:
                                rec_list.append(sub_sub_rec.sub_strand_name)
                        rec_list.append(sub_rec.topic_name)
                    rec_list.append(rec.subtopic.sub_topic.name)
                    rec_list.append(student_rec.grade_mark)
                    rec_list.append(rec.mark)
                    rec_list.append(rec.subtopic.subtopic_mark)

                    filtered_students.append(rec_list)
                    export_file_name = "Data_" + str(export_class_name) + "_" + str(export_section_name)
            return export_freezd_column_xls(export_file_name, column_names, filtered_students)
        else:
            scheme_rec = exam_scheme_academic_frequency_mapping.objects.get(id=acd_freq_id)

            column_names = ['Rec_ID','Academic Year', 'Class', 'Section', 'Student Name', 'Student Code','Scodoo Id', 'Subject', 'Frequency',
                            'Level 2', 'Level 3', 'Level 4', 'Total Marks', 'Max Marks']
            filtered_students = []
            max_mark_list = []
            all_rec = []
            for student_rec in scheme_rec.student_mark_ids.all():
                for rec in student_rec.subtopic_ids.all():
                    rec_list = []
                    rec_list.append(rec.id)
                    rec_list.append(scheme_rec.academic_mapping.year_name.year_name)
                    rec_list.append(scheme_rec.academic_mapping.class_name.class_name)
                    rec_list.append(scheme_rec.academic_mapping.section_name.section_name)
                    rec_list.append(student_rec.student.get_all_name())
                    rec_list.append(student_rec.student.student_code)
                    rec_list.append(student_rec.student.odoo_id)
                    rec_list.append(
                        scheme_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all()[0].subject.subject_name)
                    rec_list.append(scheme_rec.frequency.frequency_name)
                    all_rec.append(rec_list)
                    export_class_name = scheme_rec.academic_mapping.class_name.class_name
                    export_section_name = scheme_rec.academic_mapping.section_name.section_name

                    for sub_rec in rec.subtopic.sub_topic.exam_topic_set.all():
                        for sub_sub_rec in sub_rec.exam_sub_strand_set.all():
                            try:
                                sub_strand_id = int(sub_sub_rec.sub_strand_name)
                                sub_strand_rec = subject_substrands.objects.get(id=sub_strand_id)
                                rec_list.append(sub_strand_rec.substrands_name)
                            except:
                                rec_list.append(sub_sub_rec.sub_strand_name)
                        rec_list.append(sub_rec.topic_name)
                    rec_list.append(rec.subtopic.sub_topic.name)
                    rec_list.append(rec.mark)
                    rec_list.append(rec.subtopic.subtopic_mark)

                    filtered_students.append(rec_list)
                    export_file_name = "Data_" + str(export_class_name) + "_" + str(export_section_name)

            for data in scheme_rec.student_mark_ids.all():
                for rec in data.subtopic_ids.all():
                    max_mark_list.append(rec.subtopic.subtopic_mark)
            # return export_locked_validation_column_xls(export_file_name, column_names, filtered_students,
            #                                            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,13],max_mark_list)
            # return export_test(export_file_name, column_names, filtered_students,
            #                                            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13], max_mark_list)

            return export_test(export_file_name, column_names, all_rec,[0, 1, 2, 3, 4, 5, 6, 7, 8,9,10,11,13], max_mark_list)


    except:
        messages.success(request, "Some Error Occurred. Please Try again.")

    return redirect('/school/entered_exam_scheme_mark_list/')

#======================================================================================================

@user_login_required
def ExportRubricEntry(request):
    try:
        acd_freq_id = request.POST.get('acd_id')
        scheme_rec = rubric_academic_frequency_mapping.objects.get(id=acd_freq_id)

        column_names = ['Academic Year', 'Class', 'Section', 'Student Name', 'Student Code','Scodoo Id', 'Subject', 'Frequency','Level 2','Level 3',
                        'Level 4', 'Overall Grade']
        filtered_students = []
        for student_rec in scheme_rec.student_mark_ids.all().order_by('student__first_name'):
            for rec in student_rec.student_option_ids.all():
                rec_list = []
                rec_list.append(scheme_rec.academic_mapping.year_name.year_name)
                rec_list.append(scheme_rec.academic_mapping.class_name.class_name)
                rec_list.append(scheme_rec.academic_mapping.section_name.section_name)
                export_class_name = scheme_rec.academic_mapping.class_name.class_name
                export_section_name = scheme_rec.academic_mapping.section_name.section_name

                rec_list.append(student_rec.student.get_all_name())
                rec_list.append(student_rec.student.student_code)
                rec_list.append(student_rec.student.odoo_id)
                rec_list.append(scheme_rec.scheme_mapping.subject.subject_name)
                rec_list.append(scheme_rec.frequency.frequency_name)

                for sub_rec in rec.questions.rubric_sub_strand_set.all():
                    for sub_sub_rec in sub_rec.rubric_strand_set.all():
                        rec_list.append(sub_sub_rec.strand_name)

                        for sub_sub_sub_rec in sub_sub_rec.sub_strand_ids.all():
                            rec_list.append(sub_sub_sub_rec.sub_strand_name)

                rec_list.append(rec.questions.topic_name)
                rec_list.append(student_rec.total_mark)

                filtered_students.append(rec_list)
        export_file_name = "Data_" + str(export_class_name) + "_" + str(export_section_name)
        return export_users_xls(export_file_name, column_names, filtered_students)
    except:
        messages.success(request, "Some Error Occurred. Please Try again.")
    return redirect('/school/rubric_enter_mark_list/')

@user_login_required
@user_permission_required('assessment.can_view_view_analysis', '/school/home/')
def ShowAllReport(request):
    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    scheme_recs = scheme.objects.all()
    frequency_recs = frequency_details.objects.all()
    subject_recs = subjects.objects.all()
    current_academic_year = None
    selected_year_id = None
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
    except:
        messages.error(request, "Please add academic year first.")
        return render(request, "show_all_report.html", {'class_list': all_class_recs, 'section_list': all_section_recs, "year_list": all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})
    return render(request, "show_all_report.html", {'class_list': all_class_recs, 'section_list': all_section_recs, "year_list": all_academicyr_recs,'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,  'scheme_recs': scheme_recs,'frequency_recs':frequency_recs,'subject_recs':subject_recs})



@user_login_required
def ExportAllReport(request):
    try:
        selected_year_id = request.POST.get('year_name')
        selected_class_id = request.POST.get('class_name')
        selected_section_id = request.POST.get('section_name')
        selected_subject_id = request.POST.get('subject_name')
        selected_data_type = request.POST.get('data_type')

        if selected_data_type == '0':

            year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                                   section_name=selected_section_id,
                                                                                   class_name=selected_class_id)
            scheme_rec = scheme_academic_frequency_mapping.objects.filter(academic_mapping=year_class_section_obj,
                                                                       scheme_mapping__subject=selected_subject_id)


            column_names = ['Academic Year', 'Class', 'Section', 'Gender', 'Religion', 'Nationality', 'SEN(Yes/No)',
                            'Subject', 'Subject Code', 'Grade Scheme', 'Frequency', 'Student ID', 'Student Name','Level 1 (Strand)','Level 2 (Sub-Strand)','Level 3 (Topic)','Level 4 (Sub-Topic)','Max Score','Earned Score','Scaled Score','Grade']

            filtered_students = []
            for s_rec in scheme_rec:
                for student_rec in s_rec.student_mark_ids.all():
                    for rec in student_rec.subtopic_ids.all():
                        rec_list = []
                        rec_list.append(s_rec.academic_mapping.year_name.year_name)
                        rec_list.append(s_rec.academic_mapping.class_name.class_name)
                        rec_list.append(s_rec.academic_mapping.section_name.section_name)
                        rec_list.append(student_rec.student.gender)
                        rec_list.append(student_rec.student.nationality.nationality_name)
                        rec_list.append(student_rec.student.religion.religion_name)
                        rec_list.append(student_rec.student.sen)
                        rec_list.append(s_rec.scheme_mapping.subject.subject_name)
                        rec_list.append(s_rec.scheme_mapping.subject.subject_code)
                        rec_list.append(s_rec.scheme_mapping.grade_name.grade_name)
                        rec_list.append(s_rec.frequency.frequency_name)
                        rec_list.append(student_rec.student.student_code)
                        rec_list.append(student_rec.student.first_name)

                        for sub_rec in rec.subtopic.sub_topic.topic_set.all():
                            for sub_sub_rec in sub_rec.sub_strand_set.all():

                                for sub_sub_sub_rec in sub_sub_rec.strand_set.all():
                                    try:
                                        strand_id = int(sub_sub_sub_rec.strand_name)
                                        strand_rec = subject_strands.objects.get(id=strand_id)
                                        rec_list.append(strand_rec.strand_name)
                                    except:
                                        rec_list.append(sub_sub_sub_rec.strand_name)


                                try:
                                    sub_strand_id = int(sub_sub_rec.sub_strand_name)
                                    sub_strand_rec = subject_substrands.objects.get(id=sub_strand_id)
                                    rec_list.append(sub_strand_rec.substrands_name)
                                except:
                                    rec_list.append(sub_sub_rec.sub_strand_name)

                            rec_list.append(sub_rec.topic_name)
                        rec_list.append(rec.subtopic.sub_topic.name)
                        rec_list.append(rec.subtopic.subtopic_mark)
                        rec_list.append(rec.mark)

                        try:
                            stud_mark = int(rec.mark)
                            rec_list.append(int(rec.mark) * 100 / int(int(rec.subtopic.subtopic_mark)))
                        except:
                            rec_list.append('')


                        rec_list.append(student_rec.grade_mark)

                        filtered_students.append(rec_list)

            return export_users_xls('skill_raw_data', column_names, filtered_students)

        elif selected_data_type=='1':

            year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                                   section_name=selected_section_id,
                                                                                   class_name=selected_class_id)
            scheme_rec = scheme_academic_frequency_mapping.objects.filter(academic_mapping=year_class_section_obj,
                                                                          scheme_mapping__subject=selected_subject_id)

            column_names = ['Academic Year', 'Class', 'Section', 'Subject', 'Subject Code', 'Skill Scheme', 'Grade Scheme',
                            'Frequency', 'Student ID', 'Student Name', 'Earned Score', 'Max Score', 'Scaled Score',
                            'Grade']

            filtered_students = []
            for s_rec in scheme_rec:
                for student_rec in s_rec.student_mark_ids.all():
                    for rec in student_rec.subtopic_ids.all():
                        rec_list = []
                        rec_list.append(s_rec.academic_mapping.year_name.year_name)
                        rec_list.append(s_rec.academic_mapping.class_name.class_name)
                        rec_list.append(s_rec.academic_mapping.section_name.section_name)
                        rec_list.append(s_rec.scheme_mapping.subject.subject_name)
                        rec_list.append(s_rec.scheme_mapping.subject.subject_code)
                        rec_list.append(s_rec.scheme_mapping.scheme_name.scheme_name)
                        rec_list.append(s_rec.scheme_mapping.grade_name.grade_name)
                        rec_list.append(s_rec.frequency.frequency_name)
                        rec_list.append(student_rec.student.student_code)
                        rec_list.append(student_rec.student.first_name)
                        rec_list.append(rec.mark)
                        rec_list.append(rec.subtopic.subtopic_mark)

                        try:
                            stud_mark = int(rec.mark)
                            rec_list.append(int(rec.mark) * 100 / int(int(rec.subtopic.subtopic_mark)))
                        except:
                            rec_list.append('')

                        rec_list.append(student_rec.grade_mark)


                        filtered_students.append(rec_list)

            return export_users_xls('skill_grade_data', column_names, filtered_students)


        elif selected_data_type == '2':

            year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,section_name=selected_section_id,class_name=selected_class_id)
            scheme_recs = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping=year_class_section_obj)
            column_names = ['Academic Year','Class','Section','Gender','Religion','Nationality','SEN(Yes/No)','Subject','Subject Code','Frequency','Student ID','Student Name','Level 1 (Strand)','Level 2 (Sub-Strand)','Level 3 (Topic)','Level 4 (Sub-Topic)','Max Marks','Earned Marks','Scaled Marks','Grade']

            filtered_students = []
            for scheme_rec in scheme_recs:
                for scheme_mapping_rec in scheme_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all():
                    if int(scheme_mapping_rec.subject.id)==int(selected_subject_id):
                        for student_rec in scheme_rec.student_mark_ids.all():
                            for sub_topic_rec in student_rec.subtopic_ids.all():
                                rec_list = []
                                rec_list.append(scheme_rec.academic_mapping.year_name.year_name)
                                rec_list.append(scheme_rec.academic_mapping.class_name.class_name)
                                rec_list.append(scheme_rec.academic_mapping.section_name.section_name)
                                rec_list.append(student_rec.student.gender)
                                rec_list.append(student_rec.student.nationality.nationality_name)
                                rec_list.append(student_rec.student.religion.religion_name)
                                rec_list.append(student_rec.student.sen)
                                rec_list.append(scheme_mapping_rec.subject.subject_name)
                                rec_list.append(scheme_mapping_rec.subject.subject_code)
                                # rec_list.append(student_rec.student.student_code)
                                rec_list.append(scheme_rec.frequency.frequency_name)
                                rec_list.append(student_rec.student.student_code)
                                rec_list.append(student_rec.student.first_name)

                                for sub_rec in sub_topic_rec.subtopic.sub_topic.exam_topic_set.all():
                                    for sub_sub_rec in sub_rec.exam_sub_strand_set.all():

                                        for sub_sub_sub_rec in sub_sub_rec.exam_strand_set.all():
                                            try:
                                                strand_id = int(sub_sub_sub_rec.strand_name)
                                                strand_rec = subject_strands.objects.get(id=strand_id)
                                                rec_list.append(strand_rec.strand_name)
                                            except:
                                                rec_list.append(sub_sub_sub_rec.strand_name)

                                        try:
                                            sub_strand_id = int(sub_sub_rec.sub_strand_name)
                                            sub_strand_rec = subject_substrands.objects.get(id=sub_strand_id)
                                            rec_list.append(sub_strand_rec.substrands_name)
                                        except:
                                            rec_list.append(sub_sub_rec.sub_strand_name)

                                    rec_list.append(sub_rec.topic_name)
                                    rec_list.append(sub_topic_rec.subtopic.sub_topic.name)
                                    rec_list.append(sub_topic_rec.subtopic.subtopic_mark)
                                    rec_list.append(sub_topic_rec.mark)

                                    try:
                                        stud_mark = int(sub_topic_rec.mark)
                                        rec_list.append(int(sub_topic_rec.mark) * 100 / int(int(sub_topic_rec.subtopic.subtopic_mark)))
                                    except:
                                        rec_list.append('')

                                    rec_list.append(student_rec.grade_mark)




                                filtered_students.append(rec_list)

                    else:
                        pass



            return export_users_xls('exam_raw_data', column_names, filtered_students)



        elif selected_data_type == '3':

            year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,section_name=selected_section_id,class_name=selected_class_id)
            scheme_recs = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping=year_class_section_obj)
            column_names = ['Academic Year','Class','Section','Gender','Religion','Nationality','SEN(Yes/No)','Subject','Subject Code','Exam Name','Exam Scheme','Grade Scheme','Frequency','Student ID','Student Name','Max Marks','Earned Marks','Scaled Marks','Grade']

            filtered_students = []
            for scheme_rec in scheme_recs:
                for scheme_mapping_rec in scheme_rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all():
                    if int(scheme_mapping_rec.subject.id)==int(selected_subject_id):
                        for student_rec in scheme_rec.student_mark_ids.all():
                            for sub_topic_rec in student_rec.subtopic_ids.all():
                                for scheme_mapping_rec_rec in scheme_mapping_rec.exam_name_mapping_ids.all():
                                    rec_list = []
                                    rec_list.append(scheme_rec.academic_mapping.year_name.year_name)
                                    rec_list.append(scheme_rec.academic_mapping.class_name.class_name)
                                    rec_list.append(scheme_rec.academic_mapping.section_name.section_name)
                                    rec_list.append(student_rec.student.gender)
                                    rec_list.append(student_rec.student.nationality.nationality_name)
                                    rec_list.append(student_rec.student.religion.religion_name)
                                    rec_list.append(student_rec.student.sen)
                                    rec_list.append(scheme_mapping_rec.subject.subject_name)
                                    rec_list.append(scheme_mapping_rec.subject.subject_code)
                                    rec_list.append(scheme_mapping_rec_rec.exam_name.exam_name)
                                    rec_list.append(scheme_mapping_rec_rec.exam_scheme.exam_scheme_name)
                                    rec_list.append(scheme_mapping_rec_rec.grade_name.grade_name)
                                    rec_list.append(scheme_rec.frequency.frequency_name)
                                    rec_list.append(student_rec.student.student_code)
                                    rec_list.append(student_rec.student.first_name)
                                    rec_list.append(sub_topic_rec.subtopic.subtopic_mark)
                                    rec_list.append(sub_topic_rec.mark)
                                    try:
                                        stud_mark = int(sub_topic_rec.mark)
                                        rec_list.append(int(sub_topic_rec.mark) * 100 / int(int(sub_topic_rec.subtopic.subtopic_mark)))
                                    except:
                                        rec_list.append('')
                                    rec_list.append(student_rec.grade_mark)

                                    filtered_students.append(rec_list)

                    else:
                        pass

            return export_users_xls('exam_grade_data', column_names, filtered_students)
    except:
        messages.success(request, "Some Error Occoured. Please Try again.")

    return redirect('/school/show_all_report/')

@user_login_required
def GetMappedSubject(request):
    academic_year = request.POST.get('academic_year')
    class_name = request.POST.get('class_name')
    section_name = request.POST.get('section_name')
    select_name = request.POST.get('select_name')

    if select_name=='0':
        scheme_subject_list = []
        scheme_list = []
        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_year,section_name=section_name,class_name=class_name)
        scheme_recs = scheme_academic_frequency_mapping.objects.filter(academic_mapping=year_class_section_obj)
        for scheme_rec in scheme_recs:
            if scheme_rec.scheme_mapping.subject.id not in scheme_subject_list:
                scheme_subject_list.append(scheme_rec.scheme_mapping.subject.id)
                scheme_dict = {'id': scheme_rec.scheme_mapping.subject.id, 'subject_name': scheme_rec.scheme_mapping.subject.subject_name}
                scheme_list.append(scheme_dict)
        return JsonResponse(scheme_list, safe=False)

    elif select_name=='1':
        exam_list = []
        subject_list = []
        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=academic_year,section_name=section_name, class_name=class_name)
        exam_recs = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping=year_class_section_obj)
        for rec in exam_recs:
            for exam_rec in rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.all():
                if exam_rec.subject.id not in subject_list:
                    subject_list.append(exam_rec.subject.id)
                    exam_dict = {'id':exam_rec.subject.id,'subject_name':exam_rec.subject.subject_name}
                    exam_list.append(exam_dict)
        return JsonResponse(exam_list,safe=False)

def UserDefinedExp(request,rec_id):
    reported_score_rec = reported_score_details.objects.get(id=rec_id)
    # system_required_exp = reported_system_required_exceptions.objects.filter(reported_exam_mapp=reported_score_rec, is_active = True)
    system_required_exp = reported_exam_class_sub_mapping.objects.filter(reported_exam_mapp=reported_score_rec)
    class_rec = []
    class_id = []

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    exam_obj = exam_scheme_acd_mapping_details.objects.filter(year=current_academic_year)

    for rec in exam_obj:
        if rec.exam_name_mapping_ids.filter(is_archived = False):
            if rec.class_obj.id not in class_id:
                class_id.append(rec.class_obj.id)
                class_rec.append(rec.class_obj)

    reported_score_recs = reported_score_details.objects.all()
    reported_class_sub_mapping_recs = reported_exam_class_sub_mapping.objects.all()
    flag = True

    return render(request, "user_defined_exc.html",
                  {'current_academic_year': current_academic_year, 'class_recs': class_rec,
                   'reported_score_recs': reported_score_recs,
                   'reported_score_rec': reported_score_rec,
                   'reported_class_sub_mapping_recs': reported_class_sub_mapping_recs,
                   'system_required_exp': system_required_exp,'flag':flag})


@user_login_required
def GetSectionName(request):
    finalDict = []
    class_name = request.GET.get('class_id', None)
    academic_year = request.GET.get('academic_year_id', None)
    rep_data = academic_class_section_mapping.objects.filter(class_name=class_name, year_name=academic_year)
    for obj in rep_data:
        exam_data = {'id': obj.section_name.id, 'section_name': obj.section_name.section_name}
        finalDict.append(exam_data)
    return JsonResponse(finalDict, safe=False)

@user_login_required
def SaveUserExp(request):
    reported_score = request.POST.get('reported_score')
    data = json.loads(request.POST.get('json_data'))
    current_academic_year = academic_year.objects.get(current_academic_year=1)
    for rec in data:
        if not reported_user_defined_exceptions.objects.filter(
                year_id=current_academic_year.id, class_name_id=rec['cls'],section_name_id=rec['sec'], subject_id=rec['subj'],
                exam_name_id=rec['exam'],
                frequency_name_id=rec['frequency'], reported_exam_mapp_id=reported_score).exists():
            reported_user_defined_exceptions.objects.create(
                year_id=current_academic_year.id, class_name_id=rec['cls'],section_name_id=rec['sec'], subject_id=rec['subj'],
                exam_name_id=rec['exam'],
                frequency_name_id=rec['frequency'], reported_exam_mapp_id=reported_score)
    return redirect('/school/reported_score_class_section_mapping/')



#===================================New Target Setting=========================

@user_login_required
def Student_Target_MappedSubjectList(request):
     finalDict = []
     year_name = request.POST.get('academic_year', None)
     class_name = request.POST.get('class_name', None)
     section_name = json.loads(request.POST.get('section_name', None))
     if section_name==None:
         return JsonResponse(finalDict, safe=False)
     temp_list = []
     for sec_rec in section_name:
         academic_class_rec = academic_class_section_mapping.objects.get(section_name_id=sec_rec, class_name_id=class_name,year_name_id=year_name)
         exam_mapping_rec = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping=academic_class_rec)
         for rec in exam_mapping_rec:
             ab={}
             sub_rec = rec.scheme_mapping.exam_scheme_mapp_exam_name_relation.get().subject
             if sub_rec.id not in temp_list:
                 ab['sub_id'] = sub_rec.id
                 ab['sub_value'] = sub_rec.subject_name
                 finalDict.append(ab)
                 temp_list.append(sub_rec.id)
     return JsonResponse(finalDict,safe=False)


@user_login_required
def set_student_targert_filter_page(request):
    all_academicyr_recs = academic_year.objects.all()
    subject_list = []
    class_list=[]
    exam_scheme_academic_rec = exam_scheme_academic_frequency_mapping.objects.all()
    all_section_recs = sections.objects.all()
    scheme_recs = scheme.objects.all()
    frequency_recs = frequency_details.objects.all()
    current_academic_year = None
    selected_year_id = None

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    obj=exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping__year_name=current_academic_year)
    for rec in obj:
        if rec.academic_mapping.class_name not in class_list:
            class_list.append(rec.academic_mapping.class_name)


    try:
        all_rec=exam_scheme_acd_mapping_details.objects.all()
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id


    except:
        messages.error(request, "Please add academic year first.")
        return render(request, "set_student_target_list.html",
                      {'class_list': class_list, 'subject_list': subject_list, "year_list": all_academicyr_recs,
                       'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                       })
    return render(request, "set_student_target_list.html",
                  {'class_list': class_list, 'subject_list': subject_list, "year_list": all_academicyr_recs,
                   'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                   })

# @user_login_required
# def set_student_targert_post_page(request):
#     student_list=[]
#     class_list =[]
#     exam_list=[]
#     final_dict = {}
#     exam_scheme_rec = ""
#     frequency_list = ""
#     sz_set_target_setting_frequency = ""
#     all_class_recs = class_details.objects.all()
#     all_section_recs = sections.objects.all()
#     all_academicyr_recs = academic_year.objects.all()
#     selected_year_id = request.POST.get('year_name')  # It will be used in HTML page as a value of selected Class
#     current_academic_year = academic_year.objects.get(id=selected_year_id)
#     selected_year_name = current_academic_year.year_name
#     selected_class_id = request.POST.get('class_name')  # It will be used in HTML page as a value of selected Class
#     selected_class_rec = class_details.objects.get(id=selected_class_id)
#     selected_class_name = selected_class_rec.class_name
#     # selected_section_id = request.POST.get('section_name')  # It will be used in HTML page as a value of selected Section
#     # selected_section_rec = sections.objects.get(id=selected_section_id)
#     # selected_section_name = selected_section_rec.section_name
#     selected_subject_id = request.POST.get('subject_name')  # It will be used in HTML page as a value of selected Section
#     selected_subject_rec = subjects.objects.get(id=selected_subject_id)
#     selected_subject_name = selected_subject_rec.subject_name
#     selected_section_id = json.loads(request.POST.get('section_hidden_data'))
#     current_academic_year = academic_year.objects.get(current_academic_year=1)
#     obj=exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping__year_name=current_academic_year)
#     for rec in obj:
#         if rec.academic_mapping.class_name not in class_list:
#             class_list.append(rec.academic_mapping.class_name)
#     modelDict = []
#     grade_list = []
#     all_student_list = []
#     frequency_list = []
#     exam_scheme_rec = []
#     # ac_recs = academic_class_section_mapping.objects.get(year_name=selected_year_id,section_name=selected_section_id,class_name=selected_class_id)
#     # target_rec = sz_set_target_setting.objects.filter(academic_mapping=ac_recs,subject_name=selected_subject_rec)
#
#     # if target_rec:
#     #      messages.warning(request, "Target is already set for selected class, section and subject.")
#     #      return redirect('/school/view_set_target_setting/' + str(target_rec[0].id))
#     for sec_rec in selected_section_id:
#         exam_scheme_academic_recs = exam_scheme_academic_frequency_mapping.objects.filter(scheme_mapping__exam_scheme_mapp_exam_name_relation__subject=selected_subject_rec,academic_mapping__year_name_id=selected_year_id,academic_mapping__class_name_id=selected_class_id,academic_mapping__section_name_id=sec_rec, is_submitted=True,is_archived=False,is_target_set=False)
#         for rec_exam_scheme_acd in exam_scheme_academic_recs:
#             for data in rec_exam_scheme_acd.student_mark_ids.all():
#                  temp_dict = {}
#                  exam_scheme_rec.append(rec_exam_scheme_acd.id)
#                  frequency_list.append(rec_exam_scheme_acd.frequency.id)
#                  temp_dict['acd_student_rec'] = data
#                  temp_dict['grade'] = data.grade_mark
#                  for grade_obj in rec_exam_scheme_acd.scheme_mapping.grade_name.grade_name_rel.all():
#                      if grade_obj.grades == data.grade_mark:
#                          temp_dict['grade'] = grade_obj
#                          break
#                  selected_section_rec = sections.objects.get(id=sec_rec)
#                  selected_section_name = selected_section_rec.section_name
#                  if data.student.academic_class_section.class_name.class_name == selected_class_name and data.student.academic_class_section.section_name.section_name == selected_section_name:
#                      temp_dict['student'] = data.student
#                      temp_dict['grade_detail_obj'] = rec_exam_scheme_acd.scheme_mapping.grade_name.grade_name_rel.all()
#                      all_student_list.append(temp_dict)
#                      final_dict = {}
#                      for i in all_student_list:
#                          if i["student"] not in final_dict:
#                              final_dict[i["student"]] = {'rec': [{'grade': i['grade'], 'acd_student_rec': i['acd_student_rec'],'grade_detail':i['grade_detail_obj']}]}
#                          else:
#                              final_dict[i["student"]]['rec'].append({'grade': i['grade'], 'acd_student_rec': i['acd_student_rec'],'grade_detail':i['grade_detail_obj']})
#         temp_list=[]
#         for exam_rec in exam_scheme_academic_recs :
#             if exam_rec not in temp_list:
#                 rec = {}
#                 rec['exam_name'] = exam_rec.scheme_mapping.exam_name
#                 rec['frequency_list'] = exam_rec.frequency
#                 rec['garde_obj'] = grade_sub_details.objects.filter(grade=exam_rec.scheme_mapping.grade_name)
#                 modelDict.append(rec)
#     return render(request, "set_student_target_list.html",{'exam_list':exam_list,'class_list':class_list,'student_list':student_list,'Selected_Year_Id':selected_year_id,'selected_year_name':selected_year_name,'selected_class_id':selected_class_id,'selected_class_name':selected_class_name,'selected_subject_id':selected_subject_id,
#                                                            'selected_subject_name':selected_subject_name,'modelDict':modelDict,'grade_list':grade_list,'final_dict':final_dict,'selected_section_name':selected_section_name,
#                                                            'selected_subject_id':selected_subject_id,'selected_section_id':selected_section_id,'exam_scheme_rec':exam_scheme_rec,"frequency_list":frequency_list})

@user_login_required
def set_student_targert_post_page(request):
    student_list=[]
    class_list =[]
    exam_list=[]
    final_dict = {}
    exam_scheme_rec = ""
    frequency_list = ""
    sz_set_target_setting_frequency = ""

    all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    selected_year_id = request.POST.get('year_name')  # It will be used in HTML page as a value of selected Class
    current_academic_year = academic_year.objects.get(id=selected_year_id)
    selected_year_name = current_academic_year.year_name

    selected_class_id = request.POST.get('class_name')  # It will be used in HTML page as a value of selected Class
    selected_class_rec = class_details.objects.get(id=selected_class_id)
    selected_class_name = selected_class_rec.class_name

    selected_section_id = request.POST.get('section_name')  # It will be used in HTML page as a value of selected Section
    selected_section_rec = sections.objects.get(id=selected_section_id)
    selected_section_name = selected_section_rec.section_name

    selected_subject_id = request.POST.get('subject_name')  # It will be used in HTML page as a value of selected Section
    selected_subject_rec = subjects.objects.get(id=selected_subject_id)
    selected_subject_name = selected_subject_rec.subject_name

    section_rec_ids = json.loads(request.POST.get('section_hidden_data'))
    section_id_list = []
    if section_rec_ids:
        for sec_rec in section_rec_ids:
            if sec_rec != "":
                section_id_list.append(int(sec_rec))

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    obj=exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping__year_name=current_academic_year)

    for rec in obj:
        if rec.academic_mapping.class_name not in class_list:
            class_list.append(rec.academic_mapping.class_name)

    ac_recs = academic_class_section_mapping.objects.get(year_name=selected_year_id,section_name=selected_section_id,class_name=selected_class_id)

    target_rec = sz_set_target_setting.objects.filter(academic_mapping=ac_recs,subject_name=selected_subject_rec)

    if target_rec:
         messages.warning(request, "Target is already set for selected class, section and subject.")
         return redirect('/school/view_set_target_setting/' + str(target_rec[0].id))


    exam_scheme_academic_recs = exam_scheme_academic_frequency_mapping.objects.filter(scheme_mapping__exam_scheme_mapp_exam_name_relation__subject=selected_subject_rec,academic_mapping=ac_recs, is_submitted=True,is_archived=False,is_target_set=False)

    modelDict = []
    grade_list=[]
    all_student_list = []
    frequency_list = []
    exam_scheme_rec = []


    for rec_exam_scheme_acd in exam_scheme_academic_recs:

        for data in rec_exam_scheme_acd.student_mark_ids.all():
             temp_dict = {}
             exam_scheme_rec.append(rec_exam_scheme_acd.id)
             frequency_list.append(rec_exam_scheme_acd.frequency.id)

             temp_dict['acd_student_rec'] = data
             temp_dict['grade'] = data.grade_mark
             for grade_obj in rec_exam_scheme_acd.scheme_mapping.grade_name.grade_name_rel.all():
                 if grade_obj.grades == data.grade_mark:
                     temp_dict['grade'] = grade_obj
                     break

             if data.student.academic_class_section.class_name.class_name == selected_class_name and data.student.academic_class_section.section_name.section_name == selected_section_name:

                 temp_dict['student'] = data.student
                 temp_dict['grade_detail_obj'] = rec_exam_scheme_acd.scheme_mapping.grade_name.grade_name_rel.all()
                 # for i in student.scheme_mapping.grade_name.grade_name_rel.all().id:
                 all_student_list.append(temp_dict)
                 final_dict = {}
                 for i in all_student_list:
                     if i["student"] not in final_dict:
                         final_dict[i["student"]] = {'rec': [{'grade': i['grade'], 'acd_student_rec': i['acd_student_rec'],'grade_detail':i['grade_detail_obj']}]}
                     else:
                         final_dict[i["student"]]['rec'].append({'grade': i['grade'], 'acd_student_rec': i['acd_student_rec'],'grade_detail':i['grade_detail_obj']})


    temp_list=[]

    for exam_rec in exam_scheme_academic_recs :
        if exam_rec not in temp_list:
            rec = {}
            rec['exam_name'] = exam_rec.scheme_mapping.exam_name
            rec['frequency_list'] = exam_rec.frequency
            rec['garde_obj'] = grade_sub_details.objects.filter(grade=exam_rec.scheme_mapping.grade_name)

            modelDict.append(rec)

    return render(request, "set_student_target_list.html",{'exam_list':exam_list,'class_list':class_list,'student_list':student_list,'Selected_Year_Id':selected_year_id,'selected_year_name':selected_year_name,'selected_class_id':selected_class_id,'selected_class_name':selected_class_name,'selected_subject_id':selected_subject_id,
                                                           'selected_subject_name':selected_subject_name,'modelDict':modelDict,'grade_list':grade_list,'final_dict':final_dict,'selected_section_name':selected_section_name,
                                                           'selected_subject_id':selected_subject_id,'selected_section_id':selected_section_id,'exam_scheme_rec':exam_scheme_rec,"frequency_list":frequency_list,'section_id_list':section_id_list})


@user_login_required
def set_student_target_save(request):
    class_list = []
    current_academic_year = academic_year.objects.get(current_academic_year=1)
    obj = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping__year_name=current_academic_year)
    for rec in obj:
        if rec.academic_mapping.class_name not in class_list:
            class_list.append(rec.academic_mapping.class_name)

    if request.method == 'POST':
        year_id = request.POST.get('year_name')
        class_id = request.POST.get('class_name')
        section_id = request.POST.get('section_name')
        frequency_id = request.POST.get('frequency_list')
        subject_id = request.POST.get('subject_name')
        all_exam_scheme_rec = request.POST.get('all_exam_scheme_rec')
        frequency_id = ast.literal_eval(frequency_id)
        all_exam_scheme_rec = ast.literal_eval(all_exam_scheme_rec)
        data = json.loads(request.POST.get('data'))
        academic_mapping = academic_class_section_mapping.objects.get(year_name=year_id, class_name=class_id,section_name=section_id)

        if sz_set_target_setting.objects.filter(academic_mapping=academic_mapping,subject_name_id=subject_id).exists() :
            set_target_obj = sz_set_target_setting.objects.get(academic_mapping_id=academic_mapping,subject_name_id=subject_id)

            for rec in frequency_id:
                set_target_frequency = sz_set_target_setting_frequency.objects.create(frequency_name_id=rec)
                set_target_obj.frequency_ids.add(set_target_frequency)

            for rec in data:
                student_target_obj = set_target_obj.students.get(student_id = rec['studentid'])
                # student_target_obj = sz_set_student_target_setting.objects.create(student_id=rec['studentid'])
                set_target_obj.students.add(student_target_obj)

                for count in range(int(rec['count'])):
                    count += 1
                    # print sz_set_student_target_sub_details.objects.all()
                    set_student_target_sub_details_obj = sz_set_student_target_sub_details.objects.create(frequency_name_id=rec['frequency-' + str(count)], grade_name_id=rec['grade-' + str(count)])
                    student_target_obj.target_sub_details.add(set_student_target_sub_details_obj)
            pass

        else:

            set_target_obj = sz_set_target_setting.objects.create(academic_mapping=academic_mapping,subject_name_id=subject_id)

            for rec in frequency_id:
                set_target_frequency = sz_set_target_setting_frequency.objects.create(frequency_name_id=rec)
                set_target_obj.frequency_ids.add(set_target_frequency)


            for rec in data:
                student_target_obj = sz_set_student_target_setting.objects.create(student_id=rec['studentid'])
                set_target_obj.students.add(student_target_obj)

                for count in range(int(rec['count'])):
                    count += 1
                    # print sz_set_student_target_sub_details.objects.all()
                    set_student_target_sub_details_obj = sz_set_student_target_sub_details.objects.create(
                        frequency_name_id=rec['frequency-' + str(count)], grade_name_id=rec['grade-' + str(count)],exam_name_id=rec['exam_name-' + str(count)])
                    student_target_obj.target_sub_details.add(set_student_target_sub_details_obj)


        exam_scheme_academic_frequency_mapping.objects.filter(id__in=all_exam_scheme_rec).update(is_target_set=True)

        # return redirect('/school/all_set_student_target_list/',{"class_list":class_list})
        return redirect('/school/set_student_targert_filter_page/')

        # return render(request, "set_student_target_list.html")

@user_login_required
def ViewSetTargetSetting(request,rec_id):
    temp_list = []
    modelDict = []
    frequency_list = []

    target_setting_rec = sz_set_target_setting.objects.get(id=rec_id)


    exam_list = []
    for target_rec in target_setting_rec.students.all():
        for target_sub_details_rec in target_rec.target_sub_details.all():
            if target_sub_details_rec.exam_name.id not in exam_list or target_sub_details_rec.frequency_name.id not in exam_list:
                exam_list.append(target_sub_details_rec.exam_name.id)
                exam_list.append(target_sub_details_rec.frequency_name.id)
                rec = {}
                rec['exam_name'] = target_sub_details_rec.exam_name.exam_name
                rec['frequency_list'] = target_sub_details_rec.frequency_name
                rec['garde_obj'] = grade_sub_details.objects.filter(grade=target_sub_details_rec.grade_name.grade)
                modelDict.append(rec)

    return render(request, "view_target_setting.html",
                  {'target_setting_rec': target_setting_rec, 'modelDict': modelDict, 'frequency_list': frequency_list})


@user_login_required
def SaveEditSetTargetSetting(request):
    selected_target_setting_rec = request.POST.get('selected_target_setting_rec')
    data = json.loads(request.POST.get('data'))

    if sz_set_target_setting.objects.get(id=selected_target_setting_rec):
        for rec in data:
            for count in range(int(rec['count'])):
                count += 1

                sz_set_student_target_sub_details.objects.filter(id=rec['target_sub_rec_id-' + str(count)]).update(
                    grade_name_id=rec['grade-' + str(count)])
    messages.success(request, "Record Updated Successfully.")
    return redirect('/school/view_set_target_setting/' + str(selected_target_setting_rec))




@user_login_required
def all_set_student_target_list(request):
    target_setting_list = sz_set_target_setting.objects.all()

    return render(request,"set_target_setting_list.html",{'mapping_rec':target_setting_list})


@user_login_required
def TargetSettingSubTile(request):
    target_setting_list = sz_set_target_setting.objects.all()
    return render(request, "target_setting_sub_tile.html")



@user_login_required
def StudentTargetViewSectionDetail(request):
    modelDict = []
    class_name = request.GET.get('class_name', None)
    academic_year = request.GET.get('academic_year', None)
    ClassSectionMap = []
    section_list = []
    acde_class_obj = exam_scheme_academic_frequency_mapping.objects.filter(academic_mapping__year_name=academic_year,academic_mapping__class_name=class_name)
    for i in acde_class_obj:
        if i.academic_mapping.section_name not in section_list :
            section_list.append(i.academic_mapping.section_name)
            modelDict.append(model_to_dict(i.academic_mapping.section_name))



    return JsonResponse(modelDict, safe=False)


@user_login_required
def GetMappedApprovalSubject(request):
    year_name = request.POST.get('academic_year', None)
    class_name = request.POST.get('class_name', None)
    section_name = request.POST.get('section_name',None)
    scheme_mapped_subject_list = []
    year_class_section_obj = academic_class_section_mapping.objects.get(year_name=year_name, section_name=section_name,class_name=class_name)
    scheme_mark_recs = scheme_academic_frequency_mapping.objects.filter(academic_mapping_id=year_class_section_obj.id)
    for scheme_mark_rec in scheme_mark_recs:
        scheme_mapped_subject_list.append(scheme_mark_rec.scheme_mapping.subject)
    scheme_mapped_subject_list = set(scheme_mapped_subject_list)
    subject_rec_list = []
    for subject_rec in scheme_mapped_subject_list:
        subject_dict = {'id': subject_rec.id, 'subject_name': subject_rec.subject_name}
        subject_rec_list.append(subject_dict)
    return JsonResponse(subject_rec_list,safe=False)

def ExportStudentTargetList(request):
    try :
        target_rec = request.POST.get('all_target_rec')
        target_setting_rec = sz_set_target_setting.objects.filter(id=target_rec)
        filtered_students = []
        for target_setting_rec in target_setting_rec:
            for student_rec in target_setting_rec.students.all():
                for frequency_deatil in student_rec.target_sub_details.all():
                    rec_list = []
                    rec_list.append(student_rec.student)
                    rec_list.append(student_rec.student.student_code)
                    rec_list.append(student_rec.student.odoo_id)
                    rec_list.append(target_setting_rec.academic_mapping.class_name.class_name)
                    rec_list.append(target_setting_rec.academic_mapping.section_name.section_name)
                    export_class_name = target_setting_rec.academic_mapping.class_name.class_name
                    export_section_name = target_setting_rec.academic_mapping.section_name.section_name

                    rec_list.append(frequency_deatil.frequency_name.frequency_name)
                    if frequency_deatil.grade_name :
                        rec_list.append(frequency_deatil.grade_name.grades)
                    else:

                        pass
                    filtered_students.append(rec_list)
        export_file_name = "Data_" + str(export_class_name) + "_" + str(export_section_name)
        column_names = ['Student Name', 'Student Code', 'Scodoo id', 'class', 'Section','Frequency Name','Set Target']
        return export_users_xls(export_file_name, column_names, filtered_students)

    except:
        messages.warning(request, "Some Error Occurred. Try Again.")
    return redirect('/school/all_set_student_target_list/')




@user_login_required
def copy_ruburic_scheme_mapping(request):
    scheme_rec = []
    year_class_section_object = academic_class_section_mapping.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    grade_recs = grade_details.objects.filter(is_archive = False)

    if year_class_section_object:
        try:
            year_name = request.POST.get('year_name', None)
            if not year_name:
                current_academic_year = academic_year.objects.get(current_academic_year=1)
                selected_year_id = current_academic_year.id
            else:
                current_academic_year = academic_year.objects.get(id=year_name)
                selected_year_id = year_name


            mapping_list = rubric_mapping_details.objects.filter(year=selected_year_id)
            return render(request, 'copy_rubric_mapping.html',
                          { "year_list": all_academicyr_recs,
                           'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                            'grade_recs':grade_recs,'mapping_list':mapping_list})

        except academic_year.DoesNotExist:
            return render(request, 'view_rubric_mapping.html',
                          { "year_list": all_academicyr_recs})
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'view_rubric_mapping.html')


@user_login_required
def CopyRubricMappingFilter(request):
    scheme_rec = []
    year_class_section_object = academic_class_section_mapping.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    grade_recs = grade_details.objects.filter(is_archive = False)

    if year_class_section_object:
        try:
            year_name = request.POST.get('year_name', None)
            if not year_name:
                current_academic_year = academic_year.objects.get(current_academic_year=1)
                selected_year_id = current_academic_year.id
            else:
                current_academic_year = academic_year.objects.get(id=year_name)
                selected_year_id = year_name

            mapping_list = rubric_mapping_details.objects.filter(year=selected_year_id)

            return render(request, 'copy_rubric_mapping_filter.html',
                          { "year_list": all_academicyr_recs,
                           'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                            'grade_recs':grade_recs,'mapping_list':mapping_list})

        except academic_year.DoesNotExist:
            return render(request, 'view_rubric_mapping.html',
                          {"year_list": all_academicyr_recs})
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'view_rubric_mapping.html')


@user_login_required
def SaveRubricCopyMapping(request):
    try:
        if request.method == 'POST':
            acd_year = request.POST.get('acd_year')
            copy_year = request.POST.get('copy_year')

            scheme_recs = rubric_mapping_details.objects.filter(year_id=acd_year,is_archived=False)


            for rec in scheme_recs:
                scheme_rec = rubric_mapping_details.objects.filter(year_id=copy_year, class_obj_id=rec.class_obj.id, subject_id=rec.subject.id,
                                                                   rubric_name_id=rec.rubric_name.id, grade_name_id=rec.grade_name.id,is_archived=False)
                if scheme_rec:
                    pass
                else:
                    scheme_rec = rubric_mapping_details.objects.create(year_id=copy_year, class_obj_id=rec.class_obj.id,subject_id=rec.subject.id,rubric_name_id=rec.rubric_name.id,grade_name_id=rec.grade_name.id)

        messages.success(request,"Record Mapped Successfully.")
        return redirect('/school/view_rubric_mapping/')
    except:
        messages.warning(request, "Some Error Occurred. Please Try again.")
        return redirect('/school/view_rubric_mapping/')


@user_login_required
def GetRubricSchemeName(request):
    year_name = request.POST['academic_year']
    class_name = request.POST['class_name']
    subject_name = request.POST['subject_name']
    rubric_category = []
    rubric_mapping_recs = rubric_mapping_details.objects.filter(year=year_name, class_obj=class_name, subject=subject_name, is_archived=False)
    for rub_mapp_rec in rubric_mapping_recs:
        rubric_category_dict={'id':rub_mapp_rec.rubric_name.rubrics_category_name.id, 'rubric_category_name': rub_mapp_rec.rubric_name.rubrics_category_name.rubric_catgory}
        rubric_category.append(rubric_category_dict)
    return JsonResponse(rubric_category,safe=False)


@user_login_required
def SaveReportScoreDetails(request):
    try:
        if request.method == 'POST':
            copy_report_name = request.POST.get('copy_report_name')
            report_name = request.POST.get('report_name')
            report_rec = reported_score_details.objects.get(id=copy_report_name)
            report_obj = reported_score_details.objects.create(report_name=report_name)

            grade_id = report_obj.id
            if (grade_id >= 1) and (grade_id <= 9):
                report_code = "R-000" + str(grade_id)
                reported_score_details.objects.filter(id=grade_id).update(code=report_code)

            elif (grade_id >= 10) and (grade_id <= 99):
                report_code = "R-00" + str(grade_id)
                reported_score_details.objects.filter(id=grade_id).update(code=report_code)

            elif (grade_id >= 100) and (grade_id <= 999):
                report_code = "R-0" + str(grade_id)
                reported_score_details.objects.filter(id=grade_id).update(code=report_code)

            else:
                report_code = "R-" + str(grade_id)
                reported_score_details.objects.filter(id=grade_id).update(code=report_code)

            for sub_report_rec in report_rec.reported_score_id.all():
                grade_slab_form = reported_sub_details.objects.create(term_assessment=sub_report_rec.term_assessment,weightage=sub_report_rec.weightage,exam_name=sub_report_rec.exam_name,frequency_name=sub_report_rec.frequency_name)
                report_obj.reported_score_id.add(grade_slab_form.id)

        messages.success(request,"Record Copied Successfully.")
        return redirect('/school/reported_score_list/')
    except:
        messages.warning(request, "Some Error Occurred. Please Try again.")
        return redirect('/school/reported_score_list/')


@user_login_required
def copy_exam_scheme_mapping(request):
    scheme_rec = []
    year_class_section_object = academic_class_section_mapping.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    grade_recs = grade_details.objects.filter(is_archive = False)

    if year_class_section_object:
        try:
            year_name = request.POST.get('year_name', None)
            if not year_name:
                current_academic_year = academic_year.objects.get(current_academic_year=1)
                selected_year_id = current_academic_year.id
            else:
                current_academic_year = academic_year.objects.get(id=year_name)
                selected_year_id = year_name


            mapping_list = exam_scheme_acd_mapping_details.objects.filter(year=selected_year_id)
            return render(request, 'copy_exam_scheme_mapping.html',
                          { "year_list": all_academicyr_recs,
                           'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                            'grade_recs':grade_recs,'mapping_list':mapping_list})

        except academic_year.DoesNotExist:
            return render(request, 'view_exam_scheme_subject_mapping.html',
                          { "year_list": all_academicyr_recs})
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'view_exam_scheme_subject_mapping.html')



@user_login_required
def Copy_exam_Mapping_Filter(request):
    scheme_rec = []
    year_class_section_object = academic_class_section_mapping.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    grade_recs = grade_details.objects.filter(is_archive = False)

    if year_class_section_object:
        try:
            year_name = request.POST.get('year_name', None)
            if not year_name:
                current_academic_year = academic_year.objects.get(current_academic_year=1)
                selected_year_id = current_academic_year.id
            else:
                current_academic_year = academic_year.objects.get(id=year_name)
                selected_year_id = year_name

            mapping_list = exam_scheme_acd_mapping_details.objects.filter(year=selected_year_id)

            return render(request, 'copy_exam_scheme_mapping_filter.html',
                          { "year_list": all_academicyr_recs,
                           'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id,
                            'grade_recs':grade_recs,'mapping_list':mapping_list})

        except academic_year.DoesNotExist:
            return render(request, 'view_exam_scheme_subject_mapping.html',
                          {"year_list": all_academicyr_recs})
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'view_exam_scheme_subject_mapping.html')

@user_login_required
def Save_Exam_Copy_Mapping(request):
    try:
        if request.method == 'POST':
            acd_year = request.POST.get('acd_year')
            copy_year = request.POST.get('copy_year')
            scheme_recs = exam_scheme_acd_mapping_details.objects.filter(year_id=acd_year,exam_name_mapping_ids__is_archived=False)
            for rec in scheme_recs:
                for exam_scheme_rec in rec.exam_name_mapping_ids.all():
                    scheme_rec = exam_scheme_acd_mapping_details.objects.filter(year_id=copy_year, class_obj_id=rec.class_obj.id, subject_id=rec.subject.id,exam_name_mapping_ids__exam_scheme=exam_scheme_rec.exam_scheme.id,exam_name_mapping_ids__exam_name=exam_scheme_rec.exam_name.id, exam_name_mapping_ids__grade_name_id=exam_scheme_rec.grade_name.id,exam_name_mapping_ids__is_archived=False)
                    if scheme_rec:
                        pass
                    else:
                        copy_scheme_rec = exam_scheme_acd_mapping_details.objects.create(year_id=copy_year,class_obj_id=rec.class_obj.id,subject_id=rec.subject.id)
                        exam_name_mapping_obj = exam_scheme_exam_name_mapping.objects.create(exam_name_id=exam_scheme_rec.exam_name.id, exam_scheme_id=exam_scheme_rec.exam_scheme.id,grade_name_id=exam_scheme_rec.grade_name.id)
                        copy_scheme_rec.exam_name_mapping_ids.add(exam_name_mapping_obj)



        messages.success(request,"Record Mapped Successfully.")
        return redirect('/school/view_exam_scheme_subject_mapping/')
    except:
        messages.warning(request, "Some Error Occurred. Please Try again.")
        return redirect('/school/view_exam_scheme_subject_mapping/')


@user_login_required
def perodic_data_subtile(request):
    try:
        return render(request,'perodic_sub_tiles.html')

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again."+str(e))
    return redirect('/school/perodic_data_subtile/')


@user_login_required
def perodic_scheme_definition_list(request):
    try:
        perodic_scheme_list =[]
        temp_list =[]
        perodic_obj = perodic_assesment_data.objects.all()
        return render(request,'perodic_scheme_definition_list.html',{'perodic_obj':perodic_obj})

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again."+str(e))
    return redirect('/school/perodic_data_subtile/')


@user_login_required
def perodic_define_scheme(request):

    frequency_mapping_ids = request.POST.getlist('frequency_mapping', None)
    frequency_recs = frequency_details.objects.all()
    subject_recs = subjects.objects.all()

    frequency_id_list = []
    frequency_obj_list =[]
    mandatory_subject_list = []
    subject_list = []
    optional_subject_list = []

    if frequency_mapping_ids:
        for freq_id in frequency_mapping_ids:
            if freq_id != "":
                frequency_id_list.append(int(freq_id))

        frequency_obj_list = frequency_details.objects.filter(id__in=frequency_id_list)

    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        academic_class_section_recs = academic_class_section_mapping.objects.filter(year_name_id=selected_year_id)
        mandatory_subject_recs = mandatory_subject_mapping.objects.filter(
            academic_class_section_mapping__in=academic_class_section_recs)

        for mandatory_subject_rec in mandatory_subject_recs:
            mandatory_subject_list.append(mandatory_subject_rec.subject_id)
        mandatory_subject_list_set = set(mandatory_subject_list)
        subject_mandatory_recs = subjects.objects.filter(id__in=mandatory_subject_list_set)

        for subject_mandatory_rec in subject_mandatory_recs:
            subject_list.append(subject_mandatory_rec)

        student_recs = student_details.objects.filter(academic_class_section__in=academic_class_section_recs)
        for student_rec in student_recs:
            for subject in student_rec.subject_ids.all():
                optional_subject_list.append(subject)

        optional_subject_list_set = set(optional_subject_list)
        for optional_subject_rec in optional_subject_list_set:
            subject_list.append(optional_subject_rec)

    except Exception as e:
        messages.success(request, "Current Academic Year Not Not Found. Please Create One And Try Again."+str(e))
        return redirect('/school/perodic_scheme_definition_list/')

    return render(request, "perodic_scheme_defination.html",{'subject_recs':subject_list,'frequency_recs':frequency_recs,'frequency_id_list':frequency_id_list,'frequency_obj_list':frequency_obj_list})

@user_login_required
def PerodicSchemeDefineFilter(request):
    try:
        if request.POST:
            request.session['data'] = request.POST
            selected_scheme_code = request.POST.get('scheme_code')
            selected_scheme_name = request.POST.get('scheme_name')
            frequency_mapping_ids = request.POST.getlist('frequency_mapping',None)
            request.session['frequency_mapping_ids']=frequency_mapping_ids
            selected_subject_name = request.POST.get('subject_name')
        else:
            selected_scheme_code = request.session['data'].get('scheme_code')
            selected_scheme_name = request.session['data'].get('scheme_name')
            frequency_mapping_ids = request.session['frequency_mapping_ids']
            selected_subject_name = request.session['data'].get('subject_name')
        subject_recs = subjects.objects.all()
        frequency_recs = frequency_details.objects.all()
        frequency_id_list =[]

        if frequency_mapping_ids:
            for freq_id in frequency_mapping_ids:
                if freq_id!="":
                    frequency_id_list.append(int(freq_id))

            frequency_obj_list = frequency_details.objects.filter(id__in = frequency_id_list)

        subject_rec = subjects.objects.get(id=selected_subject_name)
        subject_list = subject_rec.strands_ids.all()
        return render(request, "view_perodic_scheme_filter.html",{'subject_list':subject_list,'selected_scheme_code':selected_scheme_code,'selected_scheme_name':selected_scheme_name,'selected_subject_name':subject_rec,'subject_recs':subject_recs,'frequency_recs':frequency_recs,'frequency_id_list':frequency_id_list,'frequency_obj_list':frequency_obj_list})

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/perodic_data_subtile/')




@user_login_required
def save_perodic_defination_scheme(request):
    try:
        if request.method == 'POST':

                scheme_code  = request.POST.get('scheme_code')
                scheme_name = request.POST.get('scheme_name')
                submit_rec = request.POST.get('submit_rec')
                val_dict =  json.loads(request.POST['data'])
                subject_name = request.POST.get('subject_name')
                frequency_data = json.loads(request.POST.get('frequency_data'))

                scheme_obj = perodic_assesment_data.objects.create(perodic_scheme_code=scheme_code, perodic_scheme_name=scheme_name,perodic_subject_id=subject_name)
                frequency_size = len(frequency_data)

                frequency_size = len(frequency_data)
                for count in range(frequency_size):
                    scheme_frequency_obj = perodic_scheme_frequecy.objects.create(perodic_frequency_name_id=frequency_data[count])
                    scheme_obj.perodic_scheme_frequency_ids.add(scheme_frequency_obj.id)

                for data in val_dict:
                    strand_data = perodic_strand.objects.create(strand_name=data['strand'])
                    scheme_obj.perodic_strand.add(strand_data.id)

                    for substrand in data['substrand']:
                        sub_str = perodic_sub_strand.objects.create(sub_strand_name=substrand['substrand_name'])
                        strand_data.sub_strand_ids.add(sub_str.id)

                        for recs in substrand['topic']:
                            topic_name1 = recs['topic_name']
                            topic_table = perodic_topics.objects.create(topic=topic_name1)
                            sub_str.topic_ids.add(topic_table.id)


                scheme.objects.filter(id=scheme_obj.id).update(is_submitted=True)

                messages.success(request, "Record Saved Successfully.")
                return redirect('/school/perodic_scheme_definition_list/')

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again.")+str(e)
    return redirect('/school/perodic_scheme_definition_list/')



@user_login_required
def edit_perodic_defination_scheme(request,scheme_id):
    try:
        option_count = itertools.count(1)
        iterator_color = make_incrementor(0)
        iterator_mark = make_incrementor(0)
        iterator_subtopic = make_incrementor(0)
        iterator_topic = make_incrementor(0)
        iterator_topic_add = make_incrementor(0)
        iterator_topic_rem = make_incrementor(0)
        iterator_substrand = make_incrementor(0)
        iterator_substrand_add = make_incrementor(0)
        iterator_substrand_rem = make_incrementor(0)
        color_count = make_incrementor(0)
        mark_count = make_incrementor(0)
        subtopic_count = make_incrementor(0)
        scheme_recs = perodic_assesment_data.objects.get(id=scheme_id)
        subject_check = scheme_recs.perodic_subject
        substrand_list = []
        name_edit_flag = False
        frequecy_recs = scheme_recs.perodic_scheme_frequency_ids.all()

        name_edit_flag = scheme_academic_frequency_mapping.objects.filter(scheme_mapping__scheme_name_id=scheme_id).exists()

        if not subject_check == None:
            subject_rec = subjects.objects.get(id=subject_check.id)
            subject_list = subject_rec.strands_ids.all()

            for obj in subject_rec.strands_ids.all():
                for sub_obj in obj.substrands_ids.all():
                    substrand_list.append(sub_obj)
            view_val = {
                'name_edit_flag': name_edit_flag,
                'iterator_color': iterator_color,
                'iterator_mark': iterator_mark,
                'iterator_subtopic': iterator_subtopic,
                'iterator_topic': iterator_topic,
                'iterator_topic_add': iterator_topic_add,
                'iterator_topic_rem': iterator_topic_rem,
                'iterator_substrand_add': iterator_substrand_add,
                'iterator_substrand_rem': iterator_substrand_rem,
                'iterator_substrand': iterator_substrand,
                'color_count': color_count,
                'mark_count': mark_count,
                'subtopic_count': subtopic_count,
                "scheme_recs": scheme_recs,
                'submitted': scheme_recs.is_submitted,
                'option_count': option_count,
                'st_top_count': make_incrementor(0),
                'st_top_count1': make_incrementor(0),
                'st_top_count2': make_incrementor(0),
                'subject_list': subject_list,
                'subject_rec': subject_rec,
                'substrand_list': substrand_list,
                'selected_scheme_code': scheme_recs.perodic_scheme_code,
                'selected_scheme_name': scheme_recs.perodic_scheme_name,
                'frequecy_recs': frequecy_recs,
            }
        return render(request,'view_edit_perodic_scheme.html',view_val)


    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again.")+str(e)
    return redirect('/school/perodic_scheme_definition_list/')



@user_login_required
def Delete_perodic_assesment_Scheme(request,scheme_id):
    try:
        scheme_recs = perodic_assesment_data.objects.get(id=scheme_id)

        for strand_rec in scheme_recs.perodic_strand.all():
            for sub_strand_rec in strand_rec.sub_strand_ids.all():
                for topic_rec in sub_strand_rec.topic_ids.all():
                    topic_rec.delete()
                sub_strand_rec.topic_ids.all()
            strand_rec.sub_strand_ids.all().delete()

        scheme_recs.perodic_strand.all().delete()
        perodic_assesment_data.objects.filter(id=scheme_id).delete()
        messages.success(request, "Perodic Scheme Deleted Successfully.")
    except Exception as e:
        messages.warning(request, "Can't delete this record. Some error occurred."+str(e))

    return redirect('/school/perodic_scheme_definition_list/')


@user_login_required
def DeletePerodicStrand(request):
    try:
        scheme_id = request.POST.get('scheme_id')
        stand_id = request.POST.get('stand_id')
        scheme_recs = perodic_assesment_data.objects.get(id=scheme_id)
        strand_obj = perodic_strand.objects.get(id=stand_id)
        scheme_recs.perodic_strand.remove(strand_obj)

        for sub_strand_rec in strand_obj.sub_strand_ids.all():
            sub_strand_obj = perodic_sub_strand.objects.get(id = sub_strand_rec.id)

            for topic_rec in sub_strand_obj.topic_ids.all():
                topic_obj = perodic_topics.objects.get(id=topic_rec.id)
                sub_strand_obj.topic_ids.remove(topic_rec)

                perodic_topics.objects.filter(id=topic_obj.id).delete()
                strand_obj.sub_strand_ids.remove(sub_strand_rec)
                perodic_sub_strand.objects.filter(id=sub_strand_rec.id).delete()
            perodic_sub_strand.objects.filter(id=stand_id).delete()

        messages.success(request, "Perodic Strand Deleted Successfully.")
        return redirect('/school/edit_perodic_defination_scheme/'+str(scheme_id))

    except Exception as e:
        messages.warning(request, "Can't delete this record. Some error occurred."+str(e))
        return redirect('/school/perodic_scheme_definition_list/')


def update_perodic_defination_scheme(request):
    scheme_id = request.POST.get('scheme_id')
    stand_id = request.POST.get('stand_id')
    submit = request.POST.get('submit')
    scheme_name = request.POST.get('scheme_name')
    val_dict = json.loads(request.POST['data'])
    frequency_data = json.loads(request.POST.get('frequency_data'))
    scheme_recs = perodic_assesment_data.objects.get(id=scheme_id)
    freq_ids=[]
    freq_ids = frequency_details.objects.filter(id__in=frequency_data).values_list('id',flat=1)

    if scheme_name:
        perodic_assesment_data.objects.filter(id=scheme_id).update(scheme_name = scheme_name)

    old_freq = []
    scheme_recs = perodic_assesment_data.objects.get(id=scheme_id)
    frequecy_recs = scheme_recs.perodic_scheme_frequency_ids.all()

    old_freq = scheme_recs.perodic_scheme_frequency_ids.all().values_list('id', flat=1)
    #
    # if scheme_academic_frequency_mapping.objects.filter(scheme_mapping__scheme_name_id=scheme_id,
    #                                                     frequency_id__in=old_freq).exists():
    #     if not set(old_freq).issubset(freq_ids):
    #         messages.warning(request, "You can't delete old frequencies as there relation exists..")
    #         return HttpResponse(json.dumps(scheme_recs.to_dict()), content_type='application/json')
    try:

        for rec in old_freq:
            scheme_recs.perodic_scheme_frequency_ids.filter(perodic_frequency_name=rec).delete()
            scheme_recs.perodic_scheme_frequency_ids.remove(rec)


        frequency_size = len(frequency_data)
        for count in range(frequency_size):
            if not scheme_recs.perodic_scheme_frequency_ids.filter(perodic_frequency_name_id=frequency_data[count]).exists():
                scheme_frequency_obj = perodic_scheme_frequecy.objects.create(perodic_frequency_name_id=frequency_data[count])
                scheme_recs.perodic_scheme_frequency_ids.add(scheme_frequency_obj.id)
            else:
                if scheme_recs.perodic_scheme_frequency_ids.all().count() > 1:
                    scheme_recs.perodic_scheme_frequency_ids.filter(perodic_frequency_name_id=frequency_data[count]).delete()
                    scheme_recs.perodic_scheme_frequency_ids.remove(frequency_data[count])
                else:
                    messages.warning(request, "A single frequency can't be deleted.")
                    pass

        for rec in val_dict:
            for data in rec:
                if not data['stand_id']:
                    strand_data = perodic_strand.objects.create(strand_name=data['strand'])
                    scheme_recs.perodic_strand.add(strand_data.id)
                else:
                    strand_data = perodic_strand.objects.get(id=data['stand_id'])
                    strand_data.strand_name = data['strand']
                    strand_data.save()

                for substrand in data['substrand']:
                    sub_str_create = None
                    try:
                        sub_str = perodic_sub_strand.objects.get(id=substrand['substrand_id'])
                    except:
                        sub_str_create = perodic_sub_strand.objects.create(sub_strand_name=substrand['substrand_name'])

                    if not sub_str_create:
                        sub_str.sub_strand_name = substrand['substrand_name']
                        sub_str.save()
                    else:
                        strand_data.sub_strand_ids.add(sub_str_create.id)

                    for recs in substrand['topic']:
                        topic_name1 = recs['topic']
                        topic_table_create = None
                        try:
                            topic_table = perodic_topics.objects.get(id=recs['topic_id'])
                        except:
                            topic_table_create = perodic_topics.objects.create(topic=topic_name1)

                        if not topic_table_create:
                            topic_table.topic = topic_name1
                            topic_table.save()
                        else:
                            if not sub_str_create:
                                sub_str.topic_ids.add(topic_table_create.id)
                            else:
                                sub_str_create.topic_ids.add(topic_table_create.id)

        perodic_assesment_data.objects.filter(id=scheme_recs.id).update(is_submitted=True, updated_on=str(datetime.now()))
        messages.success(request, "Record Updated Successfully.")
        return redirect('/school/perodic_scheme_definition_list/')

    except Exception as e:
            messages.warning(request, "Some Error Occurred... Please Try Again"+str(e))
            return redirect('/school/perodic_scheme_definition_list/')

@user_login_required
def ViewGetPerodicSchemeFreq(request):
        freq_recs = []
        scheme_id = request.POST.get('scheme_id')
        scheme_details_recs = perodic_assesment_data.objects.get(id=scheme_id)
        fre_list = []
        frequency_list =  scheme_details_recs.perodic_scheme_frequency_ids.all()
        for f_rec in frequency_list:
            f_id = f_rec.perodic_frequency_name.id
            fre_list.append(f_id)
        freq_obj = frequency_details.objects.all()
        for rec in freq_obj:
            ab = {'id': rec.id, 'freq_name': rec.frequency_name, 'fre_list': fre_list}
            freq_recs.append(ab)
        return JsonResponse(freq_recs, safe=False)


@user_login_required
def DeletePerodicTopic(request):
    try:
        scheme_id = request.POST.get('scheme_id')
        substrand_id = request.POST.get('substrand_id')
        topic_id = request.POST.get('topic_id')
        substrand_rec = perodic_sub_strand.objects.get(id=substrand_id)
        substrand_rec.topic_ids.remove(topic_id)
        perodic_topics.objects.filter(id=topic_id).delete()
        messages.success(request, "Topic Deleted Successfully.")
        return redirect('/school/edit_perodic_defination_scheme/' + str(scheme_id))

    except Exception as e:
            messages.warning(request, "Some Error Occurred... Please Try Again"+str(e))
    return redirect('/school/edit_perodic_defination_scheme/' + str(scheme_id))


def create_question_subtile(request):
    return render(request,'create_question_subtile.html')

def multiple_choice_question(request):
    try:
        perodic_scheme = perodic_assesment_data.objects.all()

    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))

    return render(request, 'multiple_choise_question.html',{'perodic_scheme':perodic_scheme})

def save_multiple_choice_question(request):
    try:

        photo = request.FILES.get('upload_question_image_name')
        title = request.POST.get('title')
        instruction = request.POST.get('instruction')
        question_name = request.POST.get('question')
        options_list = request.POST.getlist('options')
        correct_answer = request.POST.get('correct_answer')
        max_score = request.POST.get('max_score')
        perodic_strand_id = request.POST.get('strand_name')
        subject =  request.POST.get('subject_name')
        sub_strand =  request.POST.get('sub_strand')
        topic_name =  request.POST.get('topic_name')
        if correct_answer:
            correct_answer = correct_answer.split("-")[0]

        question_obj = question.objects.create(question_name=question_name,perodic_question_strand_id=perodic_strand_id, instruction=instruction, title=title,
                                            question_type="MCQ", mark=max_score,
                                               perodic_question_subject_id=subject,
                                               perodic_question_substrand_id=sub_strand,
                                               perodic_question_topic_id=topic_name,question_photo=photo)

        for rec in options_list:
            if str(rec)==str(correct_answer):
                option = answer.objects.create(answer=rec,correct_flag=True)
            else:
                option = answer.objects.create(answer=rec)

            question_obj.ans.add(option.id)

        messages.success(request, "Record Saved Successfully.")

    except Exception as e:
            messages.warning(request, "Some Error Occurred... Please Try Again"+str(e))


    return redirect('/school/create_question_subtile/')


def fill_inthe_blanks_create(request):
    perodic_scheme = perodic_assesment_data.objects.all()
    return render(request, 'fill_inthe_blanks_create.html',{'perodic_scheme':perodic_scheme })

def short_text_question_create(request):
    perodic_scheme = perodic_assesment_data.objects.all()
    return render(request,'short_text_question_create.html',{'perodic_scheme':perodic_scheme})

def save_short_text(request):
    try:
        photo = request.FILES.get('upload_question_image_name')
        title = request.POST.get('title')
        instruction = request.POST.get('instruction')
        question_name = request.POST.get('question')
        correct_answer = request.POST.get('correct_answer')
        max_score = request.POST.get('max_score')
        perodic_strand_id = request.POST.get('strand_name')
        subject = request.POST.get('subject_name')
        sub_strand = request.POST.get('sub_strand')
        topic_name = request.POST.get('topic_name')

        question_obj = question.objects.create(title=title,perodic_question_strand_id=perodic_strand_id,instruction=instruction,question_name=question_name,mark=max_score,
                                               question_type="Short Text",
                                               perodic_question_subject_id=subject,
                                               perodic_question_substrand_id=sub_strand,
                                               perodic_question_topic_id=topic_name,question_photo = photo)


        option = answer.objects.create(answer=correct_answer, correct_flag=True)
        question_obj.ans.add(option.id)
        messages.success(request, "Record Saved Successfully.")
    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))
    return redirect('/school/create_question_subtile/')


def subject_realted_strand_perodic_defination(request):

    strand_rec_list = []
    subject_id = request.POST.get("subject_id")
    perodic_scheme_id = request.POST.get("perodic_scheme_id")
    perodic_assesment_rec = perodic_assesment_data.objects.get(id=perodic_scheme_id)
    strand_list = perodic_assesment_rec.perodic_strand.all()
    for rec in strand_list:
        raw_dict ={}
        raw_dict['id']=rec.id
        raw_dict['strand_name']=subject_strands.objects.get(id=rec.strand_name).strand_name
        strand_rec_list.append(raw_dict)

    return JsonResponse(strand_rec_list, safe=False)


def subject_realted_substrand_perodic_defination(request):

    substrand_rec_list = []
    strand_id = request.POST.get("strand_name")
    strand_rec = perodic_strand.objects.get(id=strand_id)
    for rec in strand_rec.sub_strand_ids.all():
        raw_dict ={}
        raw_dict['id']=rec.id
        raw_dict['substrand_name']=subject_substrands.objects.get(id=rec.sub_strand_name).substrands_name
        substrand_rec_list.append(raw_dict)

    return JsonResponse(substrand_rec_list, safe=False)


def subject_realted_topic_perodic_defination(request):
    try:
        substrand_rec_list = []
        substrand_name = request.POST.get("substrand_name")
        strand_rec = perodic_sub_strand.objects.get(id=substrand_name)
        strand_list = strand_rec.topic_ids.all()
        for rec in strand_list:
            raw_dict ={}
            raw_dict['id']=rec.id
            raw_dict['topic_name']=rec.topic
            substrand_rec_list.append(raw_dict)
    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))
    return JsonResponse(substrand_rec_list, safe=False)

def perodic_scheme_related_subjects(request):
    try:
        subject_list  = []
        perodic_scheme_id = request.POST.get("perodic_scheme_id")
        perodic_assesment_rec = perodic_assesment_data.objects.get(id=perodic_scheme_id)
        perodic_subject = perodic_assesment_rec.perodic_subject
        subject_rec = subjects.objects.filter(id=perodic_subject.id)
        for rec in subject_rec:
            raw_dict ={}
            raw_dict['id']=perodic_subject.id
            raw_dict['subject_name']=rec.subject_name
            subject_list.append(raw_dict)

    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))

    return JsonResponse(subject_list, safe=False)


def search_question(request):
    perodic_scheme = perodic_assesment_data.objects.all()
    question_obj = question.objects.all()
    return render(request, 'search_question.html',{'perodic_scheme':perodic_scheme,'question_obj':question_obj})


def filter_search_question(request):
    try:
        subject_list = subjects.objects.all()
        perodic_strand_id = request.POST.get('strand_name')
        subject = request.POST.get('subject_name')
        sub_strand = request.POST.get('sub_strand')
        topic_name = request.POST.get('topic_name')
        perodic_scheme = request.POST.get('perodic_scheme')

        question_obj = ''
        selected_topic = ''
        selected_strand = ''
        selected_topic=''
        selected_substrand = ''
        selected_subject = ''
        perodic_assesment_recs = ''
        question_obj = question.objects.filter(perodic_question_subject=subject,perodic_question_topic=topic_name,perodic_question_substrand=sub_strand,perodic_question_strand=perodic_strand_id)

        selected_topic = perodic_topics.objects.get(id=topic_name)

        perodic_strand_obj = perodic_strand.objects.get(id=perodic_strand_id)
        selected_strand =subject_strands.objects.get(id=perodic_strand_obj.strand_name)

        substrand_obj = perodic_sub_strand.objects.get(id=sub_strand)
        selected_substrand = subject_substrands.objects.get(id=substrand_obj.sub_strand_name)

        selected_subject = subjects.objects.get(id=subject)

        perodic_assesment_recs = perodic_assesment_data.objects.get(id=perodic_scheme)
        perodic_scheme = perodic_assesment_data.objects.all()

    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))

    return render(request, 'search_question.html', {'subject_list': subject_list,"question_obj":question_obj,'selected_topic':selected_topic,'selected_strand':selected_strand,'selected_substrand':selected_substrand,'selected_subject':selected_subject,"perodic_strand_id":perodic_strand_id,'sub_strand':sub_strand,'perodic_assesment_recs':perodic_assesment_recs,'perodic_scheme':perodic_scheme,})


def view_question(request,rec_id):
    try:
        question_obj = question.objects.get(id=rec_id)
        subject_list = subjects.objects.all()

        selected_topic = perodic_topics.objects.get(id=question_obj.perodic_question_topic.id)

        perodic_strand_obj = perodic_strand.objects.get(id=question_obj.perodic_question_strand.id)

        selected_strand = subject_strands.objects.get(id=perodic_strand_obj.strand_name)

        substrand_obj = perodic_sub_strand.objects.get(id=question_obj.perodic_question_substrand.id)

        selected_substrand = subject_substrands.objects.get(id=substrand_obj.sub_strand_name)

        selected_subject = subjects.objects.get(id=question_obj.perodic_question_subject.id)

        answer_options = question_obj.ans.all()
        correct_answer  = question_obj.ans.get(correct_flag=True)

        all_perodic_scheme = perodic_assesment_data.objects.all()

        perodic_scheme = perodic_assesment_data.objects.filter(perodic_strand=perodic_strand_obj,perodic_subject=selected_subject.id)


    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))
        return redirect('/school/search_question/')

    return render(request,'view_question.html',{'question_obj':question_obj,'subject_list':subject_list,'selected_topic':selected_topic,'selected_strand':selected_strand,'selected_substrand':selected_substrand,'selected_subject':selected_subject,'answer_options':answer_options,'rec_id':rec_id,'substrand_obj':substrand_obj,'correct_answer':correct_answer,'perodic_strand_obj':perodic_strand_obj,'perodic_scheme':perodic_scheme,'all_perodic_scheme':all_perodic_scheme})

def update_multiple_choise_question(request):
    try:
        # photo = request.FILES.get('upload_question_image_name')
        title = request.POST.get('title')
        instruction = request.POST.get('instruction')
        question_name = request.POST.get('question')
        options_data = json.loads(request.POST.get('options_data'))
        #options_list = request.POST.getlist('options')
        correct_answer = request.POST.get('correct_answer')
        max_score = request.POST.get('max_score')
        perodic_strand_id = request.POST.get('strand_name')
        subject = request.POST.get('subject_name')
        sub_strand = request.POST.get('sub_strand')
        topic_name = request.POST.get('topic_name')
        rec_id = request.POST.get('rec_id')

        if correct_answer:
            correct_answer = correct_answer.split('-')[0]

        question_obj = question.objects.filter(id=rec_id)

        if request.FILES:
            print('files')
            Photo = request.FILES['upload_question_image_name']
            dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
            filename = "%s_%s.%s" % (request.POST.get('question')+('_question_id - ')+str(rec_id), dirname, 'png')
            raw_file_path_and_name = os.path.join('doc', filename)
            image_result = open(settings.MEDIA_ROOT + raw_file_path_and_name, 'wb')
            image_result.write(Photo.read())
            question_obj.update(question_photo = raw_file_path_and_name)


        question_obj.update(perodic_question_strand_id=perodic_strand_id,perodic_question_substrand_id=sub_strand,title=title,instruction=instruction,question_name=question_name,mark=max_score,perodic_question_subject_id=subject,perodic_question_topic_id=topic_name)

        if options_data:
            question_obj[0].ans.filter().update(correct_flag=False)
            for rec in options_data:
                if(rec['id'][0:6])=="option":
                    if str(rec['value']) == str(correct_answer):
                        option= answer.objects.create(answer=rec['value'],correct_flag=True)
                    else:
                        option = answer.objects.create(answer=rec['value'])

                    question_obj[0].ans.add(option.id)
                else:
                    if str(rec['value']) == str(correct_answer):
                        option = answer.objects.filter(id=rec['id']).update(answer=rec['value'], correct_flag=True)
                    else:
                        option = answer.objects.filter(id=rec['id']).update(answer=rec['value'])


        messages.success(request, "Record updated successfully")
    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))

    return redirect('/school/view_question/'+str(rec_id))

def view_short_text_question(request,rec_id):
    try:
        question_obj = question.objects.get(id=rec_id)
        subject_list = subjects.objects.all()

        selected_topic = perodic_topics.objects.get(id=question_obj.perodic_question_topic.id)
        perodic_strand_obj = perodic_strand.objects.get(id=question_obj.perodic_question_strand.id)
        selected_strand = subject_strands.objects.get(id=perodic_strand_obj.strand_name)

        substrand_obj = perodic_sub_strand.objects.get(id=question_obj.perodic_question_substrand.id)
        selected_substrand = subject_substrands.objects.get(id=substrand_obj.sub_strand_name)

        selected_subject = subjects.objects.get(id=question_obj.perodic_question_subject.id)

        answer_options = question_obj.ans.all()
        correct_answer = question_obj.ans.get(correct_flag=True)

        all_perodic_scheme = perodic_assesment_data.objects.all()

        perodic_scheme = perodic_assesment_data.objects.filter(perodic_strand=perodic_strand_obj,perodic_subject=selected_subject.id)

    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))
        return redirect('/school/search_question/')

    return render(request,'view_text_entry_question.html',{'question_obj':question_obj,'subject_list':subject_list,'selected_topic':selected_topic,'selected_strand':selected_strand,'selected_substrand':selected_substrand,'selected_subject':selected_subject,'answer_options':answer_options,'rec_id':rec_id,'substrand_obj':substrand_obj,'correct_answer':correct_answer,'all_perodic_scheme':all_perodic_scheme,'perodic_scheme':perodic_scheme,'perodic_strand_obj':perodic_strand_obj})

def update_short_text_question(request):
    try:
        title = request.POST.get('title')
        instruction = request.POST.get('instruction')
        question_name = request.POST.get('question')
        correct_answer = request.POST.get('correct_answer')
        max_score = request.POST.get('max_score')
        perodic_strand_id = request.POST.get('strand_name')
        subject = request.POST.get('subject_name')
        sub_strand = request.POST.get('sub_strand')
        topic_name = request.POST.get('topic_name')
        rec_id = request.POST.get('rec_id')
        question_obj = question.objects.filter(id=rec_id)

        if request.FILES:
            print('files')
            Photo = request.FILES['upload_question_image_name']
            dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
            filename = "%s_%s.%s" % (request.POST.get('question')+(",question_id - ")+str(rec_id), dirname, 'png')
            raw_file_path_and_name = os.path.join('doc', filename)
            image_result = open(settings.MEDIA_ROOT + raw_file_path_and_name, 'wb')
            image_result.write(Photo.read())
            question_obj.update(question_photo = raw_file_path_and_name)


        question_obj.update(title=title,instruction=instruction,question_name=question_name,mark=max_score, perodic_question_subject_id=subject,
                                            perodic_question_topic_id=topic_name,
                                            perodic_question_strand_id=perodic_strand_id,perodic_question_substrand_id=sub_strand)

        question.objects.get(id=rec_id).ans.all().delete()

        option = answer.objects.create(answer=correct_answer, correct_flag=True)
        question_obj[0].ans.add(option.id)

        messages.success(request, "Record updated successfully")
    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))

    return redirect('/school/view_short_text_question/' + str(rec_id))


def create_mcq_multiple_response(request):
    perodic_scheme = perodic_assesment_data.objects.all()
    return render(request, 'create_mcq_multiple_response.html', {'perodic_scheme':perodic_scheme})



def save_mcq_multiple_response(request):
    try:

        photo = request.FILES.get('upload_question_image_name')
        title = request.POST.get('title')
        instruction = request.POST.get('instruction')
        question_name = request.POST.get('question')
        options_list = request.POST.getlist('options')
        correct_response_data = json.loads(request.POST.get('correct_response_data'))
        max_score = request.POST.get('max_score')
        perodic_strand_id = request.POST.get('strand_name')
        subject = request.POST.get('subject_name')
        sub_strand = request.POST.get('sub_strand')
        topic_name = request.POST.get('topic_name')
        correct_response = []


        question_obj = question.objects.create(question_name=question_name,perodic_question_strand_id=perodic_strand_id, instruction=instruction,title=title,question_type="MCQ multiple response", mark=max_score,perodic_question_subject_id=subject,perodic_question_substrand_id=sub_strand,perodic_question_topic_id=topic_name, question_photo = photo)

        for rec in correct_response_data:
            correct_response.append(rec.split('-')[0])

        for rec in options_list:
            if rec in correct_response :
                option = answer.objects.create(answer=rec,correct_flag=True)
            else:
                option = answer.objects.create(answer=rec)
            question_obj.ans.add(option.id)

        messages.success(request, "Record Created successfully")
    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))

    return redirect('/school/create_question_subtile/')


def view_mcq_multiple_response(request,rec_id):
    try:


        question_obj = question.objects.get(id=rec_id)
        subject_list = subjects.objects.all()

        selected_topic = perodic_topics.objects.get(id=question_obj.perodic_question_topic.id)
        perodic_strand_obj = perodic_strand.objects.get(id=question_obj.perodic_question_strand.id)
        selected_strand = subject_strands.objects.get(id=perodic_strand_obj.strand_name)

        substrand_obj = perodic_sub_strand.objects.get(id=question_obj.perodic_question_substrand.id)
        selected_substrand = subject_substrands.objects.get(id=substrand_obj.sub_strand_name)
        selected_subject = subjects.objects.get(id=question_obj.perodic_question_subject.id)
        options = question_obj.ans.all()
        multiple_answers = question_obj.ans.filter(correct_flag=True)
        multiple_answers_list =[]
        for rec in multiple_answers:
            multiple_answers_list.append(rec.answer)

        all_perodic_scheme = perodic_assesment_data.objects.all()

        perodic_scheme = perodic_assesment_data.objects.filter(perodic_strand=perodic_strand_obj,perodic_subject=selected_subject.id)

    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))
        return redirect('/school/search_question/')


    return render(request,'view_mcq_multiple_response.html',{'subject_list':subject_list,'selected_topic':selected_topic,'selected_strand':selected_strand,'substrand_obj':substrand_obj,'selected_substrand':selected_substrand,'selected_subject':selected_subject,'options':options,'multiple_answers':multiple_answers,'question_obj':question_obj,'multiple_answers_list':multiple_answers_list,'rec_id':rec_id,'perodic_scheme':perodic_scheme,'all_perodic_scheme':all_perodic_scheme,'perodic_strand_obj':perodic_strand_obj})

def update_mcq_multiple_response(request):
    try:
        rec_id = request.POST.get('rec_id')
        title = request.POST.get('title')
        instruction = request.POST.get('instruction')
        question_name = request.POST.get('question')
        max_score = request.POST.get('max_score')
        options_data = json.loads(request.POST.get('options_data'))
        perodic_strand_id = request.POST.get('strand_name')
        subject = request.POST.get('subject_name')
        sub_strand = request.POST.get('sub_strand')
        topic_name = request.POST.get('topic_name')
        options_list = request.POST.getlist('options')
        correct_response_data = json.loads(request.POST.get('correct_response_data'))
        correct_response = []

        question_obj = question.objects.filter(id=rec_id)

        if request.FILES:
            print('files')
            Photo = request.FILES['upload_question_image_name']
            dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
            filename = "%s_%s.%s" % (request.POST.get('question')+(",question_id - ")+str(rec_id), dirname, 'png')
            raw_file_path_and_name = os.path.join('doc', filename)
            image_result = open(settings.MEDIA_ROOT + raw_file_path_and_name, 'wb')
            image_result.write(Photo.read())
            question_obj.update(question_photo = raw_file_path_and_name)

        question_obj.update(title=title, instruction=instruction, question_name=question_name,mark=max_score, perodic_question_subject_id=subject,perodic_question_topic_id=topic_name,perodic_question_strand_id=perodic_strand_id,perodic_question_substrand_id=sub_strand)


        for rec in correct_response_data:
            correct_response.append(rec.split('-')[0])


        if options_data:
            question_obj[0].ans.filter().update(correct_flag=False)
            for rec in options_data:
                if(rec['id'][0:6])=="option":
                    if (rec['value'])in correct_response :
                        option= answer.objects.create(answer=rec['value'],correct_flag=True)
                    else:
                        option = answer.objects.create(answer=rec['value'])

                    question_obj[0].ans.add(option.id)
                else:
                    if (rec['value'])in correct_response:
                        option = answer.objects.filter(id=rec['id']).update(answer=rec['value'], correct_flag=True)
                    else:
                        option = answer.objects.filter(id=rec['id']).update(answer=rec['value'])


        messages.success(request, "Record Updated Successfully")
    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))
    return redirect('/school/view_mcq_multiple_response/'+str(rec_id))

def assessment_acdemic_class_section_mapping(request):
    try:
        year_list = academic_year.objects.all()
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        assesment_obj = assesment.objects.all()
    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))

    return render(request, 'assessment_acdemic_class_section_mapping.html', {'year_list':year_list, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id, 'assesment_obj':assesment_obj})


def save_assessment_acdemic_class_section_mapping(request):
    try:
        year_id = request.POST.getlist('year_name')
        class_ids = request.POST.getlist('class_name')
        assessment_ids= request.POST.getlist('assessment_name')

        for class_id in class_ids:
            if class_id:
                sections_ids = academic_class_section_mapping.objects.filter(year_name_id=year_id[0],class_name_id=class_id).values_list('section_name')
                for section_id in sections_ids:
                    if section_id:
                        rec = academic_class_section_mapping.objects.filter(class_name_id=class_id,section_name_id=section_id[0],year_name_id=year_id[0])
                        if rec  :
                            for question in assessment_ids :
                                data = perodic_question_academic_class_section_mapping.objects.filter(academic_class_section_mapping_id=rec[0].id, assesment_id=question)
                                if data:
                                    pass
                                else:
                                    form = perodic_question_academic_class_section_mapping(academic_class_section_mapping_id=rec[0].id,assesment_id=question)
                                    form.save()

        messages.warning(request, "Record Added Successfully")

    except Exception as e:
        messages.success(request, "Some Error Occurred... Please Try Again" + str(e))

    return redirect('/school/assessment_acdemic_class_section_mapping/')

def view_assessment_acdemic_class_section_mapping(request):
    mapping_obj = perodic_question_academic_class_section_mapping.objects.all()
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

    except:
        messages.warning(request, "Current Academic Year Not Found. Please Create Current AY First.")
        return redirect('/school/assessment_acdemic_class_section_mapping/')

    return render(request, 'view_assessment_acdemic_class_section_mapping.html', {'mapping_obj':mapping_obj, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id, })


def filter_view_assessment_acdemic_class_section_mapping(request):
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        year_id = request.POST.get('year_name')
        class_id = request.POST.get('class_name')
        section_id = request.POST.get('section_name')

        selected_class = class_details.objects.get(id=class_id)
        selected_section = sections.objects.get(id=section_id)


        mapping_obj = perodic_question_academic_class_section_mapping.objects.filter(academic_class_section_mapping__year_name=current_academic_year,academic_class_section_mapping__class_name_id=class_id,academic_class_section_mapping__section_name_id=section_id)


    except Exception as e:
        messages.success(request, "Some Error Occurred... Please Try Again" + str(e))
        return redirect('/school/question_acdemic_class_section_mapping/')

    return render(request, 'view_assessment_acdemic_class_section_mapping.html',
                  {'mapping_obj': mapping_obj, 'selected_year_name': current_academic_year,
                   'Selected_Year_Id': selected_year_id,'selected_class':selected_class,'selected_section':selected_section })

def delete_assessment_acdemic_class_section_mapping(request):
   try:
        check = request.POST.getlist('check')
        for rec in check:
            object = rec.split(',')
            mandatory_id = object[0]  # rec will have list of Id's and by getting rec[0] we are getting the only id as the first record

            perodic_question_academic_class_section_mapping.objects.get(id=mandatory_id).delete()


   except Exception as e:
       messages.success(request, "Some Error Occurred... Please Try Again" + str(e))
       return redirect('/school/assessment_acdemic_class_section_mapping/')

   return redirect('/school/assessment_acdemic_class_section_mapping/')


def create_assessment(request):
    all_perodic_scheme = perodic_assesment_data.objects.all()
    assessment_code  = random_string_generator(8, include_lowercase=False, include_uppercase=True, include_number=True)
    return render(request,'create_assessment.html',{'all_perodic_scheme':all_perodic_scheme,'assessment_code':assessment_code})


def subject_based_questions(request):
    try:
        question_list = []
        topic_id = request.POST.get("topic_id")
        question_rec = question.objects.filter(perodic_question_topic_id=topic_id)
        for rec in question_rec:
            raw_dict = {}
            raw_dict['id'] = rec.id
            raw_dict['question_name'] = rec.question_name
            question_list.append(raw_dict)

    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))
    return JsonResponse(question_list, safe=False)

def save_assessment(request):
    try:
        duration = request.POST.get("duration")
        assessment_code = request.POST.get("assessment_code")
        title = request.POST.get("title")
        description = request.POST.get("description")
        question = request.POST.getlist("question_name")
        total_mark = request.POST.get("total_mark")
        schedule = request.POST.get("schedule_assessment")
        schedule_assessment = datetime.strptime(schedule, '%Y-%m-%dT%H:%M')
        duration_time = datetime.strptime(duration, '%H:%M')
        assesment_obj = assesment.objects.create(title=title,description=description,created_by=request.user,duration=duration_time,total_mark=str(total_mark),assesment_date_time=schedule_assessment,is_active=True,assessment_code=assessment_code)

        for rec in question:
            assesment_obj.assessment_question.add(rec)

        messages.success(request, "Assessment Created Successfully")
    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))
    return redirect('/school/all_assessment_list/')

def all_assessment_list(request):
    assesment_list =assesment.objects.all()
    return render(request, 'all_assessment_list.html',{"assesment_list":assesment_list})


def view_assessment(request,rec_id):
    try:
        question_obj = ''
        subject_list =''
        selected_topic= ''
        all_question= ''
        selected_strand= ''
        substrand_obj =''
        selected_substrand= ''
        selected_subject= ''
        schedule_time = ''
        perodic_scheme = ''
        subject_list = subjects.objects.all()
        all_perodic_scheme = perodic_assesment_data.objects.all()

        assesment_obj =assesment.objects.get(id=rec_id)
        question_rec = assesment_obj.assessment_question.all()
        if question_rec:
            question_obj = question.objects.get(id=question_rec[0].id)
            subject_list = subjects.objects.all()
            selected_topic = perodic_topics.objects.get(id=question_obj.perodic_question_topic.id)
            all_question = question.objects.filter(perodic_question_topic_id=selected_topic.id)

            perodic_strand_obj = perodic_strand.objects.get(id=question_obj.perodic_question_strand.id)

            selected_strand = subject_strands.objects.get(id=perodic_strand_obj.strand_name)

            substrand_obj = perodic_sub_strand.objects.get(id=question_obj.perodic_question_substrand.id)

            selected_substrand = subject_substrands.objects.get(id=substrand_obj.sub_strand_name)

            selected_subject = subjects.objects.get(id=question_obj.perodic_question_subject.id)



            perodic_scheme = perodic_assesment_data.objects.filter(perodic_strand=perodic_strand_obj,perodic_subject=selected_subject.id)

            if assesment_obj.assesment_date_time:
                schedule_time = timezone.localtime(assesment_obj.assesment_date_time)
                schedule_time =  schedule_time.strftime('%Y-%m-%dT%H:%M')

    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))
        return redirect('/school/all_assessment_list/')

    return render(request,'view_assessment.html',{"assesment_obj":assesment_obj,'substrand_obj':substrand_obj,'selected_substrand':selected_substrand,'selected_subject':selected_subject,'selected_strand':selected_strand,'selected_topic':selected_topic,'subject_list':subject_list,'question_rec':question_rec,'all_question':all_question,"rec_id":rec_id,'subject_list':subject_list,'schedule_time':schedule_time,'all_perodic_scheme':all_perodic_scheme,'perodic_scheme':perodic_scheme})

def mark_based_on_question(request):
    question_id = json.loads(request.POST.get('question_data'))
    question_rec = ''
    try:
        question_rec = question.objects.filter(id__in=question_id)
    except:
        total_mark = 0

    if question_rec:
        total_mark = 0
        for rec in question_rec:
            total_mark+=float(rec.mark)

    else:
        total_mark = 0

    total_mark = "%02d" % total_mark

    return JsonResponse(total_mark, safe=False)


def update_assessment(request):
    try:
        duration = request.POST.get("duration")
        title = request.POST.get("title")
        description = request.POST.get("description")
        schedule = request.POST.get("schedule_assessment")
        question = request.POST.getlist("question_name")
        total_mark = request.POST.get("total_mark")
        rec_id = request.POST.get("rec_id")
        duration_time = datetime.strptime(duration, '%H:%M')
        schedule_assessment = datetime.strptime(schedule, '%Y-%m-%dT%H:%M')

        current_time = datetime.now()
        duration_from_now = schedule_assessment + timedelta(hours=duration_time.hour)

        if schedule_assessment.date() <= current_time.date() or schedule_assessment.date() >= current_time.date():

            if schedule_assessment.date() <= duration_from_now.date() or schedule_assessment.date() >= duration_from_now.date():
                # if schedule_assessment < current_time >= duration_from_now:
                if schedule_assessment.time() < current_time.time() >= duration_from_now.time() or current_time.time() >= duration_from_now.time():

                    assesment_obj = assesment.objects.filter(id=rec_id)
                    assesment_obj.update(title=title,description=description,duration=duration_time,total_mark=str(total_mark),assesment_date_time=schedule_assessment)

                    assesment.objects.get(id=rec_id).assessment_question.clear()

                    for rec in question:
                        assesment_obj[0].assessment_question.add(rec)

                    messages.success(request, "Record Updated Successfully.")
                else:
                    messages.success(request, "You can not update record, assessment is in Process .")

    except Exception as e:
        messages.warning(request, "Some Error Occurred Please Try Again...." + str(e))

    return redirect('/school/view_assessment/'+str(rec_id))


def fill_in_the_blank(request):
    subject_recs = subjects.objects.all()
    perodic_scheme = perodic_assesment_data.objects.all()
    return render(request, "create_fill_in_the_blank.html",{'subject_recs':subject_recs,'perodic_scheme':perodic_scheme})


def save_fill_in_the_blank(request):
    subject_recs = subjects.objects.all()
    strand_list = ""

    try:
        if request.method == 'POST':
            photo = request.FILES.get('upload_question_image_name')
            title_textentry_name = request.POST.get('title_textentry_name')
            instruction_textenter_name = request.POST.get('instruction_textenter_name')
            question_textentry_name = request.POST.get('question_textentry_name')
            subject_name = request.POST.get('subject_name')
            strand_name = request.POST.get('strand_name')
            substrand_name = request.POST.get('sub_strand')
            topic_name = request.POST.get('topic_name')
            # option_field = request.POST.getlist('option_field')
            option_field = request.POST.getlist('options')
            question_mark_name = request.POST.get('question_mark_name')

            mcq_question_obj = question.objects.create(title=title_textentry_name,instruction=instruction_textenter_name,question_name=question_textentry_name,
                                                       perodic_question_subject_id=subject_name,perodic_question_strand_id=strand_name,perodic_question_substrand_id=substrand_name,perodic_question_topic_id=topic_name, mark=question_mark_name,question_type="Fill In The Blank",question_photo = photo)

            counter = 1
            for rec in option_field:
                mcq_data = answer.objects.create(answer=rec, postion=counter)
                mcq_question_obj.ans.add(mcq_data.id)
                counter = + 1

            messages.success(request, "Record Save Successfully.")

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
    return redirect('/school/create_question_subtile/')



def order_question_list(request):
    perodic_scheme = perodic_assesment_data.objects.all()

    return render(request, "order_list_question.html", {'perodic_scheme': perodic_scheme})


def save_order_list_question(request):
    try:
        if request.method == 'POST':
            title_order_name = request.POST.get('title_order_name')
            instruction_order_name = request.POST.get('instruction_order_name')
            question_order_name = request.POST.get('question_order_name')
            add_div = request.POST.getlist('add_div')
            mark_name = request.POST.get('mark_name')
            subject_name = request.POST.get('subject_name')
            strand_name = request.POST.get('strand_name')
            substrand_name = request.POST.get('sub_strand')
            topic_name = request.POST.get('topic_name')
            option_field = request.POST.getlist('option_field')
            upload_question_image = request.FILES.get('upload_question_image')


            question_obj = question.objects.create(title=title_order_name, instruction=instruction_order_name,question_name=question_order_name,question_photo=upload_question_image,
                                                    mark=mark_name, perodic_question_subject_id=subject_name,
                                                    perodic_question_strand_id=strand_name,
                                                    perodic_question_substrand_id=substrand_name,
                                                    perodic_question_topic_id=topic_name,
                                                    question_type='Order List Question')

            for rec in option_field:
                option_obj = option_list.objects.create(option=rec)
                question_obj.possible_ans.add(option_obj)



            count = 1
            for recs in add_div:
                ans_obj = answer.objects.create(answer=recs, postion=count)
                question_obj.ans.add(ans_obj)
                count += 1


            messages.success(request, "Record Saved Successfully.")

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))

    return redirect('/school/create_question_subtile/')


def create_match_order_list(request):
    perodic_scheme = perodic_assesment_data.objects.all()
    return render(request, "match_order_list.html",{'perodic_scheme':perodic_scheme})


def save_match_order_list(request):
    subject_recs = subjects.objects.all()
    strand_list = ""
    data = ""

    try:
        if request.method == 'POST':
            photo = request.FILES.get('upload_question_image_name')
            title_name = request.POST.get('title_name')
            instruction_name = request.POST.get('instruction_name')
            question_name = request.POST.get('question_name')
            subject_name = request.POST.get('subject_name')
            strand_name = request.POST.get('strand_name')
            substrand_name = request.POST.get('sub_strand')
            topic_name = request.POST.get('topic_name')
            question_list_data = request.POST.getlist('question_list')
            answer_response = request.POST.getlist('answer_response')
            question_mark_name = request.POST.get('question_mark_name')
            option_field = request.POST.getlist('option_field')


            mcq_question_obj = question.objects.create(title=title_name, instruction=instruction_name,question_name = question_name ,
                                                       perodic_question_subject_id = subject_name ,perodic_question_strand_id = strand_name, perodic_question_substrand_id = substrand_name ,
                                                       perodic_question_topic_id = topic_name, mark =question_mark_name ,question_type = "Match Order List" ,question_photo=photo)

            for count, list in enumerate(question_list_data):
             list_data = question_list.objects.create(questions = list ,answer_response = answer_response[count])
             mcq_question_obj.question_list_option.add(list_data.id)

            counter = 1
            for rec in option_field:
                mcq_data = answer.objects.create(answer=rec,postion=counter)
                mcq_question_obj.ans.add(mcq_data.id)
                counter += 1

            messages.success(request, "Record Save Successfully.")

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))


    return redirect('/school/create_question_subtile/')

def edit_order_list_question(request,question_id):
    subject_list = subjects.objects.all()
    question_obj = question.objects.get(id=question_id)

    selected_topic = perodic_topics.objects.get(id=question_obj.perodic_question_topic.id)
    perodic_strand_obj = perodic_strand.objects.get(id=question_obj.perodic_question_strand.id)
    selected_strand = subject_strands.objects.get(id=perodic_strand_obj.strand_name)

    substrand_obj = perodic_sub_strand.objects.get(id=question_obj.perodic_question_substrand.id)
    selected_substrand = subject_substrands.objects.get(id=substrand_obj.sub_strand_name)
    selected_subject = subjects.objects.get(id=question_obj.perodic_question_subject.id)

    all_perodic_scheme = perodic_assesment_data.objects.all()
    perodic_scheme = perodic_assesment_data.objects.filter(perodic_strand=perodic_strand_obj,
                                                           perodic_subject=selected_subject.id)

    options = question_obj.possible_ans.all()
    ans_data = question_obj.ans.all().order_by("postion")

    return render(request, "edit_order_list_question.html",{'question_obj': question_obj,'ans_data': ans_data, 'subject_list': subject_list, 'selected_topic':selected_topic,'selected_strand':selected_strand,'substrand_obj':substrand_obj,'selected_substrand':selected_substrand,'selected_subject':selected_subject,'perodic_scheme':perodic_scheme,'all_perodic_scheme':all_perodic_scheme,'options':options,'perodic_strand_obj':perodic_strand_obj})


def update_order_list_question(request):
    try:
        question_id = request.POST.get('question_id')
        title_order_name = request.POST.get('title_order_name')
        instruction_order_name = request.POST.get('instruction_order_name')
        question_order_name = request.POST.get('question_order_name')
        mark_name = request.POST.get('mark_name')
        subject_name = request.POST.get('subject_name')
        strand_name = request.POST.get('strand_name')
        sub_strand_name = request.POST.get('sub_strand')
        topic_name = request.POST.get('topic_name')
        answer_name = json.loads(request.POST.get('answer_name'))
        option_name = json.loads(request.POST.get('option_name'))

        if request.FILES:
            print('files')
            upload_question_image = request.FILES['upload_question_image']
            dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
            filename = "%s_%s.%s" % (request.POST.get('question_order_name'), dirname, 'png')
            raw_file_path_and_name = os.path.join('doc', filename)
            image_result = open(settings.MEDIA_ROOT + raw_file_path_and_name, 'wb')
            image_result.write(upload_question_image.read())
            question.objects.filter(id=question_id).update(question_photo=raw_file_path_and_name)


        question.objects.filter(id=question_id).update(title=title_order_name, instruction=instruction_order_name, question_name=question_order_name, mark=mark_name,perodic_question_subject_id=subject_name,
                                                       perodic_question_strand_id=strand_name,
                                                       perodic_question_substrand_id=sub_strand_name,
                                                       perodic_question_topic_id=topic_name,)




        for recs in option_name:
            option_list_obj= question.objects.filter(id=question_id)[0].possible_ans.filter(id=recs['id'])
            if option_list_obj:
                option_data = option_list_obj.update(option=recs['option'])
            else:
                option_obj = option_list.objects.create(option=recs['option'])
                question.objects.filter(id=question_id)[0].possible_ans.add(option_obj.id)


        for rec in answer_name:
            position = (rec.values())[1]
            answer_list_obj =question.objects.filter(id=question_id)[0].ans.filter(id=rec['id'])
            if answer_list_obj:
                list_data = answer_list_obj.update(answer=rec['answer'],postion=position)
            else:
                create_ans_data = answer.objects.create(answer=rec['answer'], postion=position)
                question.objects.filter(id=question_id)[0].ans.add(create_ans_data.id)



        messages.success(request, "record updated successfully")
    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
    return redirect('/school/edit_order_list_question/'+str(question_id))

def edit_fill_in_the_blank(request,rec_id):

    try:

        subject_recs = subjects.objects.all()
        question_obj = question.objects.get(id=rec_id)

        from assessment.models import perodic_strand
        perodic_strand_obj = perodic_strand.objects.get(id=question_obj.perodic_question_strand.id)
        selected_strand = subject_strands.objects.get(id=perodic_strand_obj.strand_name)

        selected_subject = subjects.objects.get(id=question_obj.perodic_question_subject.id)

        all_perodic_scheme = perodic_assesment_data.objects.all()
        perodic_scheme = perodic_assesment_data.objects.filter(perodic_strand=perodic_strand_obj,perodic_subject=selected_subject.id)

        substrand_obj = perodic_sub_strand.objects.get(id=question_obj.perodic_question_substrand.id)
        selected_substrand = subject_substrands.objects.get(id=substrand_obj.sub_strand_name)

        selected_topic = perodic_topics.objects.get(id=question_obj.perodic_question_topic.id)

        total_answer_response = question_obj.ans.all().count()

        ans_data = question_obj.ans.all()

        question_list = question_obj.question_list_option.all()

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))


    return render(request, "Edit_fill_in_the_blank.html",{'question_obj': question_obj,
                   'question_list': question_list, 'ans_data': ans_data, 'subject_recs': subject_recs,'selected_topic':selected_topic,
                    'selected_subject': selected_subject,'substrand_obj':substrand_obj,'selected_substrand':selected_substrand,'perodic_scheme':perodic_scheme,
                   'all_perodic_scheme':all_perodic_scheme,'rec_id':rec_id,'perodic_strand_obj':perodic_strand_obj,'selected_strand':selected_strand,'total_answer_response':total_answer_response})


def update_fill_in_the_blank_data(request):
    try:
        title_textentry_name = request.POST.get('title_textentry_name')
        instruction_textenter_name = request.POST.get('instruction_textenter_name')
        question_textentry_name = request.POST.get('question_textentry_name')
        answer_option = json.loads(request.POST.get('answer_option_data'))
        question_mark_name = request.POST.get('question_mark_name')
        perodic_scheme = request.POST.get('perodic_scheme')
        subject_name = request.POST.get('subject_name')
        strand_name = request.POST.get('strand_name')
        sub_strand = request.POST.get('sub_strand')
        topic_name = request.POST.get('topic_name')
        question_id =request.POST.get('question_id')

        obj = question.objects.filter(id=question_id)

        if request.FILES:
            print('files')
            Photo = request.FILES['upload_question_image_name']
            dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
            filename = "%s_%s.%s" % (request.POST.get('question_textentry_name')[0:6]+("_question_id - ")+str(question_id), dirname, 'png')
            raw_file_path_and_name = os.path.join('doc', filename)
            image_result = open(settings.MEDIA_ROOT + raw_file_path_and_name, 'wb')
            image_result.write(Photo.read())

            obj.update(question_photo=raw_file_path_and_name)


        obj.update(title=title_textentry_name, instruction=instruction_textenter_name, question_name=question_textentry_name, mark=question_mark_name,
                            perodic_question_subject_id=subject_name, perodic_question_topic_id=topic_name,
                            perodic_question_strand_id=strand_name, perodic_question_substrand_id=sub_strand)

        total_answer_data = obj[0].ans.all().count()

        for rec in answer_option:
            answer_list_obj = obj[0].ans.filter(id=rec['id'])
            if answer_list_obj:
                ans_data = answer_list_obj.update(answer=rec['answer'])
            else:
                counter = total_answer_data + 1
                create_ans_data = answer.objects.create(answer=rec['answer'],postion = counter)
                obj[0].ans.add(create_ans_data.id)
                counter += 1

        messages.success(request, "Record Updated Successfully.")

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))

    return redirect('/school/edit_fill_in_the_blank/'+str(question_id))

def edit_match_the_pair(request,rec_id):

    try:
        perodic_scheme = perodic_assesment_data.objects.all()

        subject_recs = subjects.objects.all()

        question_obj = question.objects.get(id=rec_id)
        sub_id = question_obj.perodic_question_subject
        selected_subject = subjects.objects.get(id=sub_id.id)

        perodic_strand_obj = perodic_strand.objects.get(id=question_obj.perodic_question_strand.id)
        selected_strand = subject_strands.objects.get(id=perodic_strand_obj.strand_name)

        substrand_obj = perodic_sub_strand.objects.get(id=question_obj.perodic_question_substrand.id)

        selected_substrand = subject_substrands.objects.get(id=substrand_obj.sub_strand_name)

        selected_topic = perodic_topics.objects.get(id=question_obj.perodic_question_topic.id)

        topic_check = question_obj.perodic_question_topic.topic
        topic_data = perodic_topics.objects.filter(topic=topic_check)[0]

        all_perodic_scheme = perodic_assesment_data.objects.all()
        perodic_scheme = perodic_assesment_data.objects.filter(perodic_strand=perodic_strand_obj,
                                                               perodic_subject=selected_subject.id)


        total_answer_response = question_obj.question_list_option.all().count()

        ans_data = question_obj.ans.all().order_by("postion")
        question_list = question_obj.question_list_option.all()



    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))

    return render(request, "Edit_match_the_pair.html",{'question_obj':question_obj,'perodic_strand_obj':perodic_strand_obj,'question_list':question_list,'ans_data':ans_data,'subject_recs':subject_recs,'topic_data':topic_data,'selected_subject':selected_subject,'selected_topic':selected_topic,'perodic_scheme':perodic_scheme,'substrand_obj':substrand_obj,'selected_substrand':selected_substrand,'selected_strand':selected_strand ,'all_perodic_scheme':all_perodic_scheme,'total_answer_response':total_answer_response})

def update_match_order_list(request):
    try:
        match_the_pair_question = json.loads(request.POST.get('match_order_list_question'))

        answer_option = json.loads(request.POST.get('answer_option_data'))

        title_name = request.POST.get('title_name')
        instruction_name = request.POST.get('instruction_name')
        question_name = request.POST.get('question_name')
        question_mark_name = request.POST.get('question_mark_name')
        perodic_scheme = request.POST.get('perodic_scheme')
        subject_name = request.POST.get('subject_name')
        strand_name = request.POST.get('strand_name')
        sub_strand = request.POST.get('sub_strand')
        topic_name = request.POST.get('topic_name')
        question_id =request.POST.get('rec_id')

        obj =  question.objects.filter(id=question_id )

        if request.FILES:
            print('files')
            Photo = request.FILES['upload_question_image_name']
            dirname = datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
            filename = "%s_%s.%s" % (request.POST.get('question_name')+("_question_id - ")+str(question_id), dirname, 'png')
            raw_file_path_and_name = os.path.join('doc', filename)
            image_result = open(settings.MEDIA_ROOT + raw_file_path_and_name, 'wb')
            image_result.write(Photo.read())
            obj.update(question_photo = raw_file_path_and_name)


        obj.update(title=title_name, instruction=instruction_name, question_name=question_name, mark=question_mark_name,
                            perodic_question_subject_id=subject_name, perodic_question_topic_id=topic_name,
                            perodic_question_strand_id=strand_name, perodic_question_substrand_id=sub_strand)

        for list in match_the_pair_question:

            option_list_obj = obj[0].question_list_option.filter(id=list['id'])
            if option_list_obj:
                list_data = option_list_obj.update(questions=list['question'],answer_response=list['possible_response'])
            else:
                created_data = question_list.objects.create(questions=list['question'], answer_response=list['possible_response'])
                obj[0].question_list_option.add(created_data.id)



        for rec in answer_option:
            position = (rec.values())[1]
            answer_list_obj = obj[0].ans.filter(id=rec['id'])
            if answer_list_obj:
                ans_data = answer_list_obj.update(answer=rec['answer'],postion = position)
                # count += 1
            else:
                # counter = total_answer_data + 1
                create_ans_data = answer.objects.create(answer=rec['answer'],postion = position)
                obj[0].ans.add(create_ans_data.id)
                # counter += 1

        messages.success(request, "Record Updated Successfully.")

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))

    return redirect('/school/edit_match_the_pair/'+str(question_id))
    # return redirect('/school/search_question/')


def review_assessment_page(request):
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

        all_classes = current_academic_year_mapped_classes(current_academic_year, request.user)
        all_academicyr_recs = academic_year.objects.all()
        return render(request,'review_assessment_page.html',{'ClassDetails': all_classes, "year_list": all_academicyr_recs,'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/perodic_data_subtile/')

def review_assessment_student_list(request):
    try:
        student_list = []
        selected_year_id = request.POST.get('year_name')
        selected_class_id = request.POST.get('class_name')
        selected_section_id = request.POST.get('section_name')
        selected_assessment_id = request.POST.get('assessment_name')
        academic_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,class_name=selected_class_id,section_name=selected_section_id)
        student_obj = student_details.objects.filter(academic_class_section = academic_class_section_obj.id)
        for rec in student_obj:
            raw_dict = {}
            raw_dict['student_name'] = rec.get_all_name()
            raw_dict['class_name'] = rec.academic_class_section.class_name
            raw_dict['section_name'] = rec.academic_class_section.section_name
            raw_dict['year_name'] = rec.academic_class_section.year_name
            raw_dict['student_code'] = rec.student_code
            raw_dict['id'] = rec.id
            student_list.append(raw_dict)

        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        all_classes = current_academic_year_mapped_classes(current_academic_year, request.user)
        all_academicyr_recs = academic_year.objects.all()

        selected_class_name = class_details.objects.get(id=selected_class_id)
        selected_section_name = sections.objects.get(id=selected_section_id)
        selected_assessment_name = assesment.objects.get(id=selected_assessment_id)


    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/perodic_data_subtile/')


    return render(request, 'review_assessment_page.html',
                  {'ClassDetails': all_classes,
                   "year_list": all_academicyr_recs,
                   'selected_year_name': current_academic_year,
                   'Selected_Year_Id': selected_year_id,
                   'student_list':student_list,
                   'selected_class_name':selected_class_name,
                   'selected_section_name':selected_section_name,
                   'selected_assessment_name':selected_assessment_name})

def get_section_class_assigned_assessment(request):
    modelDict = []
    class_name = request.GET.get('class_name', None)
    academic_year = request.GET.get('academic_year', None)
    section_name = request.GET.get('section_name', None)
    academic_class_section_mapping_rec = perodic_question_academic_class_section_mapping.objects.filter(academic_class_section_mapping__class_name=class_name,academic_class_section_mapping__year_name=academic_year,academic_class_section_mapping__section_name=section_name)

    for rec in academic_class_section_mapping_rec:
        raw_dict = {}
        raw_dict['id']= rec.assesment.id
        raw_dict['assessment_name']= rec.assesment.title
        modelDict.append(raw_dict)
    return JsonResponse(modelDict, safe=False)


def check_student_assessment_page(request):
    try:
        student_answer_data = []
        assessment_id = request.POST.get('assessment_id')
        student_id = request.POST.get('student_id')
        student_rec = student_answer.objects.filter(student_id=student_id,assessment=assessment_id)
        for rec in student_rec:
            raw_dict = {}
            temp_list = []

            raw_dict['question_type'] = rec.particular_question.question_type
            raw_dict['question_title'] = rec.particular_question.question_type
            raw_dict['question_name'] = rec.particular_question.question_name
            raw_dict['total_mark'] = rec.particular_question.mark
            for i in rec.student_given_answer.all():
                temp_list.append(i.ans_by_student)
                raw_dict['answer_by_student'] = temp_list

            student_answer_data.append(raw_dict)


    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))


    return render(request, 'check_student_assessment_page.html',{'student_answer_data':student_answer_data})


def view_all_question_subtiles(request):
    try:
        question_list = []
        if request.POST :
            assessment_id = request.POST.get('assessment_id')
            student_id = request.POST.get('student_id')
            question_type = request.POST.get('question_type')
            request.session['data'] = request.POST
        else:
            assessment_id = request.session['data'].get('assessment_id')
            student_id = request.session['data'].get('student_id')
            question_type = request.session['data'].get('question_type')


        assessment_obj = assesment.objects.get(id=assessment_id)

        question_obj = student_answer.objects.filter(student_id=student_id,assessment=assessment_obj.id, particular_question__question_type=question_type)

        for rec in question_obj:
            raw_dict = {}
            raw_dict['question_type'] = rec.particular_question.question_type
            raw_dict['question_title'] = rec.particular_question.question_type
            raw_dict['question_name'] = rec.particular_question.question_name
            raw_dict['total_mark'] = rec.particular_question.mark
            raw_dict['id'] = rec.id
            question_list.append(raw_dict)

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/perodic_data_subtile/')

    return render(request,'view_all_question_subtile.html',{'question_list':question_list,'assessment_obj':assessment_obj,'student_id':student_id})



def view_mcq_answer(request,rec_id):
    try:
        student_answer_obj = student_answer.objects.get(id=rec_id)
        correct_response = student_answer_obj.particular_question.ans.get(correct_flag=True)
        answer_by_student = student_answer_obj.student_given_answer.get()

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/view_all_question_subtiles/')

    return render(request, 'view_mcq_answer.html',{'student_answer_obj': student_answer_obj,'correct_response':correct_response,'answer_by_student':answer_by_student})



def save_student_mark_per_question(request):
    try:
        student_answer_id = request.POST.get('student_answer_id')
        mark_obtain = request.POST.get('mark_obtain')
        student_answer.objects.filter(id=student_answer_id).update(mark_obtain = mark_obtain )

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/view_all_question_subtiles/')

    return redirect('/school/view_all_question_subtiles/')


def view_multichoice_mcq_answer(request,rec_id):
    try:
        student_answer_obj = student_answer.objects.get(id=rec_id)
        multiple_answers = student_answer_obj.particular_question.ans.filter(correct_flag=True)
        multiple_answers_list = []
        options = student_answer_obj.particular_question.ans.all()
        for rec in multiple_answers:
            multiple_answers_list.append(rec.answer)

        answer_by_student = student_answer_obj.student_given_answer.all()

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/view_all_question_subtiles/')

    return render(request, 'view_multichoice_mcq_answer.html',{'student_answer_obj': student_answer_obj,'answer_by_student':answer_by_student,"options":options,'multiple_answers_list':multiple_answers_list})

def view_short_text_answer(request,rec_id):
    try:
        student_answer_obj = student_answer.objects.get(id=rec_id)
        correct_answer = student_answer_obj.particular_question.ans.get(correct_flag=True)

        answer_by_student = student_answer_obj.student_given_answer.get()

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/view_all_question_subtiles/')

    return render(request, 'view_short_text_answer.html',{'student_answer_obj': student_answer_obj,'answer_by_student':answer_by_student,'correct_answer':correct_answer})


def view_fill_in_blank_answer(request,rec_id):
    try:
        student_answer_obj = student_answer.objects.get(id=rec_id)
        correct_answer = student_answer_obj.particular_question.ans.filter()

        answer_by_student = student_answer_obj.student_given_answer.filter()

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/view_all_question_subtiles/')

    return render(request, 'view_fill_in_blank_answer.html',{'student_answer_obj': student_answer_obj,'answer_by_student':answer_by_student,'correct_answer':correct_answer})

def view_order_list_answer(request,rec_id):
    try:
        student_answer_obj = student_answer.objects.get(id=rec_id)
        correct_answer = student_answer_obj.particular_question.ans.all().order_by("postion")
        answer_by_student = student_answer_obj.student_given_answer.all().order_by("postion_by_student")

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/view_all_question_subtiles/')

    return render(request, 'view_order_list_answer.html', {'student_answer_obj': student_answer_obj, 'answer_by_student':answer_by_student, 'correct_answer':correct_answer})

def match_the_pair_answer(request,rec_id):

    try:
        student_answer_obj = student_answer.objects.get(id=rec_id)
        correct_answer = student_answer_obj.particular_question.ans.all().order_by("postion")
        answer_by_student = student_answer_obj.student_given_answer.all().order_by("postion_by_student")
        question_list = student_answer_obj.particular_question.question_list_option.all()


    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/view_all_question_subtiles/')

    return render(request, 'match_the_pair_answer.html', {'student_answer_obj': student_answer_obj, 'answer_by_student':answer_by_student, 'correct_answer':correct_answer,'question_list':question_list})


def auto_generate_marks(request):

    if request.POST:
        assessment_id = request.POST.get('assessment_id')
        student_id = request.POST.get('student_id')
        request.session['data'] = request.POST
    else:
        assessment_id = request.session['data'].get('assessment_id')
        student_id = request.session['data'].get('student_id')


    student_rec = student_answer.objects.filter(student_id=student_id, assessment=assessment_id)

    for rec in student_rec:

        if str(rec.particular_question.question_type) == 'MCQ':
            try:
                if rec.student_given_answer.all():

                    if str(rec.student_given_answer.get().ans_by_student) == str(rec.particular_question.ans.get(correct_flag=True).answer):
                        rec.mark_obtain = rec.particular_question.mark

                        rec.save()
                    else:
                        rec.mark_obtain = 0
                        rec.save()

                else:
                    rec.mark_obtain = 0
                    rec.save()

            except:
                pass

        if str(rec.particular_question.question_type) == 'MCQ multiple response':
            try:

                student_answer_count = rec.student_given_answer.all().count()
                correct_answer_count = rec.particular_question.ans.filter(correct_flag=True).count()

                if student_answer_count == correct_answer_count:
                    student_answer_list = []

                    for obj in rec.student_given_answer.all():
                        student_answer_list.append(str(obj.ans_by_student))
                    correct_answer_list = []

                    for i in rec.particular_question.ans.filter(correct_flag=True):
                        correct_answer_list.append(str(i.answer))

                    if set(correct_answer_list) == set(student_answer_list) :
                        rec.mark_obtain = rec.particular_question.mark
                        rec.save()

                    else:
                        rec.mark_obtain = 0
                        rec.save()

                else:
                    rec.mark_obtain = 0
                    rec.save()
            except:
                pass

        if str(rec.particular_question.question_type) == 'Order List Question' :
            try:
                if rec.student_given_answer.all():

                    student_answer_list = []
                    for obj in sorted(rec.student_given_answer.all(), key=lambda t: t.postion_by_student) :#sort querset using position
                        student_answer_list.append(str(obj.ans_by_student))

                    correct_answer_list = []
                    for i in sorted(rec.particular_question.ans.all(), key=lambda t: t.postion) :#sort querset using position
                        correct_answer_list.append(str(i.answer))

                    check_answer = compare_by_element(correct_answer_list,student_answer_list) #compare two list element_by_element

                    if check_answer :
                        rec.mark_obtain = rec.particular_question.mark
                        rec.save()
                    else:
                        rec.mark_obtain = 0
                        rec.save()
                else:
                    rec.mark_obtain = 0
                    rec.save()

            except:
                pass

        if str(rec.particular_question.question_type) == 'Match Order List':
            try:
                student_answer_list = []
                if rec.student_given_answer.all() :
                    for obj in sorted(rec.student_given_answer.all(),key=lambda t: t.postion_by_student):  # sort querset using position
                        student_answer_list.append(str(obj.ans_by_student))

                    correct_answer_list = []

                    for i in sorted(rec.particular_question.ans.all(), key=lambda t: t.postion):  # sort querset using position
                        correct_answer_list.append(str(i.answer))

                    check_answer =  compare_by_element(correct_answer_list, student_answer_list)  # compare two list element_by_element

                    if check_answer:
                        rec.mark_obtain = rec.particular_question.mark
                        rec.save()
                    else:
                        rec.mark_obtain = 0
                        rec.save()
                else:
                    rec.mark_obtain = 0
                    rec.save()
            except:
                pass


    try:
        mcq_question = student_answer.objects.filter(student_id=student_id, assessment=assessment_id, particular_question__question_type ="MCQ")
        mark_in_mcq = 0
        total_mcq_mark = 0
        if mcq_question:
            for rec in mcq_question:
                mark_in_mcq += float(rec.mark_obtain)
                total_mcq_mark += float(rec.particular_question.mark)

        mcq_multiresponse = student_answer.objects.filter(student_id=student_id, assessment=assessment_id,particular_question__question_type="MCQ multiple response")
        multiresponse_obtain_marks = 0
        total_multiresponse_mark = 0
        if mcq_multiresponse:
            for rec in mcq_multiresponse:
                multiresponse_obtain_marks += float(rec.mark_obtain)
                total_multiresponse_mark +=float(rec.particular_question.mark)

        order_list_question = student_answer.objects.filter(student_id=student_id, assessment=assessment_id,particular_question__question_type="Order List Question")
        order_list_obtain_marks = 0
        order_list_total_marks = 0
        if order_list_question:
            for rec in order_list_question:
                order_list_obtain_marks += float(rec.mark_obtain)
                order_list_total_marks += float(rec.particular_question.mark)

        match_order_list_question = student_answer.objects.filter(student_id=student_id, assessment=assessment_id,particular_question__question_type="Match Order List")
        match_order_list_obtain_marks = 0
        match__order_list_total_marks = 0
        if match_order_list_question:
            for rec in match_order_list_question:
                match_order_list_obtain_marks += float(rec.mark_obtain)
                match__order_list_total_marks += float(rec.particular_question.mark)


        fill_in_blank_question = student_answer.objects.filter(student_id=student_id, assessment=assessment_id,particular_question__question_type="Fill In The Blank")
        fill_in_blank_obtain_marks = 0
        fill_in_blank_total_marks = 0
        question_uncheck_fill = False

        if fill_in_blank_question:
            for rec in fill_in_blank_question :
                if rec.mark_obtain:
                    fill_in_blank_obtain_marks += float(rec.mark_obtain)
                    fill_in_blank_total_marks += float(rec.particular_question.mark)


                else:
                    question_uncheck_fill = True


        short_text_question = student_answer.objects.filter(student_id=student_id, assessment=assessment_id,particular_question__question_type="Short Text")
        short_text_obtain_marks = 0
        short_text_total_marks = 0

        question_uncheck_short = False

        if short_text_question:
            for rec in short_text_question:
                if rec.mark_obtain:
                    short_text_obtain_marks += float(rec.mark_obtain)
                    short_text_total_marks += float(rec.particular_question.mark)

                else:
                    question_uncheck_short = True


    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/view_all_question_subtiles/')

    return render(request, 'student_assessment_subtile.html',
                  {'assessment_id': assessment_id, 'student_id': student_id,
                   'mark_in_mcq': mark_in_mcq, "total_mcq_mark": total_mcq_mark,
                   'total_multiresponse_mark': total_multiresponse_mark,
                   'multiresponse_obtain_marks': multiresponse_obtain_marks,
                   'order_list_obtain_marks': order_list_obtain_marks,
                   'order_list_total_marks': order_list_total_marks,
                   'match__order_list_total_marks': match__order_list_total_marks,
                   'match_order_list_obtain_marks': match_order_list_obtain_marks,
                   'short_text_total_marks': short_text_total_marks,

                   'short_text_obtain_marks': short_text_obtain_marks,

                   'fill_in_blank_obtain_marks': fill_in_blank_obtain_marks,
                   'fill_in_blank_total_marks': fill_in_blank_total_marks,
                   'question_uncheck_short': question_uncheck_short,
                   'question_uncheck_fill': question_uncheck_fill,

                   })



def import_mcq_question_tile(request):

    return render(request, 'import_mcq_questions.html')

def import_mcq_questions(request):
    try:
        import_flag = False
        file_recs = request.FILES['excel'].get_records()
        for file_rec in file_recs:
            try:
                if file_rec['question']!="":

                    question_obj = question.objects.create(question_name=file_rec['question'].lstrip(),
                                                           perodic_question_strand_id=25,
                                                           title=file_rec['keyword'],
                                                           instruction=file_rec['keyword'],

                                                           question_type="MCQ",mark=0,
                                                           perodic_question_subject_id=8,
                                                           perodic_question_substrand_id=87,
                                                           perodic_question_topic_id=59, )

                    correct_answer = ((file_rec['answer']).encode('-utf8')).strip()

                    answer_dict = {'a':1, 'b':2, 'c': 3, 'd': 4}

                    count = 1

                    for rec in range(4):
                        try:
                            option = ((file_rec['Option_' + str(count)]).encode('-utf8')).strip()
                            option = option.replace('\n', ' ')
                        except:
                            pass
                        if option!="":
                            if option.lower() == str(correct_answer.lower()) or answer_dict.get(correct_answer.lower(),False) == count:
                                create_option = answer.objects.create(answer=option, correct_flag=True)

                            else:
                                create_option = answer.objects.create(answer=option)

                            question_obj.ans.add(create_option.id)

                        count+=1
                        import_flag = True

            except Exception as e:
                messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
                pass

        if import_flag :
            messages.success(request, "Record saved")


    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))

    return redirect('/school/import_mcq_question_tile/')

def report_card(request):
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

        all_classes = current_academic_year_mapped_classes(current_academic_year, request.user)
        all_academicyr_recs = academic_year.objects.all()
        return render(request,'report_card_page.html',{'ClassDetails': all_classes, "year_list": all_academicyr_recs,'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})
        # return render(request,'report_card_page.html')

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/perodic_data_subtile/')

def report_card_student_list(request):
    try:
        student_list = []
        if request.POST:
            selected_year_id = request.POST.get('year_name')
            selected_class_id = request.POST.get('class_name')
            selected_section_id = request.POST.get('section_name')
            selected_assessment_id = request.POST.get('assessment_name')
        else:
            selected_year_id = request.session['data'].get('year_name')
            selected_class_id = request.session['data'].get('class_name')
            selected_section_id = request.session['data'].get('section_name')
            selected_assessment_id = request.session['data'].get('assessment_name')

        academic_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,class_name=selected_class_id,section_name=selected_section_id)
        student_obj = student_details.objects.filter(academic_class_section = academic_class_section_obj.id)
        for rec in student_obj:
            raw_dict = {}
            raw_dict['student_name'] = rec.get_all_name()
            raw_dict['class_name'] = rec.academic_class_section.class_name
            raw_dict['section_name'] = rec.academic_class_section.section_name
            raw_dict['year_name'] = rec.academic_class_section.year_name
            raw_dict['student_code'] = rec.student_code
            raw_dict['id'] = rec.id
            student_list.append(raw_dict)

        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        all_classes = current_academic_year_mapped_classes(current_academic_year, request.user)
        all_academicyr_recs = academic_year.objects.all()

        selected_class_name = class_details.objects.get(id=selected_class_id)
        selected_section_name = sections.objects.get(id=selected_section_id)
        selected_assessment_name = assesment.objects.get(id=selected_assessment_id)


    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/perodic_data_subtile/')

    return render(request, 'report_card_page.html',
                  {'ClassDetails': all_classes,
                   "year_list": all_academicyr_recs,
                   'selected_year_name': current_academic_year,
                   'Selected_Year_Id': selected_year_id,
                   'student_list': student_list,
                   'selected_class_name': selected_class_name,
                   'selected_section_name': selected_section_name,
                   'selected_assessment_name': selected_assessment_name})

def generate_report_card(request):
    try:
        if request.POST:
            selected_assessment_id = request.POST.get('assessment_id')
            selected_student = request.POST.get('student_id')
            request.session['data'] = request.POST
        else:
            selected_assessment_id = request.session['data'].get('assessment_id')
            selected_student = request.session['data'].get('student_id')

        student_rec = student_details.objects.get(id=selected_student)
        current_academic_year = academic_year.objects.get(current_academic_year=1)

        # selected_assessment_name = assesment.objects.filter(id=selected_assessment_id)
        selected_assessment_name = assesment.objects.get(id=selected_assessment_id)
        subject_data = selected_assessment_name.assessment_question.all()

        for rec in subject_data:
            subject = rec.perodic_question_subject.subject_name
            topic = rec.perodic_question_topic.topic

        student_ans_data = student_answer.objects.filter(student_id=selected_student, assessment=selected_assessment_id)
        if student_ans_data.exists():
            student_obtain_marks = 0
            Total_assessment_marks = 0
            for rec in student_ans_data:
                if rec.mark_obtain:
                    student_obtain_marks += float(rec.mark_obtain)

            for data in student_ans_data:
                Total_assessment_marks += float(data.particular_question.mark)

            percentage = (student_obtain_marks / Total_assessment_marks) * 100;

            return render(request,'generate_report_card.html',{'percentage':percentage,'student_rec':student_rec,'current_academic_year':current_academic_year,'selected_assessment_name':selected_assessment_name,'subject':subject,'topic':topic,'student_obtain_marks':student_obtain_marks,'Total_assessment_marks':Total_assessment_marks})

        else:
            messages.success(request, "A student has not attended this assessment")
            return redirect('/school/perodic_data_subtile/')


    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/perodic_data_subtile/')



def analytical_report_card(request):
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

        all_classes = current_academic_year_mapped_classes(current_academic_year, request.user)
        all_academicyr_recs = academic_year.objects.all()
        return render(request, 'analytical_report_card.html', {'ClassDetails': all_classes, "year_list": all_academicyr_recs,
                                                         'selected_year_name': current_academic_year,
                                                         'Selected_Year_Id': selected_year_id})

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/perodic_data_subtile/')

def analytical_report_card_list(request):
    try:
       
        if request.POST:
            selected_year_id = request.POST.get('year_name')
            selected_class_id = request.POST.get('class_name')
            selected_section_id = request.POST.get('section_name')
            selected_assessment_id = request.POST.get('assessment_name')
        else:
            selected_year_id = request.session['data'].get('year_name')
            selected_class_id = request.session['data'].get('class_name')
            selected_section_id = request.session['data'].get('section_name')
            selected_assessment_id = request.session['data'].get('assessment_name')

        academic_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                                class_name=selected_class_id,
                                                                                section_name=selected_section_id)

        student_obj = student_details.objects.filter(academic_class_section=academic_class_section_obj.id)
        if student_obj.exists():
            total_stud = student_obj.count()
            class_name = student_obj[0].academic_class_section.class_name
            section_name = student_obj[0].academic_class_section.section_name

            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id
            all_classes = current_academic_year_mapped_classes(current_academic_year, request.user)
            all_academicyr_recs = academic_year.objects.all()

            selected_class_name = class_details.objects.get(id=selected_class_id)
            selected_section_name = sections.objects.get(id=selected_section_id)
            selected_assessment_name = assesment.objects.get(id=selected_assessment_id)
            assessment_total_marks = selected_assessment_name.total_mark

            present_student = student_grade.objects.filter(student_id__academic_class_section=academic_class_section_obj.id).count()
            absent_student = total_stud - present_student

            stud_data_obj = student_grade.objects.filter(student_id__academic_class_section = academic_class_section_obj.id)
            # stud_data = student_grade.objects.all()
            if stud_data_obj.exists():

                stud_data = []
                all_stud_list = []
                for rec in stud_data_obj:
                    raw_dict = {}
                    raw_dict['student_mark'] = rec.grade
                    raw_dict['id'] = rec.student_id_id
                    raw_dict['total_assessment_mrk'] = rec.assessment
                    stud_data.append(raw_dict)

                for rec in student_obj:
                    raw_dict1 = {}
                    raw_dict1['id'] = rec.id
                    raw_dict1['student_code'] = rec.student_code
                    all_stud_list.append(raw_dict1)

                datalist = []
                trylist = []

                for x in all_stud_list:
                    flag = False

                    for y in stud_data:
                        if x['id'] == y['id']:
                            flag = True

                            data = dict(x)
                            data.update(y)
                            datalist.append(data)
                            break

                    if not flag:
                        x['student_mark'] = 00
                        x['total_assessment_mrk'] = y['total_assessment_mrk']
                        trylist.append(x)

                datalist.extend(trylist)

                thirty_five = 0.35 * int(assessment_total_marks)
                fifty_five = 0.55* int(assessment_total_marks)
                seventy_five = 0.75 * int(assessment_total_marks)

                between_thirty_five_to_fifty_five = []
                between_fifty_five_to_seventy_five = []
                Above_seventy_five = []
                below_thirty_five=[]

                # for rec in stud_data:
                for rec in datalist:
                    total_mrk = rec['total_assessment_mrk'].total_mark
                    total_marks = (int(total_mrk) * 100) / 100

                    grade = rec['student_mark']
                    total_grade = (float(grade) * 100) / 100

                    if total_grade < thirty_five:
                        student_id = rec['id']
                        below_thirty_five.append(student_id)

                    if total_grade >= thirty_five and total_grade <= fifty_five:
                        studdent_id = rec['id']
                        between_thirty_five_to_fifty_five.append(student_id)

                    if total_grade > fifty_five and total_grade <= seventy_five:
                        student_id = rec['id']
                        between_fifty_five_to_seventy_five.append(student_id)

                    if total_grade > seventy_five:
                        student_id = rec['id']
                        Above_seventy_five.append(student_id)

                    stud_list1 = len(below_thirty_five)
                    fail_stud1 = decimal.Decimal((float(stud_list1) * 100) / float(total_stud))
                    fail_stud = (round(fail_stud1, 2))

                    stud_list2 = len(between_thirty_five_to_fifty_five)
                    stud_list = decimal.Decimal((float(stud_list2) * 100) / float(total_stud))
                    btwn_thirty_five_to_fifty_five = (round(stud_list, 2))

                    stud_list3 = len(between_fifty_five_to_seventy_five)
                    stud_list_data = decimal.Decimal((float(stud_list3) * 100) / float(total_stud))
                    btwn_fifty_five_to_seventy_five = (round(stud_list_data, 2))

                    stud_list6 = len(Above_seventy_five)
                    stud_list_data3 = decimal.Decimal((float(stud_list6) * 100) / float(total_stud))
                    stud_list_above_seventy_five = (round(stud_list_data3, 2))

            else:
                messages.success(request, "An Assessment not taken")
                return redirect('/school/ReportCardSubTile/')

        else:
            messages.success(request, "A student does not exist")
            return redirect('/school/ReportCardSubTile/')





    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/ReportCardSubTile/')

    return render(request, 'analytical_report_card.html',
                  {'ClassDetails': all_classes,'btwn_fifty_five_to_seventy_five':btwn_fifty_five_to_seventy_five,
                   "year_list": all_academicyr_recs,'stud_list_above_seventy_five':stud_list_above_seventy_five,
                   'selected_year_name': current_academic_year,'btwn_thirty_five_to_fifty_five':btwn_thirty_five_to_fifty_five,
                   'Selected_Year_Id': selected_year_id,'absent_student':absent_student,
                   'total_stud':total_stud,'class_name':class_name,'section_name':section_name,
                   'selected_class_name': selected_class_name,'present_student':present_student,
                   'selected_section_name': selected_section_name,'fail_stud':fail_stud,
                   'selected_assessment_name': selected_assessment_name,'assessment_total_marks':assessment_total_marks})


def submit_student_assessment(request):
    try:
        if request.POST:
            assessment_id = request.POST.get('assessment_id')
            student_id = request.POST.get('student_id')
            request.session['data'] = request.POST
        else:
            assessment_id = request.session['data'].get('assessment_id')
            student_id = request.session['data'].get('student_id')

        stud_id = student_details.objects.get(id = student_id)
        assessment_id_data = assesment.objects.get(id = assessment_id)


        student_ans_data = student_answer.objects.filter(student_id=student_id, assessment=assessment_id)
        if student_ans_data.exists():

            student_obtain_marks = 0
            Total_assessment_marks = 0
            for rec in student_ans_data:
                if rec.mark_obtain:
                    student_obtain_marks += float(rec.mark_obtain)

            for data in student_ans_data:
                Total_assessment_marks += float(data.particular_question.mark)

            student_grade_obj = student_grade(grade=student_obtain_marks,outof=Total_assessment_marks)
            student_grade_obj.save()

            student_grade_obj.student_id= stud_id
            student_grade_obj.save()

            student_grade_obj.assessment_id = assessment_id_data
            student_grade_obj.save()

            messages.success(request, "Record updated successfully")

        else:
            messages.success(request, "Can not submit empty data")
            return redirect('/school/auto_generate_marks/')

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/perodic_data_subtile/')


    return redirect('/school/auto_generate_marks/')

def ReportCardSubTile(request):
    return render(request, "report_card_sub_tile.html")

def assessment_report_card(request):
    try:
        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

        all_classes = current_academic_year_mapped_classes(current_academic_year, request.user)
        all_academicyr_recs = academic_year.objects.all()
        return render(request,'assessment_report_card.html',{'ClassDetails': all_classes, "year_list": all_academicyr_recs,'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/perodic_data_subtile/')

def assement_report_card_student_list(request):
    try:
        stud_list=[]
        student_list = []
        if request.POST:
            selected_year_id = request.POST.get('year_name')
            selected_class_id = request.POST.get('class_name')
            selected_section_id = request.POST.get('section_name')
            selected_assessment_id = request.POST.get('assessment_name')
        else:
            selected_year_id = request.session['data'].get('year_name')
            selected_class_id = request.session['data'].get('class_name')
            selected_section_id = request.session['data'].get('section_name')
            selected_assessment_id = request.session['data'].get('assessment_name')

        academic_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                                class_name=selected_class_id,
                                                                                section_name=selected_section_id)

        student_obj = student_details.objects.filter(academic_class_section=academic_class_section_obj.id)
        for rec in student_obj:
            raw_dict1 = {}
            raw_dict1['student_name'] = rec.get_all_name()
            raw_dict1['class_name'] = rec.academic_class_section.class_name
            raw_dict1['section_name'] = rec.academic_class_section.section_name
            raw_dict1['year_name'] = rec.academic_class_section.year_name
            raw_dict1['student_code'] = rec.student_code
            raw_dict1['id'] = rec.id
            stud_list.append(raw_dict1)

        selected_assessment_name = assesment.objects.get(id=selected_assessment_id)
        assessment_total_marks = selected_assessment_name.total_mark

        thirty_five = 0.35 * int(assessment_total_marks)
        fifty_five = 0.55 * int(assessment_total_marks)
        seventy_five = 0.75 * int(assessment_total_marks)

        student_data_obj = student_grade.objects.filter(student_id__academic_class_section = academic_class_section_obj.id)
        for rec in student_data_obj:
            raw_dict = {}

            raw_dict['id'] = rec.student_id_id
            raw_dict['student_mark'] = rec.grade

            grade = rec.grade

            total_grade = (float(grade) * 100) / 100

            if total_grade < thirty_five:
                if not ['student_grade_info'] in student_list:
                    raw_dict['student_grade_info'] = 'D'
                    student_list.append(raw_dict)
                else:
                    raw_dict['student_grade_info'] = ''
                    student_list.append(raw_dict)

            if total_grade >= thirty_five and total_grade <= fifty_five:
                if not ['student_grade_info'] in student_list:
                    raw_dict['student_grade_info'] = 'C'
                    student_list.append(raw_dict)
                else:
                    raw_dict['student_grade_info'] = ''
                    student_list.append(raw_dict)

            if total_grade > fifty_five and total_grade <= seventy_five:
                if not ['student_grade_info'] in student_list:
                    raw_dict['student_grade_info'] = 'B'
                    student_list.append(raw_dict)
                else:
                    raw_dict['student_grade_info'] = ''
                    student_list.append(raw_dict)

            if total_grade > seventy_five:
                if not ['student_grade_info'] in student_list:
                    raw_dict['student_grade_info'] = 'A'
                    student_list.append(raw_dict)
                else:
                    raw_dict['student_grade_info'] = ''
                    student_list.append(raw_dict)

        datalist = []
        trylist = []

        for x in stud_list:
            flag = False

            for y in student_list:
                if x['id'] == y['id']:
                    flag = True

                    data = dict(x)
                    data.update(y)
                    datalist.append(data)
                    break

            if not flag:
                x['student_mark'] = 00.0
                x['student_grade_info']='Absent'
                trylist.append(x)

        datalist.extend(trylist)

        temp = []
        for obj in sorted(datalist, key=lambda k: k['student_mark'], reverse=True):
            temp.append(obj)

        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id
        all_classes = current_academic_year_mapped_classes(current_academic_year, request.user)
        all_academicyr_recs = academic_year.objects.all()

        selected_class_name = class_details.objects.get(id=selected_class_id)
        selected_section_name = sections.objects.get(id=selected_section_id)
        selected_assessment_name = assesment.objects.get(id=selected_assessment_id)

    except Exception as e:
        messages.warning(request, "Some Error Occurred. Please Try again." + str(e))
        return redirect('/school/ReportCardSubTile/')

    return render(request, 'assessment_report_card.html',
                  {'ClassDetails': all_classes,
                   "year_list": all_academicyr_recs,
                   'selected_year_name': current_academic_year,
                   'Selected_Year_Id': selected_year_id,
                   'temp': temp,
                   'selected_class_name': selected_class_name,
                   'selected_section_name': selected_section_name,
                   'selected_assessment_name': selected_assessment_name})


def export_all_questions(request):
    try:
        question_list = []
        for question_obj in question.objects.filter(question_type='MCQ'):
            rec_list = []

            rec_list.append(question_obj.title)
            rec_list.append(question_obj.instruction)
            rec_list.append(question_obj.question_name)

            count = 0
            for answer_options in question_obj.ans.all():
                count = count + 1
                if answer_options:
                    rec_list.append(answer_options.answer)
                else:
                    rec_list.append('')

            if count != 4:
                for rec in range(4 - question_obj.ans.all().count()):
                    rec_list.append('')
            try:
                correct_answer  = question_obj.ans.get(correct_flag=True)
                rec_list.append(correct_answer.answer)
            except:
                correct_answer = ''
                rec_list.append(correct_answer)


            selected_topic = perodic_topics.objects.get(id=question_obj.perodic_question_topic.id)
            rec_list.append(selected_topic.id)

            perodic_strand_obj = perodic_strand.objects.get(id=question_obj.perodic_question_strand.id)

            selected_strand = subject_strands.objects.get(id=perodic_strand_obj.strand_name)
            rec_list.append(selected_strand.id)

            substrand_obj = perodic_sub_strand.objects.get(id=question_obj.perodic_question_substrand.id)

            selected_substrand = subject_substrands.objects.get(id=substrand_obj.sub_strand_name)
            rec_list.append(selected_substrand.id)

            selected_subject = subjects.objects.get(id=question_obj.perodic_question_subject.id)
            rec_list.append(selected_subject.subject_name)



            try:
                perodic_scheme = perodic_assesment_data.objects.filter(perodic_strand=perodic_strand_obj,
                                                                   perodic_subject=selected_subject.id)[0]
                rec_list.append(perodic_scheme.perodic_scheme_name)
            except:
                perodic_scheme = ''
                rec_list.append(perodic_scheme)



            if question_obj.mark:
                rec_list.append(question_obj.mark)
            else:
                rec_list.append(0)

            question_list.append(rec_list)

        export_file_name = "question_list"
        column_names = ['title', 'instruction', 'question_name', 'answer_options_1', 'answer_options_2', 'answer_options_3', 'answer_options_4',
                        'correct_answer', 'selected_topic', 'selected_strand', 'selected_substrand', 'selected_subject', 'perodic_scheme_name', 'mark']
        return export_users_xls(export_file_name, column_names, question_list)

    except Exception as e:
        messages.warning(request, "Some Error Occurred... Please Try Again" + str(e))

    return redirect('/school/search_question/')



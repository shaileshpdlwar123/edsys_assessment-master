from django.contrib import admin
from assessment.models import perodic_topics,frequency_details,perodic_assesment_data,perodic_sub_strand,perodic_strand,perodic_scheme_frequecy,question,answer,assesment,perodic_question_academic_class_section_mapping,answer_by_student,student_answer,student_grade,question_list
from assessment.models import perodic_topics,frequency_details,perodic_assesment_data,perodic_sub_strand,perodic_strand,perodic_scheme_frequecy,question,answer,assesment,perodic_question_academic_class_section_mapping,answer_by_student,student_answer,student_grade,option_list
from registration.models import subjects,subject_strands,subject_substrands

admin.site.register(perodic_topics)
admin.site.register(perodic_assesment_data)
admin.site.register(frequency_details)
admin.site.register(perodic_sub_strand)
admin.site.register(perodic_strand)
admin.site.register(perodic_scheme_frequecy)
admin.site.register(question)
admin.site.register(option_list)
admin.site.register(answer)
admin.site.register(assesment)
admin.site.register(perodic_question_academic_class_section_mapping)
admin.site.register(answer_by_student)
admin.site.register(student_answer)
admin.site.register(student_grade)
admin.site.register(question_list)

# Register your models here.

from __future__ import unicode_literals

from django.apps import AppConfig


class assessmentConfig(AppConfig):
    name = 'assessment'

from __future__ import unicode_literals

import django
from django.conf import settings
from django.db import models
from datetime import datetime
from registration.models import *


# Create your models here.
class strikeoff_details(models.Model):
    deactivation_date = models.DateTimeField(default=datetime.today)
    reactivation_date = models.DateTimeField(null=True)
    reason = models.CharField(max_length=255)
     
    student = models.ForeignKey(student_details)

    class Meta:
        permissions = (
            ('can_view_strike_off', 'can view strike off'),
            ('can_view_strike_off_apply', 'can view strike off apply'),
            ('can_view_strike_off_list', 'can view strike off list'),
        )
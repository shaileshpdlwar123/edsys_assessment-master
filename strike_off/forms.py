from django import forms
# from registration.models import *
from django.contrib.admin.widgets import FilteredSelectMultiple
import itertools
from strike_off.models import *
from registration.forms import *


class StrikeOffDetailsForm(forms.ModelForm):
    reason = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off', 'maxlength':'255'}))
    deactivation_date = forms.DateField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off' ,'type':'date'}))
    reactivation_date = forms.DateField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off' ,'type':'date'}))  
     
    class Meta:
            model = strikeoff_details
            fields =['reason','deactivation_date','reactivation_date']

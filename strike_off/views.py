from django.shortcuts import render
from registration.models import *
from forms  import *
import ast
from django.contrib import messages
import xlwt
from django.utils.translation import ugettext
from wsgiref.util import FileWrapper
from django.http import HttpResponse
import django_excel as excel
from masters.utils import export_users_xls, todays_date,current_academic_year_mapped_classes,current_class_mapped_sections
from registration.decorators import user_permission_required, user_login_required
from time import gmtime, strftime
from django.shortcuts import render,redirect
from transfer_certificate .models import TcDetails
import json

@user_login_required
@user_permission_required('strike_off.can_view_strike_off', '/school/home/')
def StrikeOffMenu(request):    
     return render(request, "strikeoff_menu.html")

#This View is for loading class and sections to the first page of StrikeOff module
@user_login_required
@user_permission_required('strike_off.can_view_strike_off_apply', '/school/home/')
def StrikeoffStudent(request):
    all_classes = class_details.objects.all()
    # all_sections = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()



    year_class_section_object = academic_class_section_mapping.objects.all()
    if year_class_section_object:
        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id

            all_class_recs = current_academic_year_mapped_classes(selected_year_id, request.user)
            return render(request, "strikeoff_apply.html",
                          {'ClassDetails': all_class_recs,"year_list": all_academicyr_recs,
                           'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})
        except academic_year.DoesNotExist:
            messages.success(request, "Current Academic Year Not Found.")
            return render(request, 'strikeoff_menu.html')
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'strikeoff_menu.html')

def load_data(val_dict,request):
    strikeoff_form = StrikeOffDetailsForm()
    # print "strikeoff_form-->",strikeoff_form
    all_classes = class_details.objects.all()
    all_sections = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    selected_year_id = val_dict.get('year_name')  # It will be used in HTML page as a value of selected Class
    selected_year_rec = academic_year.objects.get(id=selected_year_id)
    selected_year_name = selected_year_rec.year_name

    selected_class_id = val_dict.get('class_name')  # It will be used in HTML page as a value of selected Class
    selected_class = class_details.objects.get(id=selected_class_id)
    selected_class_name = selected_class.class_name

    selected_section_id = val_dict.get('section_name')  # It will be used in HTML page as a value of selected Section
    selected_section = sections.objects.get(id=selected_section_id)
    selected_section_name = sections.section_name

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    max_date = current_academic_year.end_date

    #     filtering student records to pass HTML page
    year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                        section_name=selected_section_id,
                                                                        class_name=selected_class_id)
    filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=True,
                                                       joining_date__lte=todays_date())

    if not filtered_students:
        messages.warning(request, "No students found in selected class and section.")

    all_class_recs = current_academic_year_mapped_classes(selected_year_id, request.user)

    all_section_recs = current_class_mapped_sections(selected_year_rec, selected_class_id, request.user)

    # filtered_students = student_details.objects.filter(class_name_id = selected_class, section_id = selected_section, is_active = 1)

    #     Passing values to the HTML page through form_vals
    form_vals = {
        'StudentList': filtered_students,
        'Selected_Section_Id': selected_section_id,
        'SectName': selected_section_name,
        'section_name': selected_section,
        'Section': all_section_recs,
        'Selected_Class_Id': selected_class_id,
        'ClassName': selected_class_name,
        'class_name': selected_class,
        'ClassDetails': all_class_recs,
        'StrikeOff': strikeoff_form,

        'Selected_Year_Id': selected_year_id,
        'selected_year_name': selected_year_name,
        'selected_year': selected_year_rec,
        'year_list': all_academicyr_recs,
        'max_date': str(max_date),

    }
    return form_vals
#This view is for displaying all Students list whom we want to strikeoff
@user_login_required
def ViewStudentList(request):
    val_dict = request.POST
    form_vals = {}

    if request.method == 'POST':
        request.session['val_dict'] = val_dict
        form_vals = load_data(val_dict,request)
    else:
        form_vals = load_data(request.session.get('val_dict'),request)

    return render(request, "strikeoff_apply.html" , form_vals)

#     This view will strikeoff selected student from the filtered student list
@user_login_required
def SaveStrikeOffStudent(request):
    all_classes =  class_details.objects.all() 
    all_sections = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    selected_year_id = current_academic_year.id

    if request.method == 'POST':
        selected_strikeoff_id = json.loads(request.POST.get("strikeId"))
        reason = request.POST.get("reason") #Getting  strikeoff Reason
        date = request.POST.get("deactivation_date")  # Strikeoff Deactivation Date 
        
        if selected_strikeoff_id:
            # dict_data = ast.literal_eval(selected_strikeoff_id)
            try:
                for obj in selected_strikeoff_id:
                    student_name = student_details.objects.get(id = obj)
                    
                    if date != '' and reason != '':
                        student_details.objects.filter(id = obj).update(is_active = 0)
                        str_obj = strikeoff_details.objects.create(student = student_name, reason = reason)
                    else:
                        messages.warning(request, "Date And Reason both fields are mandatory")
                    
            except:
                messages.warning(request, "Some unexpected error occoured")
                # student_name = student_details.objects.get(id = dict_data)
                # if date != '' and reason != '':
                #     student_details.objects.filter(id = dict_data).update(is_active = 0)
                #     strikeoff_details.objects.create(student = student_name, reason = reason)
        
                # else:
                #     messages.success(request, "Date And Reason both fields are mandatory")
                  
        else:
            messages.success(request, "Please select the student")
    return redirect('/school/view_student_list/')
    # return render(request, "strikeoff_apply.html" , {'ClassDetails' : all_classes, 'Section' : all_sections,  "year_list" : all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id })

@user_login_required
@user_permission_required('strike_off.can_view_strike_off_list', '/school/home/')
def StrikeoffStudentList(request):
    # all_class_recs = class_details.objects.all()
    all_section_recs = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    year_class_section_object = academic_class_section_mapping.objects.all()
    if year_class_section_object:
        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id

            all_class_recs = current_academic_year_mapped_classes(selected_year_id, request.user)



            return render(request, "strikeoff_students_list.html",
                          {'ClassDetails': all_class_recs,
                           "year_list": all_academicyr_recs, 'selected_year_name': current_academic_year,
                           'Selected_Year_Id': selected_year_id})
        except academic_year.DoesNotExist:
            messages.success(request, "Current Academic Year Not Found.")
            return render(request, 'strikeoff_menu.html')
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'strikeoff_menu.html')

#     This view will show the strikedoff students and filter the selected students from the student list
def load_st_data(val_dict,request):
    strikeoff_form = StrikeOffDetailsForm()
    all_classes = class_details.objects.all()
    all_sections = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    selected_year_id = val_dict.get('year_name')  # It will be used in HTML page as a value of selected Class
    selected_year_rec = academic_year.objects.get(id=selected_year_id)
    selected_year_name = selected_year_rec.year_name

    selected_class_id = val_dict.get('class_name')  # It will be used in HTML page as a value of selected Class
    selected_class = class_details.objects.get(id=selected_class_id)
    selected_class_name = selected_class.class_name

    selected_section_id = val_dict.get(
        'section_name')  # It will be used in HTML page as a value of selected Section
    selected_section = sections.objects.get(id=selected_section_id)
    selected_section_name = sections.section_name

    #filtering student records to pass HTML page
    year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id, section_name=selected_section_id,class_name=selected_class_id)
    filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=False)
    stud_list = []
    for obj in filtered_students:
        tc_rec = TcDetails.objects.filter(student=obj,is_confirm=True)
        if tc_rec:
            pass
        else:
            stud_list.append(obj)

    if not filtered_students:
        messages.warning(request, "No striked-off students found in selected class and section.")

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    max_date = current_academic_year.start_date
    today_date= strftime("%Y-%m-%d", gmtime())
    all_class_recs = current_academic_year_mapped_classes(selected_year_id, request.user)
    all_section_recs = current_class_mapped_sections(selected_year_id, selected_class_id, request.user)

    #     Passing values to the HTML page through form_vals
    form_vals = {
        'StudentList': stud_list,
        'Selected_Section_Id': selected_section_id,
        'SectName': selected_section_name,
        'section_name': selected_section,
        'Section': all_section_recs,
        'Selected_Class_Id': selected_class_id,
        'ClassName': selected_class_name,
        'class_name': selected_class,
        'ClassDetails': all_class_recs,
        'StrikeOff': strikeoff_form,

        'Selected_Year_Id': selected_year_id,
        'selected_year_name': selected_year_name,
        'selected_year': selected_year_rec,
        'year_list': all_academicyr_recs,
        'max_date': str(max_date),
        'today_date': str(today_date),
    }
    return form_vals


@user_login_required
def ViewDeactivatedStudentList(request):
    val_dict = request.POST
    form_vals = {}

    if request.method == 'POST':
        request.session['val_dict'] = val_dict
        form_vals = load_st_data(val_dict,request)
    else:
        form_vals = load_st_data(request.session.get('val_dict'),request)
    return render(request, "strikeoff_students_list.html" , form_vals) 


# This view is for Re-activat the strikedoff students
@user_login_required
def SaveReactivatedStudent(request):    
    all_classes = class_details.objects.all()
    all_sections = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    current_academic_year = academic_year.objects.get(current_academic_year=1)
    selected_year_id = current_academic_year.id
    date_error_flag = False

    if request.method == 'POST':
        selected_strikeoff_id = request.POST.get("strikeId")
        reason = request.POST.get("reason")
        date = request.POST.get("reactivation_date")
        dict_data = json.loads(selected_strikeoff_id)

        try:
         for obj in dict_data:
            # print obj

            student_last_strike_off_index = strikeoff_details.objects.filter(student_id=obj).count()-1
            student_last_strike_off_details = strikeoff_details.objects.filter(student_id=obj)[student_last_strike_off_index]
            activation_date = str(student_last_strike_off_details.deactivation_date).split(' ')[0]
            activation_date = datetime.strptime(activation_date, '%Y-%m-%d')
            if activation_date <= datetime.strptime(date, '%Y-%m-%d'):
            # datetime.strptime('Jun 1 2005  1:33PM', '%b %d %Y %I:%M%p')
                student_name = student_details.objects.get(id=obj)
                student_details.objects.filter(id = obj).update(is_active = 1) # 1  is for active and 0 is for in-active
                strikeoff_details.objects.create(student = student_name, reason = reason , reactivation_date = date)

            else:
                date_error_flag = True
                continue


        except:
            messages.warning(request, "Some Unexpected Error Occoured.")
            # student_name = student_details.objects.get(id = dict_data)
            # student_details.objects.filter(id = dict_data).update(is_active = 1) # 1  is for active and 0 is for in-active
            # strikeoff_details.objects.create(student = student_name, reason = reason , reactivation_date = date)

        if date_error_flag:
            messages.warning(request, "Re-Active date must be greated then Striked-off date.")
        else:

            messages.success(request, "Student reactivated sucessfully")
    return redirect('/school/view_deactivated_Student_list/')
    # return render(request, "strikeoff_students_list.html", {'ClassDetails' : all_classes, 'Section' : all_sections, "year_list" : all_academicyr_recs,'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id })


# This view is for Exporting the ms-excel sheet of strikedoff students
@user_login_required
def ExportStrikeOffStudent(request):
    try:

        selected_year_id = request.POST.get('year_name')  # It will be used in HTML page as a value of selected Class
        selected_class_id = request.POST.get('class_name')  # It will be used in HTML page as a value of selected Class
        selected_section_id = request.POST.get('section_name')  # It will be used in HTML page as a value of selected Section

        #     filtering student records to pass HTML page
        class_name = class_details.objects.get(id = selected_class_id)
        section_name = sections.objects.get(id = selected_section_id)
        file_name = "Data_"+str(class_name)+"_"+str(section_name)
        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id, section_name=selected_section_id,class_name=selected_class_id)
        filtered_students = student_details.objects.filter(academic_class_section=year_class_section_obj, is_active=False, joining_date__lte=todays_date())

        column_names = ['Student Code', 'Student Name','Scodoo Id', 'Class', 'Section', 'Parent Code', 'Father Name']
        # rows = filtered_students.values_list('student_code', 'first_name', 'academic_class_section__class_name__class_name','academic_class_section__section_name__section_name', 'parent__code', 'father_name')

        rows = []
        for striked_off_students in filtered_students:
            striked_off_students_rec = []
            striked_off_students_rec.append(striked_off_students.student_code)
            striked_off_students_rec.append(striked_off_students.get_all_name())
            striked_off_students_rec.append(striked_off_students.odoo_id)
            striked_off_students_rec.append(striked_off_students.academic_class_section.class_name.class_name)
            striked_off_students_rec.append(striked_off_students.academic_class_section.section_name.section_name)
            if striked_off_students.parent:
                striked_off_students_rec.append(striked_off_students.parent.code)
            else:
                striked_off_students_rec.append('')

            if striked_off_students.father_name:
                striked_off_students_rec.append(striked_off_students.father_name)
            else:
                striked_off_students_rec.append('')

            rows.append(striked_off_students_rec)

        return export_users_xls(file_name, column_names, rows)

    except:
        all_classes = class_details.objects.all()
        all_sections = sections.objects.all()
        all_academicyr_recs = academic_year.objects.all()

        current_academic_year = academic_year.objects.get(current_academic_year=1)
        selected_year_id = current_academic_year.id

        messages.success(request, "Some Error Occoured.")
        
        return render(request, 'strikeoff_students_list.html', {'ClassDetails': all_classes, 'Section': all_sections, "year_list" : all_academicyr_recs, 'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id})


@user_login_required
def StrikeoffLog(request):
    # srtikeoff_log = strikeoff_details.objects.all()
    srtikeoff_log = []
    user = request.user
    if request.user.is_system_admin() or request.user.is_officer():
        srtikeoff_log = strikeoff_details.objects.all()
    else:
        if request.user.is_supervisor():
            supervisor_list = []
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj in academic_supervisor_class_section_mapping.objects.filter(academic_class_section_mapping=mapped_object, supervisor_id=user.id):
                    if obj:
                        for obj in strikeoff_details.objects.filter(academic_class_section_mapping=mapped_object):
                            if obj not in supervisor_list:
                                supervisor_list.append(obj)
                                srtikeoff_log.append(obj)

        elif request.user.is_subject_teacher():
            subject_teacher_list = []
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            for mapped_object in academic_class_section_mapping.objects.filter(year_name=current_academic_year):
                for obj1 in teacher_subject_mapping.objects.filter(academic_class_section_mapping=mapped_object,staff_id=request.user.id):
                    if obj1:
                        for obj in strikeoff_details.objects.filter(student__academic_class_section=mapped_object):
                            if obj not in subject_teacher_list:
                                subject_teacher_list.append(obj)
                                srtikeoff_log.append(obj)
        else:
            srtikeoff_log = strikeoff_details.objects.filter(Q(student__academic_class_section__assistant_teacher=user.id) | Q(student__academic_class_section__staff_id=user.id))

    all_classes = class_details.objects.all()
    # all_sections = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()
    year_class_section_object = academic_class_section_mapping.objects.all()
    if year_class_section_object:
        try:
            current_academic_year = academic_year.objects.get(current_academic_year=1)
            selected_year_id = current_academic_year.id
            all_class_recs = current_academic_year_mapped_classes(selected_year_id, request.user)
            return render(request, "strikeoff_log.html",
                          {'ClassDetails': all_class_recs, "year_list": all_academicyr_recs,
                           'selected_year_name': current_academic_year, 'Selected_Year_Id': selected_year_id, 'srtikeoff_log':srtikeoff_log})
        except academic_year.DoesNotExist:
            messages.warning(request, "Current Academic Year Not Found.")
            return render(request, 'strikeoff_menu.html')
    else:
        messages.success(request, "Academic Year Class Section Mapping Not Found.")
        return render(request, 'strikeoff_menu.html')

    # return render(request, "strikeoff_log.html",{'srtikeoff_log':srtikeoff_log})

def load_log_data(val_dict,request):
    strikeoff_form = StrikeOffDetailsForm()
    # print "strikeoff_form-->",strikeoff_form
    all_classes = class_details.objects.all()
    all_sections = sections.objects.all()
    all_academicyr_recs = academic_year.objects.all()

    selected_year_id = val_dict.get('year_name')  # It will be used in HTML page as a value of selected Class
    selected_year_rec = academic_year.objects.get(id=selected_year_id)
    selected_year_name = selected_year_rec.year_name

    selected_class_id = val_dict.get('class_name')  # It will be used in HTML page as a value of selected Class
    selected_section_id = val_dict.get('section_name')

    if not selected_class_id == 'All':
        selected_class = class_details.objects.get(id=selected_class_id)
        selected_class_name = selected_class.class_name

    if not selected_section_id == 'All':
        selected_section = sections.objects.get(id=selected_section_id)
        selected_section_name = sections.section_name


    if selected_class_id == 'All':
        selected_class_name = 'All'
        selected_class_id = 'All'


    if selected_section_id == 'All':
        selected_section_id = 'All'
        selected_section = 'All'


    if selected_class_id=='All' and selected_section_id!='All':

        current_academic_year = academic_year.objects.get(current_academic_year=1)
        max_date = current_academic_year.end_date
        # year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
        #                                                                     section_name=selected_section_id
        #                                                                     )
        students_recs = student_details.objects.filter(academic_class_section__year_name_id=selected_year_id,academic_class_section__section_name_id=selected_section_id,).values_list('id')
        filtered_students = strikeoff_details.objects.filter(student_id__in=students_recs)

        all_class_recs = current_academic_year_mapped_classes(selected_year_id, request.user)

        # all_section_recs = current_class_mapped_sections(selected_year_rec, selected_class_id, request.user)

        form_vals = {
            'srtikeoff_log': filtered_students,
            'Selected_Section_Id': selected_section_id,
            'SectName': selected_section_name,
            'section_name': selected_section,
            # 'Section': all_section_recs,
            'Selected_Class_Id': selected_class_id,
            'ClassName': selected_class_name,
            # 'class_name': selected_class,
            'ClassDetails': all_class_recs,
            'StrikeOff': strikeoff_form,

            'Selected_Year_Id': selected_year_id,
            'selected_year_name': selected_year_name,
            'selected_year': selected_year_rec,
            'year_list': all_academicyr_recs,
            'max_date': str(max_date),
        }
        return form_vals

    else:

        current_academic_year = academic_year.objects.get(current_academic_year=1)
        max_date = current_academic_year.end_date

        #     filtering student records to pass HTML page
        year_class_section_obj = academic_class_section_mapping.objects.get(year_name=selected_year_id,
                                                                            section_name=selected_section_id,
                                                                            class_name=selected_class_id)
        students_recs = student_details.objects.filter(academic_class_section=year_class_section_obj).values_list('id')
        filtered_students = strikeoff_details.objects.filter(student_id__in = students_recs)

        all_class_recs = current_academic_year_mapped_classes(selected_year_id, request.user)

        all_section_recs = current_class_mapped_sections(selected_year_rec, selected_class_id, request.user)

        # filtered_students = student_details.objects.filter(class_name_id = selected_class, section_id = selected_section, is_active = 1)

        #     Passing values to the HTML page through form_vals
        form_vals = {
            'srtikeoff_log': filtered_students,
            'Selected_Section_Id': selected_section_id,
            'SectName': selected_section_name,
            'section_name': selected_section,
            'Section': all_section_recs,
            'Selected_Class_Id': selected_class_id,
            'ClassName': selected_class_name,
            'class_name': selected_class,
            'ClassDetails': all_class_recs,
            'StrikeOff': strikeoff_form,

            'Selected_Year_Id': selected_year_id,
            'selected_year_name': selected_year_name,
            'selected_year': selected_year_rec,
            'year_list': all_academicyr_recs,
            'max_date': str(max_date),
        }
        return form_vals
#This view is for displaying all Students list whom we want to strikeoff
@user_login_required
def ViewFilteredLogList(request):
    val_dict = request.POST
    form_vals = {}

    if request.method == 'POST':
        request.session['val_dict'] = val_dict
        form_vals = load_log_data(val_dict,request)
    else:
        form_vals = load_log_data(request.session.get('val_dict'),request)

    return render(request, "strikeoff_log.html" , form_vals)
######################################### Strike-Off Details ###################################
   
from django.conf.urls import include, url
from django.contrib import admin
from . import views
from django.contrib.auth.views import logout
from django.conf import settings
from django.conf.urls.static import static, serve

app_name = 'strike_off'


urlpatterns = [
    url(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),
    
    url(r'^strikeoff/$', views.StrikeOffMenu, name = 'strikeoff'),
    url(r'^strikeoff_apply/$', views.StrikeoffStudent, name = 'strikeoff_apply'),
    url(r'^view_student_list/$', views.ViewStudentList, name = 'view_student_list'),
    url(r'^save_strikeOff_student/$', views.SaveStrikeOffStudent, name = 'save_strikeOff_student'),
    url(r'^view_deactivated_Student_list/$', views.ViewDeactivatedStudentList, name = 'view_deactivated_Student_list'),
    url(r'^strikeoff_student_list/$', views.StrikeoffStudentList, name = 'strikeoff_student_list'),
    url(r'^save_reactivated_student/$', views.SaveReactivatedStudent, name = 'save_reactivated_student'),
    url(r'^export_strikeOff_student/$', views.ExportStrikeOffStudent, name = 'export_strikeOff_student'),
    url(r'^strikeoff_log/$', views.StrikeoffLog, name = 'strikeoff_log'),
    url(r'^filtered_log/$', views.ViewFilteredLogList, name = 'filtered_log'),

   ]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
   


    
    

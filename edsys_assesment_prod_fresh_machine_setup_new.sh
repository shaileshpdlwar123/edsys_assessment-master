#!/bin/bash



#ARGS
arg1=$1

#INIT
if [ -n $arg1 -a "$arg1" == "-p" ]
then
    echo "PROD MACHINE PREPARATION"
    VHOSTNAME="www.schoolzen.org"
    VHOSTALIAS="schoolzen.org"
    ISDEV=false
elif [ -n $arg1 -a "$arg1" == "-t" ]
then
    echo "TEST MACHINE PREPARATION"
    VHOSTNAME="www.test.schoolzen.org"
    VHOSTALIAS="test.schoolzen.org"
    ISDEV=true
    TEST=true
else
    echo "DEV MACHINE PREPARATION"
    VHOSTNAME="www.dev.schoolzen.org"
    VHOSTALIAS="dev.schoolzen.org"
    ISDEV=true
    TEST=false
fi


#To install python 2.7
sudo apt-get update
sudo apt install python-minimal
#To install pip
sudo apt-get install python-pip
#(As we are using pip version 9.0.1)
sudo pip install pip==9.0.1
#To install apache
sudo apt-get install apache2
#To install mysql-server
sudo apt-get install mysql-server
#for (Error: No Module MySQLdb)
sudo apt-get build-dep python-mysqldb
sudo pip2 install MySQL-python
sudo pip2 install mysqlclient==1.3.7
#To install wsgi
sudo apt-get update
sudo apt-get install git
sudo apt-get install python-pip apache2 libapache2-mod-wsgi

#pycopg2 error(Fix)
#pycurl error(fix)
#(Pycurl issue i.e __main__.ConfigurationError:Could not run curl-config: [Errno 2] No such file or directory)
#pylibzma error (fix)

sudo apt-get install libmysqlclient-dev
sudo apt-get install libpq-dev python-dev
sudo apt-get install libsqlite3-dev
sudo apt-get install libcurl4-openssl-dev  
sudo pip2 install pycurl
sudo apt-get install -y liblzma-dev

###########duplicate###############
sudo apt-get install mysql-server
#for (Error: No Module MySQLdb)
sudo apt-get build-dep python-mysqldb
sudo pip2 install MySQL-python
sudo pip2 install mysqlclient==1.3.7
######################################

#Prepare the code base
#Creating virtualenv
sudo pip2 install virtualenv

sudo chmod -R 777 /var/www/html/

cd  /var/www/html/

 
mkdir school
sudo chmod -R 777 school
cd school
virtualenv --no-site-packages venv
source venv/bin/activate

#setup code base
git clone https://gitlab.com/shraddha.redbytes/schoolzen_django.git

sudo chmod -R 777 schoolzen_django/

cd schoolzen_django/

#checkout to production branch for updated code
git status
git checkout schoolzen_prod

#pip2 install -f requirements.txt   # requirement.txt copied from sz aws pip freeze
pip2 install -r requirements.txt


#login with db
sudo service mysql restart

python /var/www/html/school/schoolzen_django/resources/create_db.py 

#setup the db and migration
python manage_prod.py makemigrations registration assessment attendance masters strike_off medical transfer_certificate promotions id_generation parent_portal
#python manage_prod.py makemigrations
python manage_prod.py migrate

sudo chmod 777 static/
python manage_prod.py collectstatic
cd ..

#Setup config files for WSGI User
rm -rf ~/tmp
mkdir ~/tmp
cp /var/www/html/school/schoolzen_django/resources/apache.host.composite.conf ~/tmp


APACHE_VHOSTNAME=`grep -c '%VHOSTNAME%' ~/tmp/apache.host.composite.conf`
if [ APACHE_VHOSTNAME ]; then
	sed -i "s#%VHOSTNAME%#$VHOSTNAME#g" ~/tmp/apache.host.composite.conf
fi

APACHE_VHOSTALIAS=`grep -c '%VHOSTALIAS%' ~/tmp/apache.host.composite.conf`
if [ APACHE_VHOSTALIAS ]; then
	sed -i "s#%VHOSTALIAS%#$VHOSTALIAS#g" ~/tmp/apache.host.composite.conf
fi

APACHE_WSGIHOME=`grep -c '%WSGIHOME%' ~/tmp/apache.host.composite.conf`
if [ APACHE_WSGIHOME ]; then
	sed -i "s#%WSGIHOME%#$WSGIHOME#g" ~/tmp/apache.host.composite.conf
fi



#Add dev.schoolzen.org virtual host
sudo cp ~/tmp/apache.host.composite.conf /etc/apache2/sites-available/
sudo a2ensite apache.host.composite
#Disable the default virtual host. #TODO later put a default error html page.
sudo a2dissite 000-default.conf
sudo a2dissite default-ssl.conf
sudo a2enmod rewrite
sudo sed -i '$a\ServerTokens ProductOnly' /etc/apache2/apache2.conf
sudo sed -i '$a\ServerSignature Off' /etc/apache2/apache2.conf
sudo service apache2 restart


python /var/www/html/school/schoolzen_django/manage_prod.py shell < /var/www/html/school/schoolzen_django/import_initial_master_data.py
#if $ISDEV
#then
#	python ~/school/schoolzen_django/manage_prod.py shell < ~/school/schoolzen_django/import_initial_master_data.py
#fi
